const chalk = require('chalk');

const error = chalk.bold.red;
const warning = chalk.keyword('orange');

const init = chalk.blue;

const master = chalk.bold.cyanBright;

const ws = chalk.yellow;

const kafka = chalk.hex('#f58142')

module.exports = {
    error,
    warning,
    init,
    master,
    ws,
    kafka
}