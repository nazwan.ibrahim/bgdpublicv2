# BGD-proxy-service

First Time Run

1. execute "npm install" in terminal

Run Service with hot reload

1. execute "npm start"

To generate swager file

1. npm run swagger-autogen

To Build Docker Image

1. docker build -t mrfarid/dev_bgdproxy .
2. docker push mrfarid/dev_bgdproxy

For Mac M1/M2

1. docker buildx build --platform linux/amd64 -t mrfarid/dev_bgdproxy .
2. docker push mrfarid/dev_bgdproxy