
const { Kafka } = require('kafkajs')
const chalk = require("../chalk");

const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092']
    
    })


    var goldPrice = [
        {
            "code": "A01",
            "buyPrice": 250.5,
            "sellPrice": 260.5,
            "name": "Supplier Price",
            "productPriceDetailForm": null
        },
        {
            "code": "A02",
            "buyPrice": 250.5,
            "sellPrice": 260.5,
            "name": "Bursa Price",
            "productPriceDetailForm": null
        }
      ]

      var jsonGoldPrice = goldPrice.reduce((obj, item) => {
        obj[item.code] = item;
        return obj;
      }, {});



const producer = kafka.producer()


const genRandom = () => {

    const  precision = 100;
    const  price = Math.floor(Math.random() * (10 * precision - 1 * precision ) + 1 * precision) / (1*precision);
    let  currentTimeStamp = new Date().toISOString()

    goldPrice[0].buyPrice= 200.5+price;
    goldPrice[0].sellPrice = 210.5+price;
    
    goldPrice[1].buyPrice= goldPrice[0].buyPrice + 10;
    goldPrice[1].sellPrice= goldPrice[0].sellPrice + 10;

    // WSMainMessage.timestamp= currentTimeStamp;


  // return `{ "timestamp": ${now} ,"price" : ${price} }`
  return JSON.stringify(jsonGoldPrice.A02);
    }

async function produceMessage() {
    
    await producer.connect();

    setInterval(async () => {

      const randomNumber = Math.floor(Math.random() * 100);
      await producer.send({
        topic: 'test',
        // messages: [{ value: `Random Number: ${randomNumber}` }],
        messages: [{ value: genRandom()}],
      });
      console.log(chalk.kafka(`Sent random price to Kafka.`+genRandom()));
    }, 1000);

  }

module.exports = {
    producer,
    produceMessage,
  };