const { Kafka } = require('kafkajs')
const chalk = require("../chalk");
const ws = require("../websockets/ws");



const kafka = new Kafka({
    clientId: 'my-app',
    brokers: ['localhost:9092']
    
    })

  const consumer = kafka.consumer({ groupId: 'test-group' })

  async function consumeMessage(){
    await consumer.connect()
    await consumer.subscribe({ topic: 'test', fromBeginning: true })

    console.log(chalk.kafka('KAFKA CONSUMER SET'));
  
    await consumer.run({
      eachMessage: async ({ topic, partition, message }) => {
        console.log({
          value: message.value.toString(),
        });
        ws.broadcastWSMessage('price',message.value.toString())
      },
    })
    }

module.exports = {
    consumer,
    consumeMessage
  };