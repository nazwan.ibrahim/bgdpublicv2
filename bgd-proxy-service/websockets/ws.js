const { json } = require('body-parser');
const { WebSocketServer } =require('ws')

const WebSocket = require('ws')

const chalk = require("../chalk");
// import  queryString  from 'querystring'
// import auth from '../controllers/auth.js'
// import consumer from '../kafka/consumer.js'

var goldPrice = [
    {
        "code": "A01",
        "buyPrice": 250.5,
        "sellPrice": 260.5,
        "name": "Supplier Price",
        "productPriceDetailForm": null
    },
    {
        "code": "A02",
        "buyPrice": 250.5,
        "sellPrice": 260.5,
        "name": "Bursa Price",
        "productPriceDetailForm": null
    }
  ]

  // convert array to json json format
var jsonGoldPrice = goldPrice.reduce((obj, item) => {
    obj[item.code] = item;
    return obj;
  }, {});

//   console.log(chalk.master(JSON.stringify(jsonGoldPrice)));

  
  var WSMainMessage = {
    // price: jsonGoldPrice.A02, // Bursa Price
    price: null,
    buyOrderBook: null,
    timestamp: null,
  }

  var priceFromAdmin = {}
  

    //for authentication
    function authenticate(request, callback) {
        const clientSecret = request.headers['client_secret'];
        if (!clientSecret) {
          return callback(new Error('Client secret not provided'));
        }
      
        // Check if the client secret is valid (e.g., in a database or config file)
        const validSecrets = ['secret1', 'secret2', 'secret3'];
        if (!validSecrets.includes(clientSecret)) {
          return callback(new Error('Invalid client secret'));
        }
      
        // If the client secret is valid, return a client object
        const client = { id: 'some-client-id' };
        return callback(null, client);
      }



  const websocketServer = new WebSocketServer({
    noServer: true,
    path: "/ws/main",
  });

function setupExpressServer(expressServer){
  expressServer.on("upgrade", (request, socket, head) => {


    socket.on('error', function(error) {
        console.error('Socket error:', error);
      });

    // authenticate(request, function(err, client) {
    //     if (err || !client) {
    //       socket.write('HTTP/1.1 401 Unauthorized\r\n\r\n');
    //       socket.destroy();
    //       console.log('UNAUTHORIZE ATTEMPT')
    //       return;
    //     }
    // });
    
    websocketServer.handleUpgrade(request, socket, head, (websocket) => {
      websocketServer.emit("connection", websocket, request);
    });
  });

}

  
  websocketServer.on(
    "connection",
    async function connection(websocketConnection, connectionRequest) {

        console.log(chalk.ws('WS: NEW CLIENT CONNECTED'));
    
        websocketConnection.on("message", (message) => {
            const parsedMessage = JSON.parse(message);
            console.log(chalk.ws(parsedMessage));
            websocketConnection.send(JSON.stringify({ message: '' }));
        });

        websocketConnection.on('close', async ()=> {
	// await consumer.disconnect();
        console.log(chalk.ws("WS: A CLIENT CLOSE CONNECTION"))
      });
    }
  );


  ////////////////////////////////////////////////// DEPRECIATED //////////////////////////////////////////////////////

  const genRandom = () => {

    const  precision = 100;
    const  price = Math.floor(Math.random() * (10 * precision - 1 * precision ) + 1 * precision) / (1*precision);
    let  currentTimeStamp = new Date().toISOString()

    goldPrice[0].buyPrice= 200.5+price;
    goldPrice[0].sellPrice = 210.5+price;
    
    goldPrice[1].buyPrice= priceFromAdmin.bursaBuyPrice
    goldPrice[1].sellPrice= priceFromAdmin.bursaSellprice

    // WSMainMessage.timestamp= currentTimeStamp;

     return JSON.stringify(jsonGoldPrice.A02);
    }

  function wsMessageGenerator(){
      console.log(chalk.ws('Websocket Started ' ));
      setInterval(() => {
            // websocketConnection.send(genRandom());
            // sendWSMessage(genRandom());
            broadcastWSMessage('price',genRandom())
            
        }, 1000);
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  function broadcastWSMessage(subtopic,message) {
    
    WSMainMessage[subtopic] = JSON.parse(message);
    
    let  currentTimeStamp = new Date().toISOString();
    WSMainMessage.timestamp= currentTimeStamp;
    // WSMainMessage.timestamp= priceFromAdmin.bursaTime;
    // console.log("Final mesage:",WSMainMessage)


    websocketServer.clients.forEach(function each(client){
            client.send(JSON.stringify(WSMainMessage));
            
            
    })
  };

  var openedSocketFlag = null;
  const timeInterval = 2000
  const setWebsocketTimeout = 2000;

  function subscribePrice(){
    const ws = new WebSocket(process.env.websocketUrl + '/bgd/pricestream', {
      handshakeTimeout:setWebsocketTimeout
    });

    ws.on('open', function open() {
      console.log('WebSocket connection established.');
      openedSocketFlag=true;
      // Send the subscription message to the server
      // ws.send('{"type":"subscribe","symbol":"BTCUSD"}');
      
    });

    ws.on('message', function incoming(data) {
      const message = JSON.parse(data);
      // console.log('Received messageDDD:', message);
      priceFromAdmin = message;
      goldPrice[1].buyPrice= priceFromAdmin.bursaBuyPrice;
      goldPrice[1].sellPrice= priceFromAdmin.bursaSellPrice;
      broadcastWSMessage('price',JSON.stringify(jsonGoldPrice.A02))
      openedSocketFlag=true;
    });

    ws.on('close', function close() {
      console.log('WebSocket connection closed.');
      openedSocketFlag = false;
     
    });

    ws.on('error', (err) => {
      console.log("handshaketimeout : "+setWebsocketTimeout)
      console.log('WEBSOCKET_ERROR: Error', new Error(err.message))
      openedSocketFlag = false
      return err
    })


  }

  async function reconnect() {
    try {
       openedSocketFlag=true;
       await subscribePrice()
    } catch (err) {
      console.log('WEBSOCKET_RECONNECT: Error', new Error(err).message)
      
    }
  }

  // reconnect()
  
 
  // setInterval(() => {
  //   if (!openedSocketFlag) {
  //     console.log("Reconnecting WebSocket...");
  //     reconnect()
  //   }
  // }, timeInterval)



module.exports = {
    websocketServer,
    broadcastWSMessage,
    setupExpressServer,
    wsMessageGenerator,
    subscribePrice
}