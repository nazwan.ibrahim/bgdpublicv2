const { QueryTypes } = require('sequelize');
const connectionDB= require('../configuration/connection');

const { jwtDecode } =require("jwt-decode");

const crypto = require('crypto');

async function sessionValidation(access_token){

    // //TEMP MODIFICATION
    // return{ status:true,
    //     data:{ 
    //         id: "empty",
    //         created_by: "empty",
    //         timestamp_:"empty"}
    //   };

    
    try{
        
        //Decode token to get sid

            console.log("TOKEN TO BE DECODE: ",access_token);
            const decodedJWT = jwtDecode(access_token);
            console.log("DECODING ACCESS TOKEN");
            // console.log(decodedJWT);
            console.log("sid: "+decodedJWT.sid)

            // console.log("TOKEN TO BE QUERY: ",access_token);
            // console.log("SI")

            const query = "SELECT * FROM user_session WHERE sid = :sid AND is_valid = true";
            const replacements = { sid: decodedJWT.sid };

            const session = await connectionDB.registrationCursor.query(query, 
                    {   replacements: replacements,
                        type: QueryTypes.SELECT });
                        
            console.log("Queried_Session: ",session);
            if(session.length>0){
                console.log("VALID SESSION");
                return{ status:true,
                        data:session[0]
                    };
            }else {
                console.log("SESSION EXPIRED");
                return {status:false,
                        data:session[0]
                };
                
            }
        }
        catch(error){
            console.log("ERROR WHEN VALIDATE USER_SESSION. Reason: ",error)
            return {status:false,
                    data:null
                    }
        }
}

async function updateUserSession(sso_id){

    try{
        const query = "UPDATE user_session SET is_valid = false where sso_user_id = :sso_id";
        const replacements = { sso_id: sso_id };

        const result = await connectionDB.registrationCursor.query(query, 
            {   replacements: replacements,
                type: QueryTypes.UPDATE });
            return true;
    }
    catch(error){
        console.log("ERROR WHEN UPDATING USER_SESSION. Reason: ",error)
        return false;
    }

}

async function createNewSession(sso_id,access_token,username){

    try{

        //Decode token to get sid

        const decodedJWT = jwtDecode(access_token);

        console.log("DECODING ACCESS TOKEN");
        // console.log(decodedJWT);
        console.log("sid: "+decodedJWT.sid)

        console.log(crypto.randomUUID());
        const query = "INSERT INTO user_session (id , sso_user_id  , access_token, sid, is_valid, version, created_by, created_date,timestamp_) VALUES(:id, :sso_user_id, :access_token, :sid, true,1,:created_by,:created_date,:timestamp_);";
        const replacements = { 
                                id : crypto.randomUUID(),
                                sso_user_id: sso_id ,
                                access_token:access_token,
                                sid:decodedJWT.sid,
                                created_by : username,
                                created_date : new Date(),
                                timestamp_ : Math.floor(Date.now() / 1000)
                            };
        
        const result = await connectionDB.registrationCursor.query(query, 
            {   replacements: replacements,
                type: QueryTypes.INSERT });
        return true;
    }
    catch(error){
        console.log("ERROR WHEN CREATING NEW USER_SESSION. Reason: ",error)
        return false;
    }

}

//insert into Transaction_Lock (userID, transactionCode, isLock = true, userSessionID) value ()
async function newTransactionLock(transactionCode,userSessionID,createdBy){
    try{
        const newUUID = crypto.randomUUID();
        const query = "INSERT INTO transaction_lock (id , transaction_code  , is_lock, user_session_id, version, created_by, created_date) VALUES(:id, :transaction_code,true,:user_session_id,1,:created_by,:created_date);";
        const replacements = { 
                                id : newUUID,
                                transaction_code : transactionCode,
                                user_session_id: userSessionID ,
                                created_by : createdBy,
                                created_date : new Date()
                            };
        
        const result = await connectionDB.registrationCursor.query(query, 
            {   replacements: replacements,
                type: QueryTypes.INSERT });
        return {status:true,
                data:{
                    lockID:newUUID
                    }
                };
    }
    catch(error){
        console.log("ERROR WHEN CREATING NEW USER_SESSION. Reason: ",error)
        return {status:false,
            data:{}
            };
    }
}

async function checkTransactionLock(sessionID){
 

    const query = "SELECT * FROM transaction_lock WHERE user_session_id = :user_session_id AND is_lock = true";
    const replacements = { user_session_id: sessionID };

    const lockedTransaction = await connectionDB.registrationCursor.query(query, 
            {   replacements: replacements,
                type: QueryTypes.SELECT });
                
    
    if(lockedTransaction.length>0){ //means there is a lock that are not released yet
        console.error("Transasaction is currently locked");
        console.log("Queried_Transaction Locked: ",lockedTransaction);
        return{ status:false,
                data:null
              };
    }else {
        return {status:true,
                data:lockedTransaction[0]
          };
    }
}

async function releaseTransactionLock(transactionLockID,username){
    
    //temporary modification
    return true;
    try{
        const query = "UPDATE transaction_lock SET is_lock = false, last_modified_date=:update_date, last_modified_by=:update_by, version = version + 1 where id = :lock_id";
        const replacements = { lock_id: transactionLockID ,
                               update_date: new Date(),
                               update_by: username
                            };

        const result = await connectionDB.registrationCursor.query(query, 
            {   replacements: replacements,
                type: QueryTypes.UPDATE });
        return true;
    }
    catch(error){
        console.log("ERROR WHEN UPDATING USER_SESSION. Reason: ",error)
        return false;
    }
}


async function validateTransactionLock(sessionID,transactionCode,username,timestamp){
 
    
    // Temporary modification
    return{ status:true,
            data:{ id: 'empty'}
        };

    try{
        const query = "BEGIN; DO $$ BEGIN perform pg_advisory_xact_lock("+timestamp+"); END $$; "
        +" WITH inserted_rows AS (INSERT INTO transaction_lock (id, transaction_code, is_lock, version, created_by ,created_date,user_session_id) SELECT :id, :transaction_code, true, 1 ,:created_by ,:created_date,:user_session_id WHERE NOT EXISTS (SELECT * FROM transaction_lock WHERE user_session_id =:user_session_id and is_lock = true) RETURNING * )"+
        "SELECT * from inserted_rows; COMMIT;";

        const newUUID = crypto.randomUUID();
        const replacements = {
                            user_session_id:sessionID,
                            id:newUUID,
                            transaction_code: transactionCode,
                            created_by: username,
                            created_date : new Date()};

        const lockedTransaction = await connectionDB.registrationCursor.query(query, 
                {   replacements: replacements,
                    type: QueryTypes.SELECT });
                    
        // console.log("TESTT", lockedTransaction);
        if(lockedTransaction.length<=0){ // not creating new lock, means there is a lock that are not released yet
            console.error("Transasaction is currently locked");
            // console.log("Queried_Transaction Locked: ",lockedTransaction[0]);
            return{ status:false,
                    data:null
                };
        }else {
            return {status:true,
                    data:lockedTransaction[0]
            };
        }
    }
    catch(error){
        console.log("ERROR" +error);
        return {status:false,
            data:null }
    }
}



module.exports = {
    sessionValidation,
    updateUserSession,
    createNewSession,
    newTransactionLock,
    checkTransactionLock,
    releaseTransactionLock,
    validateTransactionLock
};