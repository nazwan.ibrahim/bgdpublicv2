// const formidable = require("formidable");
const tradingService = require("../services/trading");
const walletService = require("../services/wallet");
const administrationService = require("../services/administration");
const moment = require('moment-timezone');
const sessionController = require("../controllers/session");


const purchaseInit = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
   #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: {
                                          "totalCash": 2000,
                                          "quantity": 2,
                                          "type":"S05",
                                          "inputType":"S21",
                                          "price":200,
                                          "fee":0.002
                                      }
                                    
      }
*/
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }
  

  await tradingService
  .purchaseInit(requestBody,requestHeaders)
  .then((response) => {
    console.log("*************** Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("*************** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const purchase = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: {
                                        "quantity": 1.3,
                                        "type": "S05",
                                        "inputType": "S22",
                                        "reference":"",
                                        "price": 200,
                                        "netPrice": 345,
                                        "totalPrice": 200.4,
                                        "priceRequestID":"",
                                        "tradingFeeList": [
                                          {
                                            "feeType": "Transaction Fee",
                                            "feeTypeCode": "F09",
                                            "setupFees": 0.2,
                                            "setupUom": "%",
                                            "setupUomCode": null,
                                            "chargeFees": 0.4,
                                            "chargeUom": "RM",
                                            "chargeUomCode": "J01"
                                          },
                                          {
                                            "feeType": "SST",
                                            "feeTypeCode": "TAX01",
                                            "setupFees": 0,
                                            "setupUom": "%",
                                            "setupUomCode": "string",
                                            "chargeFees": 0.2,
                                            "chargeUom": "RM",
                                            "chargeUomCode": "J01"
                                          }
                                        ]
                                      }
                           }
      
*/ 

    console.log("API purchase called")
    const requestBody = req.body;
    const requestHeaders = req.headers;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    var type="";
    if(requestBody.type=="S05"){
      type="F05";
    }
    else{
      type="F06";
    }

    const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,type,session.data.created_by,session.data.timestamp_);
    if(validateTransactionLock.status==false){
      console.error("Transaction locked")
      console.warn("Someone is trying to make forbiden concurrent transaction");
      res.status(401).send("Concurrent Transaction Request");
      return;
    }
  

    try{
      console.log("Step 1 : Calling api - purchase (trade engine)")
      console.log("###########  puchase REQUEST BODY :",requestBody);
      var purchaseLogic =  await tradingService
      .purchase(requestBody, requestHeaders)
      .then((response) => {
        console.log("********* PURCHASE Response : ", response.data);
        return response;
      });

      console.log("Step 2 : Calling api - createTransaction (wallet)")
      console.log("########### createTransaction REQUEST BODY : ",purchaseLogic.data);
      var createTransactions = await walletService
      .createTransactions(purchaseLogic.data, requestHeaders)
      .then((response) => {
        console.log("**********createTransaction Response : ", response);
        res.status(200).send(response);
        return response;
      });

    } catch(error) {
      console.error("*************** ERROR >: ", error);
      res.status(error.response.status).send(error.response);
    }

    const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
    
};

const getBuyPrice = async (req, res, next) => {
  /*#swagger.tags = ['Trading']
  #swagger.security = [{
    "bearerAuth": []
  }] */
  
  console.log("API getBuyPrice called")
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  try {
  const bursaRevenue = await administrationService
    .getBursaRevenue(requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      return response.data;
    })

    let margin = bursaRevenue.find(u => u.feeTypeCode == "C05");
    console.log("margin -> ", margin);

    const queryBuyPrice = await walletService
    .getQueryBuyPrice(margin, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      // hide supplier price
      delete response.data.A01;
      res.status(200).send(response.data);
      return response.data;
    })
    
  } catch (error) {
    console.log("error ->", error);
    res.status(400).send(error);
  }
};

const getSellPrice = async (req, res, next) => {
  /*#swagger.tags = ['Trading']
  #swagger.security = [{
    "bearerAuth": []
  }] */

  console.log("API getSellPrice called")
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  try {
  const bursaRevenue = await administrationService
    .getBursaRevenue(requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      return response.data;
    })

    let margin = bursaRevenue.find(u => u.feeTypeCode == "C06");
    console.log("margin -> ", margin);

    const queryBuyPrice = await walletService
    .getQuerySellPrice(margin, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      // hide supplier price
      delete response.data.A01;
      res.status(200).send(response.data);
      return response.data;
    })
    
  } catch (error) {
    console.log("error ->", error);
    res.status(400).send(error);
  }
};

const getMarketTrade = async (req, res, next) => {
  /*#swagger.tags = ['Trading']
  #swagger.security = [{
    "bearerAuth": []
  }] */

  console.log("API getSellPrice called")
  const requestBody = req.body;
  const requestHeaders = req.headers;

  try {
    var date_time = new Date();
    console.log(date_time);
    var malaysiaTime = moment.tz(date_time, "Asia/Kuala_Lumpur");
    var endTime = moment(process.env.end_time, 'h:mma');
    var startTime = moment(process.env.start_time, 'h:mma');
    
    console.log("Current Time : ", malaysiaTime);
    console.log("End Time : ", endTime);
    console.log("Start Time : ", startTime);

    if(malaysiaTime.isBefore(startTime) || malaysiaTime.isAfter(endTime)){
      res.status(400).send({
        "message":"The trading hours is currently unavailable.",
      });
    } else{
      res.status(200).send({
        "message":"The trading hours is currently available.",
      });
    }
    
  } catch (error) {
    console.log("error ->", error);
    res.status(400).send(error);
  }
};

const getMarketReconcile = async (req, res, next) => {
  /*#swagger.tags = ['Trading']
  #swagger.security = [{
    "bearerAuth": []
  }] */

  const requestBody = req.body;
  const requestHeaders = req.headers;

  try {
    var date_time = new Date();
    console.log(date_time);
    var malaysiaTime = moment.tz(date_time, "Asia/Kuala_Lumpur");
    var end_time_reconcile = moment(process.env.end_time_reconcile, 'h:mma');
    var start_time_reconcile = moment(process.env.start_time_reconcile, 'h:mma');
    
    console.log("Current Time : ", malaysiaTime);
    console.log("End Time : ", endTime);
    console.log("Start Time : ", startTime);

    if(malaysiaTime.isBefore(end_time_reconcile) || malaysiaTime.isAfter(start_time_reconcile)){
      res.status(400).send({
        "message":"Market is close for reconcilation.",
        "startTime":start_time_reconcile,
        "endTime":end_time_reconcile
      });
    } else{
      res.status(200).send({
        "message":"Market is open.",
        "startTime":start_time_reconcile,
        "endTime":end_time_reconcile
      });
    }
    
  } catch (error) {
    console.log("error ->", error);
    res.status(400).send(error);
  }
};


module.exports = {
    purchaseInit,
    purchase,
    getBuyPrice,
    getSellPrice,
    getMarketTrade,
    getMarketReconcile
  };