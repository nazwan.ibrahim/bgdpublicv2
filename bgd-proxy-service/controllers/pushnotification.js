
// var admin = require("firebase-admin");


const moment = require('moment');


const notificationService = require("../services/pushnotification");
const { response } = require("express");
const e = require("express");



// const notification_options = {
//     priority: "high",
//     timeToLive: 60 * 60 * 24
//   };



const push = async(req, res, next) =>{

    /*  
       #swagger.parameters['body'] = {
            in: 'body',
            required: true,
            schema: {
                        "notification": {
                                        "title": "something",
                                        "body": "hello"
                                        },
                        "data" : {
                                    "categoryCode":"NC01",
                                    "typeCode":"NT02",
                                    "dataUrl":"",
                                    "dataReference" : "2333",
                                },
                        "token": "",
                        "topic":"OPTIONAL only either include token or topic param"
                }
        }
    */ 

      
        const requestBody = req.body;
        const requestHeaders = req.headers;
        // const requestparams= req.query;

        const response= await notificationService
          .pushNotification(requestBody)
          .then(response =>{
            if(response=="success"){
                res.status(200).send({
                                        responseCode: 'RCS001',
                                        description: 'OK',
                                        data: '',
                                        httpStatus: 200,
                                        dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                                      });
                return
              }
            else{
              throw "failed";
            }
          })
          .catch(error =>{
              res.status(400).send({ 
                                      responseCode: 'RCE999',
                                      description: 'failed',
                                      data: 'Can not send Notification',
                                      httpStatus: 400,
                                      dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                                    });
          });

};





const saveAndPushnotification = async(requestHeaders,requestBody)=>{

  

  //createPushNotification
  console.log("save notification Body: ",requestBody)
  const createNotificationResponse= await notificationService
    .saveNotification(requestBody, requestHeaders)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      console.log("ERROR",error.response)
      return error.response;
    });
  
  if(!createNotificationResponse || createNotificationResponse.status!=200){
      console.log("FAILED TO CREATE NOTIFICATION: create to db failed")
        return {status:createNotificationResponse.status || 500,
                data:createNotificationResponse.data || "failed to create Notification"}
  }

  const getDeviceIdResponse = await notificationService
      .getDeviceId(requestBody,requestHeaders)
      .then((response=>{
          return response
      }))
      .catch((error)=>{
        console.log("ERROR",error.response)
        return error.response;
      })
  
  if(!getDeviceIdResponse || getDeviceIdResponse.status!=200 || !getDeviceIdResponse.data.data){
    console.log("FAILED TO CREATE NOTIFICATION: getDeviceId failed")
    return {status:getDeviceIdResponse.status || 500,
            data:getDeviceIdResponse.data || "getDeviceIDFailed"}
  }

  console.log("DeviceID response: ",getDeviceIdResponse.data)
  // if(!requestBody.token){ // if user already give token, just pick the token that user give instead, for testing purpsose
  //   requestBody.token=getDeviceIdResponse.data.data
  // }
  requestBody.token=getDeviceIdResponse.data.data

  
    const pushNotificationResponse = await notificationService
      .pushNotification(requestBody)
      .then(response=>{
        return;
      })
      .catch(error=>{
        return;
      })
    
    return {status:createNotificationResponse.status,
            data:createNotificationResponse.data}
 
}

const updateNotificationStatus = async(req, res, next) =>{

  /*  
    #swagger.security = [{
                 "bearerAuth": []
          }] 

    #swagger.parameters['body'] = {
            in: 'body',
            required: true,
            schema: [{
                        "notificationId":"",
                        "updateStatusCode":"G17"
                },]
        }

  */ 
    
      const requestBody = req.body;
      const requestHeaders = req.headers;
      // const requestparams= req.query;

      const response = await notificationService
          .updateNotificationStatus(requestBody,requestHeaders)
          .then(response =>{
            res.status(response.status || 200).send(response.data);
            return
          })
          .catch(error =>{
            res.status(error.response.status|| 500).send(error.response.data || "Server Error");
            return;
          });
        return ;

};

const notificationList = async(req, res, next) =>{

  /*  
    #swagger.security = [{
                 "bearerAuth": []
          }] 
  */ 
    
      const requestBody = req.body;
      const requestHeaders = req.headers;
      // const requestparams= req.query;

      const response = await notificationService
          .notificationList(requestBody,requestHeaders)
          .then(response =>{
            res.status(response.status || 200).send(response.data);
            return;
          })
          .catch(error =>{
            res.status(error.response.status|| 500).send(error.response.data || "Server Error");
          });
      return;
};

const testSaveAndPushInternal = async(req, res, next) =>{

  /*  
    #swagger.security = [{
                 "bearerAuth": []
          }] 
    
    #swagger.parameters['body'] = {
        in: 'body',
        required: true,
        schema: {
                    "notification": {
                                    "title": "something",
                                    "body": "hello"
                                    },
                    "data" : {
                                "categoryCode":"NC01",
                                "typeCode":"NT02",
                                "dataUrl":"",
                                "dataReference" : "2333",
                            },
                    "username":""
            }
    }
    
  */ 

      const requestBody = req.body;
      const requestHeaders = req.headers;
      // const requestparams= req.query;

      const createNotificationResponse= await notificationService
        .saveNotificationInternal(requestBody, requestHeaders)
        .then((response) => {
          res.status(response.status).send(response.data)
          return response;
        })
        .catch((error) => {
          console.log("ERROR",error.response)
          res.status(error.response.status || 500).send(error.response.data|| "FAILED TO CREATE NOTIFICATION")
          return error.response;
        });
      
  if(!createNotificationResponse || createNotificationResponse.status!=200){
      console.log("FAILED TO CREATE NOTIFICATION: create to db failed")
        return {status:createNotificationResponse.status || 500,
                data:createNotificationResponse.data || "failed to create Notification"}
  }

      delete requestBody.username;

      const pushNotificationResponse = await notificationService
        .pushNotification(requestBody)
        .then(response=>{
          return;
        })
        .catch(error=>{
          return;
        })
      
    return {status:createNotificationResponse.status,
            data:createNotificationResponse.data}
};


const testSaveAndPush = async(req, res, next) =>{

  /*  
    #swagger.security = [{
                 "bearerAuth": []
          }] 
    
    #swagger.parameters['body'] = {
        in: 'body',
        required: true,
        schema: {
                    "notification": {
                                    "title": "something",
                                    "body": "hello"
                                    },
                    "data" : {
                                "categoryCode":"NC01",
                                "typeCode":"NT02",
                                "dataUrl":"",
                                "dataReference" : "2333",
                            },
            }
    }
    
  */ 
    
      const requestBody = req.body;
      const requestHeaders = req.headers;
      // const requestparams= req.query;

      const response = await saveAndPushnotification(requestHeaders,requestBody)
          .then(response =>{
            res.status(response.status || 200).send(response.data);
            return;
          })
          .catch(error =>{
            res.status(error.response.status|| 500).send(error.response.data || "Server Error");
          });
      return;
};


module.exports = {
    push,
    notificationList,
    updateNotificationStatus,
    saveAndPushnotification,
    testSaveAndPush
}