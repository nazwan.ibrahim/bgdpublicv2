// const formidable = require("formidable");
const walletService = require("../services/wallet");
const onboardingService = require("../services/onboarding");
const onboardingController = require("../controllers/onboarding");
const fs = require('fs');
const sessionController = require("../controllers/session");
const notificationService = require("../controllers/pushnotification");
const emailNotificationService = require("../services/emailNotification");
const notificationMessage = require("../resource/pushNotificationMessage");


const getWalletDetails = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
    const requestBody = req.body;
    const requestHeaders = req.headers;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
    
    await walletService
    .getWalletDetails(requestBody,requestHeaders)
    .then((response) => {
      console.log("********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
       
    });

};

const createWallet = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: [
                                        {
                                            "uomCode": "J01",
                                            "amount": "0.00",
                                            "walletTypeCode": "E01",
                                            "regTypeCode": "D02"
                                        },
                                        {
                                            "uomCode": "J02",
                                            "amount": "0.00",
                                            "walletTypeCode": "E02",
                                            "regTypeCode": "D02"
                                        }
                                      ]
                                    
      }
*/ 


    const requestBody = req.body;
    const requestHeaders = req.headers;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
    

    await walletService
    .createWallet(requestBody,requestHeaders,'createWallet')
        .then((response) => {
          console.log("#############  Response : CreateWallet : ", response);
           res.status(200).send(response.data);
        })
        .catch((error) => {
          console.error("###########  ERROR : Create Wallet", error.response);
          res.status(400).send(error.response.data);
        });

    // createWallet(requestBody,requestHeaders)
    // .then((response) => {
    //   console.log("********** Response : ", response);
      
    // })
    // .catch((error) => {
    //   console.error("********** Error : ", error);
      
    // });

};

const topupwallet = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: [
                                          {
                                              "acctNumber": "",
                                              "amount": "20.00",
                                              "transactionMethodCode": "H01",
                                              "transactionTypeCode": "F01",
                                              "statusCode": "G01",
                                              "channelCode":"BW",
                                              "bicCode":"",
                                              "currency":"MYR",
                                              "customerName":"Narissa",
                                               "feesForms": [
                                                              {
                                                                  "feeType": "Transaction Fee",
                                                                  "feeTypeCode": "F09",
                                                                  "setupFees": 5.5000,
                                                                  "setupUom": "RM",
                                                                  "setupUomCode": null,
                                                                  "chargeFees": 5.5000,
                                                                  "chargeUom": "RM",
                                                                  "chargeUomCode": "J01"
                                                              }
                                                          ]
                                          }
                                      ]
                                    
      }
*/ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    
  
    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F01",session.data.created_by,session.data.timestamp_);
    if(validateTransactionLock.status==false){
      console.error("Transaction locked")
      console.warn("Someone is trying to make forbiden concurrent transaction");
      res.status(401).send("Concurrent Transaction Request");
      return;
    }

    const getIdDetailsRequest= await onboardingService
                            .getUserDetails(requestBody, requestHeaders)
                            .then((response) => {
                              // console.log("************ Response : ", response.data);
                              return response.data
                            })
                            .catch((error) => {
                              console.error("********** Error : ", error);
                              // res.status(error.response.status).send(error.response.data);
                            });
    
    if(!getIdDetailsRequest){
      const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
      res.status(500).send("Internal Error: Cannot getUserDetails")
      return;
    }
    console.log("USERDETAILS:",getIdDetailsRequest)
    console.log("ID TYPE: ",getIdDetailsRequest.identificationType.code)
    console.log("ID Number: ",getIdDetailsRequest.identificationNumber)
    
    //COMMENT TEMP FOR PROD
    // requestBody[0].identificationTypeCode = getIdDetailsRequest.identificationType.code;
    // requestBody[0].identificationNumber = getIdDetailsRequest.identificationNumber;
  
    console.log("RequestBody For walletTopup: ",requestBody)
  
    const topupRequest = await walletService
      .topupwallet(requestBody,requestHeaders)
      .then((response) => {
        console.log("********* Response : ", response);
        res.status(200).send(response);        
        return 'SUCCESS';
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(500).send(error);
        return 'FAIL';
      });
    
      if(!topupRequest){
          return;
        }
        else{
          const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
        }

};

const topupwalletV2 = async(req, res, next) =>{
/*  
    #swagger.security = [{
                "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: 
                                          {
                                              "referenceId":"",
                                              "channelCode":"BW",
                                              "bicCode":"ACFBMYK1",
                                              "transactionMethodCode":"H01"
                                          }
                                      
                                    
      }
*/ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    
  
    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F01",session.data.created_by,session.data.timestamp_);
    if(validateTransactionLock.status==false){
      console.error("Transaction locked")
      console.warn("Someone is trying to make forbiden concurrent transaction");
      res.status(401).send("Concurrent Transaction Request");
      return;
    }

    const getIdDetailsRequest= await onboardingService
                            .getUserDetails(requestBody, requestHeaders)
                            .then((response) => {
                              // console.log("************ Response : ", response.data);
                              return response.data
                            })
                            .catch((error) => {
                              console.error("********** Error : ", error);
                              // res.status(error.response.status).send(error.response.data);
                            });
    
    if(!getIdDetailsRequest){
      const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
      res.status(500).send("Internal Error: Cannot getUserDetails")
      return;
    }
    console.log("USERDETAILS:",getIdDetailsRequest)
    console.log("ID TYPE: ",getIdDetailsRequest.identificationType.code)
    console.log("ID Number: ",getIdDetailsRequest.identificationNumber)
    console.log("Full Name: ",getIdDetailsRequest.fullName)
    
    //COMMENT TEMP FOR PROD
    requestBody.identificationTypeCode = getIdDetailsRequest.identificationType.code;
    requestBody.identificationNumber = getIdDetailsRequest.identificationNumber;
    requestBody.customerName=getIdDetailsRequest.fullName;
    
    
    
    console.log("RequestBody For walletTopup: ",requestBody)
  
    const topupRequest = await walletService
      .topupwallet(requestBody,requestHeaders)
      .then((response) => {
        console.log("********* Response : ", response);
        res.status(200).send(response);        
        return 'SUCCESS';
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(500).send(error);
        return 'FAIL';
      });
    
      if(!topupRequest){
          return;
        }
        else{
          const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
        }

};

const confirmPurchase = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: 
                                        {
                                          "userName":"",
                                          "userPin":"",
                                          "referenceId":""
                                    
                                        }

                                      }
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
  
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }


      const verifyPin = await onboardingService
        .verifyPin(requestBody,requestHeaders)
        .then((response) => {
          console.log("**********Response : ", response.data);
          return response;
        })
        .catch((error) => {
          console.error("******* Error : ", error.response);
          return error.response;
        });


      if(!verifyPin || verifyPin.status!=200){
        res.status(401).send("Pin Invalid");
        return ;
      }

      const TRANSACTION_CODE= requestBody.referenceId.slice(0,3);
      console.log("TRANSACTION_CODE: ",TRANSACTION_CODE);
      
      const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,TRANSACTION_CODE,session.data.created_by,session.data.timestamp_);
      if(validateTransactionLock.status==false){
        console.error("Transaction locked")
        console.warn("Someone is trying to make forbiden concurrent transaction");
        res.status(401).send("Concurrent Transaction Request");
        return;
      }

      const confirmPurchaseReq = await walletService
        .confirmPurchase(requestBody,requestHeaders)
        .then((response) => {
          console.log("*********** Response : ", response.data);
          res.status(200).send(response.data);
          return response;
        })
        .catch((error) => {
          console.error("*********** Error : ", error);
          res.status(error.response.status).send(error.response.data);
          return ;
        });
  
        const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
        
        if(!confirmPurchaseReq|| !confirmPurchaseReq.status ||  confirmPurchaseReq.status!=200){ //if failed dont send notification yet
          return; 
        }
        
        
        console.log("Confirm Purchase Req : ", confirmPurchaseReq)

        const notificationBody= {
          notification: {
                          title: "",
                          body: ""
                          },
          data : {
                      categoryCode:"NC01",
                      typeCode:"",
                      dataUrl:"",
                      dataReference : requestBody.referenceId
                   }
        }

      
        if(TRANSACTION_CODE=="F05"){
          // if(!confirmPurchaseReq.status || !confirmPurchaseReq.status==200) //failed
            // notificationBody.notification.title="Purchase Successful"
            // notificationBody.notification.body="You have succesfully bought "+confirmPurchaseReq.data.amount+" g of gold"
            notificationBody.notification= notificationMessage.getMessage("NT051",confirmPurchaseReq.data.amount,confirmPurchaseReq.data.message);
            notificationBody.data.typeCode="NT051"
        } else if (TRANSACTION_CODE=="F06"){
          // notificationBody.notification.title="Sale Successful"
          // notificationBody.notification.body="You have sucessfully sold "+confirmPurchaseReq.data.amount+" g of gold"
          notificationBody.notification= notificationMessage.getMessage("NT061",confirmPurchaseReq.data.amount,confirmPurchaseReq.data.message);
          notificationBody.data.typeCode="NT061"
        }


        const notification= await notificationService.saveAndPushnotification(requestHeaders,notificationBody)
            .then(response=>{
              return response;
            })
            .catch((error)=>{
              return error
            })

            if(TRANSACTION_CODE=="F05"){
                const emailNotification = await emailNotificationService.goldPurchaseEmail(requestBody.referenceId);
            } else if (TRANSACTION_CODE=="F06") {
                const emailNotification = await emailNotificationService.goldSaleEmail(requestBody.referenceId);
            }
      
  
};

const withdrawWalletV2 = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: 
                                        {
                                          "userName":"",
                                          "userPin":"",
                                          "referenceId":""
                                        }

                                      }
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
  
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }


      const verifyPin = await onboardingService
        .verifyPin(requestBody,requestHeaders)
        .then((response) => {
          console.log("**********Response : ", response.data);
          return response;
        })
        .catch((error) => {
          console.error("******* Error : ", error.response);
          return error.response;
        });


      if(!verifyPin || verifyPin.status!=200){
        res.status(401).send("Pin Invalid");
        return ;
      }

      const getIdDetailsRequest= await onboardingService
        .getUserDetails(requestBody, requestHeaders)
        .then((response) => {
          // console.log("************ Response : ", response.data);
          return response.data
        })
        .catch((error) => {
          console.error("********** Error : ", error);
          // res.status(error.response.status).send(error.response.data);
        });

        if(!getIdDetailsRequest){
          res.status(500).send("Internal Error: Cannot getUserDetails")
          return;
        }

        console.log("USERDETAILS:",getIdDetailsRequest)
        console.log("Full Name: ",getIdDetailsRequest.fullName)

        
        requestBody.investorFullName=getIdDetailsRequest.fullName;

      
      const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F02",session.data.created_by,session.data.timestamp_);
      if(validateTransactionLock.status==false){
        console.error("Transaction locked")
        console.warn("Someone is trying to make forbiden concurrent transaction");
        res.status(401).send("Concurrent Transaction Request");
        return;
      }
      console.log("widraw request: ", requestBody);

      const withdrawRequest = await walletService
        .withdrawWallet(requestBody,requestHeaders)
        .then((response) => {
          console.log("***********Withdraw Response : ", response);
          res.status(200).send(response);
          return response;
        })
        .catch((error) => {
          console.error("***********Withdraw Error : ", error);
          res.status(error.response).send(error.response);
          return ;
        });

        const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);

        if(!withdrawRequest || withdrawRequest.status!='G01' ){ //if failed dont send notification yet
          return; 
        }


      const notificationBody= {
        // notification: {
        //                 title: "Withdraw Successful",
        //                 body: "You have sucessfully withdraw RM "+withdrawRequest.data.amount
        //                 },
        notification: notificationMessage.getMessage("NT021",withdrawRequest.amount,null),
        data : {
                    categoryCode:"NC01",
                    typeCode:"NT021",
                    dataUrl:"",
                    dataReference : requestBody.referenceId
                  }
      }

      const notification= await notificationService.saveAndPushnotification(requestHeaders,notificationBody)
      .then(response=>{
        return response;
      })
      .catch((error)=>{
        return error
      })

      const emailNotification = await emailNotificationService.cashWithdrawalEmail(requestBody.referenceId);
  
};

const withdrawWalletOld = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: [
                                          {
                                              "acctNumber": "",
                                              "amount": "0.50",
                                              "transactionMethodCode": "H01",
                                              "transactionTypeCode": "F02",
                                              "statusCode": "G01",
                                                "feesForms": [
                                                                {
                                                                    "feeType": "Transaction Fee",
                                                                    "feeTypeCode": "F09",
                                                                    "setupFees": 5.5000,
                                                                    "setupUom": "RM",
                                                                    "setupUomCode": null,
                                                                    "chargeFees": 5.5000,
                                                                    "chargeUom": "RM",
                                                                    "chargeUomCode": "J01"
                                                                }
                                                            ]
                                          }
                                      ]
                                    }
*/ 
    const requestBody = req.body;
    const requestHeaders = req.headers;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F02",session.data.created_by,session.data.timestamp_);
    if(validateTransactionLock.status==false){
      console.error("Transaction locked")
      console.warn("Someone is trying to make forbiden concurrent transaction");
      res.status(401).send("Concurrent Transaction Request");
      return;
    }


    const withdrawRequest = await walletService
      .withdrawWallet(requestBody,requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return 'SUCCESS';
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
        return 'FAIL';
      });

      if(!withdrawRequest){
        return;
      }
      else{
        const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
      }

};

const validateWallet = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['acctNumber'] = {
                              in: 'query',
                              required: true,
                              schema: ''
          }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await walletService
  .validateWallet(requestBody,requestHeaders,requestparams)
  .then((response) => {
    console.log("*********** Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("*********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const transferWallet = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: [
                                            {
                                              "acctNumberSender": "",
                                              "acctNumberReceiver": "",
                                              "amount": 1.5,
                                              "bursaSellPrice": 290.916407,
                                              "statusCode": "G01",
                                              "reason": "test",
                                              "reference": "",
                                              "feesForms": [
                                                {
                                                  "feeType": "Transaction Fee Sender",
                                                  "feeTypeCode": "C04",
                                                  "setupFees": 0.2,
                                                  "setupUom": "%",
                                                  "setupUomCode": "J04",
                                                  "chargeFees": 0.581832814,
                                                  "chargeUom": "RM",
                                                  "chargeUomCode": "J01"
                                                },
                                                {
                                                  "feeType": "SST",
                                                  "feeTypeCode": "TAX01",
                                                  "setupFees": 0,
                                                  "setupUom": "%",
                                                  "setupUomCode": null,
                                                  "chargeFees": 0.2,
                                                  "chargeUom": "RM",
                                                  "chargeUomCode": "J01"
                                                }
                                              ]
                                          }
                                        ]
          }
*/ 


    const requestBody = req.body;
    const requestHeaders = req.headers;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F03",session.data.created_by,session.data.timestamp_);
    if(validateTransactionLock.status==false){
      console.error("Transaction locked")
      console.warn("Someone is trying to make forbiden concurrent transaction");
      res.status(401).send("Concurrent Transaction Request");
      return;
    }

    const transferRequest = await walletService
    .transferWallet(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
      return response;
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
      return ;
    });

    const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);

    if(!transferRequest|| !transferRequest.status ||  transferRequest.status!=200){ //if failed dont send notification yet
      return; 
    }
    

    const notificationBody= {
      // notification: {
      //                 title: "Transfer Successful",
      //                 body: "You have sucessfully transfered "+transferRequest.data.amount+ " of gold to " +transferRequest.data.message
      //                 },
      notification: notificationMessage.getMessage("NT031",transferRequest.data.amount,transferRequest.data.message),
      data : {
                  categoryCode:"NC01",
                  typeCode:"NT031",
                  dataUrl:"",
                  dataReference : transferRequest.data.reference
                }
    }

    const notification= await notificationService.saveAndPushnotification(requestHeaders,notificationBody)
      .then(response=>{
        return response;
      })
      .catch((error)=>{
        return error
      })

      const emailNotificationSender = await emailNotificationService.goldTransferEmail(transferRequest.data.reference);
      const emailNotificationReceiver = await emailNotificationService.goldReceiveEmail(transferRequest.data.reference);

};
const transferWalletV2 = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                          "userName":"",
                                          "userPin":"",
                                          "referenceId":""
                                        }
            }
  */ 
  
  
      const requestBody = req.body;
      const requestHeaders = req.headers;
  
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }

      const verifyPin = await onboardingService
        .verifyPin(requestBody,requestHeaders)
        .then((response) => {
          console.log("**********Response : ", response.data);
          return response;
        })
        .catch((error) => {
          console.error("******* Error : ", error.response);
          return error.response;
        });


      if(!verifyPin || verifyPin.status!=200){
        res.status(401).send("Pin Invalid");
        return ;
      }
  
      const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F03",session.data.created_by,session.data.timestamp_);
      if(validateTransactionLock.status==false){
        console.error("Transaction locked")
        console.warn("Someone is trying to make forbiden concurrent transaction");
        res.status(401).send("Concurrent Transaction Request");
        return;
      }
  
      const transferRequest = await walletService
      .transferWallet(requestBody,requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response;
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
        return ;
      });
  
      const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
  
      if(!transferRequest|| !transferRequest.status ||  transferRequest.status!=200){ //if failed dont send notification yet
        return; 
      }
      
  
      const notificationBody= {
        // notification: {
        //                 title: "Transfer Successful",
        //                 body: "You have sucessfully transfered "+transferRequest.data.amount+ " of gold to " +transferRequest.data.message
        //                 },
        notification: notificationMessage.getMessage("NT031",transferRequest.data.amount,transferRequest.data.message),
        data : {
                    categoryCode:"NC01",
                    typeCode:"NT031",
                    dataUrl:"",
                    dataReference : transferRequest.data.reference
                  }
      }
  
      const notification= await notificationService.saveAndPushnotification(requestHeaders,notificationBody)
        .then(response=>{
          return response;
        })
        .catch((error)=>{
          return error
        })
  
        const emailNotificationSender = await emailNotificationService.goldTransferEmail(transferRequest.data.reference);
        const emailNotificationReceiver = await emailNotificationService.goldReceiveEmail(transferRequest.data.reference);
  
  };

const getTransferList = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: {
                                        "statusCode": "G05"
                                      }
          }
*/ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    await walletService
    .getTransferList(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });

};

const reedemBGD = async(req, res, next) =>{ // used for doing redeemption
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: [
                                            {
                                                "acctNumber": "",
                                                "unit": 2,
                                                "weight": 8.5,
                                                "name": "najib",
                                                "phone": "01123456789",
                                                "address": "Jalan 14",
                                                "address2": "kg hulu bendul",
                                                "postcode": "54200",
                                                "city": "Setiawangsa",
                                                "state": "Kuala Lumpur",
                                                "remarks": "test",
                                                "reference": "",
                                                "reason":"personal",
                                                "serialNumber": "",
                                                "feesForms": [
                                                    {
                                                        "feeType": "Courier Charge",
                                                        "feeTypeCode": "B03",
                                                        "setupFees": 7.0000,
                                                        "setupUom": "RM",
                                                        "setupUomCode": null,
                                                        "chargeFees": 7.0000,
                                                        "chargeUom": "RM",
                                                        "chargeUomCode": "J01"
                                                    },
                                                    {
                                                        "feeType": "Takaful Charge",
                                                        "feeTypeCode": "B04",
                                                        "setupFees": 5.5000,
                                                        "setupUom": "RM",
                                                        "setupUomCode": null,
                                                        "chargeFees": 5.5000,
                                                        "chargeUom": "RM",
                                                        "chargeUomCode": "J01"
                                                    },
                                                    {
                                                        "feeType": "Minting Charge",
                                                        "feeTypeCode": "B05",
                                                        "setupFees": 5.0000,
                                                        "setupUom": "RM",
                                                        "setupUomCode": null,
                                                        "chargeFees": 5.0000,
                                                        "chargeUom": "RM",
                                                        "chargeUomCode": "J01"
                                                    }
                                                ]
                                            }
                                        ]
          }
*/ 
    const requestBody = req.body;
    const requestHeaders = req.headers;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F04",session.data.created_by,session.data.timestamp_);
    if(validateTransactionLock.status==false){
      console.error("Transaction locked")
      console.warn("Someone is trying to make forbiden concurrent transaction");
      res.status(401).send("Concurrent Transaction Request");
      return;
    }

    const redeemRequest = await walletService
      .redeemBGD(requestBody,requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response;
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(500).send(error.response.data);
        return ;
      });

    const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
    

    if(!redeemRequest|| !redeemRequest.status ||  redeemRequest.status!=200){ //if failed dont send notification yet
      return; 
    }

    const notificationBody= {
      // notification: {
      //                 title: "Redemption Request Sucessful",
      //                 body: "Your redemption is being processed"
      //                 },
      notification:notificationMessage.getMessage("NT041",Math.round(redeemRequest.data.amount/4.25),redeemRequest.data.reference),
      data : {
                  categoryCode:"NC01",
                  typeCode:"NT041",
                  dataUrl:"",
                  dataReference : redeemRequest.data.reference
                }
    }
    
    const notification= await notificationService.saveAndPushnotification(requestHeaders,notificationBody)
      .then(response=>{
        return response;
      })
      .catch((error)=>{
        return error
      })

      const emailNotification = await emailNotificationService.goldRedeemEmail(redeemRequest.data.reference);


};

const reedemBGDV2 = async(req, res, next) =>{ // used for doing redeemption
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                          "userName":"",
                                          "userPin":"",
                                          "referenceId":""
                                }
                                              
                                          
            }
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
  
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }

      const verifyPin = await onboardingService
        .verifyPin(requestBody,requestHeaders)
        .then((response) => {
          console.log("**********Response : ", response.data);
          return response;
        })
        .catch((error) => {
          console.error("******* Error : ", error.response);
          return error.response;
        });


      if(!verifyPin || verifyPin.status!=200){
        res.status(401).send("Pin Invalid");
        return ;
      }
  
      const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F04",session.data.created_by,session.data.timestamp_);
      if(validateTransactionLock.status==false){
        console.error("Transaction locked")
        console.warn("Someone is trying to make forbiden concurrent transaction");
        res.status(401).send("Concurrent Transaction Request");
        return;
      }

      const getIdDetailsRequest= await onboardingService
                            .getUserDetails(requestBody, requestHeaders)
                            .then((response) => {
                              // console.log("************ Response : ", response.data);
                              return response.data
                            })
                            .catch((error) => {
                              console.error("********** Error : ", error);
                              // res.status(error.response.status).send(error.response.data);
                            });
    
    if(!getIdDetailsRequest){
      const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
      res.status(500).send("Internal Error: Cannot getUserDetails")
      return;
    }
    // console.log("USERDETAILS:",getIdDetailsRequest)
    // console.log("ID TYPE: ",getIdDetailsRequest.phoneNumber)
    // console.log("Full Name: ",getIdDetailsRequest.fullName)
    
    //COMMENT TEMP FOR PROD
    
    console.log("getUserDetails: ",getIdDetailsRequest)
    requestBody.investorPhoneNumber = getIdDetailsRequest.phoneNumber;
    requestBody.investorFullName = getIdDetailsRequest.fullName;

    // delete requestBody.userName;
    // delete requestBody.userPin;

    console.log("RequestBody: ", requestBody)
  
      const redeemRequest = await walletService
        .redeemBGD(requestBody,requestHeaders)
        .then((response) => {
          console.log("*********** Response : ", response);
          res.status(200).send(response);
          return response;
        })
        .catch((error) => {
          console.error("*********** Error : ", error);
          res.status(error.response).send(error.response);
          return ;
        });
  
      const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
      
  
      if(!redeemRequest || redeemRequest.status!='G01'){ //if failed dont send notification yet
        return; 
      }
  
      const notificationBody= {
        // notification: {
        //                 title: "Redemption Request Sucessful",
        //                 body: "Your redemption is being processed"
        //                 },
        notification:notificationMessage.getMessage("NT041",Math.round(redeemRequest.amount/4.25),redeemRequest.reference),
        data : {
                    categoryCode:"NC01",
                    typeCode:"NT041",
                    dataUrl:"",
                    dataReference : redeemRequest.reference
                  }
      }
      
      const notification= await notificationService.saveAndPushnotification(requestHeaders,notificationBody)
        .then(response=>{
          return response;
        })
        .catch((error)=>{
          return error
        })
  
        const emailNotification = await emailNotificationService.goldRedeemEmail(redeemRequest.reference);
  
  
};

const logisticStatus = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['redemption_id'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await walletService
    .logisticStatus(requestparams, requestBody, requestHeaders)
    .then((response) => {
      console.log("*********** Response logisticStatus: ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error logisticStatus: ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
};

const redemptionTracking = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['referenceId'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await walletService
    .redemptionTracking(requestparams, requestBody, requestHeaders)
    .then((response) => {
      console.log("*********** Response redemptionTracking: ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error redemptionTracking: ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
};

const getTransactionList = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
    const requestBody = req.body;
    const requestHeaders = req.headers;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    await walletService
    .getTransactionList(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });

};

const queryBuyPrice = async (req, res, next) => {
  /*
  #swagger.security = [{
    "bearerAuth": []
  }] 
  
  #swagger.parameters['margin'] = {
                                in: 'query',
                                required: true,
                                schema: 0.02
            }
  #swagger.parameters['uomCode'] = {
                          in: 'query',
                          required: true,
                          schema: 'J04'
  }
*/
  
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  console.log("request params -> ", requestparams);

  let margin = {
    "setupFees": requestparams.margin,
    "setupUomCode": requestparams.uomCode
  }

  console.log("margin -> ", margin);

  await walletService
    .getQueryBuyPrice(margin, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
      return response.data;
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(401).send(error);
      return error;
    });
};

const querySellPrice = async (req, res, next) => {
  /*
  #swagger.security = [{
    "bearerAuth": []
  }] 
  
  #swagger.parameters['margin'] = {
                                in: 'query',
                                required: true,
                                schema: 0.02
            }
  #swagger.parameters['uomCode'] = {
                          in: 'query',
                          required: true,
                          schema: 'J04'
  }
*/
  
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  console.log("request params -> ", requestparams);

  let margin = {
    "setupFees": requestparams.margin,
    "setupUomCode": requestparams.uomCode
  }

  console.log("margin -> ", margin);
  
  await walletService
    .getQuerySellPrice(margin, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
      return response.data;
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(401).send(error);
      return error;
    });
};

const spotOrderBuy = async (req, res, next) => {
  /*
  #swagger.security = [{
    "bearerAuth": []
  }] 
  
  #swagger.parameters['priceRequestId'] = {
                                in: 'query',
                                required: true,
                                schema: 'PVBT0000056FC8'
            }
  #swagger.parameters['price'] = {
                          in: 'query',
                          required: true,
                          schema: 285.32
  }
  #swagger.parameters['weight'] = {
                          in: 'query',
                          required: true,
                          schema: 4.25
  }
  #swagger.parameters['reference'] = {
                          in: 'query',
                          required: true,
                          schema: 'F05/20230318000001'
  }
*/
  
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  console.log("request params -> ", requestparams);

  let params = {
    "priceRequestId": requestparams.priceRequestId,
    "price": requestparams.price,
    "weight": requestparams.weight,
    "reference": requestparams.reference
  }

  console.log("params -> ", params);
  
  await walletService
    .spotOrderBuy(params, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
      return response.data;
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(401).send(error);
      return error;
    });
};

const spotOrderSell = async (req, res, next) => {
  /*
  #swagger.security = [{
    "bearerAuth": []
  }] 
  
  #swagger.parameters['priceRequestId'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
  #swagger.parameters['price'] = {
                          in: 'query',
                          required: true,
                          schema: 285.32
  }
  #swagger.parameters['weight'] = {
                          in: 'query',
                          required: true,
                          schema: 4.25
  }
  #swagger.parameters['reference'] = {
                          in: 'query',
                          required: true,
                          schema: ''
  }
*/
  
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  console.log("request params -> ", requestparams);

  let params = {
    "priceRequestId": requestparams.priceRequestId,
    "price": requestparams.price,
    "weight": requestparams.weight,
    "reference": requestparams.reference
  }

  console.log("params -> ", params);
  
  await walletService
    .spotOrderSell(params, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
      return response.data;
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(401).send(error);
      return error;
    });
};

const getProfitLoss = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['current_price'] = {
                                in: 'query',
                                required: true,
                                schema: '200.30'
            }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await walletService
    .getProfitLoss(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
};

const ambankToken = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      await walletService
      .ambankToken(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  
};   

const ambankAccountEnquiryOld = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                              "creditorAccountType": "DFLT",
                                              "receiverBIC": "HLBBMYKL",
                                              "creditorAccountNo": "",
                                              "secondValidationIndicator": "N",
                                              "idType": "01",
                                              "idNo": "981108545643",
                                              "acquaintmeStatus":"verification.accepted",
                                              "acquaintmeReference":"",
                                              "fullName":"NUR"
                                          }         
            }
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }

      var acquaintmeStatus = requestBody.acquaintmeStatus;
      var acquaintmeReference= requestBody.acquaintmeReference;
      var fullName = requestBody.fullName;

      delete requestBody.acquaintmeStatus;
      delete requestBody.acquaintmeReference;
      delete requestBody.fullName;
  
      var ambankResponse = await walletService
        .ambankAccountEnquiry(requestBody,requestHeaders,requestparams)
        .then((response) => {
          console.log("*********** Response : ", response.data);
          // res.status(200).send(response.data);
          return response
        })
      .catch((error) => {
        console.error("********** Error : ", error);
        // res.status(error.response.status).send(error.response.data);
        return error.response
      });

      console.log("AMBANK API STATUS: " + ambankResponse.status)
      console.log("AMBANK API RESULT : " ,ambankResponse.data)

      var updateTierReq = {
        "reference": acquaintmeReference,
        "tierCode":"T01",
        "kycStatusCode":""
      };

      if(ambankResponse.data && ambankResponse.status==200 ){
        console.log("AMBANK ACCOUNT ENQUIRY SUCCESSFULLY CALLED")

        if(acquaintmeStatus=="verification.accepted"){
        
              updateTierReq.kycStatusCode="G09";
              console.log('UPDATE TIER REQ BODY: ', updateTierReq)
              try{
                    var updateTier = await onboardingService
                      .updateTier(updateTierReq,requestHeaders)
                      .then((response) => {
                        console.log("#############  Response 2 : Update Tier : ", response);
                        return 
                      })
                      .catch((error) => {
                        console.error("###########  ERROR : Update Tier", error);
                        // res.status(error.response.status).send(error.response.data);
                        throw error;
                      });

                    var successResponse = {response : "SUCCESS",
                                          remark: "Successful ",
                                          message:"OCR and account enquiry passed"}
                    res.status(200).send(successResponse);
                    

                } catch(error){
                        body = await error.json();
                        stat = await error.status;
                        console.error("###########  ERROR : Create Wallet", body);
                        console.error("###########  STATUS : Create Wallet", stat);
                        res.status(stat).send({
                          response : "FAIL",
                          remark : "Succceed but Error during update tier",
                          message : body
                        });
                    }
          } else{
                  updateTierReq.kycStatusCode="G12";
                  console.log('UPDATE TIER REQ BODY: ', updateTierReq)

                  var updateTier = await onboardingService
                      .updateTier(updateTierReq,requestHeaders)
                      .then((response) => {
                        console.log("#############  Response 2 : Update Tier : ", response);
                        return 
                      })
                      .catch((error) => {
                        console.error("###########  ERROR : Update Tier", error);
                        // res.status(error.response.status).send(error.response.data);
                        throw error;
                      });

                  res.status(409).send({response : "PENDING",
                                        remark : "Pending admin validation",
                                        message:"OCR not succeed"});
            } 

      } else{

        updateTierReq.kycStatusCode="G10";
        console.log('UPDATE TIER REQ BODY: ', updateTierReq)
        var errorApiStatus=ambankResponse.status
        
        var updateTier = await onboardingService
          .updateTier(updateTierReq,requestHeaders)
          .then((response) => {
            console.log("#############  Response 2 : Update Tier : ", response);
            return 
          })
          .catch((error) => {
            console.error("###########  ERROR : Update Tier", error);
            // res.status(error.response.status).send(error.response.data);
            errorApiStatus=error.response.status;
            throw error;
          });
          
        console.log("NOT SUCCESS with message:" , ambankResponse.data)
        
        res.status(errorApiStatus).send({response : "FAIL",
                                          remark : "Failed",
                                          message:"Account invalid / inactive"});
    
      }

      var createWalletReq = [
        {
          "uomCode": "J01",
          "amount": "0.00",
          "walletTypeCode": "E01",
          "regTypeCode": "D02"
        },
        {
          "uomCode": "J02",
          "amount": "0.00",
          "walletTypeCode": "E02",
          "regTypeCode": "D02"
        }
      ];
      ////////////////// CREATE WALLET ////////////////
      var createWallet = await walletService
        .createWallet(createWalletReq,requestHeaders,'tier1KycChecking')
        .then((response) => {
            console.log("#############  Response 3 : CreateWallet : ", response);
            return response;
        })
        .catch((error) => {
          console.error("###########  ERROR : Create Wallet", error);
          // res.status(error.response.status).send(error.response.data);
        });
  
}; 

const ambankAccountEnquiry = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                    "bearerAuth": []
            }] 
        #swagger.parameters['body'] = {
                                  in: 'body',
                                  required: true,
                                  schema: {
                                                "creditorAccountType": "DFLT",
                                                "receiverBIC": "",
                                                "creditorAccountNo": "",
                                                "secondValidationIndicator": "N",
                                                "idType": "",
                                                "idNo": "",
                                                "acquaintmeStatus":"verification.accepted",
                                                "acquaintmeReference":"",
                                                "fullName":"",
                                                "identificationTypeCode":"P01",
                                                "identificationNumber":"",
                                                "userID":"uuid"
                                            }         
              }
    */ 
        const requestBody = req.body;
        const requestHeaders = req.headers;
        const requestparams= req.query;

        const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
        if(session.status==false){
          console.error("Session Expired");
          res.status(401).send("Session Expired");
          return;
        }
  
        var identificationNumber = requestBody.identificationNumber;
        var identificationTypeCode = requestBody.identificationTypeCode;
        
        var acquaintmeStatus = requestBody.acquaintmeStatus;
        var acquaintmeReference= requestBody.acquaintmeReference;
        var userID = requestBody.userID;

        //////////////////////// AMBANK ACCOUNT ENQUIRY  /////////////////////
      
        delete requestBody.acquaintmeStatus;
        delete requestBody.acquaintmeReference;
        delete requestBody.userID;

        delete requestBody.identificationNumber;
        delete requestBody.identificationTypeCode;
        
        var ambankVerificationStatus = await onboardingController.ambankEnquiryFunction(req);


        //////////////////////// KYC LOGIC /////////////////////
        console.log("OCR Verification STATUS: ",acquaintmeStatus); // either verification.accepted ,verification.declined,verification.pending
        console.log("AMBANK Verification STATUS: ",ambankVerificationStatus); // either verification.accepted ,verification.declined,verification.pending ,verification.error

        var updateTierReq = {
          "identificationTypeCode":identificationTypeCode,
          "identificationNumber":identificationNumber,
          "reference": acquaintmeReference,
          "tierCode":"T01",
          "kycStatusCode":"",
          "userID":userID
        };

        var messageResponse ={};
        var statusCode = 400;
        var updateTier = false;
        var createBank = false;
        var createWallet = false;
        

        if(ambankVerificationStatus == 'verification.error' || acquaintmeStatus == 'verification.error' ){
                updateTier = false;
                updateTierReq.kycStatusCode="G10";
                statusCode = 500;
                createBank=false;
                createWallet = false;
                if(acquaintmeStatus == 'verification.error' && ambankVerificationStatus== 'verification.error'){
                      messageResponse = { response : "ERROR",
                                          remark : "Verification has server error for both ocr and bank. Expected cause: Bank verification api not responding/invalid request format. OCR api not reponding.",
                                          message:"OCR_AND_BANK"}
                }
                else if(acquaintmeStatus == 'verification.error'){
                      messageResponse = { response : "ERROR",
                                          remark : "Verification has server error for OCR. Expected cause: OCR api not responding",
                                          message:"OCR"}
                }
                else if(ambankVerificationStatus == 'verification.error'){
                      messageResponse = { response : "ERROR",
                                          remark : "Verification has server error for bank. Expected cause: Bank verification api not responding/invalid request format",
                                          message:"BANK"}
                }
        }
        else if(acquaintmeStatus == 'verification.declined' || ambankVerificationStatus== 'verification.declined'){
        //either failed will failed
              updateTier = true;
              updateTierReq.kycStatusCode="G10";
              statusCode = 409;
              createBank=false;
              createWallet = false;
              if(acquaintmeStatus == 'verification.declined' && ambankVerificationStatus== 'verification.declined'){
                    messageResponse = { response : "FAIL",
                                        remark : "Verification has been declined for both ocr and bank. Expected cause: Face mismatch or IC image not properly captured. Account bank given not active/invalid",
                                        message:"OCR_AND_BANK"}
              }
              else if(acquaintmeStatus == 'verification.declined'){
                    messageResponse = { response : "FAIL",
                                        remark : "Verification has been declined for OCR. Expected cause: Face mismatch or IC image not properly captured",
                                        message:"OCR"}
              }
              else if(ambankVerificationStatus == 'verification.declined'){
                    messageResponse = { response : "FAIL",
                                        remark : "Verification has been declined for bank. Expected cause: Account bank given not active/invalid",
                                        message:"BANK"}
              }
        }
        else if(acquaintmeStatus == 'verification.pending' || ambankVerificationStatus== 'verification.pending' ){
        // either pending will pending 
              updateTier = true;
              updateTierReq.kycStatusCode="G12";
              statusCode = 409;
              createBank=true;
              createWallet = true;
              if(acquaintmeStatus == 'verification.pending' && ambankVerificationStatus== 'verification.pending' ){
                      messageResponse = { response : "PENDING",
                                          remark : "Verification has been declined for both OCR and bank. Expected cause: OCR mismatch or full name given not valid",
                                          message:"OCR_AND_BANK"}
              }
              else if(acquaintmeStatus == 'verification.pending'){
                      messageResponse = { response : "PENDING",
                                          remark : "Verification has been declined for OCR. Expected cause: Facial mismatch",
                                          message:"OCR"}
              }
              else if( ambankVerificationStatus== 'verification.pending' ){
                      messageResponse = { response : "PENDING",
                                          remark : "Verification has been declined for bank. Expected cause: Full name given not valid",
                                          message:"BANK"}
              }

        }
        else if(acquaintmeStatus =='verification.accepted' || ambankVerificationStatus == 'verification.accepted'){
        //both successs will success
          updateTier = true;
          updateTierReq.kycStatusCode="G09";
          statusCode = 200;
          createBank=true;
          createWallet = true;
          messageResponse = { response : "SUCCESS",
                              remark: "Successful ",
                              message:"OCR and account enquiry passed"}
        }
        else{
            updateTier = false;
            createBank=false;
            createWallet = false;
            //something WRONG with either verification status response
            res.status(400).send({ response : "FAIL",
                                    remark : "Failed cause by system error",
                                    message:"Either OCR or bank verification status had a system problem"});
            return
        }

        //////////////////////// UPDATE TIER /////////////////////

        console.log("KYC LOGIC RESPONSE" ,messageResponse);
        
        if(updateTier==true){
            console.log('Calling api updating TIER ..')
            console.log('UPDATE TIER REQ BODY: ', updateTierReq)
            
            await onboardingService
              .updateTier(updateTierReq,requestHeaders)
              .then((response) => {
                // console.log('#CONTROLLER: Response 1: Update Tier: Status: ',response)
                console.log("#CONTROLLER: Response 1: Update Tier: ", response)
                res.status(statusCode).send(messageResponse);
                return 
              })
              .catch((error) => {
                // console.error("#CONTROLLER:  ERROR : Update Tier: Status: ", error.response);
                console.error("#CONTROLLER:  ERROR : Update Tier: ", error);
                createBank=false;
                createWallet = false;
                res.status(error.response.status).send(error.response);
                return error;
              });
        }
        else{
          res.status(statusCode).send(messageResponse);
        }

        
      if(createBank==true){
    
            var createBankReqBody = req.body;
            delete createBankReqBody.creditorAccountType;
            delete createBankReqBody.secondValidationIndicator;
            delete createBankReqBody.idType;
            delete createBankReqBody.idNo;
            
            createBankReqBody.accountNumber=createBankReqBody.creditorAccountNo
            createBankReqBody.bicCode=createBankReqBody.receiverBIC

            delete createBankReqBody.creditorAccountNo
            delete createBankReqBody.receiverBIC

            var accountNumber = createBankReqBody.accountNumber
            
            console.log('CALLING API delete bank account if exist... ')
            var deleteBankReq=createBankReqBody;
            delete deleteBankReq.accountNumber;
            console.log("DELETE Bank Req Body : " , deleteBankReq);
            await onboardingService
              .deleteBankDetails(deleteBankReq,requestHeaders)
              .then((response) => {
                console.log("#CONTROLLER: Response 2: deleteBank: status", response.status)
                console.log("#CONTROLLER: Response 2: deleteBank:", response.data)
              })
              .catch((error) => {
                console.error("#CONTROLLER: Error : deleteBank: status: ", error.response.status);
                console.error("#CONTROLLER: Error : deleteBank:", error.response.data);
              });

            createBankReqBody.accountNumber = accountNumber;
            console.log("Create Bank Req Body : " , createBankReqBody);
            console.log('CALLING API creating bank account ... ')

            
            var status = await onboardingService
            .createBankDetails(createBankReqBody,requestHeaders)
            .then((response) => {
              console.log("#CONTROLLER: Response 3: createBankDetail: status: ", response.status);
              console.log("#CONTROLLER: Response 3: createBankDetail: ", response.data);
              // res.status(200).send(response.data);
            })
            .catch((error) => {
              console.error("#CONTROLLER: Error : createBankDetail: status: ", error.response.status);
              console.error("#CONTROLLER: Error : createBankDetail: ", error.response);
              // res.status(error.response.status).send(error.response.data);  
            });
      }

      if(createWallet == true){
            console.log('CALLING API creating wallet ... ')
                    ////////////////// CREATE WALLET (WILL RUN IN BACKGROUND) ////////////////
                    var createWalletReq = [
                      {
                        "uomCode": "J01",
                        "amount": "0.00",
                        "walletTypeCode": "E01",
                        "regTypeCode": "D02"
                      },
                      {
                        "uomCode": "J02",
                        "amount": "0.00",
                        "walletTypeCode": "E02",
                        "regTypeCode": "D02"
                      }
                    ];
                    
                    var createWallet = await walletService
                      .createWallet(createWalletReq,requestHeaders,'tier1KycChecking')
                      .then((response) => {
                          console.log("#CONTROLLER:  Response 4 : CreateWallet : status: ", response.status);
                          console.log("#CONTROLLER:  Response 4 : CreateWallet : ", response);
                          
                          return response;
                      })
                      .catch((error) => {
                        console.error("#CONTROLLER:  ERROR : Create Wallet", error);
                        console.error("#CONTROLLER:  ERROR : Create Wallet: status: ", error.response.status);
                        // res.status(error.response.status).send(error.response.data);
                      });
              }   

}; 

const ambankCreditTransfer = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                            "creditorAccountType": "DFLT",
                                            "receiverBIC": "",
                                            "creditorAccountNo": "",
                                            "creditorAccountName": "",
                                            "amount": "2.00",
                                            "paymentReference":"",
                                            "paymentDescription":"",
                                            "lookUpReference": ""
                                        }
            }
    
            
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      var ambankResult = await walletService
      .ambankCreditTransfer(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response.data;
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
        
      });

      console.log("AMBANK API RESULT : " + ambankResult)

      if(ambankResult){
        console.log("SUCCESS")
      }
      else{
        console.log("NOT SUCCESS")
        return;
      }
  
}; 

const ambankGetCTStatus = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      
            #swagger.parameters['cTsrcRefNo'] = {
                                in: 'query',
                                required: true,
                                schema: '12345'
            }

  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      await walletService
      .ambankGetCTStatus(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  
}; 

const testnetwork = async(req, res, next) => {
  /*          
    #swagger
    }

*/ 

    // requestBody = {};
    // requestHeaders = {};
    // requestparams= {};

    await walletService
    .testnetwork()
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });


}

const aceRedeem = async(req, res, next) =>{ //use to direct redeem
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['redeemGram'] = {
                                in: 'query',
                                required: true,
                                schema: '2'
            }
      #swagger.parameters['reference'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['itemSerialNo'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['itemDenomination'] = {
                                in: 'query',
                                required: true,
                                schema: '1'
            }
      #swagger.parameters['itemQuantity'] = {
                                in: 'query',
                                required: true,
                                schema: '1'
            }
      #swagger.parameters['deliveryName1'] = {
                                in: 'query',
                                required: true,
                                schema: 'Albab'
            }
      #swagger.parameters['deliveryName2'] = {
                                in: 'query',
                                required: true,
                                schema: 'Ahmad'
            }
      #swagger.parameters['deliveryContact1'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['deliveryContact2'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['deliveryAddress1'] = {
                                in: 'query',
                                required: true,
                                schema: 'Jalan 2'
            }
      #swagger.parameters['deliveryAddress2'] = {
                                in: 'query',
                                required: true,
                                schema: 'Kampung Dalam PAyah'
            }
      #swagger.parameters['deliveryAddress3'] = {
                                in: 'query',
                                required: true,
                                schema: 'Shah Alam'
            }
      #swagger.parameters['deliveryAddress4'] = {
                                in: 'query',
                                required: true,
                                schema: 'Selangor'
            }
      #swagger.parameters['deliveryState'] = {
                                in: 'query',
                                required: true,
                                schema: 'Selangor'
            }
      #swagger.parameters['deliveryPostcode'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    const validateTransactionLock = await sessionController.validateTransactionLock(session.data.id,"F04",session.data.created_by,session.data.timestamp_);
    if(validateTransactionLock.status==false){
      console.error("Transaction locked")
      console.warn("Someone is trying to make forbiden concurrent transaction");
      res.status(401).send("Concurrent Transaction Request");
      return;
    }

    const redeemRequest = await walletService
      .aceRedeem(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return 'SUCCESS';
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
        return 'FAIL';
      });

    if(!redeemRequest){
      return;
    }
    else{
      const releaseLock = await sessionController.releaseTransactionLock(validateTransactionLock.data.id,session.data.created_by);
    }
  
};

const transactionHistories = async(req, res, next) => {
 /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['days'] = {
                                in: 'query',
                                required: true,
                                schema: '7'
            }
      #swagger.parameters['transactionCode'] = {
                              in: 'query',
                              required: true,
                              schema: 'Topup'
      }
  */ 
    const requestHeaders = req.headers;
    const requestQuery = req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    await walletService
    .transactionHistories(requestHeaders,requestQuery)
    .then((response) => {
      const objResponse = JSON.parse(response);
      res.status(200).send(objResponse);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });

}

const cashWalletHistories = async(req, res, next) => {
  /*  
       #swagger.security = [{
                  "bearerAuth": []
           }] 
       #swagger.parameters['days'] = {
                                 in: 'query',
                                 required: true,
                                 schema: '7'
             }
   */ 
             const requestBody = req.body;
             const requestHeaders = req.headers;
             const requestparams= req.query;
 
     const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
     if(session.status==false){
       console.error("Session Expired");
       res.status(401).send("Session Expired");
       return;
     }
 
     await walletService
     .cashWalletHistories(requestBody,requestHeaders,requestparams)
     .then((response) => {
       res.status(response.status).send(response.data);
     })
     .catch((error) => {
       console.error("********** Error : ", error);
       res.status(error.response.status).send(error.response.data);
     });
 
}

//////////////////////////PAYNETTT ////////////////////////////

const returnUrl = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                            "success": true,
                                            "reference": "",
                                            "date": "",
                                            "message": ""
                                        }
                                      
        }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      await walletService
      .returnUrl(requestBody,requestHeaders)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(400).send(error.data);
          });
};

const getReturnUrl = async(req, res, next) =>{
  /*  
      
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;
  
      await walletService
      .getReturnUrl(requestBody,requestHeaders)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(400).send(error.data);
          });
};

const paynetToken = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
  */ 

  
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      await walletService
      .paynetToken(requestBody,requestHeaders,requestparams)
          .then((response) => {
            console.log("#############  Response : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : ", error);
            res.status(error.response.status).send(error.response.data);
          });
};

const paynetBankList = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['channelCode'] = {
                                in: 'query',
                                required: true,
                                schema: 'BW'
            }
      #swagger.parameters['transactionCode'] = {
                                in: 'query',
                                required: true,
                                schema: 'BW'
            }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      await walletService
      .paynetBankList(requestBody,requestHeaders,requestparams)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(error.response.status).send(error.response.data);
          });
};

const paynetInitiatePayment = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                        "channelCode":"BW",
                                        "bicCode":"ACFBMYK1",
                                        "amount":20.00,
                                        "currency":"MYR",
                                        "name":"",
                                        "recipientReference":"",
                                        "paymentDescription":"",
                                        "identificationTypeCode":"P01",
                                        "identificationNumber":""
                                    }
                                  }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;

      
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
     
  
      await walletService
      .paynetInitiatePayment(requestBody,requestHeaders)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(error.response.status).send(error.response.data);
          });
};

const paynetCancelPayment = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                            "channelCode":"BW",
                                            "bicCode":"",
                                            "endToEndId":""
                                        }
                                      }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;

      
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
     
  
      await walletService
      .paynetCancelPayment(requestBody,requestHeaders)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(error.response.status).send(error.response.data);
          });
};

const topupStatus = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['endToEndId'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['endtoEndIdSignature'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    
    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    console.log("Request Params : ", requestparams);
  
    await walletService
    .topupStatus(requestparams, requestBody, requestHeaders)
    .then(async(response) => {
      console.log("*********** Response Reference No : ", response.data.reference);     
      
      res.status(200).send(response.data);
      const emailNotification = await emailNotificationService.cashTopupEmail(response.data.reference);
      console.log("Email Notification Status : ", emailNotification);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
};

const topupStatusChecking = async(req, res, next) =>{
  /*  

  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;


    console.log("Request Params : ", requestparams);
  
    await walletService
    .topupStatusChecking(requestparams, requestBody, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
};

const transactionDetails = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                    "bearerAuth": []
            }] 
        #swagger.parameters['referenceId'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
        #swagger.parameters['transactionTypeCode'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }

      console.log("Request Params : ", requestparams);
      
      await walletService
      .transactionDetails(requestparams, requestBody, requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
    
};
    
const downloadReceipt = async(req, res, next) =>{
      /*  
          #swagger.security = [{
                      "bearerAuth": []
              }] 
          #swagger.parameters['referenceId'] = {
                                    in: 'query',
                                    required: true,
                                    schema: ''
                }
          #swagger.parameters['transactionTypeCode'] = {
                                    in: 'query',
                                    required: true,
                                    schema: ''
                }
      */ 
        const requestBody = req.body;
        const requestHeaders = req.headers;
        const requestparams= req.query;

        
        const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
        if(session.status==false){
          console.error("Session Expired");
          res.status(401).send("Session Expired");
          return;
        }
        
        await walletService
        .downloadReceipt(requestparams, requestBody, requestHeaders)
        .then((response) => {
          console.log("*********** Response : ", response.data);
          const newBlob = new Blob([response.data], {type: 'application/pdf'});
          res.type(newBlob.type)
          newBlob.arrayBuffer().then((buf) => {
              res.send(Buffer.from(buf))
          })
        })
        .catch((error) => {
          console.error("*********** Error : ", error);
          res.status(error.response.status).send(error.response.data);
        });
};


module.exports = {
    getWalletDetails,
    createWallet,
    topupwallet,
    withdrawWalletV2,
    transferWallet,
    getTransferList,
    reedemBGD,
    redemptionTracking,
    getTransactionList,
    validateWallet,
    returnUrl,
    getReturnUrl,
    queryBuyPrice,
    querySellPrice,
    spotOrderBuy,
    spotOrderSell,
    aceRedeem,
    logisticStatus,


    getProfitLoss,

    ambankToken,
    ambankAccountEnquiry,
    ambankCreditTransfer,
    ambankGetCTStatus,

    testnetwork,
    transactionHistories,

    paynetToken,
    paynetBankList,
    paynetInitiatePayment,
    paynetCancelPayment,
    topupStatus,
    topupStatusChecking,
    transactionDetails,
    downloadReceipt,

    confirmPurchase,
    cashWalletHistories,

    topupwalletV2,
    transferWalletV2,
    reedemBGDV2
    
  };