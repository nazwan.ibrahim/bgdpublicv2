const connectionService = require("../services/connection");
const walletService = require("../services/wallet");
const sessionController = require("../controllers/session");
const { jwtDecode } =require("jwt-decode");
const { options } = require("../routes/wallet");



const testnetwork = async(req, res, next) => {
    /*          
      #swagger
      }

  */ 
      // requestBody = {};
      // requestHeaders = {};
      // requestparams= {};


      
      await connectionService
      .testnetwork()
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send({
          "ip_adrress": response.data,
          "onboarding": process.env.onBoardingUrl,
          "wallet": process.env.walletUrl,
          "trading": process.env.tradingUrl,
          "admin": process.env.administrationUrl,
          "aquaintme": process.env.rekognitionUrl
        });
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  };

const ambankAccountEnquiry = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                              "creditorAccountType": "DFLT",
                                              "receiverBIC": "HLBBMYKL",
                                              "creditorAccountNo": "",
                                              "secondValidationIndicator": "N",
                                              "idType": "01",
                                              "idNo": ""
                                          }         
            }
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      var ambankResponse = await walletService
        .ambankAccountEnquiry(requestBody,requestHeaders,requestparams)
        .then((response) => {
          console.log("*********** Response : ", response.data);
          res.status(200).send(response.data);
          return response
        })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
        return error.response
      });
  
  }; 

const ambankCreditTransfer = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                            "creditorAccountType": "DFLT",
                                            "receiverBIC": "HLBBMYKL",
                                            "creditorAccountNo": "",
                                            "creditorAccountName": "",
                                            "amount": "2.00",
                                            "paymentReference":"",
                                            "paymentDescription":"",
                                            "lookUpReference": ""
                                            
                                        }
            }
    
            
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      var ambankResult = await walletService
      .ambankCreditTransfer(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response.data;
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
        
      });

      console.log("AMBANK API RESULT : " + ambankResult)

      if(ambankResult){
        console.log("SUCCESS")
      }
      else{
        console.log("NOT SUCCESS")
        return;
      }
  
  }; 

const ambankGetCTStatus = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      
            #swagger.parameters['cTsrcRefNo'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }

  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      await walletService
      .ambankGetCTStatus(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  
  }; 

const queryBuyPrice = async (req, res, next) => {
    /*
    #swagger.security = [{
      "bearerAuth": []
    }] 
    
    #swagger.parameters['margin'] = {
                                  in: 'query',
                                  required: true,
                                  schema: 0.02
              }
    #swagger.parameters['uomCode'] = {
                            in: 'query',
                            required: true,
                            schema: 'J04'
    }
  */
    
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
    console.log("request params -> ", requestparams);
  
    let margin = {
      "setupFees": requestparams.margin,
      "setupUomCode": requestparams.uomCode
    }
  
    console.log("margin -> ", margin);
  
    await walletService
      .getQueryBuyPrice(margin, requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response.data;
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error);
        return error;
      });
  };
  
const querySellPrice = async (req, res, next) => {
    /*
    #swagger.security = [{
      "bearerAuth": []
    }] 
    
    #swagger.parameters['margin'] = {
                                  in: 'query',
                                  required: true,
                                  schema: 0.02
              }
    #swagger.parameters['uomCode'] = {
                            in: 'query',
                            required: true,
                            schema: 'J04'
    }
  */
    
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    console.log("request params -> ", requestparams);
  
    let margin = {
      "setupFees": requestparams.margin,
      "setupUomCode": requestparams.uomCode
    }
  
    console.log("margin -> ", margin);
    
    await walletService
      .getQuerySellPrice(margin, requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response.data;
      })
      .catch((error) => {
        console.error("********** Error : ", error.response);
        res.status(error.response.status).send(error);
        return error;
      });
  };

const spotOrderBuy = async (req, res, next) => {
    /*
    #swagger.security = [{
      "bearerAuth": []
    }] 
    
    #swagger.parameters['priceRequestId'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
    #swagger.parameters['price'] = {
                            in: 'query',
                            required: true,
                            schema: 285.32
    }
    #swagger.parameters['weight'] = {
                            in: 'query',
                            required: true,
                            schema: 4.25
    }
    #swagger.parameters['reference'] = {
                            in: 'query',
                            required: true,
                            schema: ''
    }
  */
    
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    console.log("request params -> ", requestparams);
  
    let params = {
      "priceRequestId": requestparams.priceRequestId,
      "price": requestparams.price,
      "weight": requestparams.weight,
      "reference": requestparams.reference
    }
  
    console.log("params -> ", params);
    
    await walletService
      .spotOrderBuy(params, requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response.data;
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(401).send(error);
        return error;
      });
  };
  
const spotOrderSell = async (req, res, next) => {
    /*
    #swagger.security = [{
      "bearerAuth": []
    }] 
    
    #swagger.parameters['priceRequestId'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
    #swagger.parameters['price'] = {
                            in: 'query',
                            required: true,
                            schema: 285.32
    }
    #swagger.parameters['weight'] = {
                            in: 'query',
                            required: true,
                            schema: 4.25
    }
    #swagger.parameters['reference'] = {
                            in: 'query',
                            required: true,
                            schema: ''
    }
  */
    
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    console.log("request params -> ", requestparams);
  
    let params = {
      "priceRequestId": requestparams.priceRequestId,
      "price": requestparams.price,
      "weight": requestparams.weight,
      "reference": requestparams.reference
    }
  
    console.log("params -> ", params);
    
    await walletService
      .spotOrderSell(params, requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
        return response.data;
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(401).send(error);
        return error;
      });
  };

const aceRedeem = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['redeemGram'] = {
                                in: 'query',
                                required: true,
                                schema: '2'
            }
      #swagger.parameters['reference'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['itemSerialNo'] = {
                                in: 'query',
                                required: true,
                                schema: '233422'
            }
      #swagger.parameters['itemDenomination'] = {
                                in: 'query',
                                required: true,
                                schema: '1'
            }
      #swagger.parameters['itemQuantity'] = {
                                in: 'query',
                                required: true,
                                schema: '1'
            }
      #swagger.parameters['deliveryName1'] = {
                                in: 'query',
                                required: true,
                                schema: 'Albab'
            }
      #swagger.parameters['deliveryName2'] = {
                                in: 'query',
                                required: true,
                                schema: 'Ahmad'
            }
      #swagger.parameters['deliveryContact1'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['deliveryContact2'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['deliveryAddress1'] = {
                                in: 'query',
                                required: true,
                                schema: 'Jalan 2'
            }
      #swagger.parameters['deliveryAddress2'] = {
                                in: 'query',
                                required: true,
                                schema: 'Kampung Dalam PAyah'
            }
      #swagger.parameters['deliveryAddress3'] = {
                                in: 'query',
                                required: true,
                                schema: 'Shah Alam'
            }
      #swagger.parameters['deliveryAddress4'] = {
                                in: 'query',
                                required: true,
                                schema: 'Selangor'
            }
      #swagger.parameters['deliveryState'] = {
                                in: 'query',
                                required: true,
                                schema: 'Selangor'
            }
      #swagger.parameters['deliveryPostcode'] = {
                                in: 'query',
                                required: true,
                                schema: '40200'
            }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await walletService
    .aceRedeem(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };

const logisticStatus = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                    "bearerAuth": []
            }] 
        #swagger.parameters['redemption_id'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
        
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
  
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await walletService
      .logisticStatus(requestparams, requestBody, requestHeaders)
      .then((response) => {
        console.log("*********** Response logisticStatus: ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("*********** Error logisticStatus: ", error);
        res.status(error.response.status).send(error.response.data);
      });
    
    };

const paynetPaymentNotification = async(req, res, next) =>{
  /*  
      #swagger.parameters['body'] = {
                              in: 'body',
                              required: true,
                              schema: {
                                        "Notification": {
                                          "EventHeader": {
                                            "MsgId": "",
                                            "EventCd": "PMT001"
                                          },
                                          "EventInfo": {
                                            "EndToEndID": "",
                                            "Signature": "",
                                            "PaymentStatus": {
                                              "Code": "ACSP",
                                              "Substate": "CLEARED",
                                              "Reason": "U000"
                                            }
                                          }
                                        }
                                      }
          }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

  
    await walletService
    .paynetPaymentNotification(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };

const paynetToken = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
    */ 
    
        const requestBody = req.body;
        const requestHeaders = req.headers;
        const requestparams= req.query;

        const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
        if(session.status==false){
          console.error("Session Expired");
          res.status(401).send("Session Expired");
          return;
        }
    
        await walletService
        .paynetToken(requestBody,requestHeaders,requestparams)
            .then((response) => {
              console.log("#############  Response : ", response);
               res.status(200).send(response.data);
            })
            .catch((error) => {
              console.error("###########  ERROR : ", error);
              res.status(error.response.status).send(error.response.data);
            });
  };

const paynetBankListIntegration = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                    "bearerAuth": []
            }] 
        #swagger.parameters['channelCode'] = {
                                  in: 'query',
                                  required: true,
                                  schema: 'BW'
              }

      #swagger.parameters['pageKey'] = {
                                  in: 'query',
                                  required: false,
                                  schema: ''
              }
    */ 
    
        const requestBody = req.body;
        const requestHeaders = req.headers;
        const requestparams= req.query;

        const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
        if(session.status==false){
          console.error("Session Expired");
          res.status(401).send("Session Expired");
          return;
        }
    
        await walletService
        .integrationPaynetBankList(requestBody,requestHeaders,requestparams)
            .then((response) => {
              console.log("#############  Response : returnUrl : ", response);
                res.status(200).send(response.data);
            })
            .catch((error) => {
              console.error("###########  ERROR : returnUrl", error);
              res.status(error.response.status).send(error.response.data);
            });
  };

const paynetBankList = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['channelCode'] = {
                                in: 'query',
                                required: true,
                                schema: 'BW'
            }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
  
      await walletService
      .paynetBankList(requestBody,requestHeaders,requestparams)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
              res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(error.response.status).send(error.response.data);
          });
};

const paynetInitiatePayment = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                        "channelCode":"BW",
                                        "bicCode":"ACFBMYK1",
                                        "amount":20.00,
                                        "currency":"MYR",
                                        "name":"",
                                        "recipientReference":"",
                                        "paymentDescription":""
                                    }
                                  }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
     
  
      await walletService
      .paynetInitiatePayment(requestBody,requestHeaders)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(error.response.status).send(error.response.data);
          });
};

const paynetCancelPayment = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                            "channelCode":"",
                                            "bicCode":"",
                                            "endToEndId":""
                                        }
                                      }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
     
  
      await walletService
      .paynetCancelPayment(requestBody,requestHeaders)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
             res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(error.response.status).send(error.response.data);
          });
};

const paynetRedirectRTP = async(req, res, next) =>{
  /*  
      #swagger.parameters['EndToEndId'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['EndtoEndIdSignature'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;


      var allowedParams = ["EndtoEndId","EndtoEndIdSignature"]; // Whitelist of allowed parameters
      var urlParam = "?";
      var i = 0;
      
      for (var attributename in requestparams) {
        if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
          if (i != 0) { urlParam += "&"; }
          console.log(attributename + ": " + requestparams[attributename]);
          urlParam += (attributename + "=" + requestparams[attributename]);
          i += 1;
        }
        else{
          console.log("NOT ALLOWED PARAMETER: "+attributename)
        }
      }

      console.log("UrLPARAM: " ,process.env.webUrl+urlParam);
    
      res.redirect(process.env.webUrl+urlParam)
  };

const paynetStatusInquiry = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['endToEndId'] = {
                              in: 'query',
                              required: true,
                              schema: ''
          }
      #swagger.parameters['channelCode'] = {
                          in: 'query',
                          required: true,
                          schema: ''
      }
  */ 
  
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams = req.query;
  
      await walletService
      .paynetStatusInquiry(requestparams,requestBody,requestHeaders)
          .then((response) => {
            console.log("#############  Response : returnUrl : ", response);
              res.status(200).send(response.data);
          })
          .catch((error) => {
            console.error("###########  ERROR : returnUrl", error);
            res.status(error.response.status).send(error.response.data);
          });
};



  



module.exports = {
  queryBuyPrice,
  querySellPrice,
  spotOrderBuy,
  spotOrderSell,
  aceRedeem,
  logisticStatus,

  ambankAccountEnquiry,
  ambankCreditTransfer,
  ambankGetCTStatus,
  testnetwork,
  paynetPaymentNotification,
  paynetToken,
  paynetBankListIntegration,
  paynetBankList,
  paynetInitiatePayment,
  paynetCancelPayment,
  paynetRedirectRTP,
  paynetStatusInquiry

}