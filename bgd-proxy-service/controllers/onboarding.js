const formidable = require("formidable");
const onboardingService = require("../services/onboarding");
const walletService = require("../services/wallet");
const sessionController = require("../controllers/session");
const moment = require('moment');
const nameCompareService = require("../services/nameCompare");


/* ************************  SUB MODULE : Get Entities *****************************/
// DONE SWAG //
const getInvestorType = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getInvestorType(requestBody)
    .then((response) => {
      console.log("***********Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*************Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

// DONE SWAG //
const getIdentificationType = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getIdentificationType(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

// DONE SWAG //
const getCountry = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getCountryList(requestBody)
    .then((response) => {
      console.log("********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("******** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const getStateList = async (req, res, next) => {
  /*  
      #swagger.parameters['countryCode'] = {
          in: 'query',
          required: 'false',
          schema: ''
      }
  */ 

  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams = req.query;

  await onboardingService
    .getStateList(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("******** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const getEmploymentStatusList = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getEmploymentStatusList(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const getJobPositionType = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getJobPositionType(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const getBusinessType = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getBusinessType(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};
const getTransactionPurposeType = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']
  

  const requestBody = req.body;

  await onboardingService
    .getTransactionPurposeType(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};
const getTotalMonthlyTransList = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getTotalMonthlyTransList(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};
const getFreqMonthlyTransList = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getFreqMonthlyTransList(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};
const getSourceOfFundType = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getSourceOfFundType(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};
const getSourceOfWealthType = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']

  const requestBody = req.body;

  await onboardingService
    .getSourceOfWealthType(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};
/* *************************************************************************/
// DONE SWAG //
const generateOTP = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']
    /*  #swagger.parameters['body'] = {
          in: 'body',
          description: 'User credentials.',
          required: true,
          schema: {
              "phoneNumber":""
          }
      }
    */
    const requestBody = req.body;
  
    await onboardingService
      .generateOTP(requestBody)
      .then((response) => {
        console.log("********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("******** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  };
  
// DONE SWAG //
const verifyOTP = async (req, res, next) => {
/*  #swagger.parameters['body'] = {
          in: 'body',
          description: 'otp verification ',
          required: true,
          schema: {
                    "serviceSid":"VA91366184dd77570a1cfd49334d9395ef",
                    "phoneNumber":"+601128223268",
                    "otp":"254828"
                }
      }
    */
    const requestBody = req.body;
  
    await onboardingService
      .verifyOTP(requestBody)
      .then((response) => {
        console.log("********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  };

// DONE SWAG //
const registerIndividual = async (req, res, next) => {
/*  #swagger.parameters['body'] = {
          in: 'body',
          description: 'User details',
          required: true,
          schema: {
                    "nationalityCode":"MY",
                    "fullName":"",
                    "email":"",
                    "identificationTypeCode":"P01",
                    "identificationNumber":"",
                    "phoneNumber":"",
                    "password":"",
                    "confirmPassword": ""
                  }
      } 
*/

  const requestBody = req.body;

  var createPasswordData = {};

  createPasswordData.password = requestBody.password;
  createPasswordData.confirmPassword = requestBody.confirmPassword;

  delete requestBody["password"];
  delete requestBody["confirmPassword"];

  var isRegistered = false;


  var registerIndividual = await onboardingService
    .registerIndividual(requestBody)
    .then((response) => {
      console.log("FIRST RESPONSE: ", response.data);

      createPasswordData.userID = response.data.id;
      console.log("############ Create password : ", createPasswordData);

      return response;
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
      isRegistered = false;
    });

  var createPassword = await onboardingService
    .createPassword(createPasswordData)
    .then((response) => {
      console.log("*************** Response create Password : ", response.data);
      //return response;
      res.status(201).send({
        userID: registerIndividual.data.id,
        status: "Successful",
      });
      
      return response;
    })
    
    .catch((error) => {
      console.error("******* Error : ", error.response);
      res.status(error.response.status).send(error.response.data);
      isRegistered = false;
    });

    
      
};

// DONE SWAG //
const getSecurityQuestion = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']
  const requestBody = req.body;

  await onboardingService
    .getSecurityQuestion(requestBody)
    .then((response) => {
      console.log("********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};


// DONE SWAG //
const createSecurityQuestion = async (req, res, next) => {
/*  #swagger.parameters['body'] = {
          in: 'body',
          description: 'User details',
          required: true,
          schema: {
                    "userID":"",
                    "questionCode":"",
                    "answer":""
                }
      } 
*/  
  const requestBody = req.body;

  await onboardingService
    .createSecurityQuestion(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const verifyEmail = async(req, res, next) =>{
  /*  
      #swagger.parameters['userID'] = {
          in: 'query',
          required: 'true',
          schema: ''
      }
      #swagger.parameters['email'] = {
          in: 'query',
          required: 'false',
          schema: ''
      }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;
  
    await onboardingService
    .verifyEmail(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };

const redirectVerifyEmail = async(req, res, next) =>{
  /*  
      #swagger.parameters['userID'] = {
          in: 'query',
          required: true,
          schema: ''
      }
      #swagger.parameters['verificationID'] = {
          in: 'query',
          required: true,
          schema: ''
      }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    var verifyEmailResponse = await onboardingService
    .redirectVerifyEmail(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response Redirect URL : ", response.data);
  
      res.redirect(response.data.redirectUrl)
      return response
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
      return error.response
    });
    
  };

const getPostCodeInfo = async(req, res, next) =>{
  /*  
      /*  
      #swagger.parameters['postcode'] = {
          in: 'query',
          required: 'true',
          schema: ''
      }
  */ 
  
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    await onboardingService
    .getPostCodeInfo(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };


/* ************************  SUB MODULE : ONBOARDING *****************************/

//DONE Swag//
const getBankList = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/  
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .getBankList(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

const kycValidate = async (req, res, next) => {
  const requestBody = req;

  const form = formidable.IncomingForm();

  const body = {};

  form.parse(req, async (err, fields, files) => {
    if (err) {
      res.status(400).send("ERROR");
      return;
    }

    console.log("Session Object >>>", files.id.path);

    body.id = files.id.path;
    body.face = files.face.path;


    await onboardingService
      .kycValidate(body)
      .then((response) => {
        console.log("********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("********* Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  });

  
  
};

const firstPartyChecking = async (req, res, next) => {
  const requestBody = req.body;

  await onboardingService
    .firstPartyChecking(requestBody)
    .then((response) => {
      console.log("*************** Country Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*************** Country Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

//DONE SWAG//
const tier1KycChecking = async (req, res, next) => {
  /*
          #swagger.security = [{
               "bearerAuth": []
            }] 
          #swagger.consumes = ['multipart/form-data']  
          #swagger.parameters['faceImg'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
              description: 'face image',
          } 
          #swagger.parameters['documentImgFront'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
              description: 'front ic image',
          }
          #swagger.parameters['documentImgBack'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
              description: 'back ic image',
          }
          #swagger.parameters['userID'] = {
              in: 'formData',
              type: 'string',
              required: 'true',
              description: 'user ID (UUID)',
              schema: ""
          }
          #swagger.parameters['kycType'] = {
              in: 'formData',
              type: 'string',
              required: 'true',
              description: 'KYC TYPE CODE (T01/T02/T03)',
              schema:"T01"
          }
           #swagger.parameters['applicationID'] = {
              in: 'formData',
              type: 'string',
              required: 'false',
              description: '',
              schema:""
          }

  */
  console.log("tier1KYCChecking starting..");
        
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  const form = formidable.IncomingForm();

  const body = {};
  form.parse(req, async (err, fields, files) => {
    if (err) {
      res.status(400).send("ERROR");
      reject(err);
    }

    body.documentImgFront = files.documentImgFront.path;
    body.documentImgBack = files.documentImgBack.path;
    body.faceImg = files.faceImg.path;
    body.userID = fields.userID;
    body.kycType = fields.kycType;
    if(fields.applicationID){
      body.applicationID = fields.applicationID;
    }
    

    // try{
      var kycCheck = await onboardingService
      .kycValidate2(body)
      .then((response) => {
        console.log("######### RESPONSE 1 : KYC : ", response.data);
        res.status(200).send(response.data);
        return response.data;
      })
      .catch((error) => {
        console.error("########### ERROR : KYC : ", error.response);
        res.status(error.response.status).send(error.response.data);
        throw error;
      });

  });
}

//DONE SWAG//
const tier2KycChecking = async (req, res, next) => {
/*
          #swagger.security = [{
               "bearerAuth": []
            }] 
          #swagger.consumes = ['multipart/form-data']  
          #swagger.parameters['faceImg'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
              description: 'face image',
          } 
          #swagger.parameters['documentImgFront'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
              description: 'front ic image',
          }
          #swagger.parameters['documentImgBack'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
              description: 'back ic image',
          }
          #swagger.parameters['userName'] = {
              in: 'formData',
              type: 'string',
              required: 'true',
              description: '',
              schema: ""
          }
          #swagger.parameters['aml'] = {
              in: 'formData',
              type: 'string',
              required: 'true',
              description: 'true or false',
              schema:"true"
          }

  */

  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }
  

  
  const form = formidable.IncomingForm();

  const body = {};

  var kycCheck = await new Promise((resolve,reject) =>{ 
    form.parse(req, async (err, fields, files) => {
      if (err) {
        res.status(400).send("ERROR");
        reject(err);
      }

      console.log("TRYYY : ",files);
      console.log("TRRYY FIELd", fields);
      
      body.faceImg = files.faceImg.path;
      body.documentImgFront = files.documentImgFront.path;
      body.documentImgBack = files.documentImgBack.path;

      body.userName = fields.userName;
      body.aml = fields.aml;

      console.log("Session Object >>>", body);

      await onboardingService
        .tier2KycChecking(body)
        .then((response) => {
          console.log("######### RESPONSE 1 : KYC Shufti : ", response.data);
          res.status(200).send(response.data);
          resolve(response.data);
        })
        .catch((error) => {
          console.error("########### ERROR : KYC Shufti : ", error);
          res.status(400).send(error);
          reject(error);
        });
    });
  }).catch((error)=> {
    console.log("######## ERROR DURING KYC tier 2CHECKING :",error)
});



if(!kycCheck || kycCheck.event != "verification.accepted"){
  return;
}

var  updateTierReq = {
  "reference": kycCheck.reference,
  "tierCode":"T02"
};

console.log("Updating Tier to tier 2 ...", updateTierReq);

var updateTier = await onboardingService
.updateTier(updateTierReq,requestHeaders)
.then((response) => {
  console.log("#############  Response 2 : Update Tier 2 SUCCESS : ", response);
})
.catch((error) => {
  console.error("###########  ERROR : Update Tier", error);
});

};

const tier3KYCChecking = async (req, res, next) => {
  const requestBody = req.body;

  await onboardingService
    .tier3KYCChecking(requestBody)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

//DONE SWAG//
const updateTier = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
          in: 'body',
          description: 'Tier Upgrade',
          required: true,
          schema: {
                      "reference": "",
                      "tierCode":"T01",
                      "kycStatusCode":"G09",
                      "identificationTypeCode":"P01",
                      "identificationNumber":""
                  }
      }
*/  
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .updateTier(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};


/* ************************  SUB MODULE : Login *****************************/

async function checkStatus(accountStatusCode) {

  switch(accountStatusCode) {
    case "G18":
      // code block
      return {
              authorize: false,
              responseCode:"RCE010",
              description:"Account deleted"
          }
    case "G19":
      return {
              authorize: false,
              responseCode:"RCE009",
              description:"Account Dormant"
            }
    case "G20":
      return {
              authorize: false,
              responseCode:"RCE007",
              description:"Account suspended (Disciplinary Action)"
            }
    case "G21":
      return {
              authorize: false,
              responseCode:"RCE008",
              description:"Account suspended (Under Investigation)"
            }
    case "G23":
      return {
              authorize: false,
              responseCode:"RCE011",
              description:"Account closed"
            }
    case "G18/02":
      return {
              authorize: false,
              responseCode:"RCE015",
              description:"Account pending delete"
            }
    default:
      return{
          authorize: true,
          responseCode:"",
          description:""
      }

  }
}


//DONE SWAG//
const login = async (req, res, next) => {
  // #swagger.tags = ['Onboarding']
  /*  #swagger.parameters['obj'] = {
        in: 'body',
        description: 'User credentials.',
        required: true,
        schema: {
            username: "",
            password: ""
        }
    }
      #swagger.parameters['client'] = {
        in: 'header',
        description: 'Client secret key',
        required: 'true',
        type: 'string',
        schema: 'bgd-client'
      } 
      #swagger.parameters['secret'] = {
        in: 'header',
        description: 'Client secret key',
        required: 'true',
        type: 'string',
        schema: ''
      }   
  */

  const requestBody = req.body;
  const requestHeaders = req.headers;

  if (
    requestHeaders.client == process.env.client &&
    requestHeaders.secret == process.env.secret
  ) {
    await onboardingService.accountStatus(requestHeaders, requestBody)
      .then(async (response) => {
        
        
        if(response.data.ssoUserID && response.data.accountStatus && (response.data.emailVerified == true || response.data.emailVerified == "true")){
          const firstTimeLogin=response.data.firstTimeLogin;
          const ssoUserID = response.data.ssoUserID;
          const accountStatusCode = response.data.accountStatus;
          
          const accountStatus = await checkStatus(accountStatusCode);
          if(accountStatus.authorize==true){
              
              const requestHeadersgetSSOUserID={};
              const loginPrevilageRequest = await onboardingService
                .loginPrevilage(requestHeaders, requestBody)
                .then((response) => {
                  console.log("####### DAPAT RESPONSE*****")
                  // console.log("*************** Login Previlage Response : ", response.data);
                  return response.data;
                })
                .catch((error) => {
                  console.error("*************** Login previlage Error : ", error.response.data);
                });

                if(!loginPrevilageRequest){
                  res.status(500).send({
                            responseCode:'RCE999',
                            description:'Failed during login privelage admin',
                            data:'',
                            httpStatus:500,
                            dateTime:moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                          })
                  return;
                }
              
              requestHeadersgetSSOUserID.Authorization="Bearer "+loginPrevilageRequest.access_token;
              // console.log("REQUEST HEADER: ",requestHeadersgetSSOUserID);
              // console.log("loginPrevilageResponse",loginPrevilageRequest)

              const getSSOUserRequest = await onboardingService
                  .getSSOUserID(requestHeadersgetSSOUserID, requestBody, ssoUserID)
                  .then((response) => {
                    console.log("*************** getSSOUUserID Response : ", response.data);
                    return response.data;
                  })
                  .catch((error) => {
                    console.error("*************** getSSOUUserID Error : ", error.response.data);
                  });
              console.log("********** getSSOUserRequest: ", getSSOUserRequest)

              if(!getSSOUserRequest||getSSOUserRequest.length<1){
                res.status(500).send({
                          responseCode:'RCE999',
                          description:'Failed to get user from keycloak',
                          data:'',
                          httpStatus:500,
                          dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                        })
                return;
              }else if(getSSOUserRequest.enabled==false){
                res.status(401).send({
                              responseCode:'RCE014',
                              description:'Account locked',
                              data:'',
                              httpStatus:401,
                              dateTime:moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                            })
                return;
              }

              // console.log("USER FROM KEYCLOAK: " ,getSSOUserRequest[0]);
              console.log("USER FROM KEYCLOAK: " ,getSSOUserRequest);

              ///comment temp
              // const updateUserSession = await sessionController.updateUserSession(getSSOUserRequest[0].id);
              const updateUserSession = await sessionController.updateUserSession(getSSOUserRequest.id);
              if(!updateUserSession || updateUserSession==false){
                console.log("FAILED TO UPDATE SESSIONS")
                res.status(500).send({
                                responseCode:'RCE999',
                                description:'Failed to update session',
                                data:'',
                                httpStatus:500,
                                dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                              });
                return;
              }
              else{
                console.log("SUCCESSFULLY UPDATE SESSIONS")
              }
              
              const loginRequest = await onboardingService
                .login(requestHeaders, requestBody)
                .then((response) => {
                  console.log("*************** Login Response : ", response.data);
                  response.data.firstTimeLogin=firstTimeLogin;
                  res.status(200).send(response.data);
                  return response.data;
                })
                .catch((error) => {
                  console.error("*************** Login Error : ", error.response.data);
                  res.status(401).send(
                    {
                      responseCode:'RCE999',
                      description:'Failed to login',
                      data:error.response.data.error_description,
                      httpStatus:401,
                      dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                    });
                });

                if(!loginRequest){
                  return;
                }

                // comment temp
                // const createUserSession = await sessionController.createNewSession(getSSOUserRequest[0].id,loginRequest.access_token,getSSOUserRequest[0].username);
                const createUserSession = await sessionController.createNewSession(getSSOUserRequest.id,loginRequest.access_token,getSSOUserRequest.username);
                return;

          }else{
                //Not authorize
                res.status(401).send({
                  responseCode:accountStatus.responseCode,
                  description:accountStatus.description,
                  data:'',
                  httpStatus:401,
                  dateTime:moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                })
                return;
          }
        

        } else {
          res.status(401).send({
            responseCode:'RCE013',
            description:"Email is not verified",
            data:'',
            httpStatus:401,
            dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
          });
        }
      })
      .catch((error) => {
        console.error("******** User Account Status Error :", error);
        // console.error("************ User Email Verification Error : ", error.response.data);
        res.status(500).send({
          responseCode:'RCE999',
          description: 'Failed to check account status',
          data:error.response.data.message,
          httpStatus:500,
          dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
        });
      });

  } else {
    // res.status(401).send("Invalid client and secret");
    res.status(500).send({
      responseCode:'RCE012',
      description:'Invalid client and secret',
      data:'',
      httpStatus:500,
      dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
    });
  }
};

//DONE SWAG//
const refreshToken = async (req, res, next) => {
  /* 
      #swagger.parameters['client'] = {
        in: 'header',
        description: 'Client secret key',
        required: 'true',
        type: 'string',
        schema: 'bgd-client'
      } 
      #swagger.parameters['secret'] = {
        in: 'header',
        description: 'Client secret key',
        required: 'true',
        type: 'string',
        schema: ''
      }   
      #swagger.parameters['body'] = {
        in: 'body',
        description: 'Refesh Token',
        required: 'true',
        type: 'string',
        schema: {
                    "refresh_token":"" ,
                    "username":""
                }
     } 
  */
  const requestBody = req.body;
  const requestHeaders = req.headers;


  if (
    requestHeaders.client == process.env.client &&
    requestHeaders.secret == process.env.secret
  ) {

    var getResponse = false;
    setTimeout(()=>{
      if(getResponse==false){
        console.log("TIMEOUT")
        res.status(504).send({
                responseCode:'RCE998',
                description:'Timeout',
                data:'',
                httpStatus:504,
                dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
              });
      }
    },10000)


    // const requestHeadersgetSSOUserID={};
    // const loginPrevilageRequest = await onboardingService
    //   .loginPrevilage(requestHeaders, requestBody)
    //   .then((response) => {
    //     // console.log("*************** Login Previlage Response : ", response.data);
    //     return response.data;
    //   })
    //   .catch((error) => {
    //     console.error("*************** Login previlage Error : ", error.response.data);
    //   });

    //   if(!loginPrevilageRequest){
    //     res.status(500).send("Internal Server Error")
    //     return;
    //   }
      
    // requestHeadersgetSSOUserID.Authorization="Bearer "+loginPrevilageRequest.access_token;

    // const getSSOUserRequest = await onboardingService
    //     .getSSOUserID(requestHeadersgetSSOUserID, requestBody)
    //     .then((response) => {
    //       console.log("*************** getSSOUUserID Response : ", response.data);
    //       return response.data;
    //     })
    //     .catch((error) => {
    //       console.error("*************** getSSOUUserID Error : ", error.response.data);
    //     });

    // if(!getSSOUserRequest||getSSOUserRequest.length<1){
    //   res.status(500).send("Internal Server Error")
    //   return;
    // }else if(getSSOUserRequest.enabled==false){
    //   res.status(403).send("Account locked")
    //   return;
    // }

    // // console.log("USER FROM KEYCLOAK: " ,getSSOUserRequest[0]);
    // console.log("USER FROM KEYCLOAK: " ,getSSOUserRequest0);

    // comment temp
    // const updateUserSession = await sessionController.updateUserSession(getSSOUserRequest[0].id);
       await onboardingService.accountStatus(requestHeaders, requestBody)
          .then(async (response) => {
            // console.log("ssoUserID: ", ssoUserID);
            if(response.data.ssoUserID && response.data.accountStatus && (response.data.emailVerified == true || response.data.emailVerified == "true")){
            const accountStatusCode = response.data.accountStatus;
            const accountStatus = await checkStatus(accountStatusCode);
            if(accountStatus.authorize==true){
              
              const ssoUserID = response.data.ssoUserID;
              const updateUserSession = await sessionController.updateUserSession(ssoUserID);
              if(!updateUserSession || updateUserSession==false){
                console.log("FAILED TO UPDATE SESSIONS")
                res.status(500).send({
                              responseCode:'RCE999',
                              description:'Failed to update session',
                              data:'',
                              httpStatus:500,
                              dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                            });
                return;
              }
              else{
                console.log("SUCCESSFULLY UPDATE SESSIONS")
              }

              const refreshTokenRequest= await onboardingService
                .refreshToken(requestHeaders, requestBody)
                .then((response) => {
                  console.log("**********Response : ", response);
                  getResponse=true;
                  res.status(200).send(response.data);
                  return response.data;
                })
                .catch((error) => {
                  console.error("*********** Refresh Error : ", error);
                  getResponse=true;
                  res.status(500).send({
                                  responseCode:'RCE999',
                                  description:'Failed to refresh',
                                  data:error.response.data.error_description,
                                  httpStatus:500,
                                  dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                                });
                });

                if(!refreshTokenRequest){
                  
                  return;
                }

                // comment temp
                const createUserSession = await sessionController.createNewSession(ssoUserID,refreshTokenRequest.access_token,requestBody.username);
                
                return;

              }else{
                //Not authorize
                res.status(401).send({
                  responseCode:accountStatus.responseCode,
                  description:accountStatus.description,
                  data:'',
                  httpStatus:401,
                  dateTime:moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
                })
                return;
              }

          } else {
            res.status(401).send({
              responseCode:'RCE013',
              description:"Email is not verified",
              data:'',
              httpStatus:401,
              dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
            });
          }
      })
      .catch((error) => {
        console.error("******** User Account Status Check Error :", error);
        // console.error("************ User Email Verification Error : ", error.response.data);
        res.status(500).send({
          responseCode:'RCE999',
          description: 'Failed to check account status',
          data:error.response.data.message,
          httpStatus:500,
          dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
        });
      });


    } else {
      // res.status(401).send("Invalid client and secret");
      res.status(500).send({
        responseCode:'RCE012',
        description:'Invalid client and secret',
        data:'',
        httpStatus:500,
        dateTime: moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')
      });
    }
};

//DONE SWAG//
const getName = async (req, res, next) => {
  /* 

      #swagger.parameters['username'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
  */

  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  await onboardingService
    .getName(requestBody, requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const getUserDetails = async (req, res, next) => {
  /* #swagger.security = [{
               "bearerAuth": []
        }] 
  */

  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
    .getUserDetails(requestBody, requestHeaders)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

/* ************************  SUB MODULE : Username Password Retrieval *****************************/

//DONE SWAG//
const changePassword = async (req, res, next) => {
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
          in: 'body',
          description: 'Change password',
          required: true,
          schema: {
                    "oldPassword":"",
                    "newPassword": "",
                    "confirmPassword": ""
                }
      }
*/  
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
    .changePassword(requestBody, requestHeaders)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const validatePassword = async (req, res, next) => {
  /*  
      #swagger.parameters['body'] = {
            in: 'body',
            description: 'Validate password',
            required: true,
            schema: {
                      "password": ""
                  }
        }
  */  
    const requestBody = req.body;
    const requestHeaders = req.headers;
  
  
    await onboardingService
      .validatePassword(requestBody, requestHeaders)
      .then((response) => {
        console.log("************ Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  };

//DONE SWAG//
const securityQuestion = async (req, res, next) => {
  /*
    #swagger.parameters['email'] = {
      in: 'query',
      required: 'true',
      schema: ''
    }
  */ 

  const requestHeaders = req.headers;
  const requestparams= req.query;

  await onboardingService
    .securityQuestion(requestparams, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

//DONE SWAG//
const generateResetPassword = async (req, res, next) => {
/*  #swagger.parameters['body'] = {
          in: 'body',
          description: 'Reset password',
          required: true,
          schema: {
                      "email":"",
                      "questionCode":"Q01",
                      "answer":""
                  }
      }
    */  

  const requestBody = req.body;
  const requestHeaders = req.headers;

  await onboardingService
    .generateResetPassword(requestBody, requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

const resetPassword = async(req, res, next) =>{
  /*  
      #swagger.parameters['body'] = {
          in: 'body',
          description: 'Reset password',
          required: true,
          schema: {
                      "id":"",
                      "userID":"",
                      "newPassword":"",
                      "confirmPassword":""
                  }
      }
  */ 
  
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    await onboardingService
    .resetPassword(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};


/* ************************  SUB MODULE : User Profile *****************************/

//DONE SWAG//
const createShippingAddress = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
          in: 'body',
          description: 'create addresss',
          required: true,
          schema: {
                    "label":"Home",
                    "address1":"No 123, Jalan Keamanan",
                    "address2":"Kg Batu Belah",
                    "postcode":"",
                    "town":"Shah Alam",
                    "stateCode":"SGR",
                    "countryCode":"MY",
                    "addressTypeCode":"L03"
                }
      }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .createShippingAddress(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

//DONE SWAG//
const getShippingAddress = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .getShippingAddress(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

const updateShippingAddress = async(req, res, next) =>{
  /*  
      #swagger.security = [{
               "bearerAuth": []
        }] 
      #swagger.parameters['body'] = {
          in: 'body',
          description: 'Reset password',
          required: true,
          schema: {
                      "id":"",
                      "label":"Home",
                      "address1":"No 456, Jalan Keamanan",
                      "address2":"Kg Batu tiga",
                      "postcode":"",
                      "town":"Klang",
                      "stateCode":"SGR",
                      "countryCode":"MY",
                      "addressTypeCode":"L01"
                  }
      }
  */ 
  
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await onboardingService
    .updateShippingAddress(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
};

//DONE SWAG//
const createBankDetails = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
          in: 'body',
          description: ' Bank Details',
          required: false,
          schema: {
                      "creditorAccountType": "DFLT",
                      "secondValidationIndicator": "N",
                      "idType": "01",
                      "idNo": "",
                      "creditorAccountNo": "",
                      "receiverBIC": "ARBKMYKL",
                      
                    }
      }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  const getIdDetailsRequest= await onboardingService
                            .getUserDetails(requestBody, requestHeaders)
                            .then((response) => {
                              // console.log("************ Response : ", response.data);
                              return response.data
                            })
                            .catch((error) => {
                              console.error("********** Error : ", error);
                              // res.status(error.response.status).send(error.response.data);
                            });
    
    if(!getIdDetailsRequest){
      res.status(500).send("Internal Error: Cannot getUserDetails")
      return;
    }
    console.log("USERDETAILS:",getIdDetailsRequest)
    console.log("Full Name: ",getIdDetailsRequest.fullName)

  
   req.body.fullName = getIdDetailsRequest.fullName;
   const status = await ambankEnquiryFunction(req);

   console.log("STATUS: ",status);
  
   //FOR TESTING WE APPROVED ALSO IF PENDING, meaning name is not same as bank name
  if(status=='verification.accepted' || status=='verification.pending') {

        

        console.log('CALLING API creating bank accaount ... ')
        var createBankReqBody = req.body;
        
        delete createBankReqBody.creditorAccountType;
        delete createBankReqBody.secondValidationIndicator;
        delete createBankReqBody.idType;
        delete createBankReqBody.idNo;
        
        createBankReqBody.accountNumber=createBankReqBody.creditorAccountNo
        createBankReqBody.bicCode=createBankReqBody.receiverBIC

        delete createBankReqBody.creditorAccountNo
        delete createBankReqBody.receiverBIC


        console.log("Create Bank Req Body : " , createBankReqBody);

        const createBankDetailResponse = await onboardingService
        .createBankDetails(createBankReqBody,requestHeaders)
        .then((response) => {
          console.log("**********Response : ", response.data);
            res.status(200).send(response.data);
        
            // if( fullName &&  accountEnquiry.data.creditorAccountName != fullName){  //if fullname param is given, we check the credibility, else if not given, just succeed it for testing purpose
            //         return 'verification.pending'  //if name not same
            //       }
            //       else{
            //         return 'verification.accepted' //if name is same
            //       }
            
        })
        .catch((error) => {
          console.error("******* Error : ", error.response);
          res.status(error.response.status).send(error.response.data);  
        });
        return;
  }
  else if( status=='verification.declined'){ //error because of user account
          res.status(409).send({ response : "FAIL",
                                  remark : "Ambank verification declined",
                                  message:"Declined because of inactive account"});
        return;
  }else if( status=='verification.pending'){ //error because of user account
    res.status(409).send({ response : "PENDING",
                            remark : "Ambank verification pending",
                            message:"Pending because of given name is not same as account bank"});
    return;
}
  else { // means error in the system either mr or ambank
          res.status(409).send({ response : "ERROR",
                                      remark : "Error during ambank verification",
                                      message:"Error during ambank verification"});
          return;
  }

};

async function ambankEnquiryFunction(req){

  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams=req.query;

  var fullName = requestBody.fullName;

  delete requestBody.fullName;

  console.log("REQUEST BODY : ",requestBody);

  var createBankReqBody = requestBody;

  var aeRequestBody = requestBody;

  console.log("AE Request Body :",aeRequestBody);

  var accountEnquiry = await walletService
  .ambankAccountEnquiry(aeRequestBody,requestHeaders,requestparams)
  .then((response) => {
    console.log("**********Response : ", response.data);
    // res.status(200).send(response.data);
    return response;
  })
  .catch((error) => {
    console.error("******* Error : ", error.response.data);
    // res.status(error.response.status).send(error.response.data);
    return error.response;
  });


  console.log("ACCOUNT ENQUIRY RESPONSE STATUS: ",accountEnquiry.status)
  console.log("ACCOUNT ENQUIRY RESPONSE MESSAGE; ",accountEnquiry.data)
  
  if(!accountEnquiry || !accountEnquiry.data || !accountEnquiry.status){
    return 'verification.error'   //stop until here if ambank verification not passed
  }
  else if(accountEnquiry.data.creditorAccountName && accountEnquiry.status==200){
    // const icNameFiltered = fullName.replace(/[^a-zA-Z0-9]/g, ''); // remove all character except a-z A-Z 0-9
    // const bankNameFiltered =accountEnquiry.data.creditorAccountName.replace(/[^a-zA-Z0-9]/g, '');
    const icFullName = fullName;// remove all character except a-z A-Z 0-9
    const bankFullName =accountEnquiry.data.creditorAccountName;
    console.log("IC Credential Name: "+icFullName);
    console.log("Bank Credential Name: "+bankFullName);

    
    
    const compareNameresult =nameCompareService.nameCompare(icFullName,bankFullName)


    if(compareNameresult==true){
      return 'verification.accepted'
    }
    else{
      return 'verification.pending'
    }
  }
  else if(accountEnquiry.status==409 && accountEnquiry.data.ResponseCode=='000010' ){ // non active status
    return 'verification.declined'
  }
  else {
    return 'verification.error'
  }

  
}

//DONE SWAG//
const getBankDetails = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .getBankDetails(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

const closeUserAccount = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['userID'] = {
                                in: 'query',
                                required: false,
                                description: 'user ID (UUID)',
                                schema: ''
            }
    */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;
  
    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await onboardingService
    .closeUserAccount(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("**********Response : ", response.data);
      console.log("**********Response status : ", response.status);
      res.status(response.status).send(response.data);
    })
    .catch((error) => {
      console.error("******* Error : ", error.response);
      res.status(error.response.status).send(error.response.data);
    });
  
  };

//DONE SWAG//
const deleteBankDetails = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
          in: 'body',
          description: 'Bank code ',
          required: true,
          schema: {
                      "bicCode":""
                  }
      }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .deleteBankDetails(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};



/* ***********************  SUB MODULE : PIN GENERATION *****************************/

//DONE SWAG//
const createPin = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
          in: 'body',
          required: true,
          schema: {
                    "newPin":''
                }
      }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .createPin(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

//DONE SWAG//
const changePin = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['body'] = {
          in: 'body',
          required: true,
          schema: {
                    "currentPin":'',
                    "newPin":'',
                    "confirmPin":''
                }
      }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .changePin(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

//DONE SWAG//
const verifyPin = async(req, res, next) =>{
/*  
    #swagger.parameters['body'] = {
          in: 'body',
          required: true,
          schema: {
                      "userName":"",
                      "userPin":''
                  }
      }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  await onboardingService
  .verifyPin(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

//DONE SWAG//
const disablePin = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await onboardingService
  .disablePin(requestBody,requestHeaders)
  .then((response) => {
    console.log("**********Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("******* Error : ", error.response);
    res.status(error.response.status).send(error.response.data);
  });

};

const resetPin = async(req, res, next) =>{
  /*  
    #swagger.security = [{
               "bearerAuth": []
        }] 

    #swagger.parameters['body'] = {
          in: 'body',
          required: true,
          schema: {
                    "newPin":'',
                    "confirmPin":''
                }
      }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    // console.log(" ajsja", req.query);
    await onboardingService
    .resetPin(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };

  const addDeviceID = async(req, res, next) =>{
  /*  
     #swagger.parameters['body'] = {
          in: 'body',
          required: true,
          schema: {
                  "userID":"",
                  "deviceID":""
              }
      }
  */ 
  
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;
  
    // console.log(" ajsja", req.query);
    await onboardingService
    .addDeviceID(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("************ Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };
  const updateNotificationPermission = async(req, res, next) =>{
   /*  
          #swagger.security = [{
                 "bearerAuth": []
          }] 
          #swagger.parameters['body'] = {
            in: 'body',
            description: 'Update Push Notification & Email Notification permission',
            required: true,
            schema: {
              "userID":"",
              "allowPushNotification":true,
              "allowEmailNotification":true
            }
          }
          */ 
    
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      
      await onboardingService
      .updateNotificationPermission(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("************ Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
    
  };

  const createProfilePicture = async (req, res, next) => {

  /*
          #swagger.security = [{
               "bearerAuth": []
            }] 
          #swagger.consumes = ['multipart/form-data']  
          #swagger.parameters['image'] = {
              in: 'formData',
              type: 'file',
              required: 'true',
              description: 'profile image',
          } 
    */

    const requestBody = req;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    const form = formidable.IncomingForm();
  
    const body = {};
  
    form.parse(req, async (err, fields, files) => {
      if (err) {
        res.status(400).send("ERROR");
        return;
      }
  
      console.log("Session Object >>>", files.image.path);
  
      body.image = files.image.path;
  
  
      await onboardingService
        .createProfilePicture(body,requestHeaders,requestparams)
        .then((response) => {
          console.log("********** Response : ", response.data);
          res.status(200).send(response.data);
        })
        .catch((error) => {
          console.error("********* Error : ", error);
          res.status(error.response.status).send(error.response.data);
        });
    });
  
    
  };


  const ctosVerification = async (req, res, next) => {

    /*
          
      */
        const requestBody = req.body;
        const requestHeaders = req.headers;
        const requestparams= req.query;
      
        // console.log(" ajsja", req.query);
        await onboardingService
        .ctosVerification(requestBody,requestHeaders,requestparams)
        .then((response) => {
          console.log("************ Response : ", response);
          res.status(response.status).send(response.data);
        })
        .catch((error) => {
          console.error("********** Error : ", error);
          res.status(error.response.status).send(error.response.data);
        });
    
    };


    const validateReferralCode = async (req, res, next) => {
      /* 
    
          #swagger.parameters['referralCode'] = {
                                    in: 'query',
                                    required: true,
                                    schema: ''
                }
      */
    
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      await onboardingService
        .validateReferralCode(requestBody, requestHeaders,requestparams)
        .then((response) => {
          console.log("************ Response : ", response.data);
          res.status(200).send(response.data);
        })
        .catch((error) => {
          console.error("********** Error : ", error);
          res.status(error.response.status).send(error.response.data);
        });
    };
/**************************   SUB MODULE : SIT ***********************************************/
const ctosAuthenticateSIT = async (req, res, next) => {
          /* 
          */ 
    
    const requestHeaders = req.headers;
    await onboardingService
    .ctosAuthenticateSIT(requestHeaders)
    .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
  };

  const getCtosVerificationRecordSIT = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['id'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await onboardingService
      .getCtosVerificationRecordSIT(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("**********Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("******* Error : ", error.response);
        res.status(error.response.status).send(error.response.data);
      });
  };

const createCtosVerificationSIT = async (req, res, next) => {
          /*  
          #swagger.security = [{
                   "bearerAuth": []
            }] 
          #swagger.parameters['body'] = {
              in: 'body',
              description: 'Create CTOS CAD',
              required: true,
              schema: {
                        "userName":"",
                        "fullName": "",
                        "identificationTypeCode":"P01",
                        "identificationNumber": "",
                        "nationalityCode": "MY",
                        "countryOfTaxResidenceCode":"MY",
                        "customerType":"Individual",
                        "customerCategory":"New Customer",
                        "dob": "",
                        "industryType":"Information and Communication"
                    }
          }
          */ 
    
    const requestHeaders = req.headers;
    const requestBody = req.body;
    await onboardingService
    .createCtosVerificationSIT(requestBody,requestHeaders)
    .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
    };

  const updateCtosVerificationSIT = async (req, res, next) => {
    /*  
    #swagger.security = [{
             "bearerAuth": []
      }] 
    #swagger.parameters['body'] = {
        in: 'body',
        description: 'Create CTOS CAD',
        required: true,
        schema: {
                  "id":"",
                  "userName":"",
                  "fullName": "",
                  "identificationTypeCode":"P01",
                  "identificationNumber": "",
                  "nationalityCode": "MY",
                  "countryOfTaxResidenceCode":"MY",
                  "customerType":"Individual",
                  "customerCategory":"New Customer",
                  "dob": "",
                  "industryType":"Information and Communication",
                  "bypassekyc":"Yes"
              }
    }
    */ 

  const requestHeaders = req.headers;
  const requestBody = req.body;
  await onboardingService
  .updateCtosVerificationSIT(requestBody,requestHeaders)
  .then((response) => {
    console.log("*********** Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("*********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });
};
    
/* ************************  SUB MODULE : User Profile & Details *****************************/
    
  const createCtosVerification = async (req, res, next) => {
          /*  
          #swagger.security = [{
                   "bearerAuth": []
            }] 
          #swagger.parameters['body'] = {
              in: 'body',
              description: 'Create CTOS CAD',
              required: true,
              schema: {
                        "userName":"",
                        "verificationTypeCode":""
                    }
          }
          */ 
    
    const requestHeaders = req.headers;
    const requestBody = req.body;
    let createWallet=true;

    const createCtosVerificationResponse = await onboardingService
      .createCtosVerification(requestBody,requestHeaders)
      .then((response) => {
          console.log("*********** Response : ", response.data);
          res.status(200).send(response.data);
          return response
        })
        .catch((error) => {
          console.error("*********** Error : ", error);
          res.status(error.response.status).send(error.response.data);
          return;
        });

      // if(!createCtosVerificationResponse || !createCtosVerificationResponse.status==200){
      //   console.log("Failed During creating password : ");
      //   return;
      // }
  
      //create wallet eventhough failed ctos
      if(createWallet == true){
        console.log('CALLING API creating wallet ... ')
                ////////////////// CREATE WALLET (WILL RUN IN BACKGROUND) ////////////////
                var createWalletReq = [
                  {
                    "uomCode": "J01",
                    "amount": "0.00",
                    "walletTypeCode": "E01",
                    "regTypeCode": "D02"
                  },
                  {
                    "uomCode": "J02",
                    "amount": "0.00",
                    "walletTypeCode": "E02",
                    "regTypeCode": "D02"
                  }
                ];
                
                const createWalletResponse = await walletService
                  .createWallet(createWalletReq,requestHeaders,'functionCall')
                  .then((response) => {
                      console.log("#CONTROLLER:  Response 4 : CreateWallet : status: ", response.status);
                      console.log("#CONTROLLER:  Response 4 : CreateWallet : ", response);
                      return response;
                  })
                  .catch((error) => {
                    console.error("#CONTROLLER:  ERROR : Create Wallet", error||'unhandled error');
                    console.error("#CONTROLLER:  ERROR : Create Wallet: status: ", error.response.status || 'unhandled error');
                    // res.status(error.response.status).send(error.response.data);
                  });
          }   
    


  };
  const getCtosVerificationResult = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['userID'] = {
                                in: 'query',
                                required: false,
                                description: 'user ID (UUID)',
                                schema: ''
            }
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await onboardingService
      .getCtosVerificationResult(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("**********Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("******* Error : ", error.response);
        res.status(error.response.status).send(error.response.data);
      });
  };
  const getFinancialDetails = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['userID'] = {
                                in: 'query',
                                required: false,
                                description: 'user ID (UUID)',
                                schema: ''
            }
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await onboardingService
      .getFinancialDetails(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("**********Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("******* Error : ", error.response);
        res.status(error.response.status).send(error.response.data);
      });
  };
  const getEmploymentDetails = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['userID'] = {
                                in: 'query',
                                required: false,
                                description: 'user ID (UUID)',
                                schema: ''
            }
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await onboardingService
      .getEmploymentDetails(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("**********Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("******* Error : ", error.response);
        res.status(error.response.status).send(error.response.data);
      });
  };
  const getContactDetails = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['userID'] = {
                                in: 'query',
                                required: false,
                                description: 'user ID (UUID)',
                                schema: ''
            }
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await onboardingService
      .getContactDetails(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("**********Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("******* Error : ", error.response);
        res.status(error.response.status).send(error.response.data);
      });
  };
  const getPersonalDetails = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['userID'] = {
                                in: 'query',
                                required: false,
                                description: 'user ID (UUID)',
                                schema: ''
            }
    */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
    
      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await onboardingService
      .getPersonalDetails(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("**********Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("******* Error : ", error.response);
        res.status(error.response.status).send(error.response.data);
      });
  };
  const updateProfileDetails = async (req, res, next) => {
    /*  
        #swagger.security = [{
                    "bearerAuth": []
            }] 
        #swagger.parameters['body'] = {
            in: 'body',
            description: 'Update Personal Details',
            required: true,
            schema: {
              "personalDetails":{
                "fullName": "",
                "identificationTypeCode": "P01",
                "identificationNumber": "",
                "nationalityCode": "MY",
                "dob": ""
              },
              "contactDetails":{
                "email":"",
                "phoneNumber": "",
                "homeNumber": "",
                "officeNumber": "",
                "address":[
                  {
                    "addressTypeCode": "L01",
                    "address1": "No 25, Level 2, Jalan Keamanan",
                    "address2": "Kg Batu tiga",
                    "postcode": "",
                    "city": "Shah Alam",
                    "stateCode": "SGR",
                    "countryCode": "MY"
                  }
                ]
              },
              "employmentDetails":{
                "employmentStatusCode":"EMPL",
                "employerName":"",
                "positionCode":"PSN06",
                "natureOfBusinessCode":"BSNS02",
                "countryOfTaxResidence":"MY"
              },
              "financialDetails":{
                "transactionPurposeCode":"",
                "totalMonthlyTransactionCode":"",
                "freqMonthlyTransactionCode":"",
                "sourceOfFundCode":"",
                "sourceOfWealthCode":""
              }
            }
          }
    */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    await onboardingService
    .updateProfileDetails(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  };
  const updatePersonalDetails = async (req, res, next) => {
    /*  
        #swagger.security = [{
                    "bearerAuth": []
            }] 
        #swagger.parameters['body'] = {
            in: 'body',
            description: 'Update Personal Details',
            required: true,
            schema: {
              "fullName": "",
              "identificationTypeCode": "P01",
              "identificationNumber": "",
              "nationalityCode": "MY",
              "dob": ""
            }
          }
    */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    await onboardingService
    .updatePersonalDetails(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  };

  const updateContactDetails = async (req, res, next) => {
    /*  
        #swagger.security = [{
                    "bearerAuth": []
            }] 
        #swagger.parameters['body'] = {
            in: 'body',
            description: 'Update Contact Details',
            required: true,
            schema: {
              "email":"",
              "phoneNumber": "",
              "homeNumber": "",
              "officeNumber": "",
              "address":[
                {
                  "addressTypeCode": "L01",
                  "address1": "No 25, Level 2, Jalan Keamanan",
                  "address2": "Kg Batu tiga",
                  "postcode": "",
                  "city": "Shah Alam",
                  "stateCode": "SGR",
                  "countryCode": "MY"
                }
              ]
            }
          }
    */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    await onboardingService
    .updateContactDetails(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  };
    
  const insertEmploymentDetails = async (req, res, next) => {
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['body'] = {
            in: 'body',
            description: 'Insert Employment Details',
            required: true,
            schema: {
              "employmentStatusCode":"EMPL",
              "employerName":"",
              "positionCode":"PSN06",
              "natureOfBusinessCode":"BSNS02",
              "countryOfTaxResidence":"MY"
            }
          }
    */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    await onboardingService
    .insertEmploymentDetails(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  };
  const insertFinancialDetails = async (req, res, next) => {
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['body'] = {
            in: 'body',
            description: 'Insert Employment Details',
            required: true,
            schema: {
              "transactionPurposeCode":"",
              "totalMonthlyTransactionCode":"",
              "freqMonthlyTransactionCode":"",
              "sourceOfFundCode":"",
              "otherSourceOfFund":null,
              "sourceOfWealthCode":"",
              "otherSourceOfWealth":null
            }
          }
    */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    await onboardingService
    .insertFinancialDetails(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  };

  const createFatcaCrsDeclaration = async (req, res, next) => {
          /*  
          #swagger.security = [{
                 "bearerAuth": []
          }] 
          #swagger.parameters['body'] = {
            in: 'body',
            description: 'FATCA & CRS Declaration',
            required: true,
            schema: {
              "notUSCitizen":true,
              "notUSGreenCardHolder":true,
              "notUSBorn":true,
              "malaysiaTaxResidence":true
            }
          }
          */ 
    
    const requestBody = req.body;
    const requestHeaders = req.headers;
    await onboardingService
    .createFatcaCrsDeclaration(requestBody,requestHeaders)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  };

module.exports = {
  
  getInvestorType,
  getIdentificationType,
  getCountry,
  getStateList,
  getSecurityQuestion,
  getBankList,
  getEmploymentStatusList,
  getJobPositionType,
  getBusinessType,
  getTransactionPurposeType,
  getTotalMonthlyTransList,
  getFreqMonthlyTransList,
  getSourceOfFundType,
  getSourceOfWealthType,

  registerIndividual,
  generateOTP,
  verifyOTP,
  createSecurityQuestion,

  verifyEmail,
  redirectVerifyEmail,
 
  getPostCodeInfo,

  kycValidate,
  firstPartyChecking,
  tier1KycChecking,
  tier2KycChecking,
  tier3KYCChecking,
  updateTier,

  login,
  refreshToken,
  getName,
  getUserDetails,

  getShippingAddress,
  createShippingAddress,
  updateShippingAddress,

  getBankDetails,
  createBankDetails,
  deleteBankDetails,
  closeUserAccount,

  changePassword,
  validatePassword,
  validateReferralCode,
  securityQuestion,
  generateResetPassword,
  resetPassword,
  createPin,
  changePin,
  verifyPin,
  disablePin,
  resetPin,

  addDeviceID,
  updateNotificationPermission,

  createProfilePicture,
  getFinancialDetails,
  getEmploymentDetails,
  getContactDetails,
  getPersonalDetails,
  updateProfileDetails,
  updatePersonalDetails,
  updateContactDetails,
  insertEmploymentDetails,
  insertFinancialDetails,
  ambankEnquiryFunction,

  ctosVerification,
  createCtosVerification,
  getCtosVerificationResult,

  ctosAuthenticateSIT,
  getCtosVerificationRecordSIT,
  createCtosVerificationSIT,
  updateCtosVerificationSIT,

  createFatcaCrsDeclaration
};
