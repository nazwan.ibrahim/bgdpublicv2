// const formidable = require("formidable");
const administrationService = require("../services/administration");
const sessionController = require("../controllers/session");
const walletService = require("../services/wallet")
const tradingService = require("../services/trading");
const onboardingService = require("../services/onboarding");

const getInvestorType = async (req, res, next) => {
  // #swagger.tags = ['Administration']

  const requestBody = req.body;

  await administrationService
    .getInvestorType(requestBody)
    .then((response) => {
      console.log("*********Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("********* Error : ", error);
      res.status(401).send(error);
      return error;
    });
};

const getPrice = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 

  const requestBody = req.body;
  const requestHeaders = req.headers;

  
  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await administrationService
  .getPrice(requestBody,requestHeaders)
  .then((response) => {
    console.log("************ Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("************Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const getFee = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 

  const requestBody = req.body;
  const requestHeaders = req.headers;

  
  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await administrationService
  .getFee(requestBody,requestHeaders)
  .then((response) => {
    console.log("************ Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const getBursaRevenue = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  
  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }


  await administrationService
  .getBursaRevenue(requestBody,requestHeaders,requestparams)
  .then((response) => {
    console.log("************ Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const getBursaCostRecovery = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  
  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }


  await administrationService
  .getBursaCostRecovery(requestBody,requestHeaders,requestparams)
  .then((response) => {
    console.log("************ Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const getTierManagement = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['code'] = {
          in: 'query',
          required: true,
          schema: 'T03'
      }
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  
  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }


  await administrationService
  .getTierManagement(requestBody,requestHeaders,requestparams)
  .then((response) => {
    console.log("************ Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const initTransfer = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
    #swagger.parameters['acctNumberSender'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
        #swagger.parameters['acctNumberReceiver'] = {
                            in: 'query',
                            required: true,
                            schema: ''
        }
        #swagger.parameters['amount'] = {
                                  in: 'query',
                                  required: true,
                                  schema: '2'
              }
        #swagger.parameters['statusCode'] = {
                            in: 'query',
                            required: true,
                            schema: 'G05'
        }
        #swagger.parameters['reason'] = {
                            in: 'query',
                            required: true,
                            schema: 'test'
        }

*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await administrationService
  .initTransfer(requestBody,requestHeaders,requestparams)
  .then((response) => {
    console.log("*********** Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("*********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};
const initTransferV2 = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['acctNumberSender'] = {
                                    in: 'query',
                                    required: true,
                                    schema: ''
                }
          #swagger.parameters['acctNumberReceiver'] = {
                              in: 'query',
                              required: true,
                              schema: ''
          }
          #swagger.parameters['amount'] = {
                                    in: 'query',
                                    required: true,
                                    schema: '2'
                }
          #swagger.parameters['statusCode'] = {
                              in: 'query',
                              required: true,
                              schema: 'G05'
          }
          #swagger.parameters['reason'] = {
                              in: 'query',
                              required: true,
                              schema: 'test'
          }
  
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;
  
    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    const initTransferResponse=await administrationService
      .initTransfer(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        // res.status(200).send(response.data);
        return response;
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        // res.status(error.response.status).send(error.response.data);
        return error.response;
      });

    console.log("STATUS: "+ initTransferResponse.status )
    console.log("DATA: ",initTransferResponse.data);

    if(!initTransferResponse || initTransferResponse.status!=200){
      res.status(400).send(initTransferResponse.data || "Error when during initTransfer admin service")
      return; 
    }

    const preTransferResponse = await walletService
    .preTransfer(initTransferResponse.data,requestHeaders)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        initTransferResponse.data[0].referenceId=response.data.reference;
        res.status(200).send(initTransferResponse.data);
        return 'SUCCESS';
      })
      .catch((error) => {
        console.error("*********** Error : ", error.response);
        res.status(error.response.status).send(error.response.data);
        return 'FAIL';
      });

          
};

const getTransferMotive = async(req, res, next) =>{
/*  
    #swagger.security = [{
               "bearerAuth": []
        }] 
*/ 
  const requestBody = req.body;
  const requestHeaders = req.headers;
  const requestparams= req.query;

  const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
  if(session.status==false){
    console.error("Session Expired");
    res.status(401).send("Session Expired");
    return;
  }

  await administrationService
  .getTransferMotive(requestBody,requestHeaders,requestparams)
  .then((response) => {
    console.log("*********** Response : ", response.data);
    res.status(200).send(response.data);
  })
  .catch((error) => {
    console.error("*********** Error : ", error);
    res.status(error.response.status).send(error.response.data);
  });

};

const getRedeemMotive = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await administrationService
    .getRedeemMotive(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      const sorted = response.data.sort((a, b) => a.name.localeCompare(b.name, undefined, { sensitivity: 'base' }));
      res.status(200).send(sorted);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };

const initRedeem = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                 "bearerAuth": []
          }] 
      #swagger.parameters['acctNumber'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
        #swagger.parameters['unit'] = {
                            in: 'query',
                            required: true,
                            schema: '2'
        }
        #swagger.parameters['name'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
        #swagger.parameters['phone'] = {
                            in: 'query',
                            required: true,
                            schema: ''
        }
        #swagger.parameters['address'] = {
                            in: 'query',
                            required: true,
                            schema: 'jalan 14'
        }
        #swagger.parameters['address2'] = {
                            in: 'query',
                            required: true,
                            schema: 'kg hulu bendul'
        }
        #swagger.parameters['postcode'] = {
                            in: 'query',
                            required: true,
                            schema: '54200'
        }
        #swagger.parameters['city'] = {
                            in: 'query',
                            required: true,
                            schema: 'setiawangsa'
        }
        #swagger.parameters['state'] = {
                            in: 'query',
                            required: true,
                            schema: 'Kuala Lumpur'
        }
        #swagger.parameters['remarks'] = {
                            in: 'query',
                            required: true,
                            schema: 'test'
        }
    
  */
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await administrationService
    .initRedeem(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };
const initRedeemV2 = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['acctNumber'] = {
                                  in: 'query',
                                  required: true,
                                  schema: ''
              }
        #swagger.parameters['unit'] = {
                            in: 'query',
                            required: true,
                            schema: '2'
        }
        #swagger.parameters['name'] = {
                                  in: 'query',
                                  required: true,
                                  schema: 'najib'
              }
        #swagger.parameters['phone'] = {
                            in: 'query',
                            required: true,
                            schema: ''
        }
        #swagger.parameters['address'] = {
                            in: 'query',
                            required: true,
                            schema: 'jalan 14'
        }
        #swagger.parameters['address2'] = {
                            in: 'query',
                            required: true,
                            schema: 'kg hulu bendul'
        }
        #swagger.parameters['postcode'] = {
                            in: 'query',
                            required: true,
                            schema: '54200'
        }
        #swagger.parameters['city'] = {
                            in: 'query',
                            required: true,
                            schema: 'setiawangsa'
        }
        #swagger.parameters['state'] = {
                            in: 'query',
                            required: true,
                            schema: 'Kuala Lumpur'
        }
        #swagger.parameters['remarks'] = {
                            in: 'query',
                            required: true,
                            schema: 'test'
        }
    
  */
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    const initRedeemResponse= await administrationService
      .initRedeem(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        // res.status(200).send(response.data);
        return response;
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        // res.status(error.response.status).send(error.response.data);
        return error.response;
      });

    console.log("STATUS: "+ initRedeemResponse.status )
    console.log("DATA: ",initRedeemResponse.data);

    if(!initRedeemResponse || initRedeemResponse.status!=200){
      res.status(400).send(initRedeemResponse.data || "Error when during initRedeem admin service")
      return; 
    }

    const preRedeemResponse = await walletService
        .preRedeem(initRedeemResponse.data,requestHeaders)
          .then((response) => {
            console.log("*********** Response : ", response.data);
            initRedeemResponse.data[0].referenceId=response.data.reference;
            res.status(200).send(initRedeemResponse.data);
            return 'SUCCESS';
          })
          .catch((error) => {
            console.error("*********** Error : ", error.response);
            res.status(error.response.status).send(error.response.data);
            return 'FAIL';
          });
  
  };
const initPurchase = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['totalPrice'] = {
                                in: 'query',
                                required: true,
                                schema: '200'
            }
      #swagger.parameters['quantity'] = {
                          in: 'query',
                          required: true,
                          schema: '1'
      }
      #swagger.parameters['inputType'] = {
                          in: 'query',
                          required: true,
                          schema: 'S22'
      }
      #swagger.parameters['reference'] = {
                          in: 'query',
                          required: true,
                          schema: ''
      }
    

  */
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }

    console.log("********** Init Purchase : ", requestBody);
    console.log("********** Init Purchase : ", requestHeaders);
  
    const initPurchaseReq= await administrationService
    .initPurchase(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      // res.status(200).send(response.data);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      // res.status(error.response.status).send(error.response.data);
      return error.response;
    });



  // var type="";
  // if(requestparams.type=="S05"){
  //   type="F05";
  // }
  // else{
  //   type="F06";
  // }
  // console.log("TYPE:",type)
  
  console.log("response: ",initPurchaseReq.data);
  console.log("status: ",initPurchaseReq.status);

  

  if(!initPurchaseReq || initPurchaseReq.status!=200){
    res.status(500).send("Internal error: "+initPurchaseReq.data);
    return;
  }

  initPurchaseReq.data[0].reference=requestparams.reference;

  // initPurchaseReq.data[0].tradingFeeList= initPurchaseReq.data[0].feesForms

  // delete initPurchaseReq.data[0].feesForms;

  try{
    console.log("Step 1 : Calling api - purchase (trade engine)")
    console.log("###########  puchase REQUEST BODY :",initPurchaseReq.data[0]);
    var purchaseLogic =  await tradingService
    .purchase(initPurchaseReq.data[0], requestHeaders)
    .then((response) => {
      console.log("********* PURCHASE Response : ", response.data);
      return response;
    });

    if(!purchaseLogic){
      res.status(500).send("Internal error");
      return;
    }

    console.log("Step 2 : Calling api - createTransaction (wallet)")
    console.log("########### createTransaction REQUEST BODY : ",purchaseLogic.data);
    var createTransactions = await walletService
    .createTransactions(purchaseLogic.data, requestHeaders)
    .then((response) => {
      console.log("**********createTransaction Response : ", response);
      return response;
    });

  } catch(error) {
    console.error("*************** ERROR >: ", error);
    res.status(error.response.status).send(error.response);
  }
  console.log("RESPONSE FROM CREATE TRANSACTION: ",createTransactions)
  if(!createTransactions || createTransactions.status!="G01"){
    res.status(400).send(createTransactions)
    return;
  }
  res.status(200).send(initPurchaseReq.data)
  return;

};

const initTopup = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['acctNumber'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['amount'] = {
                          in: 'query',
                          required: true,
                          schema: '55.00'
      }
      #swagger.parameters['transactionTypeCode'] = {
                                in: 'query',
                                required: true,
                                schema: 'F01'
            }
      #swagger.parameters['walletTypeCode'] = {
                          in: 'query',
                          required: true,
                          schema: 'E01'
      }
      #swagger.parameters['channelCode'] = {
                          in: 'query',
                          required: true,
                          schema: 'BW'
      }
      #swagger.parameters['currency'] = {
                          in: 'query',
                          required: true,
                          schema: 'MYR'
      }
      #swagger.parameters['customerName'] = {
                          in: 'query',
                          required: true,
                          schema: ''
      }

  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    await administrationService
    .initTopup(requestBody,requestHeaders,requestparams)
    .then((response) => {
      console.log("*********** Response : ", response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      res.status(error.response.status).send(error.response.data);
    });
  
  };
const initTopupV2 = async(req, res, next) =>{
  /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['acctNumber'] = {
                                in: 'query',
                                required: true,
                                schema: ''
            }
      #swagger.parameters['amount'] = {
                          in: 'query',
                          required: true,
                          schema: '55.00'
      }
      #swagger.parameters['transactionTypeCode'] = {
                                in: 'query',
                                required: true,
                                schema: 'F01'
            }
      #swagger.parameters['transactionMethodCode'] = {
                                in: 'query',
                                required: true,
                                schema: 'H01'
            }
      #swagger.parameters['walletTypeCode'] = {
                          in: 'query',
                          required: true,
                          schema: 'E01'
      }
      #swagger.parameters['channelCode'] = {
                          in: 'query',
                          required: true,
                          schema: 'BW'
      }
      #swagger.parameters['currency'] = {
                          in: 'query',
                          required: true,
                          schema: 'MYR'
      }
      #swagger.parameters['customerName'] = {
                          in: 'query',
                          required: true,
                          schema: ''
      }

  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;

    const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
    if(session.status==false){
      console.error("Session Expired");
      res.status(401).send("Session Expired");
      return;
    }
  
    const initTopupResponse = await administrationService
      .initTopup(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        // res.status(200).send(response.data);
        return response;
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        // res.status(error.response.status).send(error.response.data);
        return error.response;
      });

    console.log("STATUS: "+ initTopupResponse.status )
    console.log("DATA: ",initTopupResponse.data);

    if(!initTopupResponse || initTopupResponse.status!=200){
      res.status(400).send(initTopupResponse.data || "Error when during initTopup admin service")
      return; 
    }

    initTopupResponse.data[0].transactionMethodCode = requestparams.transactionMethodCode;
    console.log("Pre Topup Req: ",initTopupResponse.data);

    const preRedeemResponse = await walletService
        .preTopup(initTopupResponse.data,requestHeaders)
          .then((response) => {
            console.log("*********** Response : ", response.data);
            initTopupResponse.data[0].referenceId=response.data.reference;
            res.status(200).send(initTopupResponse.data);
            return 'SUCCESS';
          })
          .catch((error) => {
            console.error("*********** Error : ", error.response);
            res.status(error.response.status).send(error.response.data);
            return 'FAIL';
          });
  
  
  };


  const initWithdraw = async(req, res, next) =>{
      /*  
          #swagger.security = [{
                     "bearerAuth": []
              }] 
          /*  
          #swagger.parameters['acctNumber'] = {
                                    in: 'query',
                                    required: true,
                                    schema: ''
                }
          #swagger.parameters['amount'] = {
                              in: 'query',
                              required: true,
                              schema: '55.00'
          }
          #swagger.parameters['transactionTypeCode'] = {
                                    in: 'query',
                                    required: true,
                                    schema: 'F02'
                }
          #swagger.parameters['walletTypeCode'] = {
                              in: 'query',
                              required: true,
                              schema: 'E01'
          }
      */ 
      
        const requestBody = req.body;
        const requestHeaders = req.headers;
        const requestparams= req.query;

        const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
        if(session.status==false){
          console.error("Session Expired");
          res.status(401).send("Session Expired");
          return;
        }
      
        const initWithdrawReq = await administrationService
            .initWithdraw(requestBody,requestHeaders,requestparams)
            .then((response) => {
              console.log("*********** Response : ", response.data);
              return response;
              // res.status(200).send(response.data);
            })
            .catch((error) => {
              console.error("*********** Error : ", error.response);
              return error.response;
            });

        console.log("STATUS: "+ initWithdrawReq.status )
        console.log("DATA: ",initWithdrawReq.data);


        const getBankDetailResponse = await onboardingService
        .getBankDetails(requestBody,requestHeaders)
        .then((response) => {
          console.log("**********Get Bank Detail Response : ", response.data);
          return response.data
        })
        .catch((error) => {
          console.error("******* Error : ", error.response);
          return;
        });


        if(!initWithdrawReq || initWithdrawReq.status!=200){
          res.status(400).send(initWithdrawReq.data)
          return; 
        }

        initWithdrawReq.data[0].transactionMethodCode='H01';
        initWithdrawReq.data[0].statusCode='G05';

        if(!getBankDetailResponse || getBankDetailResponse.length<=0){
          res.status(500).send("Internal Error: Cannot getBankDetails")
          return;
        }

        initWithdrawReq.data[0].investorBankName=getBankDetailResponse[0].bankName;
        initWithdrawReq.data[0].investorBankAccountNo = getBankDetailResponse[0].accountNumber;
        initWithdrawReq.data[0].investorBankBic= getBankDetailResponse[0].bicCode;


        const preWithdrawRequest = await walletService
        .preWithdraw(initWithdrawReq.data,requestHeaders)
          .then((response) => {
            console.log("***********Pre Withdraw Response : ", response);
            initWithdrawReq.data[0].referenceId=response.reference;
            res.status(200).send(initWithdrawReq.data);
            return 'SUCCESS';
          })
          .catch((error) => {
            console.error("***********Pre Withdraw Error : ", error);
            res.status(error.response).send(error.response);
            return 'FAIL';
          });

      };

  const getPriceGraph = async(req, res, next) =>{
        /*  
           #swagger.parameters['days'] = {
                                    in: 'query',
                                    required: true,
                                    schema: '30'
                }
        */ 
        
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;
        
    await administrationService
      .getPriceGraph(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
        
  };

  const tierThresholdCheck = async(req, res, next) =>{
    /*  
        #swagger.security = [{
                   "bearerAuth": []
            }] 
        #swagger.parameters['code'] = {
                                    in: 'query',
                                    required: true,
                                    schema: 'F01'
                }
          #swagger.parameters['account'] = {
                              in: 'query',
                              required: true,
                              schema: ''
          }
          #swagger.parameters['newAmount'] = {
                                    in: 'query',
                                    required: true,
                                    schema: '10.2'
                }
      
    */
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;

      const session = await sessionController.sessionValidation(requestHeaders.authorization.split(" ")[1]);
      if(session.status==false){
        console.error("Session Expired");
        res.status(401).send("Session Expired");
        return;
      }
    
      await administrationService
      .tierThresholdCheck(requestBody,requestHeaders,requestparams)
      .then((response) => {
        console.log("*********** Response : ", response.data);
        res.status(200).send(response.data);
      })
      .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
      });
    
    };

module.exports = {
    getInvestorType,
    getFee,
    getPrice,
    getBursaRevenue,
    getBursaCostRecovery,
    getTierManagement,
    initTransfer,
    getTransferMotive,
    getRedeemMotive,
    initRedeem,
    initPurchase,
    initTopup,
    initWithdraw,
    getPriceGraph,
    tierThresholdCheck,

    initRedeemV2,
    initTopupV2,
    initTransferV2
    

  };