

const administrationService = require("../services/administration");
const sessionController = require("../controllers/session");
const walletService = require("../services/wallet")
const tradingService = require("../services/trading");
const rewardService = require("../services/reward");

const validateCode = async(req, res, next) =>{
    /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['code'] = {
                                in: 'query',
                                required: true,
                                schema: 'testcode'
            }
  */ 
      const requestBody = req.body;
      const requestHeaders = req.headers;
      const requestparams= req.query;
  
      console.log("Request Params : ", requestparams);
    
      const validateCode = await rewardService
        .validateCode(requestparams, requestBody, requestHeaders)
        .then((response) => {
            console.log("*********** Response : ", response);
            res.status(200).send(response.data);
        })
        .catch((error) => {
            console.error("*********** Error : ", error);
            res.status(error.response.status).send(error.response.data);
        });
    
  };

const participant = async(req, res, next) =>{
/*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['body'] = {
                                in: 'body',
                                required: true,
                                schema: {
                                            "rewardCode": "test01",
                                            "userID": "uuid"
                                        }
            }
  */ 
    const requestBody = req.body;
    const requestHeaders = req.headers;
    const requestparams= req.query;


    // console.log("Request Params : ", requestparams);

    const participantResponse = await rewardService
        .participant(requestparams, requestBody, requestHeaders)
        .then((response) => {
        console.log("*********** Response : ", response);
        res.status(200).send(response.data);
        })
        .catch((error) => {
        console.error("*********** Error : ", error);
        res.status(error.response.status).send(error.response.data);
        });

};

const trackRewards = async(req, res, next) =>{
    /*  
      #swagger.security = [{
                  "bearerAuth": []
          }] 
      #swagger.parameters['userID'] = {
                                in: 'query',
                                required: true,
                                schema: 'uuid'
            }
  */ 
        const requestBody = req.body;
        const requestHeaders = req.headers;
        const requestparams= req.query;
    
    
        console.log("Request Params : ", requestparams);
    
        const trackRewardsResponse = await rewardService
            .trackRewards(requestparams, requestBody, requestHeaders)
            .then((response) => {
            console.log("*********** Response : ", response);
            res.status(200).send(response.data);
            })
            .catch((error) => {
            console.error("*********** Error : ", error);
            res.status(error.response.status).send(error.response.data);
            });
    
    };


    module.exports={
        trackRewards,
        participant,
        validateCode
    }
