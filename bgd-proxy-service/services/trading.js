const axios = require("axios");
const chalk = require('chalk');
const FormData = require("form-data");
const fs = require("fs");



  //////////////////////////////// NOT DONE //////////////////////////////////
const purchase = async(requestBody,requestHeaders) =>{
    //header bearer token

    // console.log("************ Entering Purchase : ", requestBody);

    var config = {
      method: 'post',
      url: process.env.tradingUrl + "/trade_engine/primaryTrading/purchase",
      data: requestBody
    };
  
    return await axios(config)
    .then(function (response) {
      // console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      // console.log("********************************",error.response.data);
      throw error;
    });
};

const purchaseInit = async(requestBody,requestHeaders) =>{
  //header bearer token

  // console.log("************ Entering Purchase : ", requestBody);

  var config = {
    method: 'get',
    url: process.env.tradingUrl + "/trade_engine/primaryTrading/purchaseInit",
    data: requestBody
  };

  return await axios(config)
  .then(function (response) {
    // console.log(JSON.stringify(response.data));
    return response;
  })
  .catch(function (error) {
    // console.log("********************************",error.response.data);
    throw error;
  });
};

//////////////////////////////// NOT DONE //////////////////////////////////
const aceSpot = async(requestBody,requestHeaders) =>{
    //header bearer token

    return {
        data:{
            "version": "1.0m",
            "action_requested": "spot_acebuy",
            "product_requested":  "DG-999-9",
            "confirmation_price": requestBody.total_price,
            "status": 1,
            "order_id": "MBI_12345678",
            "weight": requestBody.weight,
            "amount": requestBody.weight*requestBody.total_price,
            "error": {
                "id": 10000,
                "point": "goldweight",
                "msg": "goldweight - invalid parameter"
            },
            "Timestamp": "30/01/2020  13:02:08"
        }
    }

    var config = {
        method: 'post',
        url: baseUrl + "/wallet/wallet/walletWithdraw",
        headers: requestHeaders,
        data: requestBody
    };
    
    return await axios(config)
    .then(function (response) {
        console.log(JSON.stringify(response.data));
        return response;
    })
    .catch(function (error) {
        console.log("********************************",error.response.data);
        throw error;
    });
};



  
  module.exports= {
    purchase,
    purchaseInit,
    aceSpot

  }