const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");
const qs = require("qs");

const { json } = require("body-parser");

////////////////////////////TESTED/////////////////////////
const generateOTP = async (requestBody) => {
  // return dummyData.generateOTP;
  return await axios
    .post(
      process.env.onBoardingUrl + "/registration/general/generateOTP",
      requestBody
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

//////////////////////////// TESTED /////////////////////////
const verifyOTP = async (requestBody) => {
  return await axios
    .post(
      process.env.onBoardingUrl + "/registration/general/verifyOTP",
      requestBody
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

////////////////////////////TESTED/////////////////////////
const getInvestorType = async (requestBody) => {
  return await axios
    .get(process.env.onBoardingUrl + "/registration/rest/entities/UserType")
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

//////////////////////////// TESTED /////////////////////////
const getIdentificationType = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/IdentificationType"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

////////////////////////////TESTED/////////////////////////
const getCountryList = async (requestBody) => {
  console.log("ONBOARDING URL : ",process.env.onBoardingUrl)
  return await axios
    .get(process.env.onBoardingUrl + "/registration/general/country")
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};


///////////////////////////////// NOT DONE /////////////////////////////
const getStateList = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["countryCode"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/general/state"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

////////////////////////////NOT DONE/////////////////////////
const registerIndividual = async (requestBody) => {
  // return dummyDataRes.registerIndividual;

  return await axios
    .post(
      process.env.onBoardingUrl + "/registration/register/individual",
      requestBody
    )
    .then((response) => {
      // console.log("********** Response : ", response.data);
      return response;
    })

    .catch((error) => {
      // console.error("*********** Error : ", error);
      throw error;
    });
};

////////////////////////////NOT DONE/////////////////////////
const createPassword = async (requestBody) => {
  // return  dummyDataReq.createPassword;

  return await axios
    .post(
      process.env.onBoardingUrl + "/registration/password/createPassword",
      requestBody
    )
    .then((response) => {
      // console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      // console.error("*********** Error : ", error);
      throw error;
    });
};

////////////////////////////TESTED/////////////////////////
const getSecurityQuestion = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl + "/registration/rest/entities/SecurityQuestion"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

////////////////////////////TESTED/////////////////////////
const createSecurityQuestion = async (requestBody) => {
  return await axios
    .post(
      process.env.onBoardingUrl + "/registration/general/securityQuestion",
      requestBody
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

/* ************************  SUB MODULE : ONBOARDING *****************************/

////////////////////////////NOT DONE (DUPLICATED with getidentificattion type)/////////////////////////
const getIdType = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/IdentificationType"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

const getEmploymentStatusList = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/EmploymentStatus?sort=sorting"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

const getJobPositionType = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/JobPosition?sort=code"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

const getBusinessType = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/NatureOfBusiness?sort=name"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

const getTransactionPurposeType = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/TransactionPurposeType?sort=code"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

const getTotalMonthlyTransList = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/TotalMonthlyTransaction?sort=code"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const getFreqMonthlyTransList = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/FreqMonthlyTransaction/?sort=minTransFreq"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const getSourceOfFundType = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/SourceOfFundType?sort=code"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const getSourceOfWealthType = async (requestBody) => {
  return await axios
    .get(
      process.env.onBoardingUrl +
        "/registration/rest/entities/SourceOfWealthType?sort=code"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

////////////////////////////TESTED/////////////////////////
const kycValidate = async (requestBody) => {
  var data = new FormData();
  data.append("face", fs.createReadStream(requestBody.face));
  data.append("id", fs.createReadStream(requestBody.id));

  var config = {
    method: "post",
    url: process.env.rekognitionUrl + "/api/rekognition/compare_faces",
    headers: {
      ...data.getHeaders(),
    },
    data: data,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log(error);
      throw error;
    });
};

const kycValidate2 = async (requestBody) => {
  var data = new FormData();
  data.append("faceImg", fs.createReadStream(requestBody.faceImg));
  data.append("documentImgFront", fs.createReadStream(requestBody.documentImgFront));
  data.append("documentImgBack",fs.createReadStream(requestBody.documentImgBack));
  data.append('userID', requestBody.userID);
  data.append('kycType', requestBody.kycType);
  if(requestBody.applicationID){
    data.append('applicationID',requestBody.applicationID);
  }
  
  var config = {
    method: "post",
    url: process.env.rekognitionUrl + "/api/rekognition/KYC",
    headers: {
      ...data.getHeaders(),
    },
    data: data,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log(error);
      throw error;
    });
};


////////////////////////////NOT DONE/////////////////////////
const firstPartyChecking = async (requestBody) => {
  return await axios
    .post(
      process.env.rekognitionUrl +
        "/api/rekognition/compare_faces/registration/general/securityQuestion"
    )
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

/////////////////////////DONE NOT TESTED (RETURN DUMMY) sbb xda duit/////////////////////////
const tier2KycChecking = async (requestBody) => {

  return {
    // data: dummyDataRes.tier2KycChecking
    data: "mvp2"
  }

  return await axios
    .post(process.env.onBoardingUrl + "/registration/kycValidation")
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

//////////////////////////NOT DONE/////////////////////////
const tier3KYCChecking = async (requestBody) => {
  return await axios
    .post(process.env.onBoardingUrl + "/registration/general/securityQuestion")
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

//////////////////////////TESTED/////////////////////////
const updateTier = async (requestBody,requestHeaders) => {

  var myHeaders = new Headers();
  myHeaders.append("Authorization", requestHeaders.authorization);
  myHeaders.append("Content-Type", "application/json");


  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(requestBody),
    redirect: 'follow'
  };

  return await fetch(process.env.onBoardingUrl + "/registration/user/updateTier", requestOptions)
    .then(response => {
      console.log('#SERVICE: Update tier: Status: ', response.status)
      if(response.status==400){
          throw {response: 
                          {data: "Identification Number already existed",
                           status: 400}
                                        }
      }else if(response.status!=200){
        throw {response: 
                  {data: "ERROR DURING UPDATE TIER",
                  status: 500}
                                }
      }
      else{
        return response.json()
        }
      }
    )
    .then(result => {
        console.log("Update Tier Status code : ", result);
        return result
        
        // if(result.ok){
        //   return result.json();
        // } else {
        //   throw result;
        // }
    })



};


/* ************************  SUB MODULE : Login *****************************/


const loginPrevilage = async (requestHeaders, requestBody) => {

  var data = qs.stringify({
    "username": process.env.username_previlage,
    "password": process.env.password_previlage,
    "grant_type": process.env.grant_type_previlage,
    "client_id": process.env.client_id_previlage,
  });

  var config = {
    method: "post",
    url:
      process.env.keycloak +
      "/auth/realms/"+process.env.realms+"/protocol/openid-connect/token",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };

  return await axios(config)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      throw error;
    });
};

const getSSOUserID = async (requestHeaders, requestBody, ssoUserID) => {

  var config = {
    method: "get",
    url: process.env.keycloak +
    "/auth/admin/realms/"+process.env.realms+"/users/"+ssoUserID,
    headers: requestHeaders,
    //params: {"username":requestBody.username},
  };

  return await axios(config)
    .then(function (response) {
      // console.log(response.data);
      return response;
    })
    .catch(function (error) {
      console.error("Error : ", error.data);
      throw error;
    });
};


const killOldSession = async (requestHeaders, requestBody) => {

    var config = {
      method: "post",
      url: process.env.keycloak +
      "/auth/admin/realms/"+process.env.realms+"/users/"+requestBody.user+"/logout",
      headers: requestHeaders,
    };
  
    return await axios(config)
      .then(function (response) {
        console.log("RESPONSE FROM KILLSESSION REQUUEST: ",response.status);
        return response.status;
      })
      .catch(function (error) {
        console.error("Error : ", error.data);
        throw error;
      });
  };

//////////////////////////TESTED/////////////////////////
const login = async (requestHeaders, requestBody) => {
  console.log(process.env.client_id)
  console.log(process.env.client_secret)
  var data = qs.stringify({
    "username": requestBody.username,
    "password": requestBody.password,
    "grant_type": process.env.grant_type,
    "client_id": process.env.client_id,
    "client_secret": process.env.client_secret,
  });

  var config = {
    method: "post",
    url:
      process.env.keycloak +
      "/auth/realms/"+process.env.realms+"/protocol/openid-connect/token",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };

  return await axios(config)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      throw error;
    });
};

const accountStatus = async (requestHeaders, requestBody) => {

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/accountStatus",
    headers: requestHeaders,
    params: {"email":requestBody.username},
  };

  return await axios(config)
    .then(function (response) {
      console.log(response.data);
      return response;
    })
    .catch(function (error) {
      console.error("Error get acountStatus : ", error);
      // console.error("Error : ", error.response.data);
      throw error;
    });
};


///////////////////////////////// TESTED /////////////////////////////
const refreshToken = async (requestHeaders, requestBody) => {

  var data = qs.stringify({
    "grant_type": process.env.grant_type_refresh,
    "client_id": process.env.client_id,
    "client_secret": process.env.client_secret,
    "refresh_token": requestBody.refresh_token
  });
  var config = {
    method: "post",
    url:
      process.env.keycloak +
      "/auth/realms/"+process.env.realms+"/protocol/openid-connect/token",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };

  return await axios(config)
  .then(function (response) {
    console.log("************* Response : ",response);
    return response;
  })
  .catch(function (error) {
    console.log("************* Error : ",error);
    throw error;
  });
};

//////////////////////////TESTED/////////////////////////

const getName = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["username"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    console.log("attributename: "+attributename)
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/name"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const getUserDetails = async (requestBody, requestHeaders) => {
  //header bearer token
  
  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/details",
    headers: requestHeaders,
  };

  return await axios(config)
    .then(function (response) {
      // console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log(error);
      throw error;
    });
};

/* ************************  SUB MODULE : Username Password Retrieval *****************************/

//////////////////////////NOT DONE/////////////////////////
const changePassword = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/password/changePassword",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

////////////////////////// TESTED /////////////////////////
const securityQuestion = async (requestparams, requestHeaders) => {
  //header bearer token
  var allowedParams = ["email"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/general/securityQuestion"+urlParam,
    headers: requestHeaders,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

////////////////////////// TESTED /////////////////////////
const generateResetPassword = async (requestBody, requestHeaders) => {
  //header bearer token
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/password/generateResetPassword",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


///////////////////////////////// NOT DONE /////////////////////////////
const resetPassword = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token


 
  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/password/resetPassword",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const validatePassword = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/password/validatePassword",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const validateReferralCode = async (requestBody, requestHeaders,requestparams) => {
  
  var allowedParams = ["referralCode"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;

  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
  console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/register/validateReferralCode"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
  .then(function (response) {
    console.log(JSON.stringify(response.data));
    return response;
  })
  .catch(function (error) {
    console.log("********************************", error.response.data);
    throw error;
  });
};


/* *********************** User Employment Details *********************************/


/* ************************  SUB MODULE : User Profile *****************************/


///////////////////////////////// TESTED /////////////////////////////
const createShippingAddress = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/address",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


///////////////////////////////// TESTED /////////////////////////////
const getShippingAddress = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/address",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


///////////////////////////////// NOT DONE /////////////////////////////
const updateShippingAddress = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/address",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////TESTED////////////////////////
const createBankDetails = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/bank",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

/////////////////////////////////TESTED /////////////////////////////
const getBankDetails = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/bank",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const closeUserAccount = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token
  var allowedParams = ["userID"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/closeAccount" + urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};
///////////////////////////////// TESTED /////////////////////////////
const deleteBankDetails = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "delete",
    url: process.env.onBoardingUrl + "/registration/user/bank",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};




/* ************************  SUB MODULE : PIN GENERATION  *****************************/

///////////////////////////////// TESTED /////////////////////////////
const createPin = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/pin",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

///////////////////////////////// TESTED/////////////////////////////
const changePin = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/pin",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

///////////////////////////////// TESTED /////////////////////////////
const verifyPin = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/verifyPin",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const resetPin = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token




  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/resetPin",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


///////////////////////////////// TESTED /////////////////////////////
const disablePin = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/disablePin",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

///////////////////////////////// TESTED /////////////////////////////
const getBankList = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/rest/entities/Bank?sort=name",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


///////////////////////////////// NOT DONE /////////////////////////////
const verifyEmail = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["userID","email"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/register/emailVerification"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

///////////////////////////////// NOT DONE /////////////////////////////
const redirectVerifyEmail = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["userID","verificationID"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/register/emailVerification"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

///////////////////////////////// NOT DONE /////////////////////////////
const getPostCodeInfo = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["postcode"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/general/postcodeInfo"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


///////////////////////////////// NOT DONE /////////////////////////////
const addDeviceID = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = []; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/register/deviceID"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};
const updateNotificationPermission = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = []; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/register/permission"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};
/* ************************  SUB MODULE : User Profile & Details *****************************/
const createProfilePicture = async (requestBody,requestHeaders,requestparams) => {

  var allowedParams = []; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
  console.log("UrLPARAM: " ,urlParam);
  
  
  var data = new FormData();
  data.append("image", fs.createReadStream(requestBody.image));
  
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/profilePicture"+urlParam,
    headers: {
      'Authorization': requestHeaders.authorization,
      ...data.getHeaders(),
    },
    data: data,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log(error);
      throw error;
    });
};

const updateProfileDetails = async (requestBody,requestHeaders) => {
  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/profileDetails",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const updatePersonalDetails = async (requestBody,requestHeaders) => {
  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/personalDetails",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const updateContactDetails = async (requestBody,requestHeaders) => {
  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/user/contactDetails",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const insertEmploymentDetails = async (requestBody,requestHeaders) => {
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/employmentDetails",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const insertFinancialDetails = async (requestBody,requestHeaders) => {
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/financialDetails",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const getFinancialDetails = async (requestBody,requestHeaders,requestparams) => {
  //header bearer token
  
  var allowedParams = ["userID"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/financialDetails"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const getEmploymentDetails = async (requestBody,requestHeaders,requestparams) => {
  //header bearer token
  
  var allowedParams = ["userID"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/employmentDetails"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};
const getContactDetails = async (requestBody,requestHeaders,requestparams) => {
  //header bearer token
  
  var allowedParams = ["userID"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/contactDetails"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};
const getPersonalDetails = async (requestBody,requestHeaders,requestparams) => {
  //header bearer token
  
  var allowedParams = ["userID"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/user/personalDetails"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

/**************************  SUB MODULE : SIT ********************************************/
const ctosAuthenticateSIT = async (requestHeaders) => {
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/verification/ctosAuthenticate",
    headers: requestHeaders
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const getCtosVerificationRecordSIT = async (requestBody,requestHeaders,requestparams) => {
  //header bearer token
  
  var allowedParams = ["id"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/verification/ctosVerification"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};
const createCtosVerificationSIT = async (requestBody,requestHeaders) => {
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/verification/ctosVerification_SIT",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};
const updateCtosVerificationSIT = async (requestBody,requestHeaders) => {
  var config = {
    method: "put",
    url: process.env.onBoardingUrl + "/registration/verification/ctosVerification_SIT",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

/* ************************  SUB MODULE : CTOS Verification *****************************/

const ctosVerification = async (requestBody, requestHeaders) => {
  //header bearer token

  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var body = JSON.stringify(requestBody);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: body,
    redirect: 'follow'
  };

  return await fetch(process.env.onBoardingUrl + "/registration/verification/webhookStatus", requestOptions)
    .then(response => {
      return response;
    })
    .catch( error => {
      console.error(error);
      throw error;
    });
};
const getCtosVerificationResult = async (requestBody,requestHeaders,requestparams) => {
  //header bearer token
  
  var allowedParams = ["userID"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.onBoardingUrl + "/registration/verification/ctosVerificationResult"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const createCtosVerification = async (requestBody,requestHeaders) => {
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/verification/ctosVerification",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};

/* ************************  SUB MODULE : FATCA & CRS *****************************/

const createFatcaCrsDeclaration = async (requestBody,requestHeaders) => {
  var config = {
    method: "post",
    url: process.env.onBoardingUrl + "/registration/user/fatcaCrsDeclaration",
    headers: requestHeaders,
    data: requestBody,
  };
  return await axios(config)
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      throw error;
    });
};


module.exports = {
  generateOTP,
  verifyOTP,
  getInvestorType,
  getIdentificationType,
  getEmploymentStatusList,
  getJobPositionType,
  getBusinessType,
  getCountryList,
  getStateList,
  getTransactionPurposeType,
  getTotalMonthlyTransList,
  getFreqMonthlyTransList,
  getSourceOfFundType,
  getSourceOfWealthType,

  registerIndividual,
  createSecurityQuestion,
  getSecurityQuestion,
  createPassword,
  getIdType,
  kycValidate,
  kycValidate2,
  firstPartyChecking,
  tier2KycChecking,
  tier3KYCChecking,
  updateTier,


  login,
  getSSOUserID,
  loginPrevilage,
  killOldSession,

  accountStatus,
  refreshToken,
  getName,
  getUserDetails,
  closeUserAccount,
  changePassword,
  securityQuestion,
  generateResetPassword,
  resetPassword,
  validatePassword,
  validateReferralCode,

  createShippingAddress,
  getShippingAddress,
  updateShippingAddress,


  getBankDetails,
  createBankDetails,
  deleteBankDetails,


  createPin,
  changePin,
  verifyPin,
  disablePin,
  resetPin,

  getBankList,

  getPostCodeInfo,
  verifyEmail,
  redirectVerifyEmail,

  addDeviceID,
  updateNotificationPermission,

  createProfilePicture,
  updateProfileDetails,
  updatePersonalDetails,
  updateContactDetails,
  insertEmploymentDetails,
  insertFinancialDetails,
  getEmploymentDetails,
  getContactDetails,
  getPersonalDetails,
  getFinancialDetails,

  ctosVerification,
  createCtosVerification,
  getCtosVerificationResult,

  createFatcaCrsDeclaration,

  ctosAuthenticateSIT,
  getCtosVerificationRecordSIT,
  createCtosVerificationSIT,
  updateCtosVerificationSIT

};
