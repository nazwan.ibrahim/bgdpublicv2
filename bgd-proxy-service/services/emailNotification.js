const cashTopupEmail = async (referenceNo) => {
      var requestOptions = {
        method: 'POST',
        redirect: 'follow'
      };
      
      return await fetch(process.env.administrationUrl + "/bgd/emailNotification/cashTopup?referenceNo="+referenceNo, requestOptions)
        .then(response => response.text())
        .then(result => result)
        .catch(error => {throw error});
  };

  const cashWithdrawalEmail = async (referenceNo) => {
    var requestOptions = {
      method: 'POST',
      redirect: 'follow'
    };
    
    return await fetch(process.env.administrationUrl + "/bgd/emailNotification/cashWithdrawal?referenceNo="+referenceNo, requestOptions)
      .then(response => response.text())
      .then(result => result)
      .catch(error => {throw error});
};

const goldPurchaseEmail = async (referenceNo) => {
    var requestOptions = {
      method: 'POST',
      redirect: 'follow'
    };
    
    return await fetch(process.env.administrationUrl + "/bgd/emailNotification/goldPurchase?referenceNo="+referenceNo, requestOptions)
      .then(response => response.text())
      .then(result => result)
      .catch(error => {throw error});
};

const goldSaleEmail = async (referenceNo) => {
    var requestOptions = {
      method: 'POST',
      redirect: 'follow'
    };
    
    return await fetch(process.env.administrationUrl + "/bgd/emailNotification/goldSale?referenceNo="+referenceNo, requestOptions)
      .then(response => response.text())
      .then(result => result)
      .catch(error => {throw error});
};

const goldRedeemEmail = async (referenceNo) => {
    var requestOptions = {
      method: 'POST',
      redirect: 'follow'
    };
    
    return await fetch(process.env.administrationUrl + "/bgd/emailNotification/goldRedeem?referenceNo="+referenceNo, requestOptions)
      .then(response => response.text())
      .then(result => result)
      .catch(error => {throw error});
};

const goldTransferEmail = async (referenceNo) => {
    var requestOptions = {
      method: 'POST',
      redirect: 'follow'
    };
    
    return await fetch(process.env.administrationUrl + "/bgd/emailNotification/goldTransfer?referenceNo="+referenceNo, requestOptions)
      .then(response => response.text())
      .then(result => result)
      .catch(error => {throw error});
};

const goldReceiveEmail = async (referenceNo) => {
    var requestOptions = {
      method: 'POST',
      redirect: 'follow'
    };
    
    return await fetch(process.env.administrationUrl + "/bgd/emailNotification/goldReceive?referenceNo="+referenceNo, requestOptions)
      .then(response => response.text())
      .then(result => result)
      .catch(error => {throw error});
};


  module.exports = {
    cashTopupEmail,
    cashWithdrawalEmail,
    goldPurchaseEmail,
    goldSaleEmail,
    goldRedeemEmail,
    goldTransferEmail,
    goldReceiveEmail
 }

