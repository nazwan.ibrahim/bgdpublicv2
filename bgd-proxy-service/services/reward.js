const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");
require("dotenv").config();


const validateCode = async (requestparams, requestBody, requestHeaders) => {

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: process.env.administrationUrl + '/bgd/reward/validateCode',
      params: requestparams,
      headers: requestHeaders
    };
    
    return await axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        // console.error("********************************", error.response.data);
        throw error;
      });
    
};

const participant = async (requestparams, requestBody, requestHeaders) => {

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: process.env.administrationUrl + '/bgd/reward/participant',
      params: requestparams,
      data:requestBody,
      headers: requestHeaders
    };
    
    return await axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        // console.error("********************************", error.response.data);
        throw error;
      });
    
};

const trackRewards = async (requestparams, requestBody, requestHeaders) => {

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: process.env.administrationUrl + '/bgd/reward/trackRewards',
      params: requestparams,
      headers: requestHeaders
    };
    
    return await axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        // console.error("********************************", error.response.data);
        throw error;
      });
    
};




  module.exports={
        trackRewards,
        participant,
        validateCode
  }