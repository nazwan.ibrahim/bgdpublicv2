const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");

const req = require("express/lib/request");
const { url } = require("inspector");

const testnetwork = async () => {
    var config = {
      method: "get",
      url: "https://ifconfig.me",
      // headers: requestHeaders,
      // data: requestBody,
    };
  
    return await axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        console.log("********************************", error.response);
        throw error;
      });
}

module.exports= {
    testnetwork
}