const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");

// const dummyDataRes = require("../dummyData/response");
// const dummyDataReq = require("../dummyData/request");

const getInvestorType = async (requestBody) => {
  return await axios
    .get(process.env.administrationUrl + "/registration/rest/entities/UserType")
    .then((response) => {
      console.log("********** Response : ", response);
      return response;
    })
    .catch((error) => {
      console.error("*********** Error : ", error);
      return error;
    });
};

//////////////////////////////// TESTED  //////////////////////////////////
const getBursaRevenue = async (requestHeaders) => {
  //header bearer token


  var config = {
    method: "get",
    url: process.env.administrationUrl+"/bgd/product/getBursaRevenue",
    // headers: requestHeaders
    // data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


//////////////////////////////// TESTED  //////////////////////////////////
const getBursaCostRecovery = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token


  var config = {
    method: "get",
    url: process.env.administrationUrl+"/bgd/product/getBursaCostRecovery",
    // headers: requestHeaders
    // data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// TESTED  //////////////////////////////////
const getTierManagement = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["code"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }
 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.administrationUrl+"/bgd/product/getTierManagement"+urlParam,
    // headers: requestHeaders
    // data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const getPrice = async(requestBody,requestHeaders) =>{
  //header bearer token


  var config = {
    method: 'get',
    url: process.env.administrationUrl + "/bgd/product/getProductPrice",
    // data: requestBody
  };

  return await axios(config)
  .then(function (response) {
    // console.log(JSON.stringify(response.data));
    return response;
  })
  .catch(function (error) {
    // console.log("********************************",error.response.data);
    throw error;
  });
};

const getFee = async(requestBody,requestHeaders) =>{
  //header bearer token

  // console.log("************ Entering Purchase : ", requestBody);

  return { 
    data:{
      "tradingFee": 2
    }
  }

  var config = {
    method: 'post',
    url: process.env.tradingUrl + "/trade_engine/primary",
    data: requestBody
  };

  return await axios(config)
  .then(function (response) {
    // console.log(JSON.stringify(response.data));
    return response;
  })
  .catch(function (error) {
    // console.log("********************************",error.response.data);
    throw error;
  });
};

//////////////////////////////// NOTDONE  //////////////////////////////////
const initTransfer = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token
  var allowedParams = ["acctNumberSender","acctNumberReceiver","amount","statusCode","reason"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);
  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/wallet/initTransfer"+urlParam,
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// NOTDONE  //////////////////////////////////
const getTransferMotive = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token
  var allowedParams = []; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);
  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/management/getTransferMotive"+urlParam,
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// NOTDONE  //////////////////////////////////
const initRedeem = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["acctNumber","unit","name","phone","address","address2","postcode","city","state","remarks"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);
  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/wallet/initRedeem"+urlParam,
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


//////////////////////////////// NOTDONE  //////////////////////////////////
const initPurchase = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token
  var allowedParams = ["totalPrice","quantity","inputType","reference"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);
  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/wallet/initPurchase"+urlParam,
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// NOTDONE  //////////////////////////////////
const initTopup = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["acctNumber","amount","transactionTypeCode","walletTypeCode","channelCode","currency","customerName"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/wallet/initTopup" +urlParam,
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// NOTDONE  //////////////////////////////////
const initWithdraw = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["acctNumber","amount","transactionTypeCode","walletTypeCode"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/wallet/initWithdraw" +urlParam,
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


//////////////////////////////// NOTDONE  //////////////////////////////////
const getPriceGraph = async (requestBody, requestHeaders,requestparams) => {


  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/management/getPriceGraph",
    // headers: requestHeaders,
    params:requestparams,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      // console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


const tierThresholdCheck = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["code","account","newAmount",]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/wallet/tierThresholdChecking"+urlParam,
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });

  }


  //////////////////////////////// NOTDONE  //////////////////////////////////
const getRedeemMotive = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token
 

  var config = {
    method: "get",
    url: process.env.administrationUrl + "/bgd/management/getRedeemMotive",
    // headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};



module.exports = {
  getInvestorType,
  getPrice,
  getFee,
  getBursaRevenue,
  getBursaCostRecovery,
  getTierManagement,

  getTransferMotive,
  getRedeemMotive,

  initTransfer,
  initRedeem,
  initPurchase,
  initTopup,
  initPurchase,
  initWithdraw,

  getPriceGraph,
  tierThresholdCheck
};
