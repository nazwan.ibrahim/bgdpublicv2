const compare = require('name-comparer').compare;



const honorifics = [
    "Bin",
    "Binti",
    "B",
    "BT",
    "AP",
    "AL",
    "Anak lelaki",
    "Anak perempuan",
    "Mr.",
    "Ms.",
    "Mrs.",
    "Miss",
    "Dr.",
    "Prof.",
    "Ir",
    "Phd",
    "PE",
    "PEng",
    "AR",
    "Sr", //Professional Surveyor
    "PGeol", //Professional Geologist
    "Gs",// geospatial professional
    "Ts", 
    "CA",// certified account
    "Lar",// landscape architect
    "Sir",
    "Lady",
    "Baron",
    "Baroness",
    "Lord",
    "Ladyship",
    "Duke",
    "Duchess",
    "Count",
    "Countess",
    "Viscount",
    "Viscountess",
    "Knight",
    "Dame",
    "Lord Mayor",
    "Madam Mayor",
    "Nim", // korean
    "Haji",
    "Hajjah",
    "Tuan",
    "PUAN",
    "Cik",
    "Encik",
    "Datuk",
    "DATO",
    "Datin",
    "Tan Sri",
    "Tun",
    "Tunku",
    "Ungku",
    "syed",
    "sheikh",
    "En",
    "Pn",
    "Ck",
    "Your Highness",
    "Your Majesty",
    "Your Honor",
    "Yang Amat Berbahagia",
    "Yang Berhormat",
    "Yang Amat Dihormati",
    "Yang Amat Berhormat",
    "Yang Amat Bahagia Dato'",
    "Yang Amat Arif",
    "Yang Amat Mulia Tengku",
    "Yang Mulia Seri",
    "Seri Sultan",
    "Yang Di-Pertuan Agong",
    "Y.A.B. Dato'",
    "Y.A.B.",
    "Y.A.Bhg. Dato'",
    "Y.A.Bhg.",
    "Y.A.D. Dato'",
    "Y.A.D.",
    "Y.A.M. Tengku",
    "Y.A.M.",
    "Y.M. Raja",
    "Y.T.M. Raja",
    "Y.T.M. Tengku",
    "Y.T.M. Tunku",
    "Y.T.M.",
    "YBM",
    "YB",
    "YAA",
    "YH",
    "TYT",
    "YABhg",
    "YAB",
    "YBhg",
    "D.Y.M.M. Sultan",
    "D.Y.M.M. Sultanah",
    "D.Y.M.M. Raja",
    "D.Y.M.M. Raja Permaisuri Agong",
    "D.Y.M.M. Raja Perempuan",
    "D.Y.M.M. Paduka",
    "D.Y.M.M. Paduka Raja",
    "Pengiran",
    "Yang Mulia Pengiran",
    "Yang Amat Mulia Tunku",
    "Yang Teramat Mulia Tunku",
    "Seri Paduka Baginda",
    "Seri Paduka Baginda Yang Di-Pertuan Agong",
    "S.P.B.",
    "D.Y.M.M. Pemangku Raja",
    "Yang Mulia Raja",
    "Yang Mulia Raja Permaisuri Agong",
    "Yang Mulia Raja Perempuan",
    "Yang Teramat Mulia Paduka",
    "Yang Teramat Mulia Paduka Raja",
    "Seri Maharaja",
    "Seri Maharaja Mangku Negara",
    "Yang Teramat Mulia Pemangku Raja",
    "Tun Haji",
    "Tun Dr.",
    "Tun Datuk",
    "Sultan",
    "Sultana",
    "Sultanah",
    "Sri", //indian
    "Sriman",//indian
    "Srimathi",//indian
    "Shri",//indian
    "Shriman",//indian
    "Shrimati",//indian
    "Yang Mulia Sultan",
    "Yang Mulia Sultanah",
    "Yang Mulia Sultan Muda",
    "Yang Mulia Raja Muda",
    "Yang Mulia Tengku Muda",
    "Yang Mulia Tengku Laksamana",
    "Yang Mulia Tengku Temenggong",
    "Yang Mulia Tengku Besar",
    "Xian Sheng", // 先生 (Mr.)
    "Tai Tai", // 太太 (Mrs./Madam)
    "Nü Shi", 
  ];
  
  
  function removeHonorific(name) {
  
    for (const honorific of honorifics) {
      const regex = new RegExp(`^${honorific.replace(/[^a-zA-Z0-9 ]/g, '')}\\s`, 'i');
      name = name.replace(regex, '');
    }
  
    return name;
  }
  


  function nameCompare(name1,name2){
    console.log("Comparing name (Original) => "+ name1 +"  :  "+name2);
  
    name1 = name1.replace(/[^a-zA-Z0-9 ]/g, ''); // remove all character except a-z A-Z 0-9 and space
    name2 = name2.replace(/[^a-zA-Z0-9 ]/g, ''); // remove all character except a-z A-Z 0-9 and space
    console.log("Filter character character => "+ name1 +"  :  "+name2);
  
  
    name1= removeHonorific(name1);
    name2= removeHonorific(name2);
    console.log("Removes honorific => "+ name1 +"  :  "+name2);
  
    const charNameLimitLength = 15;
    name1=name1.substring(0,charNameLimitLength);
    name2=name2.substring(0,charNameLimitLength);
  
  
    console.log("limit to "+charNameLimitLength+" character => "+ name1 +"  :  "+name2);
  
    console.log("Comparing name => "+ name1 +"  :  "+name2);
    result = compare(name1,name2,{transforms:['CI','ABBR','CI_ATONIC','CI_START']});
    console.log("#COMPARISON RESULT# : ", result)
    return result;
  }




  module.exports ={
    nameCompare
  }