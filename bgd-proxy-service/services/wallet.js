const axios = require("axios");
const FormData = require("form-data");
const fs = require("fs");
require("dotenv").config();

// const dummyDataRes = require("../dummyData/response");
// const dummyDataReq = require("../dummyData/request");
const req = require("express/lib/request");
const { url } = require("inspector");


//////////////////////////////// TESTED //////////////////////////////////
const getWalletDetails = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/wallet/walletByUser",
    headers: requestHeaders,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log(error);
      throw error;
    });
};

//////////////////////////////// TESTED //////////////////////////////////
const createWallet = async (requestBody, requestHeaders,callFrom) => {
  //header bearer token

  if(callFrom == 'tier1KycChecking' || callFrom=='functionCall'){
        console.log("CREATE WALLET WAS CALLED BY ONBOARDING SERVICE");
        var myHeaders = new Headers();
        myHeaders.append("Authorization", requestHeaders.authorization);
        myHeaders.append("Content-Type", "application/json");

        // console.log("##############MY HEADER: ",myHeaders);

        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: JSON.stringify(requestBody),
          redirect: 'follow'
        };


        return await fetch(process.env.walletUrl+ "/wallet/wallet/createWallet", requestOptions)
          .then(response => response.text())
          .then(result =>{
            return JSON.parse(result);
          })
          .catch(error => {
            console.log('error', error);
            throw error;
        });
  }
  else if(callFrom == 'createWallet' ){
      console.log("CREATE WALLET WAS CALLED BY WALLET SERVICE");
      var config = {
        method: "post",
        url: process.env.walletUrl + "/wallet/wallet/createWallet",
        headers: requestHeaders,
        data: requestBody,
      };

      return await axios(config)
        .then(function (response) {
          console.log(JSON.stringify(response.data));
          return response;
        })
        .catch(function (error) {
          console.log("********************************", error.response.data);
          throw error;
        });
  };

}

//////////////////////////////// TESTED //////////////////////////////////
const topupwallet = async (requestBody, requestHeaders) => {
  //header bearer token

  var myHeaders = new Headers();
        myHeaders.append("Authorization", requestHeaders.authorization);
        myHeaders.append("Content-Type", "application/json");

        // console.log("##############MY HEADER: ",myHeaders);

        var requestOptions = {
          method: 'PUT',
          headers: myHeaders,
          body: JSON.stringify(requestBody),
          redirect: 'follow'
        };


        return await fetch(process.env.walletUrl+ "/wallet/wallet/walletTopup", requestOptions)
          .then(response => response.text())
          .then(result =>{
            console.log("RESULT: ",JSON.parse(result))
            return JSON.parse(result);
          })
          .catch(error => {
            console.log('error', error);
            throw error;
        });

  // return;
  // var config = {
  //   method: "put",
  //   url: process.env.walletUrl + "/wallet/wallet/walletTopup",
  //   headers: requestHeaders,
  //   data: requestBody,
  // };

  // return await axios(config)
  //   .then(function (response) {
  //     console.log(JSON.stringify(response.data));
  //     return response;
  //   })
  //   .catch(function (error) {
  //     console.log("********************************", error.response.data);
  //     throw error;
  //   });
};

//////////////////////////////// DUMMY //////////////////////////////////
const ambankTopup = async (requestBody, requestHeaders) => {
  //header bearer token

  return {
    data: {
      msg: "successful",
    },
  };

  var config = {
    method: "put",
    url: process.env.walletUrl + "/wallet/wallet/walletTopup",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


//////////////////////////////// TESTED //////////////////////////////////
const preWithdraw = async (requestBody, requestHeaders) => {
  //header bearer token


  var myHeaders = new Headers();
  myHeaders.append("Authorization", requestHeaders.authorization);
  myHeaders.append("Content-Type", "application/json");

  // console.log("##############MY HEADER: ",myHeaders);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(requestBody),
    redirect: 'follow'
  };


  return await fetch(process.env.walletUrl+ "/wallet/wallet/preWithdraw", requestOptions)
    .then(response => response.text())
    .then(result =>{
      console.log("RESULT: ",JSON.parse(result))
      return JSON.parse(result);
    })
    .catch(error => {
      console.log('error', error);
      throw error;
  });

  // var config = {
  //   method: "post",
  //   url: process.env.walletUrl + "/wallet/wallet/preWithdraw",
  //   headers: requestHeaders,
  //   data: requestBody,
  // };

  // return await axios(config)
  //   .then(function (response) {
  //     console.log(JSON.stringify(response.data));
  //     return response;
  //   })
  //   .catch(function (error) {
  //     console.log("********************************", error.response.data);
  //     throw error;
  //   });
};

const preRedeem = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/wallet/preRedeem",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const preTransfer = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/wallet/preTransfer",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const preTopup = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/wallet/preTopup",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// TESTED //////////////////////////////////
const withdrawWallet = async (requestBody, requestHeaders) => {
  //header bearer token



  var myHeaders = new Headers();
  myHeaders.append("Authorization", requestHeaders.authorization);
  myHeaders.append("Content-Type", "application/json");

  // console.log("##############MY HEADER: ",myHeaders);

  var requestOptions = {
    method: 'PUT',
    headers: myHeaders,
    body: JSON.stringify(requestBody),
    redirect: 'follow'
  };


  return await fetch(process.env.walletUrl+ "/wallet/wallet/walletWithdraw", requestOptions)
    .then(response => response.text())
    .then(result =>{
      console.log("RESULT: ",JSON.parse(result))
      return JSON.parse(result);
    })
    .catch(error => {
      console.log('error', error);
      throw error;
  });

};

//////////////////////////////// TESTED //////////////////////////////////
const confirmPurchase = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/wallet/confirmPurchase",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// DUMMY //////////////////////////////////
const ambankWithdraw = async (requestBody, requestHeaders) => {
  //header bearer token

  return {
    data: {
      msg: "successful",
    },
  };

  var config = {
    method: "put",
    url: process.env.walletUrl + "/wallet/wallet/walletTopup",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// TESTED //////////////////////////////////
const transferWallet = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "put",
    url: process.env.walletUrl + "/wallet/wallet/walletTransfer",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// TESTED  //////////////////////////////////
const getTransferList = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = []; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/wallet/walletTransfer"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// NOT DONE //////////////////////////////////
const redeemBGD = async (requestBody, requestHeaders) => {
  //header bearer token

  // return {
  //   data: {
  //     Msg: "Request Submited",
  //   },
  // };

    var myHeaders = new Headers();
    myHeaders.append("Authorization", requestHeaders.authorization);
    myHeaders.append("Content-Type", "application/json");

    // console.log("##############MY HEADER: ",myHeaders);

    var requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: JSON.stringify(requestBody),
      redirect: 'follow'
    };


    return await fetch(process.env.walletUrl+ "/wallet/wallet/walletRedeem", requestOptions)
      .then(response => response.text())
      .then(result =>{
        console.log("RESULT: ",JSON.parse(result))
        return JSON.parse(result);
      })
      .catch(error => {
        console.log('error', error);
        throw error;
    });


//   var config = {
//     method: "put",
//     maxBodyLength: Infinity,
//     url: process.env.walletUrl + "/wallet/wallet/walletRedeem",
//     headers: requestHeaders,
//     data: requestBody,
//   };

//   return await axios(config)
//     .then(function (response) {
//       console.log(JSON.stringify(response.data));
//       return response;
//     })
//     .catch(function (error) {
//       console.log("********************************", error.response.data);
//       throw error;
//     });
};

//////////////////////////////// NOT DONE //////////////////////////////////
const getTransactionList = async (requestBody, requestHeaders) => {
  //header bearer token


  return {
    data: [
      {
          "type": "F01",
          "date": "2023-01-20T07:19:10.245",
          "amount": 1000.00,
          "quantity": ""
      },
      {
         "type": "F05",
          "date": "2023-01-20T07:30:20.245",
          "amount": 250.00,
          "quantity": 1.500
      }
  ]
  };


  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/wallet/",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};



///////////////////////////////// DUMMY //////////////////////////////
const createTransactions = async (requestBody, requestHeaders) => {
  //header bearer token

  var myHeaders = new Headers();
  myHeaders.append("Authorization", requestHeaders.authorization);
  myHeaders.append("Content-Type", "application/json");

  console.log("##############MY HEADER: ",myHeaders);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(requestBody),
    redirect: 'follow'
  };

  console.log("cREATE TRANSACTION")

  return await fetch(process.env.walletUrl+"/wallet/wallet/createTransaction", requestOptions)
    .then(response => response.text())
    .then(result => {
      // console.log(result);
      return JSON.parse(result);
    })
    .catch(error => {
      console.log('error', error);
      throw error;
  });

};

///////////////////////////////// DUMMY //////////////////////////////
const createTradeBackTransactions = async (requestBody, requestHeaders) => {
  //header bearer token


  var myHeaders = new Headers();
  myHeaders.append("Authorization", requestHeaders.authorization);
  myHeaders.append("Content-Type", "application/json");

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: JSON.stringify(requestBody),
    redirect: 'follow'
  };

  return await fetch(process.env.walletUrl+"/wallet/wallet/createTradeBackTransaction", requestOptions)
    .then(response => response.text())
    .then(result => {
      // console.log(result);
      return JSON.parse(result);
    })
    .catch(error => {
      console.log('error', error);
      throw error;
  });

};


//////////////////////////////// NOTDONE  //////////////////////////////////
const validateWallet = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["acctNumber"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/wallet/validateWallet" +urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

//////////////////////////////// TESTED //////////////////////////////////


const getQueryBuyPrice = async (requestBody, requestHeaders) => {

  console.log("Header -> ", requestHeaders);
  var axios = require('axios');

  var config = {
    method: 'get',
    url: process.env.walletUrl + "/wallet/supplierGoldAPI/queryBuyPrice?margin="+requestBody.setupFees+"&uomCode="+requestBody.setupUomCode,
    headers: requestHeaders
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });

};

const getQuerySellPrice = async (requestBody, requestHeaders) => {

  console.log("Header -> ", requestHeaders);
  var axios = require('axios');

  var config = {
    method: 'get',
    url: process.env.walletUrl + "/wallet/supplierGoldAPI/querySellPrice?margin="+requestBody.setupFees+"&uomCode="+requestBody.setupUomCode,
    headers: { 
      'Authorization': requestHeaders.authorization, 
    }
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });

};

const spotOrderBuy = async (requestBody, requestHeaders) => {

  console.log("Header -> ", requestHeaders);
  var axios = require('axios');

  var config = {
    method: 'get',
    url: process.env.walletUrl + "/wallet/supplierGoldAPI/sportOrderBuy?priceRequestId="+requestBody.priceRequestId+"&price="+requestBody.price+"&weight="+requestBody.weight+"&reference="+requestBody.reference,
    headers: { 
      'Authorization': requestHeaders.authorization, 
    }
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });

};

const spotOrderSell = async (requestBody, requestHeaders) => {

  console.log("Header -> ", requestHeaders);
  var axios = require('axios');

  var config = {
    method: 'get',
    url: process.env.walletUrl + "/wallet/supplierGoldAPI/sportOrderSell?priceRequestId="+requestBody.priceRequestId+"&price="+requestBody.price+"&weight="+requestBody.weight+"&reference="+requestBody.reference,
    headers: { 
      'Authorization': requestHeaders.authorization, 
    }
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });

};


const getProfitLoss = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["current_price"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/wallet/profit_loss"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });
};


const ambankToken = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token


  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/token",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });
};

const ambankAccountEnquiry = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/accountEnquiryV4",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      // console.log("********************************", error.response);
      throw error;
    });
};

const ambankCreditTransfer = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/creditTransferV4",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });
};

const ambankGetCTStatus = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["cTsrcRefNo"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/getCTStatus"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });
};

const testnetwork = async () => {
  var config = {
    method: "get",
    url: "https://ifconfig.me",
    // headers: requestHeaders,
    // data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });
}

const aceRedeem = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token

  var allowedParams = ["redeemGram", "reference", "itemSerialNo","itemDenomination","itemQuantity","deliveryName1","deliveryName2","deliveryContact1","deliveryContact2","deliveryAddress1","deliveryAddress2","deliveryAddress3","deliveryAddress4","deliveryState","deliveryPostcode",]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }

 console.log("UrLPARAM: " ,urlParam);

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/supplierGoldAPI/redemption"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response);
      throw error;
    });

  }

//////////////////////////////// transactionHistories //////////////////////////////////
const transactionHistories = async (requestHeaders, requestQuery) => {
//header bearer token

var config = {
  method: "get",
  url: process.env.walletUrl + "/wallet/wallet/transactionHistories?days=" + requestQuery.days + "&transactionCode=" + requestQuery.transactionCode + "",
  headers: requestHeaders
};

  return await axios(config)
  .then(function (response) {
    var jsonStringArray = response.data;
    const jsonArray = jsonStringArray.map(jsonString => JSON.parse(jsonString));
    return JSON.stringify(jsonArray);
  })
  .catch(function (error) {
    console.log("********************************", error.response.data);
    throw error;
  });
};

const cashWalletHistories = async (requestBody,requestHeaders, requestparams) => {
  //header bearer token
  
  var config = {
    method: "get",
    maxBodyLength: Infinity,
    url: process.env.walletUrl + "/wallet/wallet/cashWalletHistories",
    headers: requestHeaders,
    params: requestparams,
  };
  
    return await axios(config)
    .then(response =>{
          console.log(response)
          return response;
        })
    .catch( error => {
      console.error(error);
      throw error;
    });
  };




/////////////////////////////////////PAYNET /////////////////////////////
const returnUrl = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/wallet/returnUrl",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const getReturnUrl = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/wallet/returnUrl",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const paynetPaymentNotification = async (requestBody, requestHeaders,requestparams) => {
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  var body = JSON.stringify(requestBody);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: body,
    redirect: 'follow'
  };

  return await fetch(process.env.walletUrl + "/wallet/paymentGatewayAPI/RPP/MY/Notification/PaymentStatus", requestOptions)
    .then(response => {
      return response;
    })
    .catch( error => {
      console.error(error);
      throw error;
    });
}

const paynetToken = async (requestBody, requestHeaders,requestparams) => {

  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/paynetToken",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const integrationPaynetBankList = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token


  var allowedParams = ["channelCode","pageKey"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }


  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/integration/listBank"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const paynetBankList = async (requestBody, requestHeaders,requestparams) => {
  //header bearer token


  var allowedParams = ["channelCode"]; // Whitelist of allowed parameters
  var urlParam = "?";
  var i = 0;
  
  for (var attributename in requestparams) {
    if (allowedParams.indexOf(attributename) !== -1) { // Check if the property is allowed
      if (i != 0) { urlParam += "&"; }
      console.log(attributename + ": " + requestparams[attributename]);
      urlParam += (attributename + "=" + requestparams[attributename]);
      i += 1;
    }
    else{
      console.log("NOT ALLOWED PARAMETER: "+attributename)
    }
  }


  var config = {
    method: "get",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/listBank"+urlParam,
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};


const paynetInitiatePayment = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "post",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/initiatePayment",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const paynetCancelPayment = async (requestBody, requestHeaders) => {
  //header bearer token

  var config = {
    method: "put",
    url: process.env.walletUrl + "/wallet/paymentGatewayAPI/cancelPayment",
    headers: requestHeaders,
    data: requestBody,
  };

  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.log("********************************", error.response.data);
      throw error;
    });
};

const topupStatus = async (requestparams, requestBody, requestHeaders) => {

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: process.env.walletUrl + '/wallet/wallet/topupStatus',
      params: requestparams,
      headers: requestHeaders
    };
    
    return await axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        console.error("********************************", error.response.data);
        throw error;
      });
    
}

const paynetRedirectRTP = async (requestBody, requestHeaders,requestparams) => {
  // var myHeaders = new Headers();
  // myHeaders.append("Content-Type", "application/json");

  var config = {
    method: 'get',
    maxBodyLength: Infinity,
    url: process.env.walletUrl + '/wallet/paymentGatewayAPI/RPP/MY/Redirect/RTP',
    params: requestparams,
    headers: requestHeaders
  };
  
  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.error("********************************", error.response.data);
      throw error;
    });
  
}

const paynetStatusInquiry = async (requestparams, requestBody, requestHeaders) => {

  var config = {
    method: 'get',
    maxBodyLength: Infinity,
    url: process.env.walletUrl + '/wallet/paymentGatewayAPI/statusInquiry',
    params: requestparams,
    headers: requestHeaders
  };
  
  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.error("********************************", error.response.data);
      throw error;
    });
  
}

const topupStatusChecking = async (requestparams, requestBody, requestHeaders) => {

  var config = {
    method: 'put',
    maxBodyLength: Infinity,
    url: process.env.walletUrl + '/wallet/wallet/topupStatusChecking',
    params: requestparams,
    headers: requestHeaders
  };
  
  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.error("********************************", error.response.data);
      throw error;
    });
  
}

const transactionDetails = async (requestparams, requestBody, requestHeaders) => {

  var config = {
    method: 'get',
    maxBodyLength: Infinity,
    url: process.env.walletUrl + '/wallet/wallet/transactionDetails',
    params: requestparams,
    headers: requestHeaders
  };
  
  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.error("********************************", error.response.data);
      throw error;
    });
  
}

////////////////////////////////////////////////////////////////////////////////

const downloadReceipt = async (requestparams, requestBody, requestHeaders) => {

  console.log("API proxy downloadReceipt called")

  var config = {
    method: 'get',
    responseType: 'arraybuffer',
    maxBodyLength: Infinity,
    url: process.env.walletUrl + '/wallet/wallet/downloadReceipt',
    params: requestparams,
    headers: requestHeaders
  };
  
  return await axios(config)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      console.error("*************ERROR ", error.response);
      throw error;
    });
}

const redemptionTracking = async (requestparams, requestBody, requestHeaders) => {

  var config = {
    method: 'get',
    maxBodyLength: Infinity,
    url: process.env.walletUrl + '/wallet/wallet/redemptionTracking',
    params: requestparams,
    headers: requestHeaders
  };
  
  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      response.data.data.helpDeskPhoneNumber= process.env.bursa_helpdesk_redemption_phoneNumber || ""
      return response;
    })
    .catch(function (error) {
      console.error("********************************", error.response.data);
      throw error;
    });
  
} 

const logisticStatus = async (requestparams, requestBody, requestHeaders) => {

  var config = {
    method: 'get',
    maxBodyLength: Infinity,
    url: process.env.walletUrl + '/wallet/supplierGoldAPI/logisticStatus',
    params: requestparams,
    headers: requestHeaders
  };
  
  return await axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
      return response;
    })
    .catch(function (error) {
      console.error("********************************", error.response.data);
      throw error;
    });
  
} 



  module.exports= {
    getWalletDetails,
    createWallet,
    validateWallet,
    
    preTopup,
    topupwallet,

    preWithdraw,
    withdrawWallet,

    confirmPurchase,

    preTransfer,
    transferWallet,
    getTransferList,

    preRedeem,
    redeemBGD,
    redemptionTracking,

    getTransactionList,

    ambankTopup,
    ambankWithdraw,
    createTransactions,
    createTradeBackTransactions,
    returnUrl,
    getReturnUrl,

    getQueryBuyPrice,
    getQuerySellPrice,
    spotOrderBuy,
    spotOrderSell,
    aceRedeem,
    logisticStatus,
    

    getProfitLoss,

    ambankToken,
    ambankAccountEnquiry,
    ambankCreditTransfer,
    ambankGetCTStatus,

    testnetwork,
    transactionHistories,

    paynetPaymentNotification,
    paynetRedirectRTP,

    paynetToken,
    integrationPaynetBankList,
    paynetBankList,
    paynetInitiatePayment,
    paynetCancelPayment,
    topupStatus,
    topupStatusChecking,
    paynetStatusInquiry,
    transactionDetails,

    downloadReceipt,
    cashWalletHistories
  }