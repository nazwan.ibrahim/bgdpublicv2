const axios = require("axios");
require("dotenv").config();
var admin = require("firebase-admin");
var serviceAccount = require(process.env.firebase_cert);
const moment = require('moment');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
//   databaseURL: ""
})
//////////////////////////TESTED////////////////////////
// const pushNotification = async (requestBody, requestHeaders) => {
//     //header bearer token
  
//     var config = {
//       method: "post",
//       url: process.env.onBoardingUrl + "/registration/user/bank",
//       headers: requestHeaders,
//       data: requestBody,
//     };
  
//     return await axios(config)
//       .then(function (response) {
//         console.log(JSON.stringify(response.data));
//         return response;
//       })
//       .catch(function (error) {
//         console.log("********************************", error.response.data);
//         throw error;
//       });
//   };


// function to call firebase
  const pushNotification = async (requestBody)=>{

//     schema: {
//       "notification": {
//                       "title": "something",
//                       "body": "hello"
//                       },
//       "data" : {
//                   "categoryCode":"NC01",
//                   "typeCode":"NT02",
//                   "dataUrl":"",
//                   "dataReference" : "2333",
//               },
//       "token": "",
//       "topic":"OPTIONAL only either include token or topic param"
// }

    requestBody.apns ={
      payload: {
          aps: {
              sound: "default"
          }
      }
    }

    requestBody.data.dateTime=moment().format('YYYY-MM-DDTHH:mm:ss.SSSSSSS')

    var message = requestBody;

      // const options =  notification_options
      console.log("message" ,message);
      var messagingService=admin.messaging();

      const messagingServiceResponse = await messagingService.send(message)
        .then( response => {
          console.log(response);
          // res.status(200).send("Notification sent successfully"+response)
          return "success"
        })
        .catch( error => {
          console.log(error);
          return"fail"
        });
      
      return messagingServiceResponse;

  }




  const saveNotification = async (requestBody, requestHeaders) => {
    //header bearer token

      const createNotificationBody={
        "categoryCode":requestBody.data.categoryCode,
        "typeCode":requestBody.data.typeCode,
        "title":requestBody.notification.title,
        "body":requestBody.notification.body,
        "dataUrl":requestBody.data.dataUrl,
        "dataReference":requestBody.data.dataReference
      }

        var myHeaders = new Headers();
        myHeaders.append("Authorization", requestHeaders.authorization);
        myHeaders.append("Content-Type", "application/json");

        // console.log("##############MY HEADER: ",myHeaders);

        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: JSON.stringify(createNotificationBody),
          redirect: 'follow'
        };

        // console.log(requestOptions)
        console.log("Request for SaveNotification",createNotificationBody)
        try {
          const response = await fetch(`${process.env.onBoardingUrl}/registration/notification/pushNotification`, requestOptions);
          
          if(response.status==401){
            return {data:"Unauthorized",status: 401}
          }
          console.log("d  "+response.status)
          const data = await response.json();
          const status = response.status;
          
          // console.log(data);
          // console.log(status);
         
          return { data, status };
        } catch (error) {
          console.error('error', error);
          return {response:error,status:500};
        }
        
      }

  
  const getDeviceId = async (requestBody, requestHeaders) => {
    //header bearer token
  
    var config = {
      method: "get",
      url: process.env.onBoardingUrl + "/registration/notification/getDeviceId",
      headers: requestHeaders,
      data: requestBody,
    };
  
    return await axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        console.log("********************************", error.response.data);
        throw error;
      });
  };


  const notificationList = async (requestBody, requestHeaders) => {
    //header bearer token
  
    var config = {
      method: "get",
      url: process.env.onBoardingUrl + "/registration/notification/notificationList",
      headers: requestHeaders,
      data: requestBody,
    };
  
    return await axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        console.log("********************************", error.response.data);
        throw error;
      });
  };

  const updateNotificationStatus = async (requestBody, requestHeaders) => {
    //header bearer token
  
    var config = {
      method: "put",
      url: process.env.onBoardingUrl + "/registration/notification/updateNotificationStatus",
      headers: requestHeaders,
      data: requestBody,
    };
  
    return await axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        return response;
      })
      .catch(function (error) {
        console.log("********************************", error.response.data);
        throw error;
      });
  };

  const saveNotificationInternal = async (requestBody, requestHeaders) => {
    //header bearer token

      const createNotificationBody={
        "categoryCode":requestBody.data.categoryCode,
        "typeCode":requestBody.data.typeCode,
        "title":requestBody.notification.title,
        "body":requestBody.notification.body,
        "dataUrl":requestBody.data.dataUrl,
        "dataReference":requestBody.data.dataReference,
        "username":requestBody.username
      }

        var myHeaders = new Headers();
        // myHeaders.append("Authorization", requestHeaders.authorization);
        myHeaders.append("Content-Type", "application/json");

        // console.log("##############MY HEADER: ",myHeaders);

        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: JSON.stringify(createNotificationBody),
          redirect: 'follow'
        };

        // console.log(requestOptions)

        try {
          const response = await fetch(`${process.env.onBoardingUrl}/registration/notification/pushNotificationInternal`, requestOptions);
          
          if(response.status==401){
            return {data:"Unauthorized",status: 401}
          }
          console.log("d  "+response.status)
          const data = await response.json();
          const status = response.status;
          
          // console.log(data);
          // console.log(status);
         
          return { data, status };
        } catch (error) {
          console.error('error', error);
          return {response:error,status:500};
        }
        
      }


// no need async operation here,
  const createNotificationBody=(title,body,categoryCode,typeCode,dataUrl,dataReference)=>{
    const notificationBody= {
    notification: {
                    title: title,
                    body: body
                    },
    data : {
                categoryCode:categoryCode,
                typeCode:typeCode,
                dataUrl:dataUrl,
                dataReference : dataReference
             }
  }
  return notificationBody;
}

  

module.exports = {
  getDeviceId,
  pushNotification,
  notificationList,
  updateNotificationStatus,
  saveNotification,
  saveNotificationInternal,
  createNotificationBody
};
