const express = require("express");
const connectionController = require("../controllers/connection");

const router = express.Router();

router.post("/Notification/PaymentStatus", connectionController.paynetPaymentNotification);


module.exports = router;