const express = require("express");
var bodyParser = require('body-parser')

const router = express.Router();


const pushNotificationController = require("../controllers/pushnotification");

router.post("/push",pushNotificationController.push);

router.put("/updateNotificationStatus",pushNotificationController.updateNotificationStatus);

router.get("/notificationList",pushNotificationController.notificationList);

router.post("/testSaveAndPushNotification",pushNotificationController.testSaveAndPush);

module.exports=router;