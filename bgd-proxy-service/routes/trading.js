const express = require("express");
// var bodyParser = require('body-parser')

const router = express.Router();


const tradingController = require("../controllers/trading");


router.post("/purchase",tradingController.purchase);
// router.get("/purchaseInit",tradingController.purchaseInit);
router.get("/queryBuyPrice",tradingController.getBuyPrice);
router.get("/querySellPrice",tradingController.getSellPrice);
router.get("/marketTrade",tradingController.getMarketTrade);
router.get("/marketReconcile",tradingController.getMarketReconcile);



module.exports = router;