const express = require("express");
// var bodyParser = require('body-parser')

const router = express.Router();


const walletController = require("../controllers/wallet");


router.get("/walletDetails",walletController.getWalletDetails);  // DONE
router.post("/createWallet",walletController.createWallet);    //DONE

router.put("/topupWallet",walletController.topupwalletV2);    //Done


router.put("/withdrawWallet",walletController.withdrawWalletV2);   // Done
router.put("/confirmPurchase",walletController.confirmPurchase);

router.put("/transferWallet",walletController.transferWalletV2);   //DONE
router.get("/transferList",walletController.getTransferList);    //DONE
// router.put("/redeemBGD",walletController.reedemBGD);   
router.put("/redeemBGD",walletController.reedemBGDV2);   
router.get("/redemptionTracking",walletController.redemptionTracking);        // Done
router.get("/transactionList",walletController.getTransactionList);   // Done

router.get("/validateWallet",walletController.validateWallet);

router.get("/queryBuyPrice", walletController.queryBuyPrice);
router.get("/querySellPrice", walletController.querySellPrice);
router.get("/spotOrderBuy", walletController.spotOrderBuy);
router.get("/spotOrderSell", walletController.spotOrderSell);
router.get("/aceRedeem", walletController.aceRedeem);
router.get("/logisticStatus",walletController.logisticStatus);

router.post("/returnUrl",walletController.returnUrl);
router.get("/returnUrl",walletController.getReturnUrl);

router.get("/profit_loss", walletController.getProfitLoss)


router.post("/ambankToken", walletController.ambankToken)
router.post("/ambankAccountEnquiry",walletController.ambankAccountEnquiry)
router.post("/ambankCreditTransfer",walletController.ambankCreditTransfer)
router.get("/ctStatus",walletController.ambankGetCTStatus)

router.get("/testnetwork",walletController.testnetwork)

router.get("/transactionHistories",walletController.transactionHistories)
router.get("/cashWalletHistories",walletController.cashWalletHistories)

router.get("/paynetToken",walletController.paynetToken)

router.get("/paynetListBank",walletController.paynetBankList)

router.post("/paynetInitiatePayment",walletController.paynetInitiatePayment)

router.put("/paynetCancelPayment",walletController.paynetCancelPayment)

router.get("/topupStatus",walletController.topupStatus)

router.put("/topupStatusChecking",walletController.topupStatusChecking)

router.get("/transactionDetails",walletController.transactionDetails)

router.get("/downloadReceipt",walletController.downloadReceipt)

module.exports = router;

