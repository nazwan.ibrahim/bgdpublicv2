const express = require("express");
const connectionController = require("../controllers/connection");
const onboardingController = require("../controllers/onboarding");

const router = express.Router();

router.post("/ctosAuthenticate",onboardingController.ctosAuthenticateSIT);
router.get("/ctosVerification",onboardingController.getCtosVerificationRecordSIT);
router.post("/createCtosVerification",onboardingController.createCtosVerificationSIT);
router.put("/ctosVerification",onboardingController.updateCtosVerificationSIT);

router.get("/testnetwork", connectionController.testnetwork);

router.post("/ambankAccountEnquiry",connectionController.ambankAccountEnquiry)
router.post("/ambankCreditTransfer",connectionController.ambankCreditTransfer)
router.get("/ctStatus",connectionController.ambankGetCTStatus)

router.get("/queryBuyPrice", connectionController.queryBuyPrice);
router.get("/querySellPrice", connectionController.querySellPrice);
router.get("/spotOrderBuy", connectionController.spotOrderBuy);
router.get("/spotOrderSell", connectionController.spotOrderSell);
router.get("/aceRedeem", connectionController.aceRedeem);
router.get("/logisticStatus",connectionController.logisticStatus);

router.get("/paynetToken",connectionController.paynetToken);
router.get("/paynetListBank",connectionController.paynetBankListIntegration)
router.post("/paynetInitiatePayment",connectionController.paynetInitiatePayment)
router.put("/paynetCancelPayment",connectionController.paynetCancelPayment)
router.get("/paynetStatusInquiry",connectionController.paynetStatusInquiry)

module.exports = router;