const express = require("express");
const onboardingController = require("../controllers/onboarding");

const router = express.Router();

router.post("/verificationStatus", onboardingController.ctosVerification);


module.exports = router;