const express = require("express");
// var bodyParser = require('body-parser')

const router = express.Router();

const administrationController = require("../controllers/administration");


router.get("/investorType",administrationController.getInvestorType);
router.get("/price",administrationController.getPrice);
router.get("/fee",administrationController.getFee);
router.get("/bursaRevenue",administrationController.getBursaRevenue);
router.get("/bursaCostRecovery",administrationController.getBursaCostRecovery);
router.get("/tierManagement",administrationController.getTierManagement);

router.get("/transferMotive",administrationController.getTransferMotive);
router.get("/redeemMotive",administrationController.getRedeemMotive);

router.get("/initTransfer",administrationController.initTransferV2);
router.get("/initRedeem",administrationController.initRedeemV2);
router.get("/initPurchase",administrationController.initPurchase); 
router.get("/initTopup",administrationController.initTopupV2);
router.get("/initWithdraw",administrationController.initWithdraw);

router.get("/priceGraph",administrationController.getPriceGraph);

router.get("/tierThresholdChecking",administrationController.tierThresholdCheck);

module.exports = router;