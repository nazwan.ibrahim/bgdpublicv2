const express = require("express");
var bodyParser = require('body-parser')

const router = express.Router();

const onboardingController = require("../controllers/onboarding");

const { route } = require("./wallet");

var urlencodedParser = bodyParser.urlencoded({ extended: false })
// #swagger.tags = ['Onboarding']

router.post("/login", urlencodedParser, onboardingController.login);
router.post("/refreshToken",urlencodedParser,onboardingController.refreshToken);

router.get("/getName",onboardingController.getName);
router.get("/getUserDetails",onboardingController.getUserDetails);
router.put("/closeUserAccount",onboardingController.closeUserAccount)

router.post("/generateOTP",onboardingController.generateOTP);
router.post("/verifyOTP",onboardingController.verifyOTP);

/* ************************  SUB MODULE : Get Entities *****************************/
router.get("/investorType",onboardingController.getInvestorType);
router.get("/identificationType",onboardingController.getIdentificationType);
router.get("/country", onboardingController.getCountry);
router.get("/state",onboardingController.getStateList)
router.get("/employmentStatusList",onboardingController.getEmploymentStatusList);
router.get("/jobPositionType",onboardingController.getJobPositionType);
router.get("/natureOfBusinessType",onboardingController.getBusinessType);
router.get("/transactionPurposeType",onboardingController.getTransactionPurposeType);
router.get("/totalMonthlyTransList",onboardingController.getTotalMonthlyTransList);
router.get("/freqMonthlyTransList",onboardingController.getFreqMonthlyTransList);
router.get("/sourceOfFundType",onboardingController.getSourceOfFundType);
router.get("/sourceOfWealthType",onboardingController.getSourceOfWealthType);

router.post("/registerIndividual", onboardingController.registerIndividual);
router.get("/securityQuestions", onboardingController.getSecurityQuestion);
router.post("/createSecurityQuestion",onboardingController.createSecurityQuestion);

router.post("/verifyEmail",onboardingController.verifyEmail);
router.get("/verifyEmail", onboardingController.redirectVerifyEmail);
router.get("/postcodeInfo", onboardingController.getPostCodeInfo);

router.post("/kycValidate",onboardingController.kycValidate);
router.post("/firstPartyChecking",onboardingController.firstPartyChecking);
router.post("/tier1KycChecking",onboardingController.tier1KycChecking)
// router.post("/tier2KycChecking",onboardingController.tier2KycChecking);
// router.post("/tier3KycChecking",onboardingController.tier3KYCChecking);
router.post("/updateTier",onboardingController.updateTier);
router.post("/createCtosVerification",onboardingController.createCtosVerification);
router.get("/ctosVerificationResult",onboardingController.getCtosVerificationResult);

router.put("/changePassword",onboardingController.changePassword);
router.post("/validatePassword",onboardingController.validatePassword);
router.get("/validateReferralCode",onboardingController.validateReferralCode);
router.get("/securityQuestion",onboardingController.securityQuestion);
router.post("/generateResetPassword",onboardingController.generateResetPassword);
router.put("/resetPassword",onboardingController.resetPassword);

router.get("/getAddress",onboardingController.getShippingAddress);
router.post("/createAddress",onboardingController.createShippingAddress);
router.put("/updateAddress",onboardingController.updateShippingAddress);

router.get("/bankList",onboardingController.getBankList);
router.get("/getBankDetails",onboardingController.getBankDetails);
router.post("/createBankDetails",onboardingController.createBankDetails);
router.delete("/deleteBankDetails",onboardingController.deleteBankDetails)

router.post("/pin",onboardingController.createPin);
router.put("/pin",onboardingController.changePin);
router.post("/verifyPin",onboardingController.verifyPin);
router.put("/disablePin",onboardingController.disablePin);
router.put("/resetPin",onboardingController.resetPin);

router.put("/addDeviceID",onboardingController.addDeviceID);
router.put("/notificationPermission",onboardingController.updateNotificationPermission);

/* ************************  SUB MODULE : User Profile & Details *****************************/
router.post("/createProfilePicture",onboardingController.createProfilePicture);
router.put("/personalDetails",onboardingController.updatePersonalDetails);
router.put("/contactDetails",onboardingController.updateContactDetails);
router.post("/employmentDetails",onboardingController.insertEmploymentDetails);
router.post("/financialDetails",onboardingController.insertFinancialDetails);
router.put("/profileDetails",onboardingController.updateProfileDetails);
router.get("/personalDetails",onboardingController.getPersonalDetails);
router.get("/contactDetails",onboardingController.getContactDetails);
router.get("/employmentDetails",onboardingController.getEmploymentDetails);
router.get("/financialDetails",onboardingController.getFinancialDetails);

router.post("/fatcaCrsDeclaration",onboardingController.createFatcaCrsDeclaration);

module.exports = router;