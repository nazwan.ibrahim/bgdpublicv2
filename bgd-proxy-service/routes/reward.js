const express = require("express");

const router = express.Router();

const rewardController = require("../controllers/reward");;

router.get("/validateCode",rewardController.validateCode);

router.post("/participant",rewardController.participant);

router.get("/trackRewards",rewardController.trackRewards);

module.exports = router;