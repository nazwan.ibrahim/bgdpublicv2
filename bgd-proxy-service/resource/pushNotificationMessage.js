
function goldValueFormat(value){
    return value.toLocaleString('en-US', {
        minimumFractionDigits: 0,
        maximumFractionDigits: 6,
    })
}

const getMessage= (notificationTypeCode,var1,var2)=>{
    switch(notificationTypeCode) {
        case "NT011":
          // code block
          return {title:"Top-Up Successful",
                 body:""}
        case "NT021":
          // code block
          return {title:"Withdrawal Successful",
                  body:"Withdrawal amount RM"+var1.toLocaleString()+"."}
        case "NT031":
            return {title:"Gold Transfer Successful",
                  body:"Transferred "+goldValueFormat(var1)  + "g of gold to " +var2+"."}
        case "NT041":
            return {title:"Gold Redemption Request Successful",
                  body:"Redemption request of "+var1.toLocaleString()+" Gold Dinar Coins (Reference No.:"+var2+"). Estimated delivery within 3-7 days."}
        case "NT051":
            return {title:"Gold Purchase Successful",
                  body:"Purchased amount "+goldValueFormat(var1)+"g of gold for RM"+var2.toLocaleString()+"."}
        case "NT061":
            return {title:"Gold Sale Successful",
                  body:"Sale amount "+goldValueFormat(var1)+"g of gold for RM"+var2.toLocaleString()+"."}
        default:
          // code block
          return {title:"",
                  body:""}
      }
}

module.exports={
    getMessage
}