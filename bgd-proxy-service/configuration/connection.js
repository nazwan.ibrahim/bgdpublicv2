const { Sequelize , QueryTypes  } = require('sequelize');
require("dotenv").config();

const database_registration_URI = process.env.database_registration_URI;
const database_registration_pass=  process.env.database_registration_pass;
const database_registration_name= process.env.database_registration_name;
const database_registration_username =process.env.database_registration_username;
const database_registration_ssl =process.env.database_registration_ssl;
const databaseConnectionString='postgres://'+database_registration_username+':'+database_registration_pass+'@'+database_registration_URI+'/'+database_registration_name;
console.log("DATABASE CONNECTION :"+ databaseConnectionString);

const registrationCursor = new Sequelize(databaseConnectionString,{
    pool: {
      max: 100,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    dialectOptions: {
        ssl: {
          require: true,
          rejectUnauthorized: false,
        },
    },
  }); 
  
const registrationDb= async()=>{ // this function used to check connection only
    try {
        await registrationCursor.authenticate(); 
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

module.exports={
    registrationDb,
    registrationCursor
}