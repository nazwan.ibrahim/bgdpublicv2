require("dotenv").config();
const fs = require('fs');

const sslConfig = {
  credentials: true,
  key: fs.readFileSync(process.env.SSL_Key),
  cert: fs.readFileSync(process.env.SSL_Cert),
  passphrase: process.env.SSL_Pass,
};

module.exports = {
    sslConfig,
}