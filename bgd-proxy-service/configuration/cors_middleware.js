require("dotenv").config();

const corsMiddleware = (req, res, next) => {
    res.append('Access-Control-Allow-Origin', [process.env.cors]);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers','*'),
    res.append('Access-Control-Allow-Credentials', 'true'),
    res.append('X-Content-Type-Options', 'nosniff',);
    res.append('X-Frame-Options', 'SAMEORIGIN');
    res.append('X-XSS-Protection', '1; mode=block');
    res.append('Content-Security-Policy', "default-src 'self'");
    
    next();
}

module.exports = {
    corsMiddleware,
}