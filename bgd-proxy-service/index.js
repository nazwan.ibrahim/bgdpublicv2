const express = require("express");
const webService = require('http');
const chalk = require("./chalk");
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
const connectionRoutes = require("./routes/connection");
const onboardingRoutes = require("./routes/onboarding");
const walletRoutes = require("./routes/wallet");
const tradingRoutes = require("./routes/trading");
const administrationRoutes = require("./routes/administration");
const rewardRoutes = require("./routes/reward");
const pushNotification= require("./routes/pushnotification");
const ctosCADRoutes= require("./routes/ctosCAD");
const kafkaConsumer = require("./kafka/consumer");
const kafkaProducer = require("./kafka/producer");
const paynetPaymentNotification = require("./routes/paynetPaymentNotification");
const connectionDB= require("./configuration/connection");
const corsMiddleware = require("./configuration/cors_middleware");
const sslConfig = require("./configuration/ssl");
require("dotenv").config();
const cors = require('cors')
const websocket = require("./websockets/ws");
const outputFile = './swagger_output.json';
// const admin = require('./firebase-config')


const port = 3000;

const app = express();

app.use(express.json());
app.use(corsMiddleware.corsMiddleware);  // CORSss
app.disable('etag');

const server = webService.createServer(app);

app.use("/api/swagger", swaggerUi.serve, swaggerUi.setup(swaggerFile));

app.use("/RPP/MY",paynetPaymentNotification
    // #swagger.tags = ['Paynet Payment Notification']
);

app.use("/api/connection/", connectionRoutes
  // #swagger.tags = ['Third-party Integration Test']
);

app.use("/api/onboarding/", onboardingRoutes
  // #swagger.tags = ['Onboarding']
);

app.use("/api/wallet/", walletRoutes 
 // #swagger.tags = ['Wallet']
);
app.use("/api/trading/", tradingRoutes 
 // #swagger.tags = ['Trading']
);

app.use("/api/administration/", administrationRoutes 
 // #swagger.tags = ['Administration']
);

app.use("/api/pushnotification/",pushNotification
// #swagger.tags = ['PushNotification']
);

app.use("/api/reward/",rewardRoutes
// #swagger.tags = ['Reward']
);

app.use("/api/helloword", async (req, res, next) => {
  res.status(200).send("Hello World");
});

//Ctos CAD webhook
app.use("/webhook/onboarding/",ctosCADRoutes);


const listener = server.listen(port, () => {
  console.log(chalk.init("Your app is listening on port " + port));
});

connectionDB.registrationDb();


// websocket.setupExpressServer(listener);
// websocket.wsMessageGenerator(); 
// websocket.subscribePrice() // run this if not using kafka

// kafkaProducer.produceMessage();
// kafkaConsumer.consumeMessage();

// websocket(listener)   (old)