import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { FuseMediaWatcherService } from '@bgd/services/media-watcher';
import { FuseNavigationService, FuseVerticalNavigationComponent } from '@bgd/components/navigation';
import { Navigation } from 'app/core/navigation/navigation.types';
import { NavigationService } from 'app/core/navigation/navigation.service';

@Component({
    selector: 'modern-layout',
    templateUrl: './modern.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ModernLayoutComponent implements OnInit, OnDestroy {
    isScreenSmall: boolean;
    navigation: Navigation;
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _navigationService: NavigationService,
        private _fuseMediaWatcherService: FuseMediaWatcherService,
        private _fuseNavigationService: FuseNavigationService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for current year
     */
    get currentYear(): number {
        return new Date().getFullYear();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to navigation data
        this._navigationService.navigation$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((navigation: Navigation) => {
                this.navigation = navigation;
            });

        // Subscribe to media changes
        this._fuseMediaWatcherService.onMediaChange$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(({ matchingAliases }) => {

                // Check if the screen is small
                this.isScreenSmall = !matchingAliases.includes('md');
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle navigation
     *
     * @param name
     */
    toggleNavigation(name: string): void {
        // Get the navigation
        const navigation = this._fuseNavigationService.getComponent<FuseVerticalNavigationComponent>(name);

        if (navigation) {
            // Toggle the opened status
            navigation.toggle();
        }
    }
    dashboard() {
        window.location.href = '/dashboard'; // Reload the current page

    }
}


// import { MegaMenuItem } from 'primeng/api';

// @Component({
//     selector: 'my-app',
//     templateUrl: './modern.component.html'
// })
// export class AppComponent {
//     gfg: MegaMenuItem[];

//     ngOnInit() {
//         this.gfg = [
//             {
//                 label: 'GeeksforGeeks',
//                 items: [
//                     [
//                         {
//                             label: 'AngularJS',
//                             items: [{ label: 'AngularJS 1' }, { label: 'AngularJS 2' }]
//                         },
//                         {
//                             label: 'ReactJS',
//                             items: [{ label: 'ReactJS 1' }, { label: 'ReactJS 2' }]
//                         }
//                     ],
//                     [
//                         {
//                             label: 'HTML',
//                             items: [{ label: 'HTML 1' }, { label: 'HTML 2' }]
//                         },
//                         {
//                             label: 'PrimeNG',
//                             items: [{ label: 'PrimeNG 1' }, { label: 'PrimeNG 2' }]
//                         }
//                     ]
//                 ]
//             }
//         ];
//     }
// }
