import { Route} from '@angular/router';
import { EditProfileComponent } from './edit-profile.component';

export const EditProfileRoutes: Route[] = [
  {
      path     : '',
      component: EditProfileComponent
  }
];

