import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { FuseAlertType } from '@bgd/components/alert';
import { filter } from 'rxjs/operators';

export interface DialogData {
    response: any;
    transactionMethodCode: any;
    bankCode: any;
}

/**
 * @title Dialog elements
 */
@Component({
    selector: 'cash-wallet-topup-delay-prompt',
    templateUrl: 'cash-wallet-topup-delay-prompt.component.html',
})
export class CashWalletTopupDelayPrompt {

    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _dashboardService: DashboardService,
        public dialogRef: MatDialogRef<CashWalletTopupDelayPrompt>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

    onProceed(): void {
        this.dialogRef.close();
        this._router.navigate(['/cash-wallet-topup-confirmation'], {
            queryParams: {
                response: this.data.response,
                transactionMethodCode: this.data.transactionMethodCode,
                bankCode: this.data.bankCode,
            }
        });
    }

    onCancel(): void {
        this.dialogRef.close();
        this._router.navigate(['/cash-wallet-topup-bank'], {
            queryParams: {
                response: this.data.response,
                transactionMethodCode: this.data.transactionMethodCode,
                bankCode: this.data.bankCode
            }
        });
    }
}
