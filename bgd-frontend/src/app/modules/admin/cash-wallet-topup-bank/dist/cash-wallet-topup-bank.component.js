"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletTopupBankComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var animations_1 = require("@bgd/animations");
var CashWalletTopupBankComponent = /** @class */ (function () {
    function CashWalletTopupBankComponent(_activatedRoute, _router, _authService, _formBuilder, bankListService, cashWalletService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._authService = _authService;
        this._formBuilder = _formBuilder;
        this.bankListService = bankListService;
        this.cashWalletService = cashWalletService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.dropdownData = [];
        this.acctNumber = '';
        this.amount = '';
        this.walletTypeCode = '';
        this.regTypeCode = '';
        this.transactionMethodCode = '';
        this.transactionTypeCode = '';
        this.uomCode = '';
        this.statusCode = '';
        this.chargeTransFee = '';
        this.chargeSstFee = '';
        this.totalAmount = '';
        this.customerName = '';
        this.channelCode = '';
        this.currency = '';
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log("DATA RESPONSE:  ", response);
        this.acctNumber = response[0].acctNumber;
        this.amount = response[0].amount;
        this.walletTypeCode = response[0].walletTypeCode;
        this.regTypeCode = response[0].regTypeCode;
        this.uomCode = response[0].uomCode;
        this.transactionMethodCode = response[0].transactionMethodCode;
        this.transactionTypeCode = response[0].transactionTypeCode;
        this.statusCode = response[0].statusCode;
        this.setuptransfees = response[0].feesForms[0].setupFees;
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.transFeeType = response[0].feesForms[0].feeType;
        this.transFeeTypeCode = response[0].feesForms[0].feeTypeCode;
        this.transSetupUom = response[0].feesForms[0].setupUom;
        this.transSetupUomCode = response[0].feesForms[0].setupUomCode;
        this.transChargeUom = response[0].feesForms[0].chargeUom;
        this.transChargeUomCode = response[0].feesForms[0].chargeUomCode;
        this.setupsstfees = response[0].feesForms[1].setupFees;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.SstFeeType = response[0].feesForms[1].feeType;
        this.SstFeeTypeCode = response[0].feesForms[1].feeTypeCode;
        this.SstSetupUom = response[0].feesForms[1].setupUom;
        this.SstSetupUomCode = response[0].feesForms[1].setupUomCode;
        this.SstChargeUom = response[0].feesForms[1].chargeUom;
        this.SstChargeUomCode = response[0].feesForms[1].chargeUomCode;
        this.totalAmount = response[0].totalAmount;
        this.customerName = response[0].customerName;
        this.channelCode = response[0].channelCode;
        this.currency = response[0].currency;
    }
    CashWalletTopupBankComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.bankListService.bankListPaynet('BW', 'BW').subscribe(function (data) {
            _this.dropdownData = data['banks'];
            console.log("BANK", _this.dropdownData);
        });
        this.bankListForm = this._formBuilder.group({
            bankCode: ['', forms_1.Validators.required],
            transactionCode: ['BW', forms_1.Validators.required]
        });
    };
    CashWalletTopupBankComponent.prototype.backButton = function () {
        this._router.navigate(['/cash-wallet-topup']);
    };
    CashWalletTopupBankComponent.prototype.topupNow = function () {
        var transactionMethodCode = 'H01';
        console.log(transactionMethodCode);
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        if (this.bankListForm.invalid) {
            return;
        }
        this.transactionMethodCode = transactionMethodCode; // Assign the value here
        this.bankListForm.disable();
        var bankCode = this.bankListForm.get('bankCode').value; // Get the bankCode value
        console.log(bankCode);
        console.log(this.bankListForm.get('bankCode').value);
        this._router.navigate(['/cash-wallet-topup-confirmation'], {
            queryParams: {
                response: JSON.stringify(response),
                transactionMethodCode: this.transactionMethodCode,
                bankCode: bankCode
            }
        });
    };
    __decorate([
        core_1.ViewChild('bankListNgForm')
    ], CashWalletTopupBankComponent.prototype, "bankListNgForm");
    CashWalletTopupBankComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet-topup-bank',
            templateUrl: './cash-wallet-topup-bank.component.html',
            styleUrls: ['./cash-wallet-topup-bank.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], CashWalletTopupBankComponent);
    return CashWalletTopupBankComponent;
}());
exports.CashWalletTopupBankComponent = CashWalletTopupBankComponent;
// const pay = {
//   channelCode: this.channelCode,
//   bicCode: this.bankListForm.get('bankCode').value,
//   amount: this.totalAmount,
//   currency: this.currency,
//   name: this.customerName,
//   recipientReference: "testing api",
//   paymentDescription: "testing api"
// }
// this.bankListService.paynetInitiatePayment(pay)
//   .pipe(
//     finalize(() => {
//       this.bankListForm.enable();
//       this.bankListForm.reset();
//     })
//   )
//   .subscribe(
//     Response => {
//       console.log(Response);
//       if (Response.redirectUrl) {
//         window.open(Response.redirectUrl, '_blank');
//       }
//       // this.alert = {
//       //   type: 'success',
//       //   message: 'Success'
//       // };
//     },
//     Response => {
//       this.alert = {
//         type: 'error',
//         message: 'Something went wrong, please try again.'
//       };
//     }
//   );
