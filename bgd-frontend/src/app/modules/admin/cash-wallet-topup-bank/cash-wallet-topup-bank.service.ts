import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root',
})

export class CashWalletTopupBankService {
    constructor(private _httpClient: HttpClient) { }

    bankList(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/bankList');
    }

    bankListPaynet(channelCode: string, transactionCode: string): Observable<any> {
        const params = new HttpParams().set('channelCode', channelCode).set('transactionCode', transactionCode)
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/paynetListBank', { params });
    }

    paynetInitiatePayment(pay: { channelCode: any; bicCode: any; amount: any; currency: any; name: any; recipientReference: any; paymentDescription: any }): Observable<any> {
        return this._httpClient.post<any>(environment.apiUrl + '/api/wallet/paynetInitiatePayment', { pay });
    }
}