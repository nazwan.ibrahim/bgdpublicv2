import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm, UntypedFormBuilder, FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { CashWalletTopupBankService } from './cash-wallet-topup-bank.service';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { finalize, map } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { CashWalletTopupDelayPrompt } from './cash-wallet-topup-delay-prompt/cash-wallet-topup-delay-prompt.component';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';

interface RequestBody {
  acctNumber: any;
  amount: any;
  walletTypeCode: any;
  regTypeCode: any;
  uomCode: any;
  transactionMethodCode: string;
  transactionTypeCode: any;
  statusCode: string;
  feesForms?: any[]; // Add optional feesForms property
  totalAmount: any;
  channelCode: any;
  bicCode: any;
  currency: any;
  customerName: any;
}

interface FeeForm {
  feeType: string;
  feeTypeCode: string;
  setupFees: number;
  setupUom: string;
  setupUomCode: string;
  chargeFees: number;
  chargeUom: string;
  chargeUomCode: string;
}

@Component({
  selector: 'bgd-cash-wallet-topup-bank',
  templateUrl: './cash-wallet-topup-bank.component.html',
  styleUrls: ['./cash-wallet-topup-bank.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})


export class CashWalletTopupBankComponent {

  @ViewChild('bankListNgForm') bankListNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  bankListForm: UntypedFormGroup;
  showAlert = false;
  dropdownData = [];

  acctNumber: any = '';
  amount: any = '';
  walletTypeCode: any = '';
  regTypeCode: any = '';
  transactionMethodCode: any = '';
  transactionTypeCode: any = '';
  uomCode: any = '';
  statusCode: any = '';
  chargeTransFee: any = '';
  chargeSstFee: any = '';
  totalAmount: any = '';
  customerName: any = '';
  channelCode: any = '';
  currency: any = '';
  EndToEndId: any;
  EndToEndIdSignature: any;
  setuptransfees: any;
  setupsstfees: any;
  transFeeType: any;
  transFeeTypeCode: any;
  transSetupUom: any;
  transSetupUomCode: any;
  transChargeUom: any;
  transChargeUomCode: any;
  SstFeeType: any;
  SstFeeTypeCode: any;
  SstSetupUom: any;
  SstSetupUomCode: any;
  SstChargeUom: any;
  SstChargeUomCode: any;
  feesForms: [];
  isDialogOpen: boolean = false;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private bankListService: CashWalletTopupBankService,
    private walletService: WalletService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {

    this.bankListService.bankListPaynet('BW', 'BW').subscribe(data => {
      this.dropdownData = data['banks'];

      console.log("Bank list:", this.dropdownData);
    });

    this.bankListForm = this._formBuilder.group({
      bankCode: ['', Validators.required],
      transactionCode: ['BW', Validators.required],
    });


  }

  backButton(): void {
    this._router.navigate(['/cash-wallet-topup']);
  }

//   openDialog(): void {
//     this.isDialogOpen = true;

//     const dialogRef = this.dialog.open(CashWalletTopupDelayPrompt, {
//       data: {
//         response: this.walletService.initResponse,
//         transactionMethodCode: 'H01',
//         bankCode: this.bankListForm.get('bankCode').value
//       }
//     });

//     dialogRef.afterClosed().subscribe(() => {
//       // location.reload();
//     });
//   }

nextBtn(): void {

  if (this.bankListForm.invalid) {
    return;
  }

  this.bankListForm.disable();

  const bankCode = this.bankListForm.get('bankCode').value; 
  this.walletService.bankCodeTopup = bankCode;

  this._router.navigate(['/cash-wallet-topup-confirmation']);
  }
  
}

