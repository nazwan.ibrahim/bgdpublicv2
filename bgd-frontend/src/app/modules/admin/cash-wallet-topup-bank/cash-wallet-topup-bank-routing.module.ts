import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupBankComponent } from 'app/modules/admin/cash-wallet-topup-bank/cash-wallet-topup-bank.component';

const routes: Routes = [
  {
    path: '',
    component: CashWalletTopupBankComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletTopupBankRoutingModule { }
