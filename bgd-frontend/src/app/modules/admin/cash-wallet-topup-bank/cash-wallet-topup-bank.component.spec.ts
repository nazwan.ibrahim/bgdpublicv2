import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupBankComponent } from './cash-wallet-topup-bank.component';

describe('CashWalletTopupBankComponent', () => {
  let component: CashWalletTopupBankComponent;
  let fixture: ComponentFixture<CashWalletTopupBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupBankComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
