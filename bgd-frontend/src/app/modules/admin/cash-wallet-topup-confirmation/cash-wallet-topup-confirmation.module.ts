import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashWalletTopupConfirmationComponent } from './cash-wallet-topup-confirmation.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletTopupConfirmationRoutingModule } from './cash-wallet-topup-confirmation-routing.module';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';


@NgModule({
    declarations: [
        CashWalletTopupConfirmationComponent
    ],
    imports: [
        CommonModule,
        CashWalletTopupConfirmationRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatRadioModule,
        MatStepperModule,
        MatSelectModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule,
        MatDividerModule,
        MatListModule,
    ]
})
export class CashWalletTopupConfirmationModule { }
