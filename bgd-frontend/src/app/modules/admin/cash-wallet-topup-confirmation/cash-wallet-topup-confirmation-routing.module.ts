import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupConfirmationComponent } from 'app/modules/admin/cash-wallet-topup-confirmation/cash-wallet-topup-confirmation.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletTopupConfirmationComponent
        // resolve  : {
        //   data: CashWalletTopupResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletTopupConfirmationRoutingModule { }
