import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupConfirmationComponent } from './cash-wallet-topup-confirmation.component';

describe('CashWalletTopupConfirmationComponent', () => {
  let component: CashWalletTopupConfirmationComponent;
  let fixture: ComponentFixture<CashWalletTopupConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupConfirmationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
