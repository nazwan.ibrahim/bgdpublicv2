"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletTopupConfirmationModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var cash_wallet_topup_confirmation_component_1 = require("./cash-wallet-topup-confirmation.component");
var shared_module_1 = require("app/shared/shared.module");
var button_1 = require("@angular/material/button");
var input_1 = require("@angular/material/input");
var cash_wallet_topup_confirmation_routing_module_1 = require("./cash-wallet-topup-confirmation-routing.module");
var select_1 = require("@angular/material/select");
var stepper_1 = require("@angular/material/stepper");
var radio_1 = require("@angular/material/radio");
var form_field_1 = require("@angular/material/form-field");
var icon_1 = require("@angular/material/icon");
var checkbox_1 = require("@angular/material/checkbox");
var progress_spinner_1 = require("@angular/material/progress-spinner");
var card_1 = require("@bgd/components/card");
var alert_1 = require("@bgd/components/alert");
var divider_1 = require("@angular/material/divider");
var list_1 = require("@angular/material/list");
var CashWalletTopupConfirmationModule = /** @class */ (function () {
    function CashWalletTopupConfirmationModule() {
    }
    CashWalletTopupConfirmationModule = __decorate([
        core_1.NgModule({
            declarations: [
                cash_wallet_topup_confirmation_component_1.CashWalletTopupConfirmationComponent
            ],
            imports: [
                common_1.CommonModule,
                cash_wallet_topup_confirmation_routing_module_1.CashWalletTopupConfirmationRoutingModule,
                shared_module_1.SharedModule,
                button_1.MatButtonModule,
                input_1.MatInputModule,
                button_1.MatButtonModule,
                checkbox_1.MatCheckboxModule,
                form_field_1.MatFormFieldModule,
                icon_1.MatIconModule,
                radio_1.MatRadioModule,
                stepper_1.MatStepperModule,
                select_1.MatSelectModule,
                input_1.MatInputModule,
                progress_spinner_1.MatProgressSpinnerModule,
                card_1.FuseCardModule,
                alert_1.FuseAlertModule,
                shared_module_1.SharedModule,
                divider_1.MatDividerModule,
                list_1.MatListModule,
            ]
        })
    ], CashWalletTopupConfirmationModule);
    return CashWalletTopupConfirmationModule;
}());
exports.CashWalletTopupConfirmationModule = CashWalletTopupConfirmationModule;
