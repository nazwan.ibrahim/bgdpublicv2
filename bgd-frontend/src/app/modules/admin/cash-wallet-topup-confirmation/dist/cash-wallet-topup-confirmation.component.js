"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletTopupConfirmationComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var rxjs_1 = require("rxjs");
var CashWalletTopupConfirmationComponent = /** @class */ (function () {
    function CashWalletTopupConfirmationComponent(_activatedRoute, _router, _authService, _formBuilder, bankListService, cashWalletService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._authService = _authService;
        this._formBuilder = _formBuilder;
        this.bankListService = bankListService;
        this.cashWalletService = cashWalletService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.dropdownData = [];
        this.today = new Date();
        this.acctNumber = '';
        this.amount = '';
        this.walletTypeCode = '';
        this.regTypeCode = '';
        this.transactionMethodCode = '';
        this.transactionTypeCode = '';
        this.uomCode = '';
        this.statusCode = '';
        this.chargeTransFee = '';
        this.chargeSstFee = '';
        this.totalAmount = '';
        this.customerName = '';
        this.channelCode = '';
        this.currency = '';
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        var transactionMethodCode = this._activatedRoute.snapshot.queryParams['transactionMethodCode'];
        var bankCode = this._activatedRoute.snapshot.queryParams['bankCode'];
        console.log("DATA RESPONSE:  ", response);
        this.acctNumber = response[0].acctNumber;
        this.amount = response[0].amount;
        this.walletTypeCode = response[0].walletTypeCode;
        this.regTypeCode = response[0].regTypeCode;
        this.uomCode = response[0].uomCode;
        this.transactionMethodCode = transactionMethodCode;
        this.transactionTypeCode = response[0].transactionTypeCode;
        this.statusCode = response[0].statusCode;
        this.setuptransfees = response[0].feesForms[0].setupFees;
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.transFeeType = response[0].feesForms[0].feeType;
        this.transFeeTypeCode = response[0].feesForms[0].feeTypeCode;
        this.transSetupUom = response[0].feesForms[0].setupUom;
        this.transSetupUomCode = response[0].feesForms[0].setupUomCode;
        this.transChargeUom = response[0].feesForms[0].chargeUom;
        this.transChargeUomCode = response[0].feesForms[0].chargeUomCode;
        this.setupsstfees = response[0].feesForms[1].setupFees;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.SstFeeType = response[0].feesForms[1].feeType;
        this.SstFeeTypeCode = response[0].feesForms[1].feeTypeCode;
        this.SstSetupUom = response[0].feesForms[1].setupUom;
        this.SstSetupUomCode = response[0].feesForms[1].setupUomCode;
        this.SstChargeUom = response[0].feesForms[1].chargeUom;
        this.SstChargeUomCode = response[0].feesForms[1].chargeUomCode;
        this.totalAmount = response[0].totalAmount;
        this.customerName = response[0].customerName;
        this.channelCode = response[0].channelCode;
        this.currency = response[0].currency;
        this.bankCode = bankCode; // Assign the retrieved bankCode
    }
    CashWalletTopupConfirmationComponent.prototype.ngOnInit = function () {
        if (this.status = "G01") {
            this.status = "Successful";
        }
        else {
            this.status = "Failed";
        }
        if (this.transactionMethodCode == "H01") {
            this.transactionMethod = "Online Banking";
        }
        else if (this.transactionMethodCode == "H02") {
            this.transactionMethod = "Credit Card & Debit Card";
        }
        else if (this.transactionMethodCode == "H03") {
            this.transactionMethod = "Auto Debit";
        }
        console.log("Bank Code: ", this.bankCode); // Access the bankCode value
    };
    CashWalletTopupConfirmationComponent.prototype.backButton = function () {
        this._router.navigate(['/cash-wallet-topup']);
    };
    CashWalletTopupConfirmationComponent.prototype.topupNow = function () {
        var _this = this;
        var requestBody = [{
                "acctNumber": this.acctNumber,
                "amount": this.amount,
                "walletTypeCode": this.walletTypeCode,
                "regTypeCode": this.regTypeCode,
                "uomCode": this.uomCode,
                "transactionMethodCode": "H01",
                "transactionTypeCode": this.transactionTypeCode,
                "statusCode": "G05",
                "feesForms": [
                    {
                        "feeType": this.transFeeType,
                        "feeTypeCode": this.transFeeTypeCode,
                        "setupFees": this.setuptransfees,
                        "setupUom": this.transSetupUom,
                        "setupUomCode": this.transSetupUomCode,
                        "chargeFees": this.chargeTransFee,
                        "chargeUom": this.transChargeUom,
                        "chargeUomCode": this.transChargeUomCode
                    },
                    {
                        "feeType": this.SstFeeType,
                        "feeTypeCode": this.SstFeeTypeCode,
                        "setupFees": this.setupsstfees,
                        "setupUom": this.SstSetupUom,
                        "setupUomCode": this.SstSetupUomCode,
                        "chargeFees": this.chargeSstFee,
                        "chargeUom": this.SstChargeUom,
                        "chargeUomCode": this.SstChargeUomCode
                    }
                ],
                "totalAmount": this.totalAmount,
                "channelCode": this.channelCode,
                "bicCode": this.bankCode,
                "currency": this.currency,
                "customerName": this.customerName
            }];
        //let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log("DATA SEND ", requestBody);
        var dataJson = JSON.stringify(requestBody);
        this.cashWalletService.topupWallet(dataJson)
            .pipe(rxjs_1.finalize(function () {
            _this.bankListForm.enable();
            _this.bankListForm.reset();
        }))
            .subscribe(function (Response) {
            console.log("Topup Wallet:  ", Response);
            if (Response.redirectUrl) {
                window.location.href = Response.redirectUrl;
            } //   this._router.navigate(['/RPP/MY/Redirect/RTP/RPP/MY/Redirect/RTP'], { queryParams: { data: dataJson } })
            // } else {
            //   this._router.navigate(['/cash-wallet-topup']);
            // }
            // if (Response.redirectUrl) {
            //   // window.open(Response.redirectUrl, '_self' ,);
            //   //this._router.navigate(['/RPP/MY/Redirect/RTP'], { queryParams: { data: dataJson } })
            //   //this.email = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('data'))
            // }
            // } else {
            //   this.alert = {
            //     type: 'error',
            //     message: 'Something went wrong, please try again.'
            //   };
            // }
            // this.alert = {
            //   type: 'success',
            //   message: 'Success'
            // };
            //this._router.navigate(['/cash-wallet-topup-status'], { queryParams: { data: dataJson } })
        }, function (Response) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    __decorate([
        core_1.ViewChild('bankListNgForm')
    ], CashWalletTopupConfirmationComponent.prototype, "bankListNgForm");
    CashWalletTopupConfirmationComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet-topup-confirmation',
            templateUrl: './cash-wallet-topup-confirmation.component.html',
            styleUrls: ['./cash-wallet-topup-confirmation.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], CashWalletTopupConfirmationComponent);
    return CashWalletTopupConfirmationComponent;
}());
exports.CashWalletTopupConfirmationComponent = CashWalletTopupConfirmationComponent;
// const pay = {
//   channelCode: this.channelCode,
//   bicCode: this.bankListForm.get('bankCode').value,
//   amount: this.totalAmount,
//   currency: this.currency,
//   name: this.customerName,
//   recipientReference: "testing api",
//   paymentDescription: "testing api"
// }
// this.bankListService.paynetInitiatePayment(pay)
//   .pipe(
//     finalize(() => {
//       this.bankListForm.enable();
//       this.bankListForm.reset();
//     })
//   )
//   .subscribe(
//     Response => {
//       console.log(Response);
//       if (Response.redirectUrl) {
//         window.open(Response.redirectUrl, '_blank');
//       }
//       // this.alert = {
//       //   type: 'success',
//       //   message: 'Success'
//       // };
//     },
//     Response => {
//       this.alert = {
//         type: 'error',
//         message: 'Something went wrong, please try again.'
//       };
//     }
//   );
