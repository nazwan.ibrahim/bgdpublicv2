import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm, UntypedFormBuilder, FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { CashWalletTopupBankService } from '../cash-wallet-topup-bank/cash-wallet-topup-bank.service';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { finalize, map } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';

interface RequestBody {
  acctNumber: any;
  amount: any;
  walletTypeCode: any;
  regTypeCode: any;
  uomCode: any;
  transactionMethodCode: string;
  transactionTypeCode: any;
  statusCode: string;
  feesForms?: any[]; // Add optional feesForms property
  totalAmount: any;
  channelCode: any;
  bicCode: any;
  currency: any;
  customerName: any;
}

interface FeeForm {
  feeType: string;
  feeTypeCode: string;
  setupFees: number;
  setupUom: string;
  setupUomCode: string;
  chargeFees: number;
  chargeUom: string;
  chargeUomCode: string;
}

@Component({
  selector: 'bgd-cash-wallet-topup-confirmation',
  templateUrl: './cash-wallet-topup-confirmation.component.html',
  styleUrls: ['./cash-wallet-topup-confirmation.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})


export class CashWalletTopupConfirmationComponent {

  @ViewChild('bankListNgForm') bankListNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  bankListForm: UntypedFormGroup;
  showAlert = false;
  dropdownData = [];

  today: Date = new Date();

  acctNumber: any = '';
  amount: any = '';
  walletTypeCode: any = '';
  regTypeCode: any = '';
  transactionMethodCode: any = '';
  transactionTypeCode: any = '';
  uomCode: any = '';
  statusCode: any = '';
  chargeTransFee: any = '';
  chargeSstFee: any = '';
  totalAmount: any = '';
  customerName: any = '';
  channelCode: any = '';
  currency: any = '';
  EndToEndId: any;
  EndToEndIdSignature: any;
  setuptransfees: any;
  setupsstfees: any;
  transFeeType: any;
  transFeeTypeCode: any;
  transSetupUom: any;
  transSetupUomCode: any;
  transChargeUom: any;
  transChargeUomCode: any;
  SstFeeType: any;
  SstFeeTypeCode: any;
  SstSetupUom: any;
  SstSetupUomCode: any;
  SstChargeUom: any;
  SstChargeUomCode: any;
  feesForms: [];
  status: any;
  transactionMethod: any;
  bankCode: any;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private cashWalletService: CashWalletService,
    private walletService: WalletService
  ) {}

  ngOnInit() {

    const response = this.walletService.initTopup;
    this.acctNumber = response[0].acctNumber;
    this.amount = response[0].amount;
    this.walletTypeCode = response[0].walletTypeCode;
    this.regTypeCode = response[0].regTypeCode;
    this.uomCode = response[0].uomCode;
    this.transactionMethodCode = response[0].transactionMethodCode != null ? response[0].transactionMethodCode : "H01";
    this.transactionTypeCode = response[0].transactionTypeCode;
    this.statusCode = response[0].statusCode;
    this.setuptransfees = response[0].feesForms[0].setupFees;
    this.chargeTransFee = response[0].feesForms[0].chargeFees;
    this.transFeeType = response[0].feesForms[0].feeType;
    this.transFeeTypeCode = response[0].feesForms[0].feeTypeCode;
    this.transSetupUom = response[0].feesForms[0].setupUom;
    this.transSetupUomCode = response[0].feesForms[0].setupUomCode;
    this.transChargeUom = response[0].feesForms[0].chargeUom;
    this.transChargeUomCode = response[0].feesForms[0].chargeUomCode;

    this.setupsstfees = response[0].feesForms[1].setupFees;
    this.chargeSstFee = response[0].feesForms[1].chargeFees;
    this.SstFeeType = response[0].feesForms[1].feeType;
    this.SstFeeTypeCode = response[0].feesForms[1].feeTypeCode;
    this.SstSetupUom = response[0].feesForms[1].setupUom;
    this.SstSetupUomCode = response[0].feesForms[1].setupUomCode;
    this.SstChargeUom = response[0].feesForms[1].chargeUom;
    this.SstChargeUomCode = response[0].feesForms[1].chargeUomCode;

    this.totalAmount = response[0].totalAmount;
    this.customerName = response[0].customerName;
    this.channelCode = response[0].channelCode;
    this.currency = response[0].currency;

    this.bankCode = this.walletService.bankCodeTopup;

    if (this.status = "G01") {
      this.status = "Successful";
    } else {
      this.status = "Failed";
    }

    if (this.transactionMethodCode == "H01") {
      this.transactionMethod = "Online Banking";
    } else if (this.transactionMethodCode == "H02") {
      this.transactionMethod = "Credit Card & Debit Card";
    } else if (this.transactionMethodCode == "H03") {
      this.transactionMethod = "Auto Debit";
    }

  }

  backButton(): void {
    this._router.navigate(['/cash-wallet-topup']);
  }

  topupNow(): void {

    const requestBody = [{
      "acctNumber": this.acctNumber,
      "amount": this.amount,
      "walletTypeCode": this.walletTypeCode,
      "regTypeCode": this.regTypeCode,
      "uomCode": this.uomCode,
      "transactionMethodCode": "H01",
      "transactionTypeCode": this.transactionTypeCode,
      "statusCode": "G05",
      "feesForms": [
        {
          "feeType": this.transFeeType,
          "feeTypeCode": this.transFeeTypeCode,
          "setupFees": this.setuptransfees,
          "setupUom": this.transSetupUom,
          "setupUomCode": this.transSetupUomCode,
          "chargeFees": this.chargeTransFee,
          "chargeUom": this.transChargeUom,
          "chargeUomCode": this.transChargeUomCode
        },
        {
          "feeType": this.SstFeeType,
          "feeTypeCode": this.SstFeeTypeCode,
          "setupFees": this.setupsstfees,
          "setupUom": this.SstSetupUom,
          "setupUomCode": this.SstSetupUomCode,
          "chargeFees": this.chargeSstFee,
          "chargeUom": this.SstChargeUom,
          "chargeUomCode": this.SstChargeUomCode
        }
      ],
      "totalAmount": this.totalAmount,
      "channelCode": this.channelCode,
      "bicCode": this.bankCode,
      "currency": this.currency,
      "customerName": this.customerName
    }]


    console.log("Requestbody:", requestBody);

    const data = JSON.stringify(requestBody);


    this.cashWalletService.topupWallet(data)
      .pipe(
        finalize(() => {
          this.bankListForm.enable();

          this.bankListForm.reset();
        })
      )
      .subscribe(
        Response => {
          console.log("confirmTopup Response:", Response);


          if (Response.redirectUrl) {
            window.open(Response.redirectUrl, '_self',);
          }          //   this._router.navigate(['/RPP/MY/Redirect/RTP/RPP/MY/Redirect/RTP'], { queryParams: { data: dataJson } })
          // } else {
          //   this._router.navigate(['/cash-wallet-topup']);
          // }
          // if (Response.redirectUrl) {
          //   // window.open(Response.redirectUrl, '_self' ,);




          //   //this._router.navigate(['/RPP/MY/Redirect/RTP'], { queryParams: { data: dataJson } })
          //   //this.email = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('data'))
          // }

          // } else {
          //   this.alert = {
          //     type: 'error',
          //     message: 'Something went wrong, please try again.'
          //   };
          // }
          // this.alert = {
          //   type: 'success',
          //   message: 'Success'
          // };
          //this._router.navigate(['/cash-wallet-topup-status'], { queryParams: { data: dataJson } })
        },
        error => {
          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };
        }
      );
  }
}
