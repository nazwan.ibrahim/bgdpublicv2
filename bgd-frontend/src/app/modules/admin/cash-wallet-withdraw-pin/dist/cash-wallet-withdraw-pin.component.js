"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletWithdrawPinComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var CashWalletWithdrawPinComponent = /** @class */ (function () {
    function CashWalletWithdrawPinComponent(_activatedRoute, _formBuilder, _router, user, cashWalletService) {
        this._activatedRoute = _activatedRoute;
        this._formBuilder = _formBuilder;
        this._router = _router;
        this.user = user;
        this.cashWalletService = cashWalletService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.acctNumber = '';
        this.amount = '';
        this.walletTypeCode = '';
        this.regTypeCode = '';
        this.transactionMethodCode = '';
        this.transactionTypeCode = '';
        this.uomCode = '';
        this.statusCode = '';
        this.chargeTransFee = '';
        this.chargeSstFee = '';
        this.totalAmount = '';
        this.disableButton = false;
        this.accountNumber = this._activatedRoute.snapshot.queryParams['data'];
        this.bankName = this._activatedRoute.snapshot.queryParams['bank'];
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.acctNumber = response[0].acctNumber;
        this.amount = response[0].amount;
        this.walletTypeCode = response[0].walletTypeCode;
        this.regTypeCode = response[0].regTypeCode;
        this.uomCode = response[0].uomCode;
        this.transactionMethodCode = response[0].transactionMethodCode;
        this.transactionTypeCode = response[0].transactionTypeCode;
        this.statusCode = response[0].statusCode;
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.transSetupFee = response[0].feesForms[0].setupFees;
        this.transSetupUom = response[0].feesForms[0].setupUom;
        this.transSetupUomCode = response[0].feesForms[0].setupUomCode;
        this.transFeeType = response[0].feesForms[0].feeType;
        this.transFeeTypeCode = response[0].feesForms[0].feeTypeCode;
        this.transChargeUom = response[0].feesForms[0].chargeUom;
        this.transChargeUomCode = response[0].feesForms[0].chargeUomCode;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.SstSetupFee = response[0].feesForms[1].setupFees;
        this.SstSetupUom = response[0].feesForms[1].setupUom;
        this.SstSetupUomCode = response[0].feesForms[1].setupUomCode;
        this.SstFeeType = response[0].feesForms[1].feeType;
        this.SstFeeTypeCode = response[0].feesForms[1].feeTypeCode;
        this.SstChargeUom = response[0].feesForms[1].chargeUom;
        this.SstChargeUomCode = response[0].feesForms[1].chargeUomCode;
        this.totalAmount = response[0].totalAmount;
    }
    CashWalletWithdrawPinComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log(this.response);
        this.user.getUserDetails().subscribe(function (data) {
            _this.email = data.email;
            _this.username = data.userName;
        });
        console.log(this.amount);
    };
    CashWalletWithdrawPinComponent.prototype.backButton = function () {
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this._router.navigate(['/cash-wallet-withdraw-bank'], {
            queryParams: {
                response: JSON.stringify(response)
            }
        });
    };
    CashWalletWithdrawPinComponent.prototype.pincodeCompleted = function (pin) {
        this.pin = pin;
    };
    CashWalletWithdrawPinComponent.prototype.confirmWithdraw = function () {
        var _this = this;
        this.disableButton = true;
        var amount = this.amount.toFixed(2);
        console.log(this.transFeeTypeCode);
        var requestBody = [{
                "acctNumber": this.acctNumber,
                "amount": amount.toString(),
                "walletTypeCode": this.walletTypeCode,
                "regTypeCode": this.regTypeCode,
                "uomCode": this.uomCode,
                "transactionMethodCode": "H01",
                "transactionTypeCode": this.transactionTypeCode,
                "statusCode": "G01",
                "feesForms": [
                    {
                        "feeType": this.transFeeType,
                        "feeTypeCode": this.transFeeTypeCode,
                        "setupFees": this.transSetupFee,
                        "setupUom": this.transSetupUom,
                        "setupUomCode": this.transSetupUomCode,
                        "chargeFees": this.chargeTransFee,
                        "chargeUom": this.transChargeUom,
                        "chargeUomCode": this.transChargeUomCode
                    },
                    {
                        "feeType": this.SstFeeType,
                        "feeTypeCode": this.SstFeeTypeCode,
                        "setupFees": this.SstSetupFee,
                        "setupUom": this.SstSetupUom,
                        "setupUomCode": this.SstSetupUomCode,
                        "chargeFees": this.chargeSstFee,
                        "chargeUom": this.SstChargeUom,
                        "chargeUomCode": this.SstChargeUomCode
                    }
                ],
                "totalAmount": this.totalAmount
            }];
        //let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log('Request Body : ', requestBody);
        var dataJson = JSON.stringify(requestBody);
        this.cashWalletService.withdrawWallet(dataJson)
            .subscribe(function (Response) {
            _this.disableButton = true;
            _this.alert = {
                type: 'success',
                message: 'Success'
            };
            var queryParams = {
                Response: JSON.stringify(Response),
                response: dataJson,
                account: _this.accountNumber,
                bank: _this.bankName
            };
            setTimeout(function () {
                _this._router.navigate(['/cash-wallet-withdraw-status'], { queryParams: queryParams });
            }, 500); // 2000 milliseconds = 2 seconds
        }, function (Response) {
            _this.disableButton = false;
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    CashWalletWithdrawPinComponent.prototype.verifyPin = function () {
        var _this = this;
        this.disableButton = true;
        var pin = {
            userName: this.username,
            email: this.email,
            userPin: this.pin
        };
        console.log(pin);
        console.log(this.disableButton);
        this.cashWalletService.verifyPin(pin)
            .subscribe(function (Response) {
            _this.alert = {
                type: 'success',
                message: 'PIN is valid'
            };
            _this.confirmWithdraw();
        }, function (error) {
            _this.disableButton = false;
            _this.alert = {
                type: 'error',
                message: 'Incorrect PIN.'
            };
            _this.showAlert = true;
        });
        this.showAlert = true;
    };
    CashWalletWithdrawPinComponent.prototype.forgotPin = function () {
        this._router.navigate(['/forgot-pin']);
    };
    __decorate([
        core_1.ViewChild('cashWalletWithdrawPinNgForm')
    ], CashWalletWithdrawPinComponent.prototype, "cashWalletWithdrawPinNgForm");
    CashWalletWithdrawPinComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet-withdraw-pin',
            templateUrl: './cash-wallet-withdraw-pin.component.html',
            styleUrls: ['./cash-wallet-withdraw-pin.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], CashWalletWithdrawPinComponent);
    return CashWalletWithdrawPinComponent;
}());
exports.CashWalletWithdrawPinComponent = CashWalletWithdrawPinComponent;
