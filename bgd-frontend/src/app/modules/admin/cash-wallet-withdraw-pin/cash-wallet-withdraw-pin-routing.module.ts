import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletWithdrawPinComponent } from './cash-wallet-withdraw-pin.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletWithdrawPinComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletWithdrawPinRoutingModule { }
