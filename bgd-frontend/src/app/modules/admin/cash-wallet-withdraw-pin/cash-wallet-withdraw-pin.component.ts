import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from '../bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-cash-wallet-withdraw-pin',
  templateUrl: './cash-wallet-withdraw-pin.component.html',
  styleUrls: ['./cash-wallet-withdraw-pin.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CashWalletWithdrawPinComponent {

  @ViewChild('cashWalletWithdrawPinNgForm') cashWalletWithdrawPinNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  cashWalletWithdrawPinForm: UntypedFormGroup;
  showAlert = false;
  pin: any;
  email: any;
  username: any;
  acctNumber: any = '';
  amount: any = '';
  walletTypeCode: any = '';
  regTypeCode: any = '';
  transactionMethodCode: any = '';
  transactionTypeCode: any = '';
  uomCode: any = '';
  statusCode: any = '';
  chargeTransFee: any = '';
  chargeSstFee: any = '';
  totalAmount: any = '';
  accountNumber: any;
  bankName: any;
  transSetupFee: any;
  SstSetupFee: any;
  transSetupUom: any;
  transSetupUomCode: any;
  transFeeType: any;
  transFeeTypeCode: any;
  SstSetupUom: any;
  SstSetupUomCode: any;
  SstFeeType: any;
  SstFeeTypeCode: any;
  transChargeUom: any;
  transChargeUomCode: any;
  SstChargeUom: any;
  SstChargeUomCode: any;
  response: any;
  disableButton: boolean = false;
  referenceId: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: UntypedFormBuilder,
    private _router: Router,
    private location: Location,
    private cashWalletService: CashWalletService,
    private walletService: WalletService
  ) {}

  ngOnInit(): void {

    this.bankName = this.walletService.bankName;
    this.accountNumber = this.walletService.accountBank;
    this.email = this.walletService.email;
    this.username = this.walletService.username;

    const response = this.walletService.initWithdraw;
    this.acctNumber = response[0].acctNumber;
    this.amount = response[0].amount;
    this.walletTypeCode = response[0].walletTypeCode;
    this.regTypeCode = response[0].regTypeCode;
    this.uomCode = response[0].uomCode;
    this.transactionMethodCode = response[0].transactionMethodCode;
    this.transactionTypeCode = response[0].transactionTypeCode;
    this.statusCode = response[0].statusCode;
    this.chargeTransFee = response[0].feesForms[0].chargeFees;
    this.transSetupFee = response[0].feesForms[0].setupFees;
    this.transSetupUom = response[0].feesForms[0].setupUom;
    this.transSetupUomCode = response[0].feesForms[0].setupUomCode;
    this.transFeeType = response[0].feesForms[0].feeType;
    this.transFeeTypeCode = response[0].feesForms[0].feeTypeCode;
    this.transChargeUom = response[0].feesForms[0].chargeUom;
    this.transChargeUomCode = response[0].feesForms[0].chargeUomCode;
    this.chargeSstFee = response[0].feesForms[1].chargeFees;
    this.SstSetupFee = response[0].feesForms[1].setupFees;
    this.SstSetupUom = response[0].feesForms[1].setupUom;
    this.SstSetupUomCode = response[0].feesForms[1].setupUomCode;
    this.SstFeeType = response[0].feesForms[1].feeType;
    this.SstFeeTypeCode = response[0].feesForms[1].feeTypeCode;
    this.SstChargeUom = response[0].feesForms[1].chargeUom;
    this.SstChargeUomCode = response[0].feesForms[1].chargeUomCode;
    this.totalAmount = response[0].totalAmount;
    this.referenceId = response[0].referenceId;
  }

  back(): void {
    this.location.back();
  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  confirmWithdraw(): void {

    this.disableButton = true;

    const requestBody = 
    {
      "userName": this.username,
      "userPin": this.pin,
      "referenceId": this.referenceId
    }

    console.log('RequestBody:', requestBody);

    const data = JSON.stringify(requestBody);

    this.cashWalletService.withdrawWallet(data)
      .subscribe(
        Response => {
          console.log("confirmWitdraw Response:", Response);

          this.disableButton = true;

          this.response = Response;
          this.walletService.withdrawResponse = this.response;

          if (Response.status === 'G01') {
            setTimeout(() => {
              this._router.navigate(['/cash-wallet-withdraw-status']);
            }, 500);
          } else if (Response.status === 'G05') {
            setTimeout(() => {
              this._router.navigate(['/cash-wallet-withdraw-pending']);
            }, 500);
          } else {
            setTimeout(() => {
              this._router.navigate(['/cash-wallet-withdraw-failed']);
            }, 500);
          }

        },
        error => {

          this.disableButton = false;

          if(error.status === 401) {

            this.alert = {
              type: 'error',
              message: 'Wrong PIN number'
            };
            
          } else {

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };

          }

          this.showAlert = true;

          setTimeout(() => {
            this.showAlert = false;
          }, 1000);

        }
      );
  }

  forgotPin() {
    this._router.navigate(['/forgot-pin']);
  }

}
