import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletWithdrawPinComponent } from './cash-wallet-withdraw-pin.component';

describe('CashWalletWithdrawPinComponent', () => {
  let component: CashWalletWithdrawPinComponent;
  let fixture: ComponentFixture<CashWalletWithdrawPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletWithdrawPinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletWithdrawPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
