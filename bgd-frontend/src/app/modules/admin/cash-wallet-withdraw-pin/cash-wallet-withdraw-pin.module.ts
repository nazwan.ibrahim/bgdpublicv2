import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { CashWalletWithdrawPinComponent } from './cash-wallet-withdraw-pin.component';
import { CashWalletWithdrawPinRoutingModule } from './cash-wallet-withdraw-pin-routing.module';
import { NgxPincodeModule } from 'ngx-pincode';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
    declarations: [
        CashWalletWithdrawPinComponent
    ],
    imports: [
        CommonModule,
        CashWalletWithdrawPinRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatRadioModule,
        MatStepperModule,
        MatSelectModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule,
        MatDividerModule,
        MatListModule,
        NgxPincodeModule,
        MatDialogModule
    ]
})
export class CashWalletWithdrawPinModule { }
