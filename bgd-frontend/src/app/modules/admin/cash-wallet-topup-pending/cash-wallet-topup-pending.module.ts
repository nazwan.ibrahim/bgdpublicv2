import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashWalletTopupPendingComponent } from './cash-wallet-topup-pending.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletTopupPendingRoutingModule } from './cash-wallet-topup-pending-routing.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    CashWalletTopupPendingComponent
  ],
  imports: [
    CommonModule,
    CashWalletTopupPendingRoutingModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class CashWalletTopupPendingModule { }
