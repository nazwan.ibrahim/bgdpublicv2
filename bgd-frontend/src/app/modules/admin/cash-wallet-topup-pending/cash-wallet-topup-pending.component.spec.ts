import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupPendingComponent } from './cash-wallet-topup-pending.component';

describe('CashWalletTopupPendingComponent', () => {
  let component: CashWalletTopupPendingComponent;
  let fixture: ComponentFixture<CashWalletTopupPendingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupPendingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
