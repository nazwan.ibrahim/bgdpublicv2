import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupPendingComponent } from 'app/modules/admin/cash-wallet-topup-pending/cash-wallet-topup-pending.component';

const routes: Routes = [
  {
    path: '',
    component: CashWalletTopupPendingComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletTopupPendingRoutingModule { }
