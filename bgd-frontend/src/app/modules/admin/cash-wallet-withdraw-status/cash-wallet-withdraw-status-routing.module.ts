import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletWithdrawStatusComponent } from 'app/modules/admin/cash-wallet-withdraw-status/cash-wallet-withdraw-status.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletWithdrawStatusComponent
        // resolve  : {
        //   data: CashWalletTopupResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletWithdrawStatusRoutingModule { }
