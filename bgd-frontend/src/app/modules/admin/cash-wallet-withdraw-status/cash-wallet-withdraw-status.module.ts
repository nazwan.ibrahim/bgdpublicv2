import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashWalletWithdrawStatusComponent } from './cash-wallet-withdraw-status.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletWithdrawStatusRoutingModule } from './cash-wallet-withdraw-status-routing.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
    declarations: [
        CashWalletWithdrawStatusComponent
    ],
    imports: [
        CommonModule,
        CashWalletWithdrawStatusRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class CashWalletWithdrawStatusModule { }
