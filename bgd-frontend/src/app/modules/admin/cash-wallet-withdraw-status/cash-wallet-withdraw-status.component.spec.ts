import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletWithdrawStatusComponent } from './cash-wallet-withdraw-status.component';

describe('CashWalletWithdrawStatusComponent', () => {
  let component: CashWalletWithdrawStatusComponent;
  let fixture: ComponentFixture<CashWalletWithdrawStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletWithdrawStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletWithdrawStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
