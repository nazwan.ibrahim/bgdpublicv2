"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MyAccountModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var my_account_component_1 = require("app/modules/admin/my-account/my-account.component");
var my_account_routing_1 = require("app/modules/admin/my-account/my-account.routing");
var button_1 = require("@angular/material/button");
var otp_account_bankdetails_component_1 = require("./cards/otp-account-bankdetails/otp-account-bankdetails.component");
var otp_change_password_component_1 = require("./cards/otp-change-password/otp-change-password.component");
//import { ChangePasswordNewComponent } from './cards/change-password-new/change-password-new.component';
// import { UserProfileComponent } from './cards/user-profile/user-profile.component';
// import { EditProfileComponent } from './cards/edit-profile/edit-profile.component';
var icon_1 = require("@angular/material/icon");
//import { ChangePinConfirmComponent } from './cards/change-pin-confirm/change-pin-confirm.component';
//import { ChangePinNewComponent } from './cards/change-pin-new/change-pin-new.component';
//import { ChangePinComponent } from './cards/change-pin/change-pin.component';
//import { SecurityComponent } from './cards/security/security.component';
//import { ChangePasswordPinComponent } from './cards/change-password-pin/change-password-pin.component';
var MyAccountModule = /** @class */ (function () {
    function MyAccountModule() {
    }
    MyAccountModule = __decorate([
        core_1.NgModule({
            declarations: [
                my_account_component_1.MyAccountComponent,
                otp_account_bankdetails_component_1.OtpAccountBankdetailsComponent,
                otp_change_password_component_1.OtpChangePasswordComponent,
            ],
            imports: [
                router_1.RouterModule.forChild(my_account_routing_1.MyAccountRoutes),
                button_1.MatButtonModule,
                icon_1.MatIconModule,
                common_1.CommonModule,
            ]
        })
    ], MyAccountModule);
    return MyAccountModule;
}());
exports.MyAccountModule = MyAccountModule;
