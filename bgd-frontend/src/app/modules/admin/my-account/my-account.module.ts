import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MyAccountComponent } from 'app/modules/admin/my-account/my-account.component';
import { MyAccountRoutes } from 'app/modules/admin/my-account/my-account.routing';
import { MatButtonModule } from '@angular/material/button';
import { OtpAccountBankdetailsComponent } from './cards/otp-account-bankdetails/otp-account-bankdetails.component';
import { OtpChangePasswordComponent } from './cards/otp-change-password/otp-change-password.component';
//import { ChangePasswordNewComponent } from './cards/change-password-new/change-password-new.component';
// import { UserProfileComponent } from './cards/user-profile/user-profile.component';
// import { EditProfileComponent } from './cards/edit-profile/edit-profile.component';
import { MatIconModule } from '@angular/material/icon';
//import { ChangePinConfirmComponent } from './cards/change-pin-confirm/change-pin-confirm.component';
//import { ChangePinNewComponent } from './cards/change-pin-new/change-pin-new.component';
//import { ChangePinComponent } from './cards/change-pin/change-pin.component';
//import { SecurityComponent } from './cards/security/security.component';
//import { ChangePasswordPinComponent } from './cards/change-password-pin/change-password-pin.component';


@NgModule({
    declarations: [
        MyAccountComponent,
        OtpAccountBankdetailsComponent,
        OtpChangePasswordComponent,
        //ChangePinConfirmComponent,
        //ChangePinNewComponent,
        //ChangePinComponent,
        //SecurityComponent,
        //ChangePasswordPinComponent,
        //ChangePasswordNewComponent,
        // UserProfileComponent,
        // EditProfileComponent
    ],
    imports: [
        RouterModule.forChild(MyAccountRoutes),
        MatButtonModule,
        MatIconModule,
        CommonModule,
        //RouterModule.forRoot(routes),
    ]


})
export class MyAccountModule {
}
