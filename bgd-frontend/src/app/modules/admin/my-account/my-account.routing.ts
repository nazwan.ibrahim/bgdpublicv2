import { Route } from '@angular/router';
import { MyAccountComponent } from 'app/modules/admin/my-account/my-account.component';

export const MyAccountRoutes: Route[] = [
    {
        path     : '',
        component: MyAccountComponent
    }
];
