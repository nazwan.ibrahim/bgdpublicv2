import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyAccountService {

  //Change Password
  encryptedPassword: any;
  encryptedNewPassword: any;
  encryptedconfirmPassword: any;
  currentPassword: any;
  
  //Change Pin
  pin: any;
  newPIN: any;
  oldPin: any;
  constructor() { }
}
