import { Route } from '@angular/router';
import { SecurityComponent } from 'app/modules/admin/my-account/cards/security/security.component';

export const SecurityRoutes: Route[] = [
    {
        path: '',
        component: SecurityComponent
    }
];
