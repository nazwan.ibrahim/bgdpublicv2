import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SecurityComponent } from 'app/modules/admin/my-account/cards/security/security.component';
import { SecurityRoutes } from 'app/modules/admin/my-account/cards/security/security.routing';

@NgModule({
    declarations: [
        SecurityComponent
    ],
    imports: [
        RouterModule.forChild(SecurityRoutes),
        CommonModule,
        SharedModule,
        MatIconModule,
        MatButtonModule
    ]
})

export class SecurityModule {
}
