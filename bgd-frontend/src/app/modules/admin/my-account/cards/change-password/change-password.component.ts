import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { AES } from 'crypto-js';
import { MyAccountService } from '../../my-account.service';

@Component({
  selector: 'bgd-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent {

  @ViewChild('changePassFormNgForm') changePassFormNgForm: NgForm;

  oldPassword: any;
  newPassword: any;
  newPassConfirm: any;
  changePassForm: FormGroup;
  hidePassword = true;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private cashWalletService: CashWalletService,
    private dashboardService: DashboardService,
    private myAccountService: MyAccountService,
  ) {
  }

  ngOnInit(): void {
    // Create the form
    this.changePassForm = this._formBuilder.group({
      oldPassword: ['', [Validators.required]],
    });
  }

  togglePasswordVisibility() {
    this.hidePassword = !this.hidePassword;
  }

  continueButton() {
    const oldPassword = this.changePassForm.get('oldPassword').value;
    this.myAccountService.encryptedPassword = AES.encrypt(oldPassword, 'encryptionSecretKey').toString();

    // console.log('Current Password:', oldPassword);
    this._router.navigate(['/change-password-new'], 
    // {
    //   queryParams: {
    //     data: encryptedPassword,
    //   }
    // }
    );
  }
};

