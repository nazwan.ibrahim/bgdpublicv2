import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ChangePasswordComponent } from 'app/modules/admin/my-account/cards/change-password/change-password.component';
import { ChangePasswordRoutes } from 'app/modules/admin/my-account/cards/change-password/change-password.routing';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';


@NgModule({
    declarations: [
        ChangePasswordComponent
    ],
    imports: [
        RouterModule.forChild(ChangePasswordRoutes),
        MatButtonModule,
        CommonModule,
        //AuthService,
        SharedModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        MatIconModule
    ]
})

export class ChangePasswordModule {
}
