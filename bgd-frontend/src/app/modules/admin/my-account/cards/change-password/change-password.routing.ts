import { Route } from '@angular/router';
import { ChangePasswordComponent } from 'app/modules/admin/my-account/cards/change-password/change-password.component';

export const ChangePasswordRoutes: Route[] = [
    {
        path: '',
        component: ChangePasswordComponent
    }
];
