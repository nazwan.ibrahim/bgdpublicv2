import { Route } from '@angular/router';
import { AccountBankdetailsComponent } from 'app/modules/admin/my-account/cards/account-bankdetails/account-bankdetails.component';

export const AccountBankdetailsRoute: Route[] = [
    {
        path     : '',
        component: AccountBankdetailsComponent
    }
];
