import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccountBankdetailsComponent } from 'app/modules/admin/my-account/cards/account-bankdetails/account-bankdetails.component';
import { AccountBankdetailsRoute } from 'app/modules/admin/my-account/cards/account-bankdetails/account-bankdetails.routing';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
    declarations: [
        AccountBankdetailsComponent
    ],
    imports     : [
        RouterModule.forChild(AccountBankdetailsRoute),
        MatInputModule,
        MatIconModule,
    ]
})
export class AccountBankDetailsModule
{
}
