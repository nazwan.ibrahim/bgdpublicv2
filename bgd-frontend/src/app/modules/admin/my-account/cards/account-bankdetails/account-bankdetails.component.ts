import { Component } from '@angular/core';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'bgd-account-bankdetails',
  templateUrl: './account-bankdetails.component.html',
  styleUrls: ['./account-bankdetails.component.scss']
})

export class AccountBankdetailsComponent {
  bankName: any;
  accountNumber: any;
  
  constructor(
    private _DashboardService: DashboardService,
    private router: Router,
  ){}

  ngOnInit() {
    this._DashboardService.getBankAccountDetails().subscribe(dataDetails => {

      if (dataDetails.length === 0) {
        this.bankName = 'No Bank Available';
        this.accountNumber = 'No Account Number';
      } else {
        this.bankName = dataDetails[0].bankName;
        this.accountNumber = dataDetails[0].accountNumber;
      }
      console.log(dataDetails);
    })
  }

  editBankDetails(){
     this.router.navigate(['/edit-account-bankdetails'])
  }

}
