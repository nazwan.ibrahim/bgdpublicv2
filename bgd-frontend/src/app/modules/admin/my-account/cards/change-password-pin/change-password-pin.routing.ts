import { Route } from '@angular/router';
import { ChangePasswordPinComponent } from 'app/modules/admin/my-account/cards/change-password-pin/change-password-pin.component';

export const ChangePasswordPinRoutes: Route[] = [
    {
        path: '',
        component: ChangePasswordPinComponent
    }
];
