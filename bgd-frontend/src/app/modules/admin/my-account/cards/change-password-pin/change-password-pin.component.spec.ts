import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordPinComponent } from './change-password-pin.component';

describe('ChangePasswordPinComponent', () => {
  let component: ChangePasswordPinComponent;
  let fixture: ComponentFixture<ChangePasswordPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePasswordPinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangePasswordPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
