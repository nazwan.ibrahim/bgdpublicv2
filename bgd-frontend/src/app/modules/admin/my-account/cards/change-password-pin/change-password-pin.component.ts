import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { timer } from 'rxjs';
import { AES } from 'crypto-js';
import CryptoJS from 'crypto-js';
import { ChangePasswordSuccessAlertDialog } from './change-password-success-alert-dialog/change-passsword-success-alert-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MyAccountService } from '../../my-account.service';

@Component({
  selector: 'bgd-change-password-pin',
  templateUrl: './change-password-pin.component.html',
  styleUrls: ['./change-password-pin.component.scss'],
  animations: [
    trigger('shake', [
      // Animation configuration
    ])
  ]
})
export class ChangePasswordPinComponent {
  oldPassword: any;
  newPassword: any;
  newPassConfirm: any;
  changePassForm: FormGroup;
  showAlert = true;
  username: any;
  email: any;
  pin: any;
  disableButton: boolean = false;


  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: '',
  };
  isDialogOpen: boolean;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private AuthService: AuthService,
    private _formBuilder: FormBuilder,
    private user: DashboardService,
    private _bgdService: bgdService,
    public dialog: MatDialog,
    private myAccountService: MyAccountService,
  ) {}
  // {
  //   const currentPassword = this._activatedRoute.snapshot.queryParamMap.get('currentPassword');
  //   this.oldPassword = AES.decrypt(currentPassword, 'encryptionSecretKey').toString(CryptoJS.enc.Utf8);
  //   const newPassword = this._activatedRoute.snapshot.queryParamMap.get('newPassword');
  //   this.newPassword = AES.decrypt(newPassword, 'encryptionSecretKey').toString(CryptoJS.enc.Utf8);
  //   const confirmPassword = this._activatedRoute.snapshot.queryParamMap.get('confirmPassword');
  //   this.newPassConfirm = AES.decrypt(confirmPassword, 'encryptionSecretKey').toString(CryptoJS.enc.Utf8);
  // }

  ngOnInit(): void {
    const currentPassword = this.myAccountService.currentPassword;
    this.oldPassword = AES.decrypt(currentPassword, 'encryptionSecretKey').toString(CryptoJS.enc.Utf8);
    const newPassword = this.myAccountService.encryptedNewPassword;
    this.newPassword = AES.decrypt(newPassword, 'encryptionSecretKey').toString(CryptoJS.enc.Utf8);
    const confirmPassword = this.myAccountService.encryptedconfirmPassword;
    this.newPassConfirm =  AES.decrypt(confirmPassword, 'encryptionSecretKey').toString(CryptoJS.enc.Utf8);

    this.user.getUserDetails().subscribe((data) => {
      this.email = data.email;
      this.username = data.userName;
    });
  }

  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(ChangePasswordSuccessAlertDialog, {
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  confirmChange() {
    const changePassword = {
      oldPassword: this.oldPassword,
      newPassword: this.newPassword,
      confirmPassword: this.newPassConfirm
    };

    this.AuthService.ChangePassword(changePassword).subscribe(
      (response) => {

        this.openDialog();

      },
      (error) => {

        if (error.status == 400) {

          this.alert = {
            type: 'error',
            message: 'Your password must be different from the previous password.'
          };

        } else {

          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };

        }

        this.showAlert = true;

        setTimeout(() => {
          this.showAlert = false;
        }, 1000);

      }
    );
  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  verifyPin() {

    this.disableButton = true;

    const pin = {
      userName: this.username,
      email: this.email,
      userPin: this.pin,
    };

    console.log(pin);

    this._bgdService.verifyPin(pin).subscribe(
      (Response) => {

        this.confirmChange();

      },
      (Response) => {

        this.disableButton = false;

        this.alert = {
          type: 'error',
          message: 'Invalid PIN',
        };

        this.showAlert = true;

        setTimeout(() => {
          this.showAlert = false;
        }, 1000);
        
      }
    );
  }

  forgotPin() {
    this._router.navigate(['/forgot-pin']);
  }
}
