"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ChangePasswordPinComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@angular/animations");
var rxjs_1 = require("rxjs");
var ChangePasswordPinComponent = /** @class */ (function () {
    function ChangePasswordPinComponent(_activatedRoute, _router, AuthService, _formBuilder, user, _bgdService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.AuthService = AuthService;
        this._formBuilder = _formBuilder;
        this.user = user;
        this._bgdService = _bgdService;
        this.showAlert = true;
        this.disableButton = false;
        this.alert = {
            type: 'success',
            message: ''
        };
    }
    ChangePasswordPinComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user.getUserDetails().subscribe(function (data) {
            _this.email = data.email;
            _this.username = data.userName;
        });
    };
    ChangePasswordPinComponent.prototype.confirmChange = function () {
        var _this = this;
        var changePassword = {
            oldPassword: decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('currentPassword')),
            newPassword: decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('newPassword')),
            confirmPassword: decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('confirmPassword'))
        };
        this.AuthService.ChangePassword(changePassword).subscribe(function (response) {
            _this.alert = {
                type: 'success',
                message: 'Your password has been changed.'
            };
            _this.showAlert = true;
            rxjs_1.timer(1000).subscribe(function () {
                _this._router.navigate(['/dashboard']);
            });
        }, function (response) {
            _this.alert = {
                type: 'error',
                message: 'Confirm password does not match the entered new password.'
            };
            _this.showAlert = true;
        });
    };
    ChangePasswordPinComponent.prototype.pincodeCompleted = function (pin) {
        this.pin = pin;
    };
    ChangePasswordPinComponent.prototype.verifyPin = function () {
        var _this = this;
        this.disableButton = true;
        var pin = {
            userName: this.username,
            email: this.email,
            userPin: this.pin
        };
        console.log(pin);
        this._bgdService.verifyPin(pin).subscribe(function (Response) {
            _this.alert = {
                type: 'success',
                message: 'Verified'
            };
            _this.confirmChange();
        }, function (Response) {
            _this.disableButton = false;
            _this.alert = {
                type: 'error',
                message: 'Invalid PIN'
            };
        });
    };
    ChangePasswordPinComponent.prototype.forgotPin = function () {
        this._router.navigate(['/forgot-pin']);
    };
    ChangePasswordPinComponent = __decorate([
        core_1.Component({
            selector: 'bgd-change-password-pin',
            templateUrl: './change-password-pin.component.html',
            styleUrls: ['./change-password-pin.component.scss'],
            animations: [
                animations_1.trigger('shake', [
                // Animation configuration
                ])
            ]
        })
    ], ChangePasswordPinComponent);
    return ChangePasswordPinComponent;
}());
exports.ChangePasswordPinComponent = ChangePasswordPinComponent;
