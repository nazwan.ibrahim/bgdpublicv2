import { Route } from '@angular/router';
import { SettingsComponent } from 'app/modules/admin/my-account/cards/settings/settings.component';

export const SettingsRoutes: Route[] = [
    {
        path     : '',
        component: SettingsComponent
    }
];
