import { Route, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { SelectLanguageComponent } from './select-language.component';

export const SelectLanguageRoutes: Route[] = [
  {
      path     : '',
      component: SelectLanguageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(SelectLanguageRoutes)],
  exports: [RouterModule]
})
export class SelectLanguageRoutingModule { }