import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SettingsComponent } from 'app/modules/admin/my-account/cards/settings/settings.component';
import { SettingsRoutes } from 'app/modules/admin/my-account/cards/settings/settings.routing';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SelectLanguageComponent } from './select-language/select-language.component';


@NgModule({
    declarations: [
        SettingsComponent,
    ],
    imports     : [
        RouterModule.forChild(SettingsRoutes),
        MatIconModule,
        MatButtonModule
    ]
})
export class SettingsModule
{
}
