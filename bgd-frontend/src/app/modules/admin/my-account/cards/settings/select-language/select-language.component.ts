import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AvailableLangs, TranslocoService } from '@ngneat/transloco';
import { take, of } from 'rxjs';
import { NgFor } from '@angular/common';

@Component({
  selector: 'bgd-select-language',
  templateUrl: './select-language.component.html',
  styleUrls: ['./select-language.component.scss']
})
export class SelectLanguageComponent {
  selectedLanguage: string = 'english'; // Add this line to define the selectedLanguage property

  availableLangs: AvailableLangs;
  activeLang: string;

  constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _translocoService: TranslocoService
  ) {
  }
  ngOninit() {
    this.availableLangs = this._translocoService.getAvailableLangs();

    console.log(this.availableLangs);


  }
}
