import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectLanguageComponent } from './select-language.component';
import { RouterModule } from '@angular/router';
import { SelectLanguageRoutes } from './select-language.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  declarations: [
    SelectLanguageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(SelectLanguageRoutes),
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule,
    MatSelectModule,
    HttpClientModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule,
    MatRadioModule,
  ],
  exports: [
    SelectLanguageComponent,
  ]
})
export class SelectLanguageModule { }
