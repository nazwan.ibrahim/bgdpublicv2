import { Route } from '@angular/router';
import { HelpSupportComponent } from 'app/modules/admin/my-account/cards/help-support/help-support.component';

export const HelpSupportRoutes: Route[] = [
    {
        path     : '',
        component: HelpSupportComponent
    }
];
