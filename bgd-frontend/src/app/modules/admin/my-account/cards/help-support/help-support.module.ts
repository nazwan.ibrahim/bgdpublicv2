import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HelpSupportComponent } from 'app/modules/admin/my-account/cards/help-support/help-support.component';
import { HelpSupportRoutes } from 'app/modules/admin/my-account/cards/help-support/help-support.routing';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
    declarations: [
        HelpSupportComponent
    ],
    imports     : [
        RouterModule.forChild(HelpSupportRoutes),
        MatIconModule,
        MatButtonModule
    ]
})
export class HelpSupportModule
{
}
