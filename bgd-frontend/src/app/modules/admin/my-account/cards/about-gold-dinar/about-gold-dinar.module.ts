import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AboutGoldDinarComponent } from 'app/modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.component';
import { AboutGoldDinarRoutes } from 'app/modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.routing';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
    declarations: [
        AboutGoldDinarComponent
    ],
    imports: [
        RouterModule.forChild(AboutGoldDinarRoutes),
        MatListModule,
        MatIconModule,
    ]
})
export class AboutGoldDinarModule {
}
