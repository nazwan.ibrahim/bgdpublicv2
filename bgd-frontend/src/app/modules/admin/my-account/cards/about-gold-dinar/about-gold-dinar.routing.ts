import { Route } from '@angular/router';
import { AboutGoldDinarComponent } from 'app/modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.component';

export const AboutGoldDinarRoutes: Route[] = [
    {
        path     : '',
        component: AboutGoldDinarComponent
    }
];
