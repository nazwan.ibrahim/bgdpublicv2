import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutGoldDinarComponent } from './about-gold-dinar.component';

describe('AboutGoldDinarComponent', () => {
  let component: AboutGoldDinarComponent;
  let fixture: ComponentFixture<AboutGoldDinarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutGoldDinarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AboutGoldDinarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
