import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrivacyNoticesComponent } from './privacy-notices.component';

const routes: Routes = [
    {
        path: '',
        component: PrivacyNoticesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PrivacyNoticesRoutingModule { }
