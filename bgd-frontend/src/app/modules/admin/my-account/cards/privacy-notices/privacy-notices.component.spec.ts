import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyNoticesComponent } from './privacy-notices.component';

describe('PrivacyNoticesComponent', () => {
  let component: PrivacyNoticesComponent;
  let fixture: ComponentFixture<PrivacyNoticesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivacyNoticesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PrivacyNoticesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
