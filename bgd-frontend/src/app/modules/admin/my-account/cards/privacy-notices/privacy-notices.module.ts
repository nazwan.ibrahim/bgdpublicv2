import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PrivacyNoticesComponent } from 'app/modules/admin/my-account/cards/privacy-notices/privacy-notices.component';
import { PrivacyNoticesRoutingModule } from 'app/modules/admin/my-account/cards/privacy-notices/privacy-notices-routing.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
    declarations: [
        PrivacyNoticesComponent
    ],
    imports: [
        PrivacyNoticesRoutingModule,
        MatIconModule,
    ]
})
export class PrivacyNoticesModule {
}
