import { Route } from '@angular/router';
import { TermConditionsComponent } from 'app/modules/admin/my-account/cards/term-conditions/term-conditions.component';

export const TermConditionsRoutes: Route[] = [
    {
        path     : '',
        component: TermConditionsComponent
    }
];
