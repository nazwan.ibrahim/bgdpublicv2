import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TermConditionsComponent } from 'app/modules/admin/my-account/cards/term-conditions/term-conditions.component';
import { TermConditionsRoutes } from 'app/modules/admin/my-account/cards/term-conditions/term-conditions.routing';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [
        TermConditionsComponent
    ],
    imports     : [
        RouterModule.forChild(TermConditionsRoutes),
        MatIconModule,
        MatButtonModule
    ]
})
export class TermConditionsModule
{
}
