import { Route } from '@angular/router';
import { ChangePinNewComponent } from 'app/modules/admin/my-account/cards/change-pin-new/change-pin-new.component';

export const ChangePinNewRoutes: Route[] = [
    {
        path: '',
        component: ChangePinNewComponent
    }
];
