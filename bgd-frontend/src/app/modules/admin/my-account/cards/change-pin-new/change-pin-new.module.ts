import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ChangePinNewComponent } from 'app/modules/admin/my-account/cards/change-pin-new/change-pin-new.component';
import { ChangePinNewRoutes } from 'app/modules/admin/my-account/cards/change-pin-new/change-pin-new.routing';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { NgxPincodeModule } from 'ngx-pincode';


@NgModule({
    declarations: [
        ChangePinNewComponent
    ],
    imports: [
        RouterModule.forChild(ChangePinNewRoutes),
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        MatIconModule,
        NgxPincodeModule,
    ]
})

export class ChangePinNewModule {
}
