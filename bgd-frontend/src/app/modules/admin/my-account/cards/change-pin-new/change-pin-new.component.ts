import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { MyAccountService } from '../../my-account.service';

@Component({
  selector: 'bgd-change-pin-new',
  templateUrl: './change-pin-new.component.html',
  styleUrls: ['./change-pin-new.component.scss']
})
export class ChangePinNewComponent {

  showAlert = true;
  username: any;
  email: any;
  pin: any;
  oldPin: any;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  constructor(

    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private AuthService: AuthService,
    private _formBuilder: FormBuilder,
    private user: DashboardService,
    private _bgdService: bgdService,
    private myAccountService: MyAccountService,
  ) {}
  // {

  //   {
  //     this._activatedRoute.paramMap.subscribe(params => {
  //       this.oldPin = params.get('userPin');
  //     })
  //     this.oldPin = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userPin'));
  //   }

  // }

  ngOnInit(): void {

    this.oldPin = this.myAccountService.pin;

    this.user.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    })

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  toConfirmPin() {
    this.myAccountService.newPIN = this.pin;
    this.myAccountService.oldPin = this.oldPin;

    console.log(this.myAccountService.newPIN);

      this._router.navigate(['/change-pin-confirm']
      // , {
      //   queryParams: {
      //     userPin: encodeURIComponent(newPIN),
      //     oldPin: encodeURIComponent(oldPin),
      //   }
      // }
      );
  }

}
