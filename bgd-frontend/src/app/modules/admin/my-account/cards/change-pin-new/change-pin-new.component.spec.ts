import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePinNewComponent } from './change-pin-new.component';

describe('ChangePinNewComponent', () => {
  let component: ChangePinNewComponent;
  let fixture: ComponentFixture<ChangePinNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePinNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangePinNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
