import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAccountBankDetailsComponent } from './edit-account-bankdetails.component';

describe('EditAccountBankdetailsComponent', () => {
  let component: EditAccountBankDetailsComponent;
  let fixture: ComponentFixture<EditAccountBankDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditAccountBankDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditAccountBankDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
