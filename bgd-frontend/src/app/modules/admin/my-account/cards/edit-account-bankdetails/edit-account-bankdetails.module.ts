import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EditAccountBankDetailsComponent } from 'app/modules/admin/my-account/cards/edit-account-bankdetails/edit-account-bankdetails.component';
import { MatButtonModule } from '@angular/material/button';
import { EditAccountBankDetailsRoute } from './edit-account-bankdetails.routing';

@NgModule({
    declarations: [
        EditAccountBankDetailsComponent,
        // UserProfileComponent,
        // EditProfileComponent
    ],
    imports: [
        RouterModule.forChild(EditAccountBankDetailsRoute),
        MatButtonModule,
        //RouterModule.forRoot(routes),
    ]


})
export class EditAccountBankDetailsModule {
}
