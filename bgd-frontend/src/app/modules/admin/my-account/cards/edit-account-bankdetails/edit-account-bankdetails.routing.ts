import { Route } from '@angular/router';
import { EditAccountBankDetailsComponent } from 'app/modules/admin/my-account/cards/edit-account-bankdetails/edit-account-bankdetails.component';

export const EditAccountBankDetailsRoute: Route[] = [
    {
        path     : '',
        component: EditAccountBankDetailsComponent
    }
];
