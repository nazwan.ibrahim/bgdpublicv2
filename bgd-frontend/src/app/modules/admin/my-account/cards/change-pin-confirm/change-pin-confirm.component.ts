import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { timer } from 'rxjs';
import { ChangePinSuccessAlertDialog } from './change-pin-success-alert-dialog/change-pin-success-alert-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MyAccountService } from '../../my-account.service';

@Component({
  selector: 'bgd-change-pin-confirm',
  templateUrl: './change-pin-confirm.component.html',
  styleUrls: ['./change-pin-confirm.component.scss'],
  animations: [
    trigger('shake', [
      // Animation configuration
    ])
  ]
})
export class ChangePinConfirmComponent {

  showAlert = true;
  username: any;
  email: any;
  pin: any;
  newPin: any;
  enteredPIN: any;
  oldPin: any;
  disableButton: boolean = false;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };
  isDialogOpen: boolean;

  constructor(

    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private AuthService: AuthService,
    private _formBuilder: FormBuilder,
    private user: DashboardService,
    private _bgdService: bgdService,
    public dialog: MatDialog,
    private myAccountService: MyAccountService,
  ) {}
  // {

  //   {
  //     this._activatedRoute.paramMap.subscribe(params => {
  //       this.enteredPIN = params.get('userPin');
  //       this.oldPin = params.get('oldPin');
  //     })
  //     this.enteredPIN = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userPin'));
  //     this.oldPin = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('oldPin'));
  //   }

  // }

  ngOnInit(): void {

    this.enteredPIN = this.myAccountService.newPIN;
    this.oldPin = this.myAccountService.oldPin;

    this.user.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    })

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(ChangePinSuccessAlertDialog, {
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  changePin() {

    this.disableButton = true;

    if (this.enteredPIN === this.pin) {

      const newPin = {
        currentPin: this.oldPin,
        newPin: this.enteredPIN,
        confirmPin: this.pin,
      }

      this.AuthService.changePin(newPin)
        .subscribe(
          response => {

            this.openDialog();

          },
          error => {

            this.disableButton = false;

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };

            this.showAlert = true;

            setTimeout(() => {
              this.showAlert = false;
            }, 1000);

          }
        );

    } else {

      this.alert = {
        type: 'error',
        message: 'PIN does not match'
      };

      this.disableButton = false;

      this.showAlert = true;

      setTimeout(() => {
        this.showAlert = false;
      }, 1000);

      this.pin = '';
    }
  }

}
