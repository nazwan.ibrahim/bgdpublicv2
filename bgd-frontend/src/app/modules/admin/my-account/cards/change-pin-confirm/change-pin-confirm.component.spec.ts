import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePinConfirmComponent } from './change-pin-confirm.component';

describe('ChangePinConfirmComponent', () => {
  let component: ChangePinConfirmComponent;
  let fixture: ComponentFixture<ChangePinConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePinConfirmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangePinConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
