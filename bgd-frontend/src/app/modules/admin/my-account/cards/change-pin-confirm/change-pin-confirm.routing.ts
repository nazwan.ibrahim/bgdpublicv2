import { Route } from '@angular/router';
import { ChangePinConfirmComponent } from 'app/modules/admin/my-account/cards/change-pin-confirm/change-pin-confirm.component';

export const ChangePinConfirmRoutes: Route[] = [
    {
        path: '',
        component: ChangePinConfirmComponent
    }
];
