"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ChangePinConfirmComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@angular/animations");
var rxjs_1 = require("rxjs");
var ChangePinConfirmComponent = /** @class */ (function () {
    function ChangePinConfirmComponent(_activatedRoute, _router, AuthService, _formBuilder, user, _bgdService) {
        var _this = this;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.AuthService = AuthService;
        this._formBuilder = _formBuilder;
        this.user = user;
        this._bgdService = _bgdService;
        this.showAlert = true;
        this.disableButton = false;
        this.alert = {
            type: 'success',
            message: ''
        };
        {
            this._activatedRoute.paramMap.subscribe(function (params) {
                _this.enteredPIN = params.get('userPin');
                _this.oldPin = params.get('oldPin');
            });
            this.enteredPIN = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userPin'));
            this.oldPin = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('oldPin'));
        }
    }
    ChangePinConfirmComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user.getUserDetails().subscribe(function (data) {
            _this.email = data.email;
            _this.username = data.userName;
        });
    };
    ChangePinConfirmComponent.prototype.pincodeCompleted = function (pin) {
        this.pin = pin;
    };
    ChangePinConfirmComponent.prototype.changePin = function () {
        var _this = this;
        this.disableButton = true;
        if (this.enteredPIN === this.pin) {
            var newPin = {
                currentPin: this.oldPin,
                newPin: this.enteredPIN,
                confirmPin: this.pin
            };
            this.AuthService.changePin(newPin)
                .subscribe(function (response) {
                _this.alert = {
                    type: 'success',
                    message: 'Your PIN has been changed.'
                };
                _this.showAlert = true;
                rxjs_1.timer(1000).subscribe(function () {
                    _this._router.navigate(['/dashboard']);
                });
            }, function (error) {
                _this.disableButton = false;
                _this.alert = {
                    type: 'error',
                    message: 'Invalid'
                };
                _this.showAlert = true;
            });
        }
        else {
            this.disableButton = false;
            this.alert = {
                type: 'error',
                message: 'PIN does not match.'
            };
            this.disableButton = false;
            this.showAlert = true;
            this.pin = '';
        }
    };
    ChangePinConfirmComponent = __decorate([
        core_1.Component({
            selector: 'bgd-change-pin-confirm',
            templateUrl: './change-pin-confirm.component.html',
            styleUrls: ['./change-pin-confirm.component.scss'],
            animations: [
                animations_1.trigger('shake', [
                // Animation configuration
                ])
            ]
        })
    ], ChangePinConfirmComponent);
    return ChangePinConfirmComponent;
}());
exports.ChangePinConfirmComponent = ChangePinConfirmComponent;
