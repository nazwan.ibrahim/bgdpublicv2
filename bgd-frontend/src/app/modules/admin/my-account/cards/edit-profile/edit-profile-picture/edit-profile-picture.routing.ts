import { Route} from '@angular/router';
import { EditProfilePictureComponent } from './edit-profile-picture.component';

export const EditProfilePictureRoutes: Route[] = [
  {
      path     : '',
      component: EditProfilePictureComponent
  }
];

