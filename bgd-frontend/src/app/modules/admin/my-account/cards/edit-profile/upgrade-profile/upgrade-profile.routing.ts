import { Route} from '@angular/router';
import { UpgradeProfileComponent } from './upgrade-profile.component';

export const UpgradeProfileRoutes: Route[] = [
  {
      path     : '',
      component: UpgradeProfileComponent
  }
];

