import { Route} from '@angular/router';
import { UploadProfilePictureComponent } from './upload-profile-picture.component';

export const UploadProfilePictureRoutes: Route[] = [
  {
      path     : '',
      component: UploadProfilePictureComponent
  }
];

