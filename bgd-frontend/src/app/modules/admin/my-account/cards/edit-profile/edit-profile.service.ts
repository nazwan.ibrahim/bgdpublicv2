import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root',
})
export class editProfileService {
    private _authenticated = true;
    constructor(private _httpClient: HttpClient) { }

    userID: String = "";
    //Read userID and fullName data
    userDetail(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getUserDetails');
    }

    //Read address1, address2 and postcode data
    userAddress(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getAddress');
    }

    updateAddress(updatedAddress: any): Observable<any> {
        return this._httpClient.put<any>(environment.apiUrl + '/api/onboarding/updateAddress', updatedAddress);
    }

    createAddress(detailAddress: any): Observable<any> {
        return this._httpClient.post<any>(environment.apiUrl + '/api/onboarding/createAddress', detailAddress);
    }

    getPostcodeInfo(postcode: any): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        const url = `/api/onboarding/postcodeInfo?postcode=${encodeURIComponent(postcode)}`;
        return this._httpClient.get(environment.apiUrl + url, { responseType: 'text' });
    }

    getState(): Observable<any> {
        return this._httpClient.get(environment.apiUrl + '/api/onboarding/state');
    }

}