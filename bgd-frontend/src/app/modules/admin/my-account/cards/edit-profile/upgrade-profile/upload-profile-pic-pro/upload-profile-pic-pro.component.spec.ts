import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadProfilePicProComponent } from './upload-profile-pic-pro.component';

describe('UploadProfilePicProComponent', () => {
  let component: UploadProfilePicProComponent;
  let fixture: ComponentFixture<UploadProfilePicProComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadProfilePicProComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UploadProfilePicProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
