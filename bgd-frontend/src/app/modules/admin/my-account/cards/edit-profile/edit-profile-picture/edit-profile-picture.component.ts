import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

const upload = `
  <svg xmlns="https://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5" />
</svg>

`;

@Component({
    selector: 'bgd-edit-profile-picture',
    templateUrl: './edit-profile-picture.component.html',
    styleUrls: ['./edit-profile-picture.component.scss'],
})
export class EditProfilePictureComponent {

    userID: any;
    profilePicture: any;
    disableButton: boolean = true;

    imageSrc: string | undefined;

    ngOnInit() {
        this.user.getUserDetails().subscribe(data => {
            this.userID = data.userID;
            console.log(this.userID);
        });

    }

    onFileSelected(event: any) {
        const file: File = event.target.files[0];

        const reader = new FileReader();
        reader.onload = () => {
            this.imageSrc = reader.result as string;
        };
        reader.readAsDataURL(file);
    }


    constructor(
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer,
        private user: AuthService,
        private _router: Router
    ) {
        // Note that we provide the icon here as a string literal here due to a limitation in
        // Stackblitz. If you want to provide the icon from a URL, you can use:
        // `iconRegistry.addSvgIcon('thumbs-up', sanitizer.bypassSecurityTrustResourceUrl('icon.svg'));`
        // iconRegistry.addSvgIconLiteral(
        //     'thumbs-up',
        //     sanitizer.bypassSecurityTrustHtml(upload)
        // );

        const svgContent = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 2C6.486 2 2 6.486 2 12s4.486 10 10 10 10-4.486 10-10S17.514 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm-2-9h4v4h2v-4h4V9h-4V5h-2v4H8v2z"/></svg>';
        const trustedSvgContent = sanitizer.bypassSecurityTrustHtml(svgContent);

        iconRegistry.addSvgIconLiteral('thumbs-up', trustedSvgContent);
    }

    onSave() {

        this.disableButton = true;

        const formData = new FormData();
        formData.append('image', this.convertToImageFile(this.imageSrc));

        this.user.createProfilePicture(formData).subscribe(
            (response) => {
                this._router.navigate(['/editProfile']);
            },
            (error) => {
                this.disableButton = false;
                console.error(error);
            }
        );
    }

    convertToImageFile(dataUrl: string): Blob {
        const base64 = dataUrl.split(',')[1];
        const byteString = window.atob(base64);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const uint8Array = new Uint8Array(arrayBuffer);

        for (let i = 0; i < byteString.length; i++) {
            uint8Array[i] = byteString.charCodeAt(i);
        }

        const blob = new Blob([arrayBuffer], { type: 'image/jpeg' }); // replace 'image/jpeg' with the appropriate MIME type

        return blob;
    }


}
