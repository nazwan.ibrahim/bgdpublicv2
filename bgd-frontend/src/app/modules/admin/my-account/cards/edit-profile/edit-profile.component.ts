import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidationErrors, AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { editProfileService } from 'app/modules/admin/my-account/cards/edit-profile/edit-profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { finalize, map } from 'rxjs';
import { FuseAlertType } from '@bgd/components/alert';
import { MatDialog } from '@angular/material/dialog';
import { EditProfileSuccessAlertDialog } from './edit-profile-success-alert-dialog/edit-profile-success-alert-dialog.compnent';

@Component({
  selector: 'bgd-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  userID: any;
  fullName: any;
  email: any;
  idMailing: any;
  idDelivery: any;
  label: any;
  address1Mailing: any;
  address2Mailing: any;
  postcodeMailing: number;
  townMailing: string;
  stateCodeMailing: any;
  address1Delivery: any;
  address2Delivery: any;
  postcodeDelivery: number;
  townDelivery: string;
  stateCodeDelivery: any;
  countryCode: any;
  addressTypeCodeMailing: any;
  addressTypeCodeDelivery: any;
  addressForm: FormGroup;
  phone_number: any;
  showAlert = false;
  profilePicture: any;
  errorMessage: any;
  addressModel: any;
  addressIndex: number = -1;
  state: any;
  stateOption = [];
  disableButton: boolean = false;
  alertVisible: boolean = false;
  oridetailDelivery: any;
  oridetailMailing: any;


  constructor(
    private _editProfileService: editProfileService,
    private _formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {

    //Form validation for both addresses
    this.addressForm = this._formBuilder.group({
      mailingAddress: this._formBuilder.group({
        address1: [''],
        address2: [''],
        postcode: [''],
        town: [''],
        state: ['', this.stateCodeMailing],
        addressTypeCode: "L01",
      }, { asyncValidators: this.checkPostcode.bind(this) }),
      deliveryAddress: this._formBuilder.group({
        address1: [''],
        address2: [''],
        postcode: [''],
        town: [''],
        state: ['', this.stateCodeDelivery],
        addressTypeCode: "L04",
      }, { asyncValidators: this.checkPostcode2.bind(this) }),
    });

    this.addressForm.get('mailingAddress.postcode')?.valueChanges.subscribe(() => {
      this.alertVisible = false;
      this.townMailing = null;
      this.stateCodeMailing = null;
    });

    this.addressForm.get('deliveryAddress.postcode')?.valueChanges.subscribe(() => {
      this.alertVisible = false;
      this.townDelivery = null;
      this.stateCodeDelivery = null;
    });

    //Get user information from API
    this._editProfileService.userDetail().subscribe(data => {
      this.userID = data.userID;
      this.email = data.email;
      this.fullName = data.fullName;
      this.phone_number = data.phoneNumber;
      this.profilePicture = data.profilePicture;
      console.log(data);
    })


    //Get address information from API
    this._editProfileService.userAddress().subscribe((data: any) => {

      console.log("List Address", data);

      if (data && data.length > 0) {

        for (let i = 0; i < data.length; i++) {

          if (data[i].addressType.code == "L01") {

            this.addressForm.get('mailingAddress').patchValue({
              address1: data[i].address1,
              address2: data[i].address2,
              postcode: data[i].postcode,
              town: data[i].town,
              state: data[i].state.code
            })

            this.idMailing = data[i].id;
            this.addressTypeCodeMailing = data[i].addressType.code;

          } else {

            this.addressForm.get('deliveryAddress').patchValue({
              address1: data[i].address1,
              address2: data[i].address2,
              postcode: data[i].postcode,
              town: data[i].town,
              state: data[i].state.code
            })

            this.idDelivery = data[i].id;
            this.addressTypeCodeDelivery = data[i].addressType.code;

          }
        }
      }

      this.oridetailMailing = this.addressForm.get('mailingAddress')?.value;
      this.oridetailDelivery = this.addressForm.get('deliveryAddress')?.value;

      console.log("Original Data", this.oridetailMailing, this.oridetailDelivery);

    });

    //Get list of state from API
    this._editProfileService.getState().subscribe(data => {
      this.stateOption = data;
    });

  }

  //Check postcode for Mailing Address
  checkPostcode: AsyncValidatorFn = async (control: AbstractControl): Promise<ValidationErrors | null> => {
    const postcodeMailing = control.get('postcode')?.value;

    if (!postcodeMailing) {
      return null;
    }

    try {
      const response = await this._editProfileService.getPostcodeInfo(postcodeMailing).toPromise();
      console.log(response);

      const result = JSON.parse(response);

      this.townMailing = result.city;
      this.stateCodeMailing = result.state.code;

      console.log(this.townMailing);

      return null;
    } catch (error) {

      if (error.status === 404) {
        console.log("Error");
        this.alertVisible = true;
        return { invalidPostcode: true };
      }
      
      return { validationError: true };
    }
  }

  //Check postcode for Delivery Address
  checkPostcode2: AsyncValidatorFn = async (control: AbstractControl): Promise<ValidationErrors | null> => {
    const postcodeDelivery = control.get('postcode')?.value;

    if (!postcodeDelivery) {
      return null;
    }

    try {
      const response = await this._editProfileService.getPostcodeInfo(postcodeDelivery).toPromise();
      console.log(response);

      const result = JSON.parse(response);

      this.townDelivery = result.city;
      this.stateCodeDelivery = result.state.code;

      console.log(this.townDelivery);

      return null;
    } catch (error) {

      if (error.status === 404) {
        console.log("Error");
        return { invalidPostcode: true };
      }

      return { validationError: true };
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(EditProfileSuccessAlertDialog, {
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  onSubmit() {

    this.disableButton = true;

    if (this.addressForm.invalid) {
      return;
    }

    this.addressForm.disable();

    const mailingAddress = this.addressForm.get('mailingAddress')?.value;
    const deliveryAddress = this.addressForm.get('deliveryAddress')?.value;

    console.log(mailingAddress, deliveryAddress);

    const mailingChanged = this.isAddressFilled(mailingAddress, this.oridetailMailing);
    const deliveryChanged = this.isAddressFilled(deliveryAddress, this.oridetailDelivery);

    if (!mailingChanged && !deliveryChanged) {
      this.router.navigate(['/myaccount']);
      return;
    }

    if (mailingChanged) {

      if (this.addressTypeCodeMailing != "L01") {

        const createdAddress = {
          label: this.label,
          address1: mailingAddress?.address1,
          address2: mailingAddress?.address2,
          postcode: mailingAddress?.postcode,
          town: this.townMailing,
          stateCode: this.stateCodeMailing,
          countryCode: this.countryCode,
          addressTypeCode: mailingAddress?.addressTypeCode
        }

        console.log("Create Mailing Address", createdAddress);

        this._editProfileService.createAddress(createdAddress)
          .pipe(
            finalize(() => {
              this.addressForm.enable();
            })
          )
          .subscribe(
            Response => {
              console.log("Created New Successfully", Response);

              this.openDialog();

            },
            error => {

              this.disableButton = false;

              this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
              };

              this.showAlert = true;

              setTimeout(() => {
                this.showAlert = false;
              }, 1000);

            }
          );

      } else {

        const updatedAddress = {
          id: this.idMailing,
          label: this.label,
          address1: mailingAddress?.address1,
          address2: mailingAddress?.address2,
          postcode: mailingAddress?.postcode,
          town: this.townMailing,
          stateCode: this.stateCodeMailing,
          countryCode: this.countryCode,
          addressTypeCode: mailingAddress?.addressTypeCode
        }

        console.log("Update Mailing Address", updatedAddress);

        this._editProfileService.updateAddress(updatedAddress)
          .pipe(
            finalize(() => {
              this.addressForm.enable();
            })
          )
          .subscribe(
            Response => {
              console.log("Updated New Successfully", Response);

              this.openDialog();

              this.oridetailMailing = this.addressForm.get('mailingAddress')?.value;
            },
            error => {
              this.disableButton = false;

              this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
              };

              this.showAlert = true;

              setTimeout(() => {
                this.showAlert = false;
              }, 1000);

            }
          );

      }

    }

    if (deliveryChanged) {

      if (this.addressTypeCodeDelivery != "L04") {

        const createdAddress = {
          label: this.label,
          address1: deliveryAddress?.address1,
          address2: deliveryAddress?.address2,
          postcode: deliveryAddress?.postcode,
          town: this.townDelivery,
          stateCode: this.stateCodeDelivery,
          countryCode: this.countryCode,
          addressTypeCode: deliveryAddress?.addressTypeCode
        }

        console.log("Create Delivery Address", createdAddress);

        this._editProfileService.createAddress(createdAddress)
          .pipe(
            finalize(() => {
              this.addressForm.enable();
            })
          )
          .subscribe(
            Response => {
              console.log("Created New Successfully", Response);

              this.openDialog();

            },
            error => {
              this.disableButton = false;

              this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
              };

              this.showAlert = true;

              setTimeout(() => {
                this.showAlert = false;
              }, 1000);
            }
          );

      } else {

        const updatedAddress = {
          id: this.idDelivery,
          label: this.label,
          address1: deliveryAddress?.address1,
          address2: deliveryAddress?.address2,
          postcode: deliveryAddress?.postcode,
          town: this.townDelivery,
          stateCode: this.stateCodeDelivery,
          countryCode: this.countryCode,
          addressTypeCode: deliveryAddress?.addressTypeCode
        }

        console.log("Update Delivery Address", updatedAddress);

        this._editProfileService.updateAddress(updatedAddress)
          .pipe(
            finalize(() => {
              this.addressForm.enable();
            })
          )
          .subscribe(
            Response => {
              console.log("Updated New Successfully", Response);

              this.openDialog();

              this.oridetailDelivery = this.addressForm.get('deliveryAddress')?.value;
            },
            error => {
              this.disableButton = false;

              this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
              };

              this.showAlert = true;

              setTimeout(() => {
                this.showAlert = false;
              }, 1000);

            }
          );

      }

    }

  }

  private isAddressFilled(newData: any, Data: any): boolean {
    return (
      newData &&
      (newData.address1 !== Data.address1 ||
        newData.address2 !== Data.address2 ||
        newData.postcode !== Data.postcode ||
        newData.town !== Data.town ||
        newData.state !== Data.state)
    );
  }

}
