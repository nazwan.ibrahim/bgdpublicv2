"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.EditProfilePictureComponent = void 0;
var core_1 = require("@angular/core");
var upload = "\n  <svg xmlns=\"https://www.w3.org/2000/svg\" fill=\"none\" viewBox=\"0 0 24 24\" stroke-width=\"1.5\" stroke=\"currentColor\" class=\"w-6 h-6\">\n  <path stroke-linecap=\"round\" stroke-linejoin=\"round\" d=\"M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5\" />\n</svg>\n\n";
var EditProfilePictureComponent = /** @class */ (function () {
    function EditProfilePictureComponent(iconRegistry, sanitizer, user, _router) {
        this.user = user;
        this._router = _router;
        this.disableButton = true;
        // Note that we provide the icon here as a string literal here due to a limitation in
        // Stackblitz. If you want to provide the icon from a URL, you can use:
        // `iconRegistry.addSvgIcon('thumbs-up', sanitizer.bypassSecurityTrustResourceUrl('icon.svg'));`
        iconRegistry.addSvgIconLiteral('thumbs-up', sanitizer.bypassSecurityTrustHtml(upload));
    }
    EditProfilePictureComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user.getUserDetails().subscribe(function (data) {
            _this.userID = data.userID;
            console.log(_this.userID);
        });
    };
    EditProfilePictureComponent.prototype.onFileSelected = function (event) {
        var _this = this;
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = function () {
            _this.imageSrc = reader.result;
        };
        reader.readAsDataURL(file);
    };
    EditProfilePictureComponent.prototype.onSave = function () {
        var _this = this;
        this.disableButton = true;
        var formData = new FormData();
        formData.append('image', this.convertToImageFile(this.imageSrc));
        this.user.createProfilePicture(formData).subscribe(function (response) {
            _this._router.navigate(['/editProfile']);
        }, function (error) {
            _this.disableButton = false;
            console.error(error);
        });
    };
    EditProfilePictureComponent.prototype.convertToImageFile = function (dataUrl) {
        var base64 = dataUrl.split(',')[1];
        var byteString = window.atob(base64);
        var arrayBuffer = new ArrayBuffer(byteString.length);
        var uint8Array = new Uint8Array(arrayBuffer);
        for (var i = 0; i < byteString.length; i++) {
            uint8Array[i] = byteString.charCodeAt(i);
        }
        var blob = new Blob([arrayBuffer], { type: 'image/jpeg' }); // replace 'image/jpeg' with the appropriate MIME type
        return blob;
    };
    EditProfilePictureComponent = __decorate([
        core_1.Component({
            selector: 'bgd-edit-profile-picture',
            templateUrl: './edit-profile-picture.component.html',
            styleUrls: ['./edit-profile-picture.component.scss']
        })
    ], EditProfilePictureComponent);
    return EditProfilePictureComponent;
}());
exports.EditProfilePictureComponent = EditProfilePictureComponent;
