import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditProfileComponent } from './edit-profile.component';
import { RouterModule } from '@angular/router';
import { EditProfileRoutes } from './edit-profile.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';

// import { EditProfilePictureComponent } from './edit-profile-picture/edit-profile-picture.component';
// import { UploadProfilePictureComponent } from './upload-profile-picture/upload-profile-picture.component';
// import { SelectProfilePictureComponent } from './select-profile-picture/select-profile-picture.component';
// import { UpgradeProfileComponent } from './upgrade-profile/upgrade-profile.component';
// import { EditProfileProComponent } from './upgrade-profile/edit-profile-pro/edit-profile-pro.component';
// import { EditProfilePicProComponent } from './upgrade-profile/edit-profile-pic-pro/edit-profile-pic-pro.component';
// import { SelectProfilePicProComponent } from './upgrade-profile/select-profile-pic-pro/select-profile-pic-pro.component';
// import { UploadProfilePicProComponent } from './upgrade-profile/upload-profile-pic-pro/upload-profile-pic-pro.component';

@NgModule({
  declarations: [
    EditProfileComponent,
    // EditProfilePictureComponent,
    // UploadProfilePictureComponent,
    // SelectProfilePictureComponent,
    // UpgradeProfileComponent,
    // EditProfileProComponent,
    // EditProfilePicProComponent,
    // SelectProfilePicProComponent,
    // UploadProfilePicProComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(EditProfileRoutes),
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule,
    MatSelectModule,
    HttpClientModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule,
  ],
  exports: [
    EditProfileComponent,
  ]
})
export class EditProfileModule { }
