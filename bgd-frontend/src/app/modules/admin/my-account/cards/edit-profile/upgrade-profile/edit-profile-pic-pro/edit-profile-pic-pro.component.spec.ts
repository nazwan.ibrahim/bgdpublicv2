import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfilePicProComponent } from './edit-profile-pic-pro.component';

describe('EditProfilePicProComponent', () => {
  let component: EditProfilePicProComponent;
  let fixture: ComponentFixture<EditProfilePicProComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditProfilePicProComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditProfilePicProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
