import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectProfilePicProComponent } from './select-profile-pic-pro.component';

describe('SelectProfilePicProComponent', () => {
  let component: SelectProfilePicProComponent;
  let fixture: ComponentFixture<SelectProfilePicProComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectProfilePicProComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectProfilePicProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
