"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.EditProfileComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
var EditProfileComponent = /** @class */ (function () {
    function EditProfileComponent(_editProfileService, _formBuilder, router) {
        this._editProfileService = _editProfileService;
        this._formBuilder = _formBuilder;
        this.router = router;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.addressIndex = -1;
        this.stateOption = [];
        this.disableButton = false;
    }
    EditProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.addressForm = this._formBuilder.group({
            address1: [this.address1 || '', forms_1.Validators.required],
            address2: [''],
            postcode: ['', forms_1.Validators.required],
            town: ['', forms_1.Validators.required],
            state: [this.stateCode, forms_1.Validators.required]
        });
        this._editProfileService.userDetail().subscribe(function (data) {
            _this.userID = data.userID;
            _this.email = data.email;
            _this.fullName = data.fullName;
            _this.phone_number = data.phoneNumber;
            _this.profilePicture = data.profilePicture;
            console.log(data);
        });
        this._editProfileService.userAddress().pipe(rxjs_1.map(function (value) {
            _this.addressModel = value;
            console.log("ADDRESS ", value);
            for (var i = 0; i < _this.addressModel.length; i++) {
                if (_this.addressModel[i].addressType.code == "L01") {
                    _this.addressIndex = i;
                }
                else {
                    _this.addressIndex = -1;
                }
            }
            return _this.addressModel;
        })).subscribe(function (data) {
            console.log("Current Address", data);
            if (_this.addressIndex === -1 || !data[_this.addressIndex]) {
                _this.id = null;
                _this.label = null;
                _this.address1 = null;
                _this.address2 = null;
                _this.postcode = null;
                _this.town = null;
                _this.state = null;
            }
            else {
                _this.id = data[_this.addressIndex].id;
                _this.label = data[_this.addressIndex].label;
                _this.address1 = data[_this.addressIndex].address1;
                _this.address2 = data[_this.addressIndex].address2;
                _this.postcode = data[_this.addressIndex].postcode;
                _this.town = data[_this.addressIndex].town;
                _this.stateCode = data[_this.addressIndex].state.code;
                _this.state = data[_this.addressIndex].state.name;
                _this.addressTypeCode = data[_this.addressIndex].addressType.code;
            }
            _this.addressForm.patchValue({
                address1: _this.address1,
                address2: _this.address2,
                postcode: _this.postcode,
                town: _this.town,
                state: _this.stateCode
            });
        });
        console.log(this.address1);
        console.log(this.address2);
        console.log(this.postcode);
        console.log(this.town);
        console.log(this.stateCode);
        this._editProfileService.getState().subscribe(function (data) {
            _this.stateOption = data;
        });
        this.addressForm.get('address1').valueChanges
            .pipe(operators_1.debounceTime(300)) // Optional debounce time to delay the value retrieval
            .subscribe(function (address1Value) {
            console.log('Address 1 Value:', address1Value);
        });
        // this._editProfileService.userAddress().subscribe(dataAddress => {
        //  this.id = dataAddress[0].id;
        //  this.label = dataAddress[0].label;
        //  this.address1 = dataAddress[0].address1;
        //  this.address2 = dataAddress[0].address2;
        //  this.postcode = dataAddress[0].postcode;
        //  this.town = dataAddress[0].town;
        //  this.stateCode = dataAddress[0].state.code;
        //  this.countryCode = dataAddress[0].country.code;
        //  this.addressTypeCode = dataAddress[0].addressType.code;
        //  console.log(dataAddress);
        // Update form values
        // this.addressForm.setValue({
        //   id: this.id,
        //   label: this.label,
        //   address1: this.address1,
        //   address2: this.address2,
        //   postcode: this.postcode,
        //   town: this.town,
        //   state: this.state,
        //   countryCode: this.countryCode,
        //   addressTypeCode: this.addressTypeCode,
        // });
        // })
    };
    EditProfileComponent.prototype.checkPostcode = function () {
        var _this = this;
        this.postcode = this.addressForm.get('postcode').value;
        console.log(this.postcode);
        this._editProfileService.getPostcodeInfo(this.postcode).subscribe(function (result) {
            var response = JSON.parse(result);
            _this.town = response.city;
            _this.stateCode = response.state.code;
            console.log(_this.town);
        }, function (error) {
            if (error.status === 404) { // check if the error is a 404 Not Found
                _this.town = null;
                _this.errorMessage = 'Invalid postcode'; // set a custom error message
            }
            else {
                _this.errorMessage = 'An error occurred'; // set a generic error message
            }
        });
    };
    EditProfileComponent.prototype.onSubmit = function () {
        var _this = this;
        this.disableButton = true;
        if (this.addressForm.invalid) {
            return;
        }
        this.addressForm.disable();
        if (this.addressTypeCode != "L01") {
            var createdAddress = {
                label: this.label,
                address1: this.addressForm.get('address1').dirty ? this.addressForm.get('address1').value : this.address1,
                address2: this.addressForm.get('address2').dirty ? this.addressForm.get('address2').value : this.address2,
                postcode: this.addressForm.get('postcode').value,
                town: this.town,
                stateCode: this.stateCode,
                countryCode: this.countryCode,
                addressTypeCode: "L01"
            };
            console.log("Create Address", createdAddress);
            // Call the createAddress service method with the new address details
            this._editProfileService.createAddress(createdAddress)
                .pipe(rxjs_1.finalize(function () {
                _this.addressForm.enable();
                // this.addressForm.reset();
            }))
                .subscribe(function (Response) {
                console.log("Create", Response);
                _this.alert = {
                    type: 'success',
                    message: 'Saved Successfully'
                };
                _this.showAlert = true;
                console.log(_this.showAlert);
                // Navigate to the next page on successful address create
                setTimeout(function () {
                    _this.router.navigate(['/myaccount']);
                }, 1000); // 3-second delay (3000 milliseconds)
            }, function (error) {
                _this.disableButton = false;
                console.log('Alert:', _this.showAlert);
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
                _this.showAlert = true;
            });
        }
        else if (this.addressTypeCode == "L01") {
            var updatedAddress = {
                id: this.id,
                label: this.label,
                address1: this.addressForm.get('address1').dirty ? this.addressForm.get('address1').value : this.address1,
                address2: this.addressForm.get('address2').dirty ? this.addressForm.get('address2').value : this.address2,
                postcode: this.addressForm.get('postcode').value,
                town: this.town,
                stateCode: this.stateCode,
                countryCode: this.countryCode,
                addressTypeCode: "L01"
            };
            console.log("Update Address", updatedAddress);
            // Call the updateAddress service method with the new address details
            this._editProfileService.updateAddress(updatedAddress)
                .pipe(rxjs_1.finalize(function () {
                _this.addressForm.enable();
                // this.addressForm.reset();
            }))
                .subscribe(function (Response) {
                console.log("Update", Response);
                _this.alert = {
                    type: 'success',
                    message: 'Saved Successfully'
                };
                _this.showAlert = true;
                console.log(_this.showAlert);
                // Navigate to the next page on successful address update
                setTimeout(function () {
                    _this.router.navigate(['/myaccount']);
                }, 1000); // 3-second delay (3000 milliseconds)
            }, function (error) {
                _this.disableButton = false;
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
                _this.showAlert = true;
            });
        }
    };
    EditProfileComponent = __decorate([
        core_1.Component({
            selector: 'bgd-edit-profile',
            templateUrl: './edit-profile.component.html',
            styleUrls: ['./edit-profile.component.scss']
        })
    ], EditProfileComponent);
    return EditProfileComponent;
}());
exports.EditProfileComponent = EditProfileComponent;
