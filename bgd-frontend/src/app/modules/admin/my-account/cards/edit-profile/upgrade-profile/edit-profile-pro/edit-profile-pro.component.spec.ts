import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfileProComponent } from './edit-profile-pro.component';

describe('EditProfileProComponent', () => {
  let component: EditProfileProComponent;
  let fixture: ComponentFixture<EditProfileProComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditProfileProComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditProfileProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
