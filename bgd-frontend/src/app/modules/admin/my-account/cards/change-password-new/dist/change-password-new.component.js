"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ChangePasswordNewComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var validators_1 = require("@bgd/validators");
var ChangePasswordNewComponent = /** @class */ (function () {
    function ChangePasswordNewComponent(_activatedRoute, _router, AuthService, _formBuilder) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.AuthService = AuthService;
        this._formBuilder = _formBuilder;
        this.showAlert = true;
        this.hideConfirmPassword = true;
        this.hideNewPassword = true;
        this.disableButton = false;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.oldPassword = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('currentPassword'));
    }
    ChangePasswordNewComponent.prototype.ngOnInit = function () {
        // Create the form
        this.changePassForm = this._formBuilder.group({
            newPassword: ['', [forms_1.Validators.required]],
            confirmPassword: ['', [forms_1.Validators.required]]
        }, {
            validators: validators_1.FuseValidators.mustMatch('newPassword', 'confirmPassword')
        });
    };
    ChangePasswordNewComponent.prototype.toggleConfirmPasswordVisibility = function () {
        this.hideConfirmPassword = !this.hideConfirmPassword;
    };
    ChangePasswordNewComponent.prototype.toggleNewPasswordVisibility = function () {
        this.hideNewPassword = !this.hideNewPassword;
    };
    Object.defineProperty(ChangePasswordNewComponent.prototype, "password", {
        get: function () {
            return this.changePassForm.get('newPassword');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ChangePasswordNewComponent.prototype, "passwordLength", {
        get: function () {
            var _a, _b;
            return (_b = (_a = this.changePassForm.get('newPassword')) === null || _a === void 0 ? void 0 : _a.value) === null || _b === void 0 ? void 0 : _b.length;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ChangePasswordNewComponent.prototype, "hasLowerCase", {
        get: function () {
            var _a;
            return /[a-z]/.test((_a = this.changePassForm.get('newPassword')) === null || _a === void 0 ? void 0 : _a.value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ChangePasswordNewComponent.prototype, "hasUpperCase", {
        get: function () {
            var _a;
            return /[A-Z]/.test((_a = this.changePassForm.get('newPassword')) === null || _a === void 0 ? void 0 : _a.value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ChangePasswordNewComponent.prototype, "hasNumber", {
        get: function () {
            var _a;
            return /[0-9]/.test((_a = this.changePassForm.get('newPassword')) === null || _a === void 0 ? void 0 : _a.value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ChangePasswordNewComponent.prototype, "hasSpecialChar", {
        get: function () {
            var _a;
            return /[!@#$%^&*()\[\]\-_+={}:";',<.>/?]/.test((_a = this.changePassForm.get('newPassword')) === null || _a === void 0 ? void 0 : _a.value);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ChangePasswordNewComponent.prototype, "hasMatchPassword", {
        get: function () {
            var _a, _b;
            var newPassword = (_a = this.changePassForm.get('newPassword')) === null || _a === void 0 ? void 0 : _a.value;
            var confirmPassword = (_b = this.changePassForm.get('confirmPassword')) === null || _b === void 0 ? void 0 : _b.value;
            if (newPassword === null || confirmPassword === null || confirmPassword.trim() === '') {
                return false;
            }
            if (newPassword !== null && confirmPassword !== null) {
                if (newPassword === confirmPassword) {
                    return true;
                }
            }
            return false;
        },
        enumerable: false,
        configurable: true
    });
    ChangePasswordNewComponent.prototype.continueButton = function () {
        this.disableButton = true;
        var newPassword = this.changePassForm.get('newPassword').value;
        var confirmPassword = this.changePassForm.get('confirmPassword').value;
        var currentPassword = this.oldPassword;
        if (newPassword !== currentPassword && newPassword === confirmPassword) {
            // Passwords meet the conditions, navigate to the next page
            this._router.navigate(['/change-password-pin'], {
                queryParams: {
                    currentPassword: encodeURIComponent(currentPassword),
                    newPassword: encodeURIComponent(newPassword),
                    confirmPassword: encodeURIComponent(confirmPassword)
                }
            });
        }
        else {
            this.disableButton = false;
            // Passwords don't meet the conditions, show an alert with a custom error message
            this.showAlert = true;
            this.alert = {
                type: 'error',
                message: 'The passwords do not match or the new password is the same as the current password. Please try again.'
            };
        }
    };
    __decorate([
        core_1.ViewChild('changePassFormNgForm')
    ], ChangePasswordNewComponent.prototype, "changePassFormNgForm");
    ChangePasswordNewComponent = __decorate([
        core_1.Component({
            selector: 'bgd-change-password',
            templateUrl: './change-password-new.component.html',
            styleUrls: ['./change-password-new.component.scss']
        })
    ], ChangePasswordNewComponent);
    return ChangePasswordNewComponent;
}());
exports.ChangePasswordNewComponent = ChangePasswordNewComponent;
