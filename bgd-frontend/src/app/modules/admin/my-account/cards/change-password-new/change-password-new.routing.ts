import { Route } from '@angular/router';
import { ChangePasswordNewComponent } from 'app/modules/admin/my-account/cards/change-password-new/change-password-new.component';

export const ChangePasswordNewRoutes: Route[] = [
    {
        path: '',
        component: ChangePasswordNewComponent
    }
];
