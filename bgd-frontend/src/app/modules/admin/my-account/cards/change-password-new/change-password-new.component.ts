import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators, FormControl, ValidationErrors } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { AES } from 'crypto-js';
import * as CryptoJS from 'crypto-js';
import { MyAccountService } from '../../my-account.service';

@Component({
  selector: 'bgd-change-password',
  templateUrl: './change-password-new.component.html',
  styleUrls: ['./change-password-new.component.scss']
})
export class ChangePasswordNewComponent {

  @ViewChild('changePassFormNgForm') changePassFormNgForm: NgForm;

  oldPassword: any;
  newPassword: any;
  confirmPassword: any;
  changePassForm: FormGroup;
  showAlert = true;
  hideConfirmPassword = true;
  hideNewPassword = true;
  disableButton: boolean = false;
  hasCommonWord: boolean = false;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private AuthService: AuthService,
    private _formBuilder: FormBuilder,
    private myAccountService: MyAccountService,
  ) { }

  // {
  //   const encryptedPassword = this._activatedRoute.snapshot.queryParamMap.get('data');
  //   this.oldPassword = encryptedPassword;
  // }

  ngOnInit(): void {
    this.oldPassword = this.myAccountService.encryptedPassword
    console.log([this.oldPassword])

    // Create the form
    this.changePassForm = this._formBuilder.group({
      newPassword: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()\\[\\]\\-_+={}:";\'\',<.>/?]).*$')],
        this.checkPassword.bind(this)],
      confirmPassword: ['', [Validators.required]],
    },

      {
        validators: FuseValidators.mustMatch('newPassword', 'confirmPassword')
      }
    );

  }

  toggleConfirmPasswordVisibility() {
    this.hideConfirmPassword = !this.hideConfirmPassword;
  }

  toggleNewPasswordVisibility() {
    this.hideNewPassword = !this.hideNewPassword;
  }

  get password() {
    return this.changePassForm.get('newPassword');
  }

  get passwordLength() {
    return this.changePassForm.get('newPassword')?.value?.length;
  }

  get hasLowerCase() {
    return /[a-z]/.test(this.changePassForm.get('newPassword')?.value);
  }

  get hasUpperCase() {
    return /[A-Z]/.test(this.changePassForm.get('newPassword')?.value);
  }

  get hasNumber() {
    return /[0-9]/.test(this.changePassForm.get('newPassword')?.value);
  }

  get hasSpecialChar() {
    return /[!@#$%^&*()\[\]\-_+={}:";',<.>/?]/.test(this.changePassForm.get('newPassword')?.value);
  }

  get hasMatchPassword() {
    const newPassword = this.changePassForm.get('newPassword')?.value;
    const confirmPassword = this.changePassForm.get('confirmPassword')?.value;

    if (newPassword === null || confirmPassword === null || confirmPassword.trim() === '') {
      return false;
    }

    if (newPassword !== null && confirmPassword !== null) {
      if (newPassword === confirmPassword) {
        return true;
      }
    }

    return false;
  }

  async checkPassword(control: FormControl): Promise<ValidationErrors | null> {
    const newPassword = control.value;
    const password = { password: newPassword };

    try {
      const response = await this.AuthService.ValidatePassword(password).toPromise();

      const validateMessage = JSON.parse(response);


      if (validateMessage.message === "validated") {
        this.hasCommonWord = true;
      } else {
        this.hasCommonWord = false;
      }

      return null;
    } catch (error) {

      return { validationError: true };
    }
  }

  continueButton() {

    this.disableButton = true;

    const newPassword = this.changePassForm.get('newPassword').value;
    this.myAccountService.encryptedNewPassword = AES.encrypt(newPassword, 'encryptionSecretKey').toString();
    this.myAccountService.currentPassword = this.oldPassword
    const confirmPassword = this.changePassForm.get('confirmPassword').value;
    this.myAccountService.encryptedconfirmPassword = AES.encrypt(confirmPassword, 'encryptionSecretKey').toString();

    this._router.navigate(['/change-password-pin'],

    );

  }
}

