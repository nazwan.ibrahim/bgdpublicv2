import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { ChangePinComponent } from 'app/modules/admin/my-account/cards/change-pin/change-pin.component';
import { ChangePinRoutes } from 'app/modules/admin/my-account/cards/change-pin/change-pin.routing';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { NgxPincodeModule } from 'ngx-pincode';


@NgModule({
    declarations: [
        ChangePinComponent
    ],
    imports: [
        RouterModule.forChild(ChangePinRoutes),
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        MatIconModule,
        NgxPincodeModule,   
    ]
})

export class ChangePinModule {
}
