import { Route } from '@angular/router';
import { ChangePinComponent } from 'app/modules/admin/my-account/cards/change-pin/change-pin.component';

export const ChangePinRoutes: Route[] = [
    {
        path: '',
        component: ChangePinComponent
    }
];
