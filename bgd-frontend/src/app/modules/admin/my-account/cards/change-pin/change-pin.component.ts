import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MyAccountService } from '../../my-account.service';

@Component({
    selector: 'bgd-change-pin',
    templateUrl: './change-pin.component.html',
    styleUrls: ['./change-pin.component.scss'],
    animations: [
        trigger('shake', [
            // Animation configuration
        ])
    ]
})
export class ChangePinComponent {
    showAlert = false;
    username: any;
    email: any;
    pin: any;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: '',
    };

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private AuthService: AuthService,
        private _formBuilder: FormBuilder,
        private user: DashboardService,
        private _bgdService: bgdService,
        private myAccountService: MyAccountService,
    ) { }

    ngOnInit(): void {
        this.user.getUserDetails().subscribe((data) => {
            this.email = data.email;
            this.username = data.userName;
        });
    }

    pincodeCompleted(pin: any) {
        this.pin = pin;
    }

    forgotPin() {
        this._router.navigate(['/forgot-pin']);
    }

    verifyPin() {

        const pin = {
            userName: this.username,
            email: this.email,
            userPin: this.pin,
        };

        this.myAccountService.pin = this.pin;

        console.log(pin);

        this._bgdService.verifyPin(pin).subscribe(
            (response) => {


                this._router.navigate(['/change-pin-new']
                // , {
                //     queryParams: {
                //         userPin: encodeURIComponent(currentPIN),
                //     },
                // }
                );

            },
            (error) => {

                this.alert = {
                    type: 'error',
                    message: 'Invalid PIN',
                };

                this.showAlert = true;

                setTimeout(() => {
                    this.showAlert = false;
                }, 1000);
            }
        );
    }
}
