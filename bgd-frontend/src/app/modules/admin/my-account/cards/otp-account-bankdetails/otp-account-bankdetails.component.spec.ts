import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtpAccountBankdetailsComponent } from './otp-account-bankdetails.component';

describe('OtpAccountBankdetailsComponent', () => {
  let component: OtpAccountBankdetailsComponent;
  let fixture: ComponentFixture<OtpAccountBankdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtpAccountBankdetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OtpAccountBankdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
