import { Route} from '@angular/router';
import { OtpComponent } from './otp.component';

export const OtpRoutes: Route[] = [
  {
      path     : '',
      component: OtpComponent
  }
];

