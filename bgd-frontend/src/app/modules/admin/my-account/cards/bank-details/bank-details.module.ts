import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BankDetailsRoutes } from './bank-details.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { BankDetailsComponent } from './bank-details.component';



@NgModule({
  declarations: [
    BankDetailsComponent

   
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(BankDetailsRoutes),
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule
  ]
})
export class BankDetailsModules { }
