import { Route} from '@angular/router';
import { BankDetailsComponent } from './bank-details.component';

export const BankDetailsRoutes: Route[] = [
  {
      path     : '',
      component: BankDetailsComponent
  }
];

