import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccbankDetailsComponent } from './accbank-details.component';

describe('AccbankDetailsComponent', () => {
  let component: AccbankDetailsComponent;
  let fixture: ComponentFixture<AccbankDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccbankDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccbankDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
