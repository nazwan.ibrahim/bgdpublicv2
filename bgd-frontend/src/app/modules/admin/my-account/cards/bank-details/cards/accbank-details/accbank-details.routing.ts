import { Route} from '@angular/router';
import { AccbankDetailsComponent } from './accbank-details.component';

export const AccbankDetailsRoutes: Route[] = [
  {
      path     : '',
      component: AccbankDetailsComponent
  }
];

