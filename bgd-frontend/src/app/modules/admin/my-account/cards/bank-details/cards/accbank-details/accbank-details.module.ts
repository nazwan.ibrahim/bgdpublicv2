import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AccbankDetailsRoutes } from './accbank-details.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { AccbankDetailsComponent } from './accbank-details.component';



@NgModule({
  declarations: [
    AccbankDetailsComponent,
   
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AccbankDetailsRoutes),
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule
  ]
})
export class AccbankDetailsModule { }
