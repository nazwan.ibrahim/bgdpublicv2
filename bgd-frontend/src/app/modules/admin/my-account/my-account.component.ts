import { Component } from '@angular/core';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';

@Component({
  selector: 'bgd-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent {

  fullName: any;
  profilePicture: any;

  constructor(
    private _bgdService: bgdService,
  ) { }

  ngOnInit() {
    this._bgdService.getUserDetails().subscribe(data => {
      this.fullName = data.fullName;
      this.profilePicture = data.profilePicture;
      console.log(data);
      console.log(this.profilePicture);
    })
  }

}


