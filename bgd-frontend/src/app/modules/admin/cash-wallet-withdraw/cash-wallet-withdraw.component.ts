import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';
import { finalize, map } from 'rxjs';

@Component({
  selector: 'bgd-cash-wallet-withdraw',
  templateUrl: './cash-wallet-withdraw.component.html',
  styleUrls: ['./cash-wallet-withdraw.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CashWalletWithdrawComponent {

  @ViewChild('cashWalletWithdrawNgForm') cashWalletWithdrawNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  cashWalletWithdrawForm: UntypedFormGroup;
  availAmount: any;
  inputAmount: any;
  accountWallet: any;
  response: any;
  disableButton = false;
  bankName: string;
  labelAlertVisible: boolean = false;
  setupFees: any;
  setupUom: any;
  costModel: any;
  bursaCostIndex: number = -1;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private cashWalletService: CashWalletService,
    private _formBuilder: UntypedFormBuilder,
    private _router: Router,
    private walletService: WalletService
  ) {}

  ngOnInit(): void {
    
    this.availAmount = this.walletService.availAmount;
    this.accountWallet = this.walletService.accountWallet;

    this.cashWalletWithdrawForm = this._formBuilder.group({
      price: ['', Validators.required],
    });


    this.cashWalletService.getbursaCostRecovery().pipe(
      map((value: any) => {
        this.costModel = value;

        for (var i = 0; i < this.costModel.length; i++) {
          if (this.costModel[i].feeTypeCode == "B02") {
            this.bursaCostIndex = i;
            break;
          }
        }

        return this.costModel;
      })
    ).subscribe((data: any) => {

      this.setupFees = data[this.bursaCostIndex].setupFees;
      this.setupUom = data[this.bursaCostIndex].setupUom;
    });
  }

  compareAmount() {
    this.inputAmount = this.cashWalletWithdrawForm.get('price').value;

    if (this.availAmount < this.inputAmount) {

      this.disableButton = true;
      this.labelAlertVisible = true;
    } else {
      this.disableButton = false;
      this.labelAlertVisible = false;
    }
  }

  backButton(): void {
    this._router.navigate(['/cash-wallet']);
  }

  withdrawMethod(): void {
    if (this.cashWalletWithdrawForm.invalid) {
      return;
    }

    this.cashWalletWithdrawForm.disable();

    const acctNumber = this.accountWallet;
    const amount = this.cashWalletWithdrawForm.get('price').value;
    const transactionTypeCode = "F02";
    const walletTypeCode = "E01";

    this.cashWalletService.getinitWithdraw(acctNumber, amount, transactionTypeCode, walletTypeCode)
      .pipe(
        finalize(() => {
          this.cashWalletWithdrawForm.enable();

          this.cashWalletWithdrawForm.reset();
        })
      )
      .subscribe(
        Response => {

          console.log("InitWithdraw Response:", Response);

          this.response = Response;
          this.walletService.initWithdraw = this.response;

          this.alert = {
            type: 'success',
            message: 'Success'
          };

          this._router.navigate(['/cash-wallet-withdraw-bank']);

        },
        error => {

          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };

          this._router.navigate(['/transaction-limit']);

        }
      );
  }
}
