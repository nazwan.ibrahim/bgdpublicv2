import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletWithdrawComponent } from './cash-wallet-withdraw.component';
import { CashWalletWithdrawRoutingModule } from './cash-wallet-withdraw-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';

@NgModule({
    declarations: [
        CashWalletWithdrawComponent
    ],
    imports: [
        CommonModule,
        CashWalletWithdrawRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        MatCardModule
    ]
})
export class CashWalletWithdrawModule { }