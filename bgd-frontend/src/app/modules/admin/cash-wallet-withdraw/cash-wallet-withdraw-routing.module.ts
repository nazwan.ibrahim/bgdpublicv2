import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletWithdrawComponent } from './cash-wallet-withdraw.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletWithdrawComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletWithdrawRoutingModule { }
