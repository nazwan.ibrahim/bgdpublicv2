import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletWithdrawComponent } from './cash-wallet-withdraw.component';

describe('CashWalletWithdrawComponent', () => {
  let component: CashWalletWithdrawComponent;
  let fixture: ComponentFixture<CashWalletWithdrawComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletWithdrawComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletWithdrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
