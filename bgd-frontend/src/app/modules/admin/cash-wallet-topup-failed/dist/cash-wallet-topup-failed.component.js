"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletTopupFailedComponent = void 0;
var core_1 = require("@angular/core");
var file_saver_1 = require("file-saver");
var CashWalletTopupFailedComponent = /** @class */ (function () {
    function CashWalletTopupFailedComponent(_activatedRoute, _router, _formBuilder, cashWalletService, bgdService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this.cashWalletService = cashWalletService;
        this.bgdService = bgdService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log("Status Response", response);
        this.totalAmount = response.amount;
        this.transactionMethodCode = response.transactionMethodCode;
        this.setuptransFee = response.fees[0].setupFees;
        this.chargeTransFee = response.fees[0].chargeFees;
        this.setupSstFee = response.fees[1].setupFees;
        this.chargeSstFee = response.fees[1].chargeFees;
        this.status = response.statusCode;
        this.ref = response.reference;
        if (this.status == "G01") {
            this.status = "Successful";
        }
        else {
            this.status = "Failed";
        }
        if (this.transactionMethodCode == "H01") {
            this.transactionMethod = "Online Banking";
        }
        else if (this.transactionMethodCode == "H02") {
            this.transactionMethod = "Credit Card & Debit Card";
        }
        else if (this.transactionMethodCode == "H03") {
            this.transactionMethod = "Auto Debit";
        }
    }
    CashWalletTopupFailedComponent.prototype.downloadReceipt = function () {
        this.bgdService.downloadReceipt(this.ref, 'F01').subscribe(function (response) {
            console.log("Response Do", response);
            // Handle the response
            var blob = new Blob([response], { type: 'application/pdf' });
            file_saver_1.saveAs(blob, 'receipt.pdf');
        }, function (error) {
            // Handle any errors that occur during the API request
            console.error('Error downloading receipt:', error);
        });
    };
    CashWalletTopupFailedComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet-topup-failed',
            templateUrl: './cash-wallet-topup-failed.component.html',
            styleUrls: ['./cash-wallet-topup-failed.component.scss']
        })
    ], CashWalletTopupFailedComponent);
    return CashWalletTopupFailedComponent;
}());
exports.CashWalletTopupFailedComponent = CashWalletTopupFailedComponent;
