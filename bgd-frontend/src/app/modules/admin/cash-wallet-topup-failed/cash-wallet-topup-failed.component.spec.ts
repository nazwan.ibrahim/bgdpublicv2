import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupFailedComponent } from './cash-wallet-topup-failed.component';

describe('CashWalletTopupFailedComponent', () => {
  let component: CashWalletTopupFailedComponent;
  let fixture: ComponentFixture<CashWalletTopupFailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupFailedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
