import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashWalletTopupFailedComponent } from './cash-wallet-topup-failed.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletTopupFailedRoutingModule } from './cash-wallet-topup-failed-routing.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
    declarations: [
        CashWalletTopupFailedComponent
    ],
    imports: [
        CommonModule,
        CashWalletTopupFailedRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class CashWalletTopupFailedModule { }
