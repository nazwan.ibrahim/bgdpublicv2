import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupFailedComponent } from 'app/modules/admin/cash-wallet-topup-failed/cash-wallet-topup-failed.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletTopupFailedComponent
        // resolve  : {
        //   data: CashWalletTopupResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletTopupFailedRoutingModule { }
