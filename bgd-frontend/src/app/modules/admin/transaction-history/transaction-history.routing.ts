import { Route} from '@angular/router';
import { TransactionHistoryComponent } from './transaction-history.component';

export const TransactionHistoryRoutes: Route[] = [
  {
      path     : '',
      component: TransactionHistoryComponent
  }
];

