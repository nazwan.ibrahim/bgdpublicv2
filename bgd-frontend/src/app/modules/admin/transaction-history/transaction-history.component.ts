import { DecimalPipe } from '@angular/common';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { bgdService } from '../bgd/bgd.service';
import { saveAs } from 'file-saver';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

/**
 * @title Table with pagination
 */
@Component({
  selector: 'bgd-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.scss']
})
export class TransactionHistoryComponent {
  displayedColumns: string[] = ['created_date', 'transaction', 'amount'];
  dataSource = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  pageSize: number = 10;
  currentPageIndex: number = 0;
  displayData = [];
  totalItemsCount: number;
  selectedOption: string = 'All';
  selectedFilter: string = 'All';
  details: any;

  constructor(
    private cashWalletService: CashWalletService,
    private _router: Router,
    private bgdService: bgdService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {

    this.cashWalletService.getTransactionHistories('All', 'All').subscribe(data => {
      console.log("Transaction ", data);

      const groupedTransactions = {};

      data.forEach(group => {
        group.forEach(trans => {
          const referenceId = trans.reference_id;

          // Filtering based on trans.status_code
          if (trans.status_code === 'G01' || trans.status_code === 'G05') {
            if (!groupedTransactions[referenceId]) {
              groupedTransactions[referenceId] = [];
            }

            if (trans.transaction_code === 'F01' || trans.transaction_code === 'F02' || trans.transaction_code === 'F03' || trans.transaction_code === 'F04' || trans.transaction_code === 'F05' || trans.transaction_code === 'F06') {
              trans.transaction = trans.transaction.replace('Marketplace Buy', 'Gold Purchase');
              trans.transaction = trans.transaction.replace('Bursa Store Sell', 'Gold Sale');
              trans.transaction = trans.transaction.replace('TopUp', 'Cash Top Up');
              trans.transaction = trans.transaction.replace('Withdraw', 'Cash Withdrawal');

              if (trans.amount < 0) {
                trans.transaction = trans.transaction.replace('Transfer', 'Gold Transfer');
              } else {
                trans.transaction = trans.transaction.replace('Transfer', 'Gold Receive');
              }

              trans.transaction = trans.transaction.replace('Redeem', 'Gold Redemption');
              trans.status = trans.status.replace('Completed', 'Successful');
              trans.status = trans.status.replace('Reject', 'Unsuccessful');
              trans.status = trans.status.replace('Failed', 'Cancelled');

              if (trans.uom === "Gram") {
                trans.uom = trans.uom.replace('Gram', 'g');
              } else {
                if (trans.amount < 0) {
                  trans.amount = Math.abs(trans.amount);
                  trans.uom = trans.uom.replace('RM', '-RM');
                }
              }

              groupedTransactions[referenceId].push(trans);
            }
          }
        });
      });

      this.dataSource = Object.values(groupedTransactions);
      console.log("Transaction", this.dataSource);
      this.totalItemsCount = this.dataSource.length;
      this.displayData = this.dataSource.slice(0, this.pageSize);

      console.log("Display All:", this.displayData);
    });

    // Load initial page
    this.loadPage(0);
  }

  onOptionChange(option: string) {

    this.selectedOption = option;

    this.filterData();
  }

  onFilterSelect(filter: string) {

    this.selectedFilter = filter;

    this.filterData();
  }

  onPageChange(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPageIndex = event.pageIndex;

    // Load the selected page
    this.loadPage(event.pageIndex);
  }

  loadPage(pageIndex: number) {
    // Calculate the range of items to display based on the selected page
    const startIndex = pageIndex * this.pageSize;
    const endIndex = startIndex + this.pageSize;

    // Set displayData to the range of items to display
    this.displayData = this.dataSource.slice(startIndex, endIndex);

  }

  filterData() {

    this.cashWalletService.getTransactionHistories(this.selectedOption, this.selectedFilter)
      .subscribe(data => {
        console.log("Transaction ", data);
        if (this.selectedFilter !== 'All') {
          const groupedTransactions = {};

          data.forEach(group => {
            group.forEach(trans => {
              const referenceId = trans.reference_id;

                if (!groupedTransactions[referenceId]) {
                  groupedTransactions[referenceId] = [];
                }

                trans.transaction = trans.transaction.replace('Marketplace Buy', 'Gold Purchase');
                trans.transaction = trans.transaction.replace('Bursa Store Sell', 'Gold Sale');
                trans.transaction = trans.transaction.replace('TopUp', 'Cash Top Up');
                trans.transaction = trans.transaction.replace('Withdraw', 'Cash Withdrawal');

                if (trans.amount < 0) {
                  trans.transaction = trans.transaction.replace('Transfer', 'Gold Transfer');
                } else {
                  trans.transaction = trans.transaction.replace('Transfer', 'Gold Receive');
                }

                trans.transaction = trans.transaction.replace('Redeem', 'Gold Redemption');
                trans.status = trans.status.replace('Completed', 'Successful');
                trans.status = trans.status.replace('Reject', 'Unsuccessful');
                trans.status = trans.status.replace('Failed', 'Cancelled');

                if (trans.uom === "Gram") {
                  trans.uom = trans.uom.replace('Gram', 'g');
                } else {

                  if (trans.amount < 0) {
                    trans.amount = Math.abs(trans.amount);
                    trans.uom = trans.uom.replace('RM', '-RM');
                  }

                }

                groupedTransactions[referenceId].push(trans);

            });
          });

          console.log("Grouped Transactions", groupedTransactions);

          this.dataSource = Object.values(groupedTransactions);
          console.log("Transaction", this.dataSource);
          this.totalItemsCount = this.dataSource.length;
          this.displayData = this.dataSource.slice(0, this.pageSize);

          console.log("Display All:", this.displayData);
        } else {
          const groupedTransactions = {};

          data.forEach(group => {
            group.forEach(trans => {
              const referenceId = trans.reference_id;

                if (!groupedTransactions[referenceId]) {
                  groupedTransactions[referenceId] = [];
                }
                if (trans.transaction_code === 'F01' || trans.transaction_code === 'F02' || trans.transaction_code === 'F03' || trans.transaction_code === 'F04' || trans.transaction_code === 'F05' || trans.transaction_code === 'F06') {
                  trans.transaction = trans.transaction.replace('Marketplace Buy', 'Gold Purchase');
                  trans.transaction = trans.transaction.replace('Bursa Store Sell', 'Gold Sale');
                  trans.transaction = trans.transaction.replace('TopUp', 'Cash Top Up');
                  trans.transaction = trans.transaction.replace('Withdraw', 'Cash Withdrawal');

                  if (trans.amount < 0) {
                    trans.transaction = trans.transaction.replace('Transfer', 'Gold Transfer');
                  } else {
                    trans.transaction = trans.transaction.replace('Transfer', 'Gold Receive');
                  }

                  trans.transaction = trans.transaction.replace('Redeem', 'Gold Redemption');
                  trans.status = trans.status.replace('Completed', 'Successful');
                  trans.status = trans.status.replace('Reject', 'Unsuccessful');
                  trans.status = trans.status.replace('Failed', 'Cancelled');

                  if (trans.uom === "Gram") {
                    trans.uom = trans.uom.replace('Gram', 'g');
                  } else {

                    if (trans.amount < 0) {
                      trans.amount = Math.abs(trans.amount);
                      trans.uom = trans.uom.replace('RM', '-RM');
                    }

                  }

                  groupedTransactions[referenceId].push(trans);
                }
            });
          });

          console.log("Grouped Transactions", groupedTransactions);

          this.dataSource = Object.values(groupedTransactions);
          console.log("Transaction", this.dataSource);
          this.totalItemsCount = this.dataSource.length;
          this.displayData = this.dataSource.slice(0, this.pageSize);

          console.log("Display All:", this.displayData);
        }
      });
  }

  getDetails(referenceId, transactionTypeCode) {

    console.log("ReferenceId:", referenceId);
    console.log("transaction:", transactionTypeCode);

    this.bgdService.downloadReceipt(referenceId, transactionTypeCode).subscribe(
      (response: any) => {
        console.log("Response:", response);
        // Handle the response
        const blob = new Blob([response], { type: 'application/pdf' });

        // Create a URL for the blob object
        const url = URL.createObjectURL(blob);

        // Open the PDF in a new window or tab for the user to view
        // const sanitizedUrl: SafeUrl = this.sanitizer.bypassSecurityTrustUrl(url);
        // window.open(sanitizedUrl.toString(), '_blank');

        window.open(url, '_blank');
        // saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        // Handle any errors that occur during the API request
        console.error('Error downloading receipt:', error);
      }
    )
    // this.cashWalletService.getTransactionDetails(referenceId, transactionTypeCode)
    //   .subscribe(data => {
    //     this.details = data;
    //     console.log(this.details);

    //     if (transactionTypeCode === "F01") {
    //       this._router.navigate(['/transaction-history-details-topup'], { queryParams: { data: JSON.stringify(this.details) } })
    //     } else if (transactionTypeCode === "F02") {
    //       this._router.navigate(['/transaction-history-details-withdraw'], { queryParams: { data: JSON.stringify(this.details) } })
    //     } else if (transactionTypeCode === "F03") {
    //       this._router.navigate(['/transaction-history-details-transfer'], { queryParams: { data: JSON.stringify(this.details) } })
    //     } else if (transactionTypeCode === "F04") {
    //       this._router.navigate(['/transaction-history-details-redeem'], { queryParams: { data: JSON.stringify(this.details) } })
    //     } else if (transactionTypeCode === "F05") {
    //       this._router.navigate(['/transaction-history-details-buy-bgd'], { queryParams: { data: JSON.stringify(this.details) } })
    //     } else if (transactionTypeCode === "F06") {
    //       this._router.navigate(['/transaction-history-details-sell-bgd'], { queryParams: { data: JSON.stringify(this.details) } })
    //     }

    //   })
  }

}
