import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionHistoryRoutes } from './transaction-history.routing';
import { TransactionHistoryComponent } from './transaction-history.component';
import { RouterModule } from '@angular/router';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    TransactionHistoryComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(TransactionHistoryRoutes),
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule
  ]
})
export class TransactionHistoryModule { }
