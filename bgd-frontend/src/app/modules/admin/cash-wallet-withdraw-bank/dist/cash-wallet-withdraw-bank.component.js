"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletWithdrawBankComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var CashWalletWithdrawBankComponent = /** @class */ (function () {
    function CashWalletWithdrawBankComponent(_activatedRoute, _router, _formBuilder, cashWalletService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this.cashWalletService = cashWalletService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        this.acctNumber = '';
        this.accountNumber = this._activatedRoute.snapshot.queryParams['data'];
        this.bankName = this._activatedRoute.snapshot.queryParams['bank'];
        // this.topupDetails = this._activatedRoute.snapshot.queryParams['response'];
        // console.log(this.topupDetails);
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.amount = response[0].amount;
        this.acctNumber = response[0].acctNumber;
        this.amount = response[0].amount;
        this.walletTypeCode = response[0].walletTypeCode;
        this.regTypeCode = response[0].regTypeCode;
        this.uomCode = response[0].uomCode;
        this.transactionMethodCode = response[0].transactionMethodCode;
        this.transactionTypeCode = response[0].transactionTypeCode;
        this.statusCode = response[0].statusCode;
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.setupTransFee = response[0].feesForms[0].setupFees;
        this.setupTransOum = response[0].feesForms[0].setupUom;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.setupSstFee = response[0].feesForms[1].setupFees;
        this.setupSstOum = response[0].feesForms[1].setupUom;
        this.totalAmount = response[0].totalAmount;
    }
    /*   ngOnInit() {
    
        this.amount = this.amount - this.chargeSstFee - this.chargeTransFee;
    
      } */
    CashWalletWithdrawBankComponent.prototype.backButton = function () {
        this._router.navigate(['/cash-wallet-withdraw']);
    };
    // nextButton(): void {
    //   this._router.navigate(['/cash-wallet-withdraw-pin'], { queryParams: { response: this.topupDetails, account: this.accountNumber, bank: this.bankName } })
    // }
    CashWalletWithdrawBankComponent.prototype.confirmWithdraw = function () {
        //const requestBody = [{
        //  "acctNumber": this.acctNumber,
        //  "amount": this.amount,
        //  "walletTypeCode": this.walletTypeCode,
        //  "regTypeCode": this.regTypeCode,
        //  "uomCode": this.uomCode,
        //  "transactionMethodCode": "H01",
        //  "transactionTypeCode": this.transactionTypeCode,
        //  "statusCode": "G01",
        //  "feesForms": [
        //    {
        //      "feeType": "Transaction Fee",
        //      "feeTypeCode": "F09",
        //      "setupFees": 0.02,
        //      "setupUom": "%",
        //      "setupUomCode": "J04",
        //      "chargeFees": this.chargeTransFee,
        //      "chargeUom": "RM",
        //      "chargeUomCode": "J01"
        //    },
        //    {
        //      "feeType": "SST",
        //      "feeTypeCode": "TAX01",
        //      "setupFees": 0,
        //      "setupUom": "%",
        //      "setupUomCode": null,
        //      "chargeFees": this.chargeSstFee,
        //      "chargeUom": "RM",
        //      "chargeUomCode": "J01"
        //    }
        //  ],
        //  "totalAmount": this.totalAmount
        //}]
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log(response);
        this._router.navigate(['/cash-wallet-withdraw-pin'], { queryParams: { response: JSON.stringify(response), data: this.accountNumber, bank: this.bankName } });
    };
    CashWalletWithdrawBankComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet-withdraw-bank',
            templateUrl: './cash-wallet-withdraw-bank.component.html',
            styleUrls: ['./cash-wallet-withdraw-bank.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], CashWalletWithdrawBankComponent);
    return CashWalletWithdrawBankComponent;
}());
exports.CashWalletWithdrawBankComponent = CashWalletWithdrawBankComponent;
