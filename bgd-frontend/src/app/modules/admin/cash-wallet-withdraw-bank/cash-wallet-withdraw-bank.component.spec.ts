import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletWithdrawBankComponent } from './cash-wallet-withdraw-bank.component';

describe('CashWalletWithdrawBankComponent', () => {
  let component: CashWalletWithdrawBankComponent;
  let fixture: ComponentFixture<CashWalletWithdrawBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletWithdrawBankComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletWithdrawBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
