import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletWithdrawBankComponent } from './cash-wallet-withdraw-bank.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletWithdrawBankComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletWithdrawBankRoutingModule { }
