import { Location } from '@angular/common';
import { Component, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';

@Component({
  selector: 'bgd-cash-wallet-withdraw-bank',
  templateUrl: './cash-wallet-withdraw-bank.component.html',
  styleUrls: ['./cash-wallet-withdraw-bank.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CashWalletWithdrawBankComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  today: Date = new Date();
  accountNumber: any;
  bankName: string;
  topupDetails: any;
  amount: any;
  chargeTransFee: any;
  acctNumber: any = '';
  walletTypeCode: any;
  regTypeCode: any;
  uomCode: any;
  totalAmount: any;
  chargeSstFee: any;
  statusCode: any;
  transactionMethodCode: any;
  transactionTypeCode: any;
  response: any;
  setupTransFee: any;
  setupTransOum: any;
  setupSstFee: any;
  setupSstOum: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private location: Location,
    private walletService: WalletService
  ) {}

  ngOnInit(): void {

    this.bankName = this.walletService.bankName;
    this.accountNumber = this.walletService.accountBank;

    const response = this.walletService.initWithdraw;
    this.amount = response[0].amount;
    this.acctNumber = response[0].acctNumber;
    this.amount = response[0].amount;
    this.walletTypeCode = response[0].walletTypeCode;
    this.regTypeCode = response[0].regTypeCode;
    this.uomCode = response[0].uomCode;
    this.transactionMethodCode = response[0].transactionMethodCode;
    this.transactionTypeCode = response[0].transactionTypeCode;
    this.statusCode = response[0].statusCode;
    this.chargeTransFee = response[0].feesForms[0].chargeFees;
    this.setupTransFee = response[0].feesForms[0].setupFees;
    this.setupTransOum = response[0].feesForms[0].setupUom;
    this.chargeSstFee = response[0].feesForms[1].chargeFees;
    this.setupSstFee = response[0].feesForms[1].setupFees;
    this.setupSstOum = response[0].feesForms[1].setupUom;
    this.totalAmount = response[0].totalAmount;
    
  }

  back(): void {
    this.location.back();
  }

  confirmWithdraw(): void {
    this._router.navigate(['/cash-wallet-withdraw-pin']);
  }


}
