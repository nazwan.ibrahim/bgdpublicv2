import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralRedeemComponent } from './referral-redeem.component';

describe('ReferralRedeemComponent', () => {
  let component: ReferralRedeemComponent;
  let fixture: ComponentFixture<ReferralRedeemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferralRedeemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReferralRedeemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
