import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupPaynetComponent } from './cash-wallet-topup-paynet.component';

describe('CashWalletTopupFpxComponent', () => {
  let component: CashWalletTopupPaynetComponent;
  let fixture: ComponentFixture<CashWalletTopupPaynetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CashWalletTopupPaynetComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupPaynetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
