import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashWalletTopupPaynetComponent } from './cash-wallet-topup-paynet.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletTopupPaynetRoutingModule } from './cash-wallet-topup-paynet-routing.module';


@NgModule({
  declarations: [
    CashWalletTopupPaynetComponent
  ],
  imports: [
    CommonModule,
    CashWalletTopupPaynetRoutingModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
  ]
})
export class CashWalletTopupPaynetModule { }
