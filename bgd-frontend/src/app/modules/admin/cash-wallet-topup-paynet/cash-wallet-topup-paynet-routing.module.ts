import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupPaynetComponent } from 'app/modules/admin/cash-wallet-topup-paynet/cash-wallet-topup-paynet.component';

const routes: Routes = [
  {
    path: '',
    component: CashWalletTopupPaynetComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletTopupPaynetRoutingModule { }
