import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseValidators } from '@bgd/validators';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';

@Component({
  selector: 'bgd-cash-wallet-topup-paynet',
  templateUrl: './cash-wallet-topup-paynet.component.html',
  styleUrls: ['./cash-wallet-topup-paynet.component.scss']
})
export class CashWalletTopupPaynetComponent {

  endToEndIdSignature: string;
  endToEndId: string;
  status: any;
  response: any;

  constructor(
    private _authService: AuthService,
    private _cashWalletService: CashWalletService,
    private _formBuilder: UntypedFormBuilder,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private walletService: WalletService
  ) {

  }

  ngOnInit() {
    this._checkTopupStatus();
  }

  private _checkTopupStatus() {


    this.endToEndId = this._activatedRoute.snapshot.queryParamMap.get('EndtoEndId');
    this.endToEndIdSignature = this._activatedRoute.snapshot.queryParamMap.get('EndtoEndIdSignature');

    console.log("endToendId", this.endToEndId);
    console.log("endToendId", this.endToEndIdSignature);

    // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    this._cashWalletService.topupStatus(this.endToEndId, this.endToEndIdSignature)
      .subscribe(
        Response => {
          console.log("Topup Status:", Response);

          this.response = Response;
          this.walletService.topupResponse = this.response;

          if (Response.statusCode == "G01") {
            // const data = decodeURIComponent(this._activatedRoute.snapshot.queryParams['data']);
            setTimeout(() => {
              this._router.navigate(['/cash-wallet-topup-status']);
            }, 10000); // delay for 1 seconds (1000 milliseconds)
          } else if (Response.statusCode == "G05") {
            // const data = decodeURIComponent(this._activatedRoute.snapshot.queryParams['data']);
            setTimeout(() => {
              this._router.navigate(['/cash-wallet-topup-pending']);
            }, 10000); // delay for 1 seconds (1000 milliseconds)
          } else {
            setTimeout(() => {
              this._router.navigate(['/cash-wallet-topup-failed']);
            }, 10000); // delay for 1 seconds (1000 milliseconds)
          }
        },

        Response => {
          console.log("Topup Status:", Response);

          // const data = decodeURIComponent(this._activatedRoute.snapshot.queryParams['data']);
          setTimeout(() => {
            this._router.navigate(['/cash-wallet-topup-failed']);
          }, 10000); // delay for 1 seconds (1000 milliseconds)
        }
      );
  }
}
