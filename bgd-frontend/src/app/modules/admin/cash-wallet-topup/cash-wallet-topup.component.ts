import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { finalize, map } from 'rxjs';

@Component({
  selector: 'bgd-cash-wallet-topup',
  templateUrl: './cash-wallet-topup.component.html',
  styleUrls: ['./cash-wallet-topup.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CashWalletTopupComponent {

  @ViewChild('cashWalletTopupNgForm') cashWalletTopupNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  availAmount: any;
  accountNum: any;
  cashWalletTopupForm: UntypedFormGroup;
  response: any;
  inputAmount: number;
  fullName: any = '';
  costModel: any;
  bursaCostIndex: number;
  setupFees: any;
  setupUom: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private cashWalletService: CashWalletService,
    private _formBuilder: UntypedFormBuilder,
    private _router: Router,
    private _dashboardService: DashboardService,
    private walletService: WalletService

  ) {}

  ngOnInit(): void {

    this.accountNum = this.walletService.accountWallet;
    this.availAmount = this.walletService.availAmount;
    this.fullName = this.walletService.fullName;
    
    this.cashWalletTopupForm = this._formBuilder.group({
      price: ['', Validators.required],
    });

    this.cashWalletService.getbursaCostRecovery().pipe(
      map((value: any) => {
        this.costModel = value;

        for (var i = 0; i < this.costModel.length; i++) {
          if (this.costModel[i].feeTypeCode == "B01") {
            this.bursaCostIndex = i;
            break;
          }
        }

        return this.costModel;
      })
    ).subscribe((data: any) => {

      this.setupFees = data[this.bursaCostIndex].setupFees;
      this.setupUom = data[this.bursaCostIndex].setupUom;
      
    });
  }

  selectAmount(value: number) {
    this.inputAmount = value;
  }

  backButton(): void {
    this._router.navigate(['/cash-wallet']);
  }

  topupMethod() {
    if (this.cashWalletTopupForm.invalid) {
      return;
    }

    this.cashWalletTopupForm.disable();

    const acctNumber = this.accountNum;
    const amount = this.cashWalletTopupForm.get('price').value;
    const transactionTypeCode = "F01";
    const walletTypeCode = "E01";
    const channelCode = "BW";
    const currency = "MYR";
    const customerName = this.fullName;


    this.cashWalletService.getinitTopup(acctNumber, amount, transactionTypeCode, walletTypeCode, channelCode, currency, customerName)
      .pipe(
        finalize(() => {
          this.cashWalletTopupForm.enable();

          this.cashWalletTopupForm.reset();
        })
      )
      .subscribe(
        Response => {

          this.alert = {
            type: 'success',
            message: 'Success'
          };
          
          console.log("initResponse:", Response);

          this.response = Response;
          this.walletService.initTopup = this.response;

          this._router.navigate(['/cash-wallet-topup-bank']);

        },
        error => {

          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };

          this._router.navigate(['/transaction-limit']);

        }
      );
  }
}
