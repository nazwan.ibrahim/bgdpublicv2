import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupComponent } from './cash-wallet-topup.component';

describe('CashWalletTopupComponent', () => {
  let component: CashWalletTopupComponent;
  let fixture: ComponentFixture<CashWalletTopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
