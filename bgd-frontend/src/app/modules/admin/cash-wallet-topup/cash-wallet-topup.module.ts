import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupComponent } from './cash-wallet-topup.component';
import { CashWalletTopupRoutingModule } from './cash-wallet-topup-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';

@NgModule({
    declarations: [
      CashWalletTopupComponent
    ],
  imports: [
    CommonModule,
    CashWalletTopupRoutingModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatCardModule
  ]
})
export class CashWalletTopupModule { }