import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupComponent } from 'app/modules/admin/cash-wallet-topup/cash-wallet-topup.component';

const routes: Routes = [
  {
    path: '',
    component: CashWalletTopupComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletTopupRoutingModule { }
