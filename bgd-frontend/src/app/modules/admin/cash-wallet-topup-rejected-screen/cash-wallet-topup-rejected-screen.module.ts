import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { CashWalletTopupRejectedScreenComponent } from './cash-wallet-topup-rejected-screen.component';
import { CashWalletTopupRejectedScreenRoutingModule } from './cash-wallet-topup-rejected-screen-routing.module';


@NgModule({
    declarations: [
        CashWalletTopupRejectedScreenComponent
    ],
    imports: [
        CommonModule,
        CashWalletTopupRejectedScreenRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class CashWalletTopupRejectedScreenModule { }
