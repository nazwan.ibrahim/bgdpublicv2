import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'bgd-cash-wallet-topup-rejected-screen',
  templateUrl: './cash-wallet-topup-rejected-screen.component.html',
  styleUrls: ['./cash-wallet-topup-rejected-screen.component.scss']
})
export class CashWalletTopupRejectedScreenComponent {

  constructor(
    private _router: Router
  ) {}

  viewDetails() {
    this._router.navigate(['/cash-wallet-topup-rejected']);
  }

}
