import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupRejectedScreenComponent } from './cash-wallet-topup-rejected-screen.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletTopupRejectedScreenComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletTopupRejectedScreenRoutingModule { }
