import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupRejectedScreenComponent } from './cash-wallet-topup-rejected-screen.component';

describe('CashWalletTopupRejectedScreenComponent', () => {
  let component: CashWalletTopupRejectedScreenComponent;
  let fixture: ComponentFixture<CashWalletTopupRejectedScreenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupRejectedScreenComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupRejectedScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
