import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReferralRoutes } from './referral.routing';
import { ReferralComponent } from './referral.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    ReferralComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ReferralRoutes),
  ]
})
export class ReferralModule { }
