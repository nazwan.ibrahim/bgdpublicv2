import { Route} from '@angular/router';
import { ReferralComponent } from './referral.component';

export const ReferralRoutes: Route[] = [
  {
      path     : '',
      component: ReferralComponent
  }
];

