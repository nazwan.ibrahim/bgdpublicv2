import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupAmountComponent } from './cash-wallet-topup-amount.component';

describe('CashWalletTopupAmountComponent', () => {
  let component: CashWalletTopupAmountComponent;
  let fixture: ComponentFixture<CashWalletTopupAmountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupAmountComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
