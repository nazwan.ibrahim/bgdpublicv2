import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupAmountComponent } from 'app/modules/admin/cash-wallet-topup-amount/cash-wallet-topup-amount.component';

const routes: Routes = [
  {
    path: '',
    component: CashWalletTopupAmountComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletTopupAmountRoutingModule { }
