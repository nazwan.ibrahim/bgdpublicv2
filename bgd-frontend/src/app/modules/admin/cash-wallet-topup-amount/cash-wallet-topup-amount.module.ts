import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//import { CashWalletTopuAmountRoutingModule } from './cash-wallet-topup-amount.routing.module';
import { CashWalletTopupAmountComponent } from './cash-wallet-topup-amount.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletTopupAmountRoutingModule } from './cash-wallet-topup-amount-routing.module';


@NgModule({
  declarations: [
    CashWalletTopupAmountComponent
  ],
  imports: [
    CommonModule,
    CashWalletTopupAmountRoutingModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
  ]
})
export class CashWalletTopupAmountModule { }
