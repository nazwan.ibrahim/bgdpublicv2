import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root',
})
export class bgdService {
    constructor(private _httpClient: HttpClient) { }

    //Read data
    identificationType(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/identificationType');
    }


    confirmPurchase(requestBody: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._httpClient.put(environment.apiUrl + '/api/wallet/confirmPurchase', requestBody, { headers });
    }

    initPurchase(totalPrice: string, quantity: number, type: string, inputType: string, price: number, reference: string): Observable<any> {

        const params = new HttpParams()
            .set('totalPrice', totalPrice)
            .set('quantity', quantity)
            .set('type', type)
            .set('inputType', inputType)
            .set('price', price)
            .set('reference', reference);

        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/initPurchase', { params: params });
    }

    initTransfer(acctNumberSender: any, acctNumberReceiver: any, amount: any, statusCode: any, reason: any): Observable<any> {
        const params = new HttpParams()
            .set('acctNumberSender', acctNumberSender)
            .set('acctNumberReceiver', acctNumberReceiver)
            .set('amount', amount)
            .set('statusCode', statusCode)
            .set('reason', reason);
        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/initTransfer', { params });
    }

    walletDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/walletDetails');
    }

    getUserDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getUserDetails');
    }

    userAddress(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getAddress');
    }

    updateAddress(newAddress: any): Observable<any> {
        return this._httpClient.post<any>(environment.apiUrl + '/api/onboarding/updateAddress', newAddress);
    }

    verifyPin(pin: { userName: string; email: string; userPin: string; }): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/verifyPin', pin);
    }

    validateAccount(acctNumber: string) {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        const url = `/api/wallet/validateWallet?acctNumber=${encodeURIComponent(acctNumber)}`;
        return this._httpClient.get(environment.apiUrl + url, { responseType: 'text' });
    }

    redeemBGD(requestBody: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._httpClient.put(environment.apiUrl + '/api/wallet/redeemBGD', requestBody, { headers });
    }

    transferWallet(requestBody: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._httpClient.put(environment.apiUrl + '/api/wallet/transferWallet', requestBody, { headers });
    }

    // getUnitPrice(): Observable<any> {
    //     return this._httpClient.get<any>(environment.apiUrl + '/api/administration/initTransfer')
    // }

    downloadReceipt(referenceId: any, transactionTypeCode: any,): Observable<any> {
        const params = new HttpParams()
            .set('referenceId', referenceId)
            .set('transactionTypeCode', transactionTypeCode)
        return this._httpClient.get(environment.apiUrl + '/api/wallet/downloadReceipt', {
            params,
            responseType: 'blob'
        });
    }
}

