"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RedeemBgdAmountComponent = void 0;
var core_1 = require("@angular/core");
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
var RedeemBgdAmountComponent = /** @class */ (function () {
    function RedeemBgdAmountComponent(_activatedRoute, router, _dashboardService, _authService, _redeemBgdService) {
        this._activatedRoute = _activatedRoute;
        this.router = router;
        this._dashboardService = _dashboardService;
        this._authService = _authService;
        this._redeemBgdService = _redeemBgdService;
        this.walletDetails = [];
        this.cashWalletIndex = -1;
        this.selectedOption = 1;
        this.availAmount = 0;
        this.remarks = 'test';
        this.disableButton = false;
        // define goldRedeemOptions as an empty array initially
        this.goldRedeemOptions = [];
        this.alert = {
            type: 'error',
            message: ''
        };
    }
    RedeemBgdAmountComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._redeemBgdService.walletDetails().pipe(operators_1.tap(function (value) {
            if (!value) {
                return rxjs_1.of(null);
            }
            _this.walletModel = value;
            for (var i = 0; i < _this.walletModel.length; i++) {
                if (_this.walletModel[i].type == "Gold") {
                    _this.goldWalletIndex = i;
                    break;
                }
            }
            return _this._redeemBgdService.walletDetails();
        }), operators_1.catchError(function (error) {
            return rxjs_1.of(null);
        })).subscribe(function (data) {
            if (!data) {
                return rxjs_1.of(null);
            }
            _this.availAmount = data[_this.goldWalletIndex].availableAmount.toFixed(6);
            _this.acctNumber = data[_this.goldWalletIndex].acctNumber;
            _this.checkRedeem();
        });
        this._dashboardService.getUserDetails().subscribe(function (data) {
            _this.fullName = data.fullName;
            _this.phoneNumber = data.phoneNumber;
        });
        // this._dashboardService.getUserAddress().pipe(
        //   map((value: any) => {
        //     this.addressModel = value;
        //     for (var i = 0; i < this.addressModel.length; i++) {
        //       if (this.addressModel[i].addressType.code == "L04") {
        //         this.addressIndex = i;
        //         break;
        //       } else {
        //         this.addressModel[i].addressType.code == "L01";
        //         this.addressIndex = i;
        //       }
        //     }
        //     return this.addressModel;
        //   })
        // ).subscribe((data: any) => {
        //   console.log("ADDRESS:", data);
        //   this.address = data[this.addressIndex].address1 ?? null;
        //   this.address2 = data[this.addressIndex].address2 ?? null;
        //   this.postcode = data[this.addressIndex].postcode ?? null;
        //   this.town = data[this.addressIndex].town ?? null;
        //   this.state = data[this.addressIndex].state?.name ?? null;
        //   this.label = data[this.addressIndex].label ?? null;
        // });
    };
    RedeemBgdAmountComponent.prototype.checkRedeem = function () {
        var onePieceOfGoldWeight = 4.250000; // weight of one piece of gold in grams
        var availableGoldWeight = this.availAmount; // available amount of gold weight in grams
        // check if the available gold weight is sufficient to redeem at least one piece of gold
        if (availableGoldWeight >= onePieceOfGoldWeight) {
            var numberOfGoldPieces = Math.floor(availableGoldWeight / onePieceOfGoldWeight); // calculate the number of gold pieces that can be redeemed
            // create an array of options for the user to select from
            var options = [];
            for (var i = 1; i <= numberOfGoldPieces; i++) {
                options.push(i);
            }
            // set the options for the user to select from
            this.goldRedeemOptions = options;
        }
        else {
            // set the options to an empty array if the user does not have enough gold to redeem
            this.goldRedeemOptions = [];
            this.alert.type = 'error';
            this.alert.message = 'You do not have enough gold to redeem.';
        }
    };
    RedeemBgdAmountComponent.prototype.backButton = function () {
        this.router.navigate(['/redeem-bgd']);
    };
    RedeemBgdAmountComponent.prototype.redeemSelect = function () {
        var _this = this;
        this.disableButton = true;
        if (this.selectedOption) { // check if the user has selected an option
            this._dashboardService.initRedeem(this.acctNumber, this.selectedOption, this.fullName, this.phoneNumber, this.address, this.address2, this.postcode, this.town, this.state)
                .subscribe(function (response) {
                console.log("Response InitRedeem:", response);
                _this.response = response;
                var acctNumber = response[0].acctNumber;
                var weight = response[0].weight;
                var remarks = response[0].remarks;
                var serialNumber = response[0].serialNumber;
                var reference = response[0].reference;
                var name = response[0].name;
                var phone = response[0].phone;
                var courierCharge = response[0].feesForms[0].chargeFees;
                var courierfeeTypeCode = response[0].feesForms[0].feeTypeCode;
                var couriersetupFees = response[0].feesForms[0].setupFees;
                var couriersetupUom = response[0].feesForms[0].setupUom;
                var couriersetupUomCode = response[0].feesForms[0].setupUomCode;
                var courierchargeFees = response[0].feesForms[0].chargeFees;
                var courierchargeUom = response[0].feesForms[0].chargeUom;
                var courierchargeUomCode = response[0].feesForms[0].chargeUomCode;
                var SSTCharge = response[0].feesForms[1].chargeFees;
                var SSTfeeTypeCode = response[0].feesForms[1].feeTypeCode;
                var SSTsetupFees = response[0].feesForms[1].setupFees;
                var SSTsetupUom = response[0].feesForms[1].setupUom;
                var SSTsetupUomCode = response[0].feesForms[1].setupUomCode;
                var SSTchargeFees = response[0].feesForms[1].chargeFees;
                var SSTchargeUom = response[0].feesForms[1].chargeUom;
                var SSTchargeUomCode = response[0].feesForms[1].chargeUomCode;
                var takafulCharge = response[0].feesForms[2].chargeFees;
                var takafulfeeTypeCode = response[0].feesForms[2].feeTypeCode;
                var takafulsetupFees = response[0].feesForms[2].setupFees;
                var takafulsetupUom = response[0].feesForms[2].setupUom;
                var takafulsetupUomCode = response[0].feesForms[2].setupUomCode;
                var takafulchargeFees = response[0].feesForms[2].chargeFees;
                var takafulchargeUom = response[0].feesForms[2].chargeUom;
                var takafulchargeUomCode = response[0].feesForms[2].chargeUomCode;
                var SST2Charge = response[0].feesForms[3].chargeFees;
                var SST2feeTypeCode = response[0].feesForms[3].feeTypeCode;
                var SST2setupFees = response[0].feesForms[3].setupFees;
                var SST2setupUom = response[0].feesForms[3].setupUom;
                var SST2setupUomCode = response[0].feesForms[3].setupUomCode;
                var SST2chargeFees = response[0].feesForms[3].chargeFees;
                var SST2chargeUom = response[0].feesForms[3].chargeUom;
                var SST2chargeUomCode = response[0].feesForms[3].chargeUomCode;
                var mintingCharge = response[0].feesForms[4].chargeFees;
                var mintingfeeTypeCode = response[0].feesForms[4].feeTypeCode;
                var mintingsetupFees = response[0].feesForms[4].setupFees;
                var mintingsetupUom = response[0].feesForms[4].setupUom;
                var mintingsetupUomCode = response[0].feesForms[4].setupUomCode;
                var mintingchargeFees = response[0].feesForms[4].chargeFees;
                var mintingchargeUom = response[0].feesForms[4].chargeUom;
                var mintingchargeUomCode = response[0].feesForms[4].chargeUomCode;
                var SST3Charge = response[0].feesForms[5].chargeFees;
                var SST3feeTypeCode = response[0].feesForms[5].feeTypeCode;
                var SST3setupFees = response[0].feesForms[5].setupFees;
                var SST3setupUom = response[0].feesForms[5].setupUom;
                var SST3setupUomCode = response[0].feesForms[5].setupUomCode;
                var SST3chargeFees = response[0].feesForms[5].chargeFees;
                var SST3chargeUom = response[0].feesForms[5].chargeUom;
                var SST3chargeUomCode = response[0].feesForms[5].chargeUomCode;
                var unit = response[0].unit;
                _this.router.navigate(['/redeem-bgd-address'], {
                    queryParams: {
                        acctNumber: acctNumber,
                        weight: weight,
                        remarks: remarks,
                        serialNumber: serialNumber,
                        reference: reference,
                        name: name,
                        phone: phone,
                        courierCharge: courierCharge,
                        courierfeeTypeCode: courierfeeTypeCode,
                        couriersetupFees: couriersetupFees,
                        couriersetupUom: couriersetupUom,
                        couriersetupUomCode: couriersetupUomCode,
                        courierchargeFees: courierchargeFees,
                        courierchargeUom: courierchargeUom,
                        courierchargeUomCode: courierchargeUomCode,
                        SSTCharge: SSTCharge,
                        SSTfeeTypeCode: SSTfeeTypeCode,
                        SSTsetupFees: SSTsetupFees,
                        SSTsetupUom: SSTsetupUom,
                        SSTsetupUomCode: SSTsetupUomCode,
                        SSTchargeFees: SSTchargeFees,
                        SSTchargeUom: SSTchargeUom,
                        SSTchargeUomCode: SSTchargeUomCode,
                        takafulCharge: takafulCharge,
                        takafulfeeTypeCode: takafulfeeTypeCode,
                        takafulsetupFees: takafulsetupFees,
                        takafulsetupUom: takafulsetupUom,
                        takafulsetupUomCode: takafulsetupUomCode,
                        takafulchargeFees: takafulchargeFees,
                        takafulchargeUom: takafulchargeUom,
                        takafulchargeUomCode: takafulchargeUomCode,
                        SST2Charge: SST2Charge,
                        SST2feeTypeCode: SST2feeTypeCode,
                        SST2setupFees: SST2setupFees,
                        SST2setupUom: SST2setupUom,
                        SST2setupUomCode: SST2setupUomCode,
                        SST2chargeFees: SST2chargeFees,
                        SST2chargeUom: SST2chargeUom,
                        SST2chargeUomCode: SST2chargeUomCode,
                        mintingCharge: mintingCharge,
                        mintingfeeTypeCode: mintingfeeTypeCode,
                        mintingsetupFees: mintingsetupFees,
                        mintingsetupUom: mintingsetupUom,
                        mintingsetupUomCode: mintingsetupUomCode,
                        mintingchargeFees: mintingchargeFees,
                        mintingchargeUom: mintingchargeUom,
                        mintingchargeUomCode: mintingchargeUomCode,
                        SST3Charge: SST3Charge,
                        SST3feeTypeCode: SST3feeTypeCode,
                        SST3setupFees: SST3setupFees,
                        SST3setupUom: SST3setupUom,
                        SST3setupUomCode: SST3setupUomCode,
                        SST3chargeFees: SST3chargeFees,
                        SST3chargeUom: SST3chargeUom,
                        SST3chargeUomCode: SST3chargeUomCode,
                        unit: unit
                    }
                });
            });
        }
        else {
            this.disableButton = false;
            // display an error message if the user hasn't selected an option
            this.alert.type = 'error';
            this.alert.message = 'Please select a number of gold pieces to redeem.';
        }
        this.disableButton = false;
    };
    RedeemBgdAmountComponent = __decorate([
        core_1.Component({
            selector: 'bgd-redeem-bgd-amount',
            templateUrl: './redeem-bgd-amount.component.html',
            styleUrls: ['./redeem-bgd-amount.component.scss']
        })
    ], RedeemBgdAmountComponent);
    return RedeemBgdAmountComponent;
}());
exports.RedeemBgdAmountComponent = RedeemBgdAmountComponent;
