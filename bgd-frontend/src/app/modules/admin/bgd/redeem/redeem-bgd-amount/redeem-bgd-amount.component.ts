import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { FuseAlertType } from '@bgd/components/alert';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { catchError, map, tap } from 'rxjs/operators';
import { redeemBgdService } from '../redeem-bgd/redeem-bgd.service';
import { of } from 'rxjs';
import { RedeemService } from '../redeem.service';

@Component({
  selector: 'bgd-redeem-bgd-amount',
  templateUrl: './redeem-bgd-amount.component.html',
  styleUrls: ['./redeem-bgd-amount.component.scss']
})
export class RedeemBgdAmountComponent {

  walletDetails = [];
  walletModel: any;
  cashWalletIndex: number = -1;
  goldWalletIndex: number;
  selectedOption: number = 1;
  availAmount: number = 0;
  curAccNum: any;
  fullName: any;
  phoneNumber: any;
  address1: any;
  address2: any;
  postcode: any;
  town: string;
  acctNumber: any;
  unit: any;
  name: any;
  phone: any;
  address: any;
  remarks: any = 'test';
  disableButton: boolean = false;
  tempValue: any;
  feesForms: any;
  addressForm: any;
  coinRedeemed: any;
  courierCharge: any;
  takafulCharge: any;
  mintingCharge: any;
  totalAmount: any;
  id: any;
  label: any;
  state: any;
  addressModel: any;
  addressIndex: any;
  response: any;
  // define goldRedeemOptions as an empty array initially
  goldRedeemOptions: number[] = [];

  alert: { type: FuseAlertType; message: string } = {
    type: 'error',
    message: ''
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private router: Router,
    private _dashboardService: DashboardService,
    private _authService: AuthService,
    private _redeemBgdService: redeemBgdService,
    private redeemService: RedeemService,
  ) { }

  ngOnInit(): void {
    this._redeemBgdService.walletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Gold") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._redeemBgdService.walletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }
      this.availAmount = data[this.goldWalletIndex].availableAmount.toFixed(6);
      this.acctNumber = data[this.goldWalletIndex].acctNumber;

      this.checkRedeem();
    });

    this._dashboardService.getUserDetails().subscribe(data => {
      this.fullName = data.fullName;
      this.phoneNumber = data.phoneNumber;
    });

    // this._dashboardService.getUserAddress().pipe(
    //   map((value: any) => {
    //     this.addressModel = value;

    //     for (var i = 0; i < this.addressModel.length; i++) {
    //       if (this.addressModel[i].addressType.code == "L04") {
    //         this.addressIndex = i;
    //         break;
    //       } else {
    //         this.addressModel[i].addressType.code == "L01";
    //         this.addressIndex = i;
    //       }
    //     }
    //     return this.addressModel;
    //   })
    // ).subscribe((data: any) => {
    //   console.log("ADDRESS:", data);

    //   this.address = data[this.addressIndex].address1 ?? null;
    //   this.address2 = data[this.addressIndex].address2 ?? null;
    //   this.postcode = data[this.addressIndex].postcode ?? null;
    //   this.town = data[this.addressIndex].town ?? null;
    //   this.state = data[this.addressIndex].state?.name ?? null;
    //   this.label = data[this.addressIndex].label ?? null;
    // });
  }

  checkRedeem() {
    const onePieceOfGoldWeight = 4.250000; // weight of one piece of gold in grams
    const availableGoldWeight = this.availAmount; // available amount of gold weight in grams

    // check if the available gold weight is sufficient to redeem at least one piece of gold
    if (availableGoldWeight >= onePieceOfGoldWeight) {
      const numberOfGoldPieces = Math.floor(availableGoldWeight / onePieceOfGoldWeight); // calculate the number of gold pieces that can be redeemed

      // create an array of options for the user to select from
      const options = [];
      for (let i = 1; i <= numberOfGoldPieces; i++) {
        options.push(i);
      }

      // set the options for the user to select from
      this.goldRedeemOptions = options;
    } else {
      // set the options to an empty array if the user does not have enough gold to redeem
      this.goldRedeemOptions = [];
      this.alert.type = 'error';
      this.alert.message = 'You do not have enough gold to redeem.';
    }
  }

  backButton(): void {
    this.router.navigate(['/redeem-bgd']);
  }

  redeemSelect(): void {

    this.disableButton = true;

    if (this.selectedOption) { // check if the user has selected an option

      this._dashboardService.initRedeem(this.acctNumber, this.selectedOption, this.fullName, this.phoneNumber, this.address, this.address2, this.postcode, this.town, this.state)
        .subscribe((response: any) => {

          console.log("Response InitRedeem:", response);
          this.response = response;

          this.redeemService.acctNumber = response[0].acctNumber;
          this.redeemService.weight = response[0].weight;
          this.redeemService.remarks = response[0].remarks;
          this.redeemService.serialNumber = response[0].serialNumber;
          this.redeemService.reference = response[0].reference;
          this.redeemService.name = response[0].name;
          this.redeemService.phone = response[0].phone;

          this.redeemService.courierCharge = response[0].feesForms[0].chargeFees;
          this.redeemService.courierfeeTypeCode = response[0].feesForms[0].feeTypeCode;
          this.redeemService.couriersetupFees = response[0].feesForms[0].setupFees;
          this.redeemService.couriersetupUom = response[0].feesForms[0].setupUom;
          this.redeemService.couriersetupUomCode = response[0].feesForms[0].setupUomCode;
          this.redeemService.courierchargeFees = response[0].feesForms[0].chargeFees;
          this.redeemService.courierchargeUom = response[0].feesForms[0].chargeUom;
          this.redeemService.courierchargeUomCode = response[0].feesForms[0].chargeUomCode;

          this.redeemService.SSTCharge = response[0].feesForms[1].chargeFees;
          this.redeemService.SSTfeeTypeCode = response[0].feesForms[1].feeTypeCode;
          this.redeemService.SSTsetupFees = response[0].feesForms[1].setupFees;
          this.redeemService.SSTsetupUom = response[0].feesForms[1].setupUom;
          this.redeemService.SSTsetupUomCode = response[0].feesForms[1].setupUomCode;
          this.redeemService.SSTchargeFees = response[0].feesForms[1].chargeFees;
          this.redeemService.SSTchargeUom = response[0].feesForms[1].chargeUom;
          this.redeemService.SSTchargeUomCode = response[0].feesForms[1].chargeUomCode;

          this.redeemService.takafulCharge = response[0].feesForms[2].chargeFees;
          this.redeemService.takafulfeeTypeCode = response[0].feesForms[2].feeTypeCode;
          this.redeemService.takafulsetupFees = response[0].feesForms[2].setupFees;
          this.redeemService.takafulsetupUom = response[0].feesForms[2].setupUom;
          this.redeemService.takafulsetupUomCode = response[0].feesForms[2].setupUomCode;
          this.redeemService.takafulchargeFees = response[0].feesForms[2].chargeFees;
          this.redeemService.takafulchargeUom = response[0].feesForms[2].chargeUom;
          this.redeemService.takafulchargeUomCode = response[0].feesForms[2].chargeUomCode;

          this.redeemService.SST2Charge = response[0].feesForms[3].chargeFees;
          this.redeemService.SST2feeTypeCode = response[0].feesForms[3].feeTypeCode;
          this.redeemService.SST2setupFees = response[0].feesForms[3].setupFees;
          this.redeemService.SST2setupUom = response[0].feesForms[3].setupUom;
          this.redeemService.SST2setupUomCode = response[0].feesForms[3].setupUomCode;
          this.redeemService.SST2chargeFees = response[0].feesForms[3].chargeFees;
          this.redeemService.SST2chargeUom = response[0].feesForms[3].chargeUom;
          this.redeemService.SST2chargeUomCode = response[0].feesForms[3].chargeUomCode;

          this.redeemService.mintingCharge = response[0].feesForms[4].chargeFees;
          this.redeemService.mintingfeeTypeCode = response[0].feesForms[4].feeTypeCode;
          this.redeemService.mintingsetupFees = response[0].feesForms[4].setupFees;
          this.redeemService.mintingsetupUom = response[0].feesForms[4].setupUom;
          this.redeemService.mintingsetupUomCode = response[0].feesForms[4].setupUomCode;
          this.redeemService.mintingchargeFees = response[0].feesForms[4].chargeFees;
          this.redeemService.mintingchargeUom = response[0].feesForms[4].chargeUom;
          this.redeemService.mintingchargeUomCode = response[0].feesForms[4].chargeUomCode;

          this.redeemService.SST3Charge = response[0].feesForms[5].chargeFees;
          this.redeemService.SST3feeTypeCode = response[0].feesForms[5].feeTypeCode;
          this.redeemService.SST3setupFees = response[0].feesForms[5].setupFees;
          this.redeemService.SST3setupUom = response[0].feesForms[5].setupUom;
          this.redeemService.SST3setupUomCode = response[0].feesForms[5].setupUomCode;
          this.redeemService.SST3chargeFees = response[0].feesForms[5].chargeFees;
          this.redeemService.SST3chargeUom = response[0].feesForms[5].chargeUom;
          this.redeemService.SST3chargeUomCode = response[0].feesForms[5].chargeUomCode;

          this.redeemService.unit = response[0].unit;

          this.router.navigate(['/redeem-bgd-address']
          // ,
          //   {
          //     queryParams: {
          //       acctNumber: acctNumber,
          //       weight: weight,
          //       remarks: remarks,
          //       serialNumber: serialNumber,
          //       reference: reference,
          //       name: name,
          //       phone: phone,
          //       courierCharge: courierCharge,
          //       courierfeeTypeCode: courierfeeTypeCode,
          //       couriersetupFees: couriersetupFees,
          //       couriersetupUom: couriersetupUom,
          //       couriersetupUomCode: couriersetupUomCode,
          //       courierchargeFees: courierchargeFees,
          //       courierchargeUom: courierchargeUom,
          //       courierchargeUomCode: courierchargeUomCode,
          //       SSTCharge: SSTCharge,
          //       SSTfeeTypeCode: SSTfeeTypeCode,
          //       SSTsetupFees: SSTsetupFees,
          //       SSTsetupUom: SSTsetupUom,
          //       SSTsetupUomCode: SSTsetupUomCode,
          //       SSTchargeFees: SSTchargeFees,
          //       SSTchargeUom: SSTchargeUom,
          //       SSTchargeUomCode: SSTchargeUomCode,
          //       takafulCharge: takafulCharge,
          //       takafulfeeTypeCode: takafulfeeTypeCode,
          //       takafulsetupFees: takafulsetupFees,
          //       takafulsetupUom: takafulsetupUom,
          //       takafulsetupUomCode: takafulsetupUomCode,
          //       takafulchargeFees: takafulchargeFees,
          //       takafulchargeUom: takafulchargeUom,
          //       takafulchargeUomCode: takafulchargeUomCode,
          //       SST2Charge: SST2Charge,
          //       SST2feeTypeCode: SST2feeTypeCode,
          //       SST2setupFees: SST2setupFees,
          //       SST2setupUom: SST2setupUom,
          //       SST2setupUomCode: SST2setupUomCode,
          //       SST2chargeFees: SST2chargeFees,
          //       SST2chargeUom: SST2chargeUom,
          //       SST2chargeUomCode: SST2chargeUomCode,
          //       mintingCharge: mintingCharge,
          //       mintingfeeTypeCode: mintingfeeTypeCode,
          //       mintingsetupFees: mintingsetupFees,
          //       mintingsetupUom: mintingsetupUom,
          //       mintingsetupUomCode: mintingsetupUomCode,
          //       mintingchargeFees: mintingchargeFees,
          //       mintingchargeUom: mintingchargeUom,
          //       mintingchargeUomCode: mintingchargeUomCode,
          //       SST3Charge: SST3Charge,
          //       SST3feeTypeCode: SST3feeTypeCode,
          //       SST3setupFees: SST3setupFees,
          //       SST3setupUom: SST3setupUom,
          //       SST3setupUomCode: SST3setupUomCode,
          //       SST3chargeFees: SST3chargeFees,
          //       SST3chargeUom: SST3chargeUom,
          //       SST3chargeUomCode: SST3chargeUomCode,
          //       unit: unit
          //     }
          //   }
            );

        });
    } else {
      this.disableButton = false;
      // display an error message if the user hasn't selected an option
      this.alert.type = 'error';
      this.alert.message = 'Please select a number of gold pieces to redeem.';
    }
    this.disableButton = false;
  }
}
