import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RedeemBgdAmountComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-amount/redeem-bgd-amount.component';
import { RedeemBgdAmountRoutingModule } from 'app/modules/admin/bgd/redeem/redeem-bgd-amount/redeem-bgd-amount-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        RedeemBgdAmountComponent,

    ],
    imports: [
        RedeemBgdAmountRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule
    ]
})
export class RedeemBgdAmountModule {
}
