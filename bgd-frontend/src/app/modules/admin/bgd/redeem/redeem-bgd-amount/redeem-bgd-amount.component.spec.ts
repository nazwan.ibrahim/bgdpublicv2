import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdAmountComponent } from './redeem-bgd-amount.component';

describe('RedeemBgdAmountComponent', () => {
  let component: RedeemBgdAmountComponent;
  let fixture: ComponentFixture<RedeemBgdAmountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemBgdAmountComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
