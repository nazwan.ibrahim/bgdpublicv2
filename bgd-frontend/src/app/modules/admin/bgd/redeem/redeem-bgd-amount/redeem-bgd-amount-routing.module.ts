import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeemBgdAmountComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-amount/redeem-bgd-amount.component';

const routes: Routes = [
  {
    path: '',
    component: RedeemBgdAmountComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedeemBgdAmountRoutingModule { }
