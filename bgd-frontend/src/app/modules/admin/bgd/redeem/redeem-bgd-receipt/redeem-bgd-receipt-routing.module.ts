import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeemBgdReceiptComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-receipt/redeem-bgd-receipt.component';

const routes: Routes = [
  {
    path: '',
    component: RedeemBgdReceiptComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedeemBgdReceiptRoutingModule { }
