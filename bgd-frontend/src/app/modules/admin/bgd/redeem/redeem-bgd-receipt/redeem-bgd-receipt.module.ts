import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RedeemBgdReceiptComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-receipt/redeem-bgd-receipt.component';
import { RedeemBgdReceiptRoutingModule } from 'app/modules/admin/bgd/redeem/redeem-bgd-receipt/redeem-bgd-receipt-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';

@NgModule({
    declarations: [
        RedeemBgdReceiptComponent,

    ],
    imports: [
        RedeemBgdReceiptRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
    ]
})
export class RedeemBgdReceiptModule {
}
