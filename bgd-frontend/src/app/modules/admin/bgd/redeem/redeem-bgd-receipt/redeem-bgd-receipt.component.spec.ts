import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdReceiptComponent } from './redeem-bgd-receipt.component';

describe('RedeemBgdReceiptComponent', () => {
  let component: RedeemBgdReceiptComponent;
  let fixture: ComponentFixture<RedeemBgdReceiptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemBgdReceiptComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
