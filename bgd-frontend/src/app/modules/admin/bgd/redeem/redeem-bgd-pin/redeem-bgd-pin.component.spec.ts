import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdPinComponent } from './redeem-bgd-pin.component';

describe('RedeemBgdPinComponent', () => {
  let component: RedeemBgdPinComponent;
  let fixture: ComponentFixture<RedeemBgdPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemBgdPinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
