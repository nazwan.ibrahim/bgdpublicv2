import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RedeemService } from '../redeem.service';

@Component({
  selector: 'bgd-redeem-bgd-pin',
  templateUrl: './redeem-bgd-pin.component.html',
  styleUrls: ['./redeem-bgd-pin.component.scss'],
  animations: [
    trigger('shake', [
      // Animation configuration
    ])
  ]
})
export class RedeemBgdPinComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  username: any;
  email: any;
  pin: any;
  address: any;
  setupCourierCharge: any;
  setupTakafulCharge: any;
  setupMintingCharge: any;
  disableButton: boolean = false;
  selectedOption: string = '';
  availAmount: any;
  curAccNum: any;
  fullName: any;
  phoneNumber: any;
  address2: any;
  postcode: any;
  town: string;
  state: string;
  acctNumber: any;
  unit: any;
  weight: any;
  name: any;
  phone: any;
  address1: any;
  remarks: any;
  tempValue: any;
  feesForms: any;
  addressForm: any;
  coinRedeemed: any;
  redeemReason: any;
  totalAmount: any;
  response: any;
  reference: any;
  serialNumber: any;
  walletBalance: any;
  userID: any;
  today: Date = new Date();
  reason: any;
  courierCharge: any;
  courierfeeTypeCode: any;
  couriersetupFees: any;
  couriersetupUom: any;
  couriersetupUomCode: any;
  courierchargeFees: any;
  courierchargeUom: any;
  courierchargeUomCode: any;
  SSTCharge: any;
  SSTfeeTypeCode: any;
  SSTsetupFees: any;
  SSTsetupUom: any;
  SSTsetupUomCode: any;
  SSTchargeFees: any;
  SSTchargeUom: any;
  SSTchargeUomCode: any;
  takafulCharge: any;
  takafulfeeTypeCode: any;
  takafulsetupFees: any;
  takafulsetupUom: any;
  takafulsetupUomCode: any;
  takafulchargeFees: any;
  takafulchargeUom: any;
  takafulchargeUomCode: any;
  SST2Charge: any;
  SST2feeTypeCode: any;
  SST2setupFees: any;
  SST2setupUom: any;
  SST2setupUomCode: any;
  SST2chargeFees: any;
  SST2chargeUom: any;
  SST2chargeUomCode: any;
  mintingCharge: any;
  mintingfeeTypeCode: any;
  mintingsetupFees: any;
  mintingsetupUom: any;
  mintingsetupUomCode: any;
  mintingchargeFees: any;
  mintingchargeUom: any;
  mintingchargeUomCode: any;
  SST3Charge: any;
  SST3feeTypeCode: any;
  SST3setupFees: any;
  SST3setupUom: any;
  SST3setupUomCode: any;
  SST3chargeFees: any;
  SST3chargeUom: any;
  SST3chargeUomCode: any;
  totalAmountSST: any;
  showAlert = false;
  enoughBalance: boolean = false;
  totalFee: any;

  constructor(
    private DashboardService: DashboardService,
    private _bgdService: bgdService,
    private _activatedRoute: ActivatedRoute,
    private user: DashboardService,
    private _formBuilder: UntypedFormBuilder,
    private router: Router,
    private redeemService: RedeemService,
  ) {}

  ngOnInit(): void {
    this.reason = this.redeemService.reason;
    this.acctNumber = this.redeemService.acctNumber;
    this.fullName = this.redeemService.name;
    this.phone = this.redeemService.phone;
    this.courierCharge = parseFloat(this.redeemService.courierCharge);
    this.courierfeeTypeCode = this.redeemService.courierfeeTypeCode;
    this.couriersetupFees = this.redeemService.couriersetupFees;
    this.couriersetupUom = this.redeemService.couriersetupUom;
    this.couriersetupUom = this.redeemService.couriersetupUom;
    this.courierchargeFees = this.redeemService.courierchargeFees;
    this.courierchargeUom = this.redeemService.courierchargeUom;
    this.courierchargeUomCode = this.redeemService.courierchargeUomCode;
    this.SSTCharge = parseFloat(this.redeemService.SSTCharge);
    this.SSTfeeTypeCode = this.redeemService.SSTfeeTypeCode;
    this.SSTsetupFees = this.redeemService.SSTsetupFees;
    this.SSTsetupUom = this.redeemService.SSTsetupUom;
    this.SSTsetupUomCode = this.redeemService.SSTsetupUomCode;
    this.SSTchargeFees = this.redeemService.SSTchargeFees;
    this.SSTchargeUom = this.redeemService.SSTchargeUom;
    this.SSTchargeUomCode = this.redeemService.SSTchargeUomCode;
    this.takafulCharge = parseFloat(this.redeemService.takafulCharge);
    this.takafulfeeTypeCode = this.redeemService.takafulfeeTypeCode;
    this.takafulsetupFees = this.redeemService.takafulsetupFees;
    this.takafulsetupUom = this.redeemService.takafulsetupUom;
    this.takafulsetupUomCode = this.redeemService.takafulsetupUomCode;
    this.takafulchargeFees = this.redeemService.takafulchargeFees;
    this.takafulchargeUom = this.redeemService.takafulchargeUom;
    this.takafulchargeUomCode = this.redeemService.takafulchargeUomCode;
    this.SST2Charge = parseFloat(this.redeemService.SST2Charge);
    this.SST2feeTypeCode = this.redeemService.SST2feeTypeCode;
    this.SST2setupFees = this.redeemService.SST2setupFees;
    this.SST2setupUom = this.redeemService.SST2setupUom;
    this.SST2setupUomCode = this.redeemService.SST2setupUomCode;
    this.SST2chargeFees = this.redeemService.SST2chargeFees;
    this.SST2chargeUom = this.redeemService.SST2chargeUom;
    this.SST2chargeUomCode = this.redeemService.SST2chargeUomCode;
    this.mintingCharge = parseFloat(this.redeemService.mintingCharge);
    this.mintingfeeTypeCode = this.redeemService.mintingfeeTypeCode;
    this.mintingsetupFees = this.redeemService.mintingsetupFees;
    this.mintingsetupUom = this.redeemService.mintingsetupUom;
    this.mintingsetupUomCode = this.redeemService.mintingsetupUomCode;
    this.mintingchargeFees = this.redeemService.mintingchargeFees;
    this.mintingchargeUom = this.redeemService.mintingchargeUom;
    this.mintingchargeUomCode = this.redeemService.mintingchargeUomCode;
    this.SST3Charge = parseFloat(this.redeemService.SST3Charge);
    this.SST3feeTypeCode = this.redeemService.SST3feeTypeCode;
    this.SST3setupFees = this.redeemService.SST3setupFees;
    this.SST3setupUom = this.redeemService.SST3setupUom;
    this.SST3setupUomCode = this.redeemService.SST3setupUomCode;
    this.SST3chargeFees = this.redeemService.SST3chargeFees;
    this.SST3chargeUom = this.redeemService.SST3chargeUom;
    this.SST3chargeUomCode = this.redeemService.SST3chargeUomCode;
    this.unit = this.redeemService.unit;
    this.weight = this.redeemService.weight;
    this.remarks = this.redeemService.remarks;
    this.serialNumber = this.redeemService.serialNumber;
    this.reference = this.redeemService.reference;
    this.address1 = this.redeemService.address1;
    this.address2 = this.redeemService.address2;
    this.postcode = this.redeemService.postcode;
    this.town = this.redeemService.town;
    this.state = this.redeemService.state;

    console.log("Response", this.reason);

    this.totalAmountSST = this.SSTCharge + this.SST2Charge + this.SST3Charge;

    this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge + this.totalAmountSST;

    this.user.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    })

  }

  confirmRedeem() {
    this.disableButton = true;
    //Continue after PIN

    //let requestBody = JSON.parse(decodeURIComponent(this._activatedRoute.snapshot.queryParams['response']));
    const requestBody =
    [
      {
        "acctNumber": this.acctNumber,
        "unit": this.unit,
        "weight": this.weight,
        "name": this.fullName,
        "phone": this.phone,
        "address1": this.address1,
        "address2": this.address2,
        "postcode": this.postcode,
        "city": this.town,
        "state": this.state,
        "remarks": this.remarks,
        "reference": this.reference,
        "serialNumber": this.serialNumber,
        "reason": this.reason,
        "feesForms": [
          {
            "feeType": "Courier Fee",
            "feeTypeCode": this.courierfeeTypeCode,
            "setupFees": this.couriersetupFees,
            "setupUom": this.couriersetupUom,
            "setupUomCode": this.couriersetupUomCode,
            "chargeFees": this.courierchargeFees,
            "chargeUom": this.courierchargeUom,
            "chargeUomCode": this.courierchargeUomCode
          },
          {
            "feeType": "SST",
            "feeTypeCode": this.SSTfeeTypeCode,
            "setupFees": this.SSTsetupFees,
            "setupUom": this.SSTsetupUom,
            "setupUomCode": this.SSTsetupUomCode,
            "chargeFees": this.SSTchargeFees,
            "chargeUom": this.SSTchargeUom,
            "chargeUomCode": this.SSTchargeUomCode
          },
          {
            "feeType": "Takaful Fee",
            "feeTypeCode": this.takafulfeeTypeCode,
            "setupFees": this.takafulsetupFees,
            "setupUom": this.takafulsetupUom,
            "setupUomCode": this.takafulchargeUomCode,
            "chargeFees": this.takafulchargeFees,
            "chargeUom": this.takafulchargeUom,
            "chargeUomCode": this.takafulchargeUomCode
          },
          {
            "feeType": "SST",
            "feeTypeCode": this.SST2feeTypeCode,
            "setupFees": this.SST2setupFees,
            "setupUom": this.SST2setupUom,
            "setupUomCode": this.SST2setupUomCode,
            "chargeFees": this.SST2chargeFees,
            "chargeUom": this.SST2chargeUom,
            "chargeUomCode": this.SST2chargeUomCode
          },
          {
            "feeType": "Minting Fee",
            "feeTypeCode": this.mintingfeeTypeCode,
            "setupFees": this.mintingsetupFees,
            "setupUom": this.mintingsetupUom,
            "setupUomCode": this.mintingsetupUomCode,
            "chargeFees": this.mintingchargeFees,
            "chargeUom": this.mintingchargeUom,
            "chargeUomCode": this.mintingchargeUomCode
          },
          {
            "feeType": "SST",
            "feeTypeCode": this.SST3feeTypeCode,
            "setupFees": this.SST3setupFees,
            "setupUom": this.SST3setupUom,
            "setupUomCode": this.SST3setupUomCode,
            "chargeFees": this.SST3chargeFees,
            "chargeUom": this.SST3chargeUom,
            "chargeUomCode": this.SST3chargeUomCode
          }
        ]
      }
    ]

      // [
      //   {
      //     "acctNumber": this.acctNumber,
      //     "unit": this.unit,
      //     "weight": this.weight,
      //     "name": this.fullName,
      //     "phone": this.phoneNumber,
      //     "address": this.address,
      //     "address2": this.address2,
      //     "postcode": this.postcode,
      //     "city": this.town,
      //     "state": this.state,
      //     "remarks": this.remarks,
      //     "reference": this.reference,
      //     "serialNumber": this.serialNumber,
      //     "reason": this.reason,
      //     "feesForms": [
      //       {
      //         "feeType": "Courier Fee",
      //         "feeTypeCode": "B03",
      //         "setupFees": this.setupCourierCharge,
      //         "setupUom": "RM",
      //         "setupUomCode": null,
      //         "chargeFees": this.courierCharge,
      //         "chargeUom": "RM",
      //         "chargeUomCode": "J01"
      //       },
      //       {
      //         "feeType": "Takaful Fee",
      //         "feeTypeCode": "B04",
      //         "setupFees": this.setupTakafulCharge,
      //         "setupUom": "RM",
      //         "setupUomCode": null,
      //         "chargeFees": this.takafulCharge,
      //         "chargeUom": "RM",
      //         "chargeUomCode": "J01"
      //       },
      //       {
      //         "feeType": "Minting Fee",
      //         "feeTypeCode": "B05",
      //         "setupFees": this.setupMintingCharge,
      //         "setupUom": "RM",
      //         "setupUomCode": null,
      //         "chargeFees": this.mintingCharge,
      //         "chargeUom": "RM",
      //         "chargeUomCode": "J01"
      //       }
      //     ]
      //   }
      // ]


    console.log("Requestbody:", requestBody);

    let dataJson = JSON.stringify(requestBody);

    this._bgdService.redeemBGD(dataJson)
      .subscribe(
        Response => {

          this.alert = {
            type: 'success',
            message: 'Success'
          };


          const queryParams = {
            data: dataJson,
            Response: JSON.stringify(Response)
          };

          setTimeout(() => {
            this.router.navigate(['/redeem-bgd-status'], { queryParams });
          }, 500); // 3000 milliseconds = 3 seconds
        },
        error => {

          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };
        }
      );

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  backButton(): void {
    this.router.navigate(['/redeem-bgd-confirmation']);
  }

  verifyPin() {
    console.log(this.disableButton);
    this.disableButton = true;

    const pin = {
      userName: this.username,
      email: this.email,
      userPin: this.pin
    }

    console.log(pin);

    this._bgdService.verifyPin(pin)
      .subscribe(
        Response => {

          this.alert = {
            type: 'success',
            message: 'Verified'
          };

          this.confirmRedeem();

        },
        error => {
          this.disableButton = false;

          this.alert = {
            type: 'error',
            message: 'Incorrect PIN.'
          };
          this.showAlert = true;
        }
      );
    this.disableButton = false;
    this.showAlert = true;
  }

  forgotPin() {
    this.router.navigate(['/forgot-pin']);
  }
}
