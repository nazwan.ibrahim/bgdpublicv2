import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RedeemBgdPinComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-pin/redeem-bgd-pin.component';
import { RedeemBgdPinRoutingModule } from 'app/modules/admin/bgd/redeem/redeem-bgd-pin/redeem-bgd-pin-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { NgxPincodeModule } from 'ngx-pincode';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@bgd/components/alert';


@NgModule({
    declarations: [
        RedeemBgdPinComponent,

    ],
    imports: [
        RedeemBgdPinRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        NgxPincodeModule,
        MatIconModule,
        FuseAlertModule,
    ]
})
export class RedeemBgdPinModule {
}
