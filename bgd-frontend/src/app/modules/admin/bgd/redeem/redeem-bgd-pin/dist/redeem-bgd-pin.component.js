"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RedeemBgdPinComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@angular/animations");
var RedeemBgdPinComponent = /** @class */ (function () {
    function RedeemBgdPinComponent(DashboardService, _bgdService, _activatedRoute, user, _formBuilder, router) {
        this.DashboardService = DashboardService;
        this._bgdService = _bgdService;
        this._activatedRoute = _activatedRoute;
        this.user = user;
        this._formBuilder = _formBuilder;
        this.router = router;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.disableButton = false;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.reason = this._activatedRoute.snapshot.queryParams['reason'];
        console.log("Response   ", response);
        this.acctNumber = response[0].acctNumber;
        this.fullName = response[0].name;
        this.phoneNumber = response[0].phone;
        this.address = response[0].address;
        this.address2 = response[0].address2;
        this.postcode = response[0].postcode;
        this.town = response[0].city;
        this.state = response[0].state;
        this.remarks = response[0].remarks;
        this.reference = response[0].reference;
        this.serialNumber = response[0].serialNumber;
        this.unit = response[0].unit;
        this.weight = response[0].weight;
        this.courierCharge = response[0].feesForms[0].chargeFees;
        this.takafulCharge = response[0].feesForms[2].chargeFees;
        this.mintingCharge = response[0].feesForms[4].chargeFees;
        this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge;
        // this.unit = parseInt(decodeURIComponent(this._activatedRoute.snapshot.queryParams['goldPieces']).replace(/\%20/g, ''));
        // this.courierCharge = parseInt(decodeURIComponent(this._activatedRoute.snapshot.queryParams['courierCharge']));
        // this.takafulCharge = parseInt(decodeURIComponent(this._activatedRoute.snapshot.queryParams['takafulCharge']));
        // this.mintingCharge = parseInt(decodeURIComponent(this._activatedRoute.snapshot.queryParams['mintingCharge']));
        // this.redeemReason = decodeURIComponent(this._activatedRoute.snapshot.queryParams['redeemReason']).replace(/\%20/g, '');
        // this.address = decodeURIComponent(this._activatedRoute.snapshot.queryParams['userAddress'])
        //   .replace(/\%20/g, ' ')
        //   .replace(/\%2C/g, ' ');
        // this.acctNumber = decodeURIComponent(this._activatedRoute.snapshot.queryParams['acctNumber']).replace(/\%20/g, '');
        // this.fullName = decodeURIComponent(this._activatedRoute.snapshot.queryParams['fullName']).replace(/\%20/g, '');
        // this.phoneNumber = decodeURIComponent(this._activatedRoute.snapshot.queryParams['userPhone']).replace(/\%20/g, '').replace(/\%2B/g, '+');
        // this.remarks = decodeURIComponent(this._activatedRoute.snapshot.queryParams['remarks']).replace(/\%20/g, '');
        // this.address1 = decodeURIComponent(this._activatedRoute.snapshot.queryParams['address1']).replace(/\%20/g, '');
        // this.address2 = decodeURIComponent(this._activatedRoute.snapshot.queryParams['address2']).replace(/\%20/g, '');
        // this.postcode = decodeURIComponent(this._activatedRoute.snapshot.queryParams['postcode']).replace(/\%20/g, '');
        // this.town = decodeURIComponent(this._activatedRoute.snapshot.queryParams['town']).replace(/\%20/g, '');
        // this.state = decodeURIComponent(this._activatedRoute.snapshot.queryParams['state']).replace(/\%20/g, '');
    }
    RedeemBgdPinComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user.getUserDetails().subscribe(function (data) {
            _this.email = data.email;
            _this.username = data.userName;
        });
    };
    RedeemBgdPinComponent.prototype.confirmRedeem = function () {
        var _this = this;
        this.disableButton = true;
        //Continue after PIN
        //let requestBody = JSON.parse(decodeURIComponent(this._activatedRoute.snapshot.queryParams['response']));
        var requestBody = [
            {
                "acctNumber": this.acctNumber,
                "unit": this.unit,
                "weight": this.weight,
                "name": this.fullName,
                "phone": this.phoneNumber,
                "address": this.address,
                "address2": this.address2,
                "postcode": this.postcode,
                "city": this.town,
                "state": this.state,
                "remarks": this.remarks,
                "reference": this.reference,
                "serialNumber": this.serialNumber,
                "reason": this.reason,
                "feesForms": [
                    {
                        "feeType": "Courier Fee",
                        "feeTypeCode": "B03",
                        "setupFees": this.setupCourierCharge,
                        "setupUom": "RM",
                        "setupUomCode": null,
                        "chargeFees": this.courierCharge,
                        "chargeUom": "RM",
                        "chargeUomCode": "J01"
                    },
                    {
                        "feeType": "Takaful Fee",
                        "feeTypeCode": "B04",
                        "setupFees": this.setupTakafulCharge,
                        "setupUom": "RM",
                        "setupUomCode": null,
                        "chargeFees": this.takafulCharge,
                        "chargeUom": "RM",
                        "chargeUomCode": "J01"
                    },
                    {
                        "feeType": "Minting Fee",
                        "feeTypeCode": "B05",
                        "setupFees": this.setupMintingCharge,
                        "setupUom": "RM",
                        "setupUomCode": null,
                        "chargeFees": this.mintingCharge,
                        "chargeUom": "RM",
                        "chargeUomCode": "J01"
                    }
                ]
            }
        ];
        console.log(requestBody);
        var dataJson = JSON.stringify(requestBody);
        this._bgdService.redeemBGD(dataJson)
            .subscribe(function (Response) {
            _this.alert = {
                type: 'success',
                message: 'Success'
            };
            var queryParams = {
                data: dataJson,
                Response: JSON.stringify(Response)
            };
            setTimeout(function () {
                _this.router.navigate(['/redeem-bgd-status'], { queryParams: queryParams });
            }, 500); // 3000 milliseconds = 3 seconds
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    RedeemBgdPinComponent.prototype.pincodeCompleted = function (pin) {
        this.pin = pin;
    };
    RedeemBgdPinComponent.prototype.backButton = function () {
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.router.navigate(['/redeem-bgd-confirmation'], {
            queryParams: {
                response: JSON.stringify(response),
                reason: JSON.stringify(this.reason)
            }
        });
    };
    RedeemBgdPinComponent.prototype.verifyPin = function () {
        var _this = this;
        console.log(this.disableButton);
        this.disableButton = true;
        var pin = {
            userName: this.username,
            email: this.email,
            userPin: this.pin
        };
        console.log(pin);
        this._bgdService.verifyPin(pin)
            .subscribe(function (Response) {
            _this.alert = {
                type: 'success',
                message: 'Verified'
            };
            _this.confirmRedeem();
        }, function (error) {
            _this.disableButton = false;
            _this.alert = {
                type: 'error',
                message: 'Incorrect PIN.'
            };
            _this.showAlert = true;
        });
        this.disableButton = false;
        this.showAlert = true;
    };
    RedeemBgdPinComponent.prototype.forgotPin = function () {
        this.router.navigate(['/forgot-pin']);
    };
    RedeemBgdPinComponent = __decorate([
        core_1.Component({
            selector: 'bgd-redeem-bgd-pin',
            templateUrl: './redeem-bgd-pin.component.html',
            styleUrls: ['./redeem-bgd-pin.component.scss'],
            animations: [
                animations_1.trigger('shake', [
                // Animation configuration
                ])
            ]
        })
    ], RedeemBgdPinComponent);
    return RedeemBgdPinComponent;
}());
exports.RedeemBgdPinComponent = RedeemBgdPinComponent;
