import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeemBgdPinComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-pin/redeem-bgd-pin.component';

const routes: Routes = [
  {
    path: '',
    component: RedeemBgdPinComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedeemBgdPinRoutingModule { }
