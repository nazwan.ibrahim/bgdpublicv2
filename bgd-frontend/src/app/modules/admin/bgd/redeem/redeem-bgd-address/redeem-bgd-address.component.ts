import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { catchError, map, tap } from 'rxjs/operators';
import { min } from 'lodash';
import { redeemBgdService } from '../redeem-bgd/redeem-bgd.service';
import { of } from 'rxjs';
import { trigger } from '@angular/animations';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { RedeemService } from '../redeem.service';

@Component({
  selector: 'bgd-redeem-bgd-address',
  templateUrl: './redeem-bgd-address.component.html',
  styleUrls: ['./redeem-bgd-address.component.scss'],
  animations: [
    trigger('shake', [])
  ]
})
export class RedeemBgdAddressComponent {
  @ViewChild('bgdRedeemNgForm') bgdRedeemNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  showAlert = false;


  bgdRedeemForm: UntypedFormGroup;
  walletDetails = [];
  walletModel: any;
  cashWalletIndex: number = -1;
  goldWalletIndex: number;
  selectedOption: string = '';
  addressModel: any;
  addressIndex: number = -1;
  availAmount: any;
  fullName: any;
  phoneNumber: any;
  address1: any;
  address2: any;
  postcode: any;
  town: string;
  state: any;
  acctNumber: any;
  unit: any;
  name: any;
  phone: any;
  address: any;
  remarks: any;
  tempValue: any;
  feesForms: any;
  addressForm: any;
  coinRedeemed: any;
  redeemReason: any;
  totalAmount: any;
  response: any;
  id: any;
  label: any;
  dropdownData = [];
  reason: any;
  weight: any;
  serialNumber: any;
  reference: any;
  note: any;
  courierCharge: any;
  courierfeeTypeCode: any;
  couriersetupFees: any;
  couriersetupUom: any;
  couriersetupUomCode: any;
  courierchargeFees: any;
  courierchargeUom: any;
  courierchargeUomCode: any;
  SSTCharge: any;
  SSTfeeTypeCode: any;
  SSTsetupFees: any;
  SSTsetupUom: any;
  SSTsetupUomCode: any;
  SSTchargeFees: any;
  SSTchargeUom: any;
  SSTchargeUomCode: any;
  takafulCharge: any;
  takafulfeeTypeCode: any;
  takafulsetupFees: any;
  takafulsetupUom: any;
  takafulsetupUomCode: any;
  takafulchargeFees: any;
  takafulchargeUom: any;
  takafulchargeUomCode: any;
  SST2Charge: any;
  SST2feeTypeCode: any;
  SST2setupFees: any;
  SST2setupUom: any;
  SST2setupUomCode: any;
  SST2chargeFees: any;
  SST2chargeUom: any;
  SST2chargeUomCode: any;
  mintingCharge: any;
  mintingfeeTypeCode: any;
  mintingsetupFees: any;
  mintingsetupUom: any;
  mintingsetupUomCode: any;
  mintingchargeFees: any;
  mintingchargeUom: any;
  mintingchargeUomCode: any;
  SST3Charge: any;
  SST3feeTypeCode: any;
  SST3setupFees: any;
  SST3setupUom: any;
  SST3setupUomCode: any;
  SST3chargeFees: any;
  SST3chargeUom: any;
  SST3chargeUomCode: any;
  totalAmountSST: any;
  disableButton: boolean = false;

  constructor(
    private router: Router,
    private walletDetailsService: CashWalletService,
    private _DashboardService: DashboardService,
    private _activatedRoute: ActivatedRoute,
    private formBuilder: UntypedFormBuilder,
    private _redeemBgdService: redeemBgdService,
    private redeemService: RedeemService,
  ) {

  }

  ngOnInit(): void {
    // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    this.acctNumber = this.redeemService.acctNumber;
    this.fullName = this.redeemService.name;
    this.phone = this.redeemService.phone;
    this.courierCharge = parseFloat(this.redeemService.courierCharge);
    this.courierfeeTypeCode = this.redeemService.courierfeeTypeCode;
    this.couriersetupFees = this.redeemService.couriersetupFees;
    this.couriersetupUom = this.redeemService.couriersetupUom;
    this.couriersetupUom = this.redeemService.couriersetupUom;
    this.courierchargeFees = this.redeemService.courierchargeFees;
    this.courierchargeUom = this.redeemService.courierchargeUom;
    this.courierchargeUomCode = this.redeemService.courierchargeUomCode;
    this.SSTCharge = parseFloat(this.redeemService.SSTCharge);
    this.SSTfeeTypeCode = this.redeemService.SSTfeeTypeCode;
    this.SSTsetupFees = this.redeemService.SSTsetupFees;
    this.SSTsetupUom = this.redeemService.SSTsetupUom;
    this.SSTsetupUomCode = this.redeemService.SSTsetupUomCode;
    this.SSTchargeFees = this.redeemService.SSTchargeFees;
    this.SSTchargeUom = this.redeemService.SSTchargeUom;
    this.SSTchargeUomCode = this.redeemService.SSTchargeUomCode;
    this.takafulCharge = parseFloat(this.redeemService.takafulCharge);
    this.takafulfeeTypeCode = this.redeemService.takafulfeeTypeCode;
    this.takafulsetupFees = this.redeemService.takafulsetupFees;
    this.takafulsetupUom = this.redeemService.takafulsetupUom;
    this.takafulsetupUomCode = this.redeemService.takafulsetupUomCode;
    this.takafulchargeFees = this.redeemService.takafulchargeFees;
    this.takafulchargeUom = this.redeemService.takafulchargeUom;
    this.takafulchargeUomCode = this.redeemService.takafulchargeUomCode;
    this.SST2Charge = parseFloat(this.redeemService.SST2Charge);
    this.SST2feeTypeCode = this.redeemService.SST2feeTypeCode;
    this.SST2setupFees = this.redeemService.SST2setupFees;
    this.SST2setupUom = this.redeemService.SST2setupUom;
    this.SST2setupUomCode = this.redeemService.SST2setupUomCode;
    this.SST2chargeFees = this.redeemService.SST2chargeFees;
    this.SST2chargeUom = this.redeemService.SST2chargeUom;
    this.SST2chargeUomCode = this.redeemService.SST2chargeUomCode;
    this.mintingCharge = parseFloat(this.redeemService.mintingCharge);
    this.mintingfeeTypeCode = this.redeemService.mintingfeeTypeCode;
    this.mintingsetupFees = this.redeemService.mintingsetupFees;
    this.mintingsetupUom = this.redeemService.mintingsetupUom;
    this.mintingsetupUomCode = this.redeemService.mintingsetupUomCode;
    this.mintingchargeFees = this.redeemService.mintingchargeFees;
    this.mintingchargeUom = this.redeemService.mintingchargeUom;
    this.mintingchargeUomCode = this.redeemService.mintingchargeUomCode;
    this.SST3Charge = parseFloat(this.redeemService.SST3Charge);
    this.SST3feeTypeCode = this.redeemService.SST3feeTypeCode;
    this.SST3setupFees = this.redeemService.SST3setupFees;
    this.SST3setupUom = this.redeemService.SST3setupUom;
    this.SST3setupUomCode = this.redeemService.SST3setupUomCode;
    this.SST3chargeFees = this.redeemService.SST3chargeFees;
    this.SST3chargeUom = this.redeemService.SST3chargeUom;
    this.SST3chargeUomCode = this.redeemService.SST3chargeUomCode;
    this.unit = this.redeemService.unit;
    this.weight = this.redeemService.weight;
    this.remarks = this.redeemService.remarks;
    this.serialNumber = this.redeemService.serialNumber;
    this.reference = this.redeemService.reference;
    

    this.totalAmountSST = this.SSTCharge + this.SST2Charge + this.SST3Charge;

    this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge + this.totalAmountSST;


    console.log("Total Courier ", this.courierCharge);
    console.log("Total Takafu; ", this.takafulCharge);
    console.log("Total Minting ", this.mintingCharge);
    console.log("Total SST ", this.totalAmountSST);

    console.log("Total Pay ", this.totalAmount);

    // if (response[0].name == null || response[0].name == null || response[0].name == '' || !response[0].name) {
    //   this.fullName = "Enter your phone name";
    // } else {
    //   this.fullName = response[0].name;
    // }
    // if (response[0].phone == null || response[0].phone == '' || !response[0].phone) {
    //   this.phoneNumber = "Enter your phone number";
    // } else {
    //   this.phoneNumber = response[0].phone;
    // }
    // if (response[0].address == "undefined" ||response[0].address == null || response[0].address == '' || !response[0].address) {
    //   this.address1 = "Enter your delivery address";
    // } else {
    //   this.address1 = response[0].address;
    // }
    // if (response[0].address2 == "undefined" ||response[0].address2 == null || response[0].address2 == '' || !response[0].address2) {
    //   this.address2 = "Enter your delivery address";
    // } else {
    //   this.address2 = response[0].address2;
    // }
    // if (response[0].postcode == "undefined" || response[0].postcode == null || response[0].postcode == '' || !response[0].postcode) {
    //   this.postcode = "Enter your postcode";
    // } else {
    //   this.postcode = response[0].postcode;
    // }
    // if (response[0].city == null || response[0].city == '' || !response[0].city) {
    //   this.town = "Enter your city";
    // } else {
    //   this.town = response[0].city;
    // }
    // if (response[0].state == null || response[0].state == '' || !response[0].state) {
    //   this.state = "Enter your state";
    // } else {
    //   this.state = response[0].state;
    // }
    // if (response[0].label == null || response[0].label == '' || !response[0].label) {
    //   this.label = "Note to the driver";
    // } else {
    //   this.label = response[0].label;

    // }
    /* this.walletDetailsService.getWalletDetails().pipe(
      map((value: any) => {
        this.walletModel = value;
        console.log("WalletModel", this.walletModel);

        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Cash") {
            this.cashWalletIndex = i;
            break;
          }
        }

        return this.walletModel; // return the modified walletModel
      }),
      catchError((error: any) => {
        if (error.status === 404) {
          // Handle 404 error here, such as setting default values
          this.walletModel = []; // Set an empty array or any default value
          return throwError("No wallet data.");
        }
        // Handle other errors if needed
        return throwError("An error occurred while fetching wallet data");
      })
    ).subscribe((data: any) => {
      // access the data returned by the map operator

      this.availAmount = data[this.cashWalletIndex].availableAmount.toFixed(2);
      console.log(this.availAmount);

    }); */

    // this._DashboardService.getUserDetails().subscribe(data => {
    //   this.fullName = data.fullName;
    //   this.phoneNumber = data.phoneNumber;
    // })

    this.bgdRedeemForm = this.formBuilder.group({
      redeemReason: [this.redeemService.reason || '', Validators.required]
  });

    this._DashboardService.getRedeemMotive().subscribe(data => {
      console.log("motive : ", data);
      this.dropdownData = data;
    });

    if (this.redeemService.reason) {
      this.bgdRedeemForm.patchValue({
          redeemReason: this.redeemService.reason
      });
  }

  this.reason = this.bgdRedeemForm.get('redeemReason').value;

    // this._DashboardService.initRedeem(this.acctNumber, this.unit, this.fullName, this.phoneNumber, this.address, this.address2, this.postcode, this.town, this.state, this.remarks)
    //   .subscribe((response: any) => {

    //     console.log(response);
    //     this.feesForms = response[0].feesForms;
    //     this.courierCharge = this.feesForms[0].chargeFees;
    //     this.takafulCharge = this.feesForms[1].chargeFees;
    //     this.mintingCharge = this.feesForms[2].chargeFees;

    //     console.log(this.courierCharge);
    //     console.log(this.takafulCharge);
    //     console.log(this.mintingCharge);

    //     this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge;
    // });

    this._DashboardService.getUserAddress().pipe(
      map((value: any) => {
        this.addressModel = value;

        for (var i = 0; i < this.addressModel.length; i++) {
          if (this.addressModel[i].addressType.code == "L04") {
            this.addressIndex = i;
            break;
          } else if (this.addressModel[i].addressType.code == "L01") {
            this.addressIndex = i;
          } else {
            this.addressIndex = -1;
          }
        }

        return this.addressModel;
      })
    ).subscribe((data: any) => {
      console.log("ADDRESS ", data);

      if (this.addressIndex === -1 || !data[this.addressIndex]) {
        this.id = null;
        this.label = null;
        this.address1 = null;
        this.address2 = null;
        this.postcode = null;
        this.town = null;
        this.state = null;

      } else {
        this.id = data[this.addressIndex].id;
        this.label = data[this.addressIndex].label;
        this.address1 = data[this.addressIndex].address1;
        this.address2 = data[this.addressIndex].address2;
        this.postcode = data[this.addressIndex].postcode;
        this.town = data[this.addressIndex].town;
        this.state = data[this.addressIndex].state.name;
      }


      // if (this.phone == null || this.phone == '' || !this.phone) {
      //   this.phoneNumber = "Enter your phone number"
      // } else {
      //   this.phoneNumber = this.phone;
      // }

      // if (this.address1 == null || this.address1 == '' || !this.address1) {
      //   this.address1 = "Enter your address line 1"
      // } else {
      //   this.address1 = this.address1
      // }

      // if (this.address2 == null || this.address2 == '' || !this.address2) {
      //   this.address2 = "Enter your address line 2"
      // } else {
      //   this.address2 = this.address2;
      // }

      // if (this.postcode == null || this.postcode == '' || !this.postcode) {
      //   this.postcode = "Enter your postcode"
      // } else {
      //   this.postcode = this.postcode;
      // }

      // if (this.town == null || this.town == '' || !this.town) {
      //   this.town = "Enter your city"
      // } else {
      //   this.town = this.town;
      // }

      // if (this.state == null || this.state == '' || !this.state) {
      //   this.state = "Enter your state"
      // } else {
      //   this.state = this.state;
      // }

      // if (this.label == null || this.label == '' || !this.label) {
      //   this.label = "Note to the driver"
      // } else {
      //   this.label = this.label;
      // }

    });
  }


  editDetail(): void {

    this.router.navigate(['/redeem-bgd-delivery-address']
      );
  }

  backButton(): void {
    this.router.navigate(['/redeem-bgd-amount']);
  }

  redeemContinue() {

    this.disableButton = true;

    this.reason = this.bgdRedeemForm.get('redeemReason').value;
    // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    console.log(this.reason);

    if ((this.address == null || this.address == '') && (this.address2 == null || this.address2 == '') && (this.postcode == null || this.postcode == '') && (this.address == null || this.address == '') && (this.postcode == null || this.postcode == '') && (this.town == null || this.town == '') && (this.state == null || this.state == '')) {
      this.disableButton = false;
      this.alert = {
        type: 'error',
        message: 'Please enter your delivery address'
      };
      this.showAlert = true;

    } else {

      this.disableButton = false;

      this.redeemService.reason = this.reason;
      this.redeemService.acctNumber = this.acctNumber;
      this.redeemService.weight = this.weight;
      this.redeemService.remarks = this.remarks;
      this.redeemService.serialNumber = this.serialNumber;
      this.redeemService.reference = this.reference;
      this.redeemService.name = this.fullName;
      this.redeemService.phone = this.phone;
      this.redeemService.courierCharge = this.courierCharge;
      this.redeemService.courierfeeTypeCode = this.courierfeeTypeCode;
      this.redeemService.couriersetupFees = this.couriersetupFees;
      this.redeemService.couriersetupUom = this.couriersetupUom;
      this.redeemService.couriersetupUomCode = this.couriersetupUomCode;
      this.redeemService.courierchargeFees = this.courierchargeFees;
      this.redeemService.courierchargeUom = this.courierchargeUom;
      this.redeemService.courierchargeUomCode = this.courierchargeUomCode;
      this.redeemService.SSTCharge = this.SSTCharge;
      this.redeemService.SSTfeeTypeCode = this.SSTfeeTypeCode;
      this.redeemService.SSTsetupFees = this.SSTsetupFees;
      this.redeemService.SSTsetupUom = this.SSTsetupUom;
      this.redeemService.SSTsetupUomCode = this.SSTsetupUomCode;
      this.redeemService.SSTchargeFees = this.SSTchargeFees;
      this.redeemService.SSTchargeUom = this.SSTchargeUom;
      this.redeemService.SSTchargeUomCode = this.SSTchargeUomCode;
      this.redeemService.takafulCharge = this.takafulCharge;
      this.redeemService.takafulfeeTypeCode = this.takafulfeeTypeCode;
      this.redeemService.takafulsetupFees = this.takafulsetupFees;
      this.redeemService.takafulsetupUom = this.takafulsetupUom;
      this.redeemService.takafulsetupUomCode = this.takafulsetupUomCode;
      this.redeemService.takafulchargeFees = this.takafulchargeFees;
      this.redeemService.takafulchargeUom = this.takafulchargeUom;
      this.redeemService.takafulchargeUomCode = this.takafulchargeUomCode;
      this.redeemService.SST2Charge = this.SST2Charge;
      this.redeemService.SST2feeTypeCode = this.SST2feeTypeCode;
      this.redeemService.SST2setupFees = this.SST2setupFees;
      this.redeemService.SST2setupUom = this.SST2setupUom;
      this.redeemService.SST2setupUomCode = this.SST2setupUomCode;
      this.redeemService.SST2chargeFees = this.SST2chargeFees;
      this.redeemService.SST2chargeUom = this.SST2chargeUom;
      this.redeemService.SST2chargeUomCode = this.SST2chargeUomCode;
      this.redeemService.mintingCharge = this.mintingCharge;
      this.redeemService.mintingfeeTypeCode = this.mintingfeeTypeCode;
      this.redeemService.mintingsetupFees = this.mintingsetupFees;
      this.redeemService.mintingsetupUom = this.mintingsetupUom;
      this.redeemService.mintingsetupUomCode = this.mintingsetupUomCode;
      this.redeemService.mintingchargeFees = this.mintingchargeFees;
      this.redeemService.mintingchargeUom = this.mintingchargeUom;
      this.redeemService.mintingchargeUomCode = this.mintingchargeUomCode;
      this.redeemService.SST3Charge = this.SST3Charge;
      this.redeemService.SST3feeTypeCode = this.SST3feeTypeCode;
      this.redeemService.SST3setupFees = this.SST3setupFees;
      this.redeemService.SST3setupUom = this.SST3setupUom;
      this.redeemService.SST3setupUomCode = this.SST3setupUomCode;
      this.redeemService.SST3chargeFees = this.SST3chargeFees;
      this.redeemService.SST3chargeUom = this.SST3chargeUom;
      this.redeemService.SST3chargeUomCode = this.SST3chargeUomCode;
      this.redeemService.unit = this.unit;
      this.redeemService.address1 = this.address1,
      this.redeemService.address2 = this.address2,
      this.redeemService.postcode = this.postcode,
      this.redeemService.town = this.town,
      this.redeemService.state = this.state,

      this.router.navigate(['/redeem-bgd-confirmation'])
 
    }
    this.disableButton = false;
  }
}
