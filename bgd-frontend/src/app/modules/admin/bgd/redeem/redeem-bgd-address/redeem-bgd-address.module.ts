import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RedeemBgdAddressComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-address/redeem-bgd-address.component';
import { RedeemBgdAddressRoutingModule } from 'app/modules/admin/bgd/redeem/redeem-bgd-address/redeem-bgd-address-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@bgd/components/alert';
import { FuseCardModule } from '@bgd/components/card';

@NgModule({
    declarations: [
        RedeemBgdAddressComponent,

    ],
    imports: [
        RedeemBgdAddressRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        FuseAlertModule,
        FuseCardModule,
    ]
})
export class RedeemBgdAddressModule {
}
