import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdAddressComponent } from './redeem-bgd-address.component';

describe('RedeemBgdAddressComponent', () => {
  let component: RedeemBgdAddressComponent;
  let fixture: ComponentFixture<RedeemBgdAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemBgdAddressComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
