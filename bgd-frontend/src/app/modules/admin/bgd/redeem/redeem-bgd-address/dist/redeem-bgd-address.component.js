"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RedeemBgdAddressComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var operators_1 = require("rxjs/operators");
var animations_1 = require("@angular/animations");
var RedeemBgdAddressComponent = /** @class */ (function () {
    function RedeemBgdAddressComponent(router, walletDetailsService, _DashboardService, _activatedRoute, formBuilder, _redeemBgdService) {
        this.router = router;
        this.walletDetailsService = walletDetailsService;
        this._DashboardService = _DashboardService;
        this._activatedRoute = _activatedRoute;
        this.formBuilder = formBuilder;
        this._redeemBgdService = _redeemBgdService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.walletDetails = [];
        this.cashWalletIndex = -1;
        this.selectedOption = '';
        this.addressIndex = -1;
        this.dropdownData = [];
        this.disableButton = false;
    }
    RedeemBgdAddressComponent.prototype.ngOnInit = function () {
        // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        var _this = this;
        this.acctNumber = this._activatedRoute.snapshot.queryParams['acctNumber'];
        this.fullName = this._activatedRoute.snapshot.queryParams['name'];
        this.phone = this._activatedRoute.snapshot.queryParams['phone'];
        this.courierCharge = parseFloat(this._activatedRoute.snapshot.queryParams['courierCharge']);
        this.courierfeeTypeCode = this._activatedRoute.snapshot.queryParams['courierfeeTypeCode'];
        this.couriersetupFees = this._activatedRoute.snapshot.queryParams['couriersetupFees'];
        this.couriersetupUom = this._activatedRoute.snapshot.queryParams['couriersetupUom'];
        this.couriersetupUom = this._activatedRoute.snapshot.queryParams['couriersetupUom'];
        this.courierchargeFees = this._activatedRoute.snapshot.queryParams['courierchargeFees'];
        this.courierchargeUom = this._activatedRoute.snapshot.queryParams['courierchargeUom'];
        this.courierchargeUomCode = this._activatedRoute.snapshot.queryParams['courierchargeUomCode'];
        this.SSTCharge = parseFloat(this._activatedRoute.snapshot.queryParams['SSTCharge']);
        this.SSTfeeTypeCode = this._activatedRoute.snapshot.queryParams['SSTfeeTypeCode'];
        this.SSTsetupFees = this._activatedRoute.snapshot.queryParams['SSTsetupFees'];
        this.SSTsetupUom = this._activatedRoute.snapshot.queryParams['SSTsetupUom'];
        this.SSTsetupUomCode = this._activatedRoute.snapshot.queryParams['SSTsetupUomCode'];
        this.SSTchargeFees = this._activatedRoute.snapshot.queryParams['SSTchargeFees'];
        this.SSTchargeUom = this._activatedRoute.snapshot.queryParams['SSTchargeUom'];
        this.SSTchargeUomCode = this._activatedRoute.snapshot.queryParams['SSTchargeUomCode'];
        this.takafulCharge = parseFloat(this._activatedRoute.snapshot.queryParams['takafulCharge']);
        this.takafulfeeTypeCode = this._activatedRoute.snapshot.queryParams['takafulfeeTypeCode'];
        this.takafulsetupFees = this._activatedRoute.snapshot.queryParams['takafulsetupFees'];
        this.takafulsetupUom = this._activatedRoute.snapshot.queryParams['takafulsetupUom'];
        this.takafulsetupUomCode = this._activatedRoute.snapshot.queryParams['takafulsetupUomCode'];
        this.takafulchargeFees = this._activatedRoute.snapshot.queryParams['takafulchargeFees'];
        this.takafulchargeUom = this._activatedRoute.snapshot.queryParams['takafulchargeUom'];
        this.takafulchargeUomCode = this._activatedRoute.snapshot.queryParams['takafulchargeUomCode'];
        this.SST2Charge = parseFloat(this._activatedRoute.snapshot.queryParams['SST2Charge']);
        this.SST2feeTypeCode = this._activatedRoute.snapshot.queryParams['SST2feeTypeCode'];
        this.SST2setupFees = this._activatedRoute.snapshot.queryParams['SST2setupFees'];
        this.SST2setupUom = this._activatedRoute.snapshot.queryParams['SST2setupUom'];
        this.SST2setupUomCode = this._activatedRoute.snapshot.queryParams['SST2setupUomCode'];
        this.SST2chargeFees = this._activatedRoute.snapshot.queryParams['SST2chargeFees'];
        this.SST2chargeUom = this._activatedRoute.snapshot.queryParams['SST2chargeUom'];
        this.SST2chargeUomCode = this._activatedRoute.snapshot.queryParams['SST2chargeUomCode'];
        this.mintingCharge = parseFloat(this._activatedRoute.snapshot.queryParams['mintingCharge']);
        this.mintingfeeTypeCode = this._activatedRoute.snapshot.queryParams['mintingfeeTypeCode'];
        this.mintingsetupFees = this._activatedRoute.snapshot.queryParams['mintingsetupFees'];
        this.mintingsetupUom = this._activatedRoute.snapshot.queryParams['mintingsetupUom'];
        this.mintingsetupUomCode = this._activatedRoute.snapshot.queryParams['mintingsetupUomCode'];
        this.mintingchargeFees = this._activatedRoute.snapshot.queryParams['mintingchargeFees'];
        this.mintingchargeUom = this._activatedRoute.snapshot.queryParams['mintingchargeUom'];
        this.mintingchargeUomCode = this._activatedRoute.snapshot.queryParams['mintingchargeUomCode'];
        this.SST3Charge = parseFloat(this._activatedRoute.snapshot.queryParams['SST3Charge']);
        this.SST3feeTypeCode = this._activatedRoute.snapshot.queryParams['SST3feeTypeCode'];
        this.SST3setupFees = this._activatedRoute.snapshot.queryParams['SST3setupFees'];
        this.SST3setupUom = this._activatedRoute.snapshot.queryParams['SST3setupUom'];
        this.SST3setupUomCode = this._activatedRoute.snapshot.queryParams['SST3setupUomCode'];
        this.SST3chargeFees = this._activatedRoute.snapshot.queryParams['SST3chargeFees'];
        this.SST3chargeUom = this._activatedRoute.snapshot.queryParams['SST3chargeUom'];
        this.SST3chargeUomCode = this._activatedRoute.snapshot.queryParams['SST3chargeUomCode'];
        this.unit = this._activatedRoute.snapshot.queryParams['unit'];
        this.weight = this._activatedRoute.snapshot.queryParams['weight'];
        this.remarks = this._activatedRoute.snapshot.queryParams['remarks'];
        this.serialNumber = this._activatedRoute.snapshot.queryParams['serialNumber'];
        this.reference = this._activatedRoute.snapshot.queryParams['reference'];
        this.totalAmountSST = this.SSTCharge + this.SST2Charge + this.SST3Charge;
        this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge + this.totalAmountSST;
        console.log("Total Courier ", this.courierCharge);
        console.log("Total Takafu; ", this.takafulCharge);
        console.log("Total Minting ", this.mintingCharge);
        console.log("Total SST ", this.totalAmountSST);
        console.log("Total Pay ", this.totalAmount);
        // if (response[0].name == null || response[0].name == null || response[0].name == '' || !response[0].name) {
        //   this.fullName = "Enter your phone name";
        // } else {
        //   this.fullName = response[0].name;
        // }
        // if (response[0].phone == null || response[0].phone == '' || !response[0].phone) {
        //   this.phoneNumber = "Enter your phone number";
        // } else {
        //   this.phoneNumber = response[0].phone;
        // }
        // if (response[0].address == "undefined" ||response[0].address == null || response[0].address == '' || !response[0].address) {
        //   this.address1 = "Enter your delivery address";
        // } else {
        //   this.address1 = response[0].address;
        // }
        // if (response[0].address2 == "undefined" ||response[0].address2 == null || response[0].address2 == '' || !response[0].address2) {
        //   this.address2 = "Enter your delivery address";
        // } else {
        //   this.address2 = response[0].address2;
        // }
        // if (response[0].postcode == "undefined" || response[0].postcode == null || response[0].postcode == '' || !response[0].postcode) {
        //   this.postcode = "Enter your postcode";
        // } else {
        //   this.postcode = response[0].postcode;
        // }
        // if (response[0].city == null || response[0].city == '' || !response[0].city) {
        //   this.town = "Enter your city";
        // } else {
        //   this.town = response[0].city;
        // }
        // if (response[0].state == null || response[0].state == '' || !response[0].state) {
        //   this.state = "Enter your state";
        // } else {
        //   this.state = response[0].state;
        // }
        // if (response[0].label == null || response[0].label == '' || !response[0].label) {
        //   this.label = "Note to the driver";
        // } else {
        //   this.label = response[0].label;
        // }
        /* this.walletDetailsService.getWalletDetails().pipe(
          map((value: any) => {
            this.walletModel = value;
            console.log("WalletModel", this.walletModel);
    
            for (var i = 0; i < this.walletModel.length; i++) {
              if (this.walletModel[i].type == "Cash") {
                this.cashWalletIndex = i;
                break;
              }
            }
    
            return this.walletModel; // return the modified walletModel
          }),
          catchError((error: any) => {
            if (error.status === 404) {
              // Handle 404 error here, such as setting default values
              this.walletModel = []; // Set an empty array or any default value
              return throwError("No wallet data.");
            }
            // Handle other errors if needed
            return throwError("An error occurred while fetching wallet data");
          })
        ).subscribe((data: any) => {
          // access the data returned by the map operator
    
          this.availAmount = data[this.cashWalletIndex].availableAmount.toFixed(2);
          console.log(this.availAmount);
    
        }); */
        // this._DashboardService.getUserDetails().subscribe(data => {
        //   this.fullName = data.fullName;
        //   this.phoneNumber = data.phoneNumber;
        // })
        this._DashboardService.getRedeemMotive().subscribe(function (data) {
            console.log("motive : ", data);
            _this.dropdownData = data;
        });
        this.bgdRedeemForm = this.formBuilder.group({
            redeemReason: ['', forms_1.Validators.required]
        });
        // this._DashboardService.initRedeem(this.acctNumber, this.unit, this.fullName, this.phoneNumber, this.address, this.address2, this.postcode, this.town, this.state, this.remarks)
        //   .subscribe((response: any) => {
        //     console.log(response);
        //     this.feesForms = response[0].feesForms;
        //     this.courierCharge = this.feesForms[0].chargeFees;
        //     this.takafulCharge = this.feesForms[1].chargeFees;
        //     this.mintingCharge = this.feesForms[2].chargeFees;
        //     console.log(this.courierCharge);
        //     console.log(this.takafulCharge);
        //     console.log(this.mintingCharge);
        //     this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge;
        // });
        this._DashboardService.getUserAddress().pipe(operators_1.map(function (value) {
            _this.addressModel = value;
            for (var i = 0; i < _this.addressModel.length; i++) {
                if (_this.addressModel[i].addressType.code == "L04") {
                    _this.addressIndex = i;
                    break;
                }
                else if (_this.addressModel[i].addressType.code == "L01") {
                    _this.addressIndex = i;
                }
                else {
                    _this.addressIndex = -1;
                }
            }
            return _this.addressModel;
        })).subscribe(function (data) {
            console.log("ADDRESS ", data);
            if (_this.addressIndex === -1 || !data[_this.addressIndex]) {
                _this.id = null;
                _this.label = null;
                _this.address1 = null;
                _this.address2 = null;
                _this.postcode = null;
                _this.town = null;
                _this.state = null;
            }
            else {
                _this.id = data[_this.addressIndex].id;
                _this.label = data[_this.addressIndex].label;
                _this.address1 = data[_this.addressIndex].address1;
                _this.address2 = data[_this.addressIndex].address2;
                _this.postcode = data[_this.addressIndex].postcode;
                _this.town = data[_this.addressIndex].town;
                _this.state = data[_this.addressIndex].state.name;
            }
            // if (this.phone == null || this.phone == '' || !this.phone) {
            //   this.phoneNumber = "Enter your phone number"
            // } else {
            //   this.phoneNumber = this.phone;
            // }
            // if (this.address1 == null || this.address1 == '' || !this.address1) {
            //   this.address1 = "Enter your address line 1"
            // } else {
            //   this.address1 = this.address1
            // }
            // if (this.address2 == null || this.address2 == '' || !this.address2) {
            //   this.address2 = "Enter your address line 2"
            // } else {
            //   this.address2 = this.address2;
            // }
            // if (this.postcode == null || this.postcode == '' || !this.postcode) {
            //   this.postcode = "Enter your postcode"
            // } else {
            //   this.postcode = this.postcode;
            // }
            // if (this.town == null || this.town == '' || !this.town) {
            //   this.town = "Enter your city"
            // } else {
            //   this.town = this.town;
            // }
            // if (this.state == null || this.state == '' || !this.state) {
            //   this.state = "Enter your state"
            // } else {
            //   this.state = this.state;
            // }
            // if (this.label == null || this.label == '' || !this.label) {
            //   this.label = "Note to the driver"
            // } else {
            //   this.label = this.label;
            // }
        });
    };
    RedeemBgdAddressComponent.prototype.editDetail = function () {
        this.router.navigate(['/redeem-bgd-delivery-address'], {
            queryParams: {
                reason: this.redeemReason,
                acctNumber: this.acctNumber,
                weight: this.weight,
                remarks: this.remarks,
                serialNumber: this.serialNumber,
                reference: this.reference,
                name: this.fullName,
                phone: this.phone,
                courierCharge: this.courierCharge,
                courierfeeTypeCode: this.courierfeeTypeCode,
                couriersetupFees: this.couriersetupFees,
                couriersetupUom: this.couriersetupUom,
                couriersetupUomCode: this.couriersetupUomCode,
                courierchargeFees: this.courierchargeFees,
                courierchargeUom: this.courierchargeUom,
                courierchargeUomCode: this.courierchargeUomCode,
                SSTCharge: this.SSTCharge,
                SSTfeeTypeCode: this.SSTfeeTypeCode,
                SSTsetupFees: this.SSTsetupFees,
                SSTsetupUom: this.SSTsetupUom,
                SSTsetupUomCode: this.SSTsetupUomCode,
                SSTchargeFees: this.SSTchargeFees,
                SSTchargeUom: this.SSTchargeUom,
                SSTchargeUomCode: this.SSTchargeUomCode,
                takafulCharge: this.takafulCharge,
                takafulfeeTypeCode: this.takafulfeeTypeCode,
                takafulsetupFees: this.takafulsetupFees,
                takafulsetupUom: this.takafulsetupUom,
                takafulsetupUomCode: this.takafulsetupUomCode,
                takafulchargeFees: this.takafulchargeFees,
                takafulchargeUom: this.takafulchargeUom,
                takafulchargeUomCode: this.takafulchargeUomCode,
                SST2Charge: this.SST2Charge,
                SST2feeTypeCode: this.SST2feeTypeCode,
                SST2setupFees: this.SST2setupFees,
                SST2setupUom: this.SST2setupUom,
                SST2setupUomCode: this.SST2setupUomCode,
                SST2chargeFees: this.SST2chargeFees,
                SST2chargeUom: this.SST2chargeUom,
                SST2chargeUomCode: this.SST2chargeUomCode,
                mintingCharge: this.mintingCharge,
                mintingfeeTypeCode: this.mintingfeeTypeCode,
                mintingsetupFees: this.mintingsetupFees,
                mintingsetupUom: this.mintingsetupUom,
                mintingsetupUomCode: this.mintingsetupUomCode,
                mintingchargeFees: this.mintingchargeFees,
                mintingchargeUom: this.mintingchargeUom,
                mintingchargeUomCode: this.mintingchargeUomCode,
                SST3Charge: this.SST3Charge,
                SST3feeTypeCode: this.SST3feeTypeCode,
                SST3setupFees: this.SST3setupFees,
                SST3setupUom: this.SST3setupUom,
                SST3setupUomCode: this.SST3setupUomCode,
                SST3chargeFees: this.SST3chargeFees,
                SST3chargeUom: this.SST3chargeUom,
                SST3chargeUomCode: this.SST3chargeUomCode,
                unit: this.unit
            }
        });
    };
    RedeemBgdAddressComponent.prototype.backButton = function () {
        this.router.navigate(['/redeem-bgd-amount']);
    };
    RedeemBgdAddressComponent.prototype.redeemContinue = function () {
        this.disableButton = true;
        this.reason = this.bgdRedeemForm.get('redeemReason').value;
        // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log(this.reason);
        if ((this.address == null || this.address == '') && (this.postcode == null || this.postcode == '') && (this.address == null || this.address == '') && (this.postcode == null || this.postcode == '') && (this.town == null || this.town == '') && (this.state == null || this.state == '')) {
            this.disableButton = false;
            this.alert = {
                type: 'error',
                message: 'Please enter your delivery address'
            };
            this.showAlert = true;
        }
        else {
            this.disableButton = false;
            this.router.navigate(['/redeem-bgd-confirmation'], {
                queryParams: {
                    // response: JSON.stringify(response),
                    reason: this.reason,
                    acctNumber: this.acctNumber,
                    weight: this.weight,
                    remarks: this.remarks,
                    serialNumber: this.serialNumber,
                    reference: this.reference,
                    name: this.fullName,
                    phone: this.phone,
                    courierCharge: this.courierCharge,
                    courierfeeTypeCode: this.courierfeeTypeCode,
                    couriersetupFees: this.couriersetupFees,
                    couriersetupUom: this.couriersetupUom,
                    couriersetupUomCode: this.couriersetupUomCode,
                    courierchargeFees: this.courierchargeFees,
                    courierchargeUom: this.courierchargeUom,
                    courierchargeUomCode: this.courierchargeUomCode,
                    SSTCharge: this.SSTCharge,
                    SSTfeeTypeCode: this.SSTfeeTypeCode,
                    SSTsetupFees: this.SSTsetupFees,
                    SSTsetupUom: this.SSTsetupUom,
                    SSTsetupUomCode: this.SSTsetupUomCode,
                    SSTchargeFees: this.SSTchargeFees,
                    SSTchargeUom: this.SSTchargeUom,
                    SSTchargeUomCode: this.SSTchargeUomCode,
                    takafulCharge: this.takafulCharge,
                    takafulfeeTypeCode: this.takafulfeeTypeCode,
                    takafulsetupFees: this.takafulsetupFees,
                    takafulsetupUom: this.takafulsetupUom,
                    takafulsetupUomCode: this.takafulsetupUomCode,
                    takafulchargeFees: this.takafulchargeFees,
                    takafulchargeUom: this.takafulchargeUom,
                    takafulchargeUomCode: this.takafulchargeUomCode,
                    SST2Charge: this.SST2Charge,
                    SST2feeTypeCode: this.SST2feeTypeCode,
                    SST2setupFees: this.SST2setupFees,
                    SST2setupUom: this.SST2setupUom,
                    SST2setupUomCode: this.SST2setupUomCode,
                    SST2chargeFees: this.SST2chargeFees,
                    SST2chargeUom: this.SST2chargeUom,
                    SST2chargeUomCode: this.SST2chargeUomCode,
                    mintingCharge: this.mintingCharge,
                    mintingfeeTypeCode: this.mintingfeeTypeCode,
                    mintingsetupFees: this.mintingsetupFees,
                    mintingsetupUom: this.mintingsetupUom,
                    mintingsetupUomCode: this.mintingsetupUomCode,
                    mintingchargeFees: this.mintingchargeFees,
                    mintingchargeUom: this.mintingchargeUom,
                    mintingchargeUomCode: this.mintingchargeUomCode,
                    SST3Charge: this.SST3Charge,
                    SST3feeTypeCode: this.SST3feeTypeCode,
                    SST3setupFees: this.SST3setupFees,
                    SST3setupUom: this.SST3setupUom,
                    SST3setupUomCode: this.SST3setupUomCode,
                    SST3chargeFees: this.SST3chargeFees,
                    SST3chargeUom: this.SST3chargeUom,
                    SST3chargeUomCode: this.SST3chargeUomCode,
                    unit: this.unit,
                    address1: this.address1,
                    address2: this.address2,
                    postcode: this.postcode,
                    town: this.town,
                    state: this.state
                }
            });
        }
        this.disableButton = false;
    };
    __decorate([
        core_1.ViewChild('bgdRedeemNgForm')
    ], RedeemBgdAddressComponent.prototype, "bgdRedeemNgForm");
    RedeemBgdAddressComponent = __decorate([
        core_1.Component({
            selector: 'bgd-redeem-bgd-address',
            templateUrl: './redeem-bgd-address.component.html',
            styleUrls: ['./redeem-bgd-address.component.scss'],
            animations: [
                animations_1.trigger('shake', [])
            ]
        })
    ], RedeemBgdAddressComponent);
    return RedeemBgdAddressComponent;
}());
exports.RedeemBgdAddressComponent = RedeemBgdAddressComponent;
