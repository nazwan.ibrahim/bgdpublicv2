import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeemBgdAddressComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-address/redeem-bgd-address.component';

const routes: Routes = [
  {
    path: '',
    component: RedeemBgdAddressComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedeemBgdAddressRoutingModule { }
