import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeemBgdStatusComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-status/redeem-bgd-status.component';

const routes: Routes = [
    {
        path: '',
        component: RedeemBgdStatusComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RedeemBgdStatusRoutingModule { }
