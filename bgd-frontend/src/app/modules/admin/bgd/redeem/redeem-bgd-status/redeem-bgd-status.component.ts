import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { bgdService } from '../../bgd.service';
import { saveAs } from 'file-saver';
@Component({
  selector: 'bgd-redeem-bgd-status',
  templateUrl: './redeem-bgd-status.component.html',
  styleUrls: ['./redeem-bgd-status.component.scss']
})
export class RedeemBgdStatusComponent {

  unit: any;
  address: any;
  acctNumber: any;
  fullName: any;
  phoneNumber: any;
  remarks: any;
  address1: any;
  address2: any;
  postcode: any;
  town: any;
  redeemReason: any;
  takafulCharge: any;
  mintingCharge: any;
  courierCharge: any;
  SSTCharge: any;
  SST2Charge: any;
  SST3Charge: any;
  totalAmount: any;
  totalAmountSST: any;
  state: any;
  today: Date = new Date();
  reason: any;
  message: any;
  status: any;
  ref: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private router: Router,
    private bgdService: bgdService,

  ) {
    let response = JSON.parse(this._activatedRoute.snapshot.queryParams['data']);
    let Response = JSON.parse(this._activatedRoute.snapshot.queryParams['Response']);

    console.log("Data:", response);
    console.log("Response:", Response);

    this.acctNumber = response[0].acctNumber;
    this.fullName = response[0].name;
    this.address = response[0].address;
    this.address2 = response[0].address2;
    this.postcode = response[0].postcode;
    this.town = response[0].city;
    this.state = response[0].state;
    this.remarks = response[0].remarks;
    this.unit = response[0].unit;
    this.phoneNumber = response[0].phone;
    this.courierCharge = parseFloat(response[0].feesForms[0].chargeFees);
    this.SSTCharge = parseFloat(response[0].feesForms[1].chargeFees);
    this.takafulCharge = parseFloat(response[0].feesForms[2].chargeFees);
    this.SST2Charge = parseFloat(response[0].feesForms[3].chargeFees);
    this.mintingCharge = parseFloat(response[0].feesForms[4].chargeFees);
    this.SST3Charge = parseFloat(response[0].feesForms[5].chargeFees);
    this.reason = response[0].reason;
    this.totalAmountSST = this.SSTCharge + this.SST2Charge + this.SST3Charge;
    this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge + this.totalAmountSST;
    this.message = Response.message;
    this.status = Response.status;
    this.ref = Response.reference;

    console.log(this.totalAmountSST);


  }

  downloadReceipt() {

    this.bgdService.downloadReceipt(this.ref, 'F04').subscribe(
      (response: any) => {
        console.log("Response Do", response);
        // Handle the response
        const blob = new Blob([response], { type: 'application/pdf' });
        saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        // Handle any errors that occur during the API request
        console.error('Error downloading receipt:', error);
      }
    );
  }

}
