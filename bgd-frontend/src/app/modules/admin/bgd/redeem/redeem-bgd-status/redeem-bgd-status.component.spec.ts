import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdStatusComponent } from './redeem-bgd-status.component';

describe('RedeemBgdStatusComponent', () => {
  let component: RedeemBgdStatusComponent;
  let fixture: ComponentFixture<RedeemBgdStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemBgdStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
