import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RedeemBgdComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd/redeem-bgd.component';
import { RedeemBgdRoutes } from 'app/modules/admin/bgd/redeem/redeem-bgd/redeem-bgd-routing';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@bgd/components/alert';

@NgModule({
    declarations: [
        RedeemBgdComponent,

    ],
    imports: [
        MatButtonModule,
        RouterModule.forChild(RedeemBgdRoutes),
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        FuseAlertModule

    ]
})
export class RedeemBgdModule {
}
