import { Route } from '@angular/router';
import { RedeemBgdComponent } from './redeem-bgd.component';

export const RedeemBgdRoutes: Route[] = [
  {
    path: '',
    component: RedeemBgdComponent
  }
];
