import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root',
})
export class redeemBgdService {
    private _authenticated = true;
    constructor(private _httpClient: HttpClient) { }

    //
    initRedeem(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/initRedeem');
    }

    walletDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/walletDetails');
    }
}