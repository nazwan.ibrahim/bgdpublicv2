import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdComponent } from './redeem-bgd.component';

describe('RedeemBgdComponent', () => {
  let component: RedeemBgdComponent;
  let fixture: ComponentFixture<RedeemBgdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RedeemBgdComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
