"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RedeemBgdComponent = void 0;
var core_1 = require("@angular/core");
var operators_1 = require("rxjs/operators");
var rxjs_1 = require("rxjs");
var RedeemBgdComponent = /** @class */ (function () {
    function RedeemBgdComponent(_redeemBgdService, _httpClient, _DashboardService, router) {
        this._redeemBgdService = _redeemBgdService;
        this._httpClient = _httpClient;
        this._DashboardService = _DashboardService;
        this.router = router;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.id = '';
        this.weight = '';
        this.walletDetails = [];
        this.cashWalletIndex = -1;
        this.selectedOption = '';
        this.remarks = ' ';
        this.disableButton = false;
    }
    RedeemBgdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._redeemBgdService.walletDetails().pipe(operators_1.tap(function (value) {
            if (!value) {
                return rxjs_1.of(null);
            }
            _this.walletModel = value;
            for (var i = 0; i < _this.walletModel.length; i++) {
                if (_this.walletModel[i].type == "Gold") {
                    _this.goldWalletIndex = i;
                    break;
                }
            }
            return _this._redeemBgdService.walletDetails();
        }), operators_1.catchError(function (error) {
            return rxjs_1.of(null);
        })).subscribe(function (data) {
            if (!data) {
                return rxjs_1.of(null);
            }
            _this.availAmount = data[_this.goldWalletIndex].availableAmount.toFixed(6);
            var maxNumberOfGoldPieces;
            if (_this.availAmount >= 4.250000) {
                _this.maxNumberOfGoldPieces = Math.floor(_this.availAmount / 4.250000);
            }
            else {
                _this.maxNumberOfGoldPieces = 0;
            }
            console.log(maxNumberOfGoldPieces);
        });
    };
    RedeemBgdComponent.prototype.redeemNow = function () {
        this.disableButton = true;
        var onePieceOfGoldWeight = 4.250000; // weight of one piece of gold in grams
        var availableGoldWeight = this.availAmount; // available amount of gold weight in grams
        // check if the available gold weight is sufficient to redeem at least one piece of gold
        if (availableGoldWeight >= onePieceOfGoldWeight) {
            this.router.navigate(['/redeem-bgd-amount']);
        }
        else {
            this.disableButton = false;
            this.alert.type = 'error';
            this.alert.message = 'Insufficient amount of gold.';
            // display error message
            console.log(alert);
        }
        this.disableButton = false;
    };
    RedeemBgdComponent.prototype.cancel = function () {
        this.router.navigate(['/dashboard']);
    };
    RedeemBgdComponent = __decorate([
        core_1.Component({
            selector: 'bgd-redeem-bgd',
            templateUrl: './redeem-bgd.component.html',
            styleUrls: ['./redeem-bgd.component.scss']
        })
    ], RedeemBgdComponent);
    return RedeemBgdComponent;
}());
exports.RedeemBgdComponent = RedeemBgdComponent;
