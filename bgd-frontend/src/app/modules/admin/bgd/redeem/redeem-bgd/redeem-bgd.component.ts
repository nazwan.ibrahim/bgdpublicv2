import { Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { redeemBgdService } from 'app/modules/admin/bgd/redeem/redeem-bgd/redeem-bgd.service';
import { switchMap, map, tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { of } from 'rxjs';
import { FuseAlertModule, FuseAlertType } from '@bgd/components/alert';
import { CommonModule } from '@angular/common';
import { fuseAnimations } from '@bgd/animations';


@Component({
  selector: 'bgd-redeem-bgd',
  templateUrl: './redeem-bgd.component.html',
  styleUrls: ['./redeem-bgd.component.scss'],
  animations: fuseAnimations
})
export class RedeemBgdComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  maxNumberOfGoldPieces: number;
  id: any = '';
  weight: any = '';
  walletDetails = [];
  goldWalletIndex: number;
  accountNum: any;
  uomType: any;
  availAmount: any;
  accountNumber: any;
  bankName: any;
  bankCode: any;
  walletModel: any;
  cashWalletIndex: number = -1;
  selectedOption: string = '';
  curAccNum: any;
  fullName: any;
  phoneNumber: any;
  address1: any;
  address2: any;
  postcode: any;
  town: string;
  acctNumber: any;
  unit: any;
  name: any;
  phone: any;
  address: any;
  remarks: any = ' ';
  tempValue: any;
  feesForms: any;
  addressForm: any;
  coinRedeemed: any;
  courierCharge: any;
  takafulCharge: any;
  mintingCharge: any;
  goldRedeemOptions: any;
  disableButton: boolean = false;
  showAlert = false;
  enoughRedeem: boolean = false;

  constructor(
    private _redeemBgdService: redeemBgdService,
    private _httpClient: HttpClient,
    private _DashboardService: DashboardService,
    private router: Router
  ) { }

  ngOnInit() {
    this._redeemBgdService.walletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Gold") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._redeemBgdService.walletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }
      this.availAmount = data[this.goldWalletIndex].availableAmount.toFixed(6);

      const maxNumberOfGoldPieces = 0;
      if (this.availAmount >= 4.250000) {
        this.maxNumberOfGoldPieces = Math.floor(this.availAmount / 4.250000);
        this.enoughRedeem = true;
      } else {
        this.maxNumberOfGoldPieces = 0;
        this.enoughRedeem = false;
      }
      console.log(maxNumberOfGoldPieces);
    });
  }


  redeemNow(): void {

    this.disableButton = true;

    const onePieceOfGoldWeight = 4.250000; // weight of one piece of gold in grams
    const availableGoldWeight = this.availAmount; // available amount of gold weight in grams

    // check if the available gold weight is sufficient to redeem at least one piece of gold
    if (availableGoldWeight >= onePieceOfGoldWeight) {
      this.router.navigate(['/redeem-bgd-amount']);
    } else {
   this.disableButton = false;

      this.alert = {
        type: 'error',
        message: 'Insufficient gold wallet balance.'
      };
      this.showAlert = true;
    }

    this.disableButton = false;
  }

  cancel(): void {
    this.router.navigate(['/dashboard']);
  }
}
