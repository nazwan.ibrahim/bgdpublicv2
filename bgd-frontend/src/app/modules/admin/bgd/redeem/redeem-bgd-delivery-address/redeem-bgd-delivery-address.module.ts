import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RedeemBgdDeliveryAddressComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-delivery-address/redeem-bgd-delivery-address.component';
import { RedeemBgdDeliveryAddressRoutingModule } from 'app/modules/admin/bgd/redeem/redeem-bgd-delivery-address/redeem-bgd-delivery-address-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { RedeemAddressDialogComponent } from './redeem-address-dialog/redeem-address-dialog.component';

@NgModule({
    declarations: [
        RedeemBgdDeliveryAddressComponent,
        RedeemAddressDialogComponent,

    ],
    imports: [
        RedeemBgdDeliveryAddressRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule
    ]
})
export class RedeemBgdDeliveryAddressModule {
}
