import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeemBgdDeliveryAddressComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-delivery-address/redeem-bgd-delivery-address.component';

const routes: Routes = [
  {
    path: '',
    component: RedeemBgdDeliveryAddressComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedeemBgdDeliveryAddressRoutingModule { }
