import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdDeliveryAddressComponent } from './redeem-bgd-delivery-address.component';

describe('RedeemBgdDeliveryAddressComponent', () => {
  let component: RedeemBgdDeliveryAddressComponent;
  let fixture: ComponentFixture<RedeemBgdDeliveryAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemBgdDeliveryAddressComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdDeliveryAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
