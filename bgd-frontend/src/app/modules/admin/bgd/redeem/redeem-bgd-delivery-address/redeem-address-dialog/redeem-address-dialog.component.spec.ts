import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemAddressDialogComponent } from './redeem-address-dialog.component';

describe('RedeemAddressDialogComponent', () => {
  let component: RedeemAddressDialogComponent;
  let fixture: ComponentFixture<RedeemAddressDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemAddressDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemAddressDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
