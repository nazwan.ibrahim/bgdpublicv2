import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { bgdService } from '../../bgd.service';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators, FormGroup, FormControl, NgForm, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { editProfileService } from 'app/modules/admin/my-account/cards/edit-profile/edit-profile.service';
import { add, result } from 'lodash';
import { FuseAlertType } from '@bgd/components/alert';
import { finalize, map } from 'rxjs';
import { RedeemService } from '../redeem.service';
import { MatDialog } from '@angular/material/dialog';
import { RedeemAddressDialogComponent } from './redeem-address-dialog/redeem-address-dialog.component';

@Component({
  selector: 'bgd-redeem-bgd-delivery-address',
  templateUrl: './redeem-bgd-delivery-address.component.html',
  styleUrls: ['./redeem-bgd-delivery-address.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class RedeemBgdDeliveryAddressComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  @ViewChild('bgdRedeemConfirmForm') bgdRedeemConfirmNgForm: NgForm;

  bgdRedeemConfirmForm: UntypedFormGroup;
  stateOption = [];
  selectedOption: string = '';
  userID: any;
  fullName: any;
  email: any;
  address1: any;
  address2: any;
  postcode: any;
  postcodeApi: number = 0;
  city: any;
  state: any;
  response: any;
  town: string;
  addressForm: any;
  phone_number: any;
  unit: any;
  acctNumber: any;
  addressModel: any;
  addressIndex: number = -1;
  phoneNumber: any;
  remarks: any;
  id: any;
  stateCode: any;
  countryCode: any;
  addressTypeCode: any;
  label: any;
  redeemReason: any;
  showAlert = false;
  errorMessage: any;
  labelAlertVisible: boolean = false;
  phone: any;
  weight: any;
  serialNumber: any;
  reference: any;
  courierCharge: any;
  courierfeeTypeCode: any;
  couriersetupFees: any;
  couriersetupUom: any;
  couriersetupUomCode: any;
  courierchargeFees: any;
  courierchargeUom: any;
  courierchargeUomCode: any;
  SSTCharge: any;
  SSTfeeTypeCode: any;
  SSTsetupFees: any;
  SSTsetupUom: any;
  SSTsetupUomCode: any;
  SSTchargeFees: any;
  SSTchargeUom: any;
  SSTchargeUomCode: any;
  takafulCharge: any;
  takafulfeeTypeCode: any;
  takafulsetupFees: any;
  takafulsetupUom: any;
  takafulsetupUomCode: any;
  takafulchargeFees: any;
  takafulchargeUom: any;
  takafulchargeUomCode: any;
  SST2Charge: any;
  SST2feeTypeCode: any;
  SST2setupFees: any;
  SST2setupUom: any;
  SST2setupUomCode: any;
  SST2chargeFees: any;
  SST2chargeUom: any;
  SST2chargeUomCode: any;
  mintingCharge: any;
  mintingfeeTypeCode: any;
  mintingsetupFees: any;
  mintingsetupUom: any;
  mintingsetupUomCode: any;
  mintingchargeFees: any;
  mintingchargeUom: any;
  mintingchargeUomCode: any;
  SST3Charge: any;
  SST3feeTypeCode: any;
  SST3setupFees: any;
  SST3setupUom: any;
  SST3setupUomCode: any;
  SST3chargeFees: any;
  SST3chargeUom: any;
  SST3chargeUomCode: any;
  disableButton: boolean = false;

  constructor(
    private bgdService: bgdService,
    private _httpClient: HttpClient,
    private _formBuilder: FormBuilder,
    private _editProfileService: editProfileService,
    private _activatedRoute: ActivatedRoute,
    private router: Router,
    private redeemService: RedeemService,
    public dialog: MatDialog,
    ) {}
  

  ngOnInit() {

    this._editProfileService.userDetail().subscribe(data => {
      this.userID = data.userID;
      this.email = data.email;
      this.fullName = data.fullName;
      this.phoneNumber = data.phoneNumber;
      console.log("User detail", data);
    });

    this._editProfileService.userAddress().pipe(
      map((value: any) => {
        this.addressModel = value;

        console.log("ADDRESS ", value);

        for (var i = 0; i < this.addressModel.length; i++) {
          if (this.addressModel[i].addressType.code == "L04") {
            this.addressIndex = i;
            break;
          } else if (this.addressModel[i].addressType.code == "L01") {
            this.addressIndex = i;
          } else {
            this.addressIndex = -1;
          }
        }

        return this.addressModel;
      })
    ).subscribe((data: any) => {

      console.log("Current Address", data);

      if (this.addressIndex === -1 || !data[this.addressIndex]) {
        this.id = null;
        this.label = null;
        this.address1 = null;
        this.address2 = null;
        this.postcode = null;
        this.town = null;
        this.state = null;

      } else {
        this.id = data[this.addressIndex].id;
        this.label = data[this.addressIndex].label;
        this.address1 = data[this.addressIndex].address1;
        this.address2 = data[this.addressIndex].address2;
        this.postcode = data[this.addressIndex].postcode;
        this.town = data[this.addressIndex].town;
        this.stateCode = data[this.addressIndex].state.code;
        this.state = data[this.addressIndex].state.name;
        this.addressTypeCode = data[this.addressIndex].addressType.code;
      }
    });

    this._editProfileService.getState().subscribe(data => {
      this.stateOption = data;
    });

    this.bgdRedeemConfirmForm = this._formBuilder.group({
      fullName: [this.fullName],
      phoneNumber: [''],
      address1: ['', Validators.required],
      address2: [''],
      postcode: ['', Validators.required],
      town: ['', Validators.required],
      state: [this.stateCode, Validators.required]
    });
  }

  checkPostcode() {
    this.postcode = this.bgdRedeemConfirmForm.get('postcode').value;
    console.log(this.postcode);

    this._editProfileService.getPostcodeInfo(this.postcode).subscribe(
      (result) => {

        let response = JSON.parse(result);

        this.town = response.city;
        this.stateCode = response.state.code;

        console.log(this.town);
      },
      (error) => {
        if (error.status === 404) { // check if the error is a 404 Not Found
          this.town = null;
          this.errorMessage = 'Invalid postcode'; // set a custom error message
        } else {
          this.errorMessage = 'An error occurred'; // set a generic error message
        }
      }
    );
  }

  onSubmit() {

    //this.disableButton = true;

    // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    //if (this.bgdRedeemConfirmForm.invalid) {
    //  return;
    //}

    this.bgdRedeemConfirmForm.disable();

    if (this.addressTypeCode != "L04") {
      const detailAddress = {
        id: this.id,
        label: this.label,
        address1: this.bgdRedeemConfirmForm.get('address1').value,
        address2: this.bgdRedeemConfirmForm.get('address2').value,
        postcode: this.bgdRedeemConfirmForm.get('postcode').value,
        town: this.town,
        stateCode: this.stateCode,
        countryCode: this.countryCode,
        addressTypeCode: "L04"
      }

      this._editProfileService.createAddress(detailAddress)
        .pipe(
          finalize(() => {
            this.bgdRedeemConfirmForm.enable();

            this.bgdRedeemConfirmForm.reset();
          })
        )
        .subscribe(
          Response => {

            console.log("New Address :", Response);

            this.alert = {
              type: 'success',
              message: 'Success'
            };

            this.openDialog()  
          },
          error => {

            this.disableButton = false;

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };
          }
        );
    } else if (this.addressTypeCode == "L04") {
      const updatedAddress = {
        id: this.id,
        label: this.label,
        address1: this.bgdRedeemConfirmForm.get('address1').value,
        address2: this.bgdRedeemConfirmForm.get('address2').value,
        postcode: this.bgdRedeemConfirmForm.get('postcode').value,
        town: this.town,
        stateCode: this.stateCode,
        countryCode: this.countryCode,
        addressTypeCode: this.addressTypeCode
      }
      console.log("Update Address", updatedAddress);

      // Call the updateAddress service method with the new address details
      this._editProfileService.updateAddress(updatedAddress)
        .pipe(
          finalize(() => {
            this.bgdRedeemConfirmForm.enable();

            this.bgdRedeemConfirmForm.reset();
          })
        )
        .subscribe(
          Response => {

            console.log("Update", Response);

            this.alert = {
              type: 'success',
              message: 'Success'
            };

            // Navigate to the next page on successful address update
            this.openDialog()          },
          error => {

            this.disableButton = false;

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };
          }
        );
    }

  }

  cancelBtn() {
    // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    this.router.navigate(['/redeem-bgd-address']);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RedeemAddressDialogComponent, {
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }
}