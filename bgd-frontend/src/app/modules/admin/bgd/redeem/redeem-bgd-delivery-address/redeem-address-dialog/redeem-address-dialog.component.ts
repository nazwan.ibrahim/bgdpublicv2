import { Component, Inject, HostListener } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'app/core/auth/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-redeem-address-dialog',
  templateUrl: './redeem-address-dialog.component.html',
  styleUrls: ['./redeem-address-dialog.component.scss']
})
export class RedeemAddressDialogComponent {

  constructor(
    private dialogRef: MatDialogRef<RedeemAddressDialogComponent>,
    // @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthService,
    private router: Router,
    private location: Location
  )
  {}

  onProceed(): void {
    this.dialogRef.close(true);
    this.router.navigate(['/redeem-bgd-address']);
  }

  @HostListener('document:click', ['$event.target'])
  onClickOutside(targetElement: HTMLElement): void {
    if (!this.dialogRef.disableClose && targetElement.classList.contains('cdk-overlay-backdrop')) {
      this.dialogRef.close(false);
      this.location.back();
    }
  }

}
