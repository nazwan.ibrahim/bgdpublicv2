"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RedeemBgdDeliveryAddressComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var RedeemBgdDeliveryAddressComponent = /** @class */ (function () {
    function RedeemBgdDeliveryAddressComponent(bgdService, _httpClient, _formBuilder, _editProfileService, _activatedRoute, router) {
        this.bgdService = bgdService;
        this._httpClient = _httpClient;
        this._formBuilder = _formBuilder;
        this._editProfileService = _editProfileService;
        this._activatedRoute = _activatedRoute;
        this.router = router;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.stateOption = [];
        this.selectedOption = '';
        this.postcodeApi = 0;
        this.addressIndex = -1;
        this.showAlert = false;
        this.labelAlertVisible = false;
        this.disableButton = false;
        this.fullName = this._activatedRoute.snapshot.queryParams['name'];
        this.phone = this._activatedRoute.snapshot.queryParams['phone'];
        this.acctNumber = this._activatedRoute.snapshot.queryParams['acctNumber'];
        this.courierCharge = this._activatedRoute.snapshot.queryParams['courierCharge'];
        this.courierfeeTypeCode = this._activatedRoute.snapshot.queryParams['courierfeeTypeCode'];
        this.couriersetupFees = this._activatedRoute.snapshot.queryParams['couriersetupFees'];
        this.couriersetupUom = this._activatedRoute.snapshot.queryParams['couriersetupUom'];
        this.couriersetupUom = this._activatedRoute.snapshot.queryParams['couriersetupUom'];
        this.courierchargeFees = this._activatedRoute.snapshot.queryParams['courierchargeFees'];
        this.courierchargeUom = this._activatedRoute.snapshot.queryParams['courierchargeUom'];
        this.courierchargeUomCode = this._activatedRoute.snapshot.queryParams['courierchargeUomCode'];
        this.SSTCharge = this._activatedRoute.snapshot.queryParams['SSTCharge'];
        this.SSTfeeTypeCode = this._activatedRoute.snapshot.queryParams['SSTfeeTypeCode'];
        this.SSTsetupFees = this._activatedRoute.snapshot.queryParams['SSTsetupFees'];
        this.SSTsetupUom = this._activatedRoute.snapshot.queryParams['SSTsetupUom'];
        this.SSTsetupUomCode = this._activatedRoute.snapshot.queryParams['SSTsetupUomCode'];
        this.SSTchargeFees = this._activatedRoute.snapshot.queryParams['SSTchargeFees'];
        this.SSTchargeUom = this._activatedRoute.snapshot.queryParams['SSTchargeUom'];
        this.SSTchargeUomCode = this._activatedRoute.snapshot.queryParams['SSTchargeUomCode'];
        this.takafulCharge = this._activatedRoute.snapshot.queryParams['takafulCharge'];
        this.takafulfeeTypeCode = this._activatedRoute.snapshot.queryParams['takafulfeeTypeCode'];
        this.takafulsetupFees = this._activatedRoute.snapshot.queryParams['takafulsetupFees'];
        this.takafulsetupUom = this._activatedRoute.snapshot.queryParams['takafulsetupUom'];
        this.takafulsetupUomCode = this._activatedRoute.snapshot.queryParams['takafulsetupUomCode'];
        this.takafulchargeFees = this._activatedRoute.snapshot.queryParams['takafulchargeFees'];
        this.takafulchargeUom = this._activatedRoute.snapshot.queryParams['takafulchargeUom'];
        this.takafulchargeUomCode = this._activatedRoute.snapshot.queryParams['takafulchargeUomCode'];
        this.SST2Charge = this._activatedRoute.snapshot.queryParams['SST2Charge'];
        this.SST2feeTypeCode = this._activatedRoute.snapshot.queryParams['SST2feeTypeCode'];
        this.SST2setupFees = this._activatedRoute.snapshot.queryParams['SST2setupFees'];
        this.SST2setupUom = this._activatedRoute.snapshot.queryParams['SST2setupUom'];
        this.SST2setupUomCode = this._activatedRoute.snapshot.queryParams['SST2setupUomCode'];
        this.SST2chargeFees = this._activatedRoute.snapshot.queryParams['SST2chargeFees'];
        this.SST2chargeUom = this._activatedRoute.snapshot.queryParams['SST2chargeUom'];
        this.SST2chargeUomCode = this._activatedRoute.snapshot.queryParams['SST2chargeUomCode'];
        this.mintingCharge = this._activatedRoute.snapshot.queryParams['mintingCharge'];
        this.mintingfeeTypeCode = this._activatedRoute.snapshot.queryParams['mintingfeeTypeCode'];
        this.mintingsetupFees = this._activatedRoute.snapshot.queryParams['mintingsetupFees'];
        this.mintingsetupUom = this._activatedRoute.snapshot.queryParams['mintingsetupUom'];
        this.mintingsetupUomCode = this._activatedRoute.snapshot.queryParams['mintingsetupUomCode'];
        this.mintingchargeFees = this._activatedRoute.snapshot.queryParams['mintingchargeFees'];
        this.mintingchargeUom = this._activatedRoute.snapshot.queryParams['mintingchargeUom'];
        this.mintingchargeUomCode = this._activatedRoute.snapshot.queryParams['mintingchargeUomCode'];
        this.SST3Charge = this._activatedRoute.snapshot.queryParams['SST3Charge'];
        this.SST3feeTypeCode = this._activatedRoute.snapshot.queryParams['SST3feeTypeCode'];
        this.SST3setupFees = this._activatedRoute.snapshot.queryParams['SST3setupFees'];
        this.SST3setupUom = this._activatedRoute.snapshot.queryParams['SST3setupUom'];
        this.SST3setupUomCode = this._activatedRoute.snapshot.queryParams['SST3setupUomCode'];
        this.SST3chargeFees = this._activatedRoute.snapshot.queryParams['SST3chargeFees'];
        this.SST3chargeUom = this._activatedRoute.snapshot.queryParams['SST3chargeUom'];
        this.SST3chargeUomCode = this._activatedRoute.snapshot.queryParams['SST3chargeUomCode'];
        this.unit = this._activatedRoute.snapshot.queryParams['unit'];
        this.weight = this._activatedRoute.snapshot.queryParams['weight'];
        this.remarks = this._activatedRoute.snapshot.queryParams['remarks'];
        this.serialNumber = this._activatedRoute.snapshot.queryParams['serialNumber'];
        this.reference = this._activatedRoute.snapshot.queryParams['reference'];
    }
    RedeemBgdDeliveryAddressComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._editProfileService.userDetail().subscribe(function (data) {
            _this.userID = data.userID;
            _this.email = data.email;
            _this.fullName = data.fullName;
            _this.phoneNumber = data.phoneNumber;
            console.log("User detail", data);
        });
        this._editProfileService.userAddress().pipe(rxjs_1.map(function (value) {
            _this.addressModel = value;
            console.log("ADDRESS ", value);
            for (var i = 0; i < _this.addressModel.length; i++) {
                if (_this.addressModel[i].addressType.code == "L04") {
                    _this.addressIndex = i;
                    break;
                }
                else if (_this.addressModel[i].addressType.code == "L01") {
                    _this.addressIndex = i;
                }
                else {
                    _this.addressIndex = -1;
                }
            }
            return _this.addressModel;
        })).subscribe(function (data) {
            console.log("Current Address", data);
            if (_this.addressIndex === -1 || !data[_this.addressIndex]) {
                _this.id = null;
                _this.label = null;
                _this.address1 = null;
                _this.address2 = null;
                _this.postcode = null;
                _this.town = null;
                _this.state = null;
            }
            else {
                _this.id = data[_this.addressIndex].id;
                _this.label = data[_this.addressIndex].label;
                _this.address1 = data[_this.addressIndex].address1;
                _this.address2 = data[_this.addressIndex].address2;
                _this.postcode = data[_this.addressIndex].postcode;
                _this.town = data[_this.addressIndex].town;
                _this.stateCode = data[_this.addressIndex].state.code;
                _this.state = data[_this.addressIndex].state.name;
                _this.addressTypeCode = data[_this.addressIndex].addressType.code;
            }
        });
        this._editProfileService.getState().subscribe(function (data) {
            _this.stateOption = data;
        });
        this.bgdRedeemConfirmForm = this._formBuilder.group({
            fullName: [this.fullName],
            phoneNumber: [''],
            address1: ['', forms_1.Validators.required],
            address2: [''],
            postcode: ['', forms_1.Validators.required],
            town: ['', forms_1.Validators.required],
            state: [this.stateCode, forms_1.Validators.required]
        });
    };
    RedeemBgdDeliveryAddressComponent.prototype.checkPostcode = function () {
        var _this = this;
        this.postcode = this.bgdRedeemConfirmForm.get('postcode').value;
        console.log(this.postcode);
        this._editProfileService.getPostcodeInfo(this.postcode).subscribe(function (result) {
            var response = JSON.parse(result);
            _this.town = response.city;
            _this.stateCode = response.state.code;
            console.log(_this.town);
        }, function (error) {
            if (error.status === 404) { // check if the error is a 404 Not Found
                _this.town = null;
                _this.errorMessage = 'Invalid postcode'; // set a custom error message
            }
            else {
                _this.errorMessage = 'An error occurred'; // set a generic error message
            }
        });
    };
    RedeemBgdDeliveryAddressComponent.prototype.onSubmit = function () {
        var _this = this;
        this.disableButton = true;
        // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        if (this.bgdRedeemConfirmForm.invalid) {
            return;
        }
        this.bgdRedeemConfirmForm.disable();
        if (this.addressTypeCode != "L04") {
            var detailAddress = {
                id: this.id,
                label: this.label,
                address1: this.bgdRedeemConfirmForm.get('address1').value,
                address2: this.bgdRedeemConfirmForm.get('address2').value,
                postcode: this.bgdRedeemConfirmForm.get('postcode').value,
                town: this.town,
                stateCode: this.stateCode,
                countryCode: this.countryCode,
                addressTypeCode: "L04"
            };
            this._editProfileService.createAddress(detailAddress)
                .pipe(rxjs_1.finalize(function () {
                _this.bgdRedeemConfirmForm.enable();
                _this.bgdRedeemConfirmForm.reset();
            }))
                .subscribe(function (Response) {
                console.log("New Address :", Response);
                _this.alert = {
                    type: 'success',
                    message: 'Success'
                };
                // Navigate to the next page on successful address update
                _this.router.navigate(['/redeem-bgd-address'], {
                    queryParams: {
                        reason: _this.redeemReason,
                        acctNumber: _this.acctNumber,
                        weight: _this.weight,
                        remarks: _this.remarks,
                        serialNumber: _this.serialNumber,
                        reference: _this.reference,
                        name: _this.fullName,
                        phone: _this.phone,
                        courierCharge: _this.courierCharge,
                        courierfeeTypeCode: _this.courierfeeTypeCode,
                        couriersetupFees: _this.couriersetupFees,
                        couriersetupUom: _this.couriersetupUom,
                        couriersetupUomCode: _this.couriersetupUomCode,
                        courierchargeFees: _this.courierchargeFees,
                        courierchargeUom: _this.courierchargeUom,
                        courierchargeUomCode: _this.courierchargeUomCode,
                        SSTCharge: _this.SSTCharge,
                        SSTfeeTypeCode: _this.SSTfeeTypeCode,
                        SSTsetupFees: _this.SSTsetupFees,
                        SSTsetupUom: _this.SSTsetupUom,
                        SSTsetupUomCode: _this.SSTsetupUomCode,
                        SSTchargeFees: _this.SSTchargeFees,
                        SSTchargeUom: _this.SSTchargeUom,
                        SSTchargeUomCode: _this.SSTchargeUomCode,
                        takafulCharge: _this.takafulCharge,
                        takafulfeeTypeCode: _this.takafulfeeTypeCode,
                        takafulsetupFees: _this.takafulsetupFees,
                        takafulsetupUom: _this.takafulsetupUom,
                        takafulsetupUomCode: _this.takafulsetupUomCode,
                        takafulchargeFees: _this.takafulchargeFees,
                        takafulchargeUom: _this.takafulchargeUom,
                        takafulchargeUomCode: _this.takafulchargeUomCode,
                        SST2Charge: _this.SST2Charge,
                        SST2feeTypeCode: _this.SST2feeTypeCode,
                        SST2setupFees: _this.SST2setupFees,
                        SST2setupUom: _this.SST2setupUom,
                        SST2setupUomCode: _this.SST2setupUomCode,
                        SST2chargeFees: _this.SST2chargeFees,
                        SST2chargeUom: _this.SST2chargeUom,
                        SST2chargeUomCode: _this.SST2chargeUomCode,
                        mintingCharge: _this.mintingCharge,
                        mintingfeeTypeCode: _this.mintingfeeTypeCode,
                        mintingsetupFees: _this.mintingsetupFees,
                        mintingsetupUom: _this.mintingsetupUom,
                        mintingsetupUomCode: _this.mintingsetupUomCode,
                        mintingchargeFees: _this.mintingchargeFees,
                        mintingchargeUom: _this.mintingchargeUom,
                        mintingchargeUomCode: _this.mintingchargeUomCode,
                        SST3Charge: _this.SST3Charge,
                        SST3feeTypeCode: _this.SST3feeTypeCode,
                        SST3setupFees: _this.SST3setupFees,
                        SST3setupUom: _this.SST3setupUom,
                        SST3setupUomCode: _this.SST3setupUomCode,
                        SST3chargeFees: _this.SST3chargeFees,
                        SST3chargeUom: _this.SST3chargeUom,
                        SST3chargeUomCode: _this.SST3chargeUomCode,
                        unit: _this.unit
                    }
                });
            }, function (error) {
                _this.disableButton = false;
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
            });
        }
        else if (this.addressTypeCode == "L04") {
            var updatedAddress = {
                id: this.id,
                label: this.label,
                address1: this.bgdRedeemConfirmForm.get('address1').value,
                address2: this.bgdRedeemConfirmForm.get('address2').value,
                postcode: this.bgdRedeemConfirmForm.get('postcode').value,
                town: this.town,
                stateCode: this.stateCode,
                countryCode: this.countryCode,
                addressTypeCode: this.addressTypeCode
            };
            console.log("Update Address", updatedAddress);
            // Call the updateAddress service method with the new address details
            this._editProfileService.updateAddress(updatedAddress)
                .pipe(rxjs_1.finalize(function () {
                _this.bgdRedeemConfirmForm.enable();
                _this.bgdRedeemConfirmForm.reset();
            }))
                .subscribe(function (Response) {
                console.log("Update", Response);
                _this.alert = {
                    type: 'success',
                    message: 'Success'
                };
                // Navigate to the next page on successful address update
                _this.router.navigate(['/redeem-bgd-address'], {
                    queryParams: {
                        reason: _this.redeemReason,
                        acctNumber: _this.acctNumber,
                        weight: _this.weight,
                        remarks: _this.remarks,
                        serialNumber: _this.serialNumber,
                        reference: _this.reference,
                        name: _this.fullName,
                        phone: _this.phone,
                        courierCharge: _this.courierCharge,
                        courierfeeTypeCode: _this.courierfeeTypeCode,
                        couriersetupFees: _this.couriersetupFees,
                        couriersetupUom: _this.couriersetupUom,
                        couriersetupUomCode: _this.couriersetupUomCode,
                        courierchargeFees: _this.courierchargeFees,
                        courierchargeUom: _this.courierchargeUom,
                        courierchargeUomCode: _this.courierchargeUomCode,
                        SSTCharge: _this.SSTCharge,
                        SSTfeeTypeCode: _this.SSTfeeTypeCode,
                        SSTsetupFees: _this.SSTsetupFees,
                        SSTsetupUom: _this.SSTsetupUom,
                        SSTsetupUomCode: _this.SSTsetupUomCode,
                        SSTchargeFees: _this.SSTchargeFees,
                        SSTchargeUom: _this.SSTchargeUom,
                        SSTchargeUomCode: _this.SSTchargeUomCode,
                        takafulCharge: _this.takafulCharge,
                        takafulfeeTypeCode: _this.takafulfeeTypeCode,
                        takafulsetupFees: _this.takafulsetupFees,
                        takafulsetupUom: _this.takafulsetupUom,
                        takafulsetupUomCode: _this.takafulsetupUomCode,
                        takafulchargeFees: _this.takafulchargeFees,
                        takafulchargeUom: _this.takafulchargeUom,
                        takafulchargeUomCode: _this.takafulchargeUomCode,
                        SST2Charge: _this.SST2Charge,
                        SST2feeTypeCode: _this.SST2feeTypeCode,
                        SST2setupFees: _this.SST2setupFees,
                        SST2setupUom: _this.SST2setupUom,
                        SST2setupUomCode: _this.SST2setupUomCode,
                        SST2chargeFees: _this.SST2chargeFees,
                        SST2chargeUom: _this.SST2chargeUom,
                        SST2chargeUomCode: _this.SST2chargeUomCode,
                        mintingCharge: _this.mintingCharge,
                        mintingfeeTypeCode: _this.mintingfeeTypeCode,
                        mintingsetupFees: _this.mintingsetupFees,
                        mintingsetupUom: _this.mintingsetupUom,
                        mintingsetupUomCode: _this.mintingsetupUomCode,
                        mintingchargeFees: _this.mintingchargeFees,
                        mintingchargeUom: _this.mintingchargeUom,
                        mintingchargeUomCode: _this.mintingchargeUomCode,
                        SST3Charge: _this.SST3Charge,
                        SST3feeTypeCode: _this.SST3feeTypeCode,
                        SST3setupFees: _this.SST3setupFees,
                        SST3setupUom: _this.SST3setupUom,
                        SST3setupUomCode: _this.SST3setupUomCode,
                        SST3chargeFees: _this.SST3chargeFees,
                        SST3chargeUom: _this.SST3chargeUom,
                        SST3chargeUomCode: _this.SST3chargeUomCode,
                        unit: _this.unit
                    }
                });
            }, function (error) {
                _this.disableButton = false;
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
            });
        }
    };
    RedeemBgdDeliveryAddressComponent.prototype.cancelBtn = function () {
        // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.router.navigate(['/redeem-bgd-address'], {
            queryParams: {
                reason: this.redeemReason,
                acctNumber: this.acctNumber,
                weight: this.weight,
                remarks: this.remarks,
                serialNumber: this.serialNumber,
                reference: this.reference,
                name: this.fullName,
                phone: this.phone,
                courierCharge: this.courierCharge,
                courierfeeTypeCode: this.courierfeeTypeCode,
                couriersetupFees: this.couriersetupFees,
                couriersetupUom: this.couriersetupUom,
                couriersetupUomCode: this.couriersetupUomCode,
                courierchargeFees: this.courierchargeFees,
                courierchargeUom: this.courierchargeUom,
                courierchargeUomCode: this.courierchargeUomCode,
                SSTCharge: this.SSTCharge,
                SSTfeeTypeCode: this.SSTfeeTypeCode,
                SSTsetupFees: this.SSTsetupFees,
                SSTsetupUom: this.SSTsetupUom,
                SSTsetupUomCode: this.SSTsetupUomCode,
                SSTchargeFees: this.SSTchargeFees,
                SSTchargeUom: this.SSTchargeUom,
                SSTchargeUomCode: this.SSTchargeUomCode,
                takafulCharge: this.takafulCharge,
                takafulfeeTypeCode: this.takafulfeeTypeCode,
                takafulsetupFees: this.takafulsetupFees,
                takafulsetupUom: this.takafulsetupUom,
                takafulsetupUomCode: this.takafulsetupUomCode,
                takafulchargeFees: this.takafulchargeFees,
                takafulchargeUom: this.takafulchargeUom,
                takafulchargeUomCode: this.takafulchargeUomCode,
                SST2Charge: this.SST2Charge,
                SST2feeTypeCode: this.SST2feeTypeCode,
                SST2setupFees: this.SST2setupFees,
                SST2setupUom: this.SST2setupUom,
                SST2setupUomCode: this.SST2setupUomCode,
                SST2chargeFees: this.SST2chargeFees,
                SST2chargeUom: this.SST2chargeUom,
                SST2chargeUomCode: this.SST2chargeUomCode,
                mintingCharge: this.mintingCharge,
                mintingfeeTypeCode: this.mintingfeeTypeCode,
                mintingsetupFees: this.mintingsetupFees,
                mintingsetupUom: this.mintingsetupUom,
                mintingsetupUomCode: this.mintingsetupUomCode,
                mintingchargeFees: this.mintingchargeFees,
                mintingchargeUom: this.mintingchargeUom,
                mintingchargeUomCode: this.mintingchargeUomCode,
                SST3Charge: this.SST3Charge,
                SST3feeTypeCode: this.SST3feeTypeCode,
                SST3setupFees: this.SST3setupFees,
                SST3setupUom: this.SST3setupUom,
                SST3setupUomCode: this.SST3setupUomCode,
                SST3chargeFees: this.SST3chargeFees,
                SST3chargeUom: this.SST3chargeUom,
                SST3chargeUomCode: this.SST3chargeUomCode,
                unit: this.unit
            }
        });
    };
    __decorate([
        core_1.ViewChild('bgdRedeemConfirmForm')
    ], RedeemBgdDeliveryAddressComponent.prototype, "bgdRedeemConfirmNgForm");
    RedeemBgdDeliveryAddressComponent = __decorate([
        core_1.Component({
            selector: 'bgd-redeem-bgd-delivery-address',
            templateUrl: './redeem-bgd-delivery-address.component.html',
            styleUrls: ['./redeem-bgd-delivery-address.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], RedeemBgdDeliveryAddressComponent);
    return RedeemBgdDeliveryAddressComponent;
}());
exports.RedeemBgdDeliveryAddressComponent = RedeemBgdDeliveryAddressComponent;
