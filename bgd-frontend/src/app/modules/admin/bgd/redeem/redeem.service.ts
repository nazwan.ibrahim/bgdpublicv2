import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RedeemService {

   //Redeem BGD
   acctNumber: any;
   weight: any;
   remarks: any;
   serialNumber: any;
   reference: any;
   name: any;
   phone: any;
   courierCharge: any;
   courierfeeTypeCode: any;
   couriersetupFees: any;
   couriersetupUom: any;
   couriersetupUomCode: any;
   courierchargeFees: any;
   courierchargeUom: any;
   courierchargeUomCode: any;
   SSTCharge: any;
   SSTfeeTypeCode: any;
   SSTsetupFees: any;
   SSTsetupUom: any;
   SSTsetupUomCode: any;
   SSTchargeFees: any;
   SSTchargeUom: any;
   SSTchargeUomCode: any;
   takafulCharge: any;
   takafulfeeTypeCode: any;
   takafulsetupFees: any;
   takafulsetupUom: any;
   takafulsetupUomCode: any;
   takafulchargeFees: any;
   takafulchargeUom: any;
   takafulchargeUomCode: any;
   SST2Charge: any;
   SST2feeTypeCode: any;
   SST2setupFees: any;
   SST2setupUom: any;
   SST2setupUomCode: any;
   SST2chargeFees: any;
   SST2chargeUom: any;
   SST2chargeUomCode: any;
   mintingCharge: any;
   mintingfeeTypeCode: any;
   mintingsetupFees: any;
   mintingsetupUom: any;
   mintingsetupUomCode: any;
   mintingchargeFees: any;
   mintingchargeUom: any;
   mintingchargeUomCode: any;
   SST3Charge: any;
   SST3feeTypeCode: any;
   SST3setupFees: any;
   SST3setupUom: any;
   SST3setupUomCode: any;
   SST3chargeFees: any;
   SST3chargeUom: any;
   SST3chargeUomCode: any;
   unit: any;

   reason: any;

   address1: any;
   address2: any;
   postcode: any;
   town: any;
   state: any;

  constructor() { }
}
