"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RedeemBgdConfirmationModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var shared_module_1 = require("app/shared/shared.module");
var redeem_bgd_confirmation_component_1 = require("app/modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation.component");
var redeem_bgd_confirmation_routing_module_1 = require("app/modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation-routing.module");
var button_1 = require("@angular/material/button");
var input_1 = require("@angular/material/input");
var form_field_1 = require("@angular/material/form-field");
var select_1 = require("@angular/material/select");
var chips_1 = require("@angular/material/chips");
var icon_1 = require("@angular/material/icon");
var alert_1 = require("@bgd/components/alert");
var RedeemBgdConfirmationModule = /** @class */ (function () {
    function RedeemBgdConfirmationModule() {
    }
    RedeemBgdConfirmationModule = __decorate([
        core_1.NgModule({
            declarations: [
                redeem_bgd_confirmation_component_1.RedeemBgdConfirmationComponent,
            ],
            imports: [
                redeem_bgd_confirmation_routing_module_1.RedeemBgdConfirmationRoutingModule,
                button_1.MatButtonModule,
                common_1.CommonModule,
                shared_module_1.SharedModule,
                button_1.MatButtonModule,
                input_1.MatInputModule,
                form_field_1.MatFormFieldModule,
                select_1.MatSelectModule,
                chips_1.MatChipsModule,
                icon_1.MatIconModule,
                alert_1.FuseAlertModule
            ]
        })
    ], RedeemBgdConfirmationModule);
    return RedeemBgdConfirmationModule;
}());
exports.RedeemBgdConfirmationModule = RedeemBgdConfirmationModule;
