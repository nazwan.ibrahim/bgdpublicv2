"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RedeemBgdConfirmationComponent = void 0;
var core_1 = require("@angular/core");
var RedeemBgdConfirmationComponent = /** @class */ (function () {
    function RedeemBgdConfirmationComponent(_activatedRoute, router, _bgdService, _cashWalletService) {
        this._activatedRoute = _activatedRoute;
        this.router = router;
        this._bgdService = _bgdService;
        this._cashWalletService = _cashWalletService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.selectedOption = '';
        this.today = new Date();
        this.showAlert = false;
        // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.reason = this._activatedRoute.snapshot.queryParams['reason'];
        // console.log("Response   ", response);
        console.log("Response", this.reason);
        this.acctNumber = this._activatedRoute.snapshot.queryParams['acctNumber'];
        this.fullName = this._activatedRoute.snapshot.queryParams['fullName'];
        this.phone = this._activatedRoute.snapshot.queryParams['phone'];
        this.address1 = this._activatedRoute.snapshot.queryParams['address1'];
        this.address2 = this._activatedRoute.snapshot.queryParams['address2'];
        this.postcode = this._activatedRoute.snapshot.queryParams['postcode'];
        this.town = this._activatedRoute.snapshot.queryParams['town'];
        this.state = this._activatedRoute.snapshot.queryParams['state'];
        this.remarks = this._activatedRoute.snapshot.queryParams['remarks'];
        this.reference = this._activatedRoute.snapshot.queryParams['reference'];
        this.serialNumber = this._activatedRoute.snapshot.queryParams['serialNumber'];
        this.unit = this._activatedRoute.snapshot.queryParams['unit'];
        this.weight = this._activatedRoute.snapshot.queryParams['weight'];
        this.courierCharge = parseFloat(this._activatedRoute.snapshot.queryParams['courierCharge']);
        this.courierfeeTypeCode = this._activatedRoute.snapshot.queryParams['courierfeeTypeCode'];
        this.couriersetupFees = this._activatedRoute.snapshot.queryParams['couriersetupFees'];
        this.couriersetupUom = this._activatedRoute.snapshot.queryParams['couriersetupUom'];
        this.couriersetupUom = this._activatedRoute.snapshot.queryParams['couriersetupUom'];
        this.courierchargeFees = this._activatedRoute.snapshot.queryParams['courierchargeFees'];
        this.courierchargeUom = this._activatedRoute.snapshot.queryParams['courierchargeUom'];
        this.courierchargeUomCode = this._activatedRoute.snapshot.queryParams['courierchargeUomCode'];
        this.SSTCharge = parseFloat(this._activatedRoute.snapshot.queryParams['SSTCharge']);
        this.SSTfeeTypeCode = this._activatedRoute.snapshot.queryParams['SSTfeeTypeCode'];
        this.SSTsetupFees = this._activatedRoute.snapshot.queryParams['SSTsetupFees'];
        this.SSTsetupUom = this._activatedRoute.snapshot.queryParams['SSTsetupUom'];
        this.SSTsetupUomCode = this._activatedRoute.snapshot.queryParams['SSTsetupUomCode'];
        this.SSTchargeFees = this._activatedRoute.snapshot.queryParams['SSTchargeFees'];
        this.SSTchargeUom = this._activatedRoute.snapshot.queryParams['SSTchargeUom'];
        this.SSTchargeUomCode = this._activatedRoute.snapshot.queryParams['SSTchargeUomCode'];
        this.takafulCharge = parseFloat(this._activatedRoute.snapshot.queryParams['takafulCharge']);
        this.takafulfeeTypeCode = this._activatedRoute.snapshot.queryParams['takafulfeeTypeCode'];
        this.takafulsetupFees = this._activatedRoute.snapshot.queryParams['takafulsetupFees'];
        this.takafulsetupUom = this._activatedRoute.snapshot.queryParams['takafulsetupUom'];
        this.takafulsetupUomCode = this._activatedRoute.snapshot.queryParams['takafulsetupUomCode'];
        this.takafulchargeFees = this._activatedRoute.snapshot.queryParams['takafulchargeFees'];
        this.takafulchargeUom = this._activatedRoute.snapshot.queryParams['takafulchargeUom'];
        this.takafulchargeUomCode = this._activatedRoute.snapshot.queryParams['takafulchargeUomCode'];
        this.SST2Charge = parseFloat(this._activatedRoute.snapshot.queryParams['SST2Charge']);
        this.SST2feeTypeCode = this._activatedRoute.snapshot.queryParams['SST2feeTypeCode'];
        this.SST2setupFees = this._activatedRoute.snapshot.queryParams['SST2setupFees'];
        this.SST2setupUom = this._activatedRoute.snapshot.queryParams['SST2setupUom'];
        this.SST2setupUomCode = this._activatedRoute.snapshot.queryParams['SST2setupUomCode'];
        this.SST2chargeFees = this._activatedRoute.snapshot.queryParams['SST2chargeFees'];
        this.SST2chargeUom = this._activatedRoute.snapshot.queryParams['SST2chargeUom'];
        this.SST2chargeUomCode = this._activatedRoute.snapshot.queryParams['SST2chargeUomCode'];
        this.mintingCharge = parseFloat(this._activatedRoute.snapshot.queryParams['mintingCharge']);
        this.mintingfeeTypeCode = this._activatedRoute.snapshot.queryParams['mintingfeeTypeCode'];
        this.mintingsetupFees = this._activatedRoute.snapshot.queryParams['mintingsetupFees'];
        this.mintingsetupUom = this._activatedRoute.snapshot.queryParams['mintingsetupUom'];
        this.mintingsetupUomCode = this._activatedRoute.snapshot.queryParams['mintingsetupUomCode'];
        this.mintingchargeFees = this._activatedRoute.snapshot.queryParams['mintingchargeFees'];
        this.mintingchargeUom = this._activatedRoute.snapshot.queryParams['mintingchargeUom'];
        this.mintingchargeUomCode = this._activatedRoute.snapshot.queryParams['mintingchargeUomCode'];
        this.SST3Charge = parseFloat(this._activatedRoute.snapshot.queryParams['SST3Charge']);
        this.SST3feeTypeCode = this._activatedRoute.snapshot.queryParams['SST3feeTypeCode'];
        this.SST3setupFees = this._activatedRoute.snapshot.queryParams['SST3setupFees'];
        this.SST3setupUom = this._activatedRoute.snapshot.queryParams['SST3setupUom'];
        this.SST3setupUomCode = this._activatedRoute.snapshot.queryParams['SST3setupUomCode'];
        this.SST3chargeFees = this._activatedRoute.snapshot.queryParams['SST3chargeFees'];
        this.SST3chargeUom = this._activatedRoute.snapshot.queryParams['SST3chargeUom'];
        this.SST3chargeUomCode = this._activatedRoute.snapshot.queryParams['SST3chargeUomCode'];
        this.totalAmountSST = this.SSTCharge + this.SST2Charge + this.SST3Charge;
        this.totalAmount = this.courierCharge + this.takafulCharge + this.mintingCharge + this.totalAmountSST;
    }
    RedeemBgdConfirmationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._cashWalletService.getUserDetails().subscribe(function (data) {
            _this.userID = data.userID;
            _this.fullName = data.fullName;
            console.log(data);
        });
        this._cashWalletService.getWalletDetails().subscribe(function (data) {
            _this.walletBalance = (+data[0].availableAmount).toFixed(2);
        });
    };
    RedeemBgdConfirmationComponent.prototype.redeemConfirm = function () {
        var requestBody = [
            {
                "acctNumber": this.acctNumber,
                "unit": this.unit,
                "weight": this.weight,
                "name": this.fullName,
                "phone": this.phone,
                "address1": this.address1,
                "address2": this.address2,
                "postcode": this.postcode,
                "city": this.town,
                "state": this.state,
                "remarks": this.remarks,
                "reference": this.reference,
                "serialNumber": this.serialNumber,
                "reason": this.reason,
                "feesForms": [
                    {
                        "feeType": "Courier Fee",
                        "feeTypeCode": this.courierfeeTypeCode,
                        "setupFees": this.couriersetupFees,
                        "setupUom": this.couriersetupUom,
                        "setupUomCode": this.couriersetupUomCode,
                        "chargeFees": this.courierchargeFees,
                        "chargeUom": this.courierchargeUom,
                        "chargeUomCode": this.courierchargeUomCode
                    },
                    {
                        "feeType": "SST",
                        "feeTypeCode": this.SSTfeeTypeCode,
                        "setupFees": this.SSTsetupFees,
                        "setupUom": this.SSTsetupUom,
                        "setupUomCode": this.SSTsetupUomCode,
                        "chargeFees": this.SSTchargeFees,
                        "chargeUom": this.SSTchargeUom,
                        "chargeUomCode": this.SSTchargeUomCode
                    },
                    {
                        "feeType": "Takaful Fee",
                        "feeTypeCode": this.takafulfeeTypeCode,
                        "setupFees": this.takafulsetupFees,
                        "setupUom": this.takafulsetupUom,
                        "setupUomCode": this.takafulchargeUomCode,
                        "chargeFees": this.takafulchargeFees,
                        "chargeUom": this.takafulchargeUom,
                        "chargeUomCode": this.takafulchargeUomCode
                    },
                    {
                        "feeType": "SST",
                        "feeTypeCode": this.SST2feeTypeCode,
                        "setupFees": this.SST2setupFees,
                        "setupUom": this.SST2setupUom,
                        "setupUomCode": this.SST2setupUomCode,
                        "chargeFees": this.SST2chargeFees,
                        "chargeUom": this.SST2chargeUom,
                        "chargeUomCode": this.SST2chargeUomCode
                    },
                    {
                        "feeType": "Minting Fee",
                        "feeTypeCode": this.mintingfeeTypeCode,
                        "setupFees": this.mintingsetupFees,
                        "setupUom": this.mintingsetupUom,
                        "setupUomCode": this.mintingsetupUomCode,
                        "chargeFees": this.mintingchargeFees,
                        "chargeUom": this.mintingchargeUom,
                        "chargeUomCode": this.mintingchargeUomCode
                    },
                    {
                        "feeType": "SST",
                        "feeTypeCode": this.SST3feeTypeCode,
                        "setupFees": this.SST3setupFees,
                        "setupUom": this.SST3setupUom,
                        "setupUomCode": this.SST3setupUomCode,
                        "chargeFees": this.SST3chargeFees,
                        "chargeUom": this.SST3chargeUom,
                        "chargeUomCode": this.SST3chargeUomCode
                    }
                ]
            }
        ];
        console.log("Requestbody", requestBody);
        var dataJson = JSON.stringify(requestBody);
        if (this.walletBalance >= this.totalAmount) {
            this.router.navigate(['/redeem-bgd-pin'], {
                queryParams: {
                    response: dataJson,
                    reason: this.reason
                }
            });
        }
        else {
            this.showAlert = true;
            this.alert = {
                type: 'error',
                message: 'Insufficient cash wallet balance.'
            };
        }
        //let dataJson = JSON.stringify(requestBody);
        // this._bgdService.redeemBGD(dataJson)
        //   .subscribe(
        //     Response => {
        //       this.alert = {
        //         type: 'success',
        //         message: 'Success'
        //       };
        //       this.response = Response;
        //       this.router.navigate(['/redeem-bgd-status'],{queryParams: { data: dataJson }});
        //     },
        //     Response => {
        //       this.alert = {
        //         type: 'error',
        //         message: 'Something went wrong, please try again.'
        //       };
        //     }
        //   );
    };
    RedeemBgdConfirmationComponent.prototype.backButton = function () {
        this.router.navigate(['/redeem-bgd-address'], {
            queryParams: {
                // response: JSON.stringify(response),
                reason: this.reason,
                acctNumber: this.acctNumber,
                weight: this.weight,
                remarks: this.remarks,
                serialNumber: this.serialNumber,
                reference: this.reference,
                name: this.fullName,
                phone: this.phone,
                courierCharge: this.courierCharge,
                courierfeeTypeCode: this.courierfeeTypeCode,
                couriersetupFees: this.couriersetupFees,
                couriersetupUom: this.couriersetupUom,
                couriersetupUomCode: this.couriersetupUomCode,
                courierchargeFees: this.courierchargeFees,
                courierchargeUom: this.courierchargeUom,
                courierchargeUomCode: this.courierchargeUomCode,
                SSTCharge: this.SSTCharge,
                SSTfeeTypeCode: this.SSTfeeTypeCode,
                SSTsetupFees: this.SSTsetupFees,
                SSTsetupUom: this.SSTsetupUom,
                SSTsetupUomCode: this.SSTsetupUomCode,
                SSTchargeFees: this.SSTchargeFees,
                SSTchargeUom: this.SSTchargeUom,
                SSTchargeUomCode: this.SSTchargeUomCode,
                takafulCharge: this.takafulCharge,
                takafulfeeTypeCode: this.takafulfeeTypeCode,
                takafulsetupFees: this.takafulsetupFees,
                takafulsetupUom: this.takafulsetupUom,
                takafulsetupUomCode: this.takafulsetupUomCode,
                takafulchargeFees: this.takafulchargeFees,
                takafulchargeUom: this.takafulchargeUom,
                takafulchargeUomCode: this.takafulchargeUomCode,
                SST2Charge: this.SST2Charge,
                SST2feeTypeCode: this.SST2feeTypeCode,
                SST2setupFees: this.SST2setupFees,
                SST2setupUom: this.SST2setupUom,
                SST2setupUomCode: this.SST2setupUomCode,
                SST2chargeFees: this.SST2chargeFees,
                SST2chargeUom: this.SST2chargeUom,
                SST2chargeUomCode: this.SST2chargeUomCode,
                mintingCharge: this.mintingCharge,
                mintingfeeTypeCode: this.mintingfeeTypeCode,
                mintingsetupFees: this.mintingsetupFees,
                mintingsetupUom: this.mintingsetupUom,
                mintingsetupUomCode: this.mintingsetupUomCode,
                mintingchargeFees: this.mintingchargeFees,
                mintingchargeUom: this.mintingchargeUom,
                mintingchargeUomCode: this.mintingchargeUomCode,
                SST3Charge: this.SST3Charge,
                SST3feeTypeCode: this.SST3feeTypeCode,
                SST3setupFees: this.SST3setupFees,
                SST3setupUom: this.SST3setupUom,
                SST3setupUomCode: this.SST3setupUomCode,
                SST3chargeFees: this.SST3chargeFees,
                SST3chargeUom: this.SST3chargeUom,
                SST3chargeUomCode: this.SST3chargeUomCode,
                unit: this.unit,
                address: this.address1,
                address2: this.address2,
                postcode: this.postcode,
                town: this.town,
                state: this.state
            }
        });
    };
    RedeemBgdConfirmationComponent = __decorate([
        core_1.Component({
            selector: 'bgd-redeem-bgd-confirmation',
            templateUrl: './redeem-bgd-confirmation.component.html',
            styleUrls: ['./redeem-bgd-confirmation.component.scss']
        })
    ], RedeemBgdConfirmationComponent);
    return RedeemBgdConfirmationComponent;
}());
exports.RedeemBgdConfirmationComponent = RedeemBgdConfirmationComponent;
