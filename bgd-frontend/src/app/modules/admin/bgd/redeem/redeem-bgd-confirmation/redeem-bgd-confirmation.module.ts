import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RedeemBgdConfirmationComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation.component';
import { RedeemBgdConfirmationRoutingModule } from 'app/modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@bgd/components/alert';

@NgModule({
    declarations: [
        RedeemBgdConfirmationComponent,

    ],
    imports: [
        RedeemBgdConfirmationRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        FuseAlertModule

    ]
})
export class RedeemBgdConfirmationModule {
}
