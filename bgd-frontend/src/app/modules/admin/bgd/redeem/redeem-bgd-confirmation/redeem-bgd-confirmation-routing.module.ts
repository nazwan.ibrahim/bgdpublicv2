import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeemBgdConfirmationComponent } from 'app/modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation.component';

const routes: Routes = [
  {
    path: '',
    component: RedeemBgdConfirmationComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedeemBgdConfirmationRoutingModule { }
