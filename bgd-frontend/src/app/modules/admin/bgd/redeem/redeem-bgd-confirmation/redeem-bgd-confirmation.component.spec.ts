import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemBgdConfirmationComponent } from './redeem-bgd-confirmation.component';

describe('RedeemBgdConfirmationComponent', () => {
  let component: RedeemBgdConfirmationComponent;
  let fixture: ComponentFixture<RedeemBgdConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemBgdConfirmationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RedeemBgdConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
