import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferBgdAmountComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-amount/transfer-bgd-amount.component';

const routes: Routes = [
    {
        path: '',
        component: TransferBgdAmountComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransferBgdAmountRoutingModule { }
