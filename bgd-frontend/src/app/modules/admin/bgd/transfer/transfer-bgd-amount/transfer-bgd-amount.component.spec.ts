import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferBgdAmountComponent } from './transfer-bgd-amount.component';

describe('TransferBgdAmountComponent', () => {
  let component: TransferBgdAmountComponent;
  let fixture: ComponentFixture<TransferBgdAmountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferBgdAmountComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransferBgdAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
