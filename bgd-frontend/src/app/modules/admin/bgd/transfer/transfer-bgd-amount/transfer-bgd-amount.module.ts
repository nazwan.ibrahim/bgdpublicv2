import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { TransferBgdAmountComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-amount/transfer-bgd-amount.component';
import { TransferBgdAmountRoutingModule } from 'app/modules/admin/bgd/transfer/transfer-bgd-amount/transfer-bgd-amount-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        TransferBgdAmountComponent,

    ],
    imports: [
        TransferBgdAmountRoutingModule,
        MatButtonModule,
        FormsModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        FuseCardModule,
        FuseAlertModule,
        MatIconModule
    ]
})
export class TransferBgdAmountModule {
}
