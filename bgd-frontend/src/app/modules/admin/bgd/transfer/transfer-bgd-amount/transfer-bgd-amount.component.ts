import { Component, OnInit, ViewChild, ViewEncapsulation, SimpleChanges } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, map } from 'rxjs/operators';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { TransferService } from '../transfer.service';
import { Location } from '@angular/common';


@Component({
  selector: 'bgd-transfer-bgd-amount',
  templateUrl: './transfer-bgd-amount.component.html',
  styleUrls: ['./transfer-bgd-amount.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class TransferBgdAmountComponent {

  @ViewChild('bgdTransferAmountNgForm') bgdTransferAmountNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  bgdTransferAmountForm: UntypedFormGroup;
  showAlert = false;
  transcAccNum: any = '';
  transferReason: any = '';
  amountValue: any = '';
  amountGram: any = '';
  ownAccNum: any = '';
  tempValue: any;
  tempValue2: any;
  totalFees: any;
  netAmount: any;
  availAmount: any;
  inputPrice: any;
  inputGram: any;
  inputType: any;
  labelAlertVisible: boolean = false;
  buyPriceGold: number = 0;
  statusCode = 'G05';
  unitPrice: any;
  setupTF: any;
  setupSST: any;
  chargeTF: any;
  chargeSST: any;
  response: any;
  disableButton: boolean = false;
  reference: any;
  priceRequestId: any;

  /**
     * Constructor
     */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _bgdService: bgdService,
    private _formBuilder: UntypedFormBuilder,
    private _dashboardService: DashboardService,
    private transferService: TransferService,
    private location: Location
  ) {}

  ngOnInit(): void {

    this.ownAccNum = this.transferService.ownAccNum;
    this.availAmount = this.transferService.availAmount;
    this.transcAccNum = this.transferService.transAccNum;
    this.transferReason = this.transferService.reason;

    this.bgdTransferAmountForm = this._formBuilder.group({
      amountValue: ['', [Validators.required, this.amountValueValidator()]],
    });

    this.bgdTransferAmountForm.get('amountValue').valueChanges.subscribe((value) => {
      if (value < this.availAmount) {
        this.labelAlertVisible = false;
      }
    });

    this._dashboardService.getQuerySellPrice().pipe(
    ).subscribe(
      (message) => {

        console.log(message);
        this.buyPriceGold = (message['A02'].price).toFixed(2);
        this.priceRequestId = message.priceRequestID;
        this.reference = message.reference;
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  amountValueValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const value = control.value;

      if (value > this.availAmount) {
        this.labelAlertVisible = true;
        return { invalidAmount: true };
      }

      this.labelAlertVisible = false;
      return null; 
    };
  }

  back(): void {
    this.location.back();
  }

  nextBtn() {

    this.disableButton = true;

    const acctNumberSender = this.ownAccNum;
    const acctNumberReceiver = this.transcAccNum;
    const amount = this.bgdTransferAmountForm.get('amountValue').value;
    const statusCode = 'G05';
    const reason = this.transferReason;

    this._bgdService.initTransfer(acctNumberSender, acctNumberReceiver, amount, statusCode, reason)
        .pipe(
          finalize(() => {
            this.bgdTransferAmountForm.enable();

            this.bgdTransferAmountForm.reset();
          })
        )
        .subscribe(
          Response => {

            console.log("initTransfer Response:", Response);

            this.response = Response;
            this.transferService.initResponse = this.response;

            this._router.navigate(['/transfer-bgd-confirmation']);

          },
          error => {

            this.disableButton = false;

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };
          }
    );
  }


}
