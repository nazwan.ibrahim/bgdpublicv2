"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TransferBgdAmountComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var animations_1 = require("@bgd/animations");
var operators_1 = require("rxjs/operators");
var TransferBgdAmountComponent = /** @class */ (function () {
    /**
       * Constructor
       */
    function TransferBgdAmountComponent(_activatedRoute, _router, _bgdService, _formBuilder, bgdTransferService) {
        var _this = this;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._bgdService = _bgdService;
        this._formBuilder = _formBuilder;
        this.bgdTransferService = bgdTransferService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.recAccNum = '';
        this.transferReason = '';
        this.amountValue = '';
        this.curAccNum = '';
        this.statusCode = 'G05';
        this.disableButton = false;
        {
            this._activatedRoute.paramMap.subscribe(function (params) {
                _this.recAccNum = params.get('acctNumber');
                _this.transferReason = params.get('transferReason');
            });
            this.recAccNum = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('acctNumber'));
            this.transferReason = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('transferReason'));
        }
    }
    TransferBgdAmountComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Create the form
        this.bgdTransferAmountForm = this._formBuilder.group({
            amountValue: ['', [forms_1.Validators.required]]
        });
        this._bgdService.walletDetails().pipe(operators_1.map(function (value) {
            _this.tempValue = value;
            _this.curAccNum = null;
            _this.availAmount = null;
            for (var i = 0; i < _this.tempValue.length; i++) {
                if (_this.tempValue[i].type == "Gold") {
                    _this.curAccNum = _this.tempValue[i].acctNumber;
                    _this.availAmount = _this.tempValue[i].availableAmount;
                    break;
                }
            }
            return { curAccNum: _this.curAccNum, availAmount: _this.availAmount };
        })).subscribe(function (result) {
            _this.curAccNum = result.curAccNum;
            _this.availAmount = result.availAmount;
        });
    };
    TransferBgdAmountComponent.prototype.backButton = function () {
        this._router.navigate(['/transfer-bgd']);
    };
    TransferBgdAmountComponent.prototype.nextButton = function () {
        var _this = this;
        this.disableButton = true;
        var acctNumberSender = this.curAccNum;
        var acctNumberReceiver = this.recAccNum;
        var amount = this.bgdTransferAmountForm.get('amountValue').value;
        var statusCode = 'G05';
        var reason = this.transferReason;
        var availAmount = this.availAmount;
        if (availAmount >= amount) {
            this._bgdService.initTransfer(acctNumberSender, acctNumberReceiver, amount, statusCode, reason)
                .pipe(rxjs_1.finalize(function () {
                _this.bgdTransferAmountForm.enable();
                _this.bgdTransferAmountForm.reset();
            }))
                .subscribe(function (Response) {
                console.log(Response);
                _this.response = Response;
                _this._router.navigate(['/transfer-bgd-confirmation'], { queryParams: { response: JSON.stringify(_this.response) } });
            }, function (Response) {
                _this.disableButton = false;
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
            });
        }
        else {
            this.disableButton = false;
            this.alert = {
                type: 'error',
                message: 'Transfer cannot be completed. Available gold is less than the requested transfer amount.'
            };
            this.showAlert = true;
        }
        // const setupTF = this.setupTF;
        // const setupSST = this.setupSST;
        // const chargeTF = this.chargeTF;
        // const chargeSST = this.chargeSST;
        //console.log(amount);
        //   if (availAmount >= amount) {
        //     this._bgdService.initTransfer(acctNumberSender, acctNumberReceiver, amount, statusCode, reason)
        //       .subscribe((response: any) => {
        //         console.log(response);
        //         // const feesForm = response[0].feesForms;
        //         // const bursaSellPrice = response[0].bursaSellPrice;
        //         // const setupTF = feesForm[0].setupFees;
        //         // const setupSST = feesForm[1].setupFees;
        //         // const chargeTF = feesForm[0].chargeFees;
        //         // const chargeSST = feesForm[1].chargeFees;
        //         // const receiverName = response.fullName;
        //         // console.log(acctNumberReceiver);
        //         // console.log(acctNumberSender);
        //         // console.log(setupTF);
        //         // console.log(setupSST);
        //         // console.log(chargeTF);
        //         // console.log(chargeTF);
        //         // console.log(receiverName);
        //         this.response = response;
        //         this._router.navigate(['/transfer-bgd-confirmation'], {
        //           queryParams: {
        //             response: JSON.stringify(this.response)
        //             // acctNumber: encodeURIComponent(acctNumberReceiver),
        //             // receiverName: encodeURIComponent(receiverName),
        //             // setupTF: encodeURIComponent(setupTF),
        //             // setupSST: encodeURIComponent(setupSST),
        //             // chargeTF: encodeURIComponent(chargeTF),
        //             // chargeSST: encodeURIComponent(chargeSST),
        //             // userAcc: encodeURIComponent(acctNumberSender),
        //             // transferReason: encodeURIComponent(reason),
        //             // amountValue: encodeURIComponent(amount),
        //             // unitPrice: encodeURIComponent(bursaSellPrice)
        //           }
        //         });
        //       });
        //   } else {
        //     this.alert = {
        //       type: 'error',
        //       message: 'Transfer cannot be completed. Available gold is less than the requested transfer amount.'
        //     };
        //     this.showAlert = true;
        //   }
    };
    __decorate([
        core_1.ViewChild('bgdTransferAmountNgForm')
    ], TransferBgdAmountComponent.prototype, "bgdTransferAmountNgForm");
    TransferBgdAmountComponent = __decorate([
        core_1.Component({
            selector: 'bgd-transfer-bgd-amount',
            templateUrl: './transfer-bgd-amount.component.html',
            styleUrls: ['./transfer-bgd-amount.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], TransferBgdAmountComponent);
    return TransferBgdAmountComponent;
}());
exports.TransferBgdAmountComponent = TransferBgdAmountComponent;
