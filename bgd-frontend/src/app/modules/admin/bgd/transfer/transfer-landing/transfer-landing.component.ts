import { Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { switchMap, map, tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { of } from 'rxjs';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-transfer-landing',
  templateUrl: './transfer-landing.component.html',
  styleUrls: ['./transfer-landing.component.scss']
})
export class TransferLandingComponent {
  acctNumber: any;
  walletModel: any;
  goldWalletIndex: number;
  costModel: any;
  bursaCostIndex: number;
  setupFees: any;
  setupUom: any;

  constructor(
    private _bgdService: bgdService,
    private cashWalletService: CashWalletService,
    private router: Router,
    private location: Location
  ) { }

  ngOnInit(){
    this._bgdService.walletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Gold") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._bgdService.walletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }
      this.acctNumber = data[this.goldWalletIndex].acctNumber;

    });

    this.cashWalletService.getbursaRevenue().pipe(
      map((value: any) => {
        this.costModel = value;

        for (var i = 0; i < this.costModel.length; i++) {
          if (this.costModel[i].feeTypeCode == "C04") {
            this.bursaCostIndex = i;
            break;
          }
        }

        return this.costModel;
      })
    ).subscribe((data: any) => {

      this.setupFees = data[this.bursaCostIndex].setupFees;
      this.setupUom = data[this.bursaCostIndex].setupUom;

    });
  }

  copyToClipboard() {
    const inputElement = document.getElementById('form-input') as HTMLInputElement;
    inputElement.select();
    document.execCommand('copy');
  }  
  

  transferNow(){
    this.router.navigate(['/transfer-bgd']);
  }

  back(){
    this.location.back();
  }
}
