import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferLandingComponent } from 'app/modules/admin/bgd/transfer/transfer-landing/transfer-landing.component';

const routes: Routes = [
  {
    path: '',
    component: TransferLandingComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransferLandingRoutingModule { }
