import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { TransferLandingComponent } from 'app/modules/admin/bgd/transfer/transfer-landing/transfer-landing.component';
import { TransferLandingRoutingModule } from 'app/modules/admin/bgd/transfer/transfer-landing/transfer-landing-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
    declarations: [
        TransferLandingComponent,

    ],
    imports: [
        TransferLandingRoutingModule,
        MatButtonModule,
        CommonModule,
        //AuthService,
        SharedModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        MatIconModule
    ]
})
export class TransferLandingModule {
}
