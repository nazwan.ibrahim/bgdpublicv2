import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TransferService {

  ownAccNum: any;
  availAmount: any;
  availCashAmount: any;
  reason: any;
  transAccNum: any;

  initResponse: any;
  transferResponse: any;

  constructor() { }
}
