import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferBgdComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd/transfer-bgd.component';

const routes: Routes = [
  {
    path: '',
    component: TransferBgdComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransferBgdRoutingModule { }
