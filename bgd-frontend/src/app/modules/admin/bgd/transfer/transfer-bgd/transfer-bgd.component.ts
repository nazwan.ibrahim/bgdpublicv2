import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, NgForm, ValidatorFn, Validators } from '@angular/forms';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { catchError, of, tap } from 'rxjs';
import { TransferService } from '../transfer.service';
import { Location } from '@angular/common';

@Component({
  selector: 'transfer-bgd',
  templateUrl: './transfer-bgd.component.html',
  styleUrls: ['./transfer-bgd.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class TransferBgdComponent implements OnInit {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  @ViewChild('bgdTransferNgForm') bgdTransferNgForm: NgForm;

  bgdTransferForm: FormGroup;
  accNum: string = '';
  availAmount: any;
  userID: any;
  disableButton: boolean = true;
  showAlert = false;
  walletModel: any;
  goldWalletIndex: number;
  acctNum: any;
  labelAlertVisible: boolean;

  constructor(
    private _router: Router,
    private _bgdService: bgdService,
    private _formBuilder: FormBuilder,
    private transferService: TransferService,
    private location: Location
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------


  ngOnInit(): void {

    this._bgdService.walletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Gold") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._bgdService.walletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }
      
      this.acctNum = data[this.goldWalletIndex].acctNumber;
      this.transferService.ownAccNum = this.acctNum;

      this.availAmount = data[this.goldWalletIndex].availableAmount;
      this.transferService.availAmount = this.availAmount;

    });

    this.bgdTransferForm = this._formBuilder.group({
      accNum: ['', [Validators.required, this.accNumValidator()]],
      transferReason: ['', Validators.required],
    });

    this.bgdTransferForm.valueChanges.subscribe(() => {
      this.disableButton = !this.bgdTransferForm.valid;
    });

  }

  accNumValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const value = control.value;

      if (value == this.acctNum) {

        return { invalidAccNum: true };
      }

      return null; 
    };
  }

  back(): void {
    this.location.back();
  }

  nextBtn() {

    this.disableButton = true;
    
    const transferReason = this.bgdTransferForm.get('transferReason').value;
    this.transferService.reason = transferReason;

    const transAccNum = this.bgdTransferForm.get('accNum').value;
    this.transferService.transAccNum = transAccNum;

    this._bgdService.validateAccount(transAccNum).subscribe(
        (response) => {

          console.log("Validate Account:", response);

          this._router.navigate(['/transfer-bgd-amount']);

        },
        (error) => {

          this.disableButton = false;

          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };

          this.showAlert = true;
        }
    );

  }
}
