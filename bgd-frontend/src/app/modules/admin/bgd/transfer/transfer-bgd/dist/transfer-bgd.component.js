"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TransferBgdComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var animations_1 = require("@bgd/animations");
var TransferBgdComponent = /** @class */ (function () {
    function TransferBgdComponent(_activatedRoute, _router, _bgdService, _formBuilder, cashWalletService, dashboardService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._bgdService = _bgdService;
        this._formBuilder = _formBuilder;
        this.cashWalletService = cashWalletService;
        this.dashboardService = dashboardService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.accNum = '';
        this.isButtonDisabled = true;
        this.showAlert = false;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    TransferBgdComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Create the form
        this.bgdTransferForm = this._formBuilder.group({
            accNum: ['', [forms_1.Validators.required]],
            transferReason: ['', forms_1.Validators.required]
        });
        this.bgdTransferForm.valueChanges.subscribe(function () {
            _this.isButtonDisabled = !_this.bgdTransferForm.valid;
        });
    };
    TransferBgdComponent.prototype.backButton = function () {
        this._router.navigate(['/transfer-landing']);
    };
    TransferBgdComponent.prototype.continueButton = function () {
        var _this = this;
        this.isButtonDisabled = true;
        var transferReason = this.bgdTransferForm.get('transferReason').value;
        var acctNumber = this.bgdTransferForm.get('accNum').value;
        console.log('Transfer Reason:', transferReason);
        console.log('Account Number:', acctNumber);
        this._bgdService.validateAccount(acctNumber).subscribe(function (response) {
            console.log(response);
            _this._router.navigate(['/transfer-bgd-amount'], {
                queryParams: {
                    acctNumber: encodeURIComponent(acctNumber),
                    transferReason: encodeURIComponent(transferReason)
                }
            });
        }, function (error) {
            _this.isButtonDisabled = false;
            _this.alert = {
                type: 'error',
                message: 'Account number is invalid.'
            };
            _this.showAlert = true;
        });
    };
    __decorate([
        core_1.ViewChild('bgdTransferNgForm')
    ], TransferBgdComponent.prototype, "bgdTransferNgForm");
    TransferBgdComponent = __decorate([
        core_1.Component({
            selector: 'transfer-bgd',
            templateUrl: './transfer-bgd.component.html',
            styleUrls: ['./transfer-bgd.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], TransferBgdComponent);
    return TransferBgdComponent;
}());
exports.TransferBgdComponent = TransferBgdComponent;
