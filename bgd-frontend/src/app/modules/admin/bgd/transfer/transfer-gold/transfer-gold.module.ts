import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { TransferGoldComponent } from 'app/modules/admin/bgd/transfer/transfer-gold/transfer-gold.component';
import { TransferGoldRoutingModule } from 'app/modules/admin/bgd/transfer/transfer-gold/transfer-gold-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';

@NgModule({
    declarations: [
        TransferGoldComponent,

    ],
    imports: [
        TransferGoldRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
    ]
})
export class TransferGoldModule {
}
