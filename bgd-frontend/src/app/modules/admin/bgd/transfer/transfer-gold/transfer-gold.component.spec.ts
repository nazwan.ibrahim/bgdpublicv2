import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferGoldComponent } from './transfer-gold.component';

describe('TransferGoldComponent', () => {
  let component: TransferGoldComponent;
  let fixture: ComponentFixture<TransferGoldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferGoldComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransferGoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
