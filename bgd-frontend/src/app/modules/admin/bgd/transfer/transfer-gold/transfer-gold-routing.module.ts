import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferGoldComponent } from 'app/modules/admin/bgd/transfer/transfer-gold/transfer-gold.component';

const routes: Routes = [
    {
        path: '',
        component: TransferGoldComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransferGoldRoutingModule { }
