import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { TransferService } from '../transfer.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-transfer-bgd-pin',
  templateUrl: './transfer-bgd-pin.component.html',
  styleUrls: ['./transfer-bgd-pin.component.scss'],
  animations: [
    trigger('shake', [
      // Animation configuration
    ])
  ]

})
export class TransferBgdPinComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  transferReason: any = '';
  amountValue: any = '';
  email: any;
  username: any;
  chargeTF: any = '';
  chargeSST: any = '';
  setupTF: any = '';
  setupSST: any = '';
  fullName: any = '';
  curAccNum: any = '';
  recAccNum: any = '';
  unitPrice: any;
  totalAmount: any;
  response: any;
  showAlert = false;
  bgdTransferPinForm: UntypedFormGroup;
  pin: any;
  receiverName: any;
  disableButton: boolean = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _bgdService: bgdService,
    private user: DashboardService,
    private transferService: TransferService,
    private location: Location
  ) {}

  ngOnInit(): void {

    this.response = this.transferService.initResponse;

    this.user.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    })

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  back(): void {
    this.location.back();
  }

  confirmTransfer() {
    
    const requestBody = this.response;

    this._bgdService.transferWallet(requestBody)
      .subscribe(
        Response => {
          console.log("confirmTransfer Response:", Response);

          const response = Response;
          this.transferService.transferResponse = response;

          this._router.navigate(['/transfer-bgd-status']);

        },
        error => {
          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };
        }
      );
  }

  verifyPin() {

    this.disableButton = true;

    const pin = {
      userName: this.username,
      email: this.email,
      userPin: this.pin
    }

    console.log(pin);

    this._bgdService.verifyPin(pin)
      .subscribe(
        Response => {

          this.confirmTransfer();

        },
        error => {

          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };

          this.disableButton = false;

        }
      );
  }


  forgotPin() {
    this._router.navigate(['/forgot-pin']);
  }

}
