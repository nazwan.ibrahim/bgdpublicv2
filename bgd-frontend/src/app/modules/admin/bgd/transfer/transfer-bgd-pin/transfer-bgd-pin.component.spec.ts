import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferBgdPinComponent } from './transfer-bgd-pin.component';

describe('TransferBgdPinComponent', () => {
  let component: TransferBgdPinComponent;
  let fixture: ComponentFixture<TransferBgdPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferBgdPinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransferBgdPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
