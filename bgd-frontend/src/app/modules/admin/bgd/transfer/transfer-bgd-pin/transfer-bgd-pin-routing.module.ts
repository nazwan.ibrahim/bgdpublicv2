import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferBgdPinComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-pin/transfer-bgd-pin.component';

const routes: Routes = [
    {
        path: '',
        component: TransferBgdPinComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransferBgdPinRoutingModule { }
