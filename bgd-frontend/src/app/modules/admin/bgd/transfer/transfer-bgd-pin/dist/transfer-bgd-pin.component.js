"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TransferBgdPinComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@angular/animations");
var TransferBgdPinComponent = /** @class */ (function () {
    function TransferBgdPinComponent(_activatedRoute, _router, _bgdService, user, _httpClient) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._bgdService = _bgdService;
        this.user = user;
        this._httpClient = _httpClient;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.transferReason = '';
        this.amountValue = '';
        this.chargeTF = '';
        this.chargeSST = '';
        this.setupTF = '';
        this.setupSST = '';
        this.fullName = '';
        this.curAccNum = '';
        this.recAccNum = '';
        this.showAlert = false;
        this.disableButton = false;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log(response);
        this.curAccNum = response[0].acctNumberSender;
        this.recAccNum = response[0].acctNumberReceiver;
        this.receiverName = response[0].receiverProfile.fullName;
        this.setupTF = response[0].feesForms[0].setupFees;
        this.setupSST = response[0].feesForms[1].setupFees;
        this.chargeTF = response[0].feesForms[0].chargeFees;
        this.chargeSST = response[0].feesForms[1].chargeFees;
        this.transferReason = response[0].reason;
        this.amountValue = response[0].amount;
        this.unitPrice = response[0].bursaSellPrice;
        this.totalAmount = this.chargeTF + this.chargeSST;
        // this._activatedRoute.paramMap.subscribe(params => {
        //   this.curAccNum = params.get('userAcc');
        //   this.recAccNum = params.get('accNum'); 
        //   this.receiverName = params.get('receiverName');
        //   this.setupTF = params.get('setupTF');
        //   this.setupSST = params.get('setupSST');
        //   this.chargeTF = params.get('chargeTF');
        //   this.chargeSST = params.get('chargeSST');
        //   this.transferReason = params.get('transferReason');
        //   this.amountValue = params.get('amountValue');
        //   this.unitPrice = params.get('unitPrice')
        // })
        // this.curAccNum = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userAcc'));
        // this.setupTF = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('setupTF'));
        // this.setupSST = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('setupSST'));
        // this.chargeTF = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('chargeTF'));
        // this.chargeSST = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('chargeSST'));
        // this.recAccNum = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('accNum'));
        // this.transferReason = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('transferReason'));
        // this.receiverName = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('receiverName'));
        // this.amountValue = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('amountValue'));
        // this.unitPrice = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('unitPrice'));
        // this.totalAmount = this.chargeTF + this.chargeSST;
    }
    TransferBgdPinComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user.getUserDetails().subscribe(function (data) {
            _this.email = data.email;
            _this.username = data.userName;
        });
    };
    TransferBgdPinComponent.prototype.pincodeCompleted = function (pin) {
        this.pin = pin;
    };
    TransferBgdPinComponent.prototype.backButton = function () {
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this._router.navigate(['/transfer-bgd-confirmation'], {
            queryParams: {
                response: JSON.stringify(response)
            }
        });
    };
    TransferBgdPinComponent.prototype.confirmTransfer = function () {
        var _this = this;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this._bgdService.transferWallet(response)
            .subscribe(function (Response) {
            console.log(Response);
            var queryParams = {
                Response: JSON.stringify(Response),
                response: JSON.stringify(response)
            };
            setTimeout(function () {
                _this._router.navigate(['/transfer-bgd-status'], { queryParams: queryParams });
            }, 1000); // 3000 milliseconds = 3 seconds
        }, function (Response) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
        // console.log(this.amountValue);
        // this._router.navigate(['/transfer-bgd-status'], {
        //   queryParams: {
        //     queryParams: { response: JSON.stringify(response) }
        //   }
        // });
    };
    // const acctNumberSender = this.curAccNum;
    // const acctNumberReceiver = this.recAccNum;
    // const amount = this.amountValue;
    // const statusCode = 'G05';
    // const setupTF = this.setupTF;
    // const setupSST = this.setupSST;
    // const chargeTF = this.chargeTF;
    // const chargeSST = this.chargeSST;
    // const reason = this.transferReason;
    // const unitPrice = this.unitPrice;
    // const receiverName = this.receiverName;
    // [
    //   {
    //     "acctNumberSender": this.curAccNum,
    //     "acctNumberReceiver": this.recAccNum,
    //     "amount": this.amountValue,
    //     "statusCode": "G05",
    //     "reason": this.transferReason
    //   }
    // ]
    // this._router.navigate(['/transfer-bgd-status'], {
    //   queryParams: {
    //     accNum: encodeURIComponent(acctNumberReceiver),
    //     setupTF: encodeURIComponent(setupTF),
    //     setupSST: encodeURIComponent(setupSST),
    //     chargeTF: encodeURIComponent(chargeTF),
    //     chargeSST: encodeURIComponent(chargeSST),
    //     userAcc: encodeURIComponent(acctNumberSender),
    //     transferReason: encodeURIComponent(reason),
    //     amountValue: encodeURIComponent(amount),
    //     unitPrice: encodeURIComponent(unitPrice),
    //     receiverName: encodeURIComponent(receiverName)
    //   }
    // })
    TransferBgdPinComponent.prototype.verifyPin = function () {
        var _this = this;
        this.disableButton = true;
        var pin = {
            userName: this.username,
            email: this.email,
            userPin: this.pin
        };
        console.log(pin);
        this._bgdService.verifyPin(pin)
            .subscribe(function (Response) {
            _this.alert = {
                type: 'success',
                message: 'Verified'
            };
            _this.confirmTransfer();
        }, function (error) {
            _this.disableButton = false;
            _this.alert = {
                type: 'error',
                message: 'Incorrect PIN.'
            };
            _this.showAlert = true;
        });
        this.showAlert = true;
    };
    /* nextButton() {
      const acctNumberSender = this.curAccNum;
      const acctNumberReceiver = this.recAccNum;
      const amount = this.amountValue;
      const statusCode = 'G05';
      const setupFee = this.setupFees;
      const chargeFee = this.chargeFees;
      const reason = this.transferReason;
  
  
      console.log(this.amountValue);
      this._router.navigate(['/transfer-bgd-status'], {
        queryParams: {
          accNum: encodeURIComponent(acctNumberReceiver),
          setupFee: encodeURIComponent(setupFee),
          chargeFee: encodeURIComponent(chargeFee),
          userAcc: encodeURIComponent(acctNumberSender),
          transferReason: encodeURIComponent(reason),
          amountValue: encodeURIComponent(amount)
        }
      });
    } */
    TransferBgdPinComponent.prototype.forgotPin = function () {
        this._router.navigate(['/forgot-pin']);
    };
    TransferBgdPinComponent = __decorate([
        core_1.Component({
            selector: 'bgd-transfer-bgd-pin',
            templateUrl: './transfer-bgd-pin.component.html',
            styleUrls: ['./transfer-bgd-pin.component.scss'],
            animations: [
                animations_1.trigger('shake', [
                // Animation configuration
                ])
            ]
        })
    ], TransferBgdPinComponent);
    return TransferBgdPinComponent;
}());
exports.TransferBgdPinComponent = TransferBgdPinComponent;
