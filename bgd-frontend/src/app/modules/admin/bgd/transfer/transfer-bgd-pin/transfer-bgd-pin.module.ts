import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { TransferBgdPinComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-pin/transfer-bgd-pin.component';
import { TransferBgdPinRoutingModule } from 'app/modules/admin/bgd/transfer/transfer-bgd-pin/transfer-bgd-pin-routing.module';
import { MatChipsModule } from '@angular/material/chips';
import { NgxPincodeModule } from 'ngx-pincode';

@NgModule({
    declarations: [
        TransferBgdPinComponent,

    ],
    imports: [
        TransferBgdPinRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatRadioModule,
        MatStepperModule,
        MatSelectModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule,
        MatDividerModule,
        MatListModule,
        NgxPincodeModule
    ]
})
export class TransferBgdPinModule {
}
