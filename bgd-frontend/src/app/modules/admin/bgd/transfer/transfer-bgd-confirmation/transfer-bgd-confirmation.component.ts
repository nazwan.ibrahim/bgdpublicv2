import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { HttpClient } from '@angular/common/http';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { Location } from '@angular/common';
import { TransferService } from '../transfer.service';
import { catchError, of, tap } from 'rxjs';


@Component({
  selector: 'bgd-transfer-bgd-confirmation',
  templateUrl: './transfer-bgd-confirmation.component.html',
  styleUrls: ['./transfer-bgd-confirmation.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class TransferBgdConfirmationComponent {

  availAmount: number;
  checkboxChecked: boolean = false;
  disableButton: boolean = false;
  bursaSellPrice: any;
  transferReason: any = '';
  amountValue: any = '';
  chargeTF: any = '';
  chargeSST: any = '';
  setupTF: any = '';
  setupSST: any = '';
  fullName: any = '';
  ownAccNum: any = '';
  transAccNum: any = '';
  receiverNum: any = '';
  transReason: any = '';
  totalAmount: any;
  unitPrice: any;
  userID: any;
  alert: any = null;
  isChecked = false;
  today: Date = new Date();
  receiverName: any = '';
  walletModel: any;
  cashWalletIndex: number;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private transferService: TransferService,
    private location: Location,
    private _bgdService: bgdService
  ) {}

  ngOnInit(): void {

    const data = this.transferService.initResponse;
    this.ownAccNum = data[0].acctNumberSender;
    this.transAccNum = data[0].acctNumberReceiver;
    this.receiverName = data[0].receiverProfile.fullName;
    this.setupTF = data[0].feesForms[0].setupFees;
    this.setupSST = data[0].feesForms[1].setupFees;
    this.chargeTF = data[0].feesForms[0].chargeFees;
    this.chargeSST = data[0].feesForms[1].chargeFees;
    this.transferReason = data[0].reason;
    this.amountValue = data[0].amount;
    this.unitPrice = data[0].bursaSellPrice;

    this.totalAmount = this.chargeTF + this.chargeSST;

    this._bgdService.walletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Cash") {
            this.cashWalletIndex = i;
            break;
          }
        }
        return this._bgdService.walletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }

      this.availAmount = data[this.cashWalletIndex].availableAmount;
      this.transferService.availCashAmount = this.availAmount;

    });

  }

  toggleCheckbox(): void {
    this.checkboxChecked = !this.checkboxChecked;
  }

  nextButton(): void {

    this.disableButton = true;

    if (this.checkboxChecked) {
      this._router.navigate(['/transfer-bgd-pin']);
    }

    this.disableButton = false;
  }

  back() {
    this.location.back();
  }
}
