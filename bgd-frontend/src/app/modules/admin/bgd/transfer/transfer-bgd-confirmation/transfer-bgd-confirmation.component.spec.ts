import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferBgdConfirmationComponent } from './transfer-bgd-confirmation.component';

describe('TransferBgdConfirmationComponent', () => {
  let component: TransferBgdConfirmationComponent;
  let fixture: ComponentFixture<TransferBgdConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferBgdConfirmationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransferBgdConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
