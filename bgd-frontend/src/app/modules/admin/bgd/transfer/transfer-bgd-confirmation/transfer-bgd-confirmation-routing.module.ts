import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferBgdConfirmationComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-confirmation/transfer-bgd-confirmation.component';

const routes: Routes = [
    {
        path: '',
        component: TransferBgdConfirmationComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransferBgdConfirmationRoutingModule { }
