"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TransferBgdConfirmationComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var TransferBgdConfirmationComponent = /** @class */ (function () {
    function TransferBgdConfirmationComponent(_activatedRoute, _router, _cashWalletService, _bgdService, _httpClient) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._cashWalletService = _cashWalletService;
        this._bgdService = _bgdService;
        this._httpClient = _httpClient;
        this.checkboxChecked = false;
        this.disableButton = false;
        this.transferReason = '';
        this.amountValue = '';
        this.chargeTF = '';
        this.chargeSST = '';
        this.setupTF = '';
        this.setupSST = '';
        this.fullName = '';
        this.curAccNum = '';
        this.recAccNum = '';
        this.receiverNum = '';
        this.transReason = '';
        this.alert = null;
        this.isChecked = false;
        this.today = new Date();
        this.receiverName = '';
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.curAccNum = response[0].acctNumberSender;
        this.recAccNum = response[0].acctNumberReceiver;
        this.receiverName = response[0].receiverProfile.fullName;
        this.setupTF = response[0].feesForms[0].setupFees;
        this.setupSST = response[0].feesForms[1].setupFees;
        this.chargeTF = response[0].feesForms[0].chargeFees;
        this.chargeSST = response[0].feesForms[1].chargeFees;
        this.transferReason = response[0].reason;
        this.amountValue = response[0].amount;
        this.unitPrice = response[0].bursaSellPrice;
        this.receiverNum = this.recAccNum;
        this.transReason = this.transferReason;
        this.totalAmount = this.chargeTF + this.chargeSST;
        console.log(this.curAccNum);
        console.log(this.recAccNum);
        console.log(this.receiverName);
        console.log(this.setupTF);
        console.log(this.setupSST);
        console.log(this.chargeTF);
        console.log(this.chargeSST);
        console.log(this.amountValue);
        console.log(this.unitPrice);
        console.log(response);
    }
    TransferBgdConfirmationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._cashWalletService.getWalletDetails().subscribe(function (data) {
            _this.walletBalance = data[0].availableAmount;
            console.log(data);
        });
    };
    TransferBgdConfirmationComponent.prototype.toggleCheckbox = function () {
        this.checkboxChecked = !this.checkboxChecked;
    };
    TransferBgdConfirmationComponent.prototype.nextButton = function () {
        this.disableButton = true;
        if (this.checkboxChecked) {
            var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
            this._router.navigate(['/transfer-bgd-pin'], { queryParams: { response: JSON.stringify(response) } });
        }
        else {
            // Display an error message or prompt the user to check the checkbox
        }
        this.disableButton = false;
    };
    TransferBgdConfirmationComponent.prototype.backButton = function () {
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this._router.navigate(['/transfer-bgd-amount'], {
            queryParams: {
                response: JSON.stringify(response),
                acctNumber: encodeURIComponent(this.recAccNum),
                transferReason: encodeURIComponent(this.transferReason)
            }
        });
    };
    TransferBgdConfirmationComponent = __decorate([
        core_1.Component({
            selector: 'bgd-transfer-bgd-confirmation',
            templateUrl: './transfer-bgd-confirmation.component.html',
            styleUrls: ['./transfer-bgd-confirmation.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], TransferBgdConfirmationComponent);
    return TransferBgdConfirmationComponent;
}());
exports.TransferBgdConfirmationComponent = TransferBgdConfirmationComponent;
// this._activatedRoute.paramMap.subscribe(params => {
//   this.curAccNum = params.get('userAcc');
//   this.recAccNum = params.get('acctNumber');
//   this.receiverName = params.get('receiverName');
//   this.setupTF = params.get('setupTF');
//   this.setupSST = params.get('setupSST');
//   this.chargeTF = params.get('chargeTF');
//   this.chargeSST = params.get('chargeSST');
//   this.transferReason = params.get('transferReason');
//   this.amountValue = params.get('amountValue');
//   this.unitPrice = params.get('unitPrice')
// })
// this.curAccNum = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userAcc'));
// this.receiverName = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('receiverName'));
// this.setupTF = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('setupTF'));
// this.setupSST = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('setupSST'));
// this.chargeTF = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('chargeTF'));
// this.chargeSST = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('chargeSST'));
// this.recAccNum = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('acctNumber'));
// this.transferReason = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('transferReason'));
// this.amountValue = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('amountValue'));
// this.unitPrice = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('unitPrice'));
//this.totalAmount = this.chargeTF + this.chargeSST;
//}
//   ngOnInit(): void {
//     // this._cashWalletService.getUserDetails().subscribe(data => {
//     //   this.userID = data.userID;
//     //   this.fullName = data.fullName;
//     //   console.log(data);
//     // })
//     // this._cashWalletService.getWalletDetails().subscribe(data => {
//     //   this.walletBalance = data[0].availableAmount;
//     //   console.log(data);
//     // })
//   }
//   nextButton() {
//     // const acctNumberSender = this.curAccNum;
//     // const acctNumberReceiver = this.recAccNum;
//     // const amount = this.amountValue;
//     // const statusCode = 'G05';
//     // const setupTF = this.setupTF;
//     // const setupSST = this.setupSST;
//     // const chargeTF = this.chargeTF;
//     // const chargeSST = this.chargeSST;
//     // const reason = this.transferReason;
//     // const unitPrice = this.unitPrice
//     // const receiverName = this.receiverName
//     // console.log(this.amountValue);
//     // this._router.navigate(['/transfer-bgd-pin'], {
//     //   queryParams: {
//     //     accNum: encodeURIComponent(acctNumberReceiver),
//     //     setupTF: encodeURIComponent(setupTF),
//     //     setupSST: encodeURIComponent(setupSST),
//     //     chargeTF: encodeURIComponent(chargeTF),
//     //     chargeSST: encodeURIComponent(chargeSST),
//     //     userAcc: encodeURIComponent(acctNumberSender),
//     //     transferReason: encodeURIComponent(reason),
//     //     amountValue: encodeURIComponent(amount),
//     //     receiverName: encodeURIComponent(receiverName),
//     //     unitPrice: encodeURIComponent(unitPrice)
//     //   }
//     // });
//   }
// }
