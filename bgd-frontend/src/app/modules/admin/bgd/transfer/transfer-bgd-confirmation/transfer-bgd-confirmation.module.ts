import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { TransferBgdConfirmationComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-confirmation/transfer-bgd-confirmation.component';
import { TransferBgdConfirmationRoutingModule } from 'app/modules/admin/bgd/transfer/transfer-bgd-confirmation/transfer-bgd-confirmation-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';


@NgModule({
    declarations: [
        TransferBgdConfirmationComponent,

    ],
    imports: [
        TransferBgdConfirmationRoutingModule,
        MatButtonModule,
        FormsModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        MatCheckboxModule
    ],

    providers: [],
    bootstrap: [TransferBgdConfirmationComponent]

})
export class TransferBgdConfirmationModule {
}
