import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferBgdStatusComponent } from './transfer-bgd-status.component';

describe('TransferBgdStatusComponent', () => {
  let component: TransferBgdStatusComponent;
  let fixture: ComponentFixture<TransferBgdStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferBgdStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransferBgdStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
