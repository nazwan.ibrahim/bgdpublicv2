import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransferBgdStatusComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-status/transfer-bgd-status.component';

const routes: Routes = [
    {
        path: '',
        component: TransferBgdStatusComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransferBgdStatusRoutingModule { }
