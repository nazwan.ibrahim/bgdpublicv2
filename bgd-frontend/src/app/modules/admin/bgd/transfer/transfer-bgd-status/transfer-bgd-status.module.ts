import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { TransferBgdStatusComponent } from 'app/modules/admin/bgd/transfer/transfer-bgd-status/transfer-bgd-status.component';
import { TransferBgdStatusRoutingModule } from 'app/modules/admin/bgd/transfer/transfer-bgd-status/transfer-bgd-status-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        TransferBgdStatusComponent,

    ],
    imports: [
        TransferBgdStatusRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule
    ]
})
export class TransferBgdStatusModule {
}
