import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { saveAs } from 'file-saver';
import { TransferService } from '../transfer.service';
@Component({
  selector: 'bgd-transfer-bgd-status',
  templateUrl: './transfer-bgd-status.component.html',
  styleUrls: ['./transfer-bgd-status.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class TransferBgdStatusComponent {

  transferReason: any = '';
  amountValue: any = '';
  chargeTF: any = '';
  chargeSST: any = '';
  setupTF: any = '';
  setupSST: any = '';
  fullName: any = '';
  ownAccNum: any = '';
  transAccNum: any = '';
  unitPrice: any;
  totalAmount: any;
  userID: any;
  alert: any = null;
  today: Date = new Date();
  receiverName: any;
  ref: any;

  constructor(
    private _router: Router,
    private bgdService: bgdService,
    private transferService: TransferService
  ) {}

  ngOnInit(): void {

    const data = this.transferService.initResponse;
    this.ownAccNum = data[0].acctNumberSender;
    this.transAccNum = data[0].acctNumberReceiver
    this.receiverName = data[0].receiverProfile.fullName;
    this.setupTF = data[0].feesForms[0].setupFees;
    this.setupSST = data[0].feesForms[1].setupFees;
    this.chargeTF = parseFloat(data[0].feesForms[0].chargeFees);
    this.chargeSST = parseFloat(data[0].feesForms[1].chargeFees);
    this.transferReason = data[0].reason
    this.amountValue = data[0].amount;
    this.unitPrice = data[0].bursaSellPrice;

    const response = this.transferService.transferResponse;
    this.ref = response.reference;
    this.totalAmount = this.chargeTF + this.chargeSST
  }

  back() {
    this._router.navigate(['/dashboard']);
  }

  downloadReceipt() {
    this.bgdService.downloadReceipt(this.ref, 'F03').subscribe(
      (response: any) => {
        const blob = new Blob([response], { type: 'application/pdf' });
        saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        console.error('Error downloading receipt:', error);
      }
    );
  }


}
