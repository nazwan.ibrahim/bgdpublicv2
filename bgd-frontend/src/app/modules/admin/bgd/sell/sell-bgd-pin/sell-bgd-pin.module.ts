import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { SellBgdPinComponent } from 'app/modules/admin/bgd/sell/sell-bgd-pin/sell-bgd-pin.component';
import { SellBgdPinRoutingModule } from 'app/modules/admin/bgd/sell/sell-bgd-pin/sell-bgd-pin-routing.module';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { NgxPincodeModule } from 'ngx-pincode';


@NgModule({
    declarations: [
        SellBgdPinComponent,

    ],
    imports: [
        SellBgdPinRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatIconModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        NgxPincodeModule,
    ]
})
export class SellBgdPinModule {
}
