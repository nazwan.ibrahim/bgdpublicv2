import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { bgdService } from '../../bgd.service';
import { Subscription, interval, take } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SellBgdAlertDialog } from '../sell-bgd/sell-bgd-alert-dialog/sell-bgd-alert-dialog.component';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { SellService } from '../sell.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-sell-bgd-pin',
  templateUrl: './sell-bgd-pin.component.html',
  styleUrls: ['./sell-bgd-pin.component.scss'],
  animations: [
    trigger('shake', [
      // Animation configuration
    ])
  ]

})
export class SellBgdPinComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  today: Date = new Date();
  quantity: any;
  totalPrice: any;
  unitPrice: any;
  netPrice: any;
  availGram: any;
  receivablePrice: any;
  chargeTransFee: any;
  chargeSstFee: any;
  type: any = "S06";
  feeTypeCode: any;
  response: any;
  availAmount: any;
  sellPriceGold: number = 0;
  elapsedTime: number = 0;
  buyprice: number = 0;
  sellprice: number = 0;
  email: any;
  username: any;
  pin: any;
  dataJson: any;
  showAlert = false;
  isDialogOpen: boolean = false;
  disableButton: boolean = false;

  reference: any;
  priceRequestId: any;
  feeTransTypeCode: any;
  feeTransType: any;
  setupTransUomCode: any;
  setupTransUom: any;
  transChargeUomCode: any;
  transChargeUom: any;
  SstSetupUomCode: any;
  SstsetupUom: any;
  SstChargeUomCode: any;
  SstChargeUom: any;
  SstFeeType: any;
  SstFeeTypeCode: any;
  setupTransFee: any;
  setupSstFee: any;
  price: any;
  inputType: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private sellServie: SellService,
    private _dashboardService: DashboardService,
    private bgdService: bgdService,
    private dialog: MatDialog,
    private location: Location

  ) {}

  ngOnInit(): void {

    this.elapsedTime = this.sellServie.elapsedTime;
    this.reference = this.sellServie.referenceId;
    this.priceRequestId = this.sellServie.priceRequestId;

    this._activatedRoute.queryParamMap.subscribe(params => {
      this.feeTypeCode = params.get('feeTypeCode');
    });

    this._dashboardService.getQuerySellPrice().pipe(
      //take(1) // take the first message from the WebSocket and complete the subscription
    ).subscribe(
      (message) => {
        this.sellPriceGold = (message['A02'].price).toFixed(2);
        // do something with the sellprice value here
        this.startTimer(); // start the timer after the initial value of sellpriceGold is set
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );

    this._dashboardService.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    });
  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }


  timerSubscription: Subscription;
  GoldWsSubscription: Subscription;

  ngAfterViewInit() {
    this.GoldWsSubscription = this._dashboardService.getQuerySellPrice().subscribe(
      (message) => {
        this.sellPriceGold = (message['A02'].price).toFixed(2);
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  ngOnDestroy() {
    this.GoldWsSubscription.unsubscribe();
    this.stopTimer();
  }

  startTimer() {
    
    const countdownTime = 2 * 60 * 1000;
    let elapsedTime = this.elapsedTime || countdownTime;
    
    const startTime = Date.now(); 
    this.timerSubscription = interval(1000).subscribe(() => {
      const elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000); 
      
      this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000; 
      
      localStorage.setItem('elapsedTime', this.elapsedTime.toString()); 
      if (this.elapsedTime <= 0) {
        this.stopTimer(); 
        this.openDialog();
        if (!this.isDialogOpen) {
          this.startTimer();
        }
      }
    });
  }


  getElapsedTime(): string {
    const minutes = Math.floor(this.elapsedTime / 60000);
    const seconds = Math.floor((this.elapsedTime % 60000) / 1000);
    return `${minutes.toString().padStart(1, '0')} min ${seconds.toString().padStart(2, '0')} sec`;
  }

  stopTimer() {
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
  }

  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(SellBgdAlertDialog, {
      data: {
        price: this.sellPriceGold
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.startTimer();
    });
  }


  sellNow(): void {

    const requestBody = 
    {
      "userName": this.username,
      "userPin": this.pin,
      "referenceId": this.reference,
      "priceRequestID": this.priceRequestId
    }
    console.log("Requestbody:",requestBody);

    this.sellServie.pin = this.pin;
    this.sellServie.userName = this.username;

    const data = JSON.stringify(requestBody);

    this.bgdService.confirmPurchase(data)
      .subscribe(
        Response => {

          console.log("confirmPurchase Response:", Response);

          const response = Response;
          this.sellServie.purchaseResponse = response;

          this._router.navigate(['/sell-bgd-status']);

        },
        error => {

          if(error.status === 401) {

            this.alert = {
              type: 'error',
              message: 'Wrong PIN number'
            };
            
          } else {

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };

          }

          this.showAlert = true;

          setTimeout(() => {
            this.showAlert = false;
          }, 1000);
          
        }
      );
  }

  back() {
    this.location.back();
  }

  forgotPin() {
    this._router.navigate(['/forgot-pin']);
  }
}
