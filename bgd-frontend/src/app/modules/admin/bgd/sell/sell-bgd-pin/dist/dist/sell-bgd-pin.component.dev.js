"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = void 0 && (void 0).__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

exports.__esModule = true;
exports.SellBgdPinComponent = void 0;

var core_1 = require("@angular/core");

var rxjs_1 = require("rxjs");

var sell_bgd_alert_dialog_component_1 = require("../sell-bgd/sell-bgd-alert-dialog/sell-bgd-alert-dialog.component");

var animations_1 = require("@angular/animations");

var SellBgdPinComponent =
/** @class */
function () {
  function SellBgdPinComponent(_activatedRoute, _router, _formBuilder, _dashboardService, bgdService, dialog) {
    this._activatedRoute = _activatedRoute;
    this._router = _router;
    this._formBuilder = _formBuilder;
    this._dashboardService = _dashboardService;
    this.bgdService = bgdService;
    this.dialog = dialog;
    this.alert = {
      type: 'success',
      message: ''
    };
    this.today = new Date();
    this.type = "S06";
    this.sellPriceGold = 0;
    this.elapsedTime = 0;
    this.buyprice = 0;
    this.sellprice = 0;
    this.showAlert = false;
    this.isDialogOpen = false;
    this.disableButton = false;
    var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
    this.elapsedTime = this._activatedRoute.snapshot.queryParams['elapsedTime'];
    this.reference = this._activatedRoute.snapshot.queryParams['reference'];
    this.priceRequestId = this._activatedRoute.snapshot.queryParams['priceRequestId'];
    this.setupTransUomCode = response[0].feesForms[0].setupUomCode;
    this.setupTransUom = response[0].feesForms[0].setupUom;
    this.transChargeUomCode = response[0].feesForms[0].chargeUomCode;
    this.transChargeUom = response[0].feesForms[0].chargeUom;
    this.feeTransType = response[0].feesForms[0].feeType;
    this.feeTransTypeCode = response[0].feesForms[0].feeTypeCode;
    this.setupTransFee = response[0].feesForms[0].setupTransFee;
    this.chargeTransFee = response[0].feesForms[0].chargeFees;
    this.SstSetupUomCode = response[0].feesForms[1].setupUomCode;
    this.SstsetupUom = response[0].feesForms[1].setupUom;
    this.SstChargeUomCode = response[0].feesForms[1].chargeUomCode;
    this.SstChargeUom = response[0].feesForms[1].chargeUom;
    this.SstFeeType = response[0].feesForms[1].feeType;
    this.SstFeeTypeCode = response[0].feesForms[1].feeTypeCode;
    this.setupSstFee = response[0].feesForms[1].setupSstFee;
    this.chargeSstFee = response[0].feesForms[1].chargeFees;
    this.quantity = response[0].quantity;
    this.price = response[0].price;
    this.netPrice = response[0].netPrice;
    this.totalPrice = response[0].totalPrice;
    this.inputType = response[0].inputType;
    this.type = response[0].type;
    this.feeTypeCode = response[0].feesForms[1].feeTypeCode;
    this.receivablePrice = this.totalPrice - this.chargeTransFee - this.chargeSstFee; // let quantity = decodeURIComponent(this._activatedRoute.snapshot.queryParams['quantity']);
    // let totalPrice = decodeURIComponent(this._activatedRoute.snapshot.queryParams['totalPrice']);
    // let unitPrice = decodeURIComponent(this._activatedRoute.snapshot.queryParams['unitPrice']);
    // let transFee = decodeURIComponent(this._activatedRoute.snapshot.queryParams['transFee']);
    // let sstFee = decodeURIComponent(this._activatedRoute.snapshot.queryParams['sstFee']);
    // let receivablePrice = decodeURIComponent(this._activatedRoute.snapshot.queryParams['receivablePrice']);
    // this.chargeTransFee = parseFloat(transFee);
    // this.chargeSstFee = parseFloat(sstFee);
    // this.quantity = parseFloat(quantity);
    // this.totalPrice = parseFloat(totalPrice);
    // this.unitPrice = parseFloat(unitPrice);
    // this.availGram = parseFloat(this._activatedRoute.snapshot.queryParams['availGram']);
    // this.receivablePrice = parseFloat(receivablePrice);
  }

  SellBgdPinComponent.prototype.ngOnInit = function () {
    var _this = this;

    this._activatedRoute.queryParamMap.subscribe(function (params) {
      _this.feeTypeCode = params.get('feeTypeCode');
    });

    this._dashboardService.getQuerySellPrice().pipe().subscribe(function (message) {
      _this.sellPriceGold = message['A02'].price.toFixed(2); // do something with the sellprice value here

      _this.startTimer(); // start the timer after the initial value of sellpriceGold is set

    }, function (error) {
      _this.alert = {
        type: 'error',
        message: 'Something went wrong, please try again.'
      };
    });

    this._dashboardService.getUserDetails().subscribe(function (data) {
      _this.email = data.email;
      _this.username = data.userName;
    });
  };

  SellBgdPinComponent.prototype.pincodeCompleted = function (pin) {
    this.pin = pin;
  };

  SellBgdPinComponent.prototype.ngAfterViewInit = function () {
    var _this = this;

    this.GoldWsSubscription = this._dashboardService.getQuerySellPrice().subscribe(function (message) {
      _this.sellPriceGold = message['A02'].price.toFixed(2);
    }, function (error) {
      _this.alert = {
        type: 'error',
        message: 'Something went wrong, please try again.'
      };
    });
  };

  SellBgdPinComponent.prototype.ngOnDestroy = function () {
    this.GoldWsSubscription.unsubscribe();
    this.stopTimer(); // stop the timer when the component is destroyed
  };

  SellBgdPinComponent.prototype.startTimer = function () {
    var _this = this; // start the timer and subscribe to its events


    var countdownTime = 2 * 60 * 1000; // 2 minutes in milliseconds

    var elapsedTime = this.elapsedTime || countdownTime; // read elapsed time from localStorage or initialize it to countdownTime
    // console.log(elapsedTime);

    var startTime = Date.now(); // record the start time of the timer

    this.timerSubscription = rxjs_1.interval(1000).subscribe(function () {
      var elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000); // calculate elapsed time in seconds
      // console.log(elapsedTimeInSeconds);

      _this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000; // calculate remaining time
      // console.log(this.elapsedTime);

      localStorage.setItem('elapsedTime', _this.elapsedTime.toString()); // store elapsed time in localStorage

      if (_this.elapsedTime <= 0) {
        _this.stopTimer(); // stop the timer when it reaches 0


        _this.openDialog(); // open the dialog to display the new buy price


        if (!_this.isDialogOpen) {
          _this.startTimer(); // start the timer again for the next 2 minutes

        }
      }
    });
  };

  SellBgdPinComponent.prototype.getElapsedTime = function () {
    var minutes = Math.floor(this.elapsedTime / 60000);
    var seconds = Math.floor(this.elapsedTime % 60000 / 1000);
    return minutes.toString().padStart(1, '0') + " min " + seconds.toString().padStart(2, '0') + " sec";
  };

  SellBgdPinComponent.prototype.stopTimer = function () {
    // stop the timer if it is running
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
  };

  SellBgdPinComponent.prototype.openDialog = function () {
    var _this = this;

    this.isDialogOpen = true;
    var dialogRef = this.dialog.open(sell_bgd_alert_dialog_component_1.SellBgdAlertDialog, {
      data: {
        price: this.sellPriceGold
      }
    });
    dialogRef.afterClosed().subscribe(function () {
      _this.startTimer();
    });
  };

  SellBgdPinComponent.prototype.sellNow = function () {
    // const feeTypeCode = this.feeTypeCode;
    var _this = this; // const requestBody = {
    //   "reference": "<reference from api queryBuyPrice or querySellPrice>",
    //   "priceRequestID": "<priceRequestID from queryBuyPrice or querySellPrice>",
    //   "type": this.type,
    //   "quantity": this.quantity,
    //   "netPrice": this.receivablePrice,
    //   "tradingFeeList": [
    //     {
    //       "feeTypeCode": feeTypeCode,
    //       "chargeFees": this.chargeSstFee,
    //       "chargeUomCode": "J01"
    //     }
    //   ]
    // }
    // console.log(requestBody);
    // let dataJson = JSON.stringify(requestBody);
    // this.bgdService.Purchase(dataJson)
    //   .subscribe(
    //     Response => {
    //       this.alert = {
    //         type: 'success',
    //         message: 'Success'
    //       };
    //       console.log(dataJson);
    //       this._router.navigate(['/sell-bgd-status'], { queryParams: { quantity: this.quantity, totalPrice: this.totalPrice, unitPrice: this.unitPrice, transFee: this.chargeTransFee, sstFee: this.chargeSstFee, receivablePrice: this.receivablePrice } });
    //     },
    //     Response => {
    //       this.alert = {
    //         type: 'error',
    //         message: 'Something went wrong, please try again.'
    //       };
    //     }
    //   );
    // let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);


    var requestBody = {
      "quantity": this.quantity,
      "type": this.type,
      "inputType": this.inputType,
      "price": this.price,
      "netPrice": this.netPrice,
      "totalPrice": this.totalPrice,
      "priceRequestID": this.priceRequestId,
      "reference": this.reference,
      "tradingFeeList": [{
        "feeType": this.feeTransType,
        "feeTypeCode": this.feeTransTypeCode,
        "setupFees": this.setupTransFee,
        "setupUom": this.setupTransUom,
        "setupUomCode": this.setupTransUomCode,
        "chargeFees": this.chargeTransFee,
        "chargeUom": this.transChargeUom,
        "chargeUomCode": this.transChargeUomCode
      }, {
        "feeType": this.SstFeeType,
        "feeTypeCode": this.SstFeeTypeCode,
        "setupFees": this.setupSstFee,
        "setupUom": this.SstsetupUom,
        "setupUomCode": this.SstSetupUomCode,
        "chargeFees": this.chargeSstFee,
        "chargeUom": this.SstChargeUom,
        "chargeUomCode": this.SstChargeUomCode
      }]
    };
    console.log(requestBody);
    var dataJson = JSON.stringify(requestBody);
    this.bgdService.Purchase(dataJson).subscribe(function (Response) {
      console.log("Response ", Response);
      var queryParams = {
        response: JSON.stringify(Response),
        requestBody: JSON.stringify(requestBody),
        unitPrice: _this.sellPriceGold
      };
      setTimeout(function () {
        _this._router.navigate(['/sell-bgd-status'], {
          queryParams: queryParams
        });
      }, 1000); // 3000 milliseconds = 3 seconds
    }, function (Response) {
      _this.alert = {
        type: 'error',
        message: 'Something went wrong, please try again.'
      };
    });
  };

  SellBgdPinComponent.prototype.verifyPin = function () {
    var _this = this;

    this.disableButton = true;
    var pin = {
      userName: this.username,
      email: this.email,
      userPin: this.pin
    };
    console.log(pin);
    this.bgdService.verifyPin(pin).subscribe(function (Response) {
      _this.alert = {
        type: 'success',
        message: 'Verified'
      };

      _this.sellNow();
    }, function (error) {
      _this.disableButton = false;
      _this.alert = {
        type: 'error',
        message: 'Incorrect PIN.'
      };
      _this.showAlert = true;
    });
    this.showAlert = true;
  };

  SellBgdPinComponent.prototype.sellPinBack = function () {
    var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    this._router.navigate(['/sell-bgd-confirmation'], {
      queryParams: {
        response: JSON.stringify(response),
        availGram: this.availAmount,
        unitPrice: this.sellPriceGold,
        elapsedTime: this.elapsedTime,
        reference: this.reference,
        priceRequestId: this.priceRequestId
      }
    });
  };

  SellBgdPinComponent.prototype.forgotPin = function () {
    this._router.navigate(['/forgot-pin']);
  };

  SellBgdPinComponent = __decorate([core_1.Component({
    selector: 'bgd-sell-bgd-pin',
    templateUrl: './sell-bgd-pin.component.html',
    styleUrls: ['./sell-bgd-pin.component.scss'],
    animations: [animations_1.trigger('shake', [// Animation configuration
    ])]
  })], SellBgdPinComponent);
  return SellBgdPinComponent;
}();

exports.SellBgdPinComponent = SellBgdPinComponent;