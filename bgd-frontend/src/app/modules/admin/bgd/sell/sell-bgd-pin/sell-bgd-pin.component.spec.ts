import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellBgdPinComponent } from './sell-bgd-pin.component';

describe('SellBgdPinComponent', () => {
  let component: SellBgdPinComponent;
  let fixture: ComponentFixture<SellBgdPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellBgdPinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SellBgdPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
