import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SellBgdStatusComponent } from 'app/modules/admin/bgd/sell/sell-bgd-status/sell-bgd-status.component';

const routes: Routes = [
  {
    path: '',
    component: SellBgdStatusComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellBgdStatusRoutingModule { }
