import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from '../../bgd.service';
import { saveAs } from 'file-saver';
import { SellService } from '../sell.service';
@Component({
  selector: 'bgd-sell-bgd-status',
  templateUrl: './sell-bgd-status.component.html',
  styleUrls: ['./sell-bgd-status.component.scss']
})
export class SellBgdStatusComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };
  today: Date = new Date();
  chargeTransFee: any;
  chargeSstFee: any;
  quantity: any;
  receivablePrice: any;
  totalPrice: any;
  type: any;
  unitPrice: any;
  price: any;
  netPrice: any;
  referenceNumber: any;
  setupTransFee: any;
  setupSstFee: any;
  response: any;
  feeTypeCode: any;
  inputType: any;
  referenceId: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private sellservice: SellService,
    private bgdService: bgdService,

  ) {}

  ngOnInit(): void {

    this.response = this.sellservice.initResponse;
    this.setupTransFee = this.response[0].feesForms[0].setupFees;
    this.chargeTransFee = this.response[0].feesForms[0].chargeFees;
    this.setupSstFee = this.response[0].feesForms[1].setupFees;
    this.chargeSstFee = this.response[0].feesForms[1].chargeFees;
    this.feeTypeCode = this.response[0].feesForms[1].feeTypeCode;
    this.quantity = this.response[0].quantity;
    this.totalPrice = this.response[0].totalPrice;
    this.price = this.response[0].price;
    this.netPrice = this.response[0].netPrice;
    this.type = this.response[0].type;
    this.inputType = this.response[0].inputType;
    this.referenceNumber = this.response[0].reference;

    this.unitPrice = this.sellservice.sellPriceGold;
    this.referenceId = this.sellservice.referenceId;
    
  }

  downloadReceipt() {

    this.bgdService.downloadReceipt(this.referenceNumber, 'F06').subscribe(
      (response: any) => {
        const blob = new Blob([response], { type: 'application/pdf' });
        saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        console.error('Error downloading receipt:', error);
      }
    );
  }
}
