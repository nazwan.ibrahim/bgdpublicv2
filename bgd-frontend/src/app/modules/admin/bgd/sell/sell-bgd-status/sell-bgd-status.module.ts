import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { SellBgdStatusComponent } from 'app/modules/admin/bgd/sell/sell-bgd-status/sell-bgd-status.component';
import { SellBgdStatusRoutingModule } from 'app/modules/admin/bgd/sell/sell-bgd-status/sell-bgd-status-routing.module';
import { RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
    declarations: [
        SellBgdStatusComponent

    ],
    imports: [
        SellBgdStatusRoutingModule,
        CommonModule,
        SharedModule,
        MatInputModule,
        MatButtonModule,
    ]
})
export class SellBgdStatusModule {
}
