import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { SellBgdComponent } from 'app/modules/admin/bgd/sell/sell-bgd/sell-bgd.component';
import { SellBgdRoutingModule } from 'app/modules/admin/bgd/sell/sell-bgd/sell-bgd-routing.module';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        SellBgdComponent
    ],
    imports: [
        SellBgdRoutingModule,
        CommonModule,
        SharedModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        
    ]
})
export class SellBgdModule {
}
