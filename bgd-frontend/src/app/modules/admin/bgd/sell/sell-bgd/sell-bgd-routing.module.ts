import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SellBgdComponent } from 'app/modules/admin/bgd/sell/sell-bgd/sell-bgd.component';

const routes: Routes = [
  {
    path: '',
    component: SellBgdComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellBgdRoutingModule { }
