"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SellBgdComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var sell_bgd_alert_dialog_component_1 = require("./sell-bgd-alert-dialog/sell-bgd-alert-dialog.component");
var SellBgdComponent = /** @class */ (function () {
    function SellBgdComponent(_activatedRoute, _router, bgdService, _formBuilder, _dashboardService, dialog) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.bgdService = bgdService;
        this._formBuilder = _formBuilder;
        this._dashboardService = _dashboardService;
        this.dialog = dialog;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.availAmount = 0;
        this.walletModel = [];
        this.sellprice = 0;
        this.totalAmount = 0;
        this.sellPriceGold = 0;
        this.prevSellPriceGold = 0;
        this.elapsedTime = 0;
        this.disableButton = false;
        this.labelAlertVisible = false;
        this.isDialogOpen = false;
    }
    SellBgdComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sellGoldForm = this._formBuilder.group({
            price: ['', [forms_1.Validators.required]],
            grams: ['', [forms_1.Validators.required]]
        });
        this._dashboardService.getGoldWalletDetails().pipe(rxjs_1.tap(function (value) {
            if (!value) {
                return rxjs_1.of(null);
            }
            _this.walletModel = value;
            for (var i = 0; i < _this.walletModel.length; i++) {
                if (_this.walletModel[i].type == "Gold") {
                    _this.goldWalletIndex = i;
                    break;
                }
            }
            return _this._dashboardService.getGoldWalletDetails();
        }), rxjs_1.catchError(function (error) {
            return rxjs_1.of(null);
        })).subscribe(function (data) {
            if (!data) {
                return rxjs_1.of(null);
            }
            _this.availAmount = data[_this.goldWalletIndex].availableAmount.toFixed(6);
        });
        this._dashboardService.getQueryBuyPrice().pipe(
        //take(1) // take the first message from the WebSocket and complete the subscription
        ).subscribe(function (message) {
            console.log("message ", message);
            _this.sellPriceGold = (message['A02'].price).toFixed(2);
            _this.reference = message.reference;
            _this.priceRequestId = message.priceRequestID;
            console.log("Ref ", _this.reference);
            // do something with the sellprice value here
            _this.startTimer(); // start the timer after the initial value of sellpriceGold is set
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    SellBgdComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.GoldWsSubscription = this._dashboardService.getQueryBuyPrice().subscribe(function (message) {
            _this.sellprice = (message['A02'].price).toFixed(2);
            _this.reference = message.reference;
            _this.priceRequestId = message.priceRequestID;
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    SellBgdComponent.prototype.ngOnDestroy = function () {
        this.GoldWsSubscription.unsubscribe();
        this.stopTimer(); // stop the timer when the component is destroyed
    };
    SellBgdComponent.prototype.startTimer = function () {
        var _this = this;
        // start the timer and subscribe to its events
        var countdownTime = 2 * 60 * 1000; // 2 minutes in milliseconds
        this.elapsedTime = countdownTime; // initialize the elapsed time
        this.prevSellPriceGold = this.sellPriceGold; // initialize the previous value of buyPriceGold
        this.timerSubscription = rxjs_1.interval(1000).subscribe(function () {
            _this.elapsedTime -= 1000; // decrement the elapsed time by 1 second
            if (_this.elapsedTime <= 0) {
                _this.stopTimer(); // stop the timer when it reaches 0
                _this.openDialog(); // open the dialog to display the new buy price
                if (!_this.isDialogOpen) {
                    _this.startTimer(); // start the timer again for the next 2 minutes
                }
            }
        });
    };
    SellBgdComponent.prototype.getElapsedTime = function () {
        var minutes = Math.floor(this.elapsedTime / 60000);
        var seconds = Math.floor((this.elapsedTime % 60000) / 1000);
        return minutes.toString().padStart(1, '0') + " min " + seconds.toString().padStart(2, '0') + " sec";
    };
    SellBgdComponent.prototype.stopTimer = function () {
        // stop the timer if it is running
        if (this.timerSubscription && !this.timerSubscription.closed) {
            this.timerSubscription.unsubscribe();
        }
    };
    SellBgdComponent.prototype.openDialog = function () {
        var _this = this;
        this.isDialogOpen = true;
        var dialogRef = this.dialog.open(sell_bgd_alert_dialog_component_1.SellBgdAlertDialog, {
            data: {
                price: this.sellPriceGold
            }
        });
        dialogRef.afterClosed().subscribe(function () {
            _this.startTimer();
            location.reload();
        });
    };
    SellBgdComponent.prototype.ngOnChanges = function (changes) {
        if (changes.inputPrice) {
            if (!changes.inputPrice.currentValue) {
                this.resetProperties();
                return;
            }
            this.calculateInput("price");
        }
        else if (changes.inputGram) {
            if (!changes.inputGram.currentValue) {
                this.resetProperties();
                return;
            }
            this.calculateInput("gram");
        }
    };
    SellBgdComponent.prototype.resetProperties = function () {
        this.inputPrice = null;
        this.inputGram = null;
        this.labelAlertVisible = false;
        this.disableButton = false;
    };
    SellBgdComponent.prototype.calculateInput = function (inputType) {
        if (inputType === "price") {
            if (!this.inputPrice) {
                this.resetProperties();
                return;
            }
            this.inputGram = this.inputPrice / this.sellPriceGold;
            this.inputType = "S21";
        }
        else if (inputType === "gram") {
            if (!this.inputGram) {
                this.resetProperties();
                return;
            }
            this.inputPrice = this.inputGram * this.sellPriceGold;
            this.inputType = "S22";
        }
        if (this.inputPrice && this.inputGram) {
            var calculatedPrice = this.inputGram * this.sellPriceGold;
            var calculatedGram = this.inputPrice / this.sellPriceGold;
            if (calculatedGram > this.availAmount) {
                this.labelAlertVisible = true;
                this.disableButton = true;
            }
            else {
                this.labelAlertVisible = false;
                this.disableButton = false;
            }
            this.inputPrice = Number(calculatedPrice.toFixed(2)); // Limit to 2 decimal places
            this.inputGram = Number(calculatedGram.toFixed(6)); // Limit to 6 decimal places
        }
    };
    SellBgdComponent.prototype.sellMethod = function () {
        var _this = this;
        this.disableButton = true;
        if (this.sellGoldForm.invalid) {
            return;
        }
        this.sellGoldForm.disable();
        var totalPrice = this.inputPrice;
        var quantity = this.inputGram;
        var type = "S06";
        var inputType = this.inputType;
        var price = this.sellPriceGold;
        console.log(quantity);
        this.bgdService.initPurchase(totalPrice, quantity, type, inputType, price)
            .pipe(rxjs_1.finalize(function () {
            _this.sellGoldForm.enable();
            _this.sellGoldForm.reset();
        }))
            .subscribe(function (Response) {
            console.log("initPurchase Response    ", Response);
            _this.alert = {
                type: 'success',
                message: 'Success'
            };
            _this.response = Response;
            _this._router.navigate(['/sell-bgd-confirmation'], {
                queryParams: {
                    response: JSON.stringify(_this.response),
                    availGram: _this.availAmount,
                    unitPrice: _this.sellPriceGold,
                    elapsedTime: _this.elapsedTime,
                    reference: _this.reference,
                    priceRequestId: _this.priceRequestId
                }
            });
        }, function (error) {
            _this.disableButton = false;
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
            _this._router.navigate(['/transaction-limit']);
        });
    };
    __decorate([
        core_1.ViewChild('sellGoldngForm')
    ], SellBgdComponent.prototype, "sellGoldngForm");
    SellBgdComponent = __decorate([
        core_1.Component({
            selector: 'bgd-sell-bgd',
            templateUrl: './sell-bgd.component.html',
            styleUrls: ['./sell-bgd.component.scss']
        })
    ], SellBgdComponent);
    return SellBgdComponent;
}());
exports.SellBgdComponent = SellBgdComponent;
