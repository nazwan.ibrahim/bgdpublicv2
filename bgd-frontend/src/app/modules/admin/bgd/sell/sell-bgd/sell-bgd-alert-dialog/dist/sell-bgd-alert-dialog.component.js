"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.SellBgdAlertDialog = void 0;
var core_1 = require("@angular/core");
var dialog_1 = require("@angular/material/dialog");
/**
 * @title Dialog elements
 */
var SellBgdAlertDialog = /** @class */ (function () {
    function SellBgdAlertDialog(_router, _dashboardService, dialogRef, data) {
        this._router = _router;
        this._dashboardService = _dashboardService;
        this.dialogRef = dialogRef;
        this.data = data;
    }
    SellBgdAlertDialog.prototype.onProceed = function () {
        this.dialogRef.close();
        this._router.navigate(['/sell-bgd']);
        location.reload();
    };
    SellBgdAlertDialog.prototype.onCancel = function () {
        this.dialogRef.close();
        this._router.navigate(['/dashboard']).then(function () {
            setTimeout(function () {
                location.reload();
            }, 0);
        });
    };
    SellBgdAlertDialog = __decorate([
        core_1.Component({
            selector: 'sell-bgd-alert-dialog',
            templateUrl: 'sell-bgd-alert-dialog.component.html'
        }),
        __param(3, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], SellBgdAlertDialog);
    return SellBgdAlertDialog;
}());
exports.SellBgdAlertDialog = SellBgdAlertDialog;
