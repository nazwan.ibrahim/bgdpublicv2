import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellBgdComponent } from './sell-bgd.component';

describe('SellBgdComponent', () => {
  let component: SellBgdComponent;
  let fixture: ComponentFixture<SellBgdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellBgdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SellBgdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
