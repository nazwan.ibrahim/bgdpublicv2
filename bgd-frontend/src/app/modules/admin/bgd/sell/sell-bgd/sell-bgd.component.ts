import { Component, SimpleChanges, ViewChild, Inject } from '@angular/core';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { bgdService } from '../../bgd.service';
import { of, Subscription, interval, tap, catchError, take, finalize } from 'rxjs';
import { FuseAlertType } from '@bgd/components/alert';
import { MatDialog } from '@angular/material/dialog';
import { SellBgdAlertDialog } from './sell-bgd-alert-dialog/sell-bgd-alert-dialog.component';
import { SellService } from '../sell.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-sell-bgd',
  templateUrl: './sell-bgd.component.html',
  styleUrls: ['./sell-bgd.component.scss']
})
export class SellBgdComponent {

  @ViewChild('sellGoldngForm') sellGoldngForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  sellGoldForm: UntypedFormGroup;
  showAlert = false;
  availAmount: number = 0;
  walletModel: any[] = [];
  goldWalletIndex: number;
  totalAmount: number = 0;
  sellPriceGold: number = 0;
  prevSellPriceGold: number = 0;
  elapsedTime: number = 0;
  inputPrice: any;
  inputGram: any;
  inputType: any;
  response: any;
  disableButton: boolean = false;
  labelAlertVisible: boolean = false;
  isDialogOpen: boolean = false;
  priceRequestId: any;
  reference: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private bgdService: bgdService,
    private _formBuilder: UntypedFormBuilder,
    private _dashboardService: DashboardService,
    private sellService: SellService,
    private dialog: MatDialog,
    private location: Location
  ) { }

  ngOnInit(): void {

    //Sell form
    this.sellGoldForm = this._formBuilder.group({
      price: ['', [Validators.required]],
      grams: ['', [Validators.required]],
    });

    //Get cash wallet details from API
    this._dashboardService.getGoldWalletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Gold") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._dashboardService.getGoldWalletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }
      this.availAmount = data[this.goldWalletIndex].availableAmount.toFixed(6);
      this.sellService.availAmount = this.availAmount;
    });

    this._dashboardService.getQueryBuyPrice().pipe(
    ).subscribe(
      (message) => {
        console.log("Query Sell Price:", message);

        this.sellPriceGold = (message['A02'].price).toFixed(2);
        this.sellService.sellPriceGold = this.sellPriceGold;

        this.reference = message.reference;
        this.sellService.referenceId = this.reference;

        this.priceRequestId = message.priceRequestID;
        this.sellService.priceRequestId = this.priceRequestId;

        this.startTimer(); 
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );


  }

  timerSubscription: Subscription;
  GoldWsSubscription: Subscription;


  ngAfterViewInit() {
    this.GoldWsSubscription = this._dashboardService.getQueryBuyPrice().subscribe(
      (message) => {
        this.sellPriceGold = (message['A02'].price).toFixed(2);
        this.reference = message.reference;
        this.priceRequestId = message.priceRequestID;
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  ngOnDestroy() {
    this.GoldWsSubscription.unsubscribe();
    this.stopTimer(); 
  }

  startTimer() {
    
    const countdownTime = 2 * 60 * 1000;

    this.elapsedTime = countdownTime;
    this.prevSellPriceGold = this.sellPriceGold; 

    this.timerSubscription = interval(1000).subscribe(() => {

      this.elapsedTime -= 1000; 

      if (this.elapsedTime <= 0) {

        this.stopTimer(); 
        this.openDialog();

        if (!this.isDialogOpen) {
          this.startTimer(); 
        }

      }
    });
  }

  getElapsedTime(): string {
    const minutes = Math.floor(this.elapsedTime / 60000);
    const seconds = Math.floor((this.elapsedTime % 60000) / 1000);
    return `${minutes.toString().padStart(1, '0')} min ${seconds.toString().padStart(2, '0')} sec`;
  }

  stopTimer() {
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
  }

  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(SellBgdAlertDialog, {
      data: {
        price: this.sellPriceGold
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.startTimer();
      location.reload();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.inputPrice) {
      if (!changes.inputPrice.currentValue) {
        this.resetProperties();
        return;
      }
      this.calculateInput("price");
    } else if (changes.inputGram) {
      if (!changes.inputGram.currentValue) {
        this.resetProperties();
        return;
      }
      this.calculateInput("gram");
    }
  }

  resetProperties() {
    this.inputPrice = null;
    this.inputGram = null;
    this.labelAlertVisible = false;
    this.disableButton = false;
  }

  calculateInput(inputType: "price" | "gram") {
    if (inputType === "price") {
      if (!this.inputPrice) {
        this.resetProperties();
        return;
      }
      this.inputGram = this.inputPrice / this.sellPriceGold;
      this.inputType = "S21";

    } else if (inputType === "gram") {
      if (!this.inputGram) {
        this.resetProperties();
        return;
      }
      this.inputPrice = this.inputGram * this.sellPriceGold;
      this.inputType = "S22";

    }

    if (this.inputPrice && this.inputGram) {
      const calculatedPrice = this.inputGram * this.sellPriceGold;
      const calculatedGram = this.inputPrice / this.sellPriceGold;

      if (calculatedGram > this.availAmount) {
        this.labelAlertVisible = true;
        this.disableButton = true;
      } else {
        this.labelAlertVisible = false;
        this.disableButton = false;
      }
      this.inputPrice = Number(calculatedPrice.toFixed(2)); 
      this.inputGram = Number(calculatedGram.toFixed(6));
    }
  }

  sellMethod() {

    this.disableButton = true;

    if (this.sellGoldForm.invalid) {
      return;
    }

    this.sellGoldForm.disable();

    const totalPrice = this.inputPrice;
    const quantity = this.inputGram;
    const type = "S06";
    const inputType = this.inputType;
    const price = this.sellPriceGold;
    const reference = this.reference;

    this.bgdService.initPurchase(totalPrice, quantity, type, inputType, price, reference)
      .pipe(
        finalize(() => {
          this.sellGoldForm.enable();

          this.sellGoldForm.reset();
        })
      )
      .subscribe(
        Response => {
          console.log("initPurchase Response:", Response);

          this.alert = {
            type: 'success',
            message: 'Success'
          };

          this.response = Response;
          this.sellService.initResponse = this.response;
          this.sellService.elapsedTime = this.elapsedTime;

          this._router.navigate(['/sell-bgd-confirmation']);
        },
        error => {

          this.disableButton = false;

          this.alert = {
            type: 'error',
            message: 'Something went wrong, please try again.'
          };

          this._router.navigate(['/transaction-limit']);

        }
      )
  }

  back() {
    this.location.back();
  }

}
