import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NavigationEnd, Router } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { filter } from 'rxjs/operators';

export interface DialogData {
  price: any;
}

/**
 * @title Dialog elements
 */
@Component({
  selector: 'sell-bgd-alert-dialog',
  templateUrl: 'sell-bgd-alert-dialog.component.html',
})
export class SellBgdAlertDialog {

  sellPriceGold: any;
  reference: any;
  priceRequestId: any;

  constructor(
    private _router: Router,
    private _dashboardService: DashboardService,
    public dialogRef: MatDialogRef<SellBgdAlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) { }

ngOnInit(): void {
  this._router.events
    .pipe(filter(event => event instanceof NavigationEnd))
    .subscribe((event: NavigationEnd) => {
      if (event.urlAfterRedirects === '/sell-bgd') {
        location.reload();
      }
    });
}


  onProceed(): void {
    this.dialogRef.close();
    this._router.navigate(['/sell-bgd']);
  }
  
  onCancel(): void {
    this.dialogRef.close();
    this._router.navigate(['/dashboard']).then(() => {
      setTimeout(() => {
        location.reload();
      }, 0);
    });

  }
}
