import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SellService {

  availAmount: any;
  sellPriceGold: any;
  priceRequestId: any;
  referenceId: any;
  elapsedTime: any;
  pin: any;
  userName: any;

  initResponse: any;
  purchaseResponse: any;

  constructor() { }
}
