import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { bgdService } from '../../bgd.service';
import { Subscription, interval, take } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { SellBgdConfirmationAlertDialogComponent } from './sell-bgd-confirmation-alert-dialog/sell-bgd-confirmation-alert-dialog.component';
import { SellService } from '../sell.service';
import { Location } from '@angular/common';

@Component({
  selector: 'sell-buy-bgd-confirmation',
  templateUrl: './sell-bgd-confirmation.component.html',
  styleUrls: ['./sell-bgd-confirmation.component.scss']
})
export class SellBgdConfirmationComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  today: Date = new Date();
  quantity: any;
  totalPrice: any;
  unitPrice: any;
  netPrice: any;
  receivablePrice: any;
  chargeTransFee: any;
  chargeSstFee: any;
  type: any;
  inputType: any;
  feeTypeCode: any;
  response: any;
  availAmount: any;
  sellPriceGold: number = 0;
  elapsedTime: number = 0;
  buyprice: number = 0;
  sellprice: number = 0;
  priceRequestId: any;
  reference: any;
  isDialogOpen: boolean = false;
  price: any;
  setupTransFee: any;
  setupSstFee: any;
  disableButton: boolean = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _dashboardService: DashboardService,
    private sellService: SellService,
    private dialog: MatDialog,
    private location: Location

  ) {}


  ngOnInit(): void {

    this.today = new Date();
    this.availAmount = this.sellService.availAmount;
    this.elapsedTime = this.sellService.elapsedTime;
    this.reference = this.sellService.referenceId;
    this.priceRequestId = this.sellService.priceRequestId;

    this.response = this.sellService.initResponse;
    this.setupTransFee = this.response[0].feesForms[0].setupFees;
    this.chargeTransFee = this.response[0].feesForms[0].chargeFees;
    this.setupSstFee = this.response[0].feesForms[1].setupFees;
    this.chargeSstFee = this.response[0].feesForms[1].chargeFees;
    this.feeTypeCode = this.response[0].feesForms[1].feeTypeCode;
    this.quantity = this.response[0].quantity;
    this.totalPrice = this.response[0].totalPrice;
    this.price = this.response[0].price;
    this.netPrice = this.response[0].netPrice;
    this.type = this.response[0].type;
    this.inputType = this.response[0].inputType;

    this._dashboardService.getQueryBuyPrice().pipe(
    ).subscribe(
      (message) => {
        this.sellPriceGold = (message['A02'].price).toFixed(2);
        
        this.startTimer(); 
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );

  }

  timerSubscription: Subscription;
  GoldWsSubscription: Subscription;

  ngAfterViewInit() {
    this.GoldWsSubscription = this._dashboardService.getQueryBuyPrice().subscribe(
      (message) => {
        this.sellPriceGold = (message['A02'].price).toFixed(2);
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  ngOnDestroy() {
    this.GoldWsSubscription.unsubscribe();
    this.stopTimer();
  }

  startTimer() {
    
    const countdownTime = 2 * 60 * 1000;
    let elapsedTime = this.elapsedTime || countdownTime;
   
    const startTime = Date.now(); 
    this.timerSubscription = interval(1000).subscribe(() => { 
      const elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000);
      
      this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000;
      
      localStorage.setItem('elapsedTime', this.elapsedTime.toString());
      if (this.elapsedTime <= 0) {
        this.stopTimer();
        this.openDialog();
        if (!this.isDialogOpen) {
          this.startTimer();
        }
      }
    });
  }

  getElapsedTime(): string {
    const minutes = Math.floor(this.elapsedTime / 60000);
    const seconds = Math.floor((this.elapsedTime % 60000) / 1000);
    return `${minutes.toString().padStart(1, '0')} min ${seconds.toString().padStart(2, '0')} sec`;
  }

  stopTimer() {
    
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
  }

  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(SellBgdConfirmationAlertDialogComponent, {
      data: {
        totalPrice: this.totalPrice,
        quantity: this.quantity,
        type: this.type,
        inputType: this.inputType,
        price: this.sellPriceGold,
        availAmount: this.availAmount
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.startTimer();
    });
  }

  sellNow() {
    this._router.navigate(['/sell-bgd-pin']);
  }

  back() {
    this.location.back();
  }
}

