import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { SellBgdConfirmationComponent } from 'app/modules/admin/bgd/sell/sell-bgd-confirmation/sell-bgd-confirmation.component';
import { SellBgdConfirmationRoutingModule } from 'app/modules/admin/bgd/sell/sell-bgd-confirmation/sell-bgd-confirmation-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        SellBgdConfirmationComponent,

    ],
    imports: [
        SellBgdConfirmationRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatIconModule
    ]
})
export class SellBgdConfirmationModule {
}
