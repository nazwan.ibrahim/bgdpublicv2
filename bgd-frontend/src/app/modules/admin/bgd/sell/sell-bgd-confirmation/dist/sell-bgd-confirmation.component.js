"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SellBgdConfirmationComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var sell_bgd_alert_dialog_component_1 = require("../sell-bgd/sell-bgd-alert-dialog/sell-bgd-alert-dialog.component");
var SellBgdConfirmationComponent = /** @class */ (function () {
    function SellBgdConfirmationComponent(_activatedRoute, _router, _formBuilder, _dashboardService, bgdService, dialog) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this._dashboardService = _dashboardService;
        this.bgdService = bgdService;
        this.dialog = dialog;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        this.sellPriceGold = 0;
        this.elapsedTime = 0;
        this.buyprice = 0;
        this.sellprice = 0;
        this.isDialogOpen = false;
        this.disableButton = false;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.setupTransFee = response[0].feesForms[0].setupFees;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.setupSstFee = response[0].feesForms[1].setupFees;
        this.quantity = response[0].quantity;
        this.price = response[0].price;
        this.netPrice = response[0].netPrice;
        this.totalPrice = response[0].totalPrice;
        this.type = response[0].type;
        this.feeTypeCode = response[0].feesForms[1].feeTypeCode;
        this.receivablePrice = this.totalPrice - this.chargeTransFee - this.chargeSstFee;
        this.unitPrice = this._activatedRoute.snapshot.queryParams['unitPrice'];
        this.availGram = this._activatedRoute.snapshot.queryParams['availGram'];
        this.elapsedTime = this._activatedRoute.snapshot.queryParams['elapsedTime'];
        this.reference = this._activatedRoute.snapshot.queryParams['reference'];
        this.priceRequestId = this._activatedRoute.snapshot.queryParams['priceRequestId'];
        console.log("Ref ", this.reference);
    }
    SellBgdConfirmationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._dashboardService.getQueryBuyPrice().pipe(
        //take(1) // take the first message from the WebSocket and complete the subscription
        ).subscribe(function (message) {
            _this.sellPriceGold = (message['A02'].price).toFixed(2);
            // do something with the sellprice value here
            _this.startTimer(); // start the timer after the initial value of sellpriceGold is set
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    SellBgdConfirmationComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.GoldWsSubscription = this._dashboardService.getQueryBuyPrice().subscribe(function (message) {
            _this.sellPriceGold = (message['A02'].price).toFixed(2);
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    SellBgdConfirmationComponent.prototype.ngOnDestroy = function () {
        this.GoldWsSubscription.unsubscribe();
        this.stopTimer(); // stop the timer when the component is destroyed
    };
    SellBgdConfirmationComponent.prototype.startTimer = function () {
        var _this = this;
        // start the timer and subscribe to its events
        var countdownTime = 2 * 60 * 1000; // 2 minutes in milliseconds
        var elapsedTime = this.elapsedTime || countdownTime; // read elapsed time from localStorage or initialize it to countdownTime
        // console.log(elapsedTime);
        var startTime = Date.now(); // record the start time of the timer
        this.timerSubscription = rxjs_1.interval(1000).subscribe(function () {
            var elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000); // calculate elapsed time in seconds
            // console.log(elapsedTimeInSeconds);
            _this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000; // calculate remaining time
            // console.log(this.elapsedTime);
            localStorage.setItem('elapsedTime', _this.elapsedTime.toString()); // store elapsed time in localStorage
            if (_this.elapsedTime <= 0) {
                _this.stopTimer(); // stop the timer when it reaches 0
                _this.openDialog(); // open the dialog to display the new buy price
                if (!_this.isDialogOpen) {
                    _this.startTimer(); // start the timer again for the next 2 minutes
                }
            }
        });
    };
    SellBgdConfirmationComponent.prototype.getElapsedTime = function () {
        var minutes = Math.floor(this.elapsedTime / 60000);
        var seconds = Math.floor((this.elapsedTime % 60000) / 1000);
        return minutes.toString().padStart(1, '0') + " min " + seconds.toString().padStart(2, '0') + " sec";
    };
    SellBgdConfirmationComponent.prototype.stopTimer = function () {
        // stop the timer if it is running
        if (this.timerSubscription && !this.timerSubscription.closed) {
            this.timerSubscription.unsubscribe();
        }
    };
    SellBgdConfirmationComponent.prototype.openDialog = function () {
        var _this = this;
        this.isDialogOpen = true;
        var dialogRef = this.dialog.open(sell_bgd_alert_dialog_component_1.SellBgdAlertDialog, {
            data: {
                price: this.sellPriceGold
            }
        });
        dialogRef.afterClosed().subscribe(function () {
            _this.startTimer();
        });
    };
    SellBgdConfirmationComponent.prototype.sellNow = function () {
        this.disableButton = true;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this._router.navigate(['/sell-bgd-pin'], {
            queryParams: {
                response: JSON.stringify(response),
                availGram: this.availAmount,
                unitPrice: this.sellPriceGold,
                elapsedTime: this.elapsedTime,
                reference: this.reference,
                priceRequestId: this.priceRequestId
            }
        });
        this.disableButton = false;
    };
    SellBgdConfirmationComponent.prototype.sellConfirmBack = function () {
        this._router.navigate(['/sell-bgd']);
    };
    SellBgdConfirmationComponent = __decorate([
        core_1.Component({
            selector: 'sell-buy-bgd-confirmation',
            templateUrl: './sell-bgd-confirmation.component.html',
            styleUrls: ['./sell-bgd-confirmation.component.scss']
        })
    ], SellBgdConfirmationComponent);
    return SellBgdConfirmationComponent;
}());
exports.SellBgdConfirmationComponent = SellBgdConfirmationComponent;
