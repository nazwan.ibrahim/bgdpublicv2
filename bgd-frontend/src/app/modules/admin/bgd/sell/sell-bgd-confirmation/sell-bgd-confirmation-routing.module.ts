import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SellBgdConfirmationComponent } from 'app/modules/admin/bgd/sell/sell-bgd-confirmation/sell-bgd-confirmation.component';

const routes: Routes = [
  {
    path: '',
    component: SellBgdConfirmationComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SellBgdConfirmationRoutingModule { }
