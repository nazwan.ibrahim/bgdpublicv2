import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellBgdConfirmationAlertDialogComponent } from './sell-bgd-confirmation-alert-dialog.component';

describe('SellBgdConfirmationAlertDialogComponent', () => {
  let component: SellBgdConfirmationAlertDialogComponent;
  let fixture: ComponentFixture<SellBgdConfirmationAlertDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellBgdConfirmationAlertDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SellBgdConfirmationAlertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
