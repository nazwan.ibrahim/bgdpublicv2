import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { bgdService } from '../../../bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';

export interface DialogData {
  totalPrice: any;
  quantity: any;
  type: any;
  inputType: any;
  price: any;
  availAmount: any;
}

@Component({
  selector: 'bgd-sell-bgd-confirmation-alert-dialog',
  templateUrl: './sell-bgd-confirmation-alert-dialog.component.html',
  styleUrls: ['./sell-bgd-confirmation-alert-dialog.component.scss']
})
export class SellBgdConfirmationAlertDialogComponent {

  response: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private bgdService: bgdService,
    private _dashboardService: DashboardService,
    public dialogRef: MatDialogRef<SellBgdConfirmationAlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) { }

  onProceed() {
    this._dashboardService.getQueryBuyPrice().subscribe((message) => {
      const sellPriceGold = message['A02'].price.toFixed(2);
      const priceRequestId = message.priceRequestID;
      const reference = message.reference;

      if (this.data.inputType === "S21") {
        this.data.quantity = this.data.totalPrice / sellPriceGold;
      } else if (this.data.inputType === "S22") {
        this.data.totalPrice = this.data.quantity * sellPriceGold;
      }

      this.bgdService.initPurchase(this.data.totalPrice, this.data.quantity, this.data.type, this.data.inputType, sellPriceGold, reference)
        .subscribe((response) => {
          console.log("initPurchase:", response);
          this.dialogRef.close();
          this._router.navigate(['/sell-bgd-confirmation'],
            {
              queryParams: {
                response: JSON.stringify(response),
                availAmount: this.data.availAmount,
                reference: reference,
                priceRequestId: priceRequestId
              }
            });
        });
    });
  }

  onCancel() {
    this.dialogRef.close();
    this._router.navigate(['/dashboard']).then(() => {
      setTimeout(() => {
        location.reload();
      }, 0);
    });

  }


}
