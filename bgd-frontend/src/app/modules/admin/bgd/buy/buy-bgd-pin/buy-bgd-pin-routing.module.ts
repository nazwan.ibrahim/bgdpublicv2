import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyBgdPinComponent } from 'app/modules/admin/bgd/buy/buy-bgd-pin/buy-bgd-pin.component';

const routes: Routes = [
  {
    path: '',
    component: BuyBgdPinComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyBgdPinRoutingModule { }
