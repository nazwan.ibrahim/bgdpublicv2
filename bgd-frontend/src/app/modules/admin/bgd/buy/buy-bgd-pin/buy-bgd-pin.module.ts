import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { BuyBgdPinComponent } from 'app/modules/admin/bgd/buy/buy-bgd-pin/buy-bgd-pin.component';
import { BuyBgdPinRoutingModule } from 'app/modules/admin/bgd/buy/buy-bgd-pin/buy-bgd-pin-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { NgxPincodeModule } from 'ngx-pincode';

@NgModule({
    declarations: [
        BuyBgdPinComponent,
 
    ],
    imports     : [
        BuyBgdPinRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatIconModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        NgxPincodeModule,
      ]
})
export class BuyBgdPinModule
{
}
