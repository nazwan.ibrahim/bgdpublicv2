"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BuyBgdPinComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var animations_1 = require("@angular/animations");
var buy_bgd_alert_dialog_component_1 = require("../buy-bgd/buy-bgd-alert-dialog/buy-bgd-alert-dialog.component");
var BuyBgdPinComponent = /** @class */ (function () {
    function BuyBgdPinComponent(_activatedRoute, _router, _formBuilder, _dashboardService, bgdService, dialog) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this._dashboardService = _dashboardService;
        this.bgdService = bgdService;
        this.dialog = dialog;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        this.type = "S05";
        this.buyPriceGold = 0;
        this.elapsedTime = 0;
        this.buyprice = 0;
        this.sellprice = 0;
        this.showAlert = false;
        this.setupTransFee = 0;
        this.setupSstFee = 0;
        this.isDialogOpen = false;
        this.disableButton = false;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log("init  ", response);
        this.setupTransUomCode = response[0].feesForms[0].setupUomCode;
        this.setupTransUom = response[0].feesForms[0].setupUom;
        this.transChargeUomCode = response[0].feesForms[0].chargeUomCode;
        this.transChargeUom = response[0].feesForms[0].chargeUom;
        this.feeTransType = response[0].feesForms[0].feeType;
        this.feeTransTypeCode = response[0].feesForms[0].feeTypeCode;
        this.setupTransFee = response[0].feesForms[0].setupFees;
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.SstSetupUomCode = response[0].feesForms[1].setupUomCode;
        this.SstsetupUom = response[0].feesForms[1].setupUom;
        this.SstChargeUomCode = response[0].feesForms[1].chargeUomCode;
        this.SstChargeUom = response[0].feesForms[1].chargeUom;
        this.SstFeeType = response[0].feesForms[1].feeType;
        this.SstFeeTypeCode = response[0].feesForms[1].feeTypeCode;
        this.setupSstFee = response[0].feesForms[1].setupFees;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.quantity = response[0].quantity;
        this.price = response[0].price;
        this.netPrice = response[0].netPrice;
        this.totalPrice = response[0].totalPrice;
        this.inputType = response[0].inputType;
        this.type = response[0].type;
        this.feeTypeCode = response[0].feesForms[1].feeTypeCode;
        this.payablePrice = this.totalPrice + this.chargeSstFee + this.chargeTransFee;
        this.availAmount = this._activatedRoute.snapshot.queryParams['availAmount'];
        this.unitPrice = this._activatedRoute.snapshot.queryParams['unitPrice'];
        this.elapsedTime = this._activatedRoute.snapshot.queryParams['elapsedTime'];
        this.reference = this._activatedRoute.snapshot.queryParams['reference'];
        this.priceRequestId = this._activatedRoute.snapshot.queryParams['priceRequestId'];
        // let quantity = decodeURIComponent(this._activatedRoute.snapshot.queryParams['quantity']);
        // let payablePrice = decodeURIComponent(this._activatedRoute.snapshot.queryParams['payablePrice']);
        // let unitPrice = decodeURIComponent(this._activatedRoute.snapshot.queryParams['unitPrice']);
        // let chargeTransFee = decodeURIComponent(this._activatedRoute.snapshot.queryParams['chargeTransFee']);
        // let chargeSstFee = decodeURIComponent(this._activatedRoute.snapshot.queryParams['chargeSstFee']);
        // let setupTransFee = decodeURIComponent(this._activatedRoute.snapshot.queryParams['setupTransFee']);
        // let setupSstFee = decodeURIComponent(this._activatedRoute.snapshot.queryParams['setupSstFee']);
        // let totalPrice = decodeURIComponent(this._activatedRoute.snapshot.queryParams['totalPrice']);
        // this.totalPrice = parseFloat(totalPrice);
        // this.chargeTransFee = parseFloat(chargeTransFee);
        // this.chargeSstFee = parseFloat(chargeSstFee);
        // this.quantity = parseFloat(quantity);
        // this.payablePrice = parseFloat(payablePrice);
        // this.setupTransFee = parseFloat(setupTransFee);
        // this.setupSstFee = parseFloat(setupSstFee);
        // this.unitPrice = parseFloat(unitPrice);
    }
    BuyBgdPinComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._activatedRoute.queryParamMap.subscribe(function (params) {
            _this.feeTypeCode = params.get('feeTypeCode');
        });
        this._dashboardService.getQueryBuyPrice().pipe(
        //take(1) // take the first message from the WebSocket and complete the subscription
        ).subscribe(function (message) {
            _this.buyPriceGold = (message['A02'].price).toFixed(2);
            // do something with the sellprice value here
            _this.startTimer(); // start the timer after the initial value of sellpriceGold is set
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
        this._dashboardService.getUserDetails().subscribe(function (data) {
            _this.email = data.email;
            _this.username = data.userName;
        });
        // console.log(this.email);
        // console.log(this.username);
        // console.log(this.type);
        // console.log(this.quantity);
        // console.log(this.payablePrice);
        // console.log(this.chargeSstFee);
        // console.log(this.feeTypeCode);
    };
    BuyBgdPinComponent.prototype.pincodeCompleted = function (pin) {
        this.pin = pin;
    };
    BuyBgdPinComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.GoldWsSubscription = this._dashboardService.getQueryBuyPrice().subscribe(function (message) {
            _this.buyPriceGold = (message['A02'].price).toFixed(2);
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    BuyBgdPinComponent.prototype.ngOnDestroy = function () {
        this.GoldWsSubscription.unsubscribe();
        this.stopTimer(); // stop the timer when the component is destroyed
    };
    BuyBgdPinComponent.prototype.startTimer = function () {
        var _this = this;
        // start the timer and subscribe to its events
        var countdownTime = 2 * 60 * 1000; // 2 minutes in milliseconds
        var elapsedTime = this.elapsedTime || countdownTime; // read elapsed time from localStorage or initialize it to countdownTime
        console.log(elapsedTime);
        var startTime = Date.now(); // record the start time of the timer
        this.timerSubscription = rxjs_1.interval(1000).subscribe(function () {
            var elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000); // calculate elapsed time in seconds
            console.log(elapsedTimeInSeconds);
            _this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000; // calculate remaining time
            console.log(_this.elapsedTime);
            localStorage.setItem('elapsedTime', _this.elapsedTime.toString()); // store elapsed time in localStorage
            if (_this.elapsedTime <= 0) {
                _this.stopTimer(); // stop the timer when it reaches 0
                _this.openDialog(); // open the dialog to display the new buy price
                if (!_this.isDialogOpen) {
                    _this.startTimer(); // start the timer again for the next 2 minutes
                }
            }
        });
    };
    BuyBgdPinComponent.prototype.getElapsedTime = function () {
        var minutes = Math.floor(this.elapsedTime / 60000);
        var seconds = Math.floor((this.elapsedTime % 60000) / 1000);
        return minutes.toString().padStart(1, '0') + " min " + seconds.toString().padStart(2, '0') + " sec";
    };
    BuyBgdPinComponent.prototype.stopTimer = function () {
        // stop the timer if it is running
        if (this.timerSubscription && !this.timerSubscription.closed) {
            this.timerSubscription.unsubscribe();
        }
    };
    BuyBgdPinComponent.prototype.openDialog = function () {
        var _this = this;
        this.isDialogOpen = true;
        var dialogRef = this.dialog.open(buy_bgd_alert_dialog_component_1.BuyBgdAlertDialog, {
            data: {
                price: this.buyPriceGold
            }
        });
        dialogRef.afterClosed().subscribe(function () {
            _this.startTimer();
        });
    };
    BuyBgdPinComponent.prototype.purchaseNow = function () {
        //let response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        var _this = this;
        //const requestBody = {
        //   "quantity": 1,
        //     "type": "S06",
        //       "inputType": "S22",
        //         "reference": "F0520230511000213",
        //           "price": 200,
        //             "netPrice": 200,
        //               "totalPrice": 200.4,
        //                 "priceRequestID": "PVBT000005D027",
        //                   "tradingFeeList": [
        //                     {
        //                       "feeType": "SST",
        //                       "feeTypeCode": this.feeTypeCode,
        //                       "setupFees": this.setupSstFee,
        //                       "setupUom": "%",
        //                       "setupUomCode": "string",
        //                       "chargeFees": this.chargeSstFee,
        //                       "chargeUom": "RM",
        //                       "chargeUomCode": "J01"
        //                     }
        //                   ]
        // }
        // {
        //   "reference": "<reference from api queryBuyPrice or querySellPrice>",
        //   "priceRequestID": "<priceRequestID from queryBuyPrice or querySellPrice>",
        //   "type": this.type,
        //   "quantity": this.quantity,
        //   "netPrice": this.payablePrice,
        //   "tradingFeeList": [
        //     {
        //       "feeTypeCode": feeTypeCode,
        //       "chargeFees": this.chargeSstFee,
        //       "chargeUomCode": "J01"
        //     }
        //   ]
        // }
        var requestBody = {
            "quantity": this.quantity,
            "type": this.type,
            "inputType": this.inputType,
            "price": this.price,
            "netPrice": this.netPrice,
            "totalPrice": this.totalPrice,
            "priceRequestID": this.priceRequestId,
            "reference": this.reference,
            "tradingFeeList": [
                {
                    "feeType": this.feeTransType,
                    "feeTypeCode": this.feeTransTypeCode,
                    "setupFees": this.setupTransFee,
                    "setupUom": this.setupTransUom,
                    "setupUomCode": this.setupTransUomCode,
                    "chargeFees": this.chargeTransFee,
                    "chargeUom": this.transChargeUom,
                    "chargeUomCode": this.transChargeUomCode
                },
                {
                    "feeType": this.SstFeeType,
                    "feeTypeCode": this.SstFeeTypeCode,
                    "setupFees": this.setupSstFee,
                    "setupUom": this.SstsetupUom,
                    "setupUomCode": this.SstSetupUomCode,
                    "chargeFees": this.chargeSstFee,
                    "chargeUom": this.SstChargeUom,
                    "chargeUomCode": this.SstChargeUomCode
                }
            ]
        };
        console.log("Response init   ", requestBody);
        var dataJson = JSON.stringify(requestBody);
        this.bgdService.Purchase(dataJson)
            .subscribe(function (Response) {
            console.log("Purchase ", Response);
            var queryParams = {
                response: JSON.stringify(Response),
                requestBody: JSON.stringify(requestBody),
                unitPrice: _this.buyPriceGold
            };
            setTimeout(function () {
                _this._router.navigate(['/buy-bgd-status'], { queryParams: queryParams });
            }, 1000); // 3000 milliseconds = 3 seconds
        }, function (Response) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    BuyBgdPinComponent.prototype.verifyPin = function () {
        var _this = this;
        this.disableButton = true;
        var pin = {
            userName: this.username,
            email: this.email,
            userPin: this.pin
        };
        console.log(pin);
        this.bgdService.verifyPin(pin)
            .subscribe(function (Response) {
            _this.alert = {
                type: 'success',
                message: 'Verified'
            };
            _this.purchaseNow();
        }, function (error) {
            _this.disableButton = false;
            _this.alert = {
                type: 'error',
                message: 'Incorrect PIN.'
            };
            _this.showAlert = true;
        });
        this.showAlert = true;
    };
    BuyBgdPinComponent.prototype.buyPinBack = function () {
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this._router.navigate(['/buy-bgd-confirmation'], {
            queryParams: {
                response: JSON.stringify(response),
                reference: this.reference,
                priceRequestId: this.priceRequestId,
                availAmount: this.availAmount,
                unitPrice: this.buyPriceGold,
                elapsedTime: this.elapsedTime
            }
        });
    };
    BuyBgdPinComponent.prototype.forgotPin = function () {
        this._router.navigate(['/forgot-pin']);
    };
    BuyBgdPinComponent = __decorate([
        core_1.Component({
            selector: 'bgd-buy-bgd-pin',
            templateUrl: './buy-bgd-pin.component.html',
            styleUrls: ['./buy-bgd-pin.component.scss'],
            animations: [
                animations_1.trigger('shake', [
                // Animation configuration
                ])
            ]
        })
    ], BuyBgdPinComponent);
    return BuyBgdPinComponent;
}());
exports.BuyBgdPinComponent = BuyBgdPinComponent;
