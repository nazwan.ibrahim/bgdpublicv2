import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { bgdService } from '../../bgd.service';
import { Subscription, interval, take } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { BuyBgdAlertDialog } from '../buy-bgd/buy-bgd-alert-dialog/buy-bgd-alert-dialog.component';
import { BuyService } from '../buy.service';
import { Location } from '@angular/common';
@Component({
  selector: 'bgd-buy-bgd-pin',
  templateUrl: './buy-bgd-pin.component.html',
  styleUrls: ['./buy-bgd-pin.component.scss'],
  animations: [
    trigger('shake', [
      // Animation configuration
    ])
  ]
})
export class BuyBgdPinComponent {
  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  today: Date = new Date();
  quantity: any;
  totalPrice: any;
  payablePrice: any;
  unitPrice: any;
  netPrice: any;
  chargeTransFee: any;
  chargeSstFee: any;
  type: any = "S05";
  feeTypeCode: any;
  response: any;
  availAmount: any;
  buyPriceGold: number = 0;
  elapsedTime: number = 0;
  buyprice: number = 0;
  sellprice: number = 0;
  email: any;
  username: any;
  pin: any;
  showAlert = false;
  setupTransFee: number = 0;
  setupSstFee: number = 0;
  inputType: any;
  price: any;
  reference: any;
  priceRequestId: any;
  feeTransTypeCode: any;
  feeTransType: any;
  setupTransUomCode: any;
  setupTransUom: any;
  transChargeUomCode: any;
  transChargeUom: any;
  SstSetupUomCode: any;
  SstsetupUom: any;
  SstChargeUomCode: any;
  SstChargeUom: any;
  SstFeeType: any;
  SstFeeTypeCode: any;
  isDialogOpen: boolean = false;
  disableButton: boolean = false;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: UntypedFormBuilder,
    private _dashboardService: DashboardService,
    private bgdService: bgdService,
    private buyService: BuyService,
    public dialog: MatDialog,
    private location: Location

  ) {}

  ngOnInit(): void {

    this.today = new Date();
    this.availAmount = this.buyService.availAmount;
    this.reference = this.buyService.referenceId;
    this.priceRequestId = this.buyService.priceRequestId;
    this.elapsedTime = this.buyService.elapsedTime;

    this.response = this.buyService.initResponse;
    this.setupTransFee = this.response[0].feesForms[0].setupFees;
    this.chargeTransFee = this.response[0].feesForms[0].chargeFees;
    this.setupSstFee = this.response[0].feesForms[1].setupFees;
    this.chargeSstFee = this.response[0].feesForms[1].chargeFees;
    this.feeTypeCode = this.response[0].feesForms[1].feeTypeCode;
    this.quantity = this.response[0].quantity;
    this.totalPrice = this.response[0].totalPrice;
    this.price = this.response[0].price;
    this.netPrice = this.response[0].netPrice;
    this.type = this.response[0].type;
    this.inputType = this.response[0].inputType;

    this._dashboardService.getQueryBuyPrice().pipe(
    ).subscribe(
      (message) => {
        this.buyPriceGold = (message['A02'].price).toFixed(2);
        this.startTimer();
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
    
    this._dashboardService.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    });

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  timerSubscription: Subscription;
  GoldWsSubscription: Subscription;

  ngAfterViewInit() {
    this.GoldWsSubscription = this._dashboardService.getQueryBuyPrice().subscribe(
      (message) => {
        this.buyPriceGold = (message['A02'].price).toFixed(2);
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  ngOnDestroy() {
    this.GoldWsSubscription.unsubscribe();
    this.stopTimer(); 
  }

  startTimer() {
    const countdownTime = 2 * 60 * 1000; 
    let elapsedTime = this.elapsedTime || countdownTime; 

    const startTime = Date.now();
    this.timerSubscription = interval(1000).subscribe(() => { 
      const elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000); 
     
      this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000;
  
      localStorage.setItem('elapsedTime', this.elapsedTime.toString());
      if (this.elapsedTime <= 0) {
        this.stopTimer(); 
        this.openDialog(); 
        if (!this.isDialogOpen) {
          this.startTimer(); 
        }
      }
    });
  }


  getElapsedTime(): string {
    const minutes = Math.floor(this.elapsedTime / 60000);
    const seconds = Math.floor((this.elapsedTime % 60000) / 1000);
    return `${minutes.toString().padStart(1, '0')} min ${seconds.toString().padStart(2, '0')} sec`;
  }

  stopTimer() {
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
  }
  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(BuyBgdAlertDialog, {
      data: {
        price: this.buyPriceGold
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.startTimer();
    });
  }


  purchaseNow(): void {

    const requestBody = 
    {
      "userName": this.username,
      "userPin": this.pin,
      "referenceId": this.reference,
      "priceRequestID": this.priceRequestId
    }
    console.log("Requestbody:", requestBody);

    this.buyService.pin = this.pin;
    this.buyService.userName = this.username;

    const data = JSON.stringify(requestBody);

    this.bgdService.confirmPurchase(data)
      .subscribe(
        Response => {

          console.log("confirmPurchase Response:", Response);

          const response = Response;
          this.buyService.purchaseResponse = response;

          this._router.navigate(['/buy-bgd-status']);

        },
        error => {

          if(error.status === 401) {

            this.alert = {
              type: 'error',
              message: 'Wrong PIN number'
            };
            
          } else {

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };

          }

          this.showAlert = true;

          setTimeout(() => {
            this.showAlert = false;
          }, 1000);

        }
      );
  }


  back() {
    this.location.back();
  }

  forgotPin() {
    this._router.navigate(['/forgot-pin']);
  }

}
