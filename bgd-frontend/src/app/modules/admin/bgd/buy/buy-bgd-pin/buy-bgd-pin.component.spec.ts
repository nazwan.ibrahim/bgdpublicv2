import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyBgdPinComponent } from './buy-bgd-pin.component';

describe('BuyBgdPinComponent', () => {
  let component: BuyBgdPinComponent;
  let fixture: ComponentFixture<BuyBgdPinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyBgdPinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuyBgdPinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
