import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyBgdComponent } from 'app/modules/admin/bgd/buy/buy-bgd/buy-bgd.component';
import { BuyBgdConfirmationComponent } from '../buy-bgd-confirmation/buy-bgd-confirmation.component';

const routes: Routes = [
  {
    path: '',
    component: BuyBgdComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  },
  {
    path: '',
    component: BuyBgdConfirmationComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyBgdRoutingModule { }
