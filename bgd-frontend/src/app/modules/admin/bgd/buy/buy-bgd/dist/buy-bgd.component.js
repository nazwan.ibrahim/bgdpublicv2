"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BuyBgdComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var rxjs_2 = require("rxjs");
var buy_bgd_alert_dialog_component_1 = require("./buy-bgd-alert-dialog/buy-bgd-alert-dialog.component");
var BuyBgdComponent = /** @class */ (function () {
    /**
       * Constructor
       */
    function BuyBgdComponent(_activatedRoute, _router, _bgdService, _formBuilder, _dashboardService, bgdService, dialog) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._bgdService = _bgdService;
        this._formBuilder = _formBuilder;
        this._dashboardService = _dashboardService;
        this.bgdService = bgdService;
        this.dialog = dialog;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.availAmount = 0;
        this.walletModel = [];
        this.buyprice = 0;
        this.sellprice = 0;
        this.totalAmount = 0;
        this.buyPriceGold = 0;
        this.elapsedTime = 0;
        this.disableButton = false;
        this.labelAlertVisible = false;
        this.prevBuyPriceGold = 0;
        this.isDialogOpen = false;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    BuyBgdComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Create the form
        this.buyGoldForm = this._formBuilder.group({
            price: ['', [forms_1.Validators.required]],
            grams: ['', [forms_1.Validators.required]]
        });
        this._dashboardService.getGoldWalletDetails().pipe(operators_1.tap(function (value) {
            if (!value) {
                return rxjs_2.of(null);
            }
            _this.walletModel = value;
            for (var i = 0; i < _this.walletModel.length; i++) {
                if (_this.walletModel[i].type == "Cash") {
                    _this.goldWalletIndex = i;
                    break;
                }
            }
            return _this._dashboardService.getGoldWalletDetails();
        }), operators_1.catchError(function (error) {
            return rxjs_2.of(null);
        })).subscribe(function (data) {
            if (!data) {
                return rxjs_2.of(null);
            }
            _this.availAmount = data[_this.goldWalletIndex].availableAmount.toFixed(6);
        });
        this._dashboardService.getQuerySellPrice().pipe(
        // take(1) // take the first message from the WebSocket and complete the subscription
        ).subscribe(function (message) {
            console.log(message);
            _this.buyPriceGold = (message['A02'].price).toFixed(2);
            _this.priceRequestId = message.priceRequestID;
            _this.reference = message.reference;
            // do something with the sellprice value here
            _this.startTimer(); // start the timer after the initial value of sellpriceGold is set
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    BuyBgdComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.GoldWsSubscription = this._dashboardService.getQuerySellPrice().subscribe(function (message) {
            _this.buyPriceGold = (message['A02'].price).toFixed(2);
            _this.priceRequestId = message.priceRequestID;
            _this.reference = message.reference;
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    BuyBgdComponent.prototype.ngOnDestroy = function () {
        this.GoldWsSubscription.unsubscribe();
        this.stopTimer(); // stop the timer when the component is destroyed
    };
    BuyBgdComponent.prototype.startTimer = function () {
        var _this = this;
        // start the timer and subscribe to its events
        var countdownTime = 2 * 60 * 1000; // 2 minutes in milliseconds
        this.elapsedTime = countdownTime; // initialize the elapsed time
        this.prevBuyPriceGold = this.buyPriceGold; // initialize the previous value of buyPriceGold
        this.timerSubscription = rxjs_2.interval(1000).subscribe(function () {
            _this.elapsedTime -= 1000; // decrement the elapsed time by 1 second
            if (_this.elapsedTime <= 0) {
                _this.stopTimer(); // stop the timer when it reaches 0
                _this.openDialog(); // open the dialog to display the new buy price
                if (!_this.isDialogOpen) {
                    _this.startTimer(); // start the timer again for the next 2 minutes
                }
            }
        });
    };
    BuyBgdComponent.prototype.getElapsedTime = function () {
        var minutes = Math.floor(this.elapsedTime / 60000);
        var seconds = Math.floor((this.elapsedTime % 60000) / 1000);
        return minutes.toString().padStart(1, '0') + " min " + seconds.toString().padStart(2, '0') + " sec";
    };
    BuyBgdComponent.prototype.stopTimer = function () {
        // stop the timer if it is running
        if (this.timerSubscription && !this.timerSubscription.closed) {
            this.timerSubscription.unsubscribe();
        }
    };
    BuyBgdComponent.prototype.openDialog = function () {
        var _this = this;
        this.isDialogOpen = true;
        var dialogRef = this.dialog.open(buy_bgd_alert_dialog_component_1.BuyBgdAlertDialog, {
            data: {
                price: this.buyPriceGold
            }
        });
        dialogRef.afterClosed().subscribe(function () {
            _this.startTimer();
            location.reload();
        });
    };
    BuyBgdComponent.prototype.ngOnChanges = function (changes) {
        if (changes.inputPrice) {
            if (!changes.inputPrice.currentValue) {
                this.resetProperties();
                return;
            }
            this.calculateInput("price");
        }
        else if (changes.inputGram) {
            if (!changes.inputGram.currentValue) {
                this.resetProperties();
                return;
            }
            this.calculateInput("gram");
        }
    };
    BuyBgdComponent.prototype.resetProperties = function () {
        this.inputPrice = null;
        this.inputGram = null;
        this.labelAlertVisible = false;
        this.disableButton = false;
    };
    BuyBgdComponent.prototype.calculateInput = function (inputType) {
        if (inputType === "price") {
            if (!this.inputPrice) {
                this.resetProperties();
                return;
            }
            this.inputGram = this.inputPrice / this.buyPriceGold;
            this.inputType = "S21";
        }
        else if (inputType === "gram") {
            if (!this.inputGram) {
                this.resetProperties();
                return;
            }
            this.inputPrice = this.inputGram * this.buyPriceGold;
            this.inputType = "S22";
        }
        if (this.inputPrice && this.inputGram) {
            var calculatedPrice = this.inputGram * this.buyPriceGold;
            var calculatedGram = this.inputPrice / this.buyPriceGold;
            if (calculatedPrice > this.availAmount) {
                this.labelAlertVisible = true;
                this.disableButton = false;
            }
            else {
                this.labelAlertVisible = false;
                this.disableButton = false;
            }
            this.inputPrice = Number(calculatedPrice.toFixed(2)); // Limit to 2 decimal places
            this.inputGram = Number(calculatedGram.toFixed(6)); // Limit to 6 decimal places
        }
    };
    BuyBgdComponent.prototype.purchaseMethod = function () {
        var _this = this;
        this.disableButton = true;
        if (this.buyGoldForm.invalid) {
            return;
        }
        var calculatedPrice = this.inputGram * this.buyPriceGold;
        if (calculatedPrice > this.availAmount) {
            this.labelAlertVisible = true;
            this.alert = {
                type: 'error',
                message: 'Insufficient cash balance, please try again later.'
            };
            this._router.navigate(['/cash-wallet']);
        }
        else {
            this.buyGoldForm.disable();
            var totalPrice = this.inputPrice;
            var quantity = this.inputGram;
            var type = "S05";
            var inputType = this.inputType;
            var price = this.buyPriceGold;
            console.log(quantity);
            this.bgdService.initPurchase(totalPrice, quantity, type, inputType, price)
                .pipe(rxjs_1.finalize(function () {
                _this.buyGoldForm.enable();
                _this.buyGoldForm.reset();
            }))
                .subscribe(function (Response) {
                console.log("initPurchase Response    ", Response);
                _this.alert = {
                    type: 'success',
                    message: 'Success'
                };
                _this.response = Response;
                _this._router.navigate(['/buy-bgd-confirmation'], { queryParams: { response: JSON.stringify(_this.response), reference: _this.reference, priceRequestId: _this.priceRequestId, availAmount: _this.availAmount, unitPrice: _this.buyPriceGold, elapsedTime: _this.elapsedTime, inputType: _this.inputType } });
            }, function (error) {
                _this.disableButton = false;
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
                _this._router.navigate(['/transaction-limit']);
            });
        }
    };
    __decorate([
        core_1.ViewChild('buyGoldngForm')
    ], BuyBgdComponent.prototype, "buyGoldngForm");
    BuyBgdComponent = __decorate([
        core_1.Component({
            selector: 'bgd-buy-bgd',
            templateUrl: './buy-bgd.component.html',
            styleUrls: ['./buy-bgd.component.scss']
        })
    ], BuyBgdComponent);
    return BuyBgdComponent;
}());
exports.BuyBgdComponent = BuyBgdComponent;
