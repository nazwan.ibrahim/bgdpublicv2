import { AfterViewInit, Component, OnDestroy, OnInit, SimpleChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { switchMap, tap, map, catchError, take } from 'rxjs/operators';
import { of, Subscription, interval } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { BuyBgdAlertDialog } from './buy-bgd-alert-dialog/buy-bgd-alert-dialog.component';
import { BuyService } from '../buy.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-buy-bgd',
  templateUrl: './buy-bgd.component.html',
  styleUrls: ['./buy-bgd.component.scss']
})
export class BuyBgdComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('buyGoldngForm') buyGoldngForm: NgForm;


  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  buyGoldForm: UntypedFormGroup;
  showAlert = false;
  availAmount: number = 0;
  walletModel: any[] = [];
  goldWalletIndex: number;
  buyprice: number = 0;
  sellprice: number = 0;
  totalAmount: number = 0;
  buyPriceGold: number = 0;
  elapsedTime: number = 0;
  inputPrice: any;
  inputGram: any;
  inputType: any;
  response: any;
  disableButton: boolean = false;
  labelAlertVisible: boolean = false;
  prevBuyPriceGold: number = 0;
  isDialogOpen: boolean = false;
  reference: any;
  priceRequestId: any;

  /**
     * Constructor
     */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: UntypedFormBuilder,
    private _dashboardService: DashboardService,
    private bgdService: bgdService,
    private buyService: BuyService,
    public dialog: MatDialog,
    private location: Location
  ) { }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {

    //Buy form
    this.buyGoldForm = this._formBuilder.group({
      price: ['', [Validators.required]],
      grams: ['', [Validators.required]],
    });

    //Get cash wallet details from API
    this._dashboardService.getGoldWalletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Cash") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._dashboardService.getGoldWalletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }
      this.buyService.availAmount = data[this.goldWalletIndex].availableAmount.toFixed(6);
      this.availAmount = this.buyService.availAmount;
    });

    //Get sell price from API
    this._dashboardService.getQuerySellPrice().pipe(
    ).subscribe(
      (message) => {

        console.log("Query Sell Price:", message);

        this.buyPriceGold = (message['A02'].price).toFixed(2);
        this.buyService.buyPriceGold = this.buyPriceGold;

        this.priceRequestId = message.priceRequestID;
        this.buyService.priceRequestId = this.priceRequestId;

        this.reference = message.reference;
        this.buyService.referenceId = this.reference;

        this.startTimer();

      },
      (error) => {

        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };

      }
    );
  }

  timerSubscription: Subscription;
  GoldWsSubscription: Subscription;


  ngAfterViewInit() {
    this.GoldWsSubscription = this._dashboardService.getQuerySellPrice().subscribe(
      (message) => {
        this.buyService.buyPriceGold = (message['A02'].price).toFixed(2);
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  ngOnDestroy() {
    this.GoldWsSubscription.unsubscribe();
    this.stopTimer();
  }

  startTimer() {
    
    const countdownTime = 2 * 60 * 1000;

    this.elapsedTime = countdownTime;
    this.prevBuyPriceGold = this.buyPriceGold;

    this.timerSubscription = interval(1000).subscribe(() => { 

      this.elapsedTime -= 1000;

      if (this.elapsedTime <= 0) {

        this.stopTimer();
        this.openDialog();

        if (!this.isDialogOpen) {

          this.startTimer();

        }
      }
    });
  }

  getElapsedTime(): string {

    const minutes = Math.floor(this.elapsedTime / 60000);
    const seconds = Math.floor((this.elapsedTime % 60000) / 1000);

    return `${minutes.toString().padStart(1, '0')} min ${seconds.toString().padStart(2, '0')} sec`;

  }

  stopTimer() {
    
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }

  }

  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(BuyBgdAlertDialog, {
      data: {
        price: this.buyPriceGold
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.startTimer();
      location.reload();
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.inputPrice) {
      if (!changes.inputPrice.currentValue) {
        this.resetProperties();
        return;
      }
      this.calculateInput("price");
    } else if (changes.inputGram) {
      if (!changes.inputGram.currentValue) {
        this.resetProperties();
        return;
      }
      this.calculateInput("gram");
    }
  }

  resetProperties() {
    this.inputPrice = null;
    this.inputGram = null;
    this.labelAlertVisible = false;
    this.disableButton = false;
  }

  calculateInput(inputType: "price" | "gram") {
    
    if (inputType === "price") {
      if (!this.inputPrice) {
        this.resetProperties();
        return;
      }
      this.inputGram = this.inputPrice / this.buyPriceGold;
      this.inputType = "S21";
    } else if (inputType === "gram") {
      if (!this.inputGram) {
        this.resetProperties();
        return;
      }
      this.inputPrice = this.inputGram * this.buyPriceGold;
      this.inputType = "S22";
    }

    if (this.inputPrice && this.inputGram) {
      const calculatedPrice = this.inputGram * this.buyService.buyPriceGold;
      const calculatedGram = this.inputPrice / this.buyService.buyPriceGold;

      if (calculatedPrice >= this.availAmount) {
        this.labelAlertVisible = true;
        this.disableButton = true;
      } else if (calculatedPrice < 10) {
        this.disableButton = true;
      } else {
        this.labelAlertVisible = false;
        this.disableButton = false;
      }

      this.inputPrice = Number(calculatedPrice.toFixed(2)); 
      this.inputGram = Number(calculatedGram.toFixed(6));
    }
  }

  back() {
    this.location.back();
  }

  purchaseMethod() {

    this.disableButton = true;

    if (this.buyGoldForm.invalid) {
      return;
    }

      this.buyGoldForm.disable();

      const totalPrice = this.inputPrice;
      const quantity = this.inputGram;
      const type = "S05";
      const inputType = this.inputType;
      const price = this.buyPriceGold;
      const reference = this.reference;

      this.bgdService.initPurchase(totalPrice, quantity, type, inputType, price, reference)
        .pipe(
          finalize(() => {
            this.buyGoldForm.enable();
            this.buyGoldForm.reset();
          })
        )
        .subscribe(
          Response => {

            console.log("initPurchase Response:", Response);

            this.alert = {
              type: 'success',
              message: 'Success'
            };

            this.response = Response;
            this.buyService.initResponse = this.response;
            this.buyService.elapsedTime = this.elapsedTime;

            this._router.navigate(['/buy-bgd-confirmation']);
          },
          error => {

            this.disableButton = false;

            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };

            this._router.navigate(['/transaction-limit']);

          }
        )
  }
}
