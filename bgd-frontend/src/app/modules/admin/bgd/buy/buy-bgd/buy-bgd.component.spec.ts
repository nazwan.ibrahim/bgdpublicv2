import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyBgdComponent } from './buy-bgd.component';

describe('BuyBgdComponent', () => {
  let component: BuyBgdComponent;
  let fixture: ComponentFixture<BuyBgdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyBgdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuyBgdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
