import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { BuyBgdComponent } from 'app/modules/admin/bgd/buy/buy-bgd/buy-bgd.component';
import { BuyBgdRoutingModule } from 'app/modules/admin/bgd/buy/buy-bgd/buy-bgd-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';




@NgModule({
    declarations: [
        BuyBgdComponent,

    ],
    imports: [
        BuyBgdRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class BuyBgdModule {
}

