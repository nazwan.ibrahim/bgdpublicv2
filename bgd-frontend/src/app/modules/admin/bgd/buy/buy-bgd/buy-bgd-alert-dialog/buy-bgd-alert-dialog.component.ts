import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { bgdService } from '../../../bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { FuseAlertType } from '@bgd/components/alert';
import { filter } from 'rxjs/operators';

export interface DialogData {
  totalPrice: any;
  quantity: any;
  type: any;
  inputType: any;
  price: any;
}

/**
 * @title Dialog elements
 */
@Component({
  selector: 'buy-bgd-alert-dialog',
  templateUrl: 'buy-bgd-alert-dialog.component.html',
})
export class BuyBgdAlertDialog {

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private bgdService: bgdService,
    private _dashboardService: DashboardService,
    public dialogRef: MatDialogRef<BuyBgdAlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) { }

  ngOnInit(): void {
    this._router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        if (event.urlAfterRedirects === '/sell-bgd') {
          location.reload();
        }
      });
  }
  

  onProceed(): void {
    this.dialogRef.close();
    this._router.navigate(['/buy-bgd']);
  }
  

  onCancel(): void {
    this.dialogRef.close();
    this._router.navigate(['/dashboard']).then(() => {
      setTimeout(() => {
        location.reload();
      }, 0);
    });

  }
}
