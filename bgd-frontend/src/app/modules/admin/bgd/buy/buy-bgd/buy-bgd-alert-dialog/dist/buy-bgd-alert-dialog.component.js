"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.BuyBgdAlertDialog = void 0;
var core_1 = require("@angular/core");
var dialog_1 = require("@angular/material/dialog");
/**
 * @title Dialog elements
 */
var BuyBgdAlertDialog = /** @class */ (function () {
    function BuyBgdAlertDialog(_router, _activatedRoute, bgdService, _dashboardService, dialogRef, data) {
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this.bgdService = bgdService;
        this._dashboardService = _dashboardService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        this.type = "S06";
        this.buyPriceGold = 0;
        this.elapsedTime = 0;
        this.buyprice = 0;
        this.sellprice = 0;
        this.setupTransFee = 0;
        this.setupSstFee = 0;
        this.isDialogOpen = false;
    }
    BuyBgdAlertDialog.prototype.onProceed = function () {
        // this.getData();
        this.dialogRef.close();
        this._router.navigate(['/buy-bgd-confirmation']);
        location.reload();
    };
    BuyBgdAlertDialog.prototype.onCancel = function () {
        this.dialogRef.close();
        this._router.navigate(['/dashboard']).then(function () {
            setTimeout(function () {
                location.reload();
            }, 0);
        });
    };
    BuyBgdAlertDialog = __decorate([
        core_1.Component({
            selector: 'buy-bgd-alert-dialog',
            templateUrl: 'buy-bgd-alert-dialog.component.html'
        }),
        __param(5, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], BuyBgdAlertDialog);
    return BuyBgdAlertDialog;
}());
exports.BuyBgdAlertDialog = BuyBgdAlertDialog;
