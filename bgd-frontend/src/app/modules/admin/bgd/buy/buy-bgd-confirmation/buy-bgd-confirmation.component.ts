import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { bgdService } from '../../bgd.service';
import { Subscription, interval, take } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { BuyBgdConfirmationAlertDialogComponent } from './buy-bgd-confirmation-alert-dialog/buy-bgd-confirmation-alert-dialog.component';
import { BuyService } from '../buy.service';
import { Location } from '@angular/common';

@Component({
  selector: 'bgd-buy-bgd-confirmation',
  templateUrl: './buy-bgd-confirmation.component.html',
  styleUrls: ['./buy-bgd-confirmation.component.scss']
})
export class BuyBgdConfirmationComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };
  today: Date = new Date();
  quantity: any;
  totalPrice: any;
  payablePrice: any;
  unitPrice: any;
  netPrice: any;
  chargeTransFee: any;
  chargeSstFee: any;
  type: any = "S06";
  feeTypeCode: any;
  response: any;
  availAmount: any;
  buyPriceGold: number = 0;
  elapsedTime: number = 0;
  buyprice: number = 0;
  sellprice: number = 0;
  setupTransFee: number = 0;
  setupSstFee: number = 0;
  priceRequestId: any;
  reference: any;
  inputPrice: any;
  inputGram: any;
  inputType: any;
  isDialogOpen: boolean = false;
  price: any;
  disableButton: boolean = false;
  labelAlertVisible: boolean = false;
  labelAlertHidden: boolean = true;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: UntypedFormBuilder,
    private _dashboardService: DashboardService,
    private buyService: BuyService,
    public dialog: MatDialog,
    private location: Location

  ) {}

  ngOnInit(): void {

    this.today = new Date();
    this.availAmount = this.buyService.availAmount;
    this.reference = this.buyService.referenceId;
    this.priceRequestId = this.buyService.priceRequestId;
    this.elapsedTime = this.buyService.elapsedTime;

    this.response = this.buyService.initResponse;
    this.setupTransFee = this.response[0].feesForms[0].setupFees;
    this.chargeTransFee = this.response[0].feesForms[0].chargeFees;
    this.setupSstFee = this.response[0].feesForms[1].setupFees;
    this.chargeSstFee = this.response[0].feesForms[1].chargeFees;
    this.feeTypeCode = this.response[0].feesForms[1].feeTypeCode;
    this.quantity = this.response[0].quantity;
    this.totalPrice = this.response[0].totalPrice;
    this.price = this.response[0].price;
    this.netPrice = this.response[0].netPrice;
    this.type = this.response[0].type;
    this.inputType = this.response[0].inputType;

    this._dashboardService.getQuerySellPrice().pipe(
    ).subscribe(
      (message) => {
        this.buyPriceGold = (message['A02'].price).toFixed(2);
        this.startTimer(); 
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  timerSubscription: Subscription;
  GoldWsSubscription: Subscription;

  ngAfterViewInit() {
    this.GoldWsSubscription = this._dashboardService.getQuerySellPrice().subscribe(
      (message) => {
        this.buyPriceGold = (message['A02'].price).toFixed(2);
      },
      (error) => {
        this.alert = {
          type: 'error',
          message: 'Something went wrong, please try again.'
        };
      }
    );
  }

  ngOnDestroy() {
    this.GoldWsSubscription.unsubscribe();
    this.stopTimer();
  }

  startTimer() {
    const countdownTime = 2 * 60 * 1000; 
    let elapsedTime = this.elapsedTime || countdownTime; 
    
    const startTime = Date.now();
    this.timerSubscription = interval(1000).subscribe(() => { 
      const elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000); 
      
      this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000;
      
      localStorage.setItem('elapsedTime', this.elapsedTime.toString());
      if (this.elapsedTime <= 0) {
        this.stopTimer();
        this.openDialog();
        if (!this.isDialogOpen) {
          this.startTimer();
        }
      }
    });
  }

  getElapsedTime(): string {
    const minutes = Math.floor(this.elapsedTime / 60000);
    const seconds = Math.floor((this.elapsedTime % 60000) / 1000);
    return `${minutes.toString().padStart(1, '0')} min ${seconds.toString().padStart(2, '0')} sec`;
  }

  stopTimer() {
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
  }

  openDialog(): void {
    this.isDialogOpen = true;
    this.labelAlertVisible = false;
    this.labelAlertHidden = false;

    const dialogRef = this.dialog.open(BuyBgdConfirmationAlertDialogComponent, {
      data: {
        totalPrice: this.totalPrice,
        quantity: this.quantity,
        type: this.type,
        inputType: this.inputType,
        price: this.buyPriceGold,
        availAmount: this.availAmount,
        labelAlertVisible: this.labelAlertVisible,
        labelAlertHidden: this.labelAlertHidden
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this.startTimer();
    });
  }

  topupNow(): void {

    const response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    this._router.navigate(['/cash-wallet-topup'], {
      queryParams: {
        response: JSON.stringify(response), 
        reference: this.reference, 
        priceRequestId: this.priceRequestId, 
        availAmount: this.availAmount, 
        unitPrice: this.buyPriceGold, 
        elapsedTime: this.elapsedTime
      }
    });
  }

  purchaseNow() {

    this.disableButton = true;

    this._router.navigate(['/buy-bgd-pin']);

    this.disableButton = false;
  }

  back() {
    this.location.back();
  }

}
