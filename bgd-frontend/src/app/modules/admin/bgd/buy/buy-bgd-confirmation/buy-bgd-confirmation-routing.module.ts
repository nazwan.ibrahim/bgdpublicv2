import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyBgdConfirmationComponent } from 'app/modules/admin/bgd/buy/buy-bgd-confirmation/buy-bgd-confirmation.component';

const routes: Routes = [
  {
    path: '',
    component: BuyBgdConfirmationComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyBgdConfirmationRoutingModule { }
