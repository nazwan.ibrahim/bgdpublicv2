import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { BuyBgdConfirmationComponent } from 'app/modules/admin/bgd/buy/buy-bgd-confirmation/buy-bgd-confirmation.component';
import { BuyBgdConfirmationRoutingModule } from 'app/modules/admin/bgd/buy/buy-bgd-confirmation/buy-bgd-confirmation-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
    declarations: [
        BuyBgdConfirmationComponent,

    ],
    imports: [
        BuyBgdConfirmationRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatIconModule
    ]
})
export class BuyBgdConfirmationModule {
}
