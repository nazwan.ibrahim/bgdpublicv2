import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { bgdService } from '../../../bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';

export interface DialogData {
  totalPrice: any;
  quantity: any;
  type: any;
  inputType: any;
  price: any;
  availAmount: any;
  labelAlertVisible: boolean;
  labelAlertHidden: boolean;
}

@Component({
  selector: 'bgd-buy-bgd-confirmation-alert-dialog',
  templateUrl: './buy-bgd-confirmation-alert-dialog.component.html',
  styleUrls: ['./buy-bgd-confirmation-alert-dialog.component.scss']
})
export class BuyBgdConfirmationAlertDialogComponent {

  response: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private bgdService: bgdService,
    private _dashboardService: DashboardService,
    public dialogRef: MatDialogRef<BuyBgdConfirmationAlertDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) { }

  onProceed() {
    this._dashboardService.getQuerySellPrice().subscribe((message) => {
      const buyPriceGold = message['A02'].price.toFixed(2);
      const priceRequestId = message.priceRequestID;
      const reference = message.reference;

      if(this.data.inputType === "S21"){
        this.data.quantity = this.data.totalPrice / buyPriceGold;
      } else if(this.data.inputType === "S22"){
        this.data.totalPrice = this.data.quantity * buyPriceGold;
      }

      console.log(this.data.labelAlertVisible);
      console.log(this.data.totalPrice);
      console.log(this.data.availAmount);	

      if(this.data.totalPrice > this.data.availAmount) {
        this.data.labelAlertVisible = true;
        this.data.labelAlertHidden = false;
      }

      console.log(this.data.labelAlertVisible);
      console.log(this.data.labelAlertHidden);
  
      this.bgdService.initPurchase(this.data.totalPrice, this.data.quantity, this.data.type, this.data.inputType, buyPriceGold, reference)
        .subscribe((response) => {
          console.log("initPurchase:", response);
          this.dialogRef.close();
          this._router.navigate(['/buy-bgd-confirmation'], 
            { queryParams: { 
                response: JSON.stringify(response),
                availAmount: this.data.availAmount,
                reference: reference,
                priceRequestId: priceRequestId,
                labelAlertVisible: this.data.labelAlertVisible
              } 
            });
        });
    });
  }

  onCancel() {
    this.dialogRef.close();
    this._router.navigate(['/dashboard']).then(() => {
      setTimeout(() => {
        location.reload();
      }, 0);
    });

  }

}
