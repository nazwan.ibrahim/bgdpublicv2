"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BuyBgdConfirmationComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var buy_bgd_alert_dialog_component_1 = require("../buy-bgd/buy-bgd-alert-dialog/buy-bgd-alert-dialog.component");
var BuyBgdConfirmationComponent = /** @class */ (function () {
    function BuyBgdConfirmationComponent(_activatedRoute, _router, _formBuilder, _dashboardService, bgdService, dialog) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this._dashboardService = _dashboardService;
        this.bgdService = bgdService;
        this.dialog = dialog;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        this.type = "S06";
        this.buyPriceGold = 0;
        this.elapsedTime = 0;
        this.buyprice = 0;
        this.sellprice = 0;
        this.setupTransFee = 0;
        this.setupSstFee = 0;
        this.isDialogOpen = false;
        this.disableButton = false;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log("Confirm Response   ", response);
        this.setupTransFee = response[0].feesForms[0].setupFees;
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.setupSstFee = response[0].feesForms[1].setupFees;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.quantity = response[0].quantity;
        this.totalPrice = response[0].totalPrice;
        this.price = response[0].price;
        this.netPrice = response[0].netPrice;
        this.type = response[0].type;
        this.feeTypeCode = response[0].feesForms[1].feeTypeCode;
        this.availAmount = this._activatedRoute.snapshot.queryParams['availAmount'];
        this.unitPrice = this._activatedRoute.snapshot.queryParams['unitPrice'];
        this.elapsedTime = this._activatedRoute.snapshot.queryParams['elapsedTime'];
        this.reference = this._activatedRoute.snapshot.queryParams['reference'];
        this.priceRequestId = this._activatedRoute.snapshot.queryParams['priceRequestId'];
        this.inputType = this._activatedRoute.snapshot.queryParams['inputType'];
        console.log("Confirm Response   ", this.reference);
        console.log("Confirm Response   ", this.priceRequestId);
    }
    BuyBgdConfirmationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._dashboardService.getQuerySellPrice().pipe(
        //take(1) // take the first message from the WebSocket and complete the subscription
        ).subscribe(function (message) {
            _this.buyPriceGold = (message['A02'].price).toFixed(2);
            // do something with the sellprice value here
            _this.startTimer(); // start the timer after the initial value of sellpriceGold is set
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    BuyBgdConfirmationComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.GoldWsSubscription = this._dashboardService.getQuerySellPrice().subscribe(function (message) {
            _this.buyPriceGold = (message['A02'].price).toFixed(2);
        }, function (error) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    BuyBgdConfirmationComponent.prototype.ngOnDestroy = function () {
        this.GoldWsSubscription.unsubscribe();
        this.stopTimer(); // stop the timer when the component is destroyed
    };
    BuyBgdConfirmationComponent.prototype.startTimer = function () {
        var _this = this;
        // start the timer and subscribe to its events
        var countdownTime = 2 * 60 * 1000; // 2 minutes in milliseconds
        var elapsedTime = this.elapsedTime || countdownTime; // read elapsed time from localStorage or initialize it to countdownTime
        console.log(elapsedTime);
        var startTime = Date.now(); // record the start time of the timer
        this.timerSubscription = rxjs_1.interval(1000).subscribe(function () {
            var elapsedTimeInSeconds = Math.round((Date.now() - startTime) / 1000); // calculate elapsed time in seconds
            console.log(elapsedTimeInSeconds);
            _this.elapsedTime = elapsedTime - elapsedTimeInSeconds * 1000; // calculate remaining time
            console.log(_this.elapsedTime);
            localStorage.setItem('elapsedTime', _this.elapsedTime.toString()); // store elapsed time in localStorage
            if (_this.elapsedTime <= 0) {
                _this.stopTimer(); // stop the timer when it reaches 0
                _this.openDialog(); // open the dialog to display the new buy price
                if (!_this.isDialogOpen) {
                    _this.startTimer(); // start the timer again for the next 2 minutes
                }
            }
        });
    };
    BuyBgdConfirmationComponent.prototype.getElapsedTime = function () {
        var minutes = Math.floor(this.elapsedTime / 60000);
        var seconds = Math.floor((this.elapsedTime % 60000) / 1000);
        return minutes.toString().padStart(1, '0') + " min " + seconds.toString().padStart(2, '0') + " sec";
    };
    BuyBgdConfirmationComponent.prototype.stopTimer = function () {
        // stop the timer if it is running
        if (this.timerSubscription && !this.timerSubscription.closed) {
            this.timerSubscription.unsubscribe();
        }
    };
    BuyBgdConfirmationComponent.prototype.openDialog = function () {
        var _this = this;
        this.isDialogOpen = true;
        var dialogRef = this.dialog.open(buy_bgd_alert_dialog_component_1.BuyBgdAlertDialog, {
            data: {
                price: this.buyPriceGold
            }
        });
        dialogRef.afterClosed().subscribe(function () {
            _this.startTimer();
        });
    };
    BuyBgdConfirmationComponent.prototype.purchaseNow = function () {
        this.disableButton = true;
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        this._router.navigate(['/buy-bgd-pin'], {
            queryParams: {
                response: JSON.stringify(response), reference: this.reference, priceRequestId: this.priceRequestId, availAmount: this.availAmount, unitPrice: this.buyPriceGold, elapsedTime: this.elapsedTime
            }
        });
        this.disableButton = false;
    };
    BuyBgdConfirmationComponent.prototype.buyConfirmBack = function () {
        this._router.navigate(['/buy-bgd']);
    };
    BuyBgdConfirmationComponent = __decorate([
        core_1.Component({
            selector: 'bgd-buy-bgd-confirmation',
            templateUrl: './buy-bgd-confirmation.component.html',
            styleUrls: ['./buy-bgd-confirmation.component.scss']
        })
    ], BuyBgdConfirmationComponent);
    return BuyBgdConfirmationComponent;
}());
exports.BuyBgdConfirmationComponent = BuyBgdConfirmationComponent;
