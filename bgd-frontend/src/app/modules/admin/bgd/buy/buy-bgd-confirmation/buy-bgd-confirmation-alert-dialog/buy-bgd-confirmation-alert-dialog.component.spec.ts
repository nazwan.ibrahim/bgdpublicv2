import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyBgdConfirmationAlertDialogComponent } from './buy-bgd-confirmation-alert-dialog.component';

describe('BuyBgdConfirmationAlertDialogComponent', () => {
  let component: BuyBgdConfirmationAlertDialogComponent;
  let fixture: ComponentFixture<BuyBgdConfirmationAlertDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyBgdConfirmationAlertDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuyBgdConfirmationAlertDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
