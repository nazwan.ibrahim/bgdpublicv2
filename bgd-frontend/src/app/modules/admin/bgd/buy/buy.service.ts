import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BuyService {

  availAmount: any;
  buyPriceGold: any;
  priceRequestId: any;
  referenceId: any;
  elapsedTime: any;
  pin: any;
  userName: any;

  initResponse: any;
  purchaseResponse: any;

  constructor() { }
}
