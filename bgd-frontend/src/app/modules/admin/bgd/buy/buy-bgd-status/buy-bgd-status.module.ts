import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { BuyBgdStatusComponent } from 'app/modules/admin/bgd/buy/buy-bgd-status/buy-bgd-status.component';
import { BuyBgdStatusRoutingModule } from 'app/modules/admin/bgd/buy/buy-bgd-status/buy-bgd-status-routing.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
    declarations: [
        BuyBgdStatusComponent,
 
    ],
    imports     : [
        BuyBgdStatusRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
      ]
})
export class BuyBgdStatusModule
{
}
