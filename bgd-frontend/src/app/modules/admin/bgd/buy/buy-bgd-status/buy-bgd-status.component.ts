import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from '../../bgd.service';
import { saveAs } from 'file-saver';
import { BuyService } from '../buy.service';

@Component({
  selector: 'bgd-buy-bgd-status',
  templateUrl: './buy-bgd-status.component.html',
  styleUrls: ['./buy-bgd-status.component.scss']
})
export class BuyBgdStatusComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };
  today: Date = new Date();
  chargeTransFee: any;
  chargeSstFee: any;
  quantity: any;
  totalPrice: any;
  type: any;
  unitPrice: any;
  netPrice: any;
  price: any;
  referenceNumber: any;
  setupTransFee: any;
  setupSstFee: any;
  referenceId: any;
  transactionTypeCode: any;
  response: any;
  feeTypeCode: any;
  inputType: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: UntypedFormBuilder,
    private bgdService: bgdService,
    private buyService: BuyService
  ) {}

  ngOnInit(): void {

    this.response = this.buyService.initResponse;
    this.setupTransFee = this.response[0].feesForms[0].setupFees;
    this.chargeTransFee = this.response[0].feesForms[0].chargeFees;
    this.setupSstFee = this.response[0].feesForms[1].setupFees;
    this.chargeSstFee = this.response[0].feesForms[1].chargeFees;
    this.feeTypeCode = this.response[0].feesForms[1].feeTypeCode;
    this.quantity = this.response[0].quantity;
    this.totalPrice = this.response[0].totalPrice;
    this.price = this.response[0].price;
    this.netPrice = this.response[0].netPrice;
    this.type = this.response[0].type;
    this.inputType = this.response[0].inputType;
    this.referenceNumber = this.response[0].reference;

    this.unitPrice = this.buyService.buyPriceGold;
    this.referenceId = this.buyService.referenceId;
  }

  downloadReceipt() {

    this.bgdService.downloadReceipt(this.referenceNumber, 'F05').subscribe(
      (response: any) => {
        console.log("Response Do", response);
        // Handle the response
        const blob = new Blob([response], { type: 'application/pdf' });
        saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        // Handle any errors that occur during the API request
        console.error('Error downloading receipt:', error);
      }
    );
  }

}
