import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyBgdStatusComponent } from 'app/modules/admin/bgd/buy/buy-bgd-status/buy-bgd-status.component';

const routes: Routes = [
  {
    path: '',
    component: BuyBgdStatusComponent
    // resolve  : {
    //   data: BuyBgdResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyBgdStatusRoutingModule { }
