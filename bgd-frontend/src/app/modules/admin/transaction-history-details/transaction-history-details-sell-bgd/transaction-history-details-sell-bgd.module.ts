import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TransactionHistoryDetailsSellBgdComponent } from './transaction-history-details-sell-bgd.component';
import { TransactionHistoryDetailsSellBgdRoutingModule } from './transaction-history-details-sell-bgd-routing.module';


@NgModule({
  declarations: [
    TransactionHistoryDetailsSellBgdComponent
  ],
  imports: [
    TransactionHistoryDetailsSellBgdRoutingModule,
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class TransactionHistoryDetailsSellBgdModule { }
