import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionHistoryDetailsSellBgdComponent } from './transaction-history-details-sell-bgd.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionHistoryDetailsSellBgdComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionHistoryDetailsSellBgdRoutingModule { }
