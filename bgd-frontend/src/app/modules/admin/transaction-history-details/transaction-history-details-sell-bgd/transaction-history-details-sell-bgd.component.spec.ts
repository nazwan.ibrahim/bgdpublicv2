import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionHistoryDetailsSellBgdComponent } from './transaction-history-details-sell-bgd.component';

describe('TransactionHistoryDetailsSellBgdComponent', () => {
  let component: TransactionHistoryDetailsSellBgdComponent;
  let fixture: ComponentFixture<TransactionHistoryDetailsSellBgdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionHistoryDetailsSellBgdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransactionHistoryDetailsSellBgdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
