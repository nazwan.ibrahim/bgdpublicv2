import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionHistoryDetailsComponent } from './transaction-history-details.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionHistoryDetailsComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionHistoryDetailsRoutingModule { }
