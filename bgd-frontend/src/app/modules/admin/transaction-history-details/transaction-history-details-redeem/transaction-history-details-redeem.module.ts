import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TransactionHistoryDetailsRedeemRoutingModule } from './transaction-history-details-redeem-routing.module';
import { TransactionHistoryDetailsRedeemComponent } from './transaction-history-details-redeem.component';


@NgModule({
  declarations: [
    TransactionHistoryDetailsRedeemComponent
  ],
  imports: [
    TransactionHistoryDetailsRedeemRoutingModule,
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class TransactionHistoryDetailsRedeemModule { }
