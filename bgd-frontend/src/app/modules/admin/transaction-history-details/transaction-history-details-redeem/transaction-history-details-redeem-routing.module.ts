import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionHistoryDetailsRedeemComponent } from './transaction-history-details-redeem.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionHistoryDetailsRedeemComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionHistoryDetailsRedeemRoutingModule { }
