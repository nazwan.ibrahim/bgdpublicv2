import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionHistoryDetailsRedeemComponent } from './transaction-history-details-redeem.component';

describe('TransactionHistoryDetailsRedeemComponent', () => {
  let component: TransactionHistoryDetailsRedeemComponent;
  let fixture: ComponentFixture<TransactionHistoryDetailsRedeemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionHistoryDetailsRedeemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransactionHistoryDetailsRedeemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
