import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionHistoryDetailsTransferComponent } from './transaction-history-details-transfer.component';

describe('TransactionHistoryDetailsTransferComponent', () => {
  let component: TransactionHistoryDetailsTransferComponent;
  let fixture: ComponentFixture<TransactionHistoryDetailsTransferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionHistoryDetailsTransferComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransactionHistoryDetailsTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
