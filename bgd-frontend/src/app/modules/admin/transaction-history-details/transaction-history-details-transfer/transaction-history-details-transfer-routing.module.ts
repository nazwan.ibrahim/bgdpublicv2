import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionHistoryDetailsTransferComponent } from './transaction-history-details-transfer.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionHistoryDetailsTransferComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionHistoryDetailsTransferRoutingModule { }
