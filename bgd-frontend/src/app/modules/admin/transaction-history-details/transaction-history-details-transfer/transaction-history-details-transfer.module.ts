import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TransactionHistoryDetailsTransferRoutingModule } from './transaction-history-details-transfer-routing.module';
import { TransactionHistoryDetailsTransferComponent } from './transaction-history-details-transfer.component';


@NgModule({
  declarations: [
    TransactionHistoryDetailsTransferComponent
  ],
  imports: [
    TransactionHistoryDetailsTransferRoutingModule,
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class TransactionHistoryDetailsTransferModule { }
