import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionHistoryDetailsComponent } from './transaction-history-details.component';

describe('TransactionHistoryDetailsComponent', () => {
  let component: TransactionHistoryDetailsComponent;
  let fixture: ComponentFixture<TransactionHistoryDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionHistoryDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransactionHistoryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
