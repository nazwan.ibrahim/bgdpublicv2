import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionHistoryDetailsWithdrawComponent } from './transaction-history-details-withdraw.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionHistoryDetailsWithdrawComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionHistoryDetailsWithdrawRoutingModule { }
