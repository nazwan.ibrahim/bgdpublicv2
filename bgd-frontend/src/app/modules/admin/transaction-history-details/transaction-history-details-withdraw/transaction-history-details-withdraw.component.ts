import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';

@Component({
  selector: 'bgd-transaction-history-details-withdraw',
  templateUrl: './transaction-history-details-withdraw.component.html',
  styleUrls: ['./transaction-history-details-withdraw.component.scss']
})
export class TransactionHistoryDetailsWithdrawComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
};

  today: Date = new Date();
  referenceId: string = '';
  transactionType: string = '';
  displaytransactionType: string = '';
  date: any;
  price: any;
  amount: any;
  transactionAmount: any;
  status: any;
  statusCode: any;
  displayStatus: string = '';
  transactionFee: any;
  transSetupFee: any;
  transSetupOum: any;
  sstFee: any;
  sstSetupFee: any;
  sstSetupOum: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _route: Router
  ) {
    let details = JSON.parse(this._activatedRoute.snapshot.queryParams['data']);
    console.log("Details Trans:", details);

    this.referenceId = details.reference;
    this.transactionType = details.transactionType;
    this.date = details.date;
    this.price = details.price;
    this.amount = details.amount;
    this.status = details.status;
    this.statusCode = details.statusCode;
    this.transactionFee = details.fees[1].chargeFees;
    this.transSetupFee = details.fees[1].setupFees;
    this.transSetupOum = details.fees[1].setupUom;
    this.sstFee = details.fees[0].chargeFees;
    this.sstSetupFee = details.fees[0].setupFees;
    this.transactionAmount = details.transactions[0].amount;

    console.log(this.transactionType);

    if (this.transactionType === "F01") {
      this.displaytransactionType = "Cash Top Up";
    } else if (this.transactionType === "F02") {
      this.displaytransactionType = "Cash Withdrawal";
    } else if (this.transactionType === "F03") {
      this.displaytransactionType = "Transfer Gold";
    } else if (this.transactionType === "F04") {
      this.displaytransactionType = "Gold Redemption";
    } else if (this.transactionType === "F05") {
      this.displaytransactionType = "Gold Purchase";
    } else if (this.transactionType === "F06") {
      this.displaytransactionType = "Gold Sale";
    }

    console.log(this.statusCode);

    if (this.statusCode === "G01" || this.statusCode === "G06" ) {
      this.displayStatus = "Successful";
    } else if (this.statusCode === "G02" || this.statusCode === "G07") {
      this.displayStatus = "Unsuccessful";
    } else if (this.statusCode === "G08") {
      this.displayStatus = "Pending";
    }
  }

}
