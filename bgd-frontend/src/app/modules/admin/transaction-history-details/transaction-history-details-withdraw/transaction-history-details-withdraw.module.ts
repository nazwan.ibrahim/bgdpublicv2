import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TransactionHistoryDetailsWithdrawComponent } from './transaction-history-details-withdraw.component';
import { TransactionHistoryDetailsWithdrawRoutingModule } from './transaction-history-details-withdraw-routing.module';


@NgModule({
    declarations: [
        TransactionHistoryDetailsWithdrawComponent
    ],
    imports: [
        TransactionHistoryDetailsWithdrawRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class TransactionHistoryDetailsWithdrawModule { }
