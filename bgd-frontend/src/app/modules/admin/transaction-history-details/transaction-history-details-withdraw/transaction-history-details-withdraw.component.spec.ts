import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionHistoryDetailsWithdrawComponent } from './transaction-history-details-withdraw.component';

describe('TransactionHistoryDetailsWithdrawComponent', () => {
  let component: TransactionHistoryDetailsWithdrawComponent;
  let fixture: ComponentFixture<TransactionHistoryDetailsWithdrawComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionHistoryDetailsWithdrawComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransactionHistoryDetailsWithdrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
