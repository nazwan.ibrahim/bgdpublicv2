import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TransactionHistoryDetailsComponent } from './transaction-history-details.component';
import { TransactionHistoryDetailsRoutingModule } from './transaction-history-details-routing.module';
import { TransactionHistoryDetailsTransferComponent } from './transaction-history-details-transfer/transaction-history-details-transfer.component';
import { TransactionHistoryDetailsRedeemComponent } from './transaction-history-details-redeem/transaction-history-details-redeem.component';
import { TransactionHistoryDetailsBuyBgdComponent } from './transaction-history-details-buy-bgd/transaction-history-details-buy-bgd.component';
import { TransactionHistoryDetailsSellBgdComponent } from './transaction-history-details-sell-bgd/transaction-history-details-sell-bgd.component';


@NgModule({
  declarations: [
    TransactionHistoryDetailsComponent
  ],
  imports: [
    TransactionHistoryDetailsRoutingModule,
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class TransactionHistoryDetailsModule { }
