import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionHistoryDetailsBuyBgdComponent } from './transaction-history-details-buy-bgd.component';

describe('TransactionHistoryDetailsBuyBgdComponent', () => {
  let component: TransactionHistoryDetailsBuyBgdComponent;
  let fixture: ComponentFixture<TransactionHistoryDetailsBuyBgdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionHistoryDetailsBuyBgdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransactionHistoryDetailsBuyBgdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
