import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TransactionHistoryDetailsBuyBgdRoutingModule } from './transaction-history-details-buy-bgd-routing.module';
import { TransactionHistoryDetailsBuyBgdComponent } from './transaction-history-details-buy-bgd.component';


@NgModule({
  declarations: [
    TransactionHistoryDetailsBuyBgdComponent
  ],
  imports: [
    TransactionHistoryDetailsBuyBgdRoutingModule,
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class TransactionHistoryDetailsBuyBgdModule { }
