import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionHistoryDetailsBuyBgdComponent } from './transaction-history-details-buy-bgd.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionHistoryDetailsBuyBgdComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionHistoryDetailsBuyBgdRoutingModule { }
