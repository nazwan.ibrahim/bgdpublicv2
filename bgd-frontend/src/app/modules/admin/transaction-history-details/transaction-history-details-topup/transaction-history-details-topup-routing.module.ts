import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TransactionHistoryDetailsTopupComponent } from './transaction-history-details-topup.component';

const routes: Routes = [
  {
    path: '',
    component: TransactionHistoryDetailsTopupComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionHistoryDetailsTopupRoutingModule { }
