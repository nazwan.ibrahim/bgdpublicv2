import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionHistoryDetailsTopupComponent } from './transaction-history-details-topup.component';

describe('TransactionHistoryDetailsTopupComponent', () => {
  let component: TransactionHistoryDetailsTopupComponent;
  let fixture: ComponentFixture<TransactionHistoryDetailsTopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransactionHistoryDetailsTopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TransactionHistoryDetailsTopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
