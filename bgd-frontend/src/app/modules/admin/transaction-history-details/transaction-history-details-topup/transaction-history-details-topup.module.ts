import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { TransactionHistoryDetailsTopupComponent } from './transaction-history-details-topup.component';
import { TransactionHistoryDetailsTopupRoutingModule } from './transaction-history-details-topup-routing.module';


@NgModule({
    declarations: [
        TransactionHistoryDetailsTopupComponent
    ],
    imports: [
        TransactionHistoryDetailsTopupRoutingModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class TransactionHistoryDetailsTopupModule { }
