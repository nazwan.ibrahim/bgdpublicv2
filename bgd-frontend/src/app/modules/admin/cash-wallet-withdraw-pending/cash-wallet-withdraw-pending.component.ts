import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';
import { saveAs } from 'file-saver';
@Component({
  selector: 'bgd-cash-wallet-withdraw-pending',
  templateUrl: './cash-wallet-withdraw-pending.component.html',
  styleUrls: ['./cash-wallet-withdraw-pending.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CashWalletWithdrawPendingComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  today: Date = new Date();
  accountNumber: any;
  bankName: any;
  amount: any;
  chargeTransFee: any;
  ref: any;
  setupTransFee: any;
  setupTransOum: any;
  chargeSstFee: any;
  setupSstFee: any;
  setupSstOum: any;
  totalAmount: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private bgdService: bgdService,
    private walletService: WalletService
  ) {}

  ngOnInit() {

      this.bankName = this.walletService.bankName;
      this.accountNumber = this.walletService.accountBank;

      const initResponse = this.walletService.initWithdraw;
      this.amount = initResponse[0].amount;
      this.totalAmount = initResponse[0].totalAmount;
      this.chargeTransFee = initResponse[0].feesForms[0].chargeFees;
      this.setupTransFee = initResponse[0].feesForms[0].setupFees;
      this.setupTransOum = initResponse[0].feesForms[0].setupUom;
      this.chargeSstFee = initResponse[0].feesForms[1].chargeFees;
      this.setupSstFee = initResponse[0].feesForms[1].setupFees;
      this.setupSstOum = initResponse[0].feesForms[1].setupUom;

      const response = this.walletService.withdrawResponse;
      this.ref = response.reference;

  }

  downloadReceipt() {

    const reference = this.ref;

    this.bgdService.downloadReceipt(reference, 'F02').subscribe(
      (data: any) => {
        const blob = new Blob([data], { type: 'application/pdf' });
        saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        // Handle any errors that occur during the API request
        console.error('Error downloading receipt:', error);
      }
    );
  }

}