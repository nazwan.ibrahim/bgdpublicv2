"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletWithdrawPendingComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var file_saver_1 = require("file-saver");
var CashWalletWithdrawPendingComponent = /** @class */ (function () {
    function CashWalletWithdrawPendingComponent(_activatedRoute, _router, bgdService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.bgdService = bgdService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        var Response = JSON.parse(this._activatedRoute.snapshot.queryParams['Response']);
        console.log("Response ", Response);
        console.log("Purchase ", response);
        this.accountNumber = this._activatedRoute.snapshot.queryParams['account'];
        this.bankName = this._activatedRoute.snapshot.queryParams['bank'];
        this.amount = response[0].amount;
        this.totalAmount = response[0].totalAmount;
        this.chargeTransFee = response[0].feesForms[0].chargeFees;
        this.setupTransFee = response[0].feesForms[0].setupFees;
        this.setupTransOum = response[0].feesForms[0].setupUom;
        this.chargeSstFee = response[0].feesForms[1].chargeFees;
        this.setupSstFee = response[0].feesForms[1].setupFees;
        this.setupSstOum = response[0].feesForms[1].setupUom;
        this.ref = Response.reference;
    }
    /*   ngOnInit() {
        console.log(this.chargeSstFee);
        console.log(this.chargeTransFee);
        console.log(this.amount);
    
        const parsedAmount = parseFloat(this.amount);
        const parsedChargeSstFee = parseFloat(this.chargeSstFee);
        const parsedChargeTransFee = parseFloat(this.chargeTransFee);
    
        if (!isNaN(parsedAmount) && !isNaN(parsedChargeSstFee) && !isNaN(parsedChargeTransFee)) {
          this.amount = parsedAmount - parsedChargeSstFee - parsedChargeTransFee;
        }
      } */
    CashWalletWithdrawPendingComponent.prototype.downloadReceipt = function () {
        var reference = this.ref;
        this.bgdService.downloadReceipt(reference, 'F02').subscribe(function (data) {
            console.log("Response Do", data);
            // Handle the response
            var blob = new Blob([data], { type: 'application/pdf' });
            file_saver_1.saveAs(blob, 'receipt.pdf');
            // const link = document.createElement('a');
            // link.href = window.URL.createObjectURL(blob);
            // link.download = 'receipt.pdf';
            // link.click();
        }, function (error) {
            // Handle any errors that occur during the API request
            console.error('Error downloading receipt:', error);
        });
    };
    CashWalletWithdrawPendingComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet-withdraw-pending',
            templateUrl: './cash-wallet-withdraw-pending.component.html',
            styleUrls: ['./cash-wallet-withdraw-pending.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], CashWalletWithdrawPendingComponent);
    return CashWalletWithdrawPendingComponent;
}());
exports.CashWalletWithdrawPendingComponent = CashWalletWithdrawPendingComponent;
