"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletWithdrawPendingModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var cash_wallet_withdraw_pending_component_1 = require("./cash-wallet-withdraw-pending.component");
var shared_module_1 = require("app/shared/shared.module");
var button_1 = require("@angular/material/button");
var input_1 = require("@angular/material/input");
var cash_wallet_withdraw_pending_routing_module_1 = require("./cash-wallet-withdraw-pending-routing.module");
var icon_1 = require("@angular/material/icon");
var CashWalletWithdrawPendingModule = /** @class */ (function () {
    function CashWalletWithdrawPendingModule() {
    }
    CashWalletWithdrawPendingModule = __decorate([
        core_1.NgModule({
            declarations: [
                cash_wallet_withdraw_pending_component_1.CashWalletWithdrawPendingComponent
            ],
            imports: [
                common_1.CommonModule,
                cash_wallet_withdraw_pending_routing_module_1.CashWalletWithdrawPendingRoutingModule,
                shared_module_1.SharedModule,
                button_1.MatButtonModule,
                input_1.MatInputModule,
                icon_1.MatIconModule
            ]
        })
    ], CashWalletWithdrawPendingModule);
    return CashWalletWithdrawPendingModule;
}());
exports.CashWalletWithdrawPendingModule = CashWalletWithdrawPendingModule;
