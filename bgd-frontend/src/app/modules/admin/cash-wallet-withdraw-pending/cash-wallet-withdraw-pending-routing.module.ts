import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletWithdrawPendingComponent } from 'app/modules/admin/cash-wallet-withdraw-pending/cash-wallet-withdraw-pending.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletWithdrawPendingComponent
        // resolve  : {
        //   data: CashWalletTopupResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletWithdrawPendingRoutingModule { }
