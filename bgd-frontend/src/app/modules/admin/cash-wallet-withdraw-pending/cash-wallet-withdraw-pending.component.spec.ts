import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletWithdrawPendingComponent } from './cash-wallet-withdraw-pending.component';

describe('CashWalletWithdrawPendingComponent', () => {
  let component: CashWalletWithdrawPendingComponent;
  let fixture: ComponentFixture<CashWalletWithdrawPendingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletWithdrawPendingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletWithdrawPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
