import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashWalletWithdrawPendingComponent } from './cash-wallet-withdraw-pending.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletWithdrawPendingRoutingModule } from './cash-wallet-withdraw-pending-routing.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
    declarations: [
        CashWalletWithdrawPendingComponent
    ],
    imports: [
        CommonModule,
        CashWalletWithdrawPendingRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class CashWalletWithdrawPendingModule { }
