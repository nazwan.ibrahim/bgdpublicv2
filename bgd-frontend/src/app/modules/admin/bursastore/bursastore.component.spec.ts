import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BursastoreComponent } from './bursastore.component';

describe('BursastoreComponent', () => {
  let component: BursastoreComponent;
  let fixture: ComponentFixture<BursastoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BursastoreComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BursastoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
