import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupMethodComponent } from 'app/modules/admin/cash-wallet-topup-method/cash-wallet-topup-method.component';

const routes: Routes = [
  {
    path: '',
    component: CashWalletTopupMethodComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletTopupMethodRoutingModule { }
