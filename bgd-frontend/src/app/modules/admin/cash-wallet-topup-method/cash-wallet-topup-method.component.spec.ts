import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupMethodComponent } from './cash-wallet-topup-method.component';

describe('CashWalletTopupMethodComponent', () => {
  let component: CashWalletTopupMethodComponent;
  let fixture: ComponentFixture<CashWalletTopupMethodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupMethodComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
