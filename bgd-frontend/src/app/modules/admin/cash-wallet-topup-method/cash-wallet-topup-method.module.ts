import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//import { CashWalletTopuAmountMethodRoutingModule } from './cash-wallet-topup-method.routing.module';
import { CashWalletTopupMethodComponent } from './cash-wallet-topup-method.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletTopupMethodRoutingModule } from './cash-wallet-topup-method-routing.module';


@NgModule({
  declarations: [
    CashWalletTopupMethodComponent
  ],
  imports: [
    CommonModule,
    CashWalletTopupMethodRoutingModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
  ]
})
export class CashWalletTopupMethodModule { }
