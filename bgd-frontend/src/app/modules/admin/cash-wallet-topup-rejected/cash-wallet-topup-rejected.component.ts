import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'bgd-cash-wallet-topup-rejected',
  templateUrl: './cash-wallet-topup-rejected.component.html',
  styleUrls: ['./cash-wallet-topup-rejected.component.scss']
})
export class CashWalletTopupRejectedComponent {

  today: Date = new Date();
  totalAmount: any;
  transactionMethodCode: any;
  transactionMethod: any;
  chargeTransFee: any;
  status: any;
  setuptransFee: any;
  setuptransOum: any;
  setupSstFee: any;
  setupSstOum: any;
  chargeSstFee: any;
  ref: any;
  amount: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: UntypedFormBuilder,
    private cashWalletService: CashWalletService,
    private bgdService: bgdService,

  ) {
    const response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);

    console.log("Status Response", response);

    this.amount = response.amount;
    this.totalAmount = response.totalAmount;
    this.transactionMethodCode = response.transactionMethodCode;
    this.setuptransFee = response.fees[0].setupFees;
    this.chargeTransFee = response.fees[0].chargeFees;
    this.setuptransOum = response.fees[0].setupOum;
    this.setupSstFee = response.fees[1].setupFees;
    this.chargeSstFee = response.fees[1].chargeFees;
    this.setupSstOum = response.fees[1].setupOum;
    this.status = response.statusCode;
    this.ref = response.reference;


    if (this.status = "G01") {
      this.status = "Successful";
    } else if (this.status = "G02") {
      this.status = "Unsuccessful";
    } else if (this.status = "G05") {
      this.status = "Pending";
    }


    if (this.transactionMethodCode == "H01") {
      this.transactionMethod = "Online Banking";
    } else if (this.transactionMethodCode == "H02") {
      this.transactionMethod = "Credit Card & Debit Card";
    } else if (this.transactionMethodCode == "H03") {
      this.transactionMethod = "Auto Debit";
    }

  }

  downloadReceipt() {

    this.bgdService.downloadReceipt(this.ref, 'F01').subscribe(
      (response: any) => {
        console.log("Response Do", response);
        // Handle the response
        const blob = new Blob([response], { type: 'application/pdf' });
        saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        // Handle any errors that occur during the API request
        console.error('Error downloading receipt:', error);
      }
    );
  }

}

