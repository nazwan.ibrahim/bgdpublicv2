import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { CashWalletTopupRejectedComponent } from './cash-wallet-topup-rejected.component';
import { CashWalletTopupRejectedRoutingModule } from './cash-wallet-topup-rejected-routing.module';


@NgModule({
    declarations: [
        CashWalletTopupRejectedComponent
    ],
    imports: [
        CommonModule,
        CashWalletTopupRejectedRoutingModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
    ]
})
export class CashWalletTopupRejectedModule { }
