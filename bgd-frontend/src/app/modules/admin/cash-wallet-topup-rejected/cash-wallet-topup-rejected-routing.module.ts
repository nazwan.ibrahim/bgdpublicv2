import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupRejectedComponent } from './cash-wallet-topup-rejected.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletTopupRejectedComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletTopupRejectedRoutingModule { }
