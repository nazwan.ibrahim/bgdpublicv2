import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupRejectedComponent } from './cash-wallet-topup-rejected.component';

describe('CashWalletTopupRejectedComponent', () => {
  let component: CashWalletTopupRejectedComponent;
  let fixture: ComponentFixture<CashWalletTopupRejectedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupRejectedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupRejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
