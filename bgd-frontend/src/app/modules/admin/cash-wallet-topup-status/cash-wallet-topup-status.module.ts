import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashWalletTopupStatusComponent } from './cash-wallet-topup-status.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { CashWalletTopupStatusRoutingModule } from './cash-wallet-topup-status-routing.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    CashWalletTopupStatusComponent
  ],
  imports: [
    CommonModule,
    CashWalletTopupStatusRoutingModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ]
})
export class CashWalletTopupStatusModule { }
