"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletTopupStatusComponent = void 0;
var core_1 = require("@angular/core");
var file_saver_1 = require("file-saver");
var CashWalletTopupStatusComponent = /** @class */ (function () {
    function CashWalletTopupStatusComponent(_activatedRoute, _router, _formBuilder, cashWalletService, bgdService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this.cashWalletService = cashWalletService;
        this.bgdService = bgdService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        this.setupTransOum = "%";
        this.topupSSTUom = "%";
        var response = JSON.parse(this._activatedRoute.snapshot.queryParams['response']);
        console.log("Status Response", response);
        this.amount = response.amount;
        this.transactionMethodCode = response.transactionMethodCode;
        this.setuptransFee = response.fees[1].setupFees;
        this.setupTransOum = response.fees[1].setupUom;
        this.chargeTransFee = response.fees[1].chargeFees;
        this.topupSST = response.fees[0].chargeFees;
        this.topupSSTPerc = response.fees[0].setupFees;
        this.topupSSTUom = response.fees[0].setupUom;
        this.status = response.statusCode;
        this.ref = response.reference;
        console.log(this.topupSSTUom);
        console.log(this.setupTransOum);
        console.log(this.topupSSTUom);
        console.log(this.setupTransOum);
        if (this.status = "G01") {
            this.status = "Successful";
        }
        else {
            this.status = "Failed";
        }
        if (this.transactionMethodCode == "H01") {
            this.transactionMethod = "Online Banking";
        }
        else if (this.transactionMethodCode == "H02") {
            this.transactionMethod = "Credit Card & Debit Card";
        }
        else if (this.transactionMethodCode == "H03") {
            this.transactionMethod = "Auto Debit";
        }
        this.totalAmount = this.amount - this.chargeTransFee - this.topupSST;
    }
    CashWalletTopupStatusComponent.prototype.downloadReceipt = function () {
        this.bgdService.downloadReceipt(this.ref, 'F01').subscribe(function (response) {
            console.log("Response Do", response);
            // Handle the response
            var blob = new Blob([response], { type: 'application/pdf' });
            file_saver_1.saveAs(blob, 'receipt.pdf');
        }, function (error) {
            // Handle any errors that occur during the API request
            console.error('Error downloading receipt:', error);
        });
    };
    CashWalletTopupStatusComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet-topup-status',
            templateUrl: './cash-wallet-topup-status.component.html',
            styleUrls: ['./cash-wallet-topup-status.component.scss']
        })
    ], CashWalletTopupStatusComponent);
    return CashWalletTopupStatusComponent;
}());
exports.CashWalletTopupStatusComponent = CashWalletTopupStatusComponent;
