import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { CashWalletService } from 'app/modules/cash-wallet/cash-wallet.service';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { saveAs } from 'file-saver';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';
@Component({
  selector: 'bgd-cash-wallet-topup-status',
  templateUrl: './cash-wallet-topup-status.component.html',
  styleUrls: ['./cash-wallet-topup-status.component.scss']
})
export class CashWalletTopupStatusComponent {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  today: Date = new Date();
  totalAmount: any;
  transactionMethodCode: any;
  transactionMethod: any;
  chargeTransFee: any;
  status: any;
  amount: any;
  setuptransFee: any;
  setupTransOum: any = "%";
  topupSST: any;
  topupSSTPerc: any;
  topupSSTUom: any = "%";
  ref: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private walletService: WalletService,
    private bgdService: bgdService,

  ) {}

  ngOninit() {

    const response = this.walletService.topupResponse;
    this.amount = response.amount;
    this.totalAmount = response.totalAmount;
    this.transactionMethodCode = response.transactionMethodCode;
    this.setuptransFee = response.fees[1].setupFees;
    this.setupTransOum = response.fees[1].setupUom;
    this.chargeTransFee = response.fees[1].chargeFees;
    this.topupSST = response.fees[0].chargeFees;
    this.topupSSTPerc = response.fees[0].setupFees;
    this.topupSSTUom = response.fees[0].setupUom;
    this.status = response.statusCode;
    this.ref = response.reference;


    if (this.status = "G01") {
      this.status = "Successful";
    } else if (this.status = "G02") {
      this.status = "Unsuccessful";
    } else if (this.status = "G05") {
      this.status = "Pending";
    }


    if (this.transactionMethodCode == "H01") {
      this.transactionMethod = "Online Banking";
    } else if (this.transactionMethodCode == "H02") {
      this.transactionMethod = "Credit Card & Debit Card";
    } else if (this.transactionMethodCode == "H03") {
      this.transactionMethod = "Auto Debit";
    }

  }

  downloadReceipt() {

    this.bgdService.downloadReceipt(this.ref, 'F01').subscribe(
      (response: any) => {
        console.log("Response Do", response);
        // Handle the response
        const blob = new Blob([response], { type: 'application/pdf' });
        saveAs(blob, 'receipt.pdf');
      },
      (error: any) => {
        // Handle any errors that occur during the API request
        console.error('Error downloading receipt:', error);
      }
    );
  }
}
