import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletTopupStatusComponent } from 'app/modules/admin/cash-wallet-topup-status/cash-wallet-topup-status.component';

const routes: Routes = [
  {
    path: '',
    component: CashWalletTopupStatusComponent
    // resolve  : {
    //   data: CashWalletTopupResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletTopupStatusRoutingModule { }
