import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletTopupStatusComponent } from './cash-wallet-topup-status.component';

describe('CashWalletTopupStatusComponent', () => {
  let component: CashWalletTopupStatusComponent;
  let fixture: ComponentFixture<CashWalletTopupStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletTopupStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletTopupStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
