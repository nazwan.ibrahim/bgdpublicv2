import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletWithdrawFailedComponent } from './cash-wallet-withdraw-failed.component';

const routes: Routes = [
    {
        path: '',
        component: CashWalletWithdrawFailedComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CashWalletWithdrawFailedRoutingModule { }
