import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashWalletWithdrawFailedComponent } from './cash-wallet-withdraw-failed.component';

describe('CashWalletWithdrawFailedComponent', () => {
  let component: CashWalletWithdrawFailedComponent;
  let fixture: ComponentFixture<CashWalletWithdrawFailedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashWalletWithdrawFailedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CashWalletWithdrawFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
