import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraUnavailableComponent } from './camera-unavailable.component';

describe('CameraUnavailableComponent', () => {
  let component: CameraUnavailableComponent;
  let fixture: ComponentFixture<CameraUnavailableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CameraUnavailableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CameraUnavailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
