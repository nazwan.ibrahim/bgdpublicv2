import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'bgd-camera-unavailable',
  templateUrl: './camera-unavailable.component.html',
  styleUrls: ['./camera-unavailable.component.scss']
})
export class CameraUnavailableComponent {

  constructor(
    private _router: Router,
    public dialogRef: MatDialogRef<CameraUnavailableComponent>,
  ) { }

  onClose(): void {
    this.dialogRef.close();
  }

}
