import { Route} from '@angular/router';
import { CameraUnavailableComponent } from 'app/modules/auth/camera-unavailable/camera-unavailable.component';

export const CameraUnavailableRoutes: Route[] = [
  {
      path     : '',
      component: CameraUnavailableComponent
  }
];

