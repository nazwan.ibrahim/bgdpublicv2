import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CameraUnavailableRoutes } from 'app/modules/auth/camera-unavailable/camera-unavailable.routing';
import { CommonModule } from '@angular/common';
import { CameraUnavailableComponent } from './camera-unavailable.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        CameraUnavailableComponent,
 
    ],
    imports     : [
        RouterModule.forChild(CameraUnavailableRoutes),
        CommonModule,
        MatButtonModule,
        MatIconModule,
    ],
    

})
export class CameraUnavailableModule
{
}
