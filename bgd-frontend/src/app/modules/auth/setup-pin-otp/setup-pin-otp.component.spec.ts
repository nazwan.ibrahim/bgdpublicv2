import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPinOtpComponent } from './setup-pin-otp.component';

describe('SetupPinOtpComponent', () => {
  let component: SetupPinOtpComponent;
  let fixture: ComponentFixture<SetupPinOtpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupPinOtpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SetupPinOtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
