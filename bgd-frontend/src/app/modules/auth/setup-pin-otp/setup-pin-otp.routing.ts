import { Route } from '@angular/router';
import { SetupPinOtpComponent } from 'app/modules/auth/setup-pin-otp/setup-pin-otp.component';

export const SetupPinOtpRoutes: Route[] = [
    {
        path: '',
        component: SetupPinOtpComponent
    }
];

