import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { MatDialog } from '@angular/material/dialog';
import { SetupPinSuccessAlertDialog } from './setup-pin-success-alert-dialog/setup-pin-success-alert-dialog.component';

@Component({
  selector: 'bgd-setup-pin-otp',
  templateUrl: './setup-pin-otp.component.html',
  styleUrls: ['./setup-pin-otp.component.scss']
})
export class SetupPinOtpComponent {

  showAlert = false;
  username: any;
  email: any;
  pin: any;
  enteredPIN: any;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  constructor(

    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private AuthService: AuthService,
    private _formBuilder: FormBuilder,
    private user: DashboardService,
    private _bgdService: bgdService,
    public dialog: MatDialog

  ) {

    {
      this._activatedRoute.paramMap.subscribe(params => {
        this.enteredPIN = params.get('userPin');
      })
      this.enteredPIN = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userPin'));
    }

  }

  ngOnInit(): void {

    this.user.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    })

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  confirmPIN() {

    this.user.getUserDetails().subscribe(data => {
      data.pinEnable = true;
    })

    this._router.navigate(['/dashboard']);

  }

  openDialog(): void {

    const dialogRef = this.dialog.open(SetupPinSuccessAlertDialog, {
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  verifyPin() {
    if (this.enteredPIN === this.pin) {
      const newPin = {
        newPin: this.pin
      }

      this.AuthService.setupPin(newPin)
        .subscribe(
          response => {
            
            this.openDialog();

            this.confirmPIN();
          },
          error => {

            this.alert = {

              type: 'error',
              message: 'Something went wrong, please try again.'
            };

            this.showAlert = true;

            setTimeout(() => {
              this.showAlert = false;
            }, 1000);
          }
        );
    } else {

      this.alert = {
        type: 'error',
        message: 'PIN does not match.'
      };

      this.showAlert = true;

      this.pin = '';

      setTimeout(() => {
        this.showAlert = false;
      }, 1000);
    }
  }

}
