"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SetupPinOtpComponent = void 0;
var core_1 = require("@angular/core");
var SetupPinOtpComponent = /** @class */ (function () {
    function SetupPinOtpComponent(_activatedRoute, _router, AuthService, _formBuilder, user, _bgdService) {
        var _this = this;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.AuthService = AuthService;
        this._formBuilder = _formBuilder;
        this.user = user;
        this._bgdService = _bgdService;
        this.showAlert = false;
        this.alert = {
            type: 'success',
            message: ''
        };
        {
            this._activatedRoute.paramMap.subscribe(function (params) {
                _this.enteredPIN = params.get('userPin');
            });
            this.enteredPIN = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userPin'));
        }
    }
    SetupPinOtpComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user.getUserDetails().subscribe(function (data) {
            _this.email = data.email;
            _this.username = data.userName;
        });
    };
    SetupPinOtpComponent.prototype.pincodeCompleted = function (pin) {
        this.pin = pin;
    };
    SetupPinOtpComponent.prototype.confirmPIN = function () {
        this.user.getUserDetails().subscribe(function (data) {
            data.pinEnable = true;
        });
        this._router.navigate(['/dashboard']);
    };
    SetupPinOtpComponent.prototype.verifyPin = function () {
        var _this = this;
        if (this.enteredPIN === this.pin) {
            var newPin = {
                newPin: this.pin
            };
            this.AuthService.setupPin(newPin)
                .subscribe(function (response) {
                _this.alert = {
                    type: 'success',
                    message: 'Verified'
                };
                _this.confirmPIN();
            }, function (error) {
                _this.alert = {
                    type: 'error',
                    message: 'Invalid'
                };
                _this.showAlert = true;
            });
        }
        else {
            this.alert = {
                type: 'error',
                message: 'Entered PIN does not match.'
            };
            this.showAlert = true;
            this.pin = '';
            setTimeout(function () {
                _this._router.navigate(['/setup-pin']);
            }, 1000);
        }
    };
    SetupPinOtpComponent = __decorate([
        core_1.Component({
            selector: 'bgd-setup-pin-otp',
            templateUrl: './setup-pin-otp.component.html',
            styleUrls: ['./setup-pin-otp.component.scss']
        })
    ], SetupPinOtpComponent);
    return SetupPinOtpComponent;
}());
exports.SetupPinOtpComponent = SetupPinOtpComponent;
