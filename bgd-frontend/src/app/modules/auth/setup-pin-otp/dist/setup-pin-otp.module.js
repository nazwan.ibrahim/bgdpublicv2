"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SetupPinOtpModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var shared_module_1 = require("app/shared/shared.module");
var setup_pin_otp_component_1 = require("app/modules/auth/setup-pin-otp/setup-pin-otp.component");
var setup_pin_otp_routing_1 = require("app/modules/auth/setup-pin-otp/setup-pin-otp.routing");
var icon_1 = require("@angular/material/icon");
var button_1 = require("@angular/material/button");
var progress_spinner_1 = require("@angular/material/progress-spinner");
var input_1 = require("@angular/material/input");
var form_field_1 = require("@angular/material/form-field");
var select_1 = require("@angular/material/select");
var chips_1 = require("@angular/material/chips");
var card_1 = require("@bgd/components/card");
var alert_1 = require("@bgd/components/alert");
var ngx_pincode_1 = require("ngx-pincode");
var SetupPinOtpModule = /** @class */ (function () {
    function SetupPinOtpModule() {
    }
    SetupPinOtpModule = __decorate([
        core_1.NgModule({
            declarations: [
                setup_pin_otp_component_1.SetupPinOtpComponent,
            ],
            imports: [
                router_1.RouterModule.forChild(setup_pin_otp_routing_1.SetupPinOtpRoutes),
                common_1.CommonModule,
                shared_module_1.SharedModule,
                button_1.MatButtonModule,
                progress_spinner_1.MatProgressSpinnerModule,
                input_1.MatInputModule,
                form_field_1.MatFormFieldModule,
                select_1.MatSelectModule,
                card_1.FuseCardModule,
                alert_1.FuseAlertModule,
                chips_1.MatChipsModule,
                icon_1.MatIconModule,
                ngx_pincode_1.NgxPincodeModule,
            ]
        })
    ], SetupPinOtpModule);
    return SetupPinOtpModule;
}());
exports.SetupPinOtpModule = SetupPinOtpModule;
