import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { finalize } from 'rxjs';

@Component({
    selector: 'auth-sign-in',
    templateUrl: './sign-in.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AuthSignInComponent implements OnInit {
    @ViewChild('signInNgForm') signInNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };
    signInForm: UntypedFormGroup;
    showAlert = false;
    message: string = '';

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private _router: Router
    ) {
        this.message = this._activatedRoute.snapshot.queryParams['message']
        console.log(this.message);

        if (this.message) {
            this.alert = {
                type: 'success',
                message: this.message
            };

            // Show the alert
            this.showAlert = true;

            // Hide the alert after 3 seconds
            setTimeout(() => {
                this.showAlert = false;
            }, 3000);
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Create the form
        this.signInForm = this._formBuilder.group({
            username: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
        });
    }

    clearAutofill() {
        setTimeout(() => {
            const usernameField = document.getElementById('username') as HTMLInputElement;
            const passwordField = document.getElementById('password') as HTMLInputElement;
            usernameField.removeAttribute('readonly');
            passwordField.removeAttribute('readonly');
        }, 100);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign in
     */
    signIn(): void {
        // Return if the form is invalid
        if (this.signInForm.invalid) {
            return;
        }

        // Disable the form
        this.signInForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign in
        this._authService.signIn(this.signInForm.value)
            .pipe(
                finalize(() => {

                    // Re-enable the form
                    this.signInForm.enable();

                    // Reset the form
                    this.signInNgForm.resetForm();

                    // Show the alert
                    this.showAlert = true;

                    // Hide the alert after 3 seconds
                    setTimeout(() => {
                        this.showAlert = false;
                    }, 1000);
                })
            )
            .subscribe(
                (response) => {

                    this._authService.getUserDetails().subscribe(
                        response => {
                            if (response.emailVerified === true) {
                                /*if ((response.statusCode === 'T00' || response.statusCode === 'T01' || response.statusCode === 'T02' || response.statusCode === 'T03') && response.pinEnabled === false){   (old code)                          */
                                if ((response.userTier.code === 'T00' || response.userTier.code === 'T01' || response.userTier.code === 'T02' || response.userTier.code === 'T03') && response.pinEnable === false) {
                                    this._router.navigateByUrl('/setup-pin-landing');
                                } else {
                                    // Set the redirect url.
                                    // The '/signed-in-redirect' is a dummy url to catch the request and redirect the user
                                    // to the correct page after a successful sign in. This way, that url can be set via
                                    // routing file and we don't have to touch here.
                                    // const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/signed-in-redirect';
                                    //const redirectURL = '/signed-in-redirect';

                                    this.alert = {
                                        type: 'success',
                                        message: 'You are successfully logged in!'
                                    };

                                    // Navigate to the redirect url
                                    this._router.navigateByUrl('/dashboard');
                                }
                            } else {

                                this.alert = {
                                    type: 'error',
                                    message: 'Your email is not verified'
                                };

                            }
                        }
                    )
                    // Check if user's statusCode is T01 and pin is not enabled

                },
                (error) => {
                    if (error.status == 401) {

                        if (error.error.error === "invalid_grant" && error.error.error_description === "Email is not verified") {

                            this.alert = {
                                type: 'error',
                                message: error.error.error_description
                            };

                        } else {

                            this.alert = {
                                type: 'error',
                                message: 'Wrong email or password.'
                            };
                        }

                    } else {
                        // Set the alert
                        this.alert = {
                            type: 'error',
                            message: 'Something went wrong, please try again.'
                        };
                    }
                }
            );
    }
}
