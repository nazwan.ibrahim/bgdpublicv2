import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';

export interface DialogData {
    price: any;
}

/**
 * @title Dialog elements
 */
@Component({
    selector: 'session-expired-dialog',
    templateUrl: 'session-expired-dialog.component.html',
})
export class SessionExpiredDialog {


    constructor(
        private _router: Router,
        private _dashboardService: DashboardService,
        public dialogRef: MatDialogRef<SessionExpiredDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }

    onProceed(): void {
        this.dialogRef.close();
        this._router.navigate(['/sign-in']).then(() => {
            setTimeout(() => {
                location.reload();
            }, 0);
        });

    }
}
