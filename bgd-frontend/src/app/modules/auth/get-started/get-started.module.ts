import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetStartedComponent } from './get-started.component';
import { RouterModule } from '@angular/router';
import { GetStartedRoutes } from './get-started.routing';



@NgModule({
  declarations: [
    GetStartedComponent
  ],
  imports: [
    RouterModule.forChild(GetStartedRoutes),
    CommonModule
  ]
})
export class GetStartedModule { }
