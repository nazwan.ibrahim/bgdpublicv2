import { Route } from '@angular/router';
import { GetStartedComponent } from './get-started.component';

export const GetStartedRoutes: Route[] = [
    {
        path     : '',
        component: GetStartedComponent
    }
];
