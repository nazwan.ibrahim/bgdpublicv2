import { Route} from '@angular/router';
import { VerifyFailedComponent } from '../verify-failed/verify-failed.component';

export const VerifyFailedRoutes: Route[] = [
  {
      path     : '',
      component: VerifyFailedComponent
  }
];

