import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VerifyFailedRoutes } from 'app/modules/auth/verify-mykad/verify-failed/verify-failed.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { VerifyFailedComponent } from './verify-failed.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        VerifyFailedComponent,

    ],
    imports: [
        RouterModule.forChild(VerifyFailedRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatInputModule,
        MatCheckboxModule,
        // MatSelectModule,
        MatIconModule

    ],


})
export class VerifyFailedModule {
}
