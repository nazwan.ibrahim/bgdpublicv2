import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyMykadComponent } from './verify-mykad.component';

describe('VerifyMykadComponent', () => {
  let component: VerifyMykadComponent;
  let fixture: ComponentFixture<VerifyMykadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerifyMykadComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VerifyMykadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
