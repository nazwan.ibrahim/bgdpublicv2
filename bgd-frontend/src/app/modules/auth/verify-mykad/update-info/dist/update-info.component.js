"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.UpdateInfoComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var animations_1 = require("@bgd/animations");
var UpdateInfoComponent = /** @class */ (function () {
    function UpdateInfoComponent(_router, bankListService, route, user, countryListService, _formBuilder) {
        this._router = _router;
        this.bankListService = bankListService;
        this.route = route;
        this.user = user;
        this.countryListService = countryListService;
        this._formBuilder = _formBuilder;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.dropdownData = [];
        this.dropdownData2 = [];
        this.bankNum = '';
    }
    UpdateInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.acquaintmeReference = this.route.snapshot.queryParamMap.get('reference');
        this.acquaintmeStatus = this.route.snapshot.queryParamMap.get('status');
        this.name = this.route.snapshot.queryParamMap.get('name');
        this.icNumber = this.route.snapshot.queryParamMap.get('icNumber');
        console.log("reference", this.acquaintmeReference);
        console.log("status", this.acquaintmeStatus);
        console.log("name", this.name);
        console.log("ic num", this.icNumber);
        this.bankListService.bankList().subscribe(function (data) {
            _this.dropdownData = data;
        });
        // Create the form
        this.AuthRegisterForm = this._formBuilder.group({
            fullName: ['', [forms_1.Validators.required]],
            Id: ['', [forms_1.Validators.required]],
            bankCode: ['', [forms_1.Validators.required]],
            bankNum: ['', [forms_1.Validators.required]],
            agreements: [false, forms_1.Validators.requiredTrue]
        });
        this.user.getUserDetails().subscribe(function (data) {
            //this.statusCode = data.tierStatus.code;
            _this.userID = data.userID;
            console.log(_this.userID);
            console.log(_this.Id);
            console.log(_this.icNumber);
        });
    };
    UpdateInfoComponent.prototype.removeHyphens = function (id) {
        return id.replace(/-/g, '');
    };
    UpdateInfoComponent.prototype.submitDetails = function () {
        var _this = this;
        var fullName = this.fullName;
        var acctNum = this.bankNum;
        var accountType = "DFLT";
        var idType = "";
        var idNo = "";
        var receiverBIC = this.AuthRegisterForm.get('bankCode').value;
        var secValidInd = "N";
        var acquaintmeReference = this.acquaintmeReference;
        var acquaintmeStatus = this.acquaintmeStatus;
        var identificationTypeCode = "P01";
        var identificationNumber = this.removeHyphens(this.AuthRegisterForm.get('Id').value); // Apply the removeHyphens function to Id
        var body = {
            creditorAccountType: accountType,
            receiverBIC: receiverBIC,
            creditorAccountNo: acctNum,
            secondValidationIndicator: secValidInd,
            idType: idType,
            idNo: idNo,
            fullName: fullName,
            acquaintmeReference: acquaintmeReference,
            acquaintmeStatus: acquaintmeStatus,
            identificationTypeCode: identificationTypeCode,
            identificationNumber: identificationNumber
        };
        console.log(identificationNumber);
        this.user.accountEnquiry(body).subscribe(function (response) {
            console.log("RESPONSE OCR :", response);
            if (response.response = 'SUCCESS') {
                _this._router.navigate(['/verify-success']);
            }
            else if (response.response = 'FAIL') {
                _this._router.navigate(['/verify-failed']);
            }
            else if (response.response = 'PENDING') {
                _this._router.navigate(['/verification']);
            }
            else {
                _this._router.navigate(['/error-500']);
            }
        }, function (error) {
            console.log(error);
            _this._router.navigate(['/verify-failed']);
        });
    };
    __decorate([
        core_1.ViewChild('AuthRegisterNgForm')
    ], UpdateInfoComponent.prototype, "AuthRegisterNgForm");
    UpdateInfoComponent = __decorate([
        core_1.Component({
            selector: 'bgd-update-info',
            templateUrl: './update-info.component.html',
            styleUrls: ['./update-info.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], UpdateInfoComponent);
    return UpdateInfoComponent;
}());
exports.UpdateInfoComponent = UpdateInfoComponent;
