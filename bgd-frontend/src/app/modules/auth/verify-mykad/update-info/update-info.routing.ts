import { Route} from '@angular/router';
import { UpdateInfoComponent } from 'app/modules/auth/verify-mykad/update-info/update-info.component';

export const UpdateInfoRoutes: Route[] = [
  {
      path     : '',
      component: UpdateInfoComponent
  }
];

