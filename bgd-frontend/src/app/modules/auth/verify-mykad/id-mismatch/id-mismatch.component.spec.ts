import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdMismatchComponent } from './id-mismatch.component';

describe('IdMismatchComponent', () => {
  let component: IdMismatchComponent;
  let fixture: ComponentFixture<IdMismatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdMismatchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IdMismatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
