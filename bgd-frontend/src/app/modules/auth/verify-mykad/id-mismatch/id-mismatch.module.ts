import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IdMismatchRoutes } from './id-mismatch.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { IdMismatchComponent } from './id-mismatch.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        IdMismatchComponent,

    ],
    imports: [
        RouterModule.forChild(IdMismatchRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
        // MatSelectModule,
    ],


})
export class IdMismatchModule {
}
