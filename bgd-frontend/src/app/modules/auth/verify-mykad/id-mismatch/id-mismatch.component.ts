import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'bgd-id-mismatch',
  templateUrl: './id-mismatch.component.html',
  styleUrls: ['./id-mismatch.component.scss']
})
export class IdMismatchComponent {

  constructor(
    private _router: Router,
  ) { }

  resubmitButton(){
    this._router.navigate(['/upgrade-account']);
  }

}
