import { Route} from '@angular/router';
import { IdMismatchComponent } from './id-mismatch.component';

export const IdMismatchRoutes: Route[] = [
  {
      path     : '',
      component: IdMismatchComponent
  }
];

