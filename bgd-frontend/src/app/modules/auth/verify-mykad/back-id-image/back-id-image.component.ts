import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'bgd-back-id-image',
  templateUrl: './back-id-image.component.html',
  styleUrls: ['./back-id-image.component.scss']
})
export class BackIdImageComponent {

  imageSrcFront: any;
  imageSrcBack: any;

  public imageSrc: string = null;

  constructor(private route: ActivatedRoute, private _router: Router) {

  }

  ngOnInit() {
    const imageData = this.route.snapshot.queryParams['image'];
    if (imageData) {
      this.imageSrc = 'data:image/jpeg;base64,' + imageData;
    } else {
      this.imageSrcBack = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));
      this.imageSrc = this.imageSrcBack;
    }
    this.imageSrcFront = decodeURIComponent(this.route.snapshot.queryParams['imageSrcFront']);
  }

  backid() {

    this.imageSrcBack = this.imageSrc;

    this._router.navigate(['/selfie'], {
      queryParams: {
        imageSrcBack: encodeURIComponent(this.imageSrcBack),
        imageSrcFront: encodeURIComponent(this.imageSrcFront)
      }
    });
  }

  retakeButton() {
    this._router.navigate(['/back-id'], {
      queryParams: {
        imageSrcFront: encodeURIComponent(this.imageSrcFront)
      }
    });
  }

}


