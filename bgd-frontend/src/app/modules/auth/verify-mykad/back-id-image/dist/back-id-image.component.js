"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BackIdImageComponent = void 0;
var core_1 = require("@angular/core");
var BackIdImageComponent = /** @class */ (function () {
    function BackIdImageComponent(route, _router) {
        this.route = route;
        this._router = _router;
        this.imageSrc = null;
    }
    BackIdImageComponent.prototype.ngOnInit = function () {
        var imageData = this.route.snapshot.queryParams['image'];
        if (imageData) {
            this.imageSrc = 'data:image/jpeg;base64,' + imageData;
        }
        else {
            this.imageSrcBack = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));
            this.imageSrc = this.imageSrcBack;
        }
        this.imageSrcFront = decodeURIComponent(this.route.snapshot.queryParams['imageSrcFront']);
    };
    BackIdImageComponent.prototype.backid = function () {
        this.imageSrcBack = this.imageSrc;
        this._router.navigate(['/selfie'], {
            queryParams: {
                imageSrcBack: encodeURIComponent(this.imageSrcBack),
                imageSrcFront: encodeURIComponent(this.imageSrcFront)
            }
        });
    };
    BackIdImageComponent.prototype.retakeButton = function () {
        this._router.navigate(['/back-id'], {
            queryParams: {
                imageSrcFront: encodeURIComponent(this.imageSrcFront)
            }
        });
    };
    BackIdImageComponent = __decorate([
        core_1.Component({
            selector: 'bgd-back-id-image',
            templateUrl: './back-id-image.component.html',
            styleUrls: ['./back-id-image.component.scss']
        })
    ], BackIdImageComponent);
    return BackIdImageComponent;
}());
exports.BackIdImageComponent = BackIdImageComponent;
