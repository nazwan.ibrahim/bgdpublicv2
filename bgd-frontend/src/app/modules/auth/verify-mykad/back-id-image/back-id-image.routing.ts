import { Route } from '@angular/router';
import { BackIdImageComponent } from 'app/modules/auth/verify-mykad/back-id-image/back-id-image.component';

export const BackIdImageRoutes: Route[] = [
  {
    path: '',
    component: BackIdImageComponent
  }
];

