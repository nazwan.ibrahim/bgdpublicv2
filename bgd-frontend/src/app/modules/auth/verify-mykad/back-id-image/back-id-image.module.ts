import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BackIdImageRoutes } from 'app/modules/auth/verify-mykad/back-id-image/back-id-image.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { BackIdImageComponent } from './back-id-image.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        BackIdImageComponent,

    ],
    imports: [
        RouterModule.forChild(BackIdImageRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        // MatSelectModule,
        MatIconModule
    ],


})
export class BackIdImageModule {
}
