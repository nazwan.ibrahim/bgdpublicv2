import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VerifyMykadRoutes } from 'app/modules/auth/verify-mykad/verify-mykad.routing';
import {WebcamModule} from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { VerifyMykadComponent } from './verify-mykad.component';
import {MatStepperModule} from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        VerifyMykadComponent,
 
    ],
    imports     : [
        RouterModule.forChild(VerifyMykadRoutes),
        WebcamModule,
        FormsModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        // MatSelectModule,
    ],
    

})
export class VerifyMykadModule
{
}
