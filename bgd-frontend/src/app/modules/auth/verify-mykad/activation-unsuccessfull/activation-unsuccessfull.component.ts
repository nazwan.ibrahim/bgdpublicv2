import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'bgd-activation-unsuccessfull',
  templateUrl: './activation-unsuccessfull.component.html',
  styleUrls: ['./activation-unsuccessfull.component.scss']
})
export class ActivationUnsuccessfullComponent {

  constructor(
    private _router: Router,
  ) { }

  resubmitButton(){
    this._router.navigate(['/upgrade-account']);
  }
}
