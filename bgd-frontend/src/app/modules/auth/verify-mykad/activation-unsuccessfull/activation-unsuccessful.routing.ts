import { Route} from '@angular/router';
import { ActivationUnsuccessfullComponent } from './activation-unsuccessfull.component';

export const ActivationUnsuccessfullRoutes: Route[] = [
  {
      path     : '',
      component: ActivationUnsuccessfullComponent
  }
];

