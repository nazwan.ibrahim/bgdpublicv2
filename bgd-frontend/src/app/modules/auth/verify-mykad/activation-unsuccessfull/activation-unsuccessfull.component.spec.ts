import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivationUnsuccessfullComponent } from './activation-unsuccessfull.component';

describe('ActivationUnsuccessfullComponent', () => {
  let component: ActivationUnsuccessfullComponent;
  let fixture: ComponentFixture<ActivationUnsuccessfullComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivationUnsuccessfullComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ActivationUnsuccessfullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
