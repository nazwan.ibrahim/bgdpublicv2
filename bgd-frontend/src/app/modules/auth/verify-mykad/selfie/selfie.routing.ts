import { Route} from '@angular/router';
import { SelfieComponent } from 'app/modules/auth/verify-mykad/selfie/selfie.component';

export const SelfieRoutes: Route[] = [
  {
      path     : '',
      component: SelfieComponent
  }
];

