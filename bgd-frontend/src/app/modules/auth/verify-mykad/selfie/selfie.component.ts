import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'bgd-selfie',
  templateUrl: './selfie.component.html',
  styleUrls: ['./selfie.component.scss']
})
export class SelfieComponent {

  imageSrcFront: any;
  imageSrcBack: any;

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  isCaptured: boolean = false;
  @Output()
  public pictureTaken = new EventEmitter<WebcamImage>();

  public overlayWidth = 0;
  public overlayHeight = 0;
  public overlayMarginTop = 0;
  public overlayMarginLeft = 0;

  // toggle webcam on/off
  public showWebcam = true;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public videoOptions: MediaTrackConstraints = {
    width: { ideal: 1024 },
    height: { ideal: 576 }
  };
  public errors: WebcamInitError[] = [];

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

  public ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;

        this.imageSrcFront = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFront'));
        this.imageSrcBack = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));
      });
  }

  public triggerSnapshot(): void {
    this.trigger.next();
  }
  public removecurrent() {
    this.trigger.asObservable();
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
  }

  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }
  public webcamImage: WebcamImage = null;

  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.pictureTaken.emit(webcamImage);
    this.webcamImage = webcamImage;
    this._router.navigate(['/selfie-image'], {
      queryParams: {
        image: webcamImage.imageAsBase64,
        imageSrcBack: encodeURIComponent(this.imageSrcBack),
        imageSrcFront: encodeURIComponent(this.imageSrcFront)
      }
    });


  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }

  backButton() {
    const queryParams = {
      imageSrcFront: encodeURIComponent(this.imageSrcFront),
      imageSrcBack: encodeURIComponent(this.imageSrcBack),
    };

    this._router.navigate(['/back-id-image'], { queryParams });
  }


}







