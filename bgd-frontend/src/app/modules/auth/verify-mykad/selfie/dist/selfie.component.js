"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SelfieComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var ngx_webcam_1 = require("ngx-webcam");
var SelfieComponent = /** @class */ (function () {
    function SelfieComponent(_formBuilder, _router, route, location) {
        this._formBuilder = _formBuilder;
        this._router = _router;
        this.route = route;
        this.location = location;
        this.isCaptured = false;
        this.pictureTaken = new core_1.EventEmitter();
        this.overlayWidth = 0;
        this.overlayHeight = 0;
        this.overlayMarginTop = 0;
        this.overlayMarginLeft = 0;
        // toggle webcam on/off
        this.showWebcam = true;
        this.allowCameraSwitch = true;
        this.multipleWebcamsAvailable = false;
        this.videoOptions = {
            width: { ideal: 1024 },
            height: { ideal: 576 }
        };
        this.errors = [];
        // webcam snapshot trigger
        this.trigger = new rxjs_1.Subject();
        // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
        this.nextWebcam = new rxjs_1.Subject();
        this.webcamImage = null;
    }
    SelfieComponent.prototype.ngOnInit = function () {
        var _this = this;
        ngx_webcam_1.WebcamUtil.getAvailableVideoInputs()
            .then(function (mediaDevices) {
            _this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
            _this.imageSrcFront = decodeURIComponent(_this.route.snapshot.queryParamMap.get('imageSrcFront'));
            _this.imageSrcBack = decodeURIComponent(_this.route.snapshot.queryParamMap.get('imageSrcBack'));
        });
    };
    SelfieComponent.prototype.triggerSnapshot = function () {
        this.trigger.next();
    };
    SelfieComponent.prototype.removecurrent = function () {
        this.trigger.asObservable();
    };
    SelfieComponent.prototype.toggleWebcam = function () {
        this.showWebcam = !this.showWebcam;
    };
    SelfieComponent.prototype.handleInitError = function (error) {
        this.errors.push(error);
    };
    SelfieComponent.prototype.showNextWebcam = function (directionOrDeviceId) {
        // true => move forward through devices
        // false => move backwards through devices
        // string => move to device with given deviceId
        this.nextWebcam.next(directionOrDeviceId);
    };
    SelfieComponent.prototype.handleImage = function (webcamImage) {
        console.info('received webcam image', webcamImage);
        this.pictureTaken.emit(webcamImage);
        this.webcamImage = webcamImage;
        this._router.navigate(['/selfie-image'], {
            queryParams: {
                image: webcamImage.imageAsBase64,
                imageSrcBack: encodeURIComponent(this.imageSrcBack),
                imageSrcFront: encodeURIComponent(this.imageSrcFront)
            }
        });
    };
    SelfieComponent.prototype.cameraWasSwitched = function (deviceId) {
        console.log('active device: ' + deviceId);
        this.deviceId = deviceId;
    };
    Object.defineProperty(SelfieComponent.prototype, "triggerObservable", {
        get: function () {
            return this.trigger.asObservable();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SelfieComponent.prototype, "nextWebcamObservable", {
        get: function () {
            return this.nextWebcam.asObservable();
        },
        enumerable: false,
        configurable: true
    });
    SelfieComponent.prototype.backButton = function () {
        var queryParams = {
            imageSrcFront: encodeURIComponent(this.imageSrcFront),
            imageSrcBack: encodeURIComponent(this.imageSrcBack)
        };
        this._router.navigate(['/back-id-image'], { queryParams: queryParams });
    };
    __decorate([
        core_1.Output()
    ], SelfieComponent.prototype, "pictureTaken");
    SelfieComponent = __decorate([
        core_1.Component({
            selector: 'bgd-selfie',
            templateUrl: './selfie.component.html',
            styleUrls: ['./selfie.component.scss']
        })
    ], SelfieComponent);
    return SelfieComponent;
}());
exports.SelfieComponent = SelfieComponent;
