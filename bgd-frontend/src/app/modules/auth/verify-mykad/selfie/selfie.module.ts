import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SelfieRoutes } from 'app/modules/auth/verify-mykad/selfie/selfie.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { SelfieComponent } from './selfie.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        SelfieComponent,

    ],
    imports: [
        RouterModule.forChild(SelfieRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
        // MatSelectModule,
    ],


})
export class SelfieModule {
}
