import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IdUnrecognisableRoutes } from './id-unrecognisable.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { IdUnrecognisableComponent } from './id-unrecognisable.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        IdUnrecognisableComponent,

    ],
    imports: [
        RouterModule.forChild(IdUnrecognisableRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule
        // MatSelectModule,
    ],


})
export class IdUnrecognisableModule {
}
