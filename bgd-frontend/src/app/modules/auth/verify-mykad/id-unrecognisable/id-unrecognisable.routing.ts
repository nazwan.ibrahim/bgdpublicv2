import { Route} from '@angular/router';
import { IdUnrecognisableComponent } from './id-unrecognisable.component';

export const IdUnrecognisableRoutes: Route[] = [
  {
      path     : '',
      component: IdUnrecognisableComponent
  }
];

