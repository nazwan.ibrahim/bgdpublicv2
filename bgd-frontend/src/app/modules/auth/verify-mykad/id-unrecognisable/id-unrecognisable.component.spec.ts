import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdUnrecognisableComponent } from './id-unrecognisable.component';

describe('IdUnrecognisableComponent', () => {
  let component: IdUnrecognisableComponent;
  let fixture: ComponentFixture<IdUnrecognisableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdUnrecognisableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IdUnrecognisableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
