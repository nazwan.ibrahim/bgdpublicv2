import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'bgd-id-unrecognisable',
  templateUrl: './id-unrecognisable.component.html',
  styleUrls: ['./id-unrecognisable.component.scss']
})
export class IdUnrecognisableComponent {

  constructor(
    private _router: Router,
  ) { }

  resubmitButton(){
    this._router.navigate(['/upgrade-account']);
  }

}
