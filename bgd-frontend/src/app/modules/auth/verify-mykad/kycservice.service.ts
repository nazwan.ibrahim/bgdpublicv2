import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KycserviceService {

  kycId: any;
  kycName: any;
  acquaintmeReference: any;
  acquaintmeStatus: any;

  constructor() { }
  
}
