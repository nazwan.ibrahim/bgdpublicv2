import { Component, ViewChild, ViewEncapsulation, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { switchMap, map } from 'rxjs/operators';
import { Location } from '@angular/common';


function convertToJPEG(dataURL: string) {
  console.log('Converting to JPEG:', dataURL);

  // Remove the data:image/png;base64, header from the base64 string
  const base64String = dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

  console.log('Base64 string:', base64String);

  // Convert the base64 string to a Blob object
  const byteCharacters = atob(base64String);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += 512) {
    const slice = byteCharacters.slice(offset, offset + 512);
    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }
  const blob = new Blob(byteArrays, { type: "image/jpeg" });

  console.log('JPEG blob:', blob);

  return blob;
}
@Component({
  selector: 'bgd-selfie-image',
  templateUrl: './selfie-image.component.html',
  styleUrls: ['./selfie-image.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})

export class SelfieImageComponent implements OnInit {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  imageSrcFront: any;
  imageSrcBack: any;
  imageSrcFace: any;

  disableButton: boolean = false;

  statusCode: any;
  userID: any;
  response: any;
  backImage: any;
  frontImage: any;
  faceImage: any;
  backImageData: any;
  frontImageData: any;
  faceImageData: any;
  showAlert = false;
  imageData: any;

  constructor(private _router: Router,
    private route: ActivatedRoute,
    private user: AuthService,
    private location: Location,
    private _formBuilder: UntypedFormBuilder) {

    //this.imageSrcFace = 'data:image/jpeg;base64,' + imageData;
    this.imageSrcFace = 'data:image/jpeg;base64,' + decodeURIComponent(this.route.snapshot.queryParams['image']);

    this.imageSrcFront = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFront'));
    this.imageSrcBack = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));

  }

  ngOnInit() {
    // console.log(this.imageSrcFace);
    // const faceImageJPEG = convertToJPEG(this.imageSrcFace);
    // console.log(faceImageJPEG);


    this.user.getUserDetails().subscribe(data => {
      this.userID = data.userID;
      console.log(this.userID);
    });



  }

  frontid() {

    this.disableButton = true;

    const backImage = new Image();
    backImage.onload = () => {
      const backImageJPEG = convertToJPEG(this.imageSrcBack);
      //console.log(backImageJPEG);

      const frontImage = new Image();
      frontImage.onload = () => {
        const frontImageJPEG = convertToJPEG(this.imageSrcFront);
        //console.log(frontImageJPEG);

        // const faceImage = new Image();
        // faceImage.onload = () => {
        const faceImageJPEG = convertToJPEG(this.imageSrcFace);
        console.log(faceImageJPEG);

        const userID = this.userID;
        const kycType = 'T01';

        const formData = new FormData();
        formData.append('faceImg', faceImageJPEG);
        formData.append('documentImgFront', frontImageJPEG);
        formData.append('documentImgBack', backImageJPEG);
        formData.append('userID', userID);
        formData.append('kycType', kycType);

        const requestBody = {
          faceImg: formData.get('faceImg'),
          documentImgFront: formData.get('documentImgFront'),
          documentImgBack: formData.get('documentImgBack'),
          userID: formData.get('userID'),
          kycType: formData.get('kycType'),
        };

        this.user.verifyKYC(requestBody).subscribe(
          (Response) => {

            console.log("Response OCR", Response);
            if (Response.verificationStatus === 'verification.accepted') {
              const queryParams = {
                reference: Response.reference,
                status: Response.verificationStatus,
                name: Response.details.name,
                icNumber: Response.details.icNumber,

              };
              this._router.navigate(['/update-info'], { queryParams: queryParams });
            } else {

              if (Response.declinedCode === 'DCLN001') {
                const queryParams = {
                  reference: Response.reference,
                  status: Response.verificationStatus,
                  name: Response.details.name,
                  icNumber: Response.details.icNumber,
                };
                this._router.navigate(['/id-unrecognisable'], { queryParams: queryParams });
              } else if (Response.declinedCode === 'DCLN002') {
                const queryParams = {
                  reference: Response.reference,
                  status: Response.verificationStatus,
                  name: Response.details.name,
                  icNumber: Response.details.icNumber,
                };
                this._router.navigate(['/selfie-mismatch'], { queryParams: queryParams });
              } else if (Response.declinedCode === 'DCLN003') {
                const queryParams = {
                  reference: Response.reference,
                  status: Response.verificationStatus,
                  name: Response.details.name,
                  icNumber: Response.details.icNumber,
                };
                this._router.navigate(['/id-mismatch'], { queryParams: queryParams });
              } else if (Response.declinedCode !== 'DCLN000' || Response.declinedCode !== 'DCLN001' || Response.declinedCode !== 'DCLN002' || Response.declinedCode !== 'DCLN003') {
                const queryParams = {
                  reference: Response.reference,
                  status: Response.verificationStatus,
                  name: Response.details.name,
                  icNumber: Response.details.icNumber,
                };
                this._router.navigate(['/verify-failed']);
              }
            }
          },
          (error) => {
            this._router.navigate(['/verify-failed']);
            // this.showAlert = true;
            //  this.alert = {
            //    type: 'error',
            //    message: 'Something went wrong, please try again.'
            //  };
          }
        );
        //};
        //faceImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFace'));
      };
      frontImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFront'));
    };
    backImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));

    // this._router.navigate(['/update-info'], {
    //   queryParams: {
    //     imageSrcBack: encodeURIComponent(this.imageSrcBack),
    //     imageSrcFront: encodeURIComponent(this.imageSrcFront),
    //     imageSrcFace: encodeURIComponent(this.imageSrcFace)
    //   }
    // });
  }
  
  retakeButton() {

    this.location.back();

  }

}
