import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SelfieImageRoutes } from 'app/modules/auth/verify-mykad/selfie-image/selfie-image.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { SelfieImageComponent } from './selfie-image.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { FuseAlertModule } from '@bgd/components/alert';

@NgModule({
    declarations: [
        SelfieImageComponent,

    ],
    imports: [
        RouterModule.forChild(SelfieImageRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule,
        FuseAlertModule,
        // MatSelectModule,
    ],


})
export class SelfieImageModule {
}
