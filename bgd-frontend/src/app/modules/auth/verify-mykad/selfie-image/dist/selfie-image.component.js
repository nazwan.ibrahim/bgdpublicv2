"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SelfieImageComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
function convertToJPEG(dataURL) {
    console.log('Converting to JPEG:', dataURL);
    // Remove the data:image/png;base64, header from the base64 string
    var base64String = dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
    console.log('Base64 string:', base64String);
    // Convert the base64 string to a Blob object
    var byteCharacters = atob(base64String);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += 512) {
        var slice = byteCharacters.slice(offset, offset + 512);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: "image/jpeg" });
    console.log('JPEG blob:', blob);
    return blob;
}
var SelfieImageComponent = /** @class */ (function () {
    function SelfieImageComponent(_router, route, user, location, _formBuilder) {
        this._router = _router;
        this.route = route;
        this.user = user;
        this.location = location;
        this._formBuilder = _formBuilder;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.disableButton = false;
        this.showAlert = false;
        //this.imageSrcFace = 'data:image/jpeg;base64,' + imageData;
        this.imageSrcFace = 'data:image/jpeg;base64,' + decodeURIComponent(this.route.snapshot.queryParams['image']);
        this.imageSrcFront = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFront'));
        this.imageSrcBack = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));
    }
    SelfieImageComponent.prototype.ngOnInit = function () {
        // console.log(this.imageSrcFace);
        // const faceImageJPEG = convertToJPEG(this.imageSrcFace);
        // console.log(faceImageJPEG);
        var _this = this;
        this.user.getUserDetails().subscribe(function (data) {
            _this.userID = data.userID;
            console.log(_this.userID);
        });
    };
    SelfieImageComponent.prototype.frontid = function () {
        var _this = this;
        this.disableButton = true;
        var backImage = new Image();
        backImage.onload = function () {
            var backImageJPEG = convertToJPEG(_this.imageSrcBack);
            //console.log(backImageJPEG);
            var frontImage = new Image();
            frontImage.onload = function () {
                var frontImageJPEG = convertToJPEG(_this.imageSrcFront);
                //console.log(frontImageJPEG);
                // const faceImage = new Image();
                // faceImage.onload = () => {
                var faceImageJPEG = convertToJPEG(_this.imageSrcFace);
                console.log(faceImageJPEG);
                var userID = _this.userID;
                var kycType = 'T01';
                var formData = new FormData();
                formData.append('faceImg', faceImageJPEG);
                formData.append('documentImgFront', frontImageJPEG);
                formData.append('documentImgBack', backImageJPEG);
                formData.append('userID', userID);
                formData.append('kycType', kycType);
                var requestBody = {
                    faceImg: formData.get('faceImg'),
                    documentImgFront: formData.get('documentImgFront'),
                    documentImgBack: formData.get('documentImgBack'),
                    userID: formData.get('userID'),
                    kycType: formData.get('kycType')
                };
                _this.user.verifyKYC(requestBody).subscribe(function (Response) {
                    console.log("Response OCR", Response);
                    if (Response.verificationStatus === 'verification.accepted') {
                        var queryParams = {
                            reference: Response.reference,
                            status: Response.verificationStatus,
                            name: Response.details.name,
                            icNumber: Response.details.icNumber
                        };
                        _this._router.navigate(['/update-info'], { queryParams: queryParams });
                    }
                    else {
                        var queryParams = {
                            reference: Response.reference,
                            status: Response.verificationStatus,
                            name: Response.details.name,
                            icNumber: Response.details.icNumber
                        };
                        _this._router.navigate(['/verify-failed']);
                    }
                    // } else {
                    //   this._router.navigate(['/verify-failed']);
                    // }
                }, function (error) {
                    _this._router.navigate(['/verify-failed']);
                    // this.showAlert = true;
                    //  this.alert = {
                    //    type: 'error',
                    //    message: 'Something went wrong, please try again.'
                    //  };
                });
                //};
                //faceImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFace'));
            };
            frontImage.src = decodeURIComponent(_this.route.snapshot.queryParamMap.get('imageSrcFront'));
        };
        backImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));
        // this._router.navigate(['/update-info'], {
        //   queryParams: {
        //     imageSrcBack: encodeURIComponent(this.imageSrcBack),
        //     imageSrcFront: encodeURIComponent(this.imageSrcFront),
        //     imageSrcFace: encodeURIComponent(this.imageSrcFace)
        //   }
        // });
    };
    SelfieImageComponent.prototype.retakeButton = function () {
        this.location.back();
    };
    SelfieImageComponent = __decorate([
        core_1.Component({
            selector: 'bgd-selfie-image',
            templateUrl: './selfie-image.component.html',
            styleUrls: ['./selfie-image.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], SelfieImageComponent);
    return SelfieImageComponent;
}());
exports.SelfieImageComponent = SelfieImageComponent;
