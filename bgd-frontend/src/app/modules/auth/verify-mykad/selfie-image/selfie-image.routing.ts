import { Route} from '@angular/router';
import { SelfieImageComponent } from 'app/modules/auth/verify-mykad/selfie-image/selfie-image.component';

export const SelfieImageRoutes: Route[] = [
  {
      path     : '',
      component: SelfieImageComponent
  }
];

