import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VerifySuccessRoutes } from 'app/modules/auth/verify-mykad/verify-success/verify-success.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { VerifySuccessComponent } from './verify-success.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        VerifySuccessComponent,

    ],
    imports: [
        RouterModule.forChild(VerifySuccessRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatInputModule,
        MatCheckboxModule,
        // MatSelectModule,
        MatIconModule

    ],


})
export class VerifySuccessModule {
}
