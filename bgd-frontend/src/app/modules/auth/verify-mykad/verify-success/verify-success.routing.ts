import { Route} from '@angular/router';
import { VerifySuccessComponent } from './verify-success.component';

export const VerifySuccessRoutes: Route[] = [
  {
      path     : '',
      component: VerifySuccessComponent
  }
];

