import { Route} from '@angular/router';
import { BankInfoComponent} from './bank-info.component';

export const BankInfoRoutes: Route[] = [
  {
      path     : '',
      component: BankInfoComponent
  }
];

