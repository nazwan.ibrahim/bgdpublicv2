import { Component, ViewChild, ViewEncapsulation, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { ActivatedRoute, Router } from '@angular/router';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators, FormControl } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { switchMap, map } from 'rxjs/operators';
import { registerService } from '../../register/register.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { DatePipe } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { KycserviceService } from '../kycservice.service';


@Component({
  selector: 'bgd-bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.scss']
})
export class BankInfoComponent {

  @ViewChild('AuthRegisterNgForm') AuthRegisterNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  dateOfBirthControl: FormControl;
  dateOfBirthFormatted: string;

  imageSrcFront: any;
  imageSrcBack: any;
  imageSrcFace: any;

  showAlert = false;

  isDateOfBirthDisabled: boolean = true;

  dropdownData = [];
  dropdownData2 = [];
  AuthRegisterForm: UntypedFormGroup;
  fullName: any;
  Id: any;
  bankCode: any;
  bankNum: any = '';
  statusCode: any;
  userID: any;
  response: any;
  backImage: any;
  frontImage: any;
  faceImage: any;
  backImageData: any;
  frontImageData: any;
  faceImageData: any;
  idType: any;
  accountType: any;
  receiverBIC: any;
  secValidInd: any;
  acquaintmeStatus: any;
  acquaintmeReference: any;
  nationality: any;
  name: any;
  icNumber: any;
  dobCheck: string;
  ageCheck: any;
  ageCheckFormatted: any;
  disableButton: boolean = false;

  constructor(
    private _router: Router,
    private datePipe: DatePipe,
    private bankListService: AuthService,
    private route: ActivatedRoute,
    private user: AuthService,
    private dateAdapter: DateAdapter<Date>,
    private countryListService: registerService,
    private kycService: KycserviceService,
    private _formBuilder: UntypedFormBuilder) {

  }

  ngOnInit(): void {

    this.isDateOfBirthDisabled = true;

    this.acquaintmeReference = this.kycService.acquaintmeReference;
    this.acquaintmeStatus = this.kycService.acquaintmeStatus;
    this.name = this.kycService.kycName;
    this.icNumber = this.kycService.kycId;

    console.log("reference", this.acquaintmeReference);
    console.log("status", this.acquaintmeStatus);
    console.log("name", this.name);
    console.log("ic num", this.icNumber);

    this.bankListService.bankList().subscribe(data => {
      this.dropdownData = data;
    });

    // Create the form

    this.dateOfBirthControl = new FormControl('', [
      Validators.required,
    ]);
    this.dateAdapter.setLocale('en-GB');


    this.AuthRegisterForm = this._formBuilder.group({
      // fullName: ['', [Validators.required]],
      // Id: ['', [Validators.required]],
      bankCode: ['', [Validators.required]],
      bankNum: ['', [Validators.required]],
      // dateOfBirth: this.dateOfBirthControl,
      agreements: [false, Validators.requiredTrue]
    });

    this.user.getUserDetails().subscribe(data => {
      //this.statusCode = data.tierStatus.code;
      this.userID = data.userID;
      console.log(this.userID);
      console.log(this.Id);
      console.log(this.icNumber);

    })

  }

  extractDOB(): void {
    const idNumber = this.AuthRegisterForm.get('Id').value;
    const dobDigits = idNumber.replace(/-/g, '').substr(0, 6);
    const year = dobDigits.substr(0, 2);
    const month = dobDigits.substr(2, 2);
    const day = dobDigits.substr(4, 2);

    const currentYearLastTwoDigits = new Date().getFullYear().toString().substr(-2);
    const centuryPrefix = (parseInt(year) <= parseInt(currentYearLastTwoDigits) + 5) ? '20' : '19';
    const fullYear = centuryPrefix + year;

    const dateOfBirth = new Date(`${fullYear}-${month}-${day}`);

    const currentDate = new Date();
    const maxAgeDate = new Date();
    maxAgeDate.setFullYear(maxAgeDate.getFullYear() - 95); // Set the maximum age limit to 95 years

    /* if (dateOfBirth > currentDate || dateOfBirth < maxAgeDate) {

      console.log('Invalid date of birth');
      return;
    }
 */
    this.AuthRegisterForm.get('dateOfBirth').setValue(dateOfBirth);
  }

  convertToYYMMDD(date: Date): string {
    const year = date.getFullYear().toString().substr(-2);
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return year + month + day;
  }

  convertToYY(date: Date): string {
    const year = date.getFullYear().toString();
    return year;
  }


  removeHyphens(id: string): string {
    return id.replace(/-/g, '');
  }

  submitDetails() {
    // const currentDate = new Date();
    // const currentYear = currentDate.getFullYear();

    // const selectedDate = this.AuthRegisterForm.get('dateOfBirth').value;
    // this.dateOfBirthFormatted = this.convertToYYMMDD(selectedDate);
    // this.extractDOB();
    // this.ageCheckFormatted = this.convertToYY(selectedDate);

    // this.ageCheck = currentYear - this.ageCheckFormatted;

    // console.log('Current Year', currentYear);
    // console.log('Year from date picker', this.ageCheckFormatted);

    // console.log('Age', this.ageCheck);

    // console.log('DoB from date picker', this.dateOfBirthFormatted);
    // console.log('DoB from IC', this.dobCheck);

    const fullName = this.name;
    const acctNum = this.bankNum;
    const accountType = "DFLT";
    const idType = "";
    const idNo = "";
    const receiverBIC = this.AuthRegisterForm.get('bankCode').value;
    const secValidInd = "N";
    const acquaintmeReference = this.acquaintmeReference;
    const acquaintmeStatus = this.acquaintmeStatus;
    const identificationTypeCode = "P01";
    const identificationNumber = this.icNumber;
    const userID = this.userID

    console.log(identificationNumber);

    // Check if age is less than 18
    /* if (this.ageCheck < 18) {
      this.showAlert = true;
      this.alert = {
        type: 'error',
        message: 'User must be 18 years old and above to register.'
      };
    }
    // Check if date of birth matches
    else if (this.dateOfBirthFormatted !== this.dobCheck) {
      this.showAlert = true;
      this.alert = {
        type: 'error',
        message: 'Entered date of birth does not match with the one in IC number.'
      };
    }
    else { */
    const body = {
      creditorAccountType: accountType,
      receiverBIC: receiverBIC,
      creditorAccountNo: acctNum,
      secondValidationIndicator: secValidInd,
      idType: idType,
      idNo: idNo,
      fullName: fullName,
      acquaintmeReference: acquaintmeReference,
      acquaintmeStatus: acquaintmeStatus,
      identificationTypeCode: identificationTypeCode,
      identificationNumber: identificationNumber,
      userID: userID,
    };

    console.log(identificationNumber);

    this.user.accountEnquiry(body).subscribe(
      response => {
        console.log("RESPONSE OCR :", response);

        if (response.response === 'SUCCESS') {
          this._router.navigate(['/verify-success']);
        } else if (response.response === 'FAIL') {
          this._router.navigate(['/verify-failed']);
        } else if (response.response === 'PENDING') {
          this._router.navigate(['/verification']);
        } else {
          this._router.navigate(['/error-500']);
        }
      },
      error => {
        console.log(error);
        this._router.navigate(['/verify-failed']);
      }
    );
    /* } */
  }

  
  backButton() {
    this._router.navigate(['/update-info'])
  }


  /*submitKyc() {
    if (this.imageSrcFace && this.imageSrcFront && this.imageSrcBack) {
      // Convert capturedImage to file
      // const selfieByteString = atob(this.imageSrcFace.imageAsDataUrl.split(',')[1]);
      // const selfieMimeType = this.imageSrcFace.imageAsDataUrl.split(',')[0].split(':')[1].split(';')[0];
      // const selfieAb = new ArrayBuffer(selfieByteString.length);
      // const selfieIa = new Uint8Array(selfieAb);
      // for (let i = 0; i < selfieByteString.length; i++) {
      //   selfieIa[i] = selfieByteString.charCodeAt(i);
      // }
      // const selfieImageFile = new File([selfieAb], 'capturedImage.jpeg', { type: selfieMimeType });

      // const imageSrcFace = 'base64-encoded-image-string';
      // let byteCharacters, byteNumbers, byteArray, selfieImageFile;
      // try {
      //   byteCharacters = atob(imageSrcFace);
      //   byteNumbers = new Array(byteCharacters.length);
      //   for (let i = 0; i < byteCharacters.length; i++) {
      //     byteNumbers[i] = byteCharacters.charCodeAt(i);
      //   }
      //   byteArray = new Uint8Array(byteNumbers);
      //   selfieImageFile = new Blob([byteArray], { type: 'image/jpeg' });
      //   console.log(selfieImageFile);
      // } catch (error) {
      //   console.error('Error decoding image:', error);
      // }
      // const imageSrcFace = 'base64-encoded-image-string';
      // const byteCharactersFace = atob(imageSrcFace);
      // const byteNumbersFace = new Array(byteCharactersFace.length);
      // for (let i = 0; i < byteCharactersFace.length; i++) {
      //   byteNumbersFace[i] = byteCharactersFace.charCodeAt(i);
      // }
      // const byteArrayFace = new Uint8Array(byteNumbersFace);
      // const selfieImageFile = new Blob([byteArrayFace], { type: 'image/jpeg' });
      console.log(selfieImageFile);

      const imageSrcFront = 'base64-encoded-image-string';
      const byteCharactersFront = atob(imageSrcFront);
      const byteNumbersFront = new Array(byteCharactersFront.length);
      for (let i = 0; i < byteCharactersFront.length; i++) {
        byteNumbersFront[i] = byteCharactersFront.charCodeAt(i);
      }
      const byteArrayFront = new Uint8Array(byteNumbersFront);
      const frontImageFile = new Blob([byteArrayFront], { type: 'image/jpeg' });
      console.log(frontImageFile);

      const imageSrcBack = 'base64-encoded-image-string';
      const byteCharactersBack = atob(imageSrcBack);
      const byteNumbersBack = new Array(byteCharactersBack.length);
      for (let i = 0; i < byteCharactersBack.length; i++) {
        byteNumbersBack[i] = byteCharactersBack.charCodeAt(i);
      }
      const byteArrayBack = new Uint8Array(byteNumbersBack);
      const backImageFile = new Blob([byteArrayBack], { type: 'image/jpeg' });
      console.log(backImageFile);


      // Convert frontImage to file
      // const frontByteString = atob(this.imageSrcFront.imageAsDataUrl.split(',')[1]);
      // const frontMimeType = this.imageSrcFront.imageAsDataUrl.split(',')[0].split(':')[1].split(';')[0];
      // const frontAb = new ArrayBuffer(frontByteString.length);
      // const frontIa = new Uint8Array(frontAb);
      // for (let i = 0; i < frontByteString.length; i++) {
      //   frontIa[i] = frontByteString.charCodeAt(i);
      // }
      // const frontImageFile = new File([frontAb], 'frontImage.jpeg', { type: frontMimeType });

      // // Convert backImage to file
      // const backByteString = atob(this.imageSrcBack.imageAsDataUrl.split(',')[1]);
      // const backMimeType = this.imageSrcBack.imageAsDataUrl.split(',')[0].split(':')[1].split(';')[0];
      // const backAb = new ArrayBuffer(backByteString.length);
      // const backIa = new Uint8Array(backAb);
      // for (let i = 0; i < backByteString.length; i++) {
      //   backIa[i] = backByteString.charCodeAt(i);
      // }
      // const backImageFile = new File([backAb], 'backImage.jpeg', { type: backMimeType });

      // Set other data
      const userID = this.userID;
      const kycType = 'T01';

      // Create form data and append files and other data

      const formData = new FormData();
      formData.append('faceImg', selfieImageFile, 'capturedImage.jpeg');
      formData.append('documentImgFront', frontImageFile, 'frontImage.jpeg');
      formData.append('documentImgBack', backImageFile, 'backImage.jpeg');
      formData.append('userID', userID);
      formData.append('kycType', kycType);

      console.log(formData);

      // const formData = { faceImage: selfieImageFile, frontImage: frontImageFile, backImage: backImageFile, userID, kycType };

      // Send the form data to the server for KYC
      this.user.verifyKYC(formData).subscribe((response) => {
        console.log('KYC submission response:', response);
      }, (error) => {
        console.error('KYC submission error:', error);
      });
    }
  }*/




  // function convertToJPEG(dataURL: string) {
  //   // Remove the data:image/png;base64, header from the base64 string
  //   const base64String = dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

  //   // Convert the base64 string to a Blob object
  //   const blob = b64toBlob(base64String, "image/jpeg");

  //   // Create a URL for the Blob object
  //   const imageURL = URL.createObjectURL(blob);

  //   return imageURL;
  // }

  // function b64toBlob(b64Data: string, contentType: string) {
  //   contentType = contentType || "";
  //   const sliceSize = 512;
  //   const byteCharacters = atob(b64Data);
  //   const byteArrays = [];
  //   for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
  //     const slice = byteCharacters.slice(offset, offset + sliceSize);
  //     const byteNumbers = new Array(slice.length);
  //     for (let i = 0; i < slice.length; i++) {
  //       byteNumbers[i] = slice.charCodeAt(i);
  //     }
  //     const byteArray = new Uint8Array(byteNumbers);
  //     byteArrays.push(byteArray);
  //   }
  //   const blob = new Blob(byteArrays, { type: contentType });
  //   return blob;
  // }

  // submitKyc() {
  //   function getBase64Image(img: HTMLImageElement) {
  //     // Create an HTML canvas element
  //     const canvas = document.createElement("canvas");

  //     // Set the canvas dimensions to the image dimensions
  //     canvas.width = img.width;
  //     canvas.height = img.height;

  //     // Draw the image on the canvas
  //     const ctx = canvas.getContext("2d");
  //     ctx.drawImage(img, 0, 0);

  //     // Get the base64-encoded data from the canvas
  //     const dataURL = canvas.toDataURL("image/jpeg");

  //     return dataURL;
  //   }

  //   function convertToJPEG(dataURL: string) {
  //     // Remove the data:image/png;base64, header from the base64 string
  //     const base64String = dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

  //     // Convert the base64 string to a Blob object
  //     const byteCharacters = atob(base64String);
  //     const byteArrays = [];
  //     for (let offset = 0; offset < byteCharacters.length; offset += 512) {
  //       const slice = byteCharacters.slice(offset, offset + 512);
  //       const byteNumbers = new Array(slice.length);
  //       for (let i = 0; i < slice.length; i++) {
  //         byteNumbers[i] = slice.charCodeAt(i);
  //       }
  //       const byteArray = new Uint8Array(byteNumbers);
  //       byteArrays.push(byteArray);
  //     }
  //     const blob = new Blob(byteArrays, { type: "image/jpg" });

  //     return blob;
  //   }


  //   const backImage = new Image();
  //   backImage.onload = () => {
  //     //const backImageData = getBase64Image(backImage);
  //     const backImageJPEG = convertToJPEG(this.imageSrcBack);
  //     console.log(backImageJPEG);

  //     const frontImage = new Image();
  //     frontImage.onload = () => {
  //       //const frontImageData = getBase64Image(frontImage);
  //       const frontImageJPEG = convertToJPEG(this.imageSrcFront);
  //       console.log(frontImageJPEG);

  //       const faceImage = new Image();
  //       faceImage.onload = () => {
  //         //const faceImageData = getBase64Image(faceImage);
  //         const faceImageJPEG = convertToJPEG(this.imageSrcFace);
  //         console.log(faceImageJPEG);
  //         const userID = this.userID;
  //         const kycType = 'T01';

  //         // const faceImageFile = new File([faceImageJPEG], 'capturedImage.jpeg', { type: 'image/jpeg' });
  //         // const frontImageFile = new File([frontImageJPEG], 'frontImage.jpeg', { type: 'image/jpeg' });
  //         // const backImageFile = new File([backImageJPEG], 'backImage.jpeg', { type: 'image/jpeg' });
  //         // console.log(faceImageFile);
  //         // console.log(frontImageFile);
  //         // console.log(backImageFile);

  //         const body = {
  //           faceImg: faceImageJPEG,
  //           documentImgFront: frontImageJPEG,
  //           documentImgBack: backImageJPEG,
  //           userID,
  //           kycType
  //         };
  //         // const body = { faceImg: faceImageJPEG, documentImgFront: frontImageJPEG, documentImgBack: backImageJPEG, userID, kycType };
  //         // const formData = new FormData();
  //         // formData.append('faceImg', faceImageJPEG, 'capturedImage.jpeg');
  //         // formData.append('documentImgFront', frontImageJPEG, 'frontImage.jpeg');
  //         // formData.append('documentImgBack', backImageJPEG, 'backImage.jpeg');

  //         // const userID = this.userID;
  //         // const kycType = 'T01';

  //         this.user.verifyKYC(body).subscribe(
  //           (Response) => {
  //             console.log(Response);
  //             this.alert = {
  //               type: 'success',
  //               message: 'Success'
  //             };

  //             this.response = Response;

  //             this._router.navigate(['/verification'], {
  //               // queryParams: {
  //               //   fullName: encodeURIComponent(this.fullName),
  //               //   veriID: encodeURIComponent(this.veriID),
  //               //   bankAcc: encodeURIComponent(this.bankCode),
  //               //   bankNum: encodeURIComponent(this.bankNum),
  //               //   imageSrcBack: encodeURIComponent(this.imageSrcBack),
  //               //   imageSrcFront: encodeURIComponent(this.imageSrcFront),
  //               //   imageSrcFace: encodeURIComponent(this.imageSrcFace),
  //               // }
  //             });
  //           },
  //           (Response) => {

  //             this.alert = {
  //               type: 'error',
  //               message: 'Something went wrong, please try again.'
  //             };
  //           }
  //         );
  //       };
  //       faceImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFace'));
  //     };
  //     frontImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcFront'));
  //   };
  //   backImage.src = decodeURIComponent(this.route.snapshot.queryParamMap.get('imageSrcBack'));

  // }


}
