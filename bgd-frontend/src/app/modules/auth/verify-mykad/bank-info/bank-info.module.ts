import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UpdateInfoRoutes } from 'app/modules/auth/verify-mykad/update-info/update-info.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule, DatePipe } from '@angular/common';
import { BankInfoComponent } from './bank-info.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { BankInfoRoutes } from './bank-info.routing';


@NgModule({
    declarations: [
        BankInfoComponent,

    ],
    imports: [
        RouterModule.forChild(BankInfoRoutes),
        WebcamModule,
        SharedModule,
        CommonModule,
        FormsModule,
        MatStepperModule,
        MatFormFieldModule,
        MatButtonModule,
        MatInputModule,
        MatCheckboxModule,
        MatSelectModule,
        FuseAlertModule,
        MatIconModule,
        MatDatepickerModule,
        MatNativeDateModule
    ],
    providers: [
        DatePipe,
    ],


})
export class BankInfoModule {
}
