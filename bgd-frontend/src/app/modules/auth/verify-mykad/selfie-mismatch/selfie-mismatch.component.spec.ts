import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfieMismatchComponent } from './selfie-mismatch.component';

describe('SelfieMismatchComponent', () => {
  let component: SelfieMismatchComponent;
  let fixture: ComponentFixture<SelfieMismatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelfieMismatchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelfieMismatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
