import { Route} from '@angular/router';
import { SelfieMismatchComponent } from './selfie-mismatch.component';

export const SelfieMismatchRoutes: Route[] = [
  {
      path     : '',
      component: SelfieMismatchComponent
  }
];

