import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'bgd-selfie-mismatch',
  templateUrl: './selfie-mismatch.component.html',
  styleUrls: ['./selfie-mismatch.component.scss']
})
export class SelfieMismatchComponent {
  constructor(
    private _router: Router,
  ) { }

  resubmitButton(){
    this._router.navigate(['/upgrade-account']);
  }

}
