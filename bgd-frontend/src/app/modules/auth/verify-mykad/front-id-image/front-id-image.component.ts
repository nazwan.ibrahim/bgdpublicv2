import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';


@Component({
  selector: 'bgd-front-id',
  templateUrl: './front-id-image.component.html',
  styleUrls: ['./front-id-image.component.scss']
})
export class FrontIdImageComponent {

  public imageSrc: string = null;

  constructor(private route: ActivatedRoute, private _router: Router) {

  }
  ngOnInit() {
    const imageData = this.route.snapshot.queryParams['image'];
    if (imageData) {
      this.imageSrc = 'data:image/jpeg;base64,' + imageData;
    } else {
      const imageSrcFront = decodeURIComponent(this.route.snapshot.queryParams['imageSrcFront']);
      this.imageSrc = imageSrcFront;
    }
  }

  frontid() {

    this._router.navigate(['/back-id'], {
      queryParams: {
        imageSrcFront: encodeURIComponent(this.imageSrc)
      }
    });
  }

  retakeButton() {

    this._router.navigate(['/identity-verification']);
  }

}