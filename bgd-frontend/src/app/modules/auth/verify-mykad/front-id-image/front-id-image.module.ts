import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FrontIdImageRoutes } from 'app/modules/auth/verify-mykad/front-id-image/front-id-image.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { FrontIdImageComponent } from './front-id-image.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        FrontIdImageComponent,

    ],
    imports: [
        RouterModule.forChild(FrontIdImageRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatIconModule

        // MatSelectModule,
    ],


})
export class FrontIdImageModule {
}
