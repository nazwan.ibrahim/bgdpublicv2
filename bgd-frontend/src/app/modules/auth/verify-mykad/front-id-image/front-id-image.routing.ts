import { Route } from '@angular/router';
import { FrontIdImageComponent } from 'app/modules/auth/verify-mykad/front-id-image/front-id-image.component';

export const FrontIdImageRoutes: Route[] = [
  {
    path: '',
    component: FrontIdImageComponent
  }
];

