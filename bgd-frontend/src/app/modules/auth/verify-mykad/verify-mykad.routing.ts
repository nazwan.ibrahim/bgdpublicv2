import { Route} from '@angular/router';
import { VerifyMykadComponent } from 'app/modules/auth/verify-mykad/verify-mykad.component';

export const VerifyMykadRoutes: Route[] = [
  {
      path     : '',
      component: VerifyMykadComponent
  }
];

