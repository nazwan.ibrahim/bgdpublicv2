import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VerificationRoutes } from 'app/modules/auth/verify-mykad/verification/verification.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { VerificationComponent } from './verification.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        VerificationComponent,

    ],
    imports: [
        RouterModule.forChild(VerificationRoutes),
        WebcamModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        MatInputModule,
        MatCheckboxModule,
        // MatSelectModule,
        MatIconModule

    ],


})
export class VerificationModule {
}
