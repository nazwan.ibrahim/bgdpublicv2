import { Route} from '@angular/router';
import { VerificationComponent } from './verification.component';

export const VerificationRoutes: Route[] = [
  {
      path     : '',
      component: VerificationComponent
  }
];

