import { TestBed } from '@angular/core/testing';

import { KycserviceService } from './kycservice.service';

describe('KycserviceService', () => {
  let service: KycserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KycserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
