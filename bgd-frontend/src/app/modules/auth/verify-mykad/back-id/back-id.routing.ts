import { Route } from '@angular/router';
import { BackIdComponent } from 'app/modules/auth/verify-mykad/back-id/back-id.component';

export const BackIdRoutes: Route[] = [
  {
    path: '',
    component: BackIdComponent
  }
];

