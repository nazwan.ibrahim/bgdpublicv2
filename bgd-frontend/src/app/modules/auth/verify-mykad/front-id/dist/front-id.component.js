"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.FrontIdComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var ngx_webcam_1 = require("ngx-webcam");
var camera_unavailable_component_1 = require("../../camera-unavailable/camera-unavailable.component");
var FrontIdComponent = /** @class */ (function () {
    function FrontIdComponent(_router, location, dialog) {
        this._router = _router;
        this.location = location;
        this.dialog = dialog;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.isDialogOpen = false;
        this.isCaptured = false;
        this.pictureTaken = new core_1.EventEmitter();
        this.overlayWidth = 0;
        this.overlayHeight = 0;
        this.overlayMarginTop = 0;
        this.overlayMarginLeft = 0;
        // toggle webcam on/off
        this.showWebcam = true;
        this.allowCameraSwitch = true;
        this.multipleWebcamsAvailable = false;
        this.videoOptions = {
            width: { ideal: 1024 },
            height: { ideal: 576 }
        };
        this.errors = [];
        // webcam snapshot trigger
        this.trigger = new rxjs_1.Subject();
        // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
        this.nextWebcam = new rxjs_1.Subject();
        this.webcamAvailable = false;
        this.webcamImage = null;
    }
    FrontIdComponent.prototype.ngOnInit = function () {
        var _this = this;
        ngx_webcam_1.WebcamUtil.getAvailableVideoInputs()
            .then(function (mediaDevices) {
            _this.webcamAvailable = mediaDevices && mediaDevices.length > 0;
            console.log(_this.webcamAvailable);
            _this.webcamAvailable = false;
        });
        navigator.mediaDevices.getUserMedia({ video: true })
            .then(function () {
            _this.webcamAvailable = true;
            console.log("Webcam is available");
        })["catch"](function (error) {
            if (error.name === "NotFoundError" || error.name === "DevicesNotFoundError") {
                _this.webcamAvailable = false;
                console.log("Webcam not found");
                console.log(_this.webcamAvailable);
                _this.openDialog();
            }
            else if (error.name === "NotAllowedError" || error.name === "PermissionDeniedError") {
                _this.webcamAvailable = false;
                console.log("Webcam access denied");
                console.log(_this.webcamAvailable);
                _this.openDialog();
            }
            else {
                _this.webcamAvailable = false;
                console.log("Failed to access webcam", error);
                console.log(_this.webcamAvailable);
                _this.openDialog();
            }
        });
    };
    FrontIdComponent.prototype.triggerSnapshot = function () {
        this.trigger.next();
    };
    FrontIdComponent.prototype.removecurrent = function () {
        this.trigger.asObservable();
    };
    FrontIdComponent.prototype.toggleWebcam = function () {
        this.showWebcam = !this.showWebcam;
    };
    FrontIdComponent.prototype.openDialog = function () {
        var _this = this;
        this.isDialogOpen = true;
        var dialogRef = this.dialog.open(camera_unavailable_component_1.CameraUnavailableComponent, {
            data: {}
        });
        dialogRef.afterClosed().subscribe(function () {
            _this._router.navigate(['/dashboard']);
        });
    };
    FrontIdComponent.prototype.handleInitError = function (error) {
        switch (error.message) {
            case 'NotFoundError':
                this.showAlert = true;
                this.alert.type = 'error';
                this.alert.message = 'No webcam found. Please make sure your webcam is connected and try again.';
                break;
            case 'NotAllowedError':
                this.showAlert = true;
                this.alert.type = 'error';
                this.alert.message = 'Webcam access is not allowed. Please grant webcam access and try again.';
                break;
            default:
                console.error(error);
                this.showAlert = true;
                this.alert.type = 'error';
                this.alert.message = 'Failed to initialize webcam. Please try again later.';
                break;
        }
    };
    FrontIdComponent.prototype.showNextWebcam = function (directionOrDeviceId) {
        // true => move forward through devices
        // false => move backwards through devices
        // string => move to device with given deviceId
        this.nextWebcam.next(directionOrDeviceId);
    };
    FrontIdComponent.prototype.handleImage = function (webcamImage) {
        console.info('received webcam image', webcamImage);
        this.pictureTaken.emit(webcamImage);
        this.webcamImage = webcamImage;
        this._router.navigate(['/front-id-image'], { queryParams: { image: webcamImage.imageAsBase64 } });
    };
    FrontIdComponent.prototype.cameraWasSwitched = function (deviceId) {
        console.log('active device: ' + deviceId);
        this.deviceId = deviceId;
    };
    Object.defineProperty(FrontIdComponent.prototype, "triggerObservable", {
        get: function () {
            return this.trigger.asObservable();
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(FrontIdComponent.prototype, "nextWebcamObservable", {
        get: function () {
            return this.nextWebcam.asObservable();
        },
        enumerable: false,
        configurable: true
    });
    FrontIdComponent.prototype.backButton = function () {
        this._router.navigate(['/identity-verification']);
    };
    __decorate([
        core_1.Output()
    ], FrontIdComponent.prototype, "pictureTaken");
    FrontIdComponent = __decorate([
        core_1.Component({
            selector: 'bgd-front-id',
            templateUrl: './front-id.component.html',
            styleUrls: ['./front-id.component.scss']
        })
    ], FrontIdComponent);
    return FrontIdComponent;
}());
exports.FrontIdComponent = FrontIdComponent;
