import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { MatDialog } from '@angular/material/dialog';
import { CameraUnavailableComponent } from '../../camera-unavailable/camera-unavailable.component';
import { Location } from '@angular/common';


@Component({
  selector: 'bgd-front-id',
  templateUrl: './front-id.component.html',
  styleUrls: ['./front-id.component.scss']
})
export class FrontIdComponent implements OnInit {

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };
  showAlert = false;
  isDialogOpen: boolean = false;

  constructor(
    private _router: Router,
    private location: Location,
    public dialog: MatDialog
  ) {

  }

  isCaptured: boolean = false;
  @Output()
  public pictureTaken = new EventEmitter<WebcamImage>();

  public overlayWidth = 0;
  public overlayHeight = 0;
  public overlayMarginTop = 0;
  public overlayMarginLeft = 0;

  // toggle webcam on/off
  public showWebcam = true;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  public deviceId: string;
  public videoOptions: MediaTrackConstraints = {
    width: { ideal: 1024 },
    height: { ideal: 576 }
  };
  public errors: WebcamInitError[] = [];

  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
  private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

  public webcamAvailable: boolean = false;

  public ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.webcamAvailable = mediaDevices && mediaDevices.length > 0;
        console.log(this.webcamAvailable);
        this.webcamAvailable = false;
      });

    navigator.mediaDevices.getUserMedia({ video: true })
      .then(() => {
        this.webcamAvailable = true;
        console.log("Webcam is available");
      })
      .catch((error) => {
        if (error.name === "NotFoundError" || error.name === "DevicesNotFoundError") {
          this.webcamAvailable = false;
          console.log("Webcam not found");
          console.log(this.webcamAvailable);
          this.openDialog();
        } else if (error.name === "NotAllowedError" || error.name === "PermissionDeniedError") {
          this.webcamAvailable = false;
          console.log("Webcam access denied");
          console.log(this.webcamAvailable);
          this.openDialog();
        } else {
          this.webcamAvailable = false;
          console.log("Failed to access webcam", error);
          console.log(this.webcamAvailable);
          this.openDialog();
        }
      });
  }
  public triggerSnapshot(): void {
    this.trigger.next();
  }

  public removecurrent() {
    this.trigger.asObservable();
  }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  openDialog(): void {
    this.isDialogOpen = true;

    const dialogRef = this.dialog.open(CameraUnavailableComponent, {
      data: {
      }
    });

    dialogRef.afterClosed().subscribe(() => {
      this._router.navigate(['/dashboard']);
    });
  }

  public handleInitError(error: WebcamInitError): void {
    switch (error.message) {
      case 'NotFoundError':
        this.showAlert = true;
        this.alert.type = 'error';
        this.alert.message = 'No webcam found. Please make sure your webcam is connected and try again.';
        break;
      case 'NotAllowedError':
        this.showAlert = true;
        this.alert.type = 'error';
        this.alert.message = 'Webcam access is not allowed. Please grant webcam access and try again.';
        break;
      default:
        console.error(error);
        this.showAlert = true;
        this.alert.type = 'error';
        this.alert.message = 'Failed to initialize webcam. Please try again later.';
        break;
    }
  }

  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    // true => move forward through devices
    // false => move backwards through devices
    // string => move to device with given deviceId
    this.nextWebcam.next(directionOrDeviceId);
  }
  public webcamImage: WebcamImage = null;

  public handleImage(webcamImage: WebcamImage): void {
    console.info('received webcam image', webcamImage);
    this.pictureTaken.emit(webcamImage);
    this.webcamImage = webcamImage;
    this._router.navigate(['/front-id-image'], { queryParams: { image: webcamImage.imageAsBase64 } });


  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean | string> {
    return this.nextWebcam.asObservable();
  }

  backButton() {
    this._router.navigate(['/identity-verification']);
  }

}