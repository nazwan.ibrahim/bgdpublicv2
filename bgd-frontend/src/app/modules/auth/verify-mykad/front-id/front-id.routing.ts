import { Route } from '@angular/router';
import { FrontIdComponent } from 'app/modules/auth/verify-mykad/front-id/front-id.component';

export const FrontIdRoutes: Route[] = [
  {
    path: '',
    component: FrontIdComponent
  }
];

