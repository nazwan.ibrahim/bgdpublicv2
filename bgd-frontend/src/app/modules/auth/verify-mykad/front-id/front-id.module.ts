import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FrontIdRoutes } from 'app/modules/auth/verify-mykad/front-id/front-id.routing';
import { WebcamModule } from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { FrontIdComponent } from './front-id.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
    declarations: [
        FrontIdComponent,

    ],
    imports: [
        RouterModule.forChild(FrontIdRoutes),
        WebcamModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule,
        CommonModule,

        // MatSelectModule,
    ],


})
export class FrontIdModule {
}
