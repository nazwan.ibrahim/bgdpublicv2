import { Route } from '@angular/router';
import { TransactionLimitComponent } from 'app/modules/auth/transaction-limit/transaction-limit.component';

export const TransactionLimitRoutes: Route[] = [
    {
        path: '',
        component: TransactionLimitComponent
    }
];
