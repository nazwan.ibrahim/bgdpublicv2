import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TransactionLimitRoutes } from 'app/modules/auth/transaction-limit/transaction-limit.routing';
import { CommonModule } from '@angular/common';
import { TransactionLimitComponent } from './transaction-limit.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        TransactionLimitComponent,

    ],
    imports: [
        RouterModule.forChild(TransactionLimitRoutes),
        CommonModule,
        MatButtonModule,
        MatIconModule
    ],


})
export class TransactionLimitModule {
}
