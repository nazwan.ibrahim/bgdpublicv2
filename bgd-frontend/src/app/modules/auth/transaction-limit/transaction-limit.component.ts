import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'bgd-transaction-limit',
  templateUrl: './transaction-limit.component.html',
  styleUrls: ['./transaction-limit.component.scss']
})
export class TransactionLimitComponent {

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
  ) { }

  upgradeAccount() {
    this._router.navigate(['/dashboard']);
  }

}
