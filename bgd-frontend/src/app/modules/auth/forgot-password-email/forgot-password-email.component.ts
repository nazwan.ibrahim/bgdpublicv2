import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@bgd/animations';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthSharedService } from '../auth-shared.service';

@Component({
    selector: 'bgd-forgot-password-email',
    templateUrl: './forgot-password-email.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
  })
  export class ForgotPasswordEmailComponent{

    alert: { type: FuseAlertType; message: string } = {
      type   : 'success',
      message: ''
    };

    showAlert = false;
    email: any=''

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private authSharedService: AuthSharedService,
        ){}
    // {
    //   this._activatedRoute.paramMap.subscribe(params=>{
    //       this.email = params.get('data')})

      
    //   this.email = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('data'))
    // }

    ngOnInit(){
      this.email = this.authSharedService.email;
    }
  }