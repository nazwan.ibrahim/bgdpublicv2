import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { ForgotPasswordEmailComponent } from 'app/modules/auth/forgot-password-email/forgot-password-email.component';
import { ForgotPasswordEmailRoutes } from 'app/modules/auth/forgot-password-email/forgot-password-email.routing';

@NgModule({
    declarations: [
        ForgotPasswordEmailComponent
    ],
    imports     : [
        RouterModule.forChild(ForgotPasswordEmailRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule
    ]
})
export class ForgotPasswordEmailModule
{
}
