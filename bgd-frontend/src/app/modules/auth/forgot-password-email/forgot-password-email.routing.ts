import { Route } from '@angular/router';
import { ForgotPasswordEmailComponent } from 'app/modules/auth/forgot-password-email/forgot-password-email.component';

export const ForgotPasswordEmailRoutes: Route[] = [
    {
        path     : '',
        component: ForgotPasswordEmailComponent
    }
];
