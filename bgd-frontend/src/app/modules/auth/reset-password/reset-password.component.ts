import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators, AbstractControl, ValidationErrors, FormControl } from '@angular/forms';
import { finalize, map, catchError, of } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseValidators } from '@bgd/validators';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
    selector: 'auth-reset-password',
    templateUrl: './reset-password.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AuthResetPasswordComponent implements OnInit {
    @ViewChild('resetPasswordNgForm') resetPasswordNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };
    resetPasswordForm: UntypedFormGroup;
    showAlert = false;
    hasCommonWord = false;
    newPassword: any;

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private route: ActivatedRoute,
        private _router: Router,

    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Create the form
        this.resetPasswordForm = this._formBuilder.group({
            newPassword: ['', [Validators.required,
            Validators.minLength(8),
            Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()\\[\\]\\-_+={}:";\'\',<.>/?]).*$')],
                this.checkPassword.bind(this)],
            confirmPassword: ['', Validators.required],
            id: this.route.snapshot.queryParamMap.get('verificationID'),
            userID: this.route.snapshot.queryParamMap.get('userID'),

        },
            {
                validators: FuseValidators.mustMatch('newPassword', 'confirmPassword')
            }
        );

        this.resetPasswordForm.get('newPassword')?.valueChanges.subscribe(() => {
            this.hasCommonWord = false;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // clearAutofill() {
    //     const newPasswordField = document.getElementById('password') as HTMLInputElement;
    //     const confirmPasswordField = document.getElementById('password-confirm') as HTMLInputElement;
    //     newPasswordField.removeAttribute('readonly');
    //     confirmPasswordField.removeAttribute('readonly');
    // }

    /**
     * Reset password
     */
    resetPassword(): void {
        // Return if the form is invalid
        if (this.resetPasswordForm.invalid) {
            return;
        }

        // Disable the form
        this.resetPasswordForm.disable();

        // Hide the alert
        this.showAlert = false;


        // Send the request to the server
        this._authService.ResetPassword(this.resetPasswordForm.value)
            .pipe(
                finalize(() => {

                    // Re-enable the form
                    this.resetPasswordForm.enable();

                    // Show the alert
                    this.showAlert = true;

                    // Hide the alert after 3 seconds
                    setTimeout(() => {
                        this.showAlert = false;
                    }, 5000);
                })
            )
            .subscribe(
                (response) => {

                    // Set the alert
                    this.alert = {
                        type: 'success',
                        message: 'Your password has been reset successfully.'
                    };

                    // Reset the form
                    this.resetPasswordNgForm.resetForm();

                    // setTimeout(() => {
                    //     this._router.navigate(['/sign-in']);
                    // }, 3000);
                },
                (error) => {
                    if (error.status == 400) {
                        this.alert = {
                            type: 'error',
                            message: 'Your password must be different from the previous password.'
                        };
                    } else if (error.status == 404) {
                        this.alert = {
                            type: 'error',
                            message: 'This URL link has expired and is no longer accessible. Please try again.'
                        };
                    } else {
                        this.alert = {
                            type: 'error',
                            message: 'Something went wrong, please try again.'
                        };
                    }

                    // Re-enable the form
                    this.resetPasswordForm.enable();

                }
            );
    }

    get password() {
        return this.resetPasswordForm.get('newPassword');
    }

    get passwordLength() {
        return this.resetPasswordForm.get('newPassword')?.value?.length;
    }

    get hasLowerCase() {
        return /[a-z]/.test(this.resetPasswordForm.get('newPassword')?.value);
    }

    get hasUpperCase() {
        return /[A-Z]/.test(this.resetPasswordForm.get('newPassword')?.value);
    }

    get hasNumber() {
        return /[0-9]/.test(this.resetPasswordForm.get('newPassword')?.value);
    }

    get hasSpecialChar() {
        return /[!@#$%^&*()\[\]\-_+={}:";',<.>/?]/.test(this.resetPasswordForm.get('newPassword')?.value);
    }

    get hasMatchPassword() {
        const password = this.resetPasswordForm.get('newPassword')?.value;
        const confirmPassword = this.resetPasswordForm.get('confirmPassword')?.value;

        if (password === null || confirmPassword === null || confirmPassword.trim() === '') {
            return false;
        }

        if (password !== null && confirmPassword !== null) {
            if (password === confirmPassword) {
                return true;
            }
        }

        return false;
    }

    async checkPassword(control: FormControl): Promise<ValidationErrors | null> {
        const newPassword = control.value;
        const password = { password: newPassword };

        try {
            const response = await this._authService.ValidatePassword(password).toPromise();

            const validateMessage = JSON.parse(response);


            if (validateMessage.message === "validated") {
                this.hasCommonWord = true;
            } else {
                this.hasCommonWord = false;
            }

            return null;
        } catch (error) {

            return { validationError: true };
        }
    }

}
