import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupPinLandingComponent } from './setup-pin-landing.component';

describe('SetupPinLandingComponent', () => {
  let component: SetupPinLandingComponent;
  let fixture: ComponentFixture<SetupPinLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupPinLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SetupPinLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
