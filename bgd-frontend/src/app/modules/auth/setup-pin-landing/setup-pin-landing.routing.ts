import { Route } from '@angular/router';
import { SetupPinLandingComponent } from 'app/modules/auth/setup-pin-landing/setup-pin-landing.component';

export const SetupPinLandingRoutes: Route[] = [
    {
        path: '',
        component: SetupPinLandingComponent
    }
];

