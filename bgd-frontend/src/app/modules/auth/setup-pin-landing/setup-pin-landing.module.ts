import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { SetupPinLandingComponent } from 'app/modules/auth/setup-pin-landing/setup-pin-landing.component';
import { SetupPinLandingRoutes } from 'app/modules/auth/setup-pin-landing/setup-pin-landing.routing';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { NgxPincodeModule } from 'ngx-pincode';

@NgModule({
    declarations: [
        SetupPinLandingComponent,

    ],
    imports: [
        RouterModule.forChild(SetupPinLandingRoutes),
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        MatListModule,
        MatIconModule,
        NgxPincodeModule,
    ]
})
export class SetupPinLandingModule {
}