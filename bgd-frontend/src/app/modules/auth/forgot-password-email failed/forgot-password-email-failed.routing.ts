import { Route } from '@angular/router';
import { ForgotPasswordEmailFailedComponent } from './forgot-password-email-failed.component';

export const ForgotPasswordEmailFailedRoutes: Route[] = [
    {
        path     : '',
        component: ForgotPasswordEmailFailedComponent
    }
];
