import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageIdPreviewComponent } from './image-id-preview.component';

describe('ImageIdPreviewComponent', () => {
  let component: ImageIdPreviewComponent;
  let fixture: ComponentFixture<ImageIdPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageIdPreviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImageIdPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
