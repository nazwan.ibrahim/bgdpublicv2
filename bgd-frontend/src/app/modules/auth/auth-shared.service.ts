import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthSharedService {

  //Forgot Password
  email: any;
  userSecurityQuestionDesc: any;
  userSecurityQuestionCode: any;
  data: any;

  //Create Account
  fullName: any;
  phoneNumber: any;
  serviceSid: any;
  params: any;
  message: any;

  registerEmail: any;
  registerPassword: any;
  registerConfirmPassword: any;

  constructor() { }
}
