import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthSharedService } from '../auth-shared.service';

@Component({
    selector: 'auth-forgot-password',
    templateUrl: './forgot-password.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AuthForgotPasswordComponent implements OnInit {
    @ViewChild('forgotPasswordNgForm') forgotPasswordNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };

    forgotPasswordForm: UntypedFormGroup;
    showAlert = false;
    userSecurityQuestion: any;
    userSecurityQuestionDesc: any;
    userSecurityQuestionCode: any;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private authSharedService: AuthSharedService,
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Create the form
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    clearAutofill() {
        const email = document.getElementById('email') as HTMLInputElement;
        email.removeAttribute('readonly');
    }

    nextButton() {
        this.authSharedService.email = this.forgotPasswordForm.get('email').value;

        this._authService.checkSecurityQuestion(this.authSharedService.email)
            .subscribe(
                Response => {

                    this.alert = {
                        type: 'success',
                        message: 'Success'
                    };

                    let response = JSON.parse(Response);

                    this.userSecurityQuestion = response.userSecurityQuestion;
                    this.authSharedService.userSecurityQuestionDesc = response.userSecurityQuestion[0].description;
                    this.authSharedService.userSecurityQuestionCode = response.userSecurityQuestion[0].code;

                    this._router.navigate(['/forgot-password-question']
                    // , { queryParams: { email: encodeURIComponent(email), que: this.userSecurityQuestionDesc, code: this.userSecurityQuestionCode } }
                    )

                },
                Response => {

                    this.alert = {
                        type: 'error',
                        message: 'Something went wrong, please try again.'
                    };

                }
            )

    }
}
