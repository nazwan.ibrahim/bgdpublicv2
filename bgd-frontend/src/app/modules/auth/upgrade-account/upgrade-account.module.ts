import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UpgradeAccountRoutes } from 'app/modules/auth/upgrade-account/upgrade-account.routing';
import { CommonModule } from '@angular/common';
import { UpgradeAccountComponent } from './upgrade-account.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        UpgradeAccountComponent,

    ],
    imports: [
        RouterModule.forChild(UpgradeAccountRoutes),
        CommonModule,
        MatButtonModule,
        MatIconModule
    ],


})
export class UpgradeAccountModule {
}
