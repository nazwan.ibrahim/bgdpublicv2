import { Route} from '@angular/router';
import { UpgradeAccountComponent } from 'app/modules/auth/upgrade-account/upgrade-account.component';

export const UpgradeAccountRoutes: Route[] = [
  {
      path     : '',
      component: UpgradeAccountComponent
  }
];

