import { Component, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector: 'otp-phone',
    templateUrl  : './verify-otp-phone.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations


})

export class VerifyOtpPhoneComponent {


    showAlert = false;
    alert: any;

    /**
     * Constructor
     */
     constructor()
     {
     }
     
}
