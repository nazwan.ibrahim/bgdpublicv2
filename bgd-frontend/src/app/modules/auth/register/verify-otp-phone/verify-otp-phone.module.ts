import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { authPasswordRoutes } from 'app/modules/auth/password/password.routing';
import { MatRadioModule } from '@angular/material/radio';
import {MatStepperModule} from '@angular/material/stepper';
import {MatSelectModule} from '@angular/material/select';
import { VerifyOtpPhoneRoutes } from 'app/modules/auth/register/verify-otp-phone/verify-otp-phone.routing';
import { VerifyOtpPhoneComponent } from './verify-otp-phone.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgOtpInputModule } from 'ng-otp-input';


@NgModule({
    declarations: [
        VerifyOtpPhoneComponent
    ],
    imports: [
        RouterModule.forChild(VerifyOtpPhoneRoutes),
        MatButtonModule,
        FuseCardModule,
        SharedModule,
        NgOtpInputModule,
        FuseAlertModule,
        MatFormFieldModule
    ]
})
export class VerifyOtpPhoneModule
{
}
