import { Route } from '@angular/router';
import { VerifyOtpPhoneComponent } from 'app/modules/auth/register/verify-otp-phone/verify-otp-phone.component';

export const VerifyOtpPhoneRoutes: Route[] = [
    {
        path     : '',
        component: VerifyOtpPhoneComponent
    }
];
