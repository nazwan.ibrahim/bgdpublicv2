import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { registerService } from './register.service';
import { AuthSharedService } from '../auth-shared.service';

@Component({
    selector: 'register',
    templateUrl: 'register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AuthRegisterComponent implements OnInit {
    @ViewChild('AuthRegisterNgForm') AuthRegisterNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };

    AuthRegisterForm: UntypedFormGroup;
    showAlert = false;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private registerService: registerService,
        private authSharedService: AuthSharedService,
    ) {
    }

    ngOnInit(): void {
        this.AuthRegisterForm = this._formBuilder.group({
            fullName: ['', Validators.required],
            phoneNumber: ['', [Validators.required, Validators.pattern(/^\+(?:[0-9] ?){6,14}[0-9]$/)]],
        });
    }

    sendOtp(): void {
        if (this.AuthRegisterForm.invalid) {
            return;
        }

        this.AuthRegisterForm.disable();

        // Hide the alert
        this.showAlert = false;

        this.registerService.sendOtp(this.AuthRegisterForm.value.phoneNumber)
            .pipe(
                finalize(() => {

                    // Re-enable the form
                    this.AuthRegisterForm.enable();

                    // Reset the form
                    this.AuthRegisterNgForm.resetForm();

                    // Show the alert
                    this.showAlert = true;
                })
            )
            .subscribe(
                (response) => {

                    this.alert = {
                        type: 'success',
                        message: 'OTP sent successfully'
                    };

                    this.authSharedService.fullName = this.AuthRegisterForm.get('fullName').value;
                    /* console.log(this.authSharedService.fullName); */

                    this.authSharedService.phoneNumber = this.AuthRegisterForm.get('phoneNumber').value;
                    /* console.log(this.authSharedService.phoneNumber); */

                    this.authSharedService.serviceSid = response.service_sid;
                    /* console.log(this.authSharedService.serviceSid) */;

                    this._router.navigate(['/otp-phone']
                    // , { queryParams: { fullName: fullName, phoneNumber: phoneNumber, serviceSid: serviceSid } }
                    )
                },
                (response) => {

                    // Set the alert
                    this.alert = {
                        type: 'error',
                        message: 'Something went wrong, please try again.'
                    };

                    // Show the alert
                    this.showAlert = true;
                }
            );
    }

}
