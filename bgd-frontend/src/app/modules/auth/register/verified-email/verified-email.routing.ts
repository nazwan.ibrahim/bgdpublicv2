import { Route} from '@angular/router';
import { VerifiedEmailComponent } from 'app/modules/auth/register/verified-email/verified-email.component';

export const VerifiedEmailRoutes: Route[] = [
  {
      path     : '',
      component: VerifiedEmailComponent
  }
];

