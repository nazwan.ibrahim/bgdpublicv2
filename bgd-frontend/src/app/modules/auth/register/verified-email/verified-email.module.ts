import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { FuseCardModule } from '@bgd/components/card';
import { SharedModule } from 'app/shared/shared.module';
import { VerifiedEmailRoutes } from 'app/modules/auth/register/verified-email/verified-email.routing';
import { VerifiedEmailComponent } from './verified-email.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        VerifiedEmailComponent
    ],
    imports: [
        RouterModule.forChild(VerifiedEmailRoutes),
        MatButtonModule,
        FuseCardModule,
        SharedModule,
        MatIconModule
    ]
})
export class VerifiedEmailModule {
}
