import { Route } from '@angular/router';
import { AuthRegisterComponent } from 'app/modules/auth/register/register.component';

export const authRegisterRoutes: Route[] = [
    {
        path     : '',
        component: AuthRegisterComponent
    }
];
