import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { registerService } from '../register.service';
import { Location } from '@angular/common';
import { AuthSharedService } from '../../auth-shared.service';

@Component({
    selector: 'security-questions',
    templateUrl: './security-questions.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class securityQuestionsComponent implements OnInit {
    @ViewChild('securityQuestionsNgForm') securityQuestionsNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };

    securityQuestionsForm: UntypedFormGroup;
    showAlert = false;

    dropdownData = [];
    params: any = '';
    params2: any = '';
    phoneNumber: any = '';
    serviceSid: any = '';
    otp: any = '';
    fullName: any = '';
    email: any = '';
    password: any = '';
    confirmPassword: any = '';
    message: string;
    userID: any;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private registerService: registerService,
        private AuthSharedService: AuthSharedService,
        private location: Location
    ) {    }

    ngOnInit(): void {
        this.registerService.securityQuestions().subscribe(data => {
            this.dropdownData = data;
        });

        this.securityQuestionsForm = this._formBuilder.group({
            questionCode: ['', Validators.required],
            answer: ['', Validators.required],
            agreements: [false, Validators.requiredTrue]
        });
    }

    goBack(): void {
        this.location.back();
    }

    register(): void {

        this.fullName = this.AuthSharedService.fullName;
        this.email = this.AuthSharedService.registerEmail;
        this.phoneNumber = this.AuthSharedService.phoneNumber;
        this.password = this.AuthSharedService.registerPassword;
        this.confirmPassword = this.AuthSharedService.registerConfirmPassword;
        
        if (this.securityQuestionsForm.invalid) {
            return;
        }

        this.securityQuestionsForm.disable();

        this.showAlert = false;

        const formregister = {
            nationalityCode: '',
            fullName: this.fullName,
            email: this.email,
            identificationTypeCode: '',
            identificationNumber: '',
            phoneNumber: this.phoneNumber,
            password: this.password,
            confirmPassword: this.confirmPassword,
            questionCode: this.securityQuestionsForm.value.questionCode,
            answer: this.securityQuestionsForm.value.answer
        }

        console.log("FORM ", formregister);

        // Register
        this.registerService.register(formregister)
            .pipe(
                finalize(() => {
                    this.securityQuestionsForm.enable();

                    this.securityQuestionsForm.reset();

                    this.showAlert = true;

                    // Hide the alert after 3 seconds
                    setTimeout(() => {
                        this.showAlert = false;
                    }, 3000);
                })
            )
            .subscribe(
                (response) => {

                    this.alert = {
                        type: 'success',
                        message: 'Registration completed successfully'
                    };

                    const userID = response.userID;
                    console.log(userID);

                    this._router.navigate(['/confirmation-required'], { queryParams: { email: this.email } });


                    this.registerService.verifyEmail(userID)
                        .subscribe(
                            response => {

                                this.alert = {
                                    type: 'success',
                                    message: response.response.message
                                };

                                console.log(this.message);

                                this._router.navigate(['/confirmation-required'], { queryParams: { email: this.email } });
                            },
                            error => {
                                this.alert = {
                                    type: 'error',
                                    message: error.error.message
                                };
                            }
                        )

                },
                (error) => {

                    if (error.status == 400) {

                        this.alert = {
                            type: 'error',
                            message: error.error.message
                        };

                    } else {

                        this.alert = {
                            type: 'error',
                            message: 'Something went wrong, please try again.'
                        };

                    }


                });
    }
}
