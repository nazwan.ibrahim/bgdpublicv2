import { Route } from '@angular/router';
import { securityQuestionsComponent } from './security-questions.component';

export const securityQuestionsRoutes: Route[] = [
    {
        path     : '',
        component: securityQuestionsComponent
    }
];
