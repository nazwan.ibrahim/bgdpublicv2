"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.securityQuestionsComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var animations_1 = require("@bgd/animations");
var securityQuestionsComponent = /** @class */ (function () {
    function securityQuestionsComponent(_activatedRoute, _router, _authService, _formBuilder, registerService, location) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._authService = _authService;
        this._formBuilder = _formBuilder;
        this.registerService = registerService;
        this.location = location;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.dropdownData = [];
        this.params = '';
        this.params2 = '';
        this.phoneNumber = '';
        this.serviceSid = '';
        this.otp = '';
        this.fullName = '';
        this.email = '';
        this.password = '';
        this.confirmPassword = '';
        var params = JSON.parse(this._activatedRoute.snapshot.queryParams['params']);
        this.fullName = params.fullName;
        this.phoneNumber = params.phoneNumber;
        this.serviceSid = params.serviceSid;
        this.otp = params.otp;
        var params2 = JSON.parse(this._activatedRoute.snapshot.queryParams['params2']);
        this.email = params2.email;
        this.password = params2.password;
        this.confirmPassword = params2.confirmPassword;
    }
    securityQuestionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.registerService.securityQuestions().subscribe(function (data) {
            _this.dropdownData = data;
        });
        this.securityQuestionsForm = this._formBuilder.group({
            questionCode: ['', forms_1.Validators.required],
            answer: ['', forms_1.Validators.required],
            agreements: [false, forms_1.Validators.requiredTrue]
        });
    };
    securityQuestionsComponent.prototype.goBack = function () {
        this.location.back();
    };
    securityQuestionsComponent.prototype.register = function () {
        var _this = this;
        if (this.securityQuestionsForm.invalid) {
            return;
        }
        this.securityQuestionsForm.disable();
        this.showAlert = false;
        var formregister = {
            nationalityCode: '',
            fullName: this.fullName,
            email: this.email,
            identificationTypeCode: '',
            identificationNumber: '',
            phoneNumber: this.phoneNumber,
            password: this.password,
            confirmPassword: this.confirmPassword,
            questionCode: this.securityQuestionsForm.value.questionCode,
            answer: this.securityQuestionsForm.value.answer
        };
        console.log("FORM ", formregister);
        // Register
        this.registerService.register(formregister)
            .pipe(rxjs_1.finalize(function () {
            _this.securityQuestionsForm.enable();
            _this.securityQuestionsForm.reset();
            _this.showAlert = true;
            // Hide the alert after 3 seconds
            setTimeout(function () {
                _this.showAlert = false;
            }, 3000);
        }))
            .subscribe(function (response) {
            _this.alert = {
                type: 'success',
                message: 'Registration completed successfully'
            };
            var userID = response.userID;
            console.log(userID);
            _this._router.navigate(['/confirmation-required'], { queryParams: { email: _this.email } });
            _this.registerService.verifyEmail(userID)
                .subscribe(function (response) {
                _this.alert = {
                    type: 'success',
                    message: response.response.message
                };
                console.log(_this.message);
                _this._router.navigate(['/confirmation-required'], { queryParams: { email: _this.email } });
            }, function (error) {
                _this.alert = {
                    type: 'error',
                    message: error.error.message
                };
            });
        }, function (error) {
            if (error.status == 400) {
                _this.alert = {
                    type: 'error',
                    message: error.error.message
                };
            }
            else {
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
            }
        });
    };
    __decorate([
        core_1.ViewChild('securityQuestionsNgForm')
    ], securityQuestionsComponent.prototype, "securityQuestionsNgForm");
    securityQuestionsComponent = __decorate([
        core_1.Component({
            selector: 'security-questions',
            templateUrl: './security-questions.component.html',
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], securityQuestionsComponent);
    return securityQuestionsComponent;
}());
exports.securityQuestionsComponent = securityQuestionsComponent;
