import { Route } from '@angular/router';
import { OtpPhoneComponent } from 'app/modules/auth/register/otp-phone/otp-phone.component';

export const OtpPhoneRoutes: Route[] = [
    {
        path     : '',
        component: OtpPhoneComponent
    }
];
