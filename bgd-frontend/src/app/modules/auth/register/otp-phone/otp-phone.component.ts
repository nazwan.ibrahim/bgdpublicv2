import { HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { environment } from 'environments/environment';
import { NgOtpInputComponent } from 'ng-otp-input';
import { finalize } from 'rxjs';
import { registerService } from '../register.service';
import { AuthSharedService } from '../../auth-shared.service';

@Component({
    selector: 'otp-phone',
    templateUrl: './otp-phone.component.html',
    styleUrls: ['./otp-phone.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})

export class OtpPhoneComponent {

    @ViewChild(NgOtpInputComponent, { static: false }) ngOtpInput: NgOtpInputComponent;

    showOtpComponent = true;
    showAlert = false;
    alert: any;
    otp: any = '';
    phoneNumber: any = '';
    serviceSid: any = '';
    data: any = '';
    otpAttempts: number = 0;
    fullName: any = '';
    display: any;
    resendOtp: boolean = false;
    displayTimer: boolean = false;
    timer: any;

    config = {
        allowNumbersOnly: true,
        length: 6,
        isPasswordInput: false,
        disableAutoFocus: false
    };

    /**
     * Constructor
     */
    constructor(private _formBuilder: FormBuilder,
        private _authService: AuthService,
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private registerService: registerService,
        private authSharedService: AuthSharedService,
    ) {}
    // {
    //     this.start(2);

    //     this._activatedRoute.paramMap.subscribe(params => {
    //         this.fullName = params.get('fullName')
    //     })

    //     this.fullName = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('fullName'))

    //     this._activatedRoute.paramMap.subscribe(params => {
    //         this.phoneNumber = params.get('phoneNumber')
    //     })

    //     this.phoneNumber = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('phoneNumber'))

    //     this._activatedRoute.paramMap.subscribe(params => {
    //         this.serviceSid = params.get('serviceSid')
    //     })

    //     this.serviceSid = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('serviceSid'))
    // }


    /**
     * On init
     */

    ngOnInit(){
        this.start(2);
        this.fullName = this.authSharedService.fullName;
        console.log(this.fullName);
        this.phoneNumber = this.authSharedService.phoneNumber;
        console.log(this.phoneNumber);
        this.serviceSid = this.authSharedService.serviceSid;
        console.log(this.serviceSid);
    }

    hideDigits(displayPhoneNumber: string): string {
        const visibleFrontDigits = displayPhoneNumber.slice(0, 5);
        const hiddenDigits = displayPhoneNumber.slice(5, -2).replace(/\d/g, '*');
        const visibleBackDigits = displayPhoneNumber.slice(-2);
        return visibleFrontDigits + hiddenDigits + visibleBackDigits;
    }

    onOtpChange(otp: any) {
        this.otp = otp;
    }

    setVal(val: any) {
        this.ngOtpInput.setValue(val);
    }

    onConfigChange() {
        this.showOtpComponent = false;
        this.otp = null;
        setTimeout(() => {
            this.showOtpComponent = true;
        }, 0);
    }

    start(minute) {
        this.displayTimer = true;
        this.resendOtp = false;
        // let minute = 1;
        let seconds = minute * 60;
        let textSec: any = '0';
        let statSec = 60;

        const prefix = minute < 2 ? '0' : '';

        const timer = setInterval(() => {
            seconds--;
            if (statSec != 0) statSec--;
            else statSec = 59;

            // if (statSec < 10) textSec = "0" + statSec;
            // textSec = statSec;

            if (statSec < 2) {
                console.log('inside', statSec);
                textSec = '0' + statSec;
            } else {
                console.log('else', statSec);
                textSec = statSec;
            }

            // this.display = prefix + Math.floor(seconds / 60) + ":" + textSec;
            this.display = `${prefix}${Math.floor(seconds / 60)}:${textSec.toString().padStart(2, '0')}`;

            if (seconds == 0) {
                console.log('finished');
                clearInterval(timer);
                this.resendOtp = true;
                this.displayTimer = false;
            }
        }, 1000);
    }

    verifyOtp() {
        this.fullName = this.authSharedService.fullName;
        console.log(this.fullName);
        this.phoneNumber = this.authSharedService.phoneNumber;
        console.log(this.phoneNumber);
        this.serviceSid = this.authSharedService.serviceSid;
        console.log(this.serviceSid);

        // Return if the form is invalid
        if (this.ngOtpInput.otpForm.invalid) {
            return;
        }

        // Disable the form
        this.ngOtpInput.otpForm.disable();

        // Hide the alert
        this.showAlert = false;

        const otp = {
            serviceSid: this.serviceSid,
            phoneNumber: this.phoneNumber,
            otp: this.otp
        }

        console.log(otp);

        this._authService.verifyOtp(otp)
            .pipe(
                finalize(() => {

                    // Re-enable the form
                    this.ngOtpInput.otpForm.enable();

                    // Reset the form
                    this.ngOtpInput.otpForm.reset;

                    // Show the alert
                    this.showAlert = true;
                })
            )
            .subscribe(
                (response) => {

                    // Set the alert
                    this.alert = {
                        type: 'success',
                        message: 'Verified'
                    };

                    const check = response.valid;

                    if (check == true) {
                        this.authSharedService.params = {
                            serviceSid: this.serviceSid,
                            phoneNumber: this.phoneNumber,
                            otp: this.otp,
                            fullName: this.fullName
                        }

                        console.log(this.authSharedService.params);

                        this.authSharedService.message = this.alert.message;

                        this._router.navigate(['/create-account']
                        // , { queryParams: { params: JSON.stringify(params), message: this.alert.message } }
                        );
                    } else {
                        this.alert = {
                            type: 'error',
                            message: 'Invalid OTP number'
                        };
                    }

                },
                (response) => {

                    // Set the alert
                    this.alert = {
                        type: 'error',
                        message: 'Something went wrong, please try again.'
                    };
                }
            )

    }

    sendNewOtp() {
        this.start(10);

        this.registerService.sendOtp(this.phoneNumber)
            .subscribe(
                (response) => {

                    this.alert = {
                        type: 'success',
                        message: 'OTP resent successfully'
                    };

                    this.authSharedService.serviceSid = response.service_sid;
                    this.authSharedService.fullName = this.fullName;
                    this.authSharedService.phoneNumber = this.phoneNumber;
                    console.log(this.authSharedService.serviceSid);

                    this._router.navigate(['/otp-phone']
                    // , { queryParams: { fullName: this.fullName, phoneNumber: this.phoneNumber, serviceSid: serviceSid } }
                    )
                },
                (response) => {

                    this.alert = {
                        type: 'error',
                        message: 'Something went wrong, please try again.'
                    };

                    this.showAlert = true;
                }
            )
    }

}
