"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AuthRegisterComponent = void 0;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var animations_1 = require("@bgd/animations");
var AuthRegisterComponent = /** @class */ (function () {
    function AuthRegisterComponent(_activatedRoute, _router, _authService, _formBuilder, registerService) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._authService = _authService;
        this._formBuilder = _formBuilder;
        this.registerService = registerService;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
    }
    AuthRegisterComponent.prototype.ngOnInit = function () {
        this.AuthRegisterForm = this._formBuilder.group({
            fullName: ['', forms_1.Validators.required],
            phoneNumber: ['', [forms_1.Validators.required, forms_1.Validators.pattern(/^\+(?:[0-9] ?){6,14}[0-9]$/)]]
        });
    };
    AuthRegisterComponent.prototype.sendOtp = function () {
        var _this = this;
        if (this.AuthRegisterForm.invalid) {
            return;
        }
        this.AuthRegisterForm.disable();
        // Hide the alert
        this.showAlert = false;
        this.registerService.sendOtp(this.AuthRegisterForm.value.phoneNumber)
            .pipe(rxjs_1.finalize(function () {
            // Re-enable the form
            _this.AuthRegisterForm.enable();
            // Reset the form
            _this.AuthRegisterNgForm.resetForm();
            // Show the alert
            _this.showAlert = true;
        }))
            .subscribe(function (response) {
            _this.alert = {
                type: 'success',
                message: 'OTP sent successfully'
            };
            var fullName = _this.AuthRegisterForm.get('fullName').value;
            console.log(fullName);
            var phoneNumber = _this.AuthRegisterForm.get('phoneNumber').value;
            console.log(phoneNumber);
            var serviceSid = response.service_sid;
            console.log(serviceSid);
            _this._router.navigate(['/otp-phone'], { queryParams: { fullName: fullName, phoneNumber: phoneNumber, serviceSid: serviceSid } });
        }, function (response) {
            // Set the alert
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
            // Show the alert
            _this.showAlert = true;
        });
    };
    __decorate([
        core_1.ViewChild('AuthRegisterNgForm')
    ], AuthRegisterComponent.prototype, "AuthRegisterNgForm");
    AuthRegisterComponent = __decorate([
        core_1.Component({
            selector: 'register',
            templateUrl: 'register.component.html',
            styleUrls: ['./register.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], AuthRegisterComponent);
    return AuthRegisterComponent;
}());
exports.AuthRegisterComponent = AuthRegisterComponent;
