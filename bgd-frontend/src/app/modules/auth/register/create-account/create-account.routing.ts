import { Route } from '@angular/router';
import { CreateAccountComponent } from './create-account.component';

export const CreateAccountRoutes: Route[] = [
    {
        path     : '',
        component: CreateAccountComponent
    }
];
