import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators, FormControl, ValidationErrors } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';
import { FuseValidators } from '@bgd/validators';
import { AuthSharedService } from '../../auth-shared.service';

@Component({
    selector: 'create-account',
    templateUrl: './create-account.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CreateAccountComponent implements OnInit {
    @ViewChild('CreateAccountNgForm') CreateAccountNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };

    CreateAccountForm: UntypedFormGroup;
    showAlert = false;
    hasCommonWord = false;
    params: any = '';
    message: string = '';

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private location: Location,
        private authSharedService: AuthSharedService,
    ) {
        if (this.message) {
            this.alert = {
                type: 'success',
                message: this.message
            };

            // Show the alert
            this.showAlert = true;

            // Hide the alert after 3 seconds
            setTimeout(() => {
                this.showAlert = false;
            }, 3000);
        }
    }

    ngOnInit(): void {
        this.CreateAccountForm = this._formBuilder.group({
            email: ['', [
                Validators.required,
                Validators.email]],
            password: ['', [
                Validators.required,
                Validators.minLength(8),
                Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()\\[\\]\\-_+={}:";\'\',<.>/?]).*$')],
                this.checkPassword.bind(this)],
            confirmPassword: ['', Validators.required],
        },
            {
                validators: FuseValidators.mustMatch('password', 'confirmPassword')
            }
        );
    }

    clearAutofill() {
        setTimeout(() => {
            const emailField = document.getElementById('email') as HTMLInputElement;
            const passwordField = document.getElementById('password') as HTMLInputElement;
            const comfirmPasswordField = document.getElementById('comfirmPassword') as HTMLInputElement;
            emailField.removeAttribute('readonly');
            passwordField.removeAttribute('readonly');
            comfirmPasswordField.removeAttribute('readonly');
        }, 100);
    }

    get password() {
        return this.CreateAccountForm.get('password');
    }

    get passwordLength() {
        return this.CreateAccountForm.get('password')?.value?.length;
    }

    get hasLowerCase() {
        return /[a-z]/.test(this.CreateAccountForm.get('password')?.value);
    }

    get hasUpperCase() {
        return /[A-Z]/.test(this.CreateAccountForm.get('password')?.value);
    }

    get hasNumber() {
        return /[0-9]/.test(this.CreateAccountForm.get('password')?.value);
    }

    get hasSpecialChar() {
        return /[!@#$%^&*()\[\]\-_+={}:";',<.>/?]/.test(this.CreateAccountForm.get('password')?.value);
    }

    get hasMatchPassword() {
        const password = this.CreateAccountForm.get('password')?.value;
        const confirmPassword = this.CreateAccountForm.get('confirmPassword')?.value;

        if (password === null || confirmPassword === null || confirmPassword.trim() === '') {
            return false;
        }

        if (password !== null && confirmPassword !== null) {
            if (password === confirmPassword) {
                return true;
            }
        }

        return false;
    }

    async checkPassword(control: FormControl): Promise<ValidationErrors | null> {
        const newPassword = control.value;
        const password = { password: newPassword };

        try {
            const response = await this._authService.ValidatePassword(password).toPromise();

            const validateMessage = JSON.parse(response);


            if (validateMessage.message === "validated") {
                this.hasCommonWord = true;
            } else {
                this.hasCommonWord = false;
            }

            return null;
        } catch (error) {

            return { validationError: true };
        }
    }

    goBack(): void {
        this.location.back();
    }

    nextButton() {
        if (this.CreateAccountForm.invalid) {
            return;
        }

        this.CreateAccountForm.disable();

        console.log(this.params);

            this.authSharedService.registerEmail = this.CreateAccountForm.get('email').value,
            this.authSharedService.registerPassword = this.CreateAccountForm.get('password').value,
            this.authSharedService.registerConfirmPassword = this.CreateAccountForm.get('confirmPassword').value


        const email = this.CreateAccountForm.get('email').value;

        this.CreateAccountForm.enable();

        // Reset the form
        this.CreateAccountForm.reset();

        this._router.navigate(['/security-questions']);
    }
}
