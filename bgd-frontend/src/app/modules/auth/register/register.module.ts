import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { authRegisterRoutes } from 'app/modules/auth/register/register.routing';
import { MatRadioModule } from '@angular/material/radio';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { AuthRegisterComponent } from './register.component';
import { NgxMatIntlTelInputComponent } from 'ngx-mat-intl-tel-input';

@NgModule({
    declarations: [
        AuthRegisterComponent,
    ],
    imports: [
        RouterModule.forChild(authRegisterRoutes),
        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatRadioModule,
        MatStepperModule,
        MatSelectModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule,
        MatDividerModule,
        MatListModule,
        NgxMatIntlTelInputComponent
    ]
})
export class AuthRegisterModule {
}
