import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root',
})
export class registerService {
    constructor(private _httpClient: HttpClient) { }

    //Read data
    identificationType(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/identificationType');
    }

    country(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/country');
    }

    securityQuestions(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/securityQuestions');
    }

    //Save data
    // register(user : { fullName : string , nationality : string , identificationNumber : string , identificationTypeName : string , email : string , password : string , confirmPassword: string}): Observable<any> 
    register(user: { nationalityCode: string, fullName: string, email: string, identificationTypeCode: string, identificationNumber: string, phoneNumber: string, password: string, confirmPassword: string, questionCode: string, answer: string }): Observable<any> {
        console.log("DATA", user);
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/registerIndividual', user);
    }

    createSecurityQuestions(quest: { userid: string, questionCode: string, answer: string }): Observable<any> {

        return this._httpClient.post(environment.apiUrl + '/api/onboarding/createSecurityQuestions', quest);

    }


    sendOtp(phoneNumber: string): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/generateOtp', { phoneNumber });
    }

    verifyEmail(userID: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        const url = `/api/onboarding/verifyEmail?userID=${encodeURIComponent(userID)}`;
        return this._httpClient.post(environment.apiUrl + url, { responseType: 'text' });
    }

}