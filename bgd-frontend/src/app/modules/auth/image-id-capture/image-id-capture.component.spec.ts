import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageIdCaptureComponent } from './image-id-capture.component';

describe('ImageIdCaptureComponent', () => {
  let component: ImageIdCaptureComponent;
  let fixture: ComponentFixture<ImageIdCaptureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageIdCaptureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImageIdCaptureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
