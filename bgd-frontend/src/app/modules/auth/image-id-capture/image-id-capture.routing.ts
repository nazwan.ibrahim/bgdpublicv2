import { Route} from '@angular/router';
import { ImageIdCaptureComponent } from 'app/modules/auth/image-id-capture/image-id-capture.component';

export const ImageIdCaptureRoutes: Route[] = [
  {
      path     : '',
      component: ImageIdCaptureComponent
  }
];

