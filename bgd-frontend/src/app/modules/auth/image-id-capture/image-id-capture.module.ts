import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ImageIdCaptureRoutes } from 'app/modules/auth/image-id-capture/image-id-capture.routing';
import { CommonModule } from '@angular/common';
import { ImageIdCaptureComponent } from './image-id-capture.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        ImageIdCaptureComponent,
 
    ],
    imports     : [
        RouterModule.forChild(ImageIdCaptureRoutes),
        CommonModule,
        MatButtonModule,
        MatIconModule,
    ],
    

})
export class ImageIdCaptureModule
{
}
