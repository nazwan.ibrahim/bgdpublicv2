"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ForgotPasswordQuestionComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var forms_1 = require("@angular/forms");
var rxjs_1 = require("rxjs");
var ForgotPasswordQuestionComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    function ForgotPasswordQuestionComponent(_activatedRoute, _router, _authService, _formBuilder) {
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this._authService = _authService;
        this._formBuilder = _formBuilder;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.showAlert = false;
        this.email = '';
        this.userSecurityQuestionDesc = '';
        this.userSecurityQuestionCode = '';
        this.disableButton = false;
        this.email = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('email'));
        this.userSecurityQuestionDesc = this._activatedRoute.snapshot.queryParamMap.get('que');
        this.userSecurityQuestionCode = this._activatedRoute.snapshot.queryParamMap.get('code');
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ForgotPasswordQuestionComponent.prototype.ngOnInit = function () {
        // Create the form
        this.forgotPasswordQueForm = this._formBuilder.group({
            email: this.email,
            questionCode: [this.userSecurityQuestionCode, [forms_1.Validators.required]],
            answer: ['', [forms_1.Validators.required]]
        });
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Send the reset link
     */
    ForgotPasswordQuestionComponent.prototype.sendResetLink = function () {
        var _this = this;
        this.disableButton = true;
        // Return if the form is invalid
        if (this.forgotPasswordQueForm.invalid) {
            return;
        }
        // Disable the form
        this.forgotPasswordQueForm.disable();
        // Hide the alert
        this.showAlert = false;
        // Forgot password
        console.log(this.forgotPasswordQueForm.value);
        this._authService.forgotPassword(this.forgotPasswordQueForm.value)
            .pipe(rxjs_1.finalize(function () {
            // Re-enable the form
            _this.forgotPasswordQueForm.enable();
            // Reset the form
            _this.forgotPasswordQueNgForm.resetForm();
            // Show the alert
            _this.showAlert = true;
            // Hide the alert after 3 seconds
            setTimeout(function () {
                _this.showAlert = false;
            }, 3000);
        }))
            .subscribe(function (response) {
            // Set the alert
            _this.alert = {
                type: 'success',
                message: 'Password reset sent! You\'ll receive an email if you are registered on our system.'
            };
            _this._router.navigate(['/forgot-password-email'], { queryParams: { data: encodeURIComponent(_this.email) } });
        }, function (error) {
            _this.disableButton = false;
            if (error.status == 401) {
                // Set the alert
                _this.alert = {
                    type: 'error',
                    message: 'Wrong email or answer to the security question.'
                };
                setTimeout(function () {
                    _this.showAlert = false;
                    location.reload();
                }, 1000);
            }
            else {
                _this.alert = {
                    type: 'error',
                    message: 'Something went wrong, please try again.'
                };
            }
        });
    };
    __decorate([
        core_1.ViewChild('forgotPasswordQueNgForm')
    ], ForgotPasswordQuestionComponent.prototype, "forgotPasswordQueNgForm");
    ForgotPasswordQuestionComponent = __decorate([
        core_1.Component({
            selector: 'bgd-forgot-password-question',
            templateUrl: './forgot-password-question.component.html',
            styleUrls: ['./forgot-password-question.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], ForgotPasswordQuestionComponent);
    return ForgotPasswordQuestionComponent;
}());
exports.ForgotPasswordQuestionComponent = ForgotPasswordQuestionComponent;
