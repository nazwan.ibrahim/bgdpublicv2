import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@bgd/animations';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthSharedService } from '../auth-shared.service';

@Component({
  selector: 'bgd-forgot-password-question',
  templateUrl: './forgot-password-question.component.html',
  styleUrls: ['./forgot-password-question.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ForgotPasswordQuestionComponent implements OnInit {

  @ViewChild('forgotPasswordQueNgForm') forgotPasswordQueNgForm: NgForm;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };
  forgotPasswordQueForm: UntypedFormGroup;
  showAlert = false;
  email: any = '';
  userSecurityQuestionDesc: any = '';
  userSecurityQuestionCode: any = '';
  disableButton: boolean = false;

  /**
   * Constructor
   */
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _authService: AuthService,
    private _formBuilder: UntypedFormBuilder,
    private authSharedService: AuthSharedService,
  ) {}
  // {
  //   this.email = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('email'))
  //   this.userSecurityQuestionDesc = this._activatedRoute.snapshot.queryParamMap.get('que')
  //   this.userSecurityQuestionCode = this._activatedRoute.snapshot.queryParamMap.get('code')

  // }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {

    this.email = this.authSharedService.email;
    this.userSecurityQuestionDesc = this.authSharedService.userSecurityQuestionDesc;
    this.userSecurityQuestionCode = this.authSharedService.userSecurityQuestionCode;

    // Create the form
    this.forgotPasswordQueForm = this._formBuilder.group({
      email: this.email,
      questionCode: [this.userSecurityQuestionCode, [Validators.required]],
      answer: ['', [Validators.required]]

    });
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Send the reset link
   */
  sendResetLink(): void {
    this.disableButton = true;

    // Return if the form is invalid
    if (this.forgotPasswordQueForm.invalid) {
      return;
    }

    // Disable the form
    this.forgotPasswordQueForm.disable();

    // Hide the alert
    this.showAlert = false;

    // Forgot password
    console.log(this.forgotPasswordQueForm.value);

    this._authService.forgotPassword(this.forgotPasswordQueForm.value)
      .pipe(
        finalize(() => {
          // Re-enable the form
          this.forgotPasswordQueForm.enable();

          // Show the alert
          this.showAlert = true;

          // Hide the alert after 3 seconds
          setTimeout(() => {
            this.showAlert = false;
          }, 1000);
        })
      )
      .subscribe(
        (response) => {
          // Set the alert
          this.alert = {
            type: 'success',
            message: 'Password reset sent! You\'ll receive an email if you are registered on our system.'
          };

          // Reset the form
          this.forgotPasswordQueNgForm.resetForm();

          this.authSharedService.data = this.email
          this._router.navigate(['/forgot-password-email']
          // , { queryParams: { data: encodeURIComponent(this.email) } }
          );
        },
        (error) => {
          this.disableButton = false;

          if (error.status == 500) {
            // Set the alert
            this.alert = {
              type: 'error',
              message: 'Something went wrong, please try again.'
            };

          } else {

            this.alert = {
              type: 'error',
              message: 'Wrong email or answer to the security question.'
            };

          }
          // Re-enable the form
          this.forgotPasswordQueForm.enable();
        }
      );
  }


}
