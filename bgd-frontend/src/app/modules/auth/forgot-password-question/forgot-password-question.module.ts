import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { ForgotPasswordQuestionComponent } from 'app/modules/auth/forgot-password-question/forgot-password-question.component';
import { ForgotPasswordQuestionRoutes } from 'app/modules/auth/forgot-password-question/forgot-password-question.routing';

@NgModule({
    declarations: [
        ForgotPasswordQuestionComponent
    ],
    imports     : [
        RouterModule.forChild(ForgotPasswordQuestionRoutes),
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatProgressSpinnerModule,
        FuseCardModule,
        FuseAlertModule,
        SharedModule
    ]
})
export class ForgotPasswordQuestionModule
{
}
