import { Route } from '@angular/router';
import { ForgotPasswordQuestionComponent } from 'app/modules/auth/forgot-password-question/forgot-password-question.component';

export const ForgotPasswordQuestionRoutes: Route[] = [
    {
        path     : '',
        component: ForgotPasswordQuestionComponent
    }
];
