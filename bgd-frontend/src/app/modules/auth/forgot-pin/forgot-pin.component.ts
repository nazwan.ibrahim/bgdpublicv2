import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { registerService } from '../register/register.service';

@Component({
    selector: 'bgd-forgot-pin',
    templateUrl: './forgot-pin.component.html',
    styleUrls: ['./forgot-pin.component.scss']
})
export class ForgotPinComponent {

    showAlert = false;
    username: any;
    email: any;
    pin: any;
    phoneNumber: any;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: '',
    };

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router,
        private AuthService: AuthService,
        private _formBuilder: FormBuilder,
        private user: DashboardService,
        private _bgdService: bgdService,
        private registerService: registerService
    ) { }

    ngOnInit(): void {
        this.user.getUserDetails().subscribe((data) => {
            this.email = data.email;
            this.username = data.userName;
            this.phoneNumber = data.phoneNumber;

            console.log(this.phoneNumber);
        });
    }

    sendOtp(): void {
        this.registerService.sendOtp(this.phoneNumber)
            .subscribe(
                (response) => {

                    this.alert = {
                        type: 'success',
                        message: 'OTP sent successfully'
                    };

                    const serviceSid = response.service_sid;

                    this._router.navigate(['/forgot-pin-otp'], { queryParams: { username: this.username, phoneNumber: this.phoneNumber, serviceSid: serviceSid } })
                },
                (response) => {

                    // Set the alert
                    this.alert = {
                        type: 'error',
                        message: 'Something went wrong, please try again.'
                    };

                    // Show the alert
                    this.showAlert = true;
                }
            )
    }

}
