import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotPinNewComponent } from './forgot-pin-new.component';

describe('ForgotPinNewComponent', () => {
  let component: ForgotPinNewComponent;
  let fixture: ComponentFixture<ForgotPinNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgotPinNewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForgotPinNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
