import { Route } from '@angular/router';
import { ForgotPinNewComponent } from './forgot-pin-new.component';

export const ForgotPinNewRoutes: Route[] = [
    {
        path: '',
        component: ForgotPinNewComponent
    }
];
