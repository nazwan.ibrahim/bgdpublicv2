"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ForgotPinOtpComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var ng_otp_input_1 = require("ng-otp-input");
var rxjs_1 = require("rxjs");
var ForgotPinOtpComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    function ForgotPinOtpComponent(_formBuilder, _authService, _activatedRoute, _router, registerService) {
        var _this = this;
        this._formBuilder = _formBuilder;
        this._authService = _authService;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.registerService = registerService;
        this.showOtpComponent = true;
        this.showAlert = false;
        this.otp = '';
        this.phoneNumber = '';
        this.serviceSid = '';
        this.data = '';
        this.otpAttempts = 0;
        this.fullName = '';
        this.resendOtp = false;
        this.displayTimer = false;
        this.disableButton = false;
        this.config = {
            allowNumbersOnly: true,
            length: 6,
            isPasswordInput: false,
            disableAutoFocus: false
        };
        this.start(10);
        this._activatedRoute.paramMap.subscribe(function (params) {
            _this.fullName = params.get('fullName');
        });
        this.fullName = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('fullName'));
        this._activatedRoute.paramMap.subscribe(function (params) {
            _this.phoneNumber = params.get('phoneNumber');
        });
        this.phoneNumber = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('phoneNumber'));
        this._activatedRoute.paramMap.subscribe(function (params) {
            _this.serviceSid = params.get('serviceSid');
        });
        this.serviceSid = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('serviceSid'));
    }
    /**
     * On init
     */
    ForgotPinOtpComponent.prototype.onOtpChange = function (otp) {
        this.otp = otp;
    };
    ForgotPinOtpComponent.prototype.setVal = function (val) {
        this.ngOtpInput.setValue(val);
    };
    ForgotPinOtpComponent.prototype.onConfigChange = function () {
        var _this = this;
        this.showOtpComponent = false;
        this.otp = null;
        setTimeout(function () {
            _this.showOtpComponent = true;
        }, 0);
    };
    ForgotPinOtpComponent.prototype.start = function (minute) {
        var _this = this;
        this.displayTimer = true;
        this.resendOtp = false;
        // let minute = 1;
        var seconds = minute * 60;
        var textSec = '0';
        var statSec = 60;
        var prefix = minute < 10 ? '0' : '';
        var timer = setInterval(function () {
            seconds--;
            if (statSec != 0)
                statSec--;
            else
                statSec = 59;
            // if (statSec < 10) textSec = "0" + statSec;
            // textSec = statSec;
            if (statSec < 10) {
                console.log('inside', statSec);
                textSec = '0' + statSec;
            }
            else {
                console.log('else', statSec);
                textSec = statSec;
            }
            // this.display = prefix + Math.floor(seconds / 60) + ":" + textSec;
            _this.display = "" + prefix + Math.floor(seconds / 60) + ":" + textSec;
            if (seconds == 0) {
                console.log('finished');
                clearInterval(timer);
                _this.resendOtp = true;
                _this.displayTimer = false;
            }
        }, 1000);
    };
    ForgotPinOtpComponent.prototype.verifyOtp = function () {
        var _this = this;
        this.disableButton = true;
        // Return if the form is invalid
        if (this.ngOtpInput.otpForm.invalid) {
            return;
        }
        // Disable the form
        this.ngOtpInput.otpForm.disable();
        // Hide the alert
        this.showAlert = false;
        var otp = {
            serviceSid: this.serviceSid,
            phoneNumber: this.phoneNumber,
            otp: this.otp
        };
        console.log(otp);
        this._authService.verifyOtp(otp)
            .pipe(rxjs_1.finalize(function () {
            // Re-enable the form
            _this.ngOtpInput.otpForm.enable();
            // Reset the form
            _this.ngOtpInput.otpForm.reset;
            // Show the alert
            _this.showAlert = true;
        }))
            .subscribe(function (response) {
            // Set the alert
            _this.alert = {
                type: 'success',
                message: 'Verified'
            };
            var check = response.valid;
            if (check == true) {
                var params = {
                    serviceSid: _this.serviceSid,
                    phoneNumber: _this.phoneNumber,
                    otp: _this.otp,
                    fullName: _this.fullName
                };
                console.log(params);
                _this._router.navigate(['/forgot-pin-new'], { queryParams: { params: JSON.stringify(params), message: _this.alert.message } });
            }
            else {
                _this.disableButton = false;
                _this.alert = {
                    type: 'error',
                    message: 'Invalid OTP number'
                };
            }
        }, function (response) {
            _this.disableButton = false;
            // Set the alert
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
        });
    };
    ForgotPinOtpComponent.prototype.sendNewOtp = function () {
        var _this = this;
        this.start(10);
        this.registerService.sendOtp(this.phoneNumber)
            .subscribe(function (response) {
            _this.alert = {
                type: 'success',
                message: 'OTP resent successfully'
            };
            var serviceSid = response.service_sid;
            console.log(serviceSid);
            _this._router.navigate(['/otp-phone'], { queryParams: { fullName: _this.fullName, phoneNumber: _this.phoneNumber, serviceSid: serviceSid } });
        }, function (response) {
            _this.alert = {
                type: 'error',
                message: 'Something went wrong, please try again.'
            };
            _this.showAlert = true;
        });
    };
    __decorate([
        core_1.ViewChild(ng_otp_input_1.NgOtpInputComponent, { static: false })
    ], ForgotPinOtpComponent.prototype, "ngOtpInput");
    ForgotPinOtpComponent = __decorate([
        core_1.Component({
            selector: 'bgd-forgot-pin-otp',
            templateUrl: './forgot-pin-otp.component.html',
            styleUrls: ['./forgot-pin-otp.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], ForgotPinOtpComponent);
    return ForgotPinOtpComponent;
}());
exports.ForgotPinOtpComponent = ForgotPinOtpComponent;
