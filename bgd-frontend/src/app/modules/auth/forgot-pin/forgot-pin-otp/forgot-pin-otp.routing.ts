import { Route } from '@angular/router';
import { ForgotPinOtpComponent } from './forgot-pin-otp.component';

export const ForgotPinOtpRoutes: Route[] = [
    {
        path: '',
        component: ForgotPinOtpComponent
    }
];
