import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import { MatRadioModule } from '@angular/material/radio';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { NgOtpInputModule } from 'ng-otp-input';
import { MatDialogModule } from '@angular/material/dialog';
import { ForgotPinOtpRoutes } from 'app/modules/auth/forgot-pin/forgot-pin-otp/forgot-pin-otp.routing';
import { ForgotPinOtpComponent } from './forgot-pin-otp.component';



@NgModule({
    declarations: [
        ForgotPinOtpComponent
    ],
    imports: [
        RouterModule.forChild(ForgotPinOtpRoutes),
        MatButtonModule,
        FuseCardModule,
        SharedModule,
        NgOtpInputModule,
        FuseAlertModule,
        MatFormFieldModule,
        MatDialogModule,
        MatIconModule,
    ]
})
export class ForgotPinOtpModule {
}
