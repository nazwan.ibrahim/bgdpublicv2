import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { SharedModule } from 'app/shared/shared.module';
import { NgxPincodeModule } from 'ngx-pincode';
import { ForgotPinOtpComponent } from './forgot-pin-otp/forgot-pin-otp.component';
import { ForgotPinNewComponent } from './forgot-pin-new/forgot-pin-new.component';
import { ForgotPinComponent } from 'app/modules/auth/forgot-pin/forgot-pin.component';
import { ForgotPinRoutes } from 'app/modules/auth/forgot-pin/forgot-pin.routing';
import { ForgotPinConfirmComponent } from './forgot-pin-confirm/forgot-pin-confirm.component';


@NgModule({
    declarations: [
        ForgotPinComponent,
    ],
    imports: [
        RouterModule.forChild(ForgotPinRoutes),
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        FuseCardModule,
        FuseAlertModule,
        MatChipsModule,
        MatIconModule,
        NgxPincodeModule,
    ]
})
export class ForgotPinModule {
}
