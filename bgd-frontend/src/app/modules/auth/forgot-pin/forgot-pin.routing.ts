import { Route } from '@angular/router';
import { ForgotPinComponent } from 'app/modules/auth/forgot-pin/forgot-pin.component';

export const ForgotPinRoutes: Route[] = [
    {
        path: '',
        component: ForgotPinComponent
    }
];
