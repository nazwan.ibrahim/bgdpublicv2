import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgotPinConfirmComponent } from './forgot-pin-confirm.component';

describe('ForgotPinConfirmComponent', () => {
  let component: ForgotPinConfirmComponent;
  let fixture: ComponentFixture<ForgotPinConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgotPinConfirmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForgotPinConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
