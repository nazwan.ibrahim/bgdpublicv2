import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
@Component({
  selector: 'bgd-forgot-pin-confirm',
  templateUrl: './forgot-pin-confirm.component.html',
  styleUrls: ['./forgot-pin-confirm.component.scss']
})
export class ForgotPinConfirmComponent {

  showAlert = false;
  username: any;
  email: any;
  pin: any;
  newPin: any;
  enteredPIN: any;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  constructor(

    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private AuthService: AuthService,
    private _formBuilder: FormBuilder,
    private user: DashboardService,
    private _bgdService: bgdService,

  ) {

    {
      this._activatedRoute.paramMap.subscribe(params => {
        this.newPin = params.get('oldPin');
      })
      this.newPin = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('userPin'));
    }

  }

  ngOnInit(): void {

    this.user.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    })

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  changePin() {
    if (this.newPin === this.pin) {
      const newPin = {
        newPin: this.newPin,
        confirmPin: this.pin,
      }

      this.AuthService.resetPin(newPin)
        .subscribe(
          response => {
            this.alert = {
              type: 'success',
              message: 'Verified'
            };
            this._router.navigate(['/dashboard'])
          },
          error => {
            this.alert = {
              type: 'error',
              message: 'Invalid'
            };
            this.showAlert = true;
          }
        );
    } else {
      this.alert = {
        type: 'error',
        message: 'PIN does not match.'
      };
      this.showAlert = true;
      this.pin = '';
    }

  }
}