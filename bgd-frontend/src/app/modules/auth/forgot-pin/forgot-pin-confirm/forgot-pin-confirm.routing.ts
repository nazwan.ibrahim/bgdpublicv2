import { Route } from '@angular/router';
import { ForgotPinConfirmComponent } from './forgot-pin-confirm.component';

export const ForgotPinConfirmRoutes: Route[] = [
    {
        path: '',
        component: ForgotPinConfirmComponent
    }
];
