import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { SharedModule } from 'app/shared/shared.module';
import { NgxPincodeModule } from 'ngx-pincode';
import { MatDialogModule } from '@angular/material/dialog';
import { NgOtpInputModule } from 'ng-otp-input';
import { ForgotPinConfirmRoutes } from 'app/modules/auth/forgot-pin/forgot-pin-confirm/forgot-pin-confirm.routing';
import { ForgotPinConfirmComponent } from './forgot-pin-confirm.component';



@NgModule({
    declarations: [
        ForgotPinConfirmComponent
    ],
    imports: [
        RouterModule.forChild(ForgotPinConfirmRoutes),
        MatButtonModule,
        FuseCardModule,
        SharedModule,
        NgOtpInputModule,
        FuseAlertModule,
        MatFormFieldModule,
        MatDialogModule,
        MatIconModule,
        MatButtonModule,
        CommonModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatSelectModule,
        MatChipsModule,
        NgxPincodeModule,
    ]
})
export class ForgotPinConfirmModule {
}
