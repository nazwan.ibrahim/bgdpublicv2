import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupLoginPreferenceComponent } from './setup-login-preference.component';

describe('SetupLoginPreferenceComponent', () => {
  let component: SetupLoginPreferenceComponent;
  let fixture: ComponentFixture<SetupLoginPreferenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupLoginPreferenceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SetupLoginPreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
