import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsPasswordComponent } from './details-password.component';

describe('DetailsPasswordComponent', () => {
  let component: DetailsPasswordComponent;
  let fixture: ComponentFixture<DetailsPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsPasswordComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailsPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
