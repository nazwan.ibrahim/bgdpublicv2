import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { FuseValidators } from '@bgd/validators';
import { bgdService } from 'app/modules/admin/bgd/bgd.service';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';

@Component({
  selector: 'bgd-setup-pin',
  templateUrl: './setup-pin.component.html',
  styleUrls: ['./setup-pin.component.scss']
})
export class SetupPinComponent {

  showAlert = false;
  username: any;
  email: any;
  pin: any;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  constructor(

    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private AuthService: AuthService,
    private _formBuilder: FormBuilder,
    private user: DashboardService,
    private _bgdService: bgdService,

  ) {

  }

  ngOnInit(): void {

    this.user.getUserDetails().subscribe(data => {
      this.email = data.email;
      this.username = data.userName;
    })

  }

  pincodeCompleted(pin: any) {
    this.pin = pin;
  }

  nextPin() {

    const pin = this.pin;

    console.log(pin);
    this._router.navigate(['/setup-pin-otp'], {
      queryParams: {
        userPin: encodeURIComponent(pin)
      }
    });

  }
}
