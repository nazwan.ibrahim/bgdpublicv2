import { Route } from '@angular/router';
import { SetupPinComponent } from 'app/modules/auth/setup-pin/setup-pin.component';

export const SetupPinRoutes: Route[] = [
    {
        path: '',
        component: SetupPinComponent
    }
];

