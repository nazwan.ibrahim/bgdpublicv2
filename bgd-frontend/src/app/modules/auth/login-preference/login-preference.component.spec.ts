import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPreferenceComponent } from './login-preference.component';

describe('LoginPreferenceComponent', () => {
  let component: LoginPreferenceComponent;
  let fixture: ComponentFixture<LoginPreferenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginPreferenceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginPreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
