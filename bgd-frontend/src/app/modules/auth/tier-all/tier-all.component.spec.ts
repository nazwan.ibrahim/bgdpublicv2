import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TierAllComponent } from './tier-all.component';

describe('TierAllComponent', () => {
  let component: TierAllComponent;
  let fixture: ComponentFixture<TierAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TierAllComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TierAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
