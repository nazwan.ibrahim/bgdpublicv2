import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { switchMap, tap, map, catchError, take } from 'rxjs/operators';
import { of, Subscription, interval } from 'rxjs';
import { CurrencyPipe } from '@angular/common';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';

@Component({
  selector: 'bgd-tier-all',
  templateUrl: './tier-all.component.html',
  styleUrls: ['./tier-all.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations,
  providers: [CurrencyPipe]
})
export class TierAllComponent {

  email: any;
  username: any;
  statusTier: any;
  pageCode: any = 'T01';
  transferLimit1: any;
  transferLimit2: any;
  transferLimit3: any;
  withdrawLimit1: any;
  withdrawLimit2: any;
  withdrawLimit3: any;
  depositLimit1: any;
  depositLimit2: any;
  depositLimit3: any;
  tierClicked: boolean = false;
  tier2Clicked: boolean = false;
  tier3Clicked: boolean = false;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private user: DashboardService,
    private walletService: WalletService
  ) {}

  ngOnInit(): void {

    this.statusTier = this.walletService.statusTier;

    this.user.getThreshold("T01").subscribe(data => {
      this.withdrawLimit1 = data.tierThresholdForm[0].maxValue;
      this.transferLimit1 = data.tierThresholdForm[2].maxValue
      this.depositLimit1 = data.tierThresholdForm[1].maxValue
    });

    this.user.getThreshold("T02").subscribe(data => {
      this.withdrawLimit2 = data.tierThresholdForm[0].maxValue;
      this.transferLimit2 = data.tierThresholdForm[1].maxValue
      this.depositLimit2 = data.tierThresholdForm[2].maxValue
    });

    this.user.getThreshold("T03").subscribe(data => {
      this.withdrawLimit3 = data.tierThresholdForm[0].maxValue;
      this.transferLimit3 = data.tierThresholdForm[2].maxValue
      this.depositLimit3 = data.tierThresholdForm[1].maxValue
    });

  }

  clickTier(tier: string) {
    if (tier === 'T01') {
      this.tierClicked = true;
      this.tier2Clicked = false;
      this.tier3Clicked = false;
      this.pageCode = tier;
    } else if (tier === 'T02') {
      this.tierClicked = false;
      this.tier2Clicked = true;
      this.tier3Clicked = false;
      this.pageCode = tier;
    } else if (tier === 'T03') {
      this.tierClicked = false;
      this.tier2Clicked = false;
      this.tier3Clicked = true;
      this.pageCode = tier;
    }
  }

  tierButton() {

    this._router.navigate(['/tier']);

  }



}
