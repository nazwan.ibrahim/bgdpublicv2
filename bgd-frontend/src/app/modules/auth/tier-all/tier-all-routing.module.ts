import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TierAllComponent } from 'app/modules/auth/tier-all/tier-all.component';

const routes: Routes = [
    {
        path: '',
        component: TierAllComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TierAllRoutingModule { }
