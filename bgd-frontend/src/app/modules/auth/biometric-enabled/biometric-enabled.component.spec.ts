import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BiometricEnabledComponent } from './biometric-enabled.component';

describe('BiometricEnabledComponent', () => {
  let component: BiometricEnabledComponent;
  let fixture: ComponentFixture<BiometricEnabledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BiometricEnabledComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BiometricEnabledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
