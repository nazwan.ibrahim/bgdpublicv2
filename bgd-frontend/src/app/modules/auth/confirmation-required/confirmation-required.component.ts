import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fuseAnimations } from '@bgd/animations';

@Component({
    selector     : 'auth-confirmation-required',
    templateUrl  : './confirmation-required.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthConfirmationRequiredComponent
{
    email: string;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _router: Router
    )
    {
        this._activatedRoute.paramMap.subscribe(params=>{
            this.email = params.get('email')})
    
        this.email = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('email'))
    }

    loginNow() {
        this._router.navigate(['sign-in']);
    }
}
