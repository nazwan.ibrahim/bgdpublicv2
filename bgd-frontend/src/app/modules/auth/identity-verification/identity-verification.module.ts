import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IdentityVerificationRoutes } from 'app/modules/auth/identity-verification/identity-verification.routing';
import { CommonModule } from '@angular/common';
import { IdentityVerificationComponent } from './identity-verification.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        IdentityVerificationComponent,
 
    ],
    imports     : [
        RouterModule.forChild(IdentityVerificationRoutes),
        CommonModule,
        MatButtonModule,
        MatIconModule,
    ],
    

})
export class IdentityVerificationModule
{
}
