import { Route} from '@angular/router';
import { IdentityVerificationComponent } from 'app/modules/auth/identity-verification/identity-verification.component';

export const IdentityVerificationRoutes: Route[] = [
  {
      path     : '',
      component: IdentityVerificationComponent
  }
];

