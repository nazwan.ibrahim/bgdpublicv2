import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, NgForm, Validators } from '@angular/forms';
import { finalize } from 'rxjs';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { switchMap, tap, map, catchError, take } from 'rxjs/operators';
import { of, Subscription, interval } from 'rxjs';
import { CurrencyPipe } from '@angular/common';
import { WalletService } from 'app/modules/cash-wallet/wallet.service';

@Component({
  selector: 'bgd-tier',
  templateUrl: './tier.component.html',
  styleUrls: ['./tier.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class TierComponent {

  email: any;
  username: any;
  statusTier: any;
  transferLimit1: any;
  transferLimit2: any;
  transferLimit3: any;
  withdrawLimit1: any;
  withdrawLimit2: any;
  withdrawLimit3: any;
  depositLimit1: any;
  depositLimit2: any;
  depositLimit3: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private user: DashboardService,
    private walletService: WalletService
  ) {}

  ngOnInit(): void {
    this.statusTier = this.walletService.statusTier;

    this.user.getThreshold("T01").subscribe(data => {
      this.withdrawLimit1 = data.tierThresholdForm[0].maxValue;
      this.transferLimit1 = data.tierThresholdForm[2].maxValue
      this.depositLimit1 = data.tierThresholdForm[1].maxValue
    });

    this.user.getThreshold("T02").subscribe(data => {
      this.withdrawLimit2 = data.tierThresholdForm[0].maxValue;
      this.transferLimit2 = data.tierThresholdForm[1].maxValue
      this.depositLimit2 = data.tierThresholdForm[2].maxValue
    });

    this.user.getThreshold("T03").subscribe(data => {
      this.withdrawLimit3 = data.tierThresholdForm[0].maxValue;
      this.transferLimit3 = data.tierThresholdForm[1].maxValue
      this.depositLimit3 = data.tierThresholdForm[2].maxValue
    });
  }


tierAllButton() {

  this._router.navigate(['/tier-all']);

}


}
