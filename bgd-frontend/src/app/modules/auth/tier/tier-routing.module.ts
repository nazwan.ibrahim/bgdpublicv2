import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TierComponent } from 'app/modules/auth/tier/tier.component';

const routes: Routes = [
    {
        path: '',
        component: TierComponent
        // resolve  : {
        //   data: BuyBgdResolver
        // }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TierRoutingModule { }
