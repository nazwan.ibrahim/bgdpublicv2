import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { TierComponent } from 'app/modules/auth/tier/tier.component';
import { TierRoutingModule } from 'app/modules/auth/tier/tier-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        TierComponent,

    ],
    imports: [
        MatIconModule,
        TierRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatChipsModule,
    ]
})
export class TierModule {
}