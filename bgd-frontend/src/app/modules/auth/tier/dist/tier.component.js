"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TierComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var TierComponent = /** @class */ (function () {
    function TierComponent(_activatedRoute, _router, user) {
        var _this = this;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.user = user;
        {
            this._activatedRoute.paramMap.subscribe(function (params) {
                _this.statusTier = params.get('statusTier');
            });
            this.statusTier = decodeURIComponent(this._activatedRoute.snapshot.queryParamMap.get('statusTier'));
        }
    }
    TierComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user.getThreshold("T01").subscribe(function (data) {
            _this.withdrawLimit1 = data.tierThresholdForm[0].maxValue;
            _this.transferLimit1 = data.tierThresholdForm[1].maxValue;
            _this.depositLimit1 = data.tierThresholdForm[2].maxValue;
        });
        this.user.getThreshold("T02").subscribe(function (data) {
            _this.withdrawLimit2 = data.tierThresholdForm[0].maxValue;
            _this.transferLimit2 = data.tierThresholdForm[1].maxValue;
            _this.depositLimit2 = data.tierThresholdForm[2].maxValue;
        });
        this.user.getThreshold("T03").subscribe(function (data) {
            _this.withdrawLimit3 = data.tierThresholdForm[0].maxValue;
            _this.transferLimit3 = data.tierThresholdForm[1].maxValue;
            _this.depositLimit3 = data.tierThresholdForm[2].maxValue;
        });
    };
    TierComponent.prototype.tierAllButton = function () {
        var statusTier = this.statusTier;
        this._router.navigate(['/tier-all'], {
            queryParams: {
                statusTier: encodeURIComponent(statusTier)
            }
        });
    };
    TierComponent = __decorate([
        core_1.Component({
            selector: 'bgd-tier',
            templateUrl: './tier.component.html',
            styleUrls: ['./tier.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], TierComponent);
    return TierComponent;
}());
exports.TierComponent = TierComponent;
