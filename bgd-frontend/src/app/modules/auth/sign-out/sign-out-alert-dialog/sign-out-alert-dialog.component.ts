import { Component, Inject, HostListener } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'app/core/auth/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

export interface DialogData {
  price: any;
}

@Component({
  selector: 'sign-out-alert-dialog',
  templateUrl: 'sign-out-alert-dialog.component.html',
})
export class SignOutAlertDialog {
  constructor(
    private dialogRef: MatDialogRef<SignOutAlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthService,
    private router: Router,
    private location: Location
  ) { }

  onProceed(): void {
    this.dialogRef.close(true);
  }

  onCancel(): void {
    this.dialogRef.close(false);
    this.location.back();
  }

  @HostListener('document:click', ['$event.target'])
  onClickOutside(targetElement: HTMLElement): void {
    if (!this.dialogRef.disableClose && targetElement.classList.contains('cdk-overlay-backdrop')) {
      this.dialogRef.close(false);
      this.location.back();
    }
  }
}
