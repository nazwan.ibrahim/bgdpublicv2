"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.SignOutAlertDialog = void 0;
var core_1 = require("@angular/core");
var dialog_1 = require("@angular/material/dialog");
var SignOutAlertDialog = /** @class */ (function () {
    function SignOutAlertDialog(dialogRef, data, authService, router, location) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.authService = authService;
        this.router = router;
        this.location = location;
    }
    SignOutAlertDialog.prototype.onProceed = function () {
        this.dialogRef.close(true);
    };
    SignOutAlertDialog.prototype.onCancel = function () {
        this.dialogRef.close(false);
        this.location.back();
    };
    SignOutAlertDialog.prototype.onClickOutside = function (targetElement) {
        if (!this.dialogRef.disableClose && targetElement.classList.contains('cdk-overlay-backdrop')) {
            this.dialogRef.close(false);
            this.location.back();
        }
    };
    __decorate([
        core_1.HostListener('document:click', ['$event.target'])
    ], SignOutAlertDialog.prototype, "onClickOutside");
    SignOutAlertDialog = __decorate([
        core_1.Component({
            selector: 'sign-out-alert-dialog',
            templateUrl: 'sign-out-alert-dialog.component.html'
        }),
        __param(1, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], SignOutAlertDialog);
    return SignOutAlertDialog;
}());
exports.SignOutAlertDialog = SignOutAlertDialog;
