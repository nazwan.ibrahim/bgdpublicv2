"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AuthSignOutComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var sign_out_alert_dialog_component_1 = require("./sign-out-alert-dialog/sign-out-alert-dialog.component");
var AuthSignOutComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    function AuthSignOutComponent(_authService, _router, dialog) {
        this._authService = _authService;
        this._router = _router;
        this.dialog = dialog;
        this.countdown = 5;
        this.countdownMapping = {
            '=1': '# second',
            'other': '# seconds'
        };
        this._unsubscribeAll = new rxjs_1.Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    AuthSignOutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var dialogRef = this.dialog.open(sign_out_alert_dialog_component_1.SignOutAlertDialog);
        dialogRef.afterClosed().subscribe(function (result) {
            if (result) {
                // Sign out
                _this._authService.signOut();
                // Redirect after the countdown
                _this._router.navigate(['sign-in']);
                // timer(1000, 1000)
                //   .pipe(
                //     finalize(() => {
                //       this._router.navigate(['sign-in']);
                //     }),
                //     takeWhile(() => this.countdown > 0),
                //     takeUntil(this._unsubscribeAll),
                //     tap(() => this.countdown--)
                //   )
                //   .subscribe();
            }
            else {
            }
        });
    };
    /**
     * On destroy
     */
    AuthSignOutComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    };
    AuthSignOutComponent = __decorate([
        core_1.Component({
            selector: 'auth-sign-out',
            templateUrl: './sign-out.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AuthSignOutComponent);
    return AuthSignOutComponent;
}());
exports.AuthSignOutComponent = AuthSignOutComponent;
