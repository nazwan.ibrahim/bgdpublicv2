import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { finalize, Subject, takeUntil, takeWhile, tap, timer } from 'rxjs';
import { AuthService } from 'app/core/auth/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { SignOutAlertDialog } from './sign-out-alert-dialog/sign-out-alert-dialog.component';
import { result } from 'lodash';

@Component({
    selector: 'auth-sign-out',
    templateUrl: './sign-out.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AuthSignOutComponent implements OnInit, OnDestroy {
    countdown = 5;
    countdownMapping: any = {
        '=1': '# second',
        'other': '# seconds'
    };
    private _unsubscribeAll: Subject<any> = new Subject<any>();

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _router: Router,
        private dialog: MatDialog
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        const dialogRef = this.dialog.open(SignOutAlertDialog);

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // Sign out
                this._authService.signOut();

                // Redirect after the countdown
                this._router.navigate(['sign-in']);

                // timer(1000, 1000)
                //   .pipe(
                //     finalize(() => {
                //       this._router.navigate(['sign-in']);
                //     }),
                //     takeWhile(() => this.countdown > 0),
                //     takeUntil(this._unsubscribeAll),
                //     tap(() => this.countdown--)
                //   )
                //   .subscribe();
            } else {

            }
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }
}
