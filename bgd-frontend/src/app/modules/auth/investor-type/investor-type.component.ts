import { Component, OnInit, ViewChild } from '@angular/core';
import { investorTypeService } from './investor-type.service';
import { Router } from '@angular/router';
import { FuseAlertType } from '@bgd/components/alert';
import { NgForm, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'bgd-investor-type',
  templateUrl: './investor-type.component.html',
  styleUrls: ['./investor-type.component.scss']
})
export class InvestorTypeComponent implements OnInit {
  @ViewChild('InvestorTypeNgForm') InvestorTypeNgForm: NgForm;


  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  InvestorTypeForm: UntypedFormGroup;
  radioData: any[];
  investorType: any[];
  selectedOption: any;

  constructor(
    private dataService: investorTypeService,
    private router: Router,
    private _formBuilder: UntypedFormBuilder
  ) { }

  ngOnInit() {
    this.dataService.investorType().subscribe(data => {
      this.investorType = data;
      this.radioData = this.investorType.filter(item => item.code === 'K01');
      console.log(this.radioData);
      this.selectedOption = this.radioData[0].id;
      console.log(this.selectedOption);
    });

    this.InvestorTypeForm = this._formBuilder.group({
      myRadioGroup: [this.selectedOption, Validators.required]
    });
  }

  onSubmit() {

    this.router.navigate(['/register']);
  }

}
