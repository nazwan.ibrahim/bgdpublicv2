import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InvestorTypeRoutes } from 'app/modules/auth/investor-type/investor-type.routing';
import { InvestorTypeRoutingModule } from './investor-type-routing.module';
import { InvestorTypeComponent } from './investor-type.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FuseCardModule } from '@bgd/components/card';
import { FuseAlertModule } from '@bgd/components/alert';
import { SharedModule } from 'app/shared/shared.module';
import  {MatDividerModule } from '@angular/material/divider';
import { MatRadioModule } from '@angular/material/radio';


@NgModule({
  declarations: [
    InvestorTypeComponent
  ],
  imports: [
    RouterModule.forChild(InvestorTypeRoutes),
    CommonModule,
    InvestorTypeRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatProgressSpinnerModule,
    FuseCardModule,
    FuseAlertModule,
    SharedModule,
    MatDividerModule,
    MatRadioModule,
  ]
})
export class InvestorTypeModule { }
