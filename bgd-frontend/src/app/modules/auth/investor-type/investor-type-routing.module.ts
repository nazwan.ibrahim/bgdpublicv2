import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvestorTypeComponent } from 'app/modules/auth/investor-type/investor-type.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvestorTypeRoutingModule { }