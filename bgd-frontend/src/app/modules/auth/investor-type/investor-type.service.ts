import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
    providedIn: 'root',
})
export class investorTypeService {
    constructor(private _httpClient: HttpClient) { }


    // Read
    investorType(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/investorType');
    }

}