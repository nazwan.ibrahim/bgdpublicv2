import { Route} from '@angular/router';
import { InvestorTypeComponent } from 'app/modules/auth/investor-type/investor-type.component';

export const InvestorTypeRoutes: Route[] = [
  {
      path     : '',
      component: InvestorTypeComponent
  }
];

