import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoldPriceDetailsComponent } from './gold-price-details.component';

describe('GoldPriceDetailsComponent', () => {
  let component: GoldPriceDetailsComponent;
  let fixture: ComponentFixture<GoldPriceDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoldPriceDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GoldPriceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
