import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { environment } from 'environments/environment';
import { webSocket } from 'rxjs/webSocket';
import { messages } from 'app/mock-api/apps/chat/data';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    insightSelector : any;

    
    private _data: BehaviorSubject<any> = new BehaviorSubject(null);

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for data
     */
    get data$(): Observable<any> {
        return this._data.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get data
     */
    getData(): Observable<any> {
        return this._httpClient.get('api/dashboards/finance').pipe(
            tap((response: any) => {
                this._data.next(response);
            })
        );
    }

    getGoldWalletDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/walletDetails')
    }

    getPriceGold(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/trading/queryBuyPrice')
    }
    getSellGold(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/trading/querySellPrice')
    }

    getPriceGraph(days: any): Observable<any> {
        const params = {
            days: days
        };
        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/priceGraph', { params: params });
    }

    GoldWs() {
        return webSocket(environment.wsUrl);
    }

    //Get Gold Buy Price
    getQueryBuyPrice(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/trading/queryBuyPrice');
    }

    //Get Gold Sell Price
    getQuerySellPrice(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/trading/querySellPrice');
    }

    getUserDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getUserDetails');
    }

    getProfitLoss(current_price: any): Observable<any> {
        const params = new HttpParams().set('current_price', current_price);

        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/profit_loss', { params });
    }

    getUserAddress(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getAddress');
    }

    initRedeem(acctNumber: any, unit: any, name: any, phone: any, address: any, address2: any, postcode: any, city: any, state: any): Observable<any> {
        const params = new HttpParams()
            .set('acctNumber', acctNumber)
            .set('unit', unit)
            .set('name', name)
            .set('phone', phone)
            .set('address', address)
            .set('address2', address2)
            .set('postcode', postcode)
            .set('city', city)
            .set('state', state);
        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/initRedeem', { params });
    }

    getRedeemMotive(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/redeemMotive');
    }

    getThreshold(tierCode: string): Observable<any> {
        const params = new HttpParams().set('code', tierCode);
        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/tierManagement', { params });
    }

    getBankAccountDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getBankDetails');
    }
}
