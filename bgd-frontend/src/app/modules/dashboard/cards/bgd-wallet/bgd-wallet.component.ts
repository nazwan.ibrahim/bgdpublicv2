import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { switchMap, tap, map, catchError } from 'rxjs/operators';
import { DashboardService } from '../../dashboard.service';
import { of, Subscription } from 'rxjs';

@Component({
  selector: 'bgd-wallet',
  templateUrl: './bgd-wallet.component.html',
  styleUrls: ['./bgd-wallet.component.scss'],
})

export class BgdWalletComponent implements OnInit, OnDestroy, AfterViewInit {

  availAmount: number = 0;
  walletModel: any[] = [];
  goldWalletIndex: number;
  buyprice: number = 0;
  sellprice: number = 0;
  totalAmount: number = 0;
  private hasRun = false;

  constructor(private _dashboardService: DashboardService) { }

  ngOnInit(): void {
    this._dashboardService.getGoldWalletDetails().pipe(
      tap((value: any) => {
        if (!value) {
          return of(null);
        }
        this.walletModel = value;
        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Gold") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._dashboardService.getGoldWalletDetails();
      }),
      catchError((error) => {
        return of(null);
      })
    ).subscribe((data: any) => {
      if (!data) {
        return of(null);
      }
      this.availAmount = data[this.goldWalletIndex].availableAmount.toFixed(6);
      this.calculateTotalAmount(this.sellprice);
      this.hasRun = true;
    });
  }

  GoldWsSubscription: Subscription;

  calculateTotalAmount(sellprice: number) {
    this.totalAmount = Number((this.availAmount * sellprice).toFixed(2));
  }

  ngAfterViewInit() {
    this.GoldWsSubscription = this._dashboardService.GoldWs().subscribe(
      (message) => {
        this.buyprice = (message['bursaBuyPrice']).toFixed(2);
        this.sellprice = (message['bursaSellPrice']).toFixed(2)
        this.calculateTotalAmount(this.sellprice);
      },
      (error) => console.error('WebSocket error:', error),
      () => console.log('WebSocket connection closed')
    );
  }

  ngOnDestroy() {
    this.GoldWsSubscription.unsubscribe();
  }
}
