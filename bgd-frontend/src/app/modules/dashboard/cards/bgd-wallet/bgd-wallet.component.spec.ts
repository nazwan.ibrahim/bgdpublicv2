import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BgdWalletComponent } from './bgd-wallet.component';

describe('BgdWalletComponent', () => {
  let component: BgdWalletComponent;
  let fixture: ComponentFixture<BgdWalletComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BgdWalletComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BgdWalletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
