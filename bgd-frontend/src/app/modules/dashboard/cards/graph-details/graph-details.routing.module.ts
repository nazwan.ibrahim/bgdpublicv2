import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { GraphDetailsComponent } from "./graph-details.component";

const routes: Routes = [
  {
    path: '',
    component: GraphDetailsComponent,

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GraphDetailsRoutingModule { }
