import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { GraphDetailsComponent } from './graph-details.component';
import { GraphDetailsRoutingModule } from './graph-details.routing.module';
import { NgApexchartsModule } from 'ng-apexcharts';


@NgModule({
    declarations: [
        GraphDetailsComponent,
 
    ],
    imports     : [
        GraphDetailsRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
        NgApexchartsModule,

      ]
})
export class GraphDetailsModule
{
}
