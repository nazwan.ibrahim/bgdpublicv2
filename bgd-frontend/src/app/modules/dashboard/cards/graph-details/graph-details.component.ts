import { AfterViewInit, ChangeDetectionStrategy, ElementRef, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import { DashboardService } from '../../dashboard.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { switchMap } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';
import { Router } from '@angular/router';
import * as ApexCharts from 'apexcharts';
import { ChartComponent, ApexChart } from "ng-apexcharts";
import { takeUntil } from 'rxjs/operators';
import { DateTime } from 'luxon';
@Component({
    selector: 'bgd-graph-details',
    templateUrl: './graph-details.component.html',
    styleUrls: ['./graph-details.component.scss'],
    animations: fuseAnimations
})

export class GraphDetailsComponent implements OnInit {
    @ViewChild('chart') chartEl: ElementRef;

    availAmount: number = 0;
    walletModel: any[] = [];
    goldWalletIndex: number;
    buyprice: number = 0;
    sellprice: number = 0;
    totalAmount: number = 0;
    series: any;
    data: any;
    selectedInterval: string;
    chart: ApexCharts;
    date2: any;
    price: any;
    userTier: string;
    options: any;
    amount: number = 0;
    current_price: number = 0;
    idResponse: any;
    isChartLoaded = false;
    private hasRun = false;


    constructor(private _dashboardService: DashboardService,
        public sanitizer: DomSanitizer,
        private _router: Router,
        private cd: ChangeDetectorRef,
        private elRef: ElementRef) {

    }

    ngOnInit(): void {

        this.initializeChart();

        //profit loss
        this._dashboardService.getSellGold().pipe(
            switchMap(response => {
                this.current_price = response['A02'].price.toFixed(2);
                return this._dashboardService.getProfitLoss(this.current_price);
            })
        ).subscribe(
            response => {
                this.idResponse = response.id;
                this.amount = response.amount.toFixed(2);
            }
        );
        // gold price socket

        this._dashboardService.GoldWs().subscribe(
            (message) => {
                // const json = JSON.stringify(message); // parse the JSON data
                //console.log('Received message:', message);
                this.buyprice = (message['bursaBuyPrice']).toFixed(2);
                this.sellprice = (message['bursaSellPrice']).toFixed(2)
                //console.log('Buy message:', (message['price'].buyPrice).toFixed(2));
                //console.log('Sell message:', (message['price'].sellPrice).toFixed(2))
            },
            (error) => console.error('WebSocket error:', error),
            () => console.log('WebSocket connection closed')
        );

        // Get the data user

        this._dashboardService.getUserDetails().subscribe(
            users => {

                this.userTier = users['tierStatus'].code;
            }
        );

        //

        //graph price


    }

    private initializeChart(): void {
        this.series = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                width: '100%',
                height: '400%',
                type: 'area',
                sparkline: {
                    enabled: false
                },
                toolbar: {
                    show: false // Hide the toolbar
                }
            },
            title: {
                text: 'Gold Price Chart',
                align: 'left'
            },
            colors: ['#BFD3FF'],
            fill: {
                type: 'no-repeat',
                // gradient: {
                //     shadeIntensity: 1,
                //     inverseColors: false,
                //     opacityFrom: 0.5,
                //     opacityTo: 0,
                //     stops: [0, 90, 100]
                // },
            },
            series: [{
                name: 'Price',
                data: [],
            }],
            labels: [],
            stroke: {
                curve: 'straight',
                width: 2
            },
            dataLabels: {
                enabled: false
            },
            tooltip: {
                followCursor: true,
                theme: 'light',
                x: {
                    format: 'dd MMM yyyy'
                },
                y: {
                    formatter: (value): string => 'RM' + value + ' /g'
                }
            },
            x: {
                type: 'category', // Use 'category' type for the x-axis
                tickPlacement: 'between',
                labels: {
                    datetimeFormatter: {
                        year: 'numeric',
                        month: 'short',
                        day: 'numeric',
                    },
                },
                categories: [],
            },
            y: {

                opposite: true
            },
            legend: {
                horizontalAlign: "right"
            }
        };
        // Delay the retrieval of data and updating of chart until the next tick

        const selectedInterval = '30';

        this._dashboardService.getPriceGraph(selectedInterval).subscribe(
            response => {
                console.log("response la ", response);

                const data = response.map((point, index, array) => {
                    const previousWeekPoint = array[index - 1];
                    const priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                    const percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;

                    return {
                        x: new Date(point.date).toLocaleDateString('en-GB', { day: '2-digit', month: 'short' }),
                        y: point.price.toFixed(2),
                        difference: priceDifference.toFixed(2),
                        percentageDifference: percentageDifference.toFixed(2) + '%',
                    };
                });
                this.series.x.categories = data.map(point => point.x);

                this.updateChart(data);
            },
            error => console.log(error)
        );

    }

    onSelectInterval(selectedInterval) {
        // Save the selected interval to localStorage
        localStorage.setItem('selectedInterval', selectedInterval);
        console.log(selectedInterval);
        this._dashboardService.getPriceGraph(selectedInterval).subscribe(
            response => {

                console.log("DATA RES ", response);

                // const data = response.map((point, index, array) => {
                //     const previousWeekPoint = array[index - 7];
                //     const priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                //     const percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;

                if (selectedInterval === '7') {
                    const data = response.map((point, index, array) => {
                        const previousWeekPoint = array[index - 1];
                        const priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                        const percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;

                        return {
                            x: new Date(point.date).toLocaleDateString('en-GB', { day: '2-digit', month: 'short' }),
                            y: point.price.toFixed(2),
                            difference: priceDifference.toFixed(2),
                            percentageDifference: percentageDifference.toFixed(2) + '%',
                        };
                    });
                    requestAnimationFrame(() => {
                        this.updateChart(data);
                    });
                }
                else if (selectedInterval === '30') {
                    const data = response.map((point, index, array) => {
                        const previousWeekPoint = array[index - 7];
                        const priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                        const percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;

                        return {
                            x: new Date(point.date).toLocaleDateString('en-GB', { day: '2-digit', month: 'short' }),
                            y: point.price.toFixed(2),
                            difference: priceDifference.toFixed(2),
                            percentageDifference: percentageDifference.toFixed(2) + '%',
                        };
                    });
                    requestAnimationFrame(() => {
                        this.updateChart(data);
                    });
                }
                else if (selectedInterval === '180') {
                    const data = response.map(dataPoint => {
                        const date = new Date(dataPoint.date);
                        const month = date.toLocaleDateString('en-GB', { month: 'short' });
                        return {
                            x: month, // Format the date as a short month string (e.g., Jan, Feb, Mar, etc.)
                            y: dataPoint.price.toFixed(2),
                        };
                    });
                    data.sort((a, b) => a.x - b.x);

                    const startDate = data[0].x;
                    const endDate = data[data.length - 1].x;
                    const startMonth = startDate.toLocaleString('default', { month: 'short' });
                    const endMonth = endDate.toLocaleString('default', { month: 'short' });
                    const dateRange = startMonth + ' - ' + endMonth;

                    this.updateChart(data);

                    console.log('Date Range: ' + dateRange);
                }
                else if (selectedInterval === '365') {
                    const data = response.map((point, index, array) => {
                        const previousWeekPoint = array[index - 7];
                        const priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                        const percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;

                        return {
                            x: new Date(point.date),
                            y: point.price.toFixed(2),
                            difference: priceDifference.toFixed(2),
                            percentageDifference: percentageDifference.toFixed(2) + '%',
                        };
                    });

                    data.sort((a, b) => a.x - b.x);

                    const startDate = data[0].x;
                    const endDate = data[data.length - 1].x;
                    const startMonth = startDate.toLocaleString('default', { month: 'short' });
                    const endMonth = endDate.toLocaleString('default', { month: 'short' });
                    const dateRange = startMonth + ' - ' + endMonth;

                    this.updateChart(data);

                    console.log('Date Range: ' + dateRange);
                } else if (selectedInterval === '1095') {
                    const data = response.map((point, index, array) => {
                        const previousWeekPoint = array[index - 7];
                        const priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                        const percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;

                        return {
                            x: new Date(point.date),
                            y: point.price.toFixed(2),
                            difference: priceDifference.toFixed(2),
                            percentageDifference: percentageDifference.toFixed(2) + '%',
                        };
                    });

                    data.sort((a, b) => a.x - b.x);

                    const startDate = data[0].x;
                    const endDate = data[data.length - 1].x;
                    const startYear = startDate.getFullYear();
                    const endYear = endDate.getFullYear();

                    const yearDiff = endYear - startYear;

                    if (yearDiff >= 2) {
                        // Show labels for each year
                        const years = Array.from({ length: yearDiff + 1 }, (_, i) => startYear + i);
                        const dataByYears = [];

                        years.forEach((year) => {
                            const startOfYear = new Date(year, 0, 1);
                            const endOfYear = new Date(year, 11, 31);
                            const yearData = data.filter((point) => point.x >= startOfYear && point.x <= endOfYear);
                            dataByYears.push(...yearData);
                        });

                        this.updateChart(dataByYears);
                        console.log('Date Range: ' + startYear + ' - ' + endYear);
                    } else {
                        // Show the original data
                        this.updateChart(data);
                        const startMonth = startDate.toLocaleString('default', { month: 'short' });
                        const endMonth = endDate.toLocaleString('default', { month: 'short' });
                        const dateRange = startMonth + ' - ' + endMonth;
                        console.log('Date Range: ' + dateRange);
                    }
                }
            },
            error => {
                // handle the error
            }
        );
    }

    updateChart(data) {
        //this.series.series[0].data = data;
        this.series.series = [{
            name: 'Price',
            data: data,
        }];
        this.series.labels = data.map(point => point.x);
        this.isChartLoaded = true;
        this.cd.detectChanges(); // Manually trigger change detection

        if (this.chart && this.chart.updateOptions) {
            this.chart.updateOptions(this.series, false, true);
        }
    }

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

}