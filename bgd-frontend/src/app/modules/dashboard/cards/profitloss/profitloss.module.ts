import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { ProfitLossComponent } from './profitloss.component';
import { ProfitLossRoutingModule } from './profitloss.routing.module';

@NgModule({
    declarations: [
        ProfitLossComponent,
 
    ],
    imports     : [
        ProfitLossRoutingModule,
        MatButtonModule,
        CommonModule,
        SharedModule,
        MatButtonModule,
        MatInputModule,
        MatIconModule,
      ]
})
export class ProfitLossModule
{
}
