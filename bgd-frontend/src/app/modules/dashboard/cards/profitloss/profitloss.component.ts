import { Component, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { DashboardService } from '../../dashboard.service';
import { switchMap } from 'rxjs/operators';
import { fuseAnimations } from '@bgd/animations';

@Component({
  selector: 'bgd-profitloss',
  templateUrl: './profitloss.component.html',
  styleUrls: ['./profitloss.component.scss'],
  animations: fuseAnimations
})

export class ProfitLossComponent implements OnInit {
  availAmount: number = 0;
  walletModel: any[] = [];
  goldWalletIndex: number;
  buyprice: number = 0;
  sellprice: number = 0;
  totalAmount: number = 0;
  private hasRun = false;

  constructor(private _dashboardService: DashboardService) { }

  ngOnInit(): void {

    this._dashboardService.GoldWs().subscribe(
      (message) => {

        this.buyprice = (message['bursaBuyPrice']).toFixed(2);
        this.sellprice = (message['bursaSellPrice']).toFixed(2)
        this.calculateTotalAmount();

      },
      (error) => console.error('WebSocket error:', error),
      () => console.log('WebSocket connection closed')
    );
  }
  calculateTotalAmount() {
    if (this.hasRun) {
      return; // function has already run, return
    }
    if (!this.sellprice) {
      return; // sellprice is not available yet, return and wait for next update
    }

    this._dashboardService.getGoldWalletDetails().pipe(
      switchMap((value: any) => {
        this.walletModel = value;

        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Gold") {
            this.goldWalletIndex = i;
            break;
          }
        }
        return this._dashboardService.getGoldWalletDetails();
      })
    ).subscribe((data: any) => {


      this.availAmount = data[this.goldWalletIndex].availableAmount.toFixed(6);

      this.totalAmount = this.availAmount * this.sellprice;
      this.hasRun = true;

    });
  }

}
