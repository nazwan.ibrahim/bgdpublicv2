import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VerifyMykadRoutes } from 'app/modules/auth/verify-mykad/verify-mykad.routing';
import {WebcamModule} from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { InsightsDetailComponent } from './insights-detail.component';
import {MatStepperModule} from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { InsightsDetailRoutes } from './insights-detail.routing';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        InsightsDetailComponent,
 
    ],
    imports     : [
        RouterModule.forChild(InsightsDetailRoutes),
        WebcamModule,
        FormsModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        // MatSelectModule,
        MatCardModule,
        MatIconModule,
    ],
    

})
export class InsightsDetailModule
{
}
