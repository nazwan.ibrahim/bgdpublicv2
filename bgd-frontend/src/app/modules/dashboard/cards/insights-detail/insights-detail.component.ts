import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../dashboard.service';

@Component({
  selector: 'bgd-insights-detail',
  templateUrl: './insights-detail.component.html',
  styleUrls: ['./insights-detail.component.scss']
})
export class InsightsDetailComponent {
  selectedCard: number;

  constructor(
    private dashboardService: DashboardService,
  )
  {}
  ngOnInit(){
    this.selectedCard = this.dashboardService.insightSelector
    console.log(this.selectedCard)
  }

}
