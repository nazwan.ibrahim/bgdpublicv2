import { Route} from '@angular/router';
import { InsightsDetailComponent } from './insights-detail.component';

export const InsightsDetailRoutes: Route[] = [
  {
      path     : '',
      component: InsightsDetailComponent
  }
];

