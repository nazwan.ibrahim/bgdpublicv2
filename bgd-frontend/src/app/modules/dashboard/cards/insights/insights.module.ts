import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VerifyMykadRoutes } from 'app/modules/auth/verify-mykad/verify-mykad.routing';
import {WebcamModule} from 'ngx-webcam';
import { CommonModule } from '@angular/common';
import { InsightsComponent } from './insights.component';
import {MatStepperModule} from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import { InsightsRoutes } from './insights.routing';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        InsightsComponent,
 
    ],
    imports     : [
        RouterModule.forChild(InsightsRoutes),
        WebcamModule,
        FormsModule,
        CommonModule,
        MatStepperModule,
        MatButtonModule,
        // MatSelectModule,
        MatCardModule,
        MatIconModule,
    ],
    

})
export class InsightsModule
{
}
