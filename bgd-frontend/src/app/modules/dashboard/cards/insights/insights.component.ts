import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from '../../dashboard.service';

@Component({
  selector: 'bgd-insights',
  templateUrl: './insights.component.html',
  styleUrls: ['./insights.component.scss']
})
export class InsightsComponent {

  constructor(
    private _router: Router,
    private dashboardService: DashboardService,
  )
  {}

  selectedCardOne() {
    this.dashboardService.insightSelector = 1;
    this._router.navigate(['/insights-detail']);
  }

  selectedCardTwo() {
    this.dashboardService.insightSelector = 2;
    this._router.navigate(['/insights-detail']);
  }

  selectedCardThree() {
    this.dashboardService.insightSelector = 3;
    this._router.navigate(['/insights-detail']);
  }

}