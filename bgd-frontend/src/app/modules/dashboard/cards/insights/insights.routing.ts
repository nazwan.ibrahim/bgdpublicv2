import { Route} from '@angular/router';
import { InsightsComponent
 } from './insights.component';
export const InsightsRoutes: Route[] = [
  {
      path     : '',
      component: InsightsComponent
  }
];

