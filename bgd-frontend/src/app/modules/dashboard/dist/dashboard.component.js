"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.DashboardComponent = void 0;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var rxjs_2 = require("rxjs");
var DashboardComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    function DashboardComponent(_dashboardService, sanitizer, _router, cd) {
        this._dashboardService = _dashboardService;
        this.sanitizer = sanitizer;
        this._router = _router;
        this.cd = cd;
        this.buyprice = 0;
        this.sellprice = 0;
        this.amount = 0;
        this.current_price = 0;
        this.isChartLoaded = false;
        this._unsubscribeAll = new rxjs_1.Subject();
        this.chartOptions = {
            chart: {
                type: 'area'
            },
            series: [],
            xaxis: {
                type: 'datetime'
            }
        };
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initializeChart();
        //profit loss
        this._dashboardService.getPriceGold().pipe(rxjs_2.switchMap(function (response) {
            _this.current_price = response['A02'].price.toFixed(2);
            console.log('Current Price :', _this.current_price);
            return _this._dashboardService.getProfitLoss(_this.current_price);
        })).subscribe(function (response) {
            console.log(response);
            _this.idResponse = response.id;
            _this.amount = response.amount.toFixed(2);
            console.log('Current Price after profit loss  :', _this.current_price);
        });
        // gold price socket
        this._dashboardService.GoldWs().subscribe(function (message) {
            // const json = JSON.stringify(message); // parse the JSON data
            //console.log('Received message:', message);
            _this.buyprice = (message['bursaBuyPrice']).toFixed(2);
            _this.sellprice = (message['bursaSellPrice']).toFixed(2);
            //console.log('Buy message:', (message['price'].buyPrice).toFixed(2));
            //console.log('Sell message:', (message['price'].sellPrice).toFixed(2))
        }, function (error) { return console.error('WebSocket error:', error); }, function () { return console.log('WebSocket connection closed'); });
        // Get the data user
        this._dashboardService.getUserDetails().subscribe(function (users) {
            _this.tierCode = users.tierStatus.code;
            _this.statusTier = users.userTier.code;
            _this.tierName = users.userTier.name;
            console.log("User Details ", users);
            _this.userPinEnabled = users.pinEnable;
            console.log(_this.userPinEnabled);
        });
        //
        //graph price
    };
    DashboardComponent.prototype.initializeChart = function () {
        var _this = this;
        this.series = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                width: '100%',
                height: '400%',
                type: 'area',
                sparkline: {
                    enabled: false
                },
                toolbar: {
                    show: false // Hide the toolbar
                }
            },
            title: {
                text: 'Gold Price Chart',
                align: 'left'
            },
            colors: ['#BFD3FF'],
            fill: {
                type: 'no-repeat'
            },
            series: [{
                    name: 'Price',
                    data: []
                }],
            labels: [],
            stroke: {
                curve: 'straight',
                width: 2
            },
            dataLabels: {
                enabled: false
            },
            tooltip: {
                followCursor: true,
                theme: 'light',
                x: {
                    format: 'dd MMM yyyy'
                },
                y: {
                    formatter: function (value) { return 'RM' + value + ' /g'; }
                }
            },
            x: {
                labels: []
            },
            y: {
                opposite: true
            },
            legend: {
                horizontalAlign: "right"
            }
        };
        // Delay the retrieval of data and updating of chart until the next tick
        setTimeout(function () {
            _this._dashboardService.getPriceGraph('30').subscribe(function (response) {
                var data = response.map(function (point, index, array) {
                    var previousWeekPoint = array[index - 7];
                    var priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                    var percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;
                    return {
                        x: new Date(point.date).toLocaleDateString('en-GB', { day: '2-digit', month: 'short' }),
                        y: point.price.toFixed(2),
                        difference: priceDifference.toFixed(2),
                        percentageDifference: percentageDifference.toFixed(2) + '%'
                    };
                });
                requestAnimationFrame(function () {
                    _this.updateChart(data);
                });
            }, function (error) { return console.log(error); });
        });
    };
    DashboardComponent.prototype.updateChart = function (data) {
        this.series.series[0].data = data;
        this.series.labels = data.map(function (point) { return point.x; });
        console.log(this.series.series[0].data);
        this.isChartLoaded = true;
        this.cd.detectChanges();
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Prepare the chart data from the data
     *
     * @private
     */
    /*private _prepareChartData(): void {
        // Account balance
        this.series = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                width: '100%',
                height: 350,
                type: 'line',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#BFD3FF'],
            fill: {
                colors: ['#BFD3FF'],
                opacity: 1,
                type: 'solid'
            },
            series: this.data.series,
            labels: this.data.series,
            stroke: {
                curve: 'straight',
                width: 2
            },
            dataLabels: {
                enabled: false
            },
            tooltip: {
                followCursor: true,
                theme: 'dark',
                x: {
                    format: 'MMM dd, yyyy'
                },
                y: {
                    formatter: (value): string => value + '%'
                }
            },
            xaxis: {
                type: 'datetime'
            },
            yaxis: {
                opposite: true
            },
            legend: {
                horizontalAlign: "right"
            }
        };
    }*/
    DashboardComponent.prototype.profit = function () {
        this._router.navigate(['/profitloss']);
    };
    DashboardComponent.prototype.buy = function () {
        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/buy-bgd']);
        }
        else {
            this._router.navigate(['/upgrade-account']);
        }
    };
    DashboardComponent.prototype.sell = function () {
        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/sell-bgd']);
        }
        else {
            this._router.navigate(['/upgrade-account']);
        }
    };
    DashboardComponent.prototype.transfer = function () {
        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/transfer-landing']);
        }
        else {
            this._router.navigate(['/upgrade-account']);
        }
    };
    DashboardComponent.prototype.redeem = function () {
        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/redeem-bgd']);
        }
        else {
            this._router.navigate(['/upgrade-account']);
        }
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'bgd-dashboard',
            templateUrl: './dashboard.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
