import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { NgApexchartsModule } from 'ng-apexcharts';
import { SharedModule } from 'app/shared/shared.module';
import { DashboardComponent } from 'app/modules/dashboard/dashboard.component';
import { BgdWalletComponent } from './cards/bgd-wallet/bgd-wallet.component';
import { DashboardRoutingModule } from './dashboard.routing.module';
import { MatCardModule } from '@angular/material/card';
import { TranslocoModule } from '@ngneat/transloco';

@NgModule({
    declarations: [
        DashboardComponent,
        BgdWalletComponent
    ],
    imports: [
        DashboardRoutingModule,
        MatButtonModule,
        MatDividerModule,
        MatIconModule,
        MatMenuModule,
        MatProgressBarModule,
        MatSortModule,
        MatTableModule,
        NgApexchartsModule,
        SharedModule,
        MatCardModule,
        TranslocoModule
    ]
})
export class DashboardModule {
}
