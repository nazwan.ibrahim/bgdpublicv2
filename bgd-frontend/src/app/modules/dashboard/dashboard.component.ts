import { AfterViewInit, ChangeDetectionStrategy, ElementRef, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation, NgZone } from '@angular/core';
import { Subject, takeUntil, interval } from 'rxjs';
import { ChartComponent, ApexChart } from "ng-apexcharts";
import { DashboardService } from 'app/modules/dashboard/dashboard.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Subscription, switchMap } from 'rxjs';
import * as ApexCharts from 'apexcharts';

@Component({
    selector: 'bgd-dashboard',
    templateUrl: './dashboard.component.html',
    encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements OnInit, OnDestroy {

    buyprice: number = 0;
    sellprice: number = 0;
    userTier: string;
    options: any;
    series: any;
    data: any;
    selectedInterval: string;
    chart: ApexCharts;
    amount: number = 0;
    current_price: number = 0;
    idResponse: any;
    isChartLoaded = false;
    userPinEnabled: any;
    statusTier: any;
    tierCode: any;
    tierName: any;
    dataSubscription: Subscription;

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    public chartOptions: any = {
        chart: {
            type: 'area',
        },
        series: [],
        xaxis: {
            type: 'datetime',
        },
    };

    /**
     * Constructor
     */
    constructor(private _dashboardService: DashboardService,
        public sanitizer: DomSanitizer,
        private _router: Router,
        private cd: ChangeDetectorRef,

    ) {


    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.initializeChart();

        //profit loss
        this._dashboardService.getPriceGold().pipe(
            switchMap(response => {
                this.current_price = response['A02'].price.toFixed(2);
                console.log('Current Price :', this.current_price);
                return this._dashboardService.getProfitLoss(this.current_price);
            })
        ).subscribe(
            response => {
                console.log(response);
                this.idResponse = response.id;
                this.amount = response.amount.toFixed(2);
                console.log('Current Price after profit loss  :', this.current_price);
            }
        );
        // gold price socket

        this._dashboardService.GoldWs().subscribe(
            (message) => {
                // const json = JSON.stringify(message); // parse the JSON data
                //console.log('Received message:', message);
                this.buyprice = (message['bursaBuyPrice']).toFixed(2);
                this.sellprice = (message['bursaSellPrice']).toFixed(2)
                //console.log('Buy message:', (message['price'].buyPrice).toFixed(2));
                //console.log('Sell message:', (message['price'].sellPrice).toFixed(2))
            },
            (error) => console.error('WebSocket error:', error),
            () => console.log('WebSocket connection closed')
        );

        // Get the data user

        this._dashboardService.getUserDetails().subscribe(
            users => {

                this.tierCode = users.tierStatus.code;

                this.statusTier = users.userTier.code;
                this.tierName = users.userTier.name;

                console.log("User Details ", users);
                this.userPinEnabled = users.pinEnable;
                console.log(this.userPinEnabled);
            }
        );

        //

        //graph price


    }

    initializeChart() {
        this.series = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                width: '100%',
                height: '400%',
                type: 'area',
                sparkline: {
                    enabled: false
                },
                toolbar: {
                    show: false // Hide the toolbar
                }
            },
            title: {
                text: 'Gold Price Chart',
                align: 'left'
            },
            colors: ['#BFD3FF'],
            fill: {
                type: 'no-repeat',
                // gradient: {
                //     shadeIntensity: 1,
                //     inverseColors: false,
                //     opacityFrom: 0.5,
                //     opacityTo: 0,
                //     stops: [0, 90, 100]
                // },
            },
            series: [{
                name: 'Price',
                data: [],
            }],
            labels: [],
            stroke: {
                curve: 'straight',
                width: 2
            },
            dataLabels: {
                enabled: false
            },
            tooltip: {
                followCursor: true,
                theme: 'light',
                x: {
                    format: 'dd MMM yyyy'
                },
                y: {
                    formatter: (value): string => 'RM' + value + ' /g'
                }
            },
            x: {
                labels: [],
            },
            y: {

                opposite: true
            },
            legend: {
                horizontalAlign: "right"
            }
        };
        // Delay the retrieval of data and updating of chart until the next tick
        setTimeout(() => {
            this._dashboardService.getPriceGraph('30').subscribe(
                response => {
                    const data = response.slice(-7).map((point, index, array) => {
                        const previousWeekPoint = array[index - 1];
                        const priceDifference = previousWeekPoint ? point.price - previousWeekPoint.price : 0;
                        const percentageDifference = previousWeekPoint ? (priceDifference / previousWeekPoint.price) * 100 : 0;
                        return {
                            x: new Date(point.date).toLocaleDateString('en-GB', { weekday: 'short' }),
                            y: point.price.toFixed(2),
                            difference: priceDifference.toFixed(2),
                            percentageDifference: percentageDifference.toFixed(2) + '%',
                        };
                    });
                    requestAnimationFrame(() => {
                        this.updateChart(data);
                    });
                },
                error => console.log(error)
            );
        });
    }
    updateChart(data) {
        this.series.series[0].data = data;
        this.series.labels = data.map(point => point.x);
        console.log(this.series.series[0].data);

        this.isChartLoaded = true;
        this.cd.detectChanges();

    }

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next(null);
        this._unsubscribeAll.complete();
    }

    get currentYear(): number {
        return new Date().getFullYear();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Prepare the chart data from the data
     *
     * @private
     */
    /*private _prepareChartData(): void {
        // Account balance
        this.series = {
            chart: {
                animations: {
                    speed: 400,
                    animateGradually: {
                        enabled: false
                    }
                },
                fontFamily: 'inherit',
                foreColor: 'inherit',
                width: '100%',
                height: 350,
                type: 'line',
                sparkline: {
                    enabled: true
                }
            },
            colors: ['#BFD3FF'],
            fill: {
                colors: ['#BFD3FF'],
                opacity: 1,
                type: 'solid'
            },
            series: this.data.series,
            labels: this.data.series,
            stroke: {
                curve: 'straight',
                width: 2
            },
            dataLabels: {
                enabled: false
            },
            tooltip: {
                followCursor: true,
                theme: 'dark',
                x: {
                    format: 'MMM dd, yyyy'
                },
                y: {
                    formatter: (value): string => value + '%'
                }
            },
            xaxis: {
                type: 'datetime'
            },
            yaxis: {
                opposite: true
            },
            legend: {
                horizontalAlign: "right"
            }
        };
    }*/

    profit(): void {
        this._router.navigate(['/profitloss']);
    }


    buy(): void {

        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/buy-bgd']);

        } else {
            this._router.navigate(['/upgrade-account']);
        }
    }
    sell(): void {

        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/sell-bgd']);

        } else {
            this._router.navigate(['/upgrade-account']);
        }
    }
    transfer(): void {

        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/transfer-landing']);

        } else {
            this._router.navigate(['/upgrade-account']);
        }
    }
    redeem(): void {
        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/redeem-bgd']);

        } else {
            this._router.navigate(['/upgrade-account']);
        }
    }

    helpSupport(){
        this._router.navigate(["/helpsupport"])
    }

    insightsView(){
        this._router.navigate(["/insights"])
    }

}
