import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@bgd/animations';
import { FuseAlertType } from '@bgd/components/alert';
import { CashWalletService } from './cash-wallet.service';
import round from 'lodash-es/round';
import { switchMap, map, catchError } from 'rxjs/operators';
import { DashboardService } from '../dashboard/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { orderBy } from 'lodash';
import { bgdService } from '../admin/bgd/bgd.service';
import { saveAs } from 'file-saver';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { WalletService } from './wallet.service';

@Component({
  selector: 'bgd-cash-wallet',
  templateUrl: './cash-wallet.component.html',
  styleUrls: ['./cash-wallet.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class CashWalletComponent {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  alert: { type: FuseAlertType; message: string } = {
    type: 'success',
    message: ''
  };

  today: Date = new Date();
  walletDetails = [];
  dropdownData = [];
  displayData = [];
  totalItemsCount: number;
  accountWallet: any;
  fullName: any;
  uomType: any;
  availAmount: any;
  accountBank: any;
  bankName: any;
  bankCode: any;
  statusCode: any;
  statusTier: any;
  tierName: any;
  walletModel: any;
  cashWalletIndex: number = -1;
  transIndex: number = -1;
  pageSize: number = 5;
  currentPageIndex: number = 0;
  selectedOption: string = 'All';
  transaction: string = '';
  tierCode: any;
  details: any;
  displayAmount: any;
  displayAccount: string = '';
  email: any;
  username: any;

  constructor(
    private walletDetailsService: CashWalletService,
    private bgdService: bgdService,
    private user: DashboardService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private sanitizer: DomSanitizer,
    private walletService: WalletService

  ) {

  }

  ngOnInit() {

    this.walletDetailsService.getWalletDetails().pipe(
      map((value: any) => {
        this.walletModel = value;

        for (var i = 0; i < this.walletModel.length; i++) {
          if (this.walletModel[i].type == "Cash") {
            this.cashWalletIndex = i;
            break;
          }
        }

        return this.walletModel; 
      }),
      catchError((error: any) => {
        if (error.status === 404) {
          
          this.walletModel = []; // 
          return throwError("No wallet data.");
        }
       
        return throwError("An error occurred while fetching wallet data");
      })
    ).subscribe((data: any) => {

      this.availAmount = data[this.cashWalletIndex].availableAmount.toFixed(2);
      this.walletService.availAmount = this.availAmount;
      this.displayAmount = parseFloat(this.availAmount);

      this.accountWallet = data[this.cashWalletIndex].acctNumber;
      this.walletService.accountWallet = this.accountWallet;
      
      this.uomType = data[this.cashWalletIndex].uom;

    });

    this.walletDetailsService.getBankDetails().pipe(
      map((value: any) => {
        this.walletModel = value;

        return this.walletModel;
      })
    ).subscribe((data: any) => {
      console.log("Bank Details:", data);

      if (data.length === 0) {
        this.bankName = 'No Bank Available';
        this.bankCode = '';
        this.displayAccount = 'No Account Number';
        this.availAmount = "RM0.00";
      } else {
        this.bankName = data[0].bankName;
        this.walletService.bankName = this.bankName;

        this.bankCode = data[0].bankCode;
        this.walletService.bankCode = this.bankCode;

        this.accountBank = data[0].accountNumber;
        this.walletService.accountBank = this.accountBank;

        this.displayAccount = this.accountBank;
      }
    });

    this.user.getUserDetails().subscribe(data => {

      console.log("User Details:", data);

      this.email = data.email;
      this.walletService.email = this.email;

      this.username = data.userName;
      this.walletService.username = this.username;

      this.fullName = data.fullName;
      this.walletService.fullName = this.fullName;

      this.tierCode = data.tierStatus.code;
      this.walletService.tierCode = this.tierCode;

      this.statusTier = data.userTier.code;
      this.walletService.statusTier = this.statusTier;

      this.tierName = data.userTier.name;
      this.walletService.tierName = this.tierName;

    });

    this.selectedOptionChanges.subscribe(() => {
      this.walletDetailsService.getTransactionHistories(this.selectedOption, 'All').subscribe(data => {
        const filteredTransactions = data.flatMap(group =>
          group.filter(trans =>
            (trans.transaction_code === 'F01' || trans.transaction_code === 'F02') 
          )
        );

        console.log("Transaction ", filteredTransactions);

        if (this.selectedOption !== 'All') {
          const groupedTransactions = {};

          filteredTransactions.forEach(trans => {
            const referenceId = trans.reference_id;

            if (!groupedTransactions[referenceId]) {
              groupedTransactions[referenceId] = [];
            }

            trans.transaction = trans.transaction.replace('TopUp', 'Cash Top Up');
            trans.transaction = trans.transaction.replace('Withdraw', 'Cash Withdrawal');
            trans.status = trans.status.replace('Completed', 'Successful');
            trans.status = trans.status.replace('Reject', 'Unsuccessful');
            trans.status = trans.status.replace('Failed', 'Cancelled');

            if (trans.amount < 0) {
              trans.amount = Math.abs(trans.amount);
              trans.uom = trans.uom.replace('RM', '-RM');
            }

            groupedTransactions[referenceId].push(trans);
          });

          console.log("Grouped Transactions", groupedTransactions);

          this.dropdownData = Object.values(groupedTransactions);
          console.log("Transaction", this.dropdownData);
          this.totalItemsCount = this.dropdownData.length;
          this.displayData = this.dropdownData.slice(0, this.pageSize);

          console.log("Display All:", this.displayData);
        } else {
          const groupedTransactions = {};

          filteredTransactions.forEach(trans => {
            const referenceId = trans.reference_id;

            if (!groupedTransactions[referenceId]) {
              groupedTransactions[referenceId] = [];
            }

            trans.transaction = trans.transaction.replace('TopUp', 'Cash Top Up');
            trans.transaction = trans.transaction.replace('Withdraw', 'Cash Withdrawal');
            trans.status = trans.status.replace('Completed', 'Successful');
            trans.status = trans.status.replace('Reject', 'Unsuccessful');
            trans.status = trans.status.replace('Failed', 'Cancelled');

            if (trans.amount < 0) {
              trans.amount = Math.abs(trans.amount);
              trans.uom = trans.uom.replace('RM', '-RM');
            }

            groupedTransactions[referenceId].push(trans);
          });

          console.log("Grouped Transactions", groupedTransactions);

          this.dropdownData = Object.values(groupedTransactions);
          console.log("Transaction", this.dropdownData);
          this.totalItemsCount = this.dropdownData.length;
          this.displayData = this.dropdownData.slice(0, this.pageSize);

          console.log("Display All:", this.displayData);
        }
      });
    });


    // Load initial page
    this.loadPage(0);

  }

  hideDigits(displayAccount: string): string {
    const hiddenDigits = displayAccount.slice(0, -4).replace(/\d/g, '*');
    const visibleDigits = displayAccount.slice(-4);
    return hiddenDigits + visibleDigits;
  }

  tierButton() {
    this._router.navigate(['/tier']);
  }

  private get selectedOptionChanges(): Observable<string> {
    return this._selectedOptionChanges.asObservable();
  }

  private _selectedOptionChanges = new BehaviorSubject<string>('all');

  onOptionChange(option: string) {
    this._selectedOptionChanges.next(option);
  }

  onPageChange(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.currentPageIndex = event.pageIndex;

    this.loadPage(event.pageIndex);
  }

  loadPage(pageIndex: number) {
    
    const startIndex = pageIndex * this.pageSize;
    const endIndex = startIndex + this.pageSize;

    this.displayData = this.dropdownData.slice(startIndex, endIndex);
  }

  topUp(): void {

    if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
      this._router.navigate(['/cash-wallet-topup']);
    } else {
      this._router.navigate(['/upgrade-account']);
    }
  }

  withDraw(): void {
    if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
      this._router.navigate(['/cash-wallet-withdraw']);
    } else {
      this._router.navigate(['/upgrade-account']);
    }
  }

  getDetails(referenceId, transactionTypeCode) {

    console.log("ReferenceId:", referenceId);
    console.log("transaction:", transactionTypeCode);

    this.bgdService.downloadReceipt(referenceId, transactionTypeCode).subscribe(
      (response: any) => {
        const blob = new Blob([response], { type: 'application/pdf' });

        const url = URL.createObjectURL(blob);
        window.open(url, '_blank');
      },
      (error: any) => {
        
        console.error('Error downloading receipt:', error);
      }
    );
  }

}

