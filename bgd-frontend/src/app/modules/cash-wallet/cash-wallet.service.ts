import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable, tap } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CashWalletService {

    private _data: BehaviorSubject<any> = new BehaviorSubject(null);

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Getter for data
     */
    get data$(): Observable<any> {
        return this._data.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get data
     */
    getData(): Observable<any> {
        return this._httpClient.get('api/dashboards/finance').pipe(
            tap((response: any) => {
                this._data.next(response);
            })
        );
    }

    getWalletDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/walletDetails');
    }

    getUserDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getUserDetails');
    }

    getinitTopup(acctNumber: string, amount: string, transactionTypeCode: string, walletTypeCode: string, channelCode: string, currency: string, customerName: string): Observable<any> {
        let params = new HttpParams()
            .set('acctNumber', acctNumber)
            .set('amount', amount)
            .set('transactionTypeCode', transactionTypeCode)
            .set('walletTypeCode', walletTypeCode)
            .set('channelCode', channelCode)
            .set('currency', currency)
            .set('customerName', customerName);
        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/initTopup', { params: params });
    }

    topupWallet(requestBody: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._httpClient.put(environment.apiUrl + '/api/wallet/topupWallet', requestBody, { headers });
    }

    topupStatus(endToEndId: string, endToEndIdSignature: string): Observable<any> {
        let params = new HttpParams()
            .set('endToEndId', endToEndId)
            .set('endToEndIdSignature', endToEndIdSignature)
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/topupStatus', { params: params });
    }

    getinitWithdraw(acctNumber: string, amount: string, transactionTypeCode: string, walletTypeCode: string): Observable<any> {

        let params = new HttpParams()
            .set('acctNumber', acctNumber)
            .set('amount', amount)
            .set('transactionTypeCode', transactionTypeCode)
            .set('walletTypeCode', walletTypeCode);

        return this._httpClient.get<any>(environment.apiUrl + '/api/administration/initWithdraw', { params: params });
    }

    withdrawWallet(requestBody: string): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._httpClient.put(environment.apiUrl + '/api/wallet/withdrawWallet', requestBody, { headers });
    }

    getBankDetails(): Observable<any> {
        return this._httpClient.get(environment.apiUrl + '/api/onboarding/getBankDetails');
    }

    getValidateWallet(): Observable<any> {
        return this._httpClient.get(environment.apiUrl + '/api/wallet/validateWallet');
    }

    verifyPin(pin: { userName: string; email: string; userPin: string; }): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/verifyPin', pin);
    }

    getTransactionList(): Observable<any> {
        return this._httpClient.get(environment.apiUrl + '/api/wallet/transactionList');
    }

    getTransactionHistories(days: string, transactionCode: string): Observable<any> {
        const params = { days, transactionCode };
        return this._httpClient.get(environment.apiUrl + '/api/wallet/transactionHistories', { params });
    }

    getTransactionDetails(referenceId: any, transactionTypeCode: any): Observable<any> {
        const params = { referenceId, transactionTypeCode };
        return this._httpClient.get(environment.apiUrl + '/api/wallet/transactionDetails', { params });
    }

    getbursaCostRecovery(): Observable<any> {
        return this._httpClient.get(environment.apiUrl + '/api/administration/bursaCostRecovery');
    }

    getbursaRevenue(): Observable<any> {
        return this._httpClient.get(environment.apiUrl + '/api/administration/bursaRevenue');
    }

}
