import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WalletService {

  tierCode: any;
  tierName: any;
  statusTier: any;
  availAmount: any;
  accountWallet: any;
  accountBank: any;
  bankName: any;
  bankCode: any;
  fullName: any;
  email: any;
  username: any;

  initTopup: any;
  initWithdraw: any;
  topupResponse: any;
  withdrawResponse: any;
  bankCodeTopup: any;

  constructor() { }
}
