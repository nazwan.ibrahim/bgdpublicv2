import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CashWalletRoutingModule } from './cash-wallet-routing.module';
import { CashWalletComponent } from './cash-wallet.component';
import { SharedModule } from 'app/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    CashWalletComponent
  ],
  imports: [
    CommonModule,
    CashWalletRoutingModule,
    SharedModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule
  ]
})
export class CashWalletModule { }
