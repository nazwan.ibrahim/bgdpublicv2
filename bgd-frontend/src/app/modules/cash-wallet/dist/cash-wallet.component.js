"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.CashWalletComponent = void 0;
var core_1 = require("@angular/core");
var animations_1 = require("@bgd/animations");
var operators_1 = require("rxjs/operators");
var paginator_1 = require("@angular/material/paginator");
var rxjs_1 = require("rxjs");
var CashWalletComponent = /** @class */ (function () {
    function CashWalletComponent(walletDetailsService, bgdService, user, _activatedRoute, _router) {
        this.walletDetailsService = walletDetailsService;
        this.bgdService = bgdService;
        this.user = user;
        this._activatedRoute = _activatedRoute;
        this._router = _router;
        this.alert = {
            type: 'success',
            message: ''
        };
        this.today = new Date();
        this.walletDetails = [];
        this.dropdownData = [];
        this.displayData = [];
        this.cashWalletIndex = -1;
        this.transIndex = -1;
        this.pageSize = 5;
        this.currentPageIndex = 0;
        this.selectedOption = 'All';
        this.transaction = '';
        this.displayAccount = '';
        this._selectedOptionChanges = new rxjs_1.BehaviorSubject('all');
    }
    CashWalletComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.walletDetailsService.getWalletDetails().pipe(operators_1.map(function (value) {
            _this.walletModel = value;
            console.log("WalletModel", _this.walletModel);
            for (var i = 0; i < _this.walletModel.length; i++) {
                if (_this.walletModel[i].type == "Cash") {
                    _this.cashWalletIndex = i;
                    break;
                }
            }
            return _this.walletModel; // return the modified walletModel
        }), operators_1.catchError(function (error) {
            if (error.status === 404) {
                // Handle 404 error here, such as setting default values
                _this.walletModel = []; // Set an empty array or any default value
                return rxjs_1.throwError("No wallet data.");
            }
            // Handle other errors if needed
            return rxjs_1.throwError("An error occurred while fetching wallet data");
        })).subscribe(function (data) {
            // access the data returned by the map operator
            _this.availAmount = data[_this.cashWalletIndex].availableAmount.toFixed(2);
            _this.displayAmount = parseFloat(_this.availAmount);
            console.log(_this.availAmount);
            _this.accountNum = data[_this.cashWalletIndex].acctNumber;
            _this.uomType = data[_this.cashWalletIndex].uom;
        });
        this.walletDetailsService.getBankDetails().pipe(operators_1.map(function (value) {
            _this.walletModel = value;
            return _this.walletModel;
        })).subscribe(function (data) {
            if (data.length === 0) {
                _this.bankName = 'No Bank Available';
                _this.bankCode = '';
                _this.accountNumber = 'No Account Number';
                _this.availAmount = "RM0.00";
            }
            else {
                _this.bankName = data[0].bankName;
                _this.bankCode = data[0].bankCode;
                _this.accountNumber = data[0].accountNumber;
                _this.displayAccount = _this.accountNumber;
            }
        });
        this.user.getUserDetails().subscribe(function (data) {
            _this.tierCode = data.tierStatus.code;
            _this.statusTier = data.userTier.code;
            _this.tierName = data.userTier.name;
            console.log("Details ", data);
            console.log("Tier V ", _this.tierCode);
            console.log("STatus ", _this.statusTier);
            console.log("Tier Name ", _this.tierName);
            /* return this.statusCode; */
        });
        this.selectedOptionChanges.subscribe(function () {
            _this.walletDetailsService.getTransactionHistories(_this.selectedOption, 'All')
                .subscribe(function (data) {
                var filteredTransactions = data.flatMap(function (group) { return group.filter(function (trans) { return trans.transaction_code === 'F01' || trans.transaction_code === 'F02'; }); });
                console.log("Transaction ", filteredTransactions);
                if (_this.selectedOption !== 'All') {
                    var groupedTransactions_1 = {};
                    filteredTransactions.forEach(function (trans) {
                        var referenceId = trans.reference_id;
                        if (!groupedTransactions_1[referenceId]) {
                            groupedTransactions_1[referenceId] = [];
                        }
                        trans.transaction = trans.transaction.replace('TopUp', 'Cash Top Up');
                        trans.transaction = trans.transaction.replace('Withdraw', 'Cash Withdrawal');
                        trans.status = trans.status.replace('Completed', 'Successful');
                        trans.status = trans.status.replace('Reject', 'Unsuccessful');
                        if (trans.amount < 0) {
                            trans.amount = Math.abs(trans.amount);
                            trans.uom = trans.uom.replace('RM', '-RM');
                        }
                        groupedTransactions_1[referenceId].push(trans);
                    });
                    console.log("Grouped Transactions", groupedTransactions_1);
                    _this.dropdownData = Object.values(groupedTransactions_1);
                    console.log("Transaction", _this.dropdownData);
                    _this.totalItemsCount = _this.dropdownData.length;
                    _this.displayData = _this.dropdownData.slice(0, _this.pageSize);
                    console.log("Display All:", _this.displayData);
                }
                else {
                    var groupedTransactions_2 = {};
                    filteredTransactions.forEach(function (trans) {
                        var referenceId = trans.reference_id;
                        if (!groupedTransactions_2[referenceId]) {
                            groupedTransactions_2[referenceId] = [];
                        }
                        trans.transaction = trans.transaction.replace('TopUp', 'Cash Top Up');
                        trans.transaction = trans.transaction.replace('Withdraw', 'Cash Withdrawal');
                        trans.status = trans.status.replace('Completed', 'Successful');
                        trans.status = trans.status.replace('Reject', 'Unsuccessful');
                        if (trans.amount < 0) {
                            trans.amount = Math.abs(trans.amount);
                            trans.uom = trans.uom.replace('RM', '-RM');
                        }
                        groupedTransactions_2[referenceId].push(trans);
                    });
                    console.log("Grouped Transactions", groupedTransactions_2);
                    _this.dropdownData = Object.values(groupedTransactions_2);
                    console.log("Transaction", _this.dropdownData);
                    _this.totalItemsCount = _this.dropdownData.length;
                    _this.displayData = _this.dropdownData.slice(0, _this.pageSize);
                    console.log("Display All:", _this.displayData);
                }
            });
        });
        // Load initial page
        this.loadPage(0);
    };
    CashWalletComponent.prototype.hideDigits = function (displayAccount) {
        var hiddenDigits = displayAccount.slice(0, -4).replace(/\d/g, '*');
        var visibleDigits = displayAccount.slice(-4);
        return hiddenDigits + visibleDigits;
    };
    CashWalletComponent.prototype.tierButton = function () {
        var statusTier = this.statusTier;
        this._router.navigate(['/tier'], {
            queryParams: {
                statusTier: encodeURIComponent(statusTier)
            }
        });
    };
    Object.defineProperty(CashWalletComponent.prototype, "selectedOptionChanges", {
        get: function () {
            return this._selectedOptionChanges.asObservable();
        },
        enumerable: false,
        configurable: true
    });
    CashWalletComponent.prototype.onOptionChange = function (option) {
        this._selectedOptionChanges.next(option);
    };
    CashWalletComponent.prototype.onPageChange = function (event) {
        this.pageSize = event.pageSize;
        this.currentPageIndex = event.pageIndex;
        // Load the selected page
        this.loadPage(event.pageIndex);
    };
    CashWalletComponent.prototype.loadPage = function (pageIndex) {
        // Calculate the range of items to display based on the selected page
        var startIndex = pageIndex * this.pageSize;
        var endIndex = startIndex + this.pageSize;
        // Set displayData to the range of items to display
        this.displayData = this.dropdownData.slice(startIndex, endIndex);
    };
    CashWalletComponent.prototype.topUp = function () {
        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/cash-wallet-topup'], {
                queryParams: {
                    availAmount: this.availAmount,
                    accountNum: this.accountNum,
                    accountNumber: this.accountNumber,
                    bankName: this.bankName
                }
            });
        }
        else {
            this._router.navigate(['/upgrade-account']);
        }
    };
    CashWalletComponent.prototype.withDraw = function () {
        if ((this.statusTier === "T01" || this.statusTier === "T02" || this.statusTier === "T03") && this.tierCode === "G09") {
            this._router.navigate(['/cash-wallet-withdraw'], {
                queryParams: {
                    availAmount: this.availAmount,
                    accountNum: this.accountNum,
                    accountNumber: this.accountNumber,
                    bankName: this.bankName
                }
            });
        }
        else {
            this._router.navigate(['/upgrade-account']);
        }
    };
    CashWalletComponent.prototype.getDetails = function (referenceId, transactionTypeCode) {
        console.log("ReferenceId:", referenceId);
        console.log("transaction:", transactionTypeCode);
        this.bgdService.downloadReceipt(referenceId, transactionTypeCode).subscribe(function (response) {
            console.log("Response:", response);
            // Handle the response
            var blob = new Blob([response], { type: 'application/pdf' });
            // Create a URL for the blob object
            var url = URL.createObjectURL(blob);
            // Open the PDF in a new window or tab for the user to view
            window.open(url, '_blank');
            // saveAs(blob, 'receipt.pdf');
        }, function (error) {
            // Handle any errors that occur during the API request
            console.error('Error downloading receipt:', error);
        });
        // this.walletDetailsService.getTransactionDetails(referenceId, transactionTypeCode)
        //   .subscribe(data => {
        //     this.details = data;
        //     console.log(this.details);
        //     if (transactionTypeCode === "F01") {
        //       this._router.navigate(['/transaction-history-details-topup'], { queryParams: { data: JSON.stringify(this.details) } })
        //     } else if (transactionTypeCode === "F02") {
        //       this._router.navigate(['/transaction-history-details-withdraw'], { queryParams: { data: JSON.stringify(this.details) } })
        //     } 
        //   })
    };
    __decorate([
        core_1.ViewChild(paginator_1.MatPaginator)
    ], CashWalletComponent.prototype, "paginator");
    CashWalletComponent = __decorate([
        core_1.Component({
            selector: 'bgd-cash-wallet',
            templateUrl: './cash-wallet.component.html',
            styleUrls: ['./cash-wallet.component.scss'],
            encapsulation: core_1.ViewEncapsulation.None,
            animations: animations_1.fuseAnimations
        })
    ], CashWalletComponent);
    return CashWalletComponent;
}());
exports.CashWalletComponent = CashWalletComponent;
