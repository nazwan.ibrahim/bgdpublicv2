import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CashWalletComponent } from './cash-wallet.component';
import { CashWalletResolver } from './cash-wallet.resolvers';

const routes: Routes = [
  {
    path: '',
    component: CashWalletComponent
    // resolve  : {
    //   data: CashWalletResolver
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CashWalletRoutingModule { }
