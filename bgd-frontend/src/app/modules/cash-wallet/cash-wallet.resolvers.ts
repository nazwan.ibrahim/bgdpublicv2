import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CashWalletService } from './cash-wallet.service';

@Injectable({
    providedIn: 'root'
})
export class CashWalletResolver implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(private _CashWalletService: CashWalletService)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param route
     * @param state
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>
    {
        return this._CashWalletService.getData();
    }
}
