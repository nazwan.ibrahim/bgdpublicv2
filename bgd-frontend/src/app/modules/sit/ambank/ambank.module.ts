import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmbankRoutingModule } from './ambank-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AmbankRoutingModule
  ]
})
export class AmbankModule { }
