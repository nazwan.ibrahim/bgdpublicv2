import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AmbankComponent } from './ambank.component';

const routes: Routes = [
  {
    path: '',
    component: AmbankComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmbankRoutingModule { }
