import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AmbankComponent } from './ambank.component';

describe('AmbankComponent', () => {
  let component: AmbankComponent;
  let fixture: ComponentFixture<AmbankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AmbankComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AmbankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
