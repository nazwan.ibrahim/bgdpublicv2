import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { BehaviorSubject, Observable, tap } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SitService {

    constructor(private _httpClient: HttpClient,
    ) { }
    //ace
    queryBuyPrice(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/queryBuyPrice')
    }
    querySellPrice(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/querySellPrice')
    }

    spotOrderBuy(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/querySellPrice')
    }

    spotOrderSell(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/querySellPrice')
    }

    aceRedeem(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/aceRedeem')
    }

    //ambank
    ambankAccountEnquiry(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/ambankAccountEnquiry')
    }
    ambankCreditTransfer(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/ambankCreditTransfer')
    }

    //paynet

    retrieveBankList(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/retrieveBankList')
    }

    initiatedPayment(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/wallet/initiatedPayment')
    }
}