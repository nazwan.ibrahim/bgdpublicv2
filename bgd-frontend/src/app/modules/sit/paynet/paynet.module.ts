import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaynetRoutingModule } from './paynet-routing.module';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        PaynetRoutingModule
    ]
})
export class PaynetModule { }
