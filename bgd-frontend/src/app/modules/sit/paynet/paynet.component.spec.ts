import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaynetComponent } from './paynet.component';

describe('PaynetComponent', () => {
  let component: PaynetComponent;
  let fixture: ComponentFixture<PaynetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaynetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PaynetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
