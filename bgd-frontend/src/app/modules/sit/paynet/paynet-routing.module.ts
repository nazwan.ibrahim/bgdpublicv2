import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaynetComponent } from './paynet.component';

const routes: Routes = [
  {
    path: '',
    component: PaynetComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaynetRoutingModule { }
