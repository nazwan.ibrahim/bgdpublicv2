import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoldWalletComponent } from './gold-wallet.component';

const routes: Routes = [
  {
    path: '',
    component: GoldWalletComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoldWalletRoutingModule { }
