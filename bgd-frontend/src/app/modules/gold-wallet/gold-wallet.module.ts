import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoldWalletRoutingModule } from './gold-wallet-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GoldWalletRoutingModule
  ]
})
export class GoldWalletModule { }
