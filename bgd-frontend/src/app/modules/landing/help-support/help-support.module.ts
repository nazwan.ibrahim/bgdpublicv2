import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelpSupportComponent } from './help-support.component';



@NgModule({
  declarations: [
    HelpSupportComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HelpSupportModule { }
