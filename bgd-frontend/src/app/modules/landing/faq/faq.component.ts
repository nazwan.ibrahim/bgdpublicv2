import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

const bgdIcon = `
<svg xmlns="https://www.w3.org/2000/svg" width="31" height="13" viewBox="0 0 31 13">
  <g id="Group_54384" data-name="Group 54384" transform="translate(0)">
    <text id="Bursa" transform="translate(12.379 7.641)" fill="#034ea1" font-size="3" font-family="Helvetica-BoldOblique, Helvetica" font-weight="700" font-style="oblique"><tspan x="0" y="0">BURSA</tspan></text>
    <text id="Gold_Dinar" data-name="Gold Dinar" transform="translate(11.985 11.129)" fill="#cd181e" font-size="3" font-family="Helvetica-BoldOblique, Helvetica" font-weight="700" font-style="oblique"><tspan x="0" y="0">GOLD DINAR</tspan></text>
    <g id="Group_53100" data-name="Group 53100" transform="translate(0 0)">
      <g id="Group_53067" data-name="Group 53067" transform="translate(1.839)">
        <path id="Path_48894" data-name="Path 48894" d="M-194.5,169.548c.668.653.68,1.86.159,3.309l1.6-.705c.447-1.537.324-2.868-.478-3.651-1.661-1.619-5.6-.276-8.791,3a14.842,14.842,0,0,0-2.464,3.314,19.77,19.77,0,0,1,1.431-1.636c3.3-3.387,7.13-5.015,8.548-3.632" transform="translate(204.475 -167.815)" fill="#cd181e"/>
      </g>
      <g id="Group_53068" data-name="Group 53068" transform="translate(0 5.62)">
        <path id="Path_48895" data-name="Path 48895" d="M-226.065,247.958c-.669-.652-.681-1.859-.158-3.309l-1.6.706c-.447,1.535-.324,2.868.478,3.651,1.661,1.618,5.6.274,8.791-3a14.936,14.936,0,0,0,2.465-3.315,20,20,0,0,1-1.432,1.635c-3.3,3.388-7.131,5.015-8.548,3.633" transform="translate(228.075 -242.69)" fill="#034ea1"/>
      </g>
      <g id="Group_53069" data-name="Group 53069" transform="translate(2.501 2.053)">
        <path id="Path_48896" data-name="Path 48896" d="M-194.082,204.232a1.9,1.9,0,0,1-1.4-.508h0c-1.114-1.122-.3-3.745,1.821-5.848a8.379,8.379,0,0,1,3.3-2.1,2.484,2.484,0,0,1,2.565.32c1.113,1.121.3,3.744-1.823,5.848A7.043,7.043,0,0,1-194.082,204.232Zm-1.34-.562c1.083,1.091,3.662.292,5.749-1.78s2.905-4.647,1.822-5.738a2.413,2.413,0,0,0-2.487-.3,8.3,8.3,0,0,0-3.264,2.08c-2.087,2.072-2.9,4.646-1.821,5.737Z" transform="translate(195.976 -195.584)" fill="#2b2523"/>
      </g>
      <g id="Group_53070" data-name="Group 53070" transform="translate(3.322 2.106)">
        <path id="Path_48897" data-name="Path 48897" d="M-179.207,195.873l-.4.4-.119.879-.559.52v.439l-.96.44-.958.08-.281.56-.6.4v.161l-.239.838-.64.88-.6.48-.879.878.719.081,1.6-1.878.4-1.359.959-.839,1.52-.381.32-.7.518-.52.161-1.039.4-.28Z" transform="translate(185.441 -195.873)" fill="#dc1223"/>
      </g>
      <g id="Group_53071" data-name="Group 53071" transform="translate(2.545 2.707)">
        <path id="Path_48898" data-name="Path 48898" d="M-188.61,205.3a1.838,1.838,0,0,0,1-.44c.358-.324.5-.463.544-.5a1.526,1.526,0,0,0-.142-.21c-.084.1-.3.364-.442.555-.18.239-.779.119-1.079.419a.686.686,0,0,0-.12.72,1.493,1.493,0,0,1-.6.239,1.687,1.687,0,0,0-.958.54c-.181.239-.241.479-.721.479a1.05,1.05,0,0,0-.9.36c-.24.239-.36.419-.659.779s-.12.959-.12,1.139a3.551,3.551,0,0,1-.3.779l-.82-.02h-.52a4.261,4.261,0,0,0-.977.232,1.81,1.81,0,0,0,.482,1.224c.827.834,2.5.588,4.182-.477l-.011-.1a4.214,4.214,0,0,1-1.4-.039c-.319-.16-.559-.16-.6-.4s.2-.559.32-.958-.16-.6-.08-.959.559-.759.719-1.039.8-.2,1.079-.239.239-.28.44-.481a3.693,3.693,0,0,0,.32-.359,3.87,3.87,0,0,0,1.239-.48.713.713,0,0,0,.119-.759" transform="translate(195.422 -204.143)" fill="#2c2b78"/>
      </g>
      <g id="Group_53072" data-name="Group 53072" transform="translate(10.654 2.644)">
        <line id="Line_157" data-name="Line 157" y2="3.079" fill="#2b2523"/>
      </g>
      <g id="Group_53073" data-name="Group 53073" transform="translate(10.635 2.644)">
        <rect id="Rectangle_13228" data-name="Rectangle 13228" width="0.039" height="3.079" fill="#2b2523"/>
      </g>
      <g id="Group_53074" data-name="Group 53074" transform="translate(9.734 2.043)">
        <line id="Line_158" data-name="Line 158" y2="5.358" fill="#2b2523"/>
      </g>
      <g id="Group_53075" data-name="Group 53075" transform="translate(9.715 2.043)">
        <rect id="Rectangle_13229" data-name="Rectangle 13229" width="0.039" height="5.358" transform="translate(0)" fill="#2b2523"/>
      </g>
      <g id="Group_53076" data-name="Group 53076" transform="translate(5.457 3.799)">
        <line id="Line_159" data-name="Line 159" x1="5.678" fill="#2b2523"/>
      </g>
      <g id="Group_53077" data-name="Group 53077" transform="translate(5.457 3.779)">
        <rect id="Rectangle_13230" data-name="Rectangle 13230" width="5.678" height="0.039" fill="#2b2523"/>
      </g>
      <g id="Group_53078" data-name="Group 53078" transform="translate(4.298 4.978)">
        <line id="Line_160" data-name="Line 160" x1="6.836" fill="#2b2523"/>
      </g>
      <g id="Group_53079" data-name="Group 53079" transform="translate(4.298 4.959)">
        <rect id="Rectangle_13231" data-name="Rectangle 13231" width="6.836" height="0.039" transform="translate(0)" fill="#2b2523"/>
      </g>
      <g id="Group_53080" data-name="Group 53080" transform="translate(3.539 5.96)">
        <line id="Line_161" data-name="Line 161" x1="7.037" fill="#2b2523"/>
      </g>
      <g id="Group_53081" data-name="Group 53081" transform="translate(3.539 5.941)">
        <rect id="Rectangle_13232" data-name="Rectangle 13232" width="7.037" height="0.039" fill="#2b2523"/>
      </g>
      <g id="Group_53082" data-name="Group 53082" transform="translate(2.899 7.1)">
        <line id="Line_162" data-name="Line 162" x1="6.997" fill="#2b2523"/>
      </g>
      <g id="Group_53083" data-name="Group 53083" transform="translate(2.899 7.081)">
        <rect id="Rectangle_13233" data-name="Rectangle 13233" width="6.997" height="0.039" fill="#2b2523"/>
      </g>
      <g id="Group_53084" data-name="Group 53084" transform="translate(2.58 8.121)">
        <line id="Line_163" data-name="Line 163" x1="6.477" fill="#2b2523"/>
      </g>
      <g id="Group_53085" data-name="Group 53085" transform="translate(2.58 8.102)">
        <rect id="Rectangle_13234" data-name="Rectangle 13234" width="6.477" height="0.039" fill="#2b2523"/>
      </g>
      <g id="Group_53086" data-name="Group 53086" transform="translate(2.58 9.3)">
        <line id="Line_164" data-name="Line 164" x1="5.078" fill="#2b2523"/>
      </g>
      <g id="Group_53087" data-name="Group 53087" transform="translate(2.58 9.281)">
        <rect id="Rectangle_13235" data-name="Rectangle 13235" width="5.078" height="0.039" fill="#2b2523"/>
      </g>
      <g id="Group_53088" data-name="Group 53088" transform="translate(8.456 2.186)">
        <line id="Line_165" data-name="Line 165" y2="6.558" fill="#2b2523"/>
      </g>
      <g id="Group_53089" data-name="Group 53089" transform="translate(8.436 2.186)">
        <rect id="Rectangle_13236" data-name="Rectangle 13236" width="0.039" height="6.558" fill="#2b2523"/>
      </g>
      <g id="Group_53090" data-name="Group 53090" transform="translate(7.616 2.528)">
        <line id="Line_166" data-name="Line 166" y2="6.877" fill="#2b2523"/>
      </g>
      <g id="Group_53091" data-name="Group 53091" transform="translate(7.597 2.528)">
        <rect id="Rectangle_13237" data-name="Rectangle 13237" width="0.039" height="6.877" fill="#2b2523"/>
      </g>
      <g id="Group_53092" data-name="Group 53092" transform="translate(6.378 3.107)">
        <line id="Line_167" data-name="Line 167" y2="6.876" fill="#2b2523"/>
      </g>
      <g id="Group_53093" data-name="Group 53093" transform="translate(6.358 3.107)">
        <rect id="Rectangle_13238" data-name="Rectangle 13238" width="0.039" height="6.876" transform="translate(0)" fill="#2b2523"/>
      </g>
      <g id="Group_53094" data-name="Group 53094" transform="translate(5.379 3.855)">
        <line id="Line_168" data-name="Line 168" y2="6.117" fill="#2b2523"/>
      </g>
      <g id="Group_53095" data-name="Group 53095" transform="translate(5.359 3.855)">
        <rect id="Rectangle_13239" data-name="Rectangle 13239" width="0.039" height="6.117" transform="translate(0)" fill="#2b2523"/>
      </g>
      <g id="Group_53096" data-name="Group 53096" transform="translate(4.14 5.12)">
        <line id="Line_169" data-name="Line 169" y2="4.838" fill="#2b2523"/>
      </g>
      <g id="Group_53097" data-name="Group 53097" transform="translate(4.12 5.12)">
        <rect id="Rectangle_13240" data-name="Rectangle 13240" width="0.039" height="4.838" transform="translate(0)" fill="#2b2523"/>
      </g>
      <g id="Group_53098" data-name="Group 53098" transform="translate(3.22 6.349)">
        <line id="Line_170" data-name="Line 170" y2="3.599" fill="#2b2523"/>
      </g>
      <g id="Group_53099" data-name="Group 53099" transform="translate(3.201 6.349)">
        <rect id="Rectangle_13241" data-name="Rectangle 13241" width="0.039" height="3.599" fill="#2b2523"/>
      </g>
    </g>
  </g>
</svg>
`;

@Component({
  selector: 'bgd-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})
export class FaqComponent {
  displayIFrame = false;
  iframeSrc: SafeResourceUrl = '';
  selectedTab: string;

  // constructor(private sanitizer: DomSanitizer, private _router: Router) {}
  constructor(
    iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private _router: Router
  ) {
    // Note that we provide the icon here as a string literal here due to a limitation in
    // Stackblitz. If you want to provide the icon from a URL, you can use:
    // `iconRegistry.addSvgIcon('thumbs-up', sanitizer.bypassSecurityTrustResourceUrl('icon.svg'));`
    // iconRegistry.addSvgIconLiteral(
    //     'bgdIcon',
    //     sanitizer.bypassSecurityTrustHtml(bgdIcon)
    // );

    const svgContent = '<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 2C6.486 2 2 6.486 2 12s4.486 10 10 10 10-4.486 10-10S17.514 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm-2-9h4v4h2v-4h4V9h-4V5h-2v4H8v2z"/></svg>';
    const trustedSvgContent = sanitizer.bypassSecurityTrustHtml(svgContent);

    iconRegistry.addSvgIconLiteral('bgdIcon', trustedSvgContent);
  }

  showIFrame() {
    const untrustedUrl = '/aboutgd'; // Replace with your untrusted URL
    const trustedUrl: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl(untrustedUrl);

    // Set the trusted URL to the iframe source
    this.iframeSrc = trustedUrl;

    // Display the iframe
    this.displayIFrame = true;
  }

  items = [
    'How do I add a payment method?',
    'How to change delivery address?',
    'How to upgrade my free account?',
  ];
  expandedIndex = 0;

  bgdFAQ = [
    'Why did Gold Dinar change or revamp its website?',
    'What are the improvements on the new website?',
    'How do I look for the information I want on the new website? How are the menus organised?',
    'Why did Bursa Malaysia change or revamp its website?',
    'What are the improvements on the new website?',
    'How do I look for the information I want on the new website? How are the menus organised?',
  ];
}
