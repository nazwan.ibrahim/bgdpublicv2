import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq.component';
import { FaqRoutes } from './faq.routing';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { MatInputModule } from '@angular/material/input';

@NgModule({
    declarations: [FaqComponent],
    imports: [
        RouterModule.forChild(FaqRoutes),
        CommonModule,
        MatFormFieldModule,
        MatButtonModule,
        MatIconModule,
        CdkAccordionModule,
        MatInputModule,
    ],
})
export class FaqModule {}
