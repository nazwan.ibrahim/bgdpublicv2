import { Route } from '@angular/router';
import { FaqComponent } from './faq.component';

export const FaqRoutes: Route[] = [
    {
        path     : '',
        component: FaqComponent
    }
];
