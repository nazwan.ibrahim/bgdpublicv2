/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@bgd/components/navigation';
import { Router, NavigationExtras } from '@angular/router';
import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

export const defaultNavigation: FuseNavigationItem[] = [
    // {
    //     id: 'dashboard',
    //     title: 'Dashboard',
    //     type: 'basic',
    //     icon: 'heroicons_outline:chart-pie',
    //     link: '/dashboard',
    // },
    // {
    //     id: 'example',
    //     title: 'Investment',
    //     type: 'collapsable',
    //     icon: 'heroicons_outline:chart-pie',
    //     // link : '/example',
    //     children: [
    //         {
    //             id: 'example',
    //             title: 'Ar-Rahnu',
    //             type: 'basic',
    //             icon: 'heroicons_outline:chart-pie',
    //             link: '/example',
    //         }
    //     ]
    // },
    // {
    //     id: 'example',
    //     title: 'Gift',
    //     type: 'collapsable',
    //     icon: 'heroicons_outline:chart-pie',
    //     // link : '/example',
    //     children: [
    //         {
    //             id: 'example',
    //             title: 'Send a Gifts',
    //             type: 'basic',
    //             icon: 'heroicons_outline:chart-pie',
    //             link: '/maintenance',
    //         }
    //     ]
    // },
    // {
    //     id: 'example',
    //     title: 'Bursa Store',
    //     type: 'collapsable',
    //     icon: 'heroicons_outline:chart-pie',
    //     // link : '/example',
    //     children: [
    //         {
    //             id: 'example',
    //             title: 'Buy BGD',
    //             type: 'basic',
    //             icon: 'heroicons_outline:chart-pie',
    //             link: '/buy-bgd',
    //         },
    //         {
    //             id: 'example',
    //             title: 'Sell BGD',
    //             type: 'basic',
    //             icon: 'heroicons_outline:chart-pie',
    //             link: '/sell-bgd',
    //         }
    //     ],

    // },
    {
        id: 'portfolio',
        title: 'Investor Portfolio',
        icon: 'heroicons_outline:home',
        type: 'basic',
        link: '/dashboard',
        function: () => {
            window.location.href = '/dashboard'; // Reload the current page
        },
    },
    {
        id: 'cashWallet',
        title: 'Cash Wallet',
        type: 'basic',
        icon: 'account_balance_wallet',
        link: '/cash-wallet',
        function: () => {
            window.location.href = '/cash-wallet'; // Reload the current page
        },
    },
    {
        id: 'transactionhistory',
        title: 'Transaction History',
        type: 'basic',
        icon: 'assignment',
        link: '/transaction-history',
        function: () => {
            window.location.href = '/transaction-history'; // Reload the current page
        },
    },
    {
        id: 'other',
        title: 'OTHERS',
        type: 'collapsable',
        //icon: 'heroicons_outline:chart-pie',
        //link : '/myaccount',
        children: [
            {
                id: 'example',
                title: 'My Account',
                type: 'basic',
                icon: 'heroicons_outline:user',
                link: '/myaccount',
            },
            {
                id: 'example',
                title: 'Help Center',
                type: 'basic',
                icon: 'help_outline',
                link: '/helpsupport',
            },
            {
                id: 'signout',
                title: 'Logout',
                type: 'basic',
                icon: 'heroicons_outline:logout',
                link: '/sign-out',
            },
            // {
            //     id: 'example',
            //     title: 'Edit Profile',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/editProfile',
            // },
            // {
            //     id: 'example',
            //     title: 'My Profile',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/myProfile',
            // },
            // {
            //     id: 'example',
            //     title: 'Help and Support',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/helpsupport',
            // },
            // {
            //     id: 'example',
            //     title: 'Account Bank Details',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/account-bankdetail',
            // },
            // {
            //     id: 'example',
            //     title: 'Settings',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/settings',
            // },
            // {
            //     id: 'example',
            //     title: 'About Gold Dinar',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/aboutgd',
            // },
            // {
            //     id: 'example',
            //     title: 'Terms and Conditions',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/termcondition',
            // },
            // {
            //     id: 'example',
            //     title: 'Privacy Notice',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/privacynotice',
            // }
        ],

    },
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [

    {
        id: 'portfolio',
        title: 'Investor Portfolio',
        type: 'basic',
        link: '/dashboard',
        function: () => {
            window.location.href = '/dashboard'; // Reload the current page
        },
    },
    {
        id: 'cash-wallet',
        title: 'Cash Wallet',
        type: 'basic',
        //icon: 'heroicons_outline:chart-pie',
        link: '/cash-wallet',
        function: () => {
            window.location.href = '/cash-wallet'; // Reload the current page
        },
    },
    {
        id: 'history',
        title: 'History',
        type: 'basic',
        //icon: 'heroicons_outline:chart-pie',
        link: '/transaction-history',
        function: () => {
            window.location.href = '/transaction-history'; // Reload the current page
        },
    },
    {
        id: 'notification',
        title: '',
        type: 'collapsable',
        icon: 'heroicons_outline:bell',
        //link: '/notification',
    },
    {
        id: 'example',
        title: '',
        type: 'collapsable',
        icon: 'heroicons_outline:menu',
        //link : '/myaccount',
        children: [
            {
                id: 'example',
                title: 'My Account',
                type: 'basic',
                icon: 'heroicons_outline:user',
                link: '/myaccount',
            },
            {
                id: 'example',
                title: 'Help',
                type: 'basic',
                icon: 'help_outline',
                link: '/helpsupport',
            },
            {
                id: 'example',
                title: 'Logout',
                type: 'basic',
                icon: 'heroicons_outline:logout',
                link: '/sign-out',
            },
            // {
            //     id: 'example',
            //     title: 'Edit Profile',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/editProfile',
            // },
            // {
            //     id: 'example',
            //     title: 'My Profile',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/myProfile',
            // },
            // {
            //     id: 'example',
            //     title: 'Help and Support',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/helpsupport',
            // },
            // {
            //     id: 'example',
            //     title: 'Account Bank Details',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/accountbankdetail',
            // },
            // {
            //     id: 'example',
            //     title: 'Settings',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/settings',
            // },
            // {
            //     id: 'example',
            //     title: 'About Gold Dinar',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/aboutgd',
            // },
            // {
            //     id: 'example',
            //     title: 'Terms and Conditions',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/termcondition',
            // },
            // {
            //     id: 'example',
            //     title: 'Privacy Notice',
            //     type: 'basic',
            //     icon: 'heroicons_outline:chart-pie',
            //     link: '/privacynotice',
            // }
        ],
    }
];
