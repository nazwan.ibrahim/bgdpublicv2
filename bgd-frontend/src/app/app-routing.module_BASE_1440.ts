import { AboutGoldDinarModule } from './modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, ExtraOptions } from '@angular/router';
import { InitialDataResolver } from './app.resolvers';
import { AuthGuard } from './core/auth/guards/auth.guard';
import { NoAuthGuard } from './core/auth/guards/noAuth.guard';
import { LayoutComponent } from './layout/layout.component';
import { Error404Component } from './modules/error/error-404/error-404.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/dashboard' },
 

  { path: 'signed-in-redirect', pathMatch: 'full', redirectTo: '/dashboard' },

  // Auth routes for guests
  {
    path: '',
    canActivate: [NoAuthGuard],
    canActivateChild: [NoAuthGuard],
    component: LayoutComponent,
    data: {
      layout: 'empty'
    },
    children: [
      { path: 'confirmation-required', loadChildren: () => import('app/modules/auth/confirmation-required/confirmation-required.module').then(m => m.AuthConfirmationRequiredModule) },
      { path: 'forgot-password', loadChildren: () => import('app/modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule) },
      { path: 'reset-password', loadChildren: () => import('app/modules/auth/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule) },
      { path: 'sign-in', loadChildren: () => import('app/modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule) },
      { path: 'sign-up', loadChildren: () => import('app/modules/auth/sign-up/sign-up.module').then(m => m.AuthSignUpModule) },
      { path: 'register', loadChildren: () => import('app/modules/auth/register/register.module').then(m => m.AuthRegisterModule) },
      { path: 'password', loadChildren: () => import('app/modules/auth/password/password.module').then(m => m.AuthPasswordModule) },
      { path: 'otp-phone', loadChildren: () => import('app/modules/auth/otp-phone/otp-phone.module').then(m => m.AuthOtpPhoneModule) },
      { path: 'get-started', loadChildren: () => import('app/modules/auth/get-started/get-started.module').then(m => m.GetStartedModule) },
    ]
  },

  // Auth routes for authenticated users
  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: LayoutComponent,
    data: {
      layout: 'empty'
    },
    children: [
      { path: 'sign-out', loadChildren: () => import('app/modules/auth/sign-out/sign-out.module').then(m => m.AuthSignOutModule) },
      { path: 'unlock-session', loadChildren: () => import('app/modules/auth/unlock-session/unlock-session.module').then(m => m.AuthUnlockSessionModule) },
      // {path: 'login-preference', loadChildren: () => import('app/modules/auth/login-preferences/login-preference.module').then(m => m.LoginPreferencesModule)}
    ]
  },

  // Landing routes
  {
    path: '',
    component: LayoutComponent,
    data: {
      layout: 'empty'
    },
    children: [
      { path: 'home', loadChildren: () => import('app/modules/landing/home/home.module').then(m => m.LandingHomeModule) },
      { path: 'help-support', loadChildren: () => import('app/modules/landing/help-support/help-support.module').then(m => m.HelpSupportModule) },
      { path: 'faq', loadChildren: () => import('app/modules/landing/faq/faq.module').then(m => m.FaqModule) },
    ]
  },

  // Admin routes

  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: LayoutComponent,
    resolve: {
      initialData: InitialDataResolver,
    },
    children: [
      { path: 'example', loadChildren: () => import('app/modules/admin/example/example.module').then(m => m.ExampleModule) },
      { path: 'maintenance', loadChildren: () => import('app/modules/admin/maintenance/maintenance.module').then(m => m.MaintenanceModule) },
      { path: 'referral', loadChildren: () => import('app/modules/admin/referral/referral.module').then(m => m.ReferralModule) },
      { path: 'user-profile', loadChildren: () => import('app/modules/admin/profile/user-profile/user-profile.module').then(m => m.UserProfileModule) },
      { path: 'edit-profile', loadChildren: () => import('app/modules/admin/profile/edit-profile/edit-profile.module').then(m => m.EditProfileModule) },
      { path: 'verify-mykad', loadChildren: () => import('app/modules/auth/verify-mykad/verify-mykad.module').then(m => m.VerifyMykadModule) },
      { path: 'transaction-history', loadChildren: () => import('app/modules/admin/transaction-history/transaction-history.module').then(m => m.TransactionHistoryModule) },
      //my-account
      { path: 'myaccount', loadChildren: () => import('app/modules/admin/my-account/my-account.module').then(m => m.MyAccountModule) },
      { path: 'aboutgd', loadChildren: () => import('app/modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.module').then(m => m.AboutGoldDinarModule) },
      { path: 'accountbankdetail', loadChildren: () => import('app/modules/admin/my-account/cards/account-bankdetails/account-bankdetails.module').then(m => m.AccountBankDetailsModule) },
      { path: 'helpsupport', loadChildren: () => import('app/modules/admin/my-account/cards/help-support/help-support.module').then(m => m.HelpSupportModule) },
      { path: 'privacynotice', loadChildren: () => import('app/modules/admin/my-account/cards/privacy-notices/privacy-notices.module').then(m => m.PrivacyNoticesModule) },
      { path: 'settings', loadChildren: () => import('app/modules/admin/my-account/cards/settings/settings.module').then(m => m.SettingsModule) },
      { path: 'termcondition', loadChildren: () => import('app/modules/admin/my-account/cards/term-conditions/term-conditions.module').then(m => m.TermConditionsModule) },
      
    ]
  },

    // Dashboard
  {
    path: 'dashboard',
    loadChildren: () => import('./modules/dashboard/dashboard.module').then(x => x.DashboardModule),
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: LayoutComponent,
    resolve: {
      initialData: InitialDataResolver,
    },
  },

  // Cash Wallet
  {
    path: 'cash-wallet',
    loadChildren: () => import('./modules/cash-wallet/cash-wallet.module').then(x => x.CashWalletModule),
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: LayoutComponent,
    resolve: {
      initialData: InitialDataResolver,
    },
  },

  { path: '**', component: Error404Component },

  { path: 'NotFound', component: Error404Component },

];

const config: ExtraOptions = {
  useHash: false,
  scrollPositionRestoration: 'enabled',
  enableTracing: false,
  paramsInheritanceStrategy: 'always',
  anchorScrolling: 'enabled',
  preloadingStrategy: PreloadAllModules,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }