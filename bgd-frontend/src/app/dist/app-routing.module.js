"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppRoutingModule = void 0;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var app_resolvers_1 = require("./app.resolvers");
var auth_guard_1 = require("./core/auth/guards/auth.guard");
var layout_component_1 = require("./layout/layout.component");
var error_404_component_1 = require("./modules/error/error-404/error-404.component");
var routes = [
    { path: '', pathMatch: 'full', redirectTo: '/sign-in' },
    { path: 'signed-in-redirect', pathMatch: 'full', redirectTo: '/dashboard' },
    // Auth routes for guests
    {
        path: '',
        //canActivate: [NoAuthGuard],
        //canActivateChild: [NoAuthGuard],
        component: layout_component_1.LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            { path: 'confirmation-required', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/confirmation-required/confirmation-required.module'); }).then(function (m) { return m.AuthConfirmationRequiredModule; }); } },
            { path: 'verified-email', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/register/verified-email/verified-email.module'); }).then(function (m) { return m.VerifiedEmailModule; }); } },
            { path: 'forgot-password', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-password/forgot-password.module'); }).then(function (m) { return m.AuthForgotPasswordModule; }); } },
            { path: 'reset-password', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/reset-password/reset-password.module'); }).then(function (m) { return m.AuthResetPasswordModule; }); } },
            { path: 'sign-in', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/sign-in/sign-in.module'); }).then(function (m) { return m.AuthSignInModule; }); } },
            { path: 'sign-up', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/sign-up/sign-up.module'); }).then(function (m) { return m.AuthSignUpModule; }); } },
            { path: 'register', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/register/register.module'); }).then(function (m) { return m.AuthRegisterModule; }); } },
            { path: 'investor-type', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/investor-type/investor-type.module'); }).then(function (m) { return m.InvestorTypeModule; }); } },
            { path: 'password', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/password/password.module'); }).then(function (m) { return m.AuthPasswordModule; }); } },
            { path: 'register/otp-phone', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/register/otp-phone/otp-phone.module'); }).then(function (m) { return m.OtpPhoneModule; }); } },
            { path: 'get-started', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/get-started/get-started.module'); }).then(function (m) { return m.GetStartedModule; }); } },
            { path: 'otp-phone', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/register/otp-phone/otp-phone.module'); }).then(function (m) { return m.OtpPhoneModule; }); } },
            { path: 'forgot-password-question', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-password-question/forgot-password-question.module'); }).then(function (m) { return m.ForgotPasswordQuestionModule; }); } },
            { path: 'forgot-password-email', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-password-email/forgot-password-email.module'); }).then(function (m) { return m.ForgotPasswordEmailModule; }); } },
            { path: 'forgot-password-email-failed', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-password-email failed/forgot-password-email-failed.module'); }).then(function (m) { return m.ForgotPasswordEmailFailedModule; }); } },
            { path: 'create-account', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/register/create-account/create-account.module'); }).then(function (m) { return m.CreateAccountdModule; }); } },
            { path: 'security-questions', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/register/security-questions/security-questions.module'); }).then(function (m) { return m.securityQuestionsModule; }); } },
        ]
    },
    // Auth routes for authenticated users
    {
        path: '',
        canActivate: [auth_guard_1.AuthGuard],
        canActivateChild: [auth_guard_1.AuthGuard],
        component: layout_component_1.LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            { path: 'sign-out', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/sign-out/sign-out.module'); }).then(function (m) { return m.AuthSignOutModule; }); } },
            { path: 'unlock-session', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/unlock-session/unlock-session.module'); }).then(function (m) { return m.AuthUnlockSessionModule; }); } },
        ]
    },
    // Landing routes
    {
        path: '',
        component: layout_component_1.LayoutComponent,
        data: {
            layout: 'empty'
        },
        children: [
            { path: 'home', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/landing/home/home.module'); }).then(function (m) { return m.LandingHomeModule; }); } },
            { path: 'help-support', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/landing/help-support/help-support.module'); }).then(function (m) { return m.HelpSupportModule; }); } },
        ]
    },
    // Admin routes
    {
        path: '',
        canActivate: [auth_guard_1.AuthGuard],
        canActivateChild: [auth_guard_1.AuthGuard],
        component: layout_component_1.LayoutComponent,
        resolve: {
            initialData: app_resolvers_1.InitialDataResolver
        },
        children: [
            { path: 'example', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/example/example.module'); }).then(function (m) { return m.ExampleModule; }); } },
            { path: 'maintenance', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/maintenance/maintenance.module'); }).then(function (m) { return m.MaintenanceModule; }); } },
            { path: 'referral', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/referral/referral.module'); }).then(function (m) { return m.ReferralModule; }); } },
            { path: 'user-profile', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/profile/user-profile/user-profile.module'); }).then(function (m) { return m.UserProfileModule; }); } },
            //{ path: 'edit-profile', loadChildren: () => import('app/modules/admin/profile/edit-profile/edit-profile.module').then(m => m.EditProfileModule) },
            { path: 'verify-mykad', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/verify-mykad.module'); }).then(function (m) { return m.VerifyMykadModule; }); } },
            { path: 'upgrade-account', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/upgrade-account/upgrade-account.module'); }).then(function (m) { return m.UpgradeAccountModule; }); } },
            { path: 'identity-verification', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/identity-verification/identity-verification.module'); }).then(function (m) { return m.IdentityVerificationModule; }); } },
            { path: 'transaction-history', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history/transaction-history.module'); }).then(function (m) { return m.TransactionHistoryModule; }); } },
            { path: 'transaction-history-details', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history-details/transaction-history-details.module'); }).then(function (m) { return m.TransactionHistoryDetailsModule; }); } },
            { path: 'transaction-history-details-topup', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history-details/transaction-history-details-topup/transaction-history-details-topup.module'); }).then(function (m) { return m.TransactionHistoryDetailsTopupModule; }); } },
            { path: 'transaction-history-details-withdraw', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history-details/transaction-history-details-withdraw/transaction-history-details-withdraw.module'); }).then(function (m) { return m.TransactionHistoryDetailsWithdrawModule; }); } },
            { path: 'transaction-history-details-transfer', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history-details/transaction-history-details-transfer/transaction-history-details-transfer.module'); }).then(function (m) { return m.TransactionHistoryDetailsTransferModule; }); } },
            { path: 'transaction-history-details-redeem', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history-details/transaction-history-details-redeem/transaction-history-details-redeem.module'); }).then(function (m) { return m.TransactionHistoryDetailsRedeemModule; }); } },
            { path: 'transaction-history-details-buy-bgd', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history-details/transaction-history-details-buy-bgd/transaction-history-details-buy-bgd.module'); }).then(function (m) { return m.TransactionHistoryDetailsBuyBgdModule; }); } },
            { path: 'transaction-history-details-sell-bgd', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/transaction-history-details/transaction-history-details-sell-bgd/transaction-history-details-sell-bgd.module'); }).then(function (m) { return m.TransactionHistoryDetailsSellBgdModule; }); } },
            //upgrade account
            { path: 'upgrade-account', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/upgrade-account/upgrade-account.module'); }).then(function (m) { return m.UpgradeAccountModule; }); } },
            { path: 'identity-verification', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/identity-verification/identity-verification.module'); }).then(function (m) { return m.IdentityVerificationModule; }); } },
            { path: 'camera-unavailable', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/camera-unavailable/camera-unavailable.module'); }).then(function (m) { return m.CameraUnavailableModule; }); } },
            { path: 'front-id', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/front-id/front-id.module'); }).then(function (m) { return m.FrontIdModule; }); } },
            { path: 'front-id-image', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/front-id-image/front-id-image.module'); }).then(function (m) { return m.FrontIdImageModule; }); } },
            { path: 'back-id', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/back-id/back-id.module'); }).then(function (m) { return m.BackIdModule; }); } },
            { path: 'back-id-image', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/back-id-image/back-id-image.module'); }).then(function (m) { return m.BackIdImageModule; }); } },
            { path: 'selfie', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/selfie/selfie.module'); }).then(function (m) { return m.SelfieModule; }); } },
            { path: 'selfie-image', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/selfie-image/selfie-image.module'); }).then(function (m) { return m.SelfieImageModule; }); } },
            { path: 'update-info', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/update-info/update-info.module'); }).then(function (m) { return m.UpdateInfoModule; }); } },
            { path: 'verification', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/verification/verification.module'); }).then(function (m) { return m.VerificationModule; }); } },
            { path: 'verify-failed', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/verify-failed/verify-failed.module'); }).then(function (m) { return m.VerifyFailedModule; }); } },
            { path: 'verify-success', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/verify-mykad/verify-success/verify-success.module'); }).then(function (m) { return m.VerifySuccessModule; }); } },
            //my-account
            { path: 'myaccount', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/my-account.module'); }).then(function (m) { return m.MyAccountModule; }); } },
            { path: 'editProfile', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/edit-profile/edit-profile.module'); }).then(function (m) { return m.EditProfileModule; }); } },
            //{ path: 'myProfile', loadChildren: () => import('app/modules/admin/my-account/cards/user-profile/user-profile.module').then(m => m.AboutGoldDinarModule) },
            { path: 'aboutgd', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.module'); }).then(function (m) { return m.AboutGoldDinarModule; }); } },
            { path: 'account-bankdetail', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/account-bankdetails/account-bankdetails.module'); }).then(function (m) { return m.AccountBankDetailsModule; }); } },
            { path: 'edit-account-bankdetails', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/edit-account-bankdetails/edit-account-bankdetails.module'); }).then(function (m) { return m.EditAccountBankDetailsModule; }); } },
            { path: 'helpsupport', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/help-support/help-support.module'); }).then(function (m) { return m.HelpSupportModule; }); } },
            { path: 'privacynotice', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/privacy-notices/privacy-notices.module'); }).then(function (m) { return m.PrivacyNoticesModule; }); } },
            { path: 'settings', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/settings/settings.module'); }).then(function (m) { return m.SettingsModule; }); } },
            { path: 'select-language', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/settings/select-language/select-language.module'); }).then(function (m) { return m.SelectLanguageModule; }); } },
            { path: 'security', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/security/security.module'); }).then(function (m) { return m.SecurityModule; }); } },
            { path: 'change-password', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/change-password/change-password.module'); }).then(function (m) { return m.ChangePasswordModule; }); } },
            { path: 'change-password-new', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/change-password-new/change-password-new.module'); }).then(function (m) { return m.ChangePasswordNewModule; }); } },
            { path: 'change-password-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/change-password-pin/change-password-pin.module'); }).then(function (m) { return m.ChangePasswordPinModule; }); } },
            { path: 'change-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/change-pin/change-pin.module'); }).then(function (m) { return m.ChangePinModule; }); } },
            { path: 'change-pin-new', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/change-pin-new/change-pin-new.module'); }).then(function (m) { return m.ChangePinNewModule; }); } },
            { path: 'change-pin-confirm', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/change-pin-confirm/change-pin-confirm.module'); }).then(function (m) { return m.ChangePinConfirmModule; }); } },
            { path: 'forgot-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-pin/forgot-pin.module'); }).then(function (m) { return m.ForgotPinModule; }); } },
            { path: 'forgot-pin-otp', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-pin/forgot-pin-otp/forgot-pin-otp.module'); }).then(function (m) { return m.ForgotPinOtpModule; }); } },
            { path: 'forgot-pin-new', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-pin/forgot-pin-new/forgot-pin-new.module'); }).then(function (m) { return m.ForgotPinNewModule; }); } },
            { path: 'forgot-pin-confirm', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/forgot-pin/forgot-pin-confirm/forgot-pin-confirm.module'); }).then(function (m) { return m.ForgotPinConfirmModule; }); } },
            { path: 'termcondition', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/term-conditions/term-conditions.module'); }).then(function (m) { return m.TermConditionsModule; }); } },
            { path: 'editProfilePic', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/edit-profile/edit-profile-picture/edit-profile-picture.module'); }).then(function (m) { return m.EditProfilePictureModule; }); } },
            { path: 'uploadProfilePic', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/admin/my-account/cards/edit-profile/upload-profile-picture/upload-profile-picture.module'); }).then(function (m) { return m.UploadProfilePictureModule; }); } },
            { path: 'setup-pin-landing', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/setup-pin-landing/setup-pin-landing.module'); }).then(function (m) { return m.SetupPinLandingModule; }); } },
            { path: 'setup-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/setup-pin/setup-pin.module'); }).then(function (m) { return m.SetupPinModule; }); } },
            { path: 'setup-pin-otp', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/setup-pin-otp/setup-pin-otp.module'); }).then(function (m) { return m.SetupPinOtpModule; }); } },
            { path: 'transaction-limit', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/transaction-limit/transaction-limit.module'); }).then(function (m) { return m.TransactionLimitModule; }); } },
            //cash wallet
            { path: 'cash-wallet', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/cash-wallet/cash-wallet.module'); }).then(function (x) { return x.CashWalletModule; }); } },
            { path: 'cash-wallet-topup', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup/cash-wallet-topup.module'); }).then(function (x) { return x.CashWalletTopupModule; }); } },
            { path: 'cash-wallet-topup-amount', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-amount/cash-wallet-topup-amount.module'); }).then(function (x) { return x.CashWalletTopupAmountModule; }); } },
            { path: 'cash-wallet-topup-method', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-method/cash-wallet-topup-method.module'); }).then(function (x) { return x.CashWalletTopupMethodModule; }); } },
            { path: 'cash-wallet-topup-bank', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-bank/cash-wallet-topup-bank.module'); }).then(function (x) { return x.CashWalletTopupBankModule; }); } },
            { path: 'RPP/MY/Redirect/RTP', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-paynet/cash-wallet-topup-paynet.module'); }).then(function (x) { return x.CashWalletTopupPaynetModule; }); } },
            { path: 'cash-wallet-topup-status', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-status/cash-wallet-topup-status.module'); }).then(function (x) { return x.CashWalletTopupStatusModule; }); } },
            { path: 'cash-wallet-topup-pending', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-pending/cash-wallet-topup-pending.module'); }).then(function (x) { return x.CashWalletTopupPendingModule; }); } },
            { path: 'cash-wallet-topup-failed', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-failed/cash-wallet-topup-failed.module'); }).then(function (x) { return x.CashWalletTopupFailedModule; }); } },
            { path: 'cash-wallet-topup-confirmation', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-topup-confirmation/cash-wallet-topup-confirmation.module'); }).then(function (x) { return x.CashWalletTopupConfirmationModule; }); } },
            { path: 'cash-wallet-withdraw', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-withdraw/cash-wallet-withdraw.module'); }).then(function (x) { return x.CashWalletWithdrawModule; }); } },
            { path: 'cash-wallet-withdraw-bank', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-withdraw-bank/cash-wallet-withdraw-bank.module'); }).then(function (x) { return x.CashWalletWithdrawBankModule; }); } },
            { path: 'cash-wallet-withdraw-status', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-withdraw-status/cash-wallet-withdraw-status.module'); }).then(function (x) { return x.CashWalletWithdrawStatusModule; }); } },
            { path: 'cash-wallet-withdraw-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-withdraw-pin/cash-wallet-withdraw-pin.module'); }).then(function (x) { return x.CashWalletWithdrawPinModule; }); } },
            { path: 'cash-wallet-withdraw-failed', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-withdraw-failed/cash-wallet-withdraw-failed.module'); }).then(function (x) { return x.CashWalletWithdrawFailedModule; }); } },
            { path: 'cash-wallet-withdraw-pending', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/cash-wallet-withdraw-pending/cash-wallet-withdraw-pending.module'); }).then(function (x) { return x.CashWalletWithdrawPendingModule; }); } },
            //gold wallet
            //{ path: 'gold-wallet', loadChildren: () => import('./modules/gold-wallet/gold-wallet.module').then(x => x.GoldWalletModule) },
            { path: 'profitloss', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/dashboard/cards/profitloss/profitloss.module'); }).then(function (m) { return m.ProfitLossModule; }); } },
            //buy gold
            { path: 'buy-bgd', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/buy/buy-bgd/buy-bgd.module'); }).then(function (x) { return x.BuyBgdModule; }); } },
            { path: 'buy-bgd-confirmation', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/buy/buy-bgd-confirmation/buy-bgd-confirmation.module'); }).then(function (x) { return x.BuyBgdConfirmationModule; }); } },
            { path: 'buy-bgd-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/buy/buy-bgd-pin/buy-bgd-pin.module'); }).then(function (x) { return x.BuyBgdPinModule; }); } },
            { path: 'buy-bgd-status', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/buy/buy-bgd-status/buy-bgd-status.module'); }).then(function (x) { return x.BuyBgdStatusModule; }); } },
            //sell gold
            { path: 'sell-bgd', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/sell/sell-bgd/sell-bgd.module'); }).then(function (x) { return x.SellBgdModule; }); } },
            { path: 'sell-bgd-confirmation', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/sell/sell-bgd-confirmation/sell-bgd-confirmation.module'); }).then(function (x) { return x.SellBgdConfirmationModule; }); } },
            { path: 'sell-bgd-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/sell/sell-bgd-pin/sell-bgd-pin.module'); }).then(function (x) { return x.SellBgdPinModule; }); } },
            { path: 'sell-bgd-status', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/sell/sell-bgd-status/sell-bgd-status.module'); }).then(function (x) { return x.SellBgdStatusModule; }); } },
            //transfer gold
            { path: 'transfer-gold', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/transfer/transfer-gold/transfer-gold.module'); }).then(function (x) { return x.TransferGoldModule; }); } },
            { path: 'transfer-landing', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/transfer/transfer-landing/transfer-landing.module'); }).then(function (x) { return x.TransferLandingModule; }); } },
            { path: 'transfer-bgd', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/transfer/transfer-bgd/transfer-bgd.module'); }).then(function (x) { return x.TransferBgdModule; }); } },
            { path: 'transfer-bgd-amount', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/transfer/transfer-bgd-amount/transfer-bgd-amount.module'); }).then(function (x) { return x.TransferBgdAmountModule; }); } },
            { path: 'transfer-bgd-confirmation', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/transfer/transfer-bgd-confirmation/transfer-bgd-confirmation.module'); }).then(function (x) { return x.TransferBgdConfirmationModule; }); } },
            { path: 'transfer-bgd-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/transfer/transfer-bgd-pin/transfer-bgd-pin.module'); }).then(function (x) { return x.TransferBgdPinModule; }); } },
            { path: 'transfer-bgd-status', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/transfer/transfer-bgd-status/transfer-bgd-status.module'); }).then(function (x) { return x.TransferBgdStatusModule; }); } },
            //redeem gold
            { path: 'redeem-bgd', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd/redeem-bgd.module'); }).then(function (x) { return x.RedeemBgdModule; }); } },
            { path: 'redeem-bgd-amount', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd-amount/redeem-bgd-amount.module'); }).then(function (x) { return x.RedeemBgdAmountModule; }); } },
            { path: 'redeem-bgd-address', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd-address/redeem-bgd-address.module'); }).then(function (x) { return x.RedeemBgdAddressModule; }); } },
            { path: 'redeem-bgd-confirmation', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation.module'); }).then(function (x) { return x.RedeemBgdConfirmationModule; }); } },
            { path: 'redeem-bgd-delivery-address', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd-delivery-address/redeem-bgd-delivery-address.module'); }).then(function (x) { return x.RedeemBgdDeliveryAddressModule; }); } },
            { path: 'redeem-bgd-pin', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd-pin/redeem-bgd-pin.module'); }).then(function (x) { return x.RedeemBgdPinModule; }); } },
            { path: 'redeem-bgd-receipt', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd-receipt/redeem-bgd-receipt.module'); }).then(function (x) { return x.RedeemBgdReceiptModule; }); } },
            { path: 'redeem-bgd-status', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/admin/bgd/redeem/redeem-bgd-status/redeem-bgd-status.module'); }).then(function (x) { return x.RedeemBgdStatusModule; }); } },
            //faq
            { path: 'faq', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/landing/faq/faq.module'); }).then(function (m) { return m.FaqModule; }); } },
            //tier
            { path: 'tier', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/tier/tier.module'); }).then(function (m) { return m.TierModule; }); } },
            { path: 'tier-all', loadChildren: function () { return Promise.resolve().then(function () { return require('app/modules/auth/tier-all/tier-all.module'); }).then(function (m) { return m.TierAllModule; }); } },
            { path: 'view-details', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/dashboard/cards/graph-details/graph-details.module'); }).then(function (x) { return x.GraphDetailsModule; }); } },
            { path: 'error-500', loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/error/error-500/error-500.module'); }).then(function (x) { return x.Error500Module; }); } },
        ]
    },
    // Dashboard
    {
        path: 'dashboard',
        loadChildren: function () { return Promise.resolve().then(function () { return require('./modules/dashboard/dashboard.module'); }).then(function (x) { return x.DashboardModule; }); },
        canActivate: [auth_guard_1.AuthGuard],
        canActivateChild: [auth_guard_1.AuthGuard],
        component: layout_component_1.LayoutComponent,
        resolve: {
            initialData: app_resolvers_1.InitialDataResolver
        }
    },
    { path: '**', component: error_404_component_1.Error404Component },
    { path: 'NotFound', component: error_404_component_1.Error404Component },
];
var config = {
    useHash: false,
    scrollPositionRestoration: 'enabled',
    enableTracing: false,
    paramsInheritanceStrategy: 'always',
    anchorScrolling: 'enabled',
    preloadingStrategy: router_1.PreloadAllModules
};
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes, config)],
            exports: [router_1.RouterModule],
            providers: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
