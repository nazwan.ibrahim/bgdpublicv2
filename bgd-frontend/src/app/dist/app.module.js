"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var config_1 = require("@bgd/services/config");
var mock_api_1 = require("@bgd/lib/mock-api");
var core_module_1 = require("app/core/core.module");
var app_config_1 = require("app/core/config/app.config");
var mock_api_2 = require("app/mock-api");
var layout_module_1 = require("app/layout/layout.module");
var app_component_1 = require("app/app.component");
var _bgd_1 = require("@bgd");
var details_password_component_1 = require("./modules/auth/details-password/details-password.component");
var security_question_component_1 = require("./modules/auth/security-question/security-question.component");
var setup_login_preference_component_1 = require("./modules/auth/setup-login-preference/setup-login-preference.component");
var login_preference_component_1 = require("./modules/auth/login-preference/login-preference.component");
var setup_success_component_1 = require("./modules/auth/setup-success/setup-success.component");
var biometric_enabled_component_1 = require("./modules/auth/biometric-enabled/biometric-enabled.component");
var default_login_component_1 = require("./modules/auth/default-login/default-login.component");
var select_id_type_component_1 = require("./modules/auth/select-id-type/select-id-type.component");
var image_id_preview_component_1 = require("./modules/auth/image-id-preview/image-id-preview.component");
var gold_wallet_component_1 = require("./modules/admin/gold-wallet/gold-wallet.component");
var app_routing_module_1 = require("./app-routing.module");
var error_404_component_1 = require("./modules/error/error-404/error-404.component");
var referral_redeem_component_1 = require("./modules/admin/referral-redeem/referral-redeem.component");
var gold_price_details_component_1 = require("./modules/gold-price-details/gold-price-details.component");
var referral_status_component_1 = require("./modules/admin/referral-status/referral-status.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                details_password_component_1.DetailsPasswordComponent,
                security_question_component_1.SecurityQuestionComponent,
                setup_login_preference_component_1.SetupLoginPreferenceComponent,
                login_preference_component_1.LoginPreferenceComponent,
                //SetupPinComponent,
                setup_success_component_1.SetupSuccessComponent,
                biometric_enabled_component_1.BiometricEnabledComponent,
                default_login_component_1.DefaultLoginComponent,
                //PrivacyNoticesComponent
                //UpgradeAccountComponent,
                //IdentityVerificationComponent,
                select_id_type_component_1.SelectIdTypeComponent,
                //VerifyMykadComponent,
                //ImageIdCaptureComponent,
                image_id_preview_component_1.ImageIdPreviewComponent,
                //SecurityComponent,
                //CashWalletTopupComponent,
                //CashWalletTopupAmountComponent,
                //CashWalletTopupMethodComponent,
                //CashWalletTopupBankComponent,
                //ChangePasswordComponent,
                //ChangePasswordNewComponent,
                //ChangePasswordPinComponent,
                //ChangePinComponent,
                //CashWalletTopupPaynetComponent,
                //CashWalletTopupStatusComponent,
                gold_wallet_component_1.GoldWalletComponent,
                // CashWalletWithdrawComponent,
                // CashWalletWithdrawBankComponent,
                error_404_component_1.Error404Component,
                // CashWalletWithdrawPinComponent,
                // CashWalletTopupFailedComponent,
                referral_redeem_component_1.ReferralRedeemComponent,
                gold_price_details_component_1.GoldPriceDetailsComponent,
                //TierComponent,
                //TierAllComponent,
                referral_status_component_1.ReferralStatusComponent,
            ],
            imports: [
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                app_routing_module_1.AppRoutingModule,
                // RouterModule.forRoot(appRoutes, routerConfig),
                // Fuse, FuseConfig & FuseMockAPI
                _bgd_1.BgdModule,
                config_1.FuseConfigModule.forRoot(app_config_1.appConfig),
                mock_api_1.FuseMockApiModule.forRoot(mock_api_2.mockApiServices),
                // Core module of your application
                core_module_1.CoreModule,
                // Layout module of your application
                layout_module_1.LayoutModule
            ],
            bootstrap: [
                app_component_1.AppComponent
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
