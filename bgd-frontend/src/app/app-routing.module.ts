import { AboutGoldDinarModule } from './modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, ExtraOptions } from '@angular/router';
import { InitialDataResolver } from './app.resolvers';
import { AuthGuard } from './core/auth/guards/auth.guard';
import { NoAuthGuard } from './core/auth/guards/noAuth.guard';
import { LayoutComponent } from './layout/layout.component';
import { Error404Component } from './modules/error/error-404/error-404.component';
import { ComingSoonComponent } from './modules/error/coming-soon/coming-soon.component';
import { Error500Component } from './modules/error/error-500/error-500.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/sign-in' },


  { path: 'signed-in-redirect', pathMatch: 'full', redirectTo: '/dashboard' },

  // Auth routes for guests
  {
    path: '',
    // canActivate: [NoAuthGuard],
    // canActivateChild: [NoAuthGuard],
    component: LayoutComponent,
    data: {
      layout: 'empty'
    },
    children: [
      // { path: 'confirmation-required', loadChildren: () => import('app/modules/auth/confirmation-required/confirmation-required.module').then(m => m.AuthConfirmationRequiredModule) },
      { path: 'verified-email', loadChildren: () => import('app/modules/auth/register/verified-email/verified-email.module').then(m => m.VerifiedEmailModule) },
      // { path: 'forgot-password', loadChildren: () => import('app/modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule) },
      { path: 'reset-password', loadChildren: () => import('app/modules/auth/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule) },
      // { path: 'sign-in', loadChildren: () => import('app/modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule) },
      // { path: 'register', loadChildren: () => import('app/modules/auth/register/register.module').then(m => m.AuthRegisterModule) },
      // { path: 'investor-type', loadChildren: () => import('app/modules/auth/investor-type/investor-type.module').then(m => m.InvestorTypeModule) },
      // { path: 'register/otp-phone', loadChildren: () => import('app/modules/auth/register/otp-phone/otp-phone.module').then(m => m.OtpPhoneModule) },
      // { path: 'get-started', loadChildren: () => import('app/modules/auth/get-started/get-started.module').then(m => m.GetStartedModule) },
      // { path: 'otp-phone', loadChildren: () => import('app/modules/auth/register/otp-phone/otp-phone.module').then(m => m.OtpPhoneModule) },
      // { path: 'forgot-password-question', loadChildren: () => import('app/modules/auth/forgot-password-question/forgot-password-question.module').then(m => m.ForgotPasswordQuestionModule) },
      // { path: 'forgot-password-email', loadChildren: () => import('app/modules/auth/forgot-password-email/forgot-password-email.module').then(m => m.ForgotPasswordEmailModule) },
      // { path: 'forgot-password-email-failed', loadChildren: () => import('app/modules/auth/forgot-password-email failed/forgot-password-email-failed.module').then(m => m.ForgotPasswordEmailFailedModule) },
      // { path: 'create-account', loadChildren: () => import('app/modules/auth/register/create-account/create-account.module').then(m => m.CreateAccountdModule) },
      // { path: 'security-questions', loadChildren: () => import('app/modules/auth/register/security-questions/security-questions.module').then(m => m.securityQuestionsModule) },
      { path: 'RPP/MY/Redirect/RTP', loadChildren: () => import('./modules/admin/cash-wallet-topup-paynet/cash-wallet-topup-paynet.module').then(x => x.CashWalletTopupPaynetModule) },
    ]
  },

  // Auth routes for authenticated users
  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: LayoutComponent,
    data: {
      layout: 'empty'
    },
    children: [
      // { path: 'sign-out', loadChildren: () => import('app/modules/auth/sign-out/sign-out.module').then(m => m.AuthSignOutModule) },
      // { path: 'unlock-session', loadChildren: () => import('app/modules/auth/unlock-session/unlock-session.module').then(m => m.AuthUnlockSessionModule) },

      // {path: 'login-preference', loadChildren: () => import('app/modules/auth/login-preferences/login-preference.module').then(m => m.LoginPreferencesModule)}
    ]
  },

  // Landing routes
  {
    path: '',
    component: LayoutComponent,
    data: {
      layout: 'empty'
    },
    children: [
      // { path: 'home', loadChildren: () => import('app/modules/landing/home/home.module').then(m => m.LandingHomeModule) },
      // { path: 'help-support', loadChildren: () => import('app/modules/landing/help-support/help-support.module').then(m => m.HelpSupportModule) },
    ]
  },

  // Admin routes

  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: LayoutComponent,
    resolve: {
      initialData: InitialDataResolver,
    },
    children: [
      { path: 'example', loadChildren: () => import('app/modules/admin/example/example.module').then(m => m.ExampleModule) },
      { path: 'maintenance', loadChildren: () => import('app/modules/admin/maintenance/maintenance.module').then(m => m.MaintenanceModule) },
      { path: 'referral', loadChildren: () => import('app/modules/admin/referral/referral.module').then(m => m.ReferralModule) },
      { path: 'user-profile', loadChildren: () => import('app/modules/admin/profile/user-profile/user-profile.module').then(m => m.UserProfileModule) },
      //{ path: 'edit-profile', loadChildren: () => import('app/modules/admin/profile/edit-profile/edit-profile.module').then(m => m.EditProfileModule) },
      { path: 'verify-mykad', loadChildren: () => import('app/modules/auth/verify-mykad/verify-mykad.module').then(m => m.VerifyMykadModule) },
      { path: 'upgrade-account', loadChildren: () => import('app/modules/auth/upgrade-account/upgrade-account.module').then(m => m.UpgradeAccountModule) },
      { path: 'identity-verification', loadChildren: () => import('app/modules/auth/identity-verification/identity-verification.module').then(m => m.IdentityVerificationModule) },
      { path: 'transaction-history', loadChildren: () => import('app/modules/admin/transaction-history/transaction-history.module').then(m => m.TransactionHistoryModule) },
      { path: 'transaction-history-details', loadChildren: () => import('app/modules/admin/transaction-history-details/transaction-history-details.module').then(m => m.TransactionHistoryDetailsModule) },
      { path: 'transaction-history-details-topup', loadChildren: () => import('app/modules/admin/transaction-history-details/transaction-history-details-topup/transaction-history-details-topup.module').then(m => m.TransactionHistoryDetailsTopupModule) },
      { path: 'transaction-history-details-withdraw', loadChildren: () => import('app/modules/admin/transaction-history-details/transaction-history-details-withdraw/transaction-history-details-withdraw.module').then(m => m.TransactionHistoryDetailsWithdrawModule) },
      { path: 'transaction-history-details-transfer', loadChildren: () => import('app/modules/admin/transaction-history-details/transaction-history-details-transfer/transaction-history-details-transfer.module').then(m => m.TransactionHistoryDetailsTransferModule) },
      { path: 'transaction-history-details-redeem', loadChildren: () => import('app/modules/admin/transaction-history-details/transaction-history-details-redeem/transaction-history-details-redeem.module').then(m => m.TransactionHistoryDetailsRedeemModule) },
      { path: 'transaction-history-details-buy-bgd', loadChildren: () => import('app/modules/admin/transaction-history-details/transaction-history-details-buy-bgd/transaction-history-details-buy-bgd.module').then(m => m.TransactionHistoryDetailsBuyBgdModule) },
      { path: 'transaction-history-details-sell-bgd', loadChildren: () => import('app/modules/admin/transaction-history-details/transaction-history-details-sell-bgd/transaction-history-details-sell-bgd.module').then(m => m.TransactionHistoryDetailsSellBgdModule) },
      //upgrade account
      { path: 'upgrade-account', loadChildren: () => import('app/modules/auth/upgrade-account/upgrade-account.module').then(m => m.UpgradeAccountModule) },
      { path: 'identity-verification', loadChildren: () => import('app/modules/auth/identity-verification/identity-verification.module').then(m => m.IdentityVerificationModule) },
      { path: 'activation-unsuccessful', loadChildren: () => import('app/modules/auth/verify-mykad/activation-unsuccessfull/activation-unsuccessful.module').then(m => m.ActivationUnsuccessfullModule) },
      { path: 'id-unrecognisable', loadChildren: () => import('app/modules/auth/verify-mykad/id-unrecognisable/id-unrecognisable.module').then(m => m.IdUnrecognisableModule) },
      { path: 'id-mismatch', loadChildren: () => import('app/modules/auth/verify-mykad/id-mismatch/id-mismatch.module').then(m => m.IdMismatchModule) },
      { path: 'selfie-mismatch', loadChildren: () => import('app/modules/auth/verify-mykad/selfie-mismatch/selfie-mismatch.module').then(m => m.SelfieMismatchModule) },
      { path: 'camera-unavailable', loadChildren: () => import('app/modules/auth/camera-unavailable/camera-unavailable.module').then(m => m.CameraUnavailableModule) },
      { path: 'front-id', loadChildren: () => import('app/modules/auth/verify-mykad/front-id/front-id.module').then(m => m.FrontIdModule) },
      { path: 'front-id-image', loadChildren: () => import('app/modules/auth/verify-mykad/front-id-image/front-id-image.module').then(m => m.FrontIdImageModule) },
      { path: 'back-id', loadChildren: () => import('app/modules/auth/verify-mykad/back-id/back-id.module').then(m => m.BackIdModule) },
      { path: 'back-id-image', loadChildren: () => import('app/modules/auth/verify-mykad/back-id-image/back-id-image.module').then(m => m.BackIdImageModule) },
      { path: 'selfie', loadChildren: () => import('app/modules/auth/verify-mykad/selfie/selfie.module').then(m => m.SelfieModule) },
      { path: 'selfie-image', loadChildren: () => import('app/modules/auth/verify-mykad/selfie-image/selfie-image.module').then(m => m.SelfieImageModule) },
      { path: 'update-info', loadChildren: () => import('app/modules/auth/verify-mykad/update-info/update-info.module').then(m => m.UpdateInfoModule) },
      { path: 'bank-info', loadChildren: () => import('app/modules/auth/verify-mykad/bank-info/bank-info.module').then(m => m.BankInfoModule) },
      { path: 'verification', loadChildren: () => import('app/modules/auth/verify-mykad/verification/verification.module').then(m => m.VerificationModule) },
      { path: 'verify-failed', loadChildren: () => import('app/modules/auth/verify-mykad/verify-failed/verify-failed.module').then(m => m.VerifyFailedModule) },
      { path: 'verify-success', loadChildren: () => import('app/modules/auth/verify-mykad/verify-success/verify-success.module').then(m => m.VerifySuccessModule) },

      //my-account
      { path: 'myaccount', loadChildren: () => import('app/modules/admin/my-account/my-account.module').then(m => m.MyAccountModule) },
      { path: 'editProfile', loadChildren: () => import('app/modules/admin/my-account/cards/edit-profile/edit-profile.module').then(m => m.EditProfileModule) },
      //{ path: 'myProfile', loadChildren: () => import('app/modules/admin/my-account/cards/user-profile/user-profile.module').then(m => m.AboutGoldDinarModule) },
      { path: 'aboutgd', loadChildren: () => import('app/modules/admin/my-account/cards/about-gold-dinar/about-gold-dinar.module').then(m => m.AboutGoldDinarModule) },
      { path: 'account-bankdetail', loadChildren: () => import('app/modules/admin/my-account/cards/account-bankdetails/account-bankdetails.module').then(m => m.AccountBankDetailsModule) },
      { path: 'edit-account-bankdetails', loadChildren: () => import('app/modules/admin/my-account/cards/edit-account-bankdetails/edit-account-bankdetails.module').then(m => m.EditAccountBankDetailsModule) },
      { path: 'helpsupport', loadChildren: () => import('app/modules/admin/my-account/cards/help-support/help-support.module').then(m => m.HelpSupportModule) },
      { path: 'privacynotice', loadChildren: () => import('app/modules/admin/my-account/cards/privacy-notices/privacy-notices.module').then(m => m.PrivacyNoticesModule) },
      { path: 'settings', loadChildren: () => import('app/modules/admin/my-account/cards/settings/settings.module').then(m => m.SettingsModule) },
      { path: 'select-language', loadChildren: () => import('app/modules/admin/my-account/cards/settings/select-language/select-language.module').then(m => m.SelectLanguageModule) },
      { path: 'security', loadChildren: () => import('app/modules/admin/my-account/cards/security/security.module').then(m => m.SecurityModule) },
      { path: 'change-password', loadChildren: () => import('app/modules/admin/my-account/cards/change-password/change-password.module').then(m => m.ChangePasswordModule) },
      { path: 'change-password-new', loadChildren: () => import('app/modules/admin/my-account/cards/change-password-new/change-password-new.module').then(m => m.ChangePasswordNewModule) },
      { path: 'change-password-pin', loadChildren: () => import('app/modules/admin/my-account/cards/change-password-pin/change-password-pin.module').then(m => m.ChangePasswordPinModule) },
      { path: 'change-pin', loadChildren: () => import('app/modules/admin/my-account/cards/change-pin/change-pin.module').then(m => m.ChangePinModule) },
      { path: 'change-pin-new', loadChildren: () => import('app/modules/admin/my-account/cards/change-pin-new/change-pin-new.module').then(m => m.ChangePinNewModule) },
      { path: 'change-pin-confirm', loadChildren: () => import('app/modules/admin/my-account/cards/change-pin-confirm/change-pin-confirm.module').then(m => m.ChangePinConfirmModule) },
      { path: 'forgot-pin', loadChildren: () => import('app/modules/auth/forgot-pin/forgot-pin.module').then(m => m.ForgotPinModule) },
      { path: 'forgot-pin-otp', loadChildren: () => import('app/modules/auth/forgot-pin/forgot-pin-otp/forgot-pin-otp.module').then(m => m.ForgotPinOtpModule) },
      { path: 'forgot-pin-new', loadChildren: () => import('app/modules/auth/forgot-pin/forgot-pin-new/forgot-pin-new.module').then(m => m.ForgotPinNewModule) },
      { path: 'forgot-pin-confirm', loadChildren: () => import('app/modules/auth/forgot-pin/forgot-pin-confirm/forgot-pin-confirm.module').then(m => m.ForgotPinConfirmModule) },
      { path: 'termcondition', loadChildren: () => import('app/modules/admin/my-account/cards/term-conditions/term-conditions.module').then(m => m.TermConditionsModule) },

      { path: 'editProfilePic', loadChildren: () => import('app/modules/admin/my-account/cards/edit-profile/edit-profile-picture/edit-profile-picture.module').then(m => m.EditProfilePictureModule) },
      { path: 'uploadProfilePic', loadChildren: () => import('app/modules/admin/my-account/cards/edit-profile/upload-profile-picture/upload-profile-picture.module').then(m => m.UploadProfilePictureModule) },

      { path: 'setup-pin-landing', loadChildren: () => import('app/modules/auth/setup-pin-landing/setup-pin-landing.module').then(m => m.SetupPinLandingModule) },
      { path: 'setup-pin', loadChildren: () => import('app/modules/auth/setup-pin/setup-pin.module').then(m => m.SetupPinModule) },
      { path: 'setup-pin-otp', loadChildren: () => import('app/modules/auth/setup-pin-otp/setup-pin-otp.module').then(m => m.SetupPinOtpModule) },

      { path: 'transaction-limit', loadChildren: () => import('app/modules/auth/transaction-limit/transaction-limit.module').then(m => m.TransactionLimitModule) },

      //cash wallet
      { path: 'cash-wallet', loadChildren: () => import('./modules/cash-wallet/cash-wallet.module').then(x => x.CashWalletModule) },
      { path: 'cash-wallet-topup', loadChildren: () => import('./modules/admin/cash-wallet-topup/cash-wallet-topup.module').then(x => x.CashWalletTopupModule) },
      { path: 'cash-wallet-topup-amount', loadChildren: () => import('./modules/admin/cash-wallet-topup-amount/cash-wallet-topup-amount.module').then(x => x.CashWalletTopupAmountModule) },
      { path: 'cash-wallet-topup-method', loadChildren: () => import('./modules/admin/cash-wallet-topup-method/cash-wallet-topup-method.module').then(x => x.CashWalletTopupMethodModule) },
      { path: 'cash-wallet-topup-bank', loadChildren: () => import('./modules/admin/cash-wallet-topup-bank/cash-wallet-topup-bank.module').then(x => x.CashWalletTopupBankModule) },
      { path: 'cash-wallet-topup-status', loadChildren: () => import('./modules/admin/cash-wallet-topup-status/cash-wallet-topup-status.module').then(x => x.CashWalletTopupStatusModule) },
      { path: 'cash-wallet-topup-pending', loadChildren: () => import('./modules/admin/cash-wallet-topup-pending/cash-wallet-topup-pending.module').then(x => x.CashWalletTopupPendingModule) },
      { path: 'cash-wallet-topup-failed', loadChildren: () => import('./modules/admin/cash-wallet-topup-failed/cash-wallet-topup-failed.module').then(x => x.CashWalletTopupFailedModule) },
      { path: 'cash-wallet-topup-confirmation', loadChildren: () => import('./modules/admin/cash-wallet-topup-confirmation/cash-wallet-topup-confirmation.module').then(x => x.CashWalletTopupConfirmationModule) },
      { path: 'cash-wallet-withdraw', loadChildren: () => import('./modules/admin/cash-wallet-withdraw/cash-wallet-withdraw.module').then(x => x.CashWalletWithdrawModule) },
      { path: 'cash-wallet-withdraw-bank', loadChildren: () => import('./modules/admin/cash-wallet-withdraw-bank/cash-wallet-withdraw-bank.module').then(x => x.CashWalletWithdrawBankModule) },
      { path: 'cash-wallet-withdraw-status', loadChildren: () => import('./modules/admin/cash-wallet-withdraw-status/cash-wallet-withdraw-status.module').then(x => x.CashWalletWithdrawStatusModule) },
      { path: 'cash-wallet-withdraw-pin', loadChildren: () => import('./modules/admin/cash-wallet-withdraw-pin/cash-wallet-withdraw-pin.module').then(x => x.CashWalletWithdrawPinModule) },
      { path: 'cash-wallet-withdraw-failed', loadChildren: () => import('./modules/admin/cash-wallet-withdraw-failed/cash-wallet-withdraw-failed.module').then(x => x.CashWalletWithdrawFailedModule) },
      { path: 'cash-wallet-withdraw-pending', loadChildren: () => import('./modules/admin/cash-wallet-withdraw-pending/cash-wallet-withdraw-pending.module').then(x => x.CashWalletWithdrawPendingModule) },
      { path: 'cash-wallet-topup-rejected', loadChildren: () => import('./modules/admin/cash-wallet-topup-rejected/cash-wallet-topup-rejected.module').then(x => x.CashWalletTopupRejectedModule) },
      { path: 'cash-wallet-topup-rejected-screen', loadChildren: () => import('./modules/admin/cash-wallet-topup-rejected-screen/cash-wallet-topup-rejected-screen.module').then(x => x.CashWalletTopupRejectedScreenModule) },
      //gold wallet
      //{ path: 'gold-wallet', loadChildren: () => import('./modules/gold-wallet/gold-wallet.module').then(x => x.GoldWalletModule) },
      { path: 'profitloss', loadChildren: () => import('app/modules/dashboard/cards/profitloss/profitloss.module').then(m => m.ProfitLossModule) },
      //buy gold
      { path: 'buy-bgd', loadChildren: () => import('./modules/admin/bgd/buy/buy-bgd/buy-bgd.module').then(x => x.BuyBgdModule) },
      { path: 'buy-bgd-confirmation', loadChildren: () => import('./modules/admin/bgd/buy/buy-bgd-confirmation/buy-bgd-confirmation.module').then(x => x.BuyBgdConfirmationModule) },
      { path: 'buy-bgd-pin', loadChildren: () => import('./modules/admin/bgd/buy/buy-bgd-pin/buy-bgd-pin.module').then(x => x.BuyBgdPinModule) },
      { path: 'buy-bgd-status', loadChildren: () => import('./modules/admin/bgd/buy/buy-bgd-status/buy-bgd-status.module').then(x => x.BuyBgdStatusModule) },
      //sell gold
      { path: 'sell-bgd', loadChildren: () => import('./modules/admin/bgd/sell/sell-bgd/sell-bgd.module').then(x => x.SellBgdModule) },
      { path: 'sell-bgd-confirmation', loadChildren: () => import('./modules/admin/bgd/sell/sell-bgd-confirmation/sell-bgd-confirmation.module').then(x => x.SellBgdConfirmationModule) },
      { path: 'sell-bgd-pin', loadChildren: () => import('./modules/admin/bgd/sell/sell-bgd-pin/sell-bgd-pin.module').then(x => x.SellBgdPinModule) },
      { path: 'sell-bgd-status', loadChildren: () => import('./modules/admin/bgd/sell/sell-bgd-status/sell-bgd-status.module').then(x => x.SellBgdStatusModule) },
      //transfer gold
      { path: 'transfer-gold', loadChildren: () => import('./modules/admin/bgd/transfer/transfer-gold/transfer-gold.module').then(x => x.TransferGoldModule) },
      { path: 'transfer-landing', loadChildren: () => import('./modules/admin/bgd/transfer/transfer-landing/transfer-landing.module').then(x => x.TransferLandingModule) },
      { path: 'transfer-bgd', loadChildren: () => import('./modules/admin/bgd/transfer/transfer-bgd/transfer-bgd.module').then(x => x.TransferBgdModule) },
      { path: 'transfer-bgd-amount', loadChildren: () => import('./modules/admin/bgd/transfer/transfer-bgd-amount/transfer-bgd-amount.module').then(x => x.TransferBgdAmountModule) },
      { path: 'transfer-bgd-confirmation', loadChildren: () => import('./modules/admin/bgd/transfer/transfer-bgd-confirmation/transfer-bgd-confirmation.module').then(x => x.TransferBgdConfirmationModule) },
      { path: 'transfer-bgd-pin', loadChildren: () => import('./modules/admin/bgd/transfer/transfer-bgd-pin/transfer-bgd-pin.module').then(x => x.TransferBgdPinModule) },
      { path: 'transfer-bgd-status', loadChildren: () => import('./modules/admin/bgd/transfer/transfer-bgd-status/transfer-bgd-status.module').then(x => x.TransferBgdStatusModule) },

      //redeem gold
      { path: 'redeem-bgd', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd/redeem-bgd.module').then(x => x.RedeemBgdModule) },
      { path: 'redeem-bgd-amount', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd-amount/redeem-bgd-amount.module').then(x => x.RedeemBgdAmountModule) },
      { path: 'redeem-bgd-address', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd-address/redeem-bgd-address.module').then(x => x.RedeemBgdAddressModule) },
      { path: 'redeem-bgd-confirmation', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation.module').then(x => x.RedeemBgdConfirmationModule) },
      { path: 'redeem-bgd-delivery-address', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd-delivery-address/redeem-bgd-delivery-address.module').then(x => x.RedeemBgdDeliveryAddressModule) },
      { path: 'redeem-bgd-pin', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd-pin/redeem-bgd-pin.module').then(x => x.RedeemBgdPinModule) },
      { path: 'redeem-bgd-receipt', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd-receipt/redeem-bgd-receipt.module').then(x => x.RedeemBgdReceiptModule) },
      { path: 'redeem-bgd-status', loadChildren: () => import('./modules/admin/bgd/redeem/redeem-bgd-status/redeem-bgd-status.module').then(x => x.RedeemBgdStatusModule) },

      //faq
      { path: 'faq', loadChildren: () => import('app/modules/landing/faq/faq.module').then(m => m.FaqModule) },

      //tier
      { path: 'tier', loadChildren: () => import('app/modules/auth/tier/tier.module').then(m => m.TierModule) },
      { path: 'tier-all', loadChildren: () => import('app/modules/auth/tier-all/tier-all.module').then(m => m.TierAllModule) },

      { path: 'view-details', loadChildren: () => import('./modules/dashboard/cards/graph-details/graph-details.module').then(x => x.GraphDetailsModule) },

      //Insights
      { path: 'insights', loadChildren: () => import('app/modules/dashboard/cards/insights/insights.module').then(m => m.InsightsModule) },
      { path: 'insights-detail', loadChildren: () => import('app/modules/dashboard/cards/insights-detail/insights-detail.module').then(m => m.InsightsDetailModule) },
    ]
  },

  // Dashboard
  {
    path: 'dashboard',
    loadChildren: () => import('./modules/dashboard/dashboard.module').then(x => x.DashboardModule),
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: LayoutComponent,
    resolve: {
      initialData: InitialDataResolver,
    }
  },

  // { path: '**', component: Error404Component },
  { path: '**', component: ComingSoonComponent },

  // { path: 'NotFound', component: Error404Component },
  { path: 'NotFound', component: ComingSoonComponent },

];

const config: ExtraOptions = {
  useHash: false,
  scrollPositionRestoration: 'enabled',
  enableTracing: false,
  paramsInheritanceStrategy: 'always',
  anchorScrolling: 'enabled',
  preloadingStrategy: PreloadAllModules,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }