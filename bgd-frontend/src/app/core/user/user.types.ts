export interface User {
    userID: string;
    fullName: string;
    email: string;
    userName: string;
    phoneNumber: string;
    actStatus: string;
    referral: string;
    reference: string;
    nationality: string;
    country: string;
    identificationNumber: string;
    userTier: string;
    identificationType: string;
    userType: string;
    emailVerified: boolean;
    role: string;
    deviceId: string;
    profilePicture: string;
    tierStatus: string;
    active: boolean; // Add this property
}

/*{
    "userID": "69b42510-930e-0994-f458-529d431bdc00",
    "pinEnable": true,
    "fullName": "AL ARIF",
    "userName": "arif@milradius.com.my",
    "email": "arif@milradius.com.my",
    "phoneNumber": "+601116843030",
    "actStatus": {
      "id": "8e255c99-6572-e348-b832-d08b8782733a",
      "code": "G04",
      "name": "Active",
      "description": "",
      "version": 1,
      "createdBy": "system",
      "createdDate": "2023-01-19T23:19:09.716+00:00",
      "lastModifiedBy": null,
      "lastModifiedDate": "2023-01-19T23:19:09.716+00:00",
      "deletedBy": null,
      "deletedDate": null
    },
    "reference": null,
    "referral": "ARIF@11168",
    "nationality": "Malaysia",
    "country": "Malaysia",
    "identificationNumber": "950107126789",
    "userTier": {
      "id": "00f2f15b-7c82-bea5-2043-1877697f7c4c",
      "code": "T01",
      "name": "Tier 1",
      "description": null,
      "version": 1,
      "createdBy": "system",
      "createdDate": "2023-02-15T12:35:46.074+00:00",
      "lastModifiedBy": null,
      "lastModifiedDate": "2023-02-15T12:35:46.074+00:00",
      "deletedBy": null,
      "deletedDate": null
    },
    "identificationType": {
      "id": "de02cd2a-c8ca-568e-5173-d3652b6b0068",
      "code": "P01",
      "name": "MyKad",
      "description": "MyKad ID",
      "version": 1,
      "createdBy": "system",
      "createdDate": "2023-01-19T23:19:10.027+00:00",
      "lastModifiedBy": null,
      "lastModifiedDate": "2023-01-19T23:19:10.027+00:00",
      "deletedBy": null,
      "deletedDate": null
    },
    "userType": {
      "id": "26be3623-5a8c-7921-9c96-01248108184b",
      "code": "K01",
      "name": "Individual",
      "description": "Individual Investor",
      "version": 1,
      "createdBy": "system",
      "createdDate": "2023-01-19T23:19:09.253+00:00",
      "lastModifiedBy": null,
      "lastModifiedDate": "2023-01-19T23:19:09.253+00:00",
      "deletedBy": null,
      "deletedDate": null
    },
    "emailVerified": true,
    "role": [
      {
        "role": "rest-api"
      }
    ],
    "deviceID": null,
    "profilePicture": null,
    "tierStatus": {
      "code": "G10",
      "name": "verification.declined",
      "description": null
    }
  }*/