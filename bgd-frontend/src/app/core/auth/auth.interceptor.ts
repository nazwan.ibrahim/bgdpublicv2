import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { catchError, Observable, throwError, map } from 'rxjs';
import { AuthService } from 'app/core/auth/auth.service';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ReloginAlertDialog } from './relogin-alert-dialog/relogin-alert-dialog.component';
import { ReloginTimeoutAlertDialog } from './relogin-timeout-alret-dialog/relogin-timeout-alert-dialog.component';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    isDialogOpen: boolean;
    /**
     * Constructor
     */
    constructor(private _authService: AuthService,
        private _router: Router,
        public dialog: MatDialog) {
    }

    /**
     * Intercept
     *
     * @param req
     * @param next
     */

    openDialog(): void {
        this.isDialogOpen = true;

        const dialogRef = this.dialog.open(ReloginAlertDialog, {
        });

        dialogRef.afterClosed().subscribe(() => {
            //   location.reload();
        });
    }

    openDialogTimeout(): void {
        this.isDialogOpen = true;

        const dialogRef = this.dialog.open(ReloginTimeoutAlertDialog, {
        });

        dialogRef.afterClosed().subscribe(() => {
            //   location.reload();
        });
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Clone the request object
        let newReq = req.clone();
        // let newReq = req.clone({
        //     withCredentials: true, // Enable sending cookies with the request
        //     setHeaders: {
        //         'X-Content-Type-Options': 'nosniff',
        //         'X-Frame-Options': 'SAMEORIGIN',
        //         'X-XSS-Protection': '1; mode=block',
        //         'Content-Security-Policy': "default-src 'self'"
        //     }
        // });


        // Request
        //
        // If the access token didn't expire, add the Authorization header.
        // We won't add the Authorization header if the access token expired.
        // This will force the server to return a "401 Unauthorized" response
        // for the protected API routes which our response interceptor will
        // catch and delete the access token from the local storage while logging
        // the user out from the app.
        if (this._authService.accessToken && !AuthUtils.isTokenExpired(this._authService.accessToken)) {
            newReq = newReq.clone({
                headers: newReq.headers.set('Authorization', 'Bearer ' + this._authService.accessToken)
            });
        }

        // Response
        return next.handle(newReq).pipe(
            catchError((error) => {
                // Catch "401 Unauthorized" responses

                console.log(error);
                
                if (error instanceof HttpErrorResponse && error.status === 401 && error.error === 'Session Expired') {
                    //Popup dialog to relogin
                    this.openDialog();

                } else if (error instanceof HttpErrorResponse && error.status === 504) {
                    //Popup dialog to relogin
                    this.openDialogTimeout();

                }
                return throwError(error);
            })
        );
    }
}
