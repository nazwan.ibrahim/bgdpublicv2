import { Component, Inject, HostListener } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'app/core/auth/auth.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

export interface DialogData {
  price: any;
}

@Component({
  selector: 'relogin-alert-dialog',
  templateUrl: 'relogin-alert-dialog.component.html',
})
export class ReloginAlertDialog {
  constructor(
    private dialogRef: MatDialogRef<ReloginAlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _authService: AuthService,
    private _router: Router,
    private location: Location
  ) { }

  onProceed(): void {
    this.dialogRef.close(true);
    this._authService.signOut();
    location.reload();
    this._router.navigate(['/sign-in']);
  }

  @HostListener('document:click', ['$event.target'])
  onClickOutside(targetElement: HTMLElement): void {
    if (!this.dialogRef.disableClose && targetElement.classList.contains('cdk-overlay-backdrop')) {
      this.dialogRef.close(false);
      this.location.back();
    }
  }
}
