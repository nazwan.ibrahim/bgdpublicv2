import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, Observable, of, switchMap, throwError } from 'rxjs';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { UserService } from 'app/core/user/user.service';
import { environment } from 'environments/environment';

@Injectable()
export class AuthService {
    private _authenticated = false;

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    /**
     * Setter & getter for access token
     */

    private readonly loggedInKey = 'isLoggedIn';

    set accessToken(token: string) {
        sessionStorage.setItem('accessToken', token);
    }

    get accessToken(): string {
        return sessionStorage.getItem('accessToken') ?? '';
    }

    set refreshToken(refreshtoken: string) {
        sessionStorage.setItem('refreshtoken', refreshtoken);
    }

    get refreshToken(): string {
        return sessionStorage.getItem('refreshtoken') ?? '';
    }

    isLoggedIn(): boolean {
        // Check if the isLoggedIn flag is present in localStorage
        return sessionStorage.getItem(this.loggedInKey) === 'true';
    }

    initLogoutOnBrowserClose(): void {
        window.addEventListener('beforeunload', () => {
            this.signOut();
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Forgot password
     *
     * @param email
     */
    forgotPassword(forgotPassword: { email: string, questionCode: string, answer: string }) {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/generateResetPassword', forgotPassword, { responseType: 'json' })
    }
    /**
     * Reset password
     *
     * @param password
     */
    ResetPassword(resetPassword: { id: string, userID: string, newPassword: string, confirmPassword: string }): Observable<any> {
        return this._httpClient.put(environment.apiUrl + '/api/onboarding/resetPassword', resetPassword, { responseType: 'text' });
    }

    ChangePassword(ChangePassword: { oldPassword: string, newPassword: string, confirmPassword: string }): Observable<any> {
        return this._httpClient.put(environment.apiUrl + '/api/onboarding/changePassword', ChangePassword, { responseType: 'text' });
    }

    ValidatePassword(validatePassword: { password: string }): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/validatePassword', validatePassword, { responseType: 'text' });
    }

    checkSecurityQuestion(email: any): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        const url = `/api/onboarding/securityQuestion?email=${encodeURIComponent(email)}`;
        return this._httpClient.get(environment.apiUrl + url, { responseType: 'text' });
    }

    /**
     * Sign in
     *
     * @param credentials
     */
    signIn(credentials: { username: string; password: string }): Observable<any> {
        // Throw error, if the user is already logged in
        if (this._authenticated) {
            return throwError('User is already logged in.');
        }

        return this._httpClient.post(environment.apiUrl + '/api/onboarding/login',
            credentials, { headers: environment.credentials }).pipe(
                switchMap((response: any) => {

                    //         // Store the access token in the local storage
                    this.accessToken = response.access_token;

                    this.refreshToken = response.refresh_token

                    //         // Set the authenticated flag to true
                    this._authenticated = true;

                    //         // Store the user on the user service
                    //         this._userService.user = response.user;

                    //         // Return a new observable with the response

                    return of(response);
                })
            );
    }

    /**
     * Sign in using the access token
     */
    signInUsingToken(): Observable<any> {

        if (this.accessToken !== null) {
            return of(true);
        } else {
            return of(false);
        }
        // // Sign in using the token
        // return this._httpClient.post(environment.apiUrl + '/api/onboarding/refreshToken', {
        //     refreshToken: this.refreshToken
        // }, {
        //     headers: { 'client': 'bgd-client', 'secret': '2IiLzXEF5OIIhmuINQzLsGhfDYFjNNv2' }
        // }).pipe(
        //     catchError(() =>

        //         // Return false
        //         of(false)
        //     ),
        //     switchMap((response: any) => {

        //         // Replace the access token with the new one if it's available on
        //         // the response object.
        //         //
        //         // This is an added optional step for better security. Once you sign
        //         // in using the token, you should generate a new one on the server
        //         // side and attach it to the response object. Then the following
        //         // piece of code can replace the token with the refreshed one.
        //         if (response.access_token) {
        //             this.accessToken = response.access_token;
        //         }

        //         // Set the authenticated flag to true
        //         this._authenticated = true;

        //         // Store the user on the user service
        //         //this._userService.user = response.user;

        //         // Return true
        //         return of(true);
        //     })
        // );
    }

    /**
     * Sign out
     */
    signOut(): Observable<any> {
        // Remove the access token from the local storage
        sessionStorage.removeItem('accessToken');
        // Clear the isLoggedIn flag in localStorage
        sessionStorage.removeItem(this.loggedInKey);

        sessionStorage.removeItem('refreshToken');

        // Set the authenticated flag to false
        this._authenticated = false;

        // Return the observable
        return of(true);
    }


    /**
     * Sign up
     *
     * @param user
     */
    signUp(user: { name: string; email: string; password: string; company: string }): Observable<any> {
        return this._httpClient.post('api/auth/sign-up', user);
    }

    /**
     * Register
     *
     * @param user
     */
    register(user: { firstCtrl: string }): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/registerIndividual', user);
    }

    /**
     * Unlock session
     *
     * @param credentials
     */
    unlockSession(credentials: { email: string; password: string }): Observable<any> {
        return this._httpClient.post('api/auth/unlock-session', credentials);
    }

    /**
     * Check the authentication status
     */
    check(): Observable<boolean> {
        // Check if the user is logged in
        if (this._authenticated) {
            return of(true);
        }

        // Check the access token availability
        if (!this.accessToken) {
            return of(false);
        }

        // Check the access token expire date
        //if ( AuthUtils.isTokenExpired(this.accessToken) )
        //{
        //   return of(false);
        //}

        // If the access token exists and it didn't expire, sign in using it
        return this.signInUsingToken();
    }


    /**
     * Verify Otp
     *
     * @param user
     */
    verifyOtp(otp: { otp: string; phoneNumber: string; serviceSid: string; }): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/verifyOTP', otp);
    }

    createProfilePicture(formData: FormData): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/createProfilePicture', formData);
    }


    setupPin(pin: { newPin: any }): Observable<any> {
        return this._httpClient.post(environment.apiUrl + '/api/onboarding/pin', pin);
    }

    resetPin(pin: { newPin: any; confirmPin: any }): Observable<any> {
        return this._httpClient.put(environment.apiUrl + '/api/onboarding/resetPin', pin);
    }

    changePin(pin: { currentPin: any; newPin: any; confirmPin: any }): Observable<any> {
        return this._httpClient.put(environment.apiUrl + '/api/onboarding/pin', pin);
    }

    bankList(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/bankList');
    }

    //get user details
    getUserDetails(): Observable<any> {
        return this._httpClient.get<any>(environment.apiUrl + '/api/onboarding/getUserDetails');
    }

    verifyKYC(body: { faceImg: any, documentImgFront: any, documentImgBack: any, userID: any, kycType: any }): Observable<any> {
        const formData = new FormData();
        formData.append('faceImg', body.faceImg);
        formData.append('documentImgFront', body.documentImgFront);
        formData.append('documentImgBack', body.documentImgBack);
        formData.append('userID', body.userID);
        formData.append('kycType', body.kycType);

        return this._httpClient.post<any>(environment.apiUrl + '/api/onboarding/tier1KycChecking', formData);
    }

    accountEnquiry(body: { creditorAccountType: string, receiverBIC: string, creditorAccountNo: any, secondValidationIndicator: string, idType: any, idNo: any, acquaintmeStatus: string, acquaintmeReference: string, fullName: string }): Observable<any> {
        return this._httpClient.post<any>(environment.apiUrl + '/api/wallet/ambankAccountEnquiry', body);
    }
}
