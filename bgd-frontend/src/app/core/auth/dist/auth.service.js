"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AuthService = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var environment_1 = require("environments/environment");
var AuthService = /** @class */ (function () {
    /**
     * Constructor
     */
    function AuthService(_httpClient, _userService) {
        this._httpClient = _httpClient;
        this._userService = _userService;
        this._authenticated = false;
        // -----------------------------------------------------------------------------------------------------
        // @ Accessors
        // -----------------------------------------------------------------------------------------------------
        /**
         * Setter & getter for access token
         */
        this.loggedInKey = 'isLoggedIn';
    }
    Object.defineProperty(AuthService.prototype, "accessToken", {
        get: function () {
            var _a;
            return (_a = sessionStorage.getItem('accessToken')) !== null && _a !== void 0 ? _a : '';
        },
        set: function (token) {
            sessionStorage.setItem('accessToken', token);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "refreshToken", {
        get: function () {
            var _a;
            return (_a = sessionStorage.getItem('refreshtoken')) !== null && _a !== void 0 ? _a : '';
        },
        set: function (refreshtoken) {
            sessionStorage.setItem('refreshtoken', refreshtoken);
        },
        enumerable: false,
        configurable: true
    });
    AuthService.prototype.isLoggedIn = function () {
        // Check if the isLoggedIn flag is present in localStorage
        return sessionStorage.getItem(this.loggedInKey) === 'true';
    };
    AuthService.prototype.initLogoutOnBrowserClose = function () {
        var _this = this;
        window.addEventListener('beforeunload', function () {
            _this.signOut();
        });
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Forgot password
     *
     * @param email
     */
    AuthService.prototype.forgotPassword = function (forgotPassword) {
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/onboarding/generateResetPassword', forgotPassword, { responseType: 'json' });
    };
    /**
     * Reset password
     *
     * @param password
     */
    AuthService.prototype.ResetPassword = function (resetPassword) {
        return this._httpClient.put(environment_1.environment.apiUrl + '/api/onboarding/resetPassword', resetPassword, { responseType: 'text' });
    };
    AuthService.prototype.ChangePassword = function (ChangePassword) {
        return this._httpClient.put(environment_1.environment.apiUrl + '/api/onboarding/changePassword', ChangePassword, { responseType: 'text' });
    };
    AuthService.prototype.checkSecurityQuestion = function (email) {
        var headers = new http_1.HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        var url = "/api/onboarding/securityQuestion?email=" + encodeURIComponent(email);
        return this._httpClient.get(environment_1.environment.apiUrl + url, { responseType: 'text' });
    };
    /**
     * Sign in
     *
     * @param credentials
     */
    AuthService.prototype.signIn = function (credentials) {
        var _this = this;
        // Throw error, if the user is already logged in
        if (this._authenticated) {
            return rxjs_1.throwError('User is already logged in.');
        }
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/onboarding/login', credentials, { headers: environment_1.environment.credentials }).pipe(rxjs_1.switchMap(function (response) {
            //         // Store the access token in the local storage
            _this.accessToken = response.access_token;
            _this.refreshToken = response.refresh_token;
            //         // Set the authenticated flag to true
            _this._authenticated = true;
            //         // Store the user on the user service
            //         this._userService.user = response.user;
            //         // Return a new observable with the response
            return rxjs_1.of(response);
        }));
    };
    /**
     * Sign in using the access token
     */
    AuthService.prototype.signInUsingToken = function () {
        if (this.accessToken !== null) {
            return rxjs_1.of(true);
        }
        else {
            return rxjs_1.of(false);
        }
        // // Sign in using the token
        // return this._httpClient.post(environment.apiUrl + '/api/onboarding/refreshToken', {
        //     refreshToken: this.refreshToken
        // }, {
        //     headers: { 'client': 'bgd-client', 'secret': '2IiLzXEF5OIIhmuINQzLsGhfDYFjNNv2' }
        // }).pipe(
        //     catchError(() =>
        //         // Return false
        //         of(false)
        //     ),
        //     switchMap((response: any) => {
        //         // Replace the access token with the new one if it's available on
        //         // the response object.
        //         //
        //         // This is an added optional step for better security. Once you sign
        //         // in using the token, you should generate a new one on the server
        //         // side and attach it to the response object. Then the following
        //         // piece of code can replace the token with the refreshed one.
        //         if (response.access_token) {
        //             this.accessToken = response.access_token;
        //         }
        //         // Set the authenticated flag to true
        //         this._authenticated = true;
        //         // Store the user on the user service
        //         //this._userService.user = response.user;
        //         // Return true
        //         return of(true);
        //     })
        // );
    };
    /**
     * Sign out
     */
    AuthService.prototype.signOut = function () {
        // Remove the access token from the local storage
        sessionStorage.removeItem('accessToken');
        // Clear the isLoggedIn flag in localStorage
        sessionStorage.removeItem(this.loggedInKey);
        sessionStorage.removeItem('refreshToken');
        // Set the authenticated flag to false
        this._authenticated = false;
        // Return the observable
        return rxjs_1.of(true);
    };
    /**
     * Sign up
     *
     * @param user
     */
    AuthService.prototype.signUp = function (user) {
        return this._httpClient.post('api/auth/sign-up', user);
    };
    /**
     * Register
     *
     * @param user
     */
    AuthService.prototype.register = function (user) {
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/onboarding/registerIndividual', user);
    };
    /**
     * Unlock session
     *
     * @param credentials
     */
    AuthService.prototype.unlockSession = function (credentials) {
        return this._httpClient.post('api/auth/unlock-session', credentials);
    };
    /**
     * Check the authentication status
     */
    AuthService.prototype.check = function () {
        // Check if the user is logged in
        if (this._authenticated) {
            return rxjs_1.of(true);
        }
        // Check the access token availability
        if (!this.accessToken) {
            return rxjs_1.of(false);
        }
        // Check the access token expire date
        //if ( AuthUtils.isTokenExpired(this.accessToken) )
        //{
        //   return of(false);
        //}
        // If the access token exists and it didn't expire, sign in using it
        return this.signInUsingToken();
    };
    /**
     * Verify Otp
     *
     * @param user
     */
    AuthService.prototype.verifyOtp = function (otp) {
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/onboarding/verifyOTP', otp);
    };
    AuthService.prototype.createProfilePicture = function (formData) {
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/onboarding/createProfilePicture', formData);
    };
    AuthService.prototype.setupPin = function (pin) {
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/onboarding/pin', pin);
    };
    AuthService.prototype.resetPin = function (pin) {
        return this._httpClient.put(environment_1.environment.apiUrl + '/api/onboarding/resetPin', pin);
    };
    AuthService.prototype.changePin = function (pin) {
        return this._httpClient.put(environment_1.environment.apiUrl + '/api/onboarding/pin', pin);
    };
    AuthService.prototype.bankList = function () {
        return this._httpClient.get(environment_1.environment.apiUrl + '/api/onboarding/bankList');
    };
    //get user details
    AuthService.prototype.getUserDetails = function () {
        return this._httpClient.get(environment_1.environment.apiUrl + '/api/onboarding/getUserDetails');
    };
    AuthService.prototype.verifyKYC = function (body) {
        var formData = new FormData();
        formData.append('faceImg', body.faceImg);
        formData.append('documentImgFront', body.documentImgFront);
        formData.append('documentImgBack', body.documentImgBack);
        formData.append('userID', body.userID);
        formData.append('kycType', body.kycType);
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/onboarding/tier1KycChecking', formData);
    };
    AuthService.prototype.accountEnquiry = function (body) {
        return this._httpClient.post(environment_1.environment.apiUrl + '/api/wallet/ambankAccountEnquiry', body);
    };
    AuthService = __decorate([
        core_1.Injectable()
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
