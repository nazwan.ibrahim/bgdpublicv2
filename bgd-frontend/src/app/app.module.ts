import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FuseConfigModule } from '@bgd/services/config';
import { FuseMockApiModule } from '@bgd/lib/mock-api';
import { CoreModule } from 'app/core/core.module';
import { appConfig } from 'app/core/config/app.config';
import { mockApiServices } from 'app/mock-api';
import { LayoutModule } from 'app/layout/layout.module';
import { AppComponent } from 'app/app.component';
import { BgdModule } from '@bgd';
import { DetailsPasswordComponent } from './modules/auth/details-password/details-password.component';
import { SecurityQuestionComponent } from './modules/auth/security-question/security-question.component';
import { SetupLoginPreferenceComponent } from './modules/auth/setup-login-preference/setup-login-preference.component';
import { LoginPreferenceComponent } from './modules/auth/login-preference/login-preference.component';
import { SetupPinComponent } from './modules/auth/setup-pin/setup-pin.component';
// import { SetupSuccessComponent } from './modules/auth/setup-success/setup-success.component';
import { BiometricEnabledComponent } from './modules/auth/biometric-enabled/biometric-enabled.component';
import { DefaultLoginComponent } from './modules/auth/default-login/default-login.component';
import { UpgradeAccountComponent } from './modules/auth/upgrade-account/upgrade-account.component';
import { IdentityVerificationComponent } from './modules/auth/identity-verification/identity-verification.component';
import { SelectIdTypeComponent } from './modules/auth/select-id-type/select-id-type.component';
import { VerifyMykadComponent } from './modules/auth/verify-mykad/verify-mykad.component';
import { ImageIdCaptureComponent } from './modules/auth/image-id-capture/image-id-capture.component';
import { ImageIdPreviewComponent } from './modules/auth/image-id-preview/image-id-preview.component';
import { CashWalletTopupComponent } from './modules/admin/cash-wallet-topup/cash-wallet-topup.component';
import { CashWalletTopupAmountComponent } from './modules/admin/cash-wallet-topup-amount/cash-wallet-topup-amount.component';
import { CashWalletTopupMethodComponent } from './modules/admin/cash-wallet-topup-method/cash-wallet-topup-method.component';
import { CashWalletTopupBankComponent } from './modules/admin/cash-wallet-topup-bank/cash-wallet-topup-bank.component';
import { CashWalletTopupPaynetComponent } from './modules/admin/cash-wallet-topup-paynet/cash-wallet-topup-paynet.component';
import { CashWalletTopupStatusComponent } from './modules/admin/cash-wallet-topup-status/cash-wallet-topup-status.component';
import { CashWalletTopupPendingComponent } from './modules/admin/cash-wallet-topup-pending/cash-wallet-topup-pending.component';
import { GoldWalletComponent } from './modules/admin/gold-wallet/gold-wallet.component';
import { CashWalletWithdrawComponent } from './modules/admin/cash-wallet-withdraw/cash-wallet-withdraw.component';
import { CashWalletWithdrawBankComponent } from './modules/admin/cash-wallet-withdraw-bank/cash-wallet-withdraw-bank.component';
import { AppRoutingModule } from './app-routing.module';
import { Error404Component } from './modules/error/error-404/error-404.component';
import { Error500Component } from './modules/error/error-500/error-500.component';
import { MyAccountComponent } from './modules/admin/my-account/my-account.component';
import { TransferBgdAmountComponent } from './modules/admin/bgd/transfer/transfer-bgd-amount/transfer-bgd-amount.component';
import { TransferBgdConfirmationComponent } from './modules/admin/bgd/transfer/transfer-bgd-confirmation/transfer-bgd-confirmation.component';
import { TransferBgdPinComponent } from './modules/admin/bgd/transfer/transfer-bgd-pin/transfer-bgd-pin.component';
import { TransferBgdStatusComponent } from './modules/admin/bgd/transfer/transfer-bgd-status/transfer-bgd-status.component';
import { TransferGoldComponent } from './modules/admin/bgd/transfer/transfer-gold/transfer-gold.component';
import { RedeemBgdComponent } from './modules/admin/bgd/redeem/redeem-bgd/redeem-bgd.component';
import { RedeemBgdAmountComponent } from './modules/admin/bgd/redeem/redeem-bgd-amount/redeem-bgd-amount.component';
import { RedeemBgdAddressComponent } from './modules/admin/bgd/redeem/redeem-bgd-address/redeem-bgd-address.component';
import { RedeemBgdDeliveryAddressComponent } from './modules/admin/bgd/redeem/redeem-bgd-delivery-address/redeem-bgd-delivery-address.component';
import { RedeemBgdConfirmationComponent } from './modules/admin/bgd/redeem/redeem-bgd-confirmation/redeem-bgd-confirmation.component';
import { RedeemBgdPinComponent } from './modules/admin/bgd/redeem/redeem-bgd-pin/redeem-bgd-pin.component';
import { RedeemBgdReceiptComponent } from './modules/admin/bgd/redeem/redeem-bgd-receipt/redeem-bgd-receipt.component';
import { CashWalletWithdrawPinComponent } from './modules/admin/cash-wallet-withdraw-pin/cash-wallet-withdraw-pin.component';
import { CashWalletTopupFailedComponent } from './modules/admin/cash-wallet-topup-failed/cash-wallet-topup-failed.component';
import { TransactionHistoryDetailsComponent } from './modules/admin/transaction-history-details/transaction-history-details.component';
import { ReferralRedeemComponent } from './modules/admin/referral-redeem/referral-redeem.component';
import { GoldPriceDetailsComponent } from './modules/gold-price-details/gold-price-details.component';
import { TierComponent } from './modules/auth/tier/tier.component';
import { TierAllComponent } from './modules/auth/tier-all/tier-all.component';
import { ReferralStatusComponent } from './modules/admin/referral-status/referral-status.component';
import { CashWalletWithdrawStatusComponent } from './modules/admin/cash-wallet-withdraw-status/cash-wallet-withdraw-status.component';
import { PrivacyNoticesComponent } from './modules/admin/my-account/cards/privacy-notices/privacy-notices.component';
import { RedeemBgdStatusComponent } from './modules/admin/bgd/redeem/redeem-bgd-status/redeem-bgd-status.component';
import { ChangePasswordComponent } from 'app/modules/admin/my-account/cards/change-password/change-password.component';
import { TransferLandingComponent } from './modules/admin/bgd/transfer/transfer-landing/transfer-landing.component';
import { ChangePasswordNewComponent } from 'app/modules/admin/my-account/cards/change-password-new/change-password-new.component';
import { SecurityComponent } from 'app/modules/admin/my-account/cards/security/security.component';
import { ChangePasswordPinComponent } from 'app/modules/admin/my-account/cards/change-password-pin/change-password-pin.component';
import { ChangePinComponent } from 'app/modules/admin/my-account/cards/change-pin/change-pin.component';
import { SetupPinOtpComponent } from './modules/auth/setup-pin-otp/setup-pin-otp.component';
// import { BuyBgdPinComponent } from './modules/admin/bgd/buy/buy-bgd-pin/buy-bgd-pin.component';
//import { SetupPinLandingComponent } from './modules/auth/setup-pin-landing/setup-pin-landing.component';
import { SellBgdPinComponent } from './modules/admin/bgd/sell/sell-bgd-pin/sell-bgd-pin.component';
import { SetupPinLandingComponent } from './modules/auth/setup-pin-landing/setup-pin-landing.component';
import { CameraUnavailableComponent } from './modules/auth/camera-unavailable/camera-unavailable.component';
import { ForgotPinComponent } from './modules/auth/forgot-pin/forgot-pin.component';
import { TransactionLimitComponent } from './modules/auth/transaction-limit/transaction-limit.component';
import { CashWalletWithdrawPendingComponent } from './modules/admin/cash-wallet-withdraw-pending/cash-wallet-withdraw-pending.component';
import { CashWalletTopupConfirmationComponent } from './modules/admin/cash-wallet-topup-confirmation/cash-wallet-topup-confirmation.component';
import { ComingSoonComponent } from './modules/error/coming-soon/coming-soon.component';
// import { CashWalletTopupRejectedScreenComponent } from './modules/admin/cash-wallet-topup-rejected-screen/cash-wallet-topup-rejected-screen.component';
// import { CashWalletTopupRejectedComponent } from './modules/admin/cash-wallet-topup-rejected/cash-wallet-topup-rejected.component';


@NgModule({
    declarations: [
        AppComponent,
        DetailsPasswordComponent,
        SecurityQuestionComponent,
        SetupLoginPreferenceComponent,
        LoginPreferenceComponent,
        //SetupPinComponent,
        // SetupSuccessComponent,
        BiometricEnabledComponent,
        DefaultLoginComponent,
        //PrivacyNoticesComponent
        //UpgradeAccountComponent,
        //IdentityVerificationComponent,
        SelectIdTypeComponent,
        //VerifyMykadComponent,
        //ImageIdCaptureComponent,
        ImageIdPreviewComponent,
        //SecurityComponent,
        //CashWalletTopupComponent,
        //CashWalletTopupAmountComponent,
        //CashWalletTopupMethodComponent,
        //CashWalletTopupBankComponent,
        //ChangePasswordComponent,
        //ChangePasswordNewComponent,
        //ChangePasswordPinComponent,
        //ChangePinComponent,
        //CashWalletTopupPaynetComponent,
        //CashWalletTopupStatusComponent,
        GoldWalletComponent,
        // CashWalletWithdrawComponent,
        // CashWalletWithdrawBankComponent,
        Error500Component,
        Error404Component,
        // CashWalletWithdrawPinComponent,
        // CashWalletTopupFailedComponent,
        ReferralRedeemComponent,
        GoldPriceDetailsComponent,
        //TierComponent,
        //TierAllComponent,
        ReferralStatusComponent,
        ComingSoonComponent,
        // CashWalletTopupRejectedScreenComponent,
        // CashWalletTopupRejectedComponent,
        //CashWalletTopupConfirmationComponent,
        //CashWalletWithdrawPendingComponent,
        //TransactionLimitComponent,
        //ForgotPinComponent,
        // CameraUnavailableComponent,
        // BuyBgdPinComponent,
        //SellBgdPinComponent,
        //SetupPinLandingComponent,
        //SetupPinOtpComponent,
        //RedeemBgdStatusComponent,
        //CashWalletWithdrawStatusComponent,
        //TransferLandingComponent,
        //TransferBgdAmountComponent,
        //TransferBgdConfirmationComponent,
        //TransferBgdPinComponent,
        //TransferBgdStatusComponent,
        //TransferGoldComponent,
        //RedeemBgdComponent,
        //RedeemBgdAmountComponent,
        //RedeemBgdAddressComponent,
        //RedeemBgdDeliveryAddressComponent,
        //RedeemBgdConfirmationComponent,
        //RedeemBgdPinComponent,
        //RedeemBgdReceiptComponent,
        //MyAccountComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        // RouterModule.forRoot(appRoutes, routerConfig),

        // Fuse, FuseConfig & FuseMockAPI
        BgdModule,
        FuseConfigModule.forRoot(appConfig),
        FuseMockApiModule.forRoot(mockApiServices),

        // Core module of your application
        CoreModule,

        // Layout module of your application
        LayoutModule
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
