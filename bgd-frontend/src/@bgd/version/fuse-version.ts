import { Version } from '@bgd/version/version';

export const FUSE_VERSION = new Version('17.0.1').full;
