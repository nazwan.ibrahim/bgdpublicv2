# FROM node:18

# # Create app directory
# WORKDIR /usr/src/app

# # Install app dependencies
# # A wildcard is used to ensure both package.json AND package-lock.json are copied
# # where available (npm@5+)
# COPY package*.json ./

# RUN npm install
# RUN npm install pm2 -g
# # If you are building your code for production
# # RUN npm ci --only=production

# # Bundle app source
# COPY . .

# EXPOSE 4200

# CMD ["pm2 start", "ng serve"]
# # CMD [ "node", "index.js" ]


### STAGE 1: Build ###
FROM node:16.13.0 AS build
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

### STAGE 2: Run ###
FROM nginx:latest

# Create the /app directory
# RUN mkdir /app

# Create the client_temp and proxy_temp directories with appropriate permissions
RUN mkdir -p /var/cache/nginx/client_temp /var/cache/nginx/proxy_temp && \
    chown -R nginx:nginx /var/cache/nginx

RUN touch /var/run/nginx.pid && \
        chown -R nginx:nginx /var/run/nginx.pid

COPY nginx.conf /etc/nginx/nginx.conf
COPY error_page.html /usr/share/nginx/html/error_page.html
COPY --from=build /usr/src/app/dist /usr/share/nginx/html

# No need to create appuser and appgroup as they are not used in this setup
USER nginx

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]