package com.company.trade_engine.app;

import com.company.trade_engine.entity.AccountBgd;
import com.company.trade_engine.entity.PrimOrderDTO;
import com.company.trade_engine.entity.SecOrderDTO;
import io.jmix.core.DataManager;

import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.security.SystemAuthenticator;
import org.apache.kafka.common.protocol.types.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@RestController
@RequestMapping("testService")
public class testService {

    Logger logger = LoggerFactory.getLogger(PrimaryTradingApiController.class);

    @Autowired
    private DataManager dataManager;

    ExecutorService execution = Executors.newSingleThreadExecutor();
    ExecutorService execution2 = Executors.newSingleThreadExecutor();

    @Autowired
    private SystemAuthenticator systemAuthenticator;
    @Autowired
    private CurrentAuthentication currentAuthentication;

    int count=1;
    Random rand = new Random();
    List<String> list = new ArrayList<>();

    @PostMapping("/multiplethread")
    public void test(int n, String purchaseType, BigDecimal quantity){
        // add 5 element in ArrayList
        list.add("yan");
        list.add("yan2");
        list.add("yan3");
        list.add("yan4");
        list.add("yan5");
        list.add("yan6");
        list.add("yan7");
        list.add("yan8");
        list.add("yan9");
        list.add("yan10");

        int threadCount = n;
        count=1;
        execution.execute(new Runnable() {
            @Override
            public void run() {
                    systemAuthenticator.runWithSystem(()->{
                        try {
                            logger.info("First service simulator start :-");
                            testParallelExecution(threadCount,purchaseType,quantity);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
            }
        });
//        execution2.execute(new Runnable() {
//            @Override
//            public void run() {
//                systemAuthenticator.runWithSystem(()->{
//                    try {
//                        logger.info("Second service simulator start :-");
//                        testParallelExecution();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                });
//            }
//        });
    }

    public void buy(String purchaseType, BigDecimal quantity){

        PrimOrderDTO newp = dataManager.create(PrimOrderDTO.class);

        newp.setPrice(BigDecimal.valueOf(200));
        newp.setQuantity(quantity);
        newp.setType(purchaseType);

        String uri= environment.getProperty("primaryTrading/purchase");
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.postForObject(uri, newp , String.class);
        logger.info("TEST "+count+" " +purchaseType+": "+ newp.getQuantity() + "at : RM "+newp.getPrice());
        count=count+1;
    }

    public void testParallelExecution(int threadCount,String purchaseType,BigDecimal quantity)  throws InterruptedException {

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();
        logger.info("TEST STARTED");

        CountDownLatch startLatch = new CountDownLatch(1);
        CountDownLatch endLatch = new CountDownLatch(threadCount);

        for (int i = 0; i < threadCount; i++) {
            new Thread(() -> {
                try {
                    startLatch.await();
                    systemAuthenticator.runWithSystem(()->{
//                        buy(purchaseType,quantity);
                        marketplaceTest(quantity,purchaseType);
//                        addBalance();
                    });
                } catch (Exception e) {
                    logger.error("Test failed", e);
                } finally {
                    endLatch.countDown();
                }
            }).start();
        }
        startLatch.countDown();
        endLatch.await();


        stopWatch.stop();
        logger.info("TEST ENDED , Testing time taken: " + stopWatch.getTotalTimeSeconds());
    }


    public void marketplaceTest(BigDecimal size, String purchaseType){

        SecOrderDTO secOrderDTO = dataManager.create(SecOrderDTO.class);
        secOrderDTO.setUserID(123);
        secOrderDTO.setSize(size);
        secOrderDTO.setPrice(BigDecimal.valueOf(200));
        secOrderDTO.setMethod("LIMIT");

        String uri= environment.getProperty("secondaryTradinguri")+purchaseType.toLowerCase();

        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.postForObject(uri,secOrderDTO , String.class);
        logger.info("TEST "+count+" " +purchaseType+" LIMIT"+": "+ size + "at : RM "+secOrderDTO.getPrice());
        count=count+1;
    }


}
