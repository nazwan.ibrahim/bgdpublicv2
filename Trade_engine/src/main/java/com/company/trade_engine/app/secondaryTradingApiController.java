package com.company.trade_engine.app;

import com.company.trade_engine.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.security.SystemAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


@RestController
@RequestMapping("/secondaryTrading")
public class secondaryTradingApiController {

    @Autowired
    private DataManager dataManager;

    Logger logger = LoggerFactory.getLogger(PrimaryTradingApiController.class);

    List<SecOrderBook> secOrderBookBuyQueue = new ArrayList<>();
    List<SecOrderBook> secOrderBookSellQueue = new ArrayList<>();


    @Autowired
    private SimpMessagingTemplate template;


    @Scheduled(fixedDelay = 30000)
    public void webSocketScheduler(){
        logger.info("Update WEBSOCKET MESSAGE");
        buyOrderBook("scheduler call");
    }

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationReadyEvent.class)
//    @EventListener(ContextRefreshedEvent.class)
    public void doSomethingAfterStartup() {
        rebuildOrderBook();

    }


    /**       Web Socket for orderbook feed     ********/
    @MessageMapping("/orderBook")
//    @SendTo("/topic/buyOrder")
    public void buyOrderBook(String payload){
//        return marketBuyPrice;
        template.convertAndSend("/topic/buyOrder",secOrderBookBuyQueue);
        template.convertAndSend("/topic/sellOrder",secOrderBookSellQueue);
        logger.info("websocket run :" +payload);
}

    @GetMapping("/orderBookBuy")
    public ResponseEntity orderBookBuyList(){
        return ResponseEntity.status(HttpStatus.OK).body(secOrderBookBuyQueue);

    }
    @GetMapping("/orderBookSell")
    public ResponseEntity orderBookSellList(){
        return ResponseEntity.status(HttpStatus.OK).body(secOrderBookSellQueue);

    }

    @GetMapping("/buyVolume")
    public ResponseEntity orderBookBuyVolume(){
        return ResponseEntity.status(HttpStatus.OK).body(secOrderBookBuyQueue.size());
    }
    @GetMapping("/sellVolume")
    public ResponseEntity orderBookSellVolume(){
        return ResponseEntity.status(HttpStatus.OK).body(secOrderBookSellQueue.size());
    }



    @PostMapping("/buy")
    public ResponseEntity buy(@RequestBody @Valid SecOrderDTO buyOrder){

        try {

            String userName = "yan4";

            buyOrder.setFilled(BigDecimal.valueOf(0));

            //save order to db
            SecOrderStore newOrderStore = dataManager.create(SecOrderStore.class);
            newOrderStore.setUserID(buyOrder.getUserID());
            newOrderStore.setPrice(buyOrder.getPrice());

            newOrderStore.setQuantity(buyOrder.getSize());

            newOrderStore.setType(PurchaseRequestType.BUY);
            newOrderStore.setFilled(buyOrder.getFilled());
            newOrderStore.setStatus("ZERO_FILLED");
            newOrderStore.setUserName(userName);
            dataManager.save(newOrderStore);

            //check order has match or not
            //if match
            //if not match/partially match insert at the right place
            writeLock.lock();

            while (buyOrder.getSize().signum() == 1) {

                if (secOrderBookSellQueue.size() == 0 && buyOrder.getMethod().equals("MARKET")) {
                    // not enough volume
                    // handle
                    updateStore(newOrderStore.getId(), "NO_VOLUME", buyOrder.getFilled());
                    break;
                }
                // if market order, no need to check price, will filled all available
                if ((secOrderBookSellQueue.size() != 0) && (buyOrder.getMethod().equals("MARKET") || buyOrder.getPrice().compareTo(secOrderBookSellQueue.get(0).getPrice()) >= 0)) {
                    logger.info("Has Match ");
//                  BigDecimal minSize = sellOrder.getSize().min(secOrderBookBuyQueue.get(0).getSize());

                    //if market buy order, user will input size in total cash, so need to convert to gram for every new matching price
                    if (buyOrder.getMethod().equals("MARKET")) {
                        BigDecimal cashToGram = buyOrder.getSize().divide(secOrderBookSellQueue.get(0).getPrice(), 10, RoundingMode.HALF_UP);
                        buyOrder.setSize(cashToGram);
                    }

                    BigDecimal diff = buyOrder.getSize().subtract(secOrderBookSellQueue.get(0).getSize());

                    System.out.println("diff" + diff);
                    if (diff.signum() >= 0) {
                        //min value is sellQueue order

                        buyOrder.setSize(diff);
                        if (buyOrder.getMethod().equals("LIMIT")) {
                            buyOrder.setFilled(buyOrder.getFilled().add(secOrderBookSellQueue.get(0).getSize()));
                        } else if (buyOrder.getMethod().equals("MARKET")) {
                            buyOrder.setFilled(buyOrder.getFilled().add(secOrderBookSellQueue.get(0).getSize()).multiply(secOrderBookSellQueue.get(0).getPrice()));
                        }
                        //Update Store
                        updateStore(secOrderBookSellQueue.get(0).getStoreID(), "ALL_FILLED", secOrderBookSellQueue.get(0).getFilled().add(secOrderBookSellQueue.get(0).getSize()));
                        if (diff.signum() == 0) { //if both size equals meaans both are all filled
                            updateStore(newOrderStore.getId(), "ALL_FILLED", buyOrder.getFilled());
                        } else
                            updateStore(newOrderStore.getId(), "PARTIAL_FILLED", buyOrder.getFilled());

                        //if market buy, we convert back the remaining unfilled gram into cash , for the next iteration
                        if (buyOrder.getMethod().equals("MARKET")) {
                            buyOrder.setSize(buyOrder.getSize().multiply(secOrderBookSellQueue.get(0).getPrice()));
                        }

                        secOrderBookSellQueue.remove(0);
                    } else {
                        //min value is buy order
                        secOrderBookSellQueue.get(0).setFilled(secOrderBookSellQueue.get(0).getFilled().add(buyOrder.getSize()));
                        secOrderBookSellQueue.get(0).setSize(secOrderBookSellQueue.get(0).getSize().subtract(buyOrder.getSize()));

                        if (buyOrder.getMethod().equals("LIMIT")) {
                            buyOrder.setFilled(buyOrder.getFilled().add(buyOrder.getSize()));
                            buyOrder.setSize(diff);//will be negatif
                        } else if (buyOrder.getMethod().equals("MARKET")) {
                            buyOrder.setFilled(buyOrder.getFilled().add(buyOrder.getSize().multiply(secOrderBookSellQueue.get(0).getPrice())));
                            buyOrder.setSize(diff.multiply(secOrderBookSellQueue.get(0).getPrice()));//will be negatif
                        }

                        updateStore(secOrderBookSellQueue.get(0).getStoreID(), "PARTIAL_FILLED", secOrderBookSellQueue.get(0).getFilled());
                        updateStore(newOrderStore.getId(), "ALL_FILLED", buyOrder.getFilled());

                    }
                } else {
                    insertIntoBuyQueue(buyOrder, newOrderStore.getId(),userName);
                    break;
                }
            }
//            buyOrder.setStatus("SUCCESSFUL");
            return ResponseEntity.status(HttpStatus.OK).body("SUCCESSFUL");
        }catch (Exception e){
            logger.info("REASON : "+e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        finally {
            writeLock.unlock();
        }
    }


    @PostMapping("/sell")
    public ResponseEntity sell(@RequestBody @Valid SecOrderDTO sellOrder){

        try {

            String userName = "yan4";
            BigDecimal feeRate = BigDecimal.valueOf(0);

            sellOrder.setFilled(BigDecimal.valueOf(0));

            //save order to db
            SecOrderStore newOrderStore= dataManager.create(SecOrderStore.class);
            newOrderStore.setUserID(sellOrder.getUserID());
            newOrderStore.setPrice(sellOrder.getPrice());
            newOrderStore.setQuantity(sellOrder.getSize());
            newOrderStore.setType(PurchaseRequestType.SELL);
            newOrderStore.setFilled(sellOrder.getFilled());
            newOrderStore.setStatus("ZERO_FILLED");
            newOrderStore.setUserName(userName);
            dataManager.save(newOrderStore);



            //check order has match or not
            //if match
            //if not match/partially match insert at the right place
            writeLock.lock();

            while(sellOrder.getSize().signum()==1) {

                if(secOrderBookBuyQueue.size() ==0  &&  sellOrder.getMethod().equals("MARKET")){
                    // not enough volume
                    // handle
                    updateStore(newOrderStore.getId(),"NO_VOLUME",sellOrder.getFilled());
                    break;
                }

                //if limit need to check price has matching or not,but if market will match with the best available
                if ((secOrderBookBuyQueue.size() != 0) && (sellOrder.getMethod().equals("MARKET") || sellOrder.getPrice().compareTo(secOrderBookBuyQueue.get(0).getPrice()) <= 0)) {
                    logger.info("Has Match ");
    //                BigDecimal minSize = sellOrder.getSize().min(secOrderBookBuyQueue.get(0).getSize());

                    BigDecimal diff= sellOrder.getSize().subtract(secOrderBookBuyQueue.get(0).getSize());
    //                sellOrder.setSize(sellOrder.getSize().subtract(secOrderBookBuyQueue.get(0).getSize()));

                    System.out.println("diff"+diff);
                    if (diff.signum() >= 0) {
                        //The min is at buyQueue
    //                    secOrderBookBuyQueue.get(0).setFilled(secOrderBookBuyQueue.get(0).getFilled().add(secOrderBookBuyQueue.get(0).getSize()));
    //ss
                        sellOrder.setSize(diff); //update size
                        sellOrder.setFilled(sellOrder.getFilled().add(secOrderBookBuyQueue.get(0).getSize()));
                        updateStore(secOrderBookBuyQueue.get(0).getStoreID(),"ALL_FILLED", secOrderBookBuyQueue.get(0).getFilled().add(secOrderBookBuyQueue.get(0).getSize()));
                        if(diff.signum()==0) { //if both size equals meaans both are all filled
                            updateStore(newOrderStore.getId(), "ALL_FILLED", sellOrder.getFilled());
                        }
                        else
                            updateStore(newOrderStore.getId(), "PARTIAL_FILLED", sellOrder.getFilled());

                        executeTransferThread(
                                        secOrderBookBuyQueue.get(0).getUserName(),
                                        userName,
                                        secOrderBookBuyQueue.get(0).getSize(),
                                        secOrderBookBuyQueue.get(0).getPrice(),
                                        feeRate
                                );

//                        //transfer seller asset to buyer
//                        createTransaction(assetWallet,getWallet(secOrderBookBuyQueue.get(0).getUserName(), "E02"),secOrderBookBuyQueue.get(0).getSize(),TX_TYPE_SELL,TX_TYPE_BUY);
//                        //transfer buyer cash to seller
//                        createTransaction(getWallet(secOrderBookBuyQueue.get(0).getUserName(),"E01"), cashWallet,secOrderBookBuyQueue.get(0).getSize().multiply(secOrderBookBuyQueue.get(0).getPrice()),TX_TYPE_SELL,TX_TYPE_BUY);
//                        //transfer fee to bursa
//                        createTransaction(assetWallet,WALLET_BURSA_ASSET,feeRate.multiply(secOrderBookBuyQueue.get(0).getSize()),TX_TYPE_TRANSACTION_FEE,TX_TYPE_TRANSACTION_FEE);


                        secOrderBookBuyQueue.remove(0);
                    }
                    else{
                        //The min is at sell Order
                        secOrderBookBuyQueue.get(0).setFilled(secOrderBookBuyQueue.get(0).getFilled().add(sellOrder.getSize()));
                        secOrderBookBuyQueue.get(0).setSize(secOrderBookBuyQueue.get(0).getSize().subtract(sellOrder.getSize()));
                        sellOrder.setFilled(sellOrder.getFilled().add(sellOrder.getSize()));

                        updateStore(secOrderBookBuyQueue.get(0).getStoreID(),"PARTIAL_FILLED", secOrderBookBuyQueue.get(0).getFilled());
                        updateStore(newOrderStore.getId(),"ALL_FILLED",sellOrder.getFilled());

                        sellOrder.setSize(diff);
                    }
                } else{
                    insertIntoSellQueue(sellOrder,newOrderStore.getId(),userName);
                    break;
                }
            }
//            sellOrder.setStatus("SUCCESSFUL");
            return ResponseEntity.status(HttpStatus.OK).body("SUCCESSFUL");
        }catch (Exception e){
            logger.info("REASON : "+e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
        finally {
            writeLock.unlock();
        }

    }

    @Autowired
    private SystemAuthenticator systemAuthenticator;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    ExecutorService excTransaction = Executors.newFixedThreadPool(100);
    ExecutorService exctry = Executors.newFixedThreadPool(10);

    private final ExecutorCompletionService<String> completionService = new ExecutorCompletionService<>(exctry);


    public void executeTransferThread(String buyer,String seller, BigDecimal amount, BigDecimal price,BigDecimal feeRate){


        completionService.submit(()-> {

                systemAuthenticator.runWithSystem(() -> {

                });
                return "try";

        });

    }

    public void holdWallet(AccountBgd accountBgd, BigDecimal amount){
        AccountHold accountHold = dataManager.create(AccountHold.class);
        accountHold.setAccountBgd(accountBgd);
        accountHold.setAmount(amount);
        dataManager.save();
    }

    public void updateStore(UUID storeID,String status,BigDecimal filled){
        String query1="select s from SecOrderStore s " +
                "where s.id = :id";
        SecOrderStore storedBuyOrder = dataManager.load(SecOrderStore.class)
                .query(query1).parameter("id", storeID)
                .one();
        storedBuyOrder.setFilled(filled);
        storedBuyOrder.setStatus(status);
        dataManager.save(storedBuyOrder);
    }

    public void insertIntoSellQueue(SecOrderDTO sellOrder, UUID storeID,String userName) {
        logger.info("Inserting");

        //Create a buybook instance
        SecOrderBook newSellOrder = new SecOrderBook();
        newSellOrder.setPrice(sellOrder.getPrice());
        newSellOrder.setSize(sellOrder.getSize());
        newSellOrder.setFilled(sellOrder.getFilled());
        newSellOrder.setStatus("Active");
        newSellOrder.setLocked(BigDecimal.valueOf(0));
        newSellOrder.setStoreID(storeID);
        newSellOrder.setUserName(userName);

        if(secOrderBookSellQueue.size()!=0) {
            AtomicInteger n = new AtomicInteger(0);
            AtomicBoolean inserted = new AtomicBoolean(false);
            for (SecOrderBook item : secOrderBookSellQueue) {
                if (sellOrder.getPrice().compareTo(item.getPrice()) < 0) {
                    secOrderBookSellQueue.add(n.get(), newSellOrder);
                    inserted.getAndSet(true);
                    break;
                } else {
                    n.getAndAdd(1);
                }
            }
            if(!inserted.get()){
                //if not yet inserted means dia terakhir dlm queue
                secOrderBookSellQueue.add(newSellOrder);
            }
        }else{
            //means its the only first
            secOrderBookSellQueue.add(newSellOrder);
        }
    }

    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock writeLock = lock.writeLock();
    private Lock readLock = lock.readLock();


    public void insertIntoBuyQueue(SecOrderDTO buyOrder,UUID storeID,String userName){
        //Create a buybook instance
        logger.info("Inserting");

        //Create a buybook instance
        SecOrderBook newBuyOrder= new SecOrderBook();
        newBuyOrder.setPrice(buyOrder.getPrice());
        newBuyOrder.setSize(buyOrder.getSize());
        newBuyOrder.setFilled(buyOrder.getFilled());
        newBuyOrder.setStatus("Active");
        newBuyOrder.setLocked(BigDecimal.valueOf(0));
        newBuyOrder.setStoreID(storeID);
        newBuyOrder.setUserName(userName);


        if(secOrderBookBuyQueue.size()!=0) {
            AtomicInteger n = new AtomicInteger(0);
            AtomicBoolean inserted = new AtomicBoolean(false);

            for (SecOrderBook item : secOrderBookBuyQueue) {
                if (buyOrder.getPrice().compareTo(item.getPrice()) > 0) {
                    secOrderBookBuyQueue.add(n.get(), newBuyOrder);
                    inserted.getAndSet(true);
                    break;
                } else {
                    n.getAndAdd(1);
                }
            }
            if(!inserted.get()){
                //if not yet inserted means dia terakhir dlm queue
                secOrderBookBuyQueue.add(newBuyOrder);
            }
        }
        else{
            //means its the only first
            secOrderBookBuyQueue.add(newBuyOrder);
        }
    }

    @PostMapping("/rebuildOrderBook")
    public ResponseEntity rebuildOrderBook(){
        logger.info("Rebuilding ORDER BOOK....");
        try {
            String query1="select s from SecOrderStore s " +
                    "where (s.status = :status1 " +
                    "or s.status = :status2) " +
                    "and s.type = :type1 " +
                    "order by s.price desc, s.createdDate asc";
            List<SecOrderStore> buyOrderStores = dataManager.load(SecOrderStore.class)
                    .query(query1).parameter("status1", "PARTIAL_FILLED").parameter("status2", "ZERO_FILLED").parameter("type1", "BUY")
                    .list();

            String query2="select s from SecOrderStore s " +
                    "where (s.status = :status1 " +
                    "or s.status = :status2) " +
                    "and s.type = :type1 " +
                    "order by s.price asc, s.createdDate asc";
            List<SecOrderStore> sellOrderStores = dataManager.load(SecOrderStore.class)
                    .query(query2).parameter("status1", "PARTIAL_FILLED").parameter("status2", "ZERO_FILLED").parameter("type1", "SELL")
                    .list();

            secOrderBookBuyQueue.clear(); // make sure everything is cleared
            secOrderBookSellQueue.clear();

            for (SecOrderStore buyOrderStore : buyOrderStores) {
                SecOrderBook ob = new SecOrderBook();
                ob.setStoreID(buyOrderStore.getId());
                ob.setPrice(buyOrderStore.getPrice());
                ob.setSize(buyOrderStore.getQuantity().subtract(buyOrderStore.getFilled()));
                ob.setFilled(buyOrderStore.getFilled());
                ob.setStatus(buyOrderStore.getStatus());
                ob.setUserName(buyOrderStore.getUserName());
                secOrderBookBuyQueue.add(ob);
            }

            for (SecOrderStore sellOrderStore : sellOrderStores) {
                SecOrderBook os = new SecOrderBook();
                os.setStoreID(sellOrderStore.getId());
                os.setPrice(sellOrderStore.getPrice());
                os.setSize(sellOrderStore.getQuantity().subtract(sellOrderStore.getFilled()));
                os.setFilled(sellOrderStore.getFilled());
                os.setStatus(sellOrderStore.getStatus());
                os.setUserName(sellOrderStore.getUserName());
                secOrderBookSellQueue.add(os);
            }
            logger.info("Done Rebuild");
            return ResponseEntity.status(HttpStatus.OK).body("SUCCESSFUL");
        }catch (Exception e){
            logger.info("Rebuild failed because : "+e );
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR");
        }
    }

    @PostMapping("/cancelOrder")
    public ResponseEntity cancelOrder(@RequestBody @Valid SecCancelOrderDTO secCancelOrderDTO){
        AtomicBoolean removed = new AtomicBoolean(false);
        try{
            String query1="select s from SecOrderStore s " +
                    "where s.id = :id1";
            SecOrderStore orderStore = dataManager.load(SecOrderStore.class)
                    .query(query1).parameter("id1", secCancelOrderDTO.getStoreID())
                    .one();
            orderStore.setStatus("CANCELLED");
            dataManager.save(orderStore);

            if(orderStore.getType().toString().equals("BUY")) {
                for (SecOrderBook ob : secOrderBookBuyQueue) {
                    if (ob.getStoreID().equals(secCancelOrderDTO.getStoreID())) {
                        secOrderBookBuyQueue.remove(ob);
                        removed.getAndSet(true);
                        break;
                    }
                }
            }
            else if(orderStore.getType().toString().equals("SELL")){
                for (SecOrderBook os : secOrderBookSellQueue){
                    if(os.getStoreID().equals(secCancelOrderDTO.getStoreID())){
                        secOrderBookSellQueue.remove(os);
                        removed.getAndSet(true);
                        break;
                    }
                }
            }

            return removed.get()? ResponseEntity.status(HttpStatus.OK).body("SUCCESSFUL") : ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Not Found");

        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR");
        }
    }


//    @Scheduled(fixedDelay = 5000)
    public void autoBuySellSimulator() throws InterruptedException {
        Random type= new Random();
//        DecimalFormat formatter = new DecimalFormat("#0.00");  // edited here.
        double randomValue = 0 + Math.random( ) * 1000;
        BigDecimal randPrice = new BigDecimal(randomValue).setScale(2, RoundingMode.HALF_UP);

        int randomQuantity = type.nextInt(10);

        logger.info("NEXT RANDOM PRICE AND QUANTITY ");
        Thread.sleep(2000);
        simulatorApiCall(randPrice,randomQuantity,type.nextBoolean());
    }

    public void simulatorApiCall(BigDecimal randPrice,Integer randomQuantity,Boolean type ){
        String uri= type? environment.getProperty("secondaryTrading/buy"):environment.getProperty("secondaryTrading/sell");

        RestTemplate restTemplate = new RestTemplate();

//        BigDecimal price= new BigDecimal();
        BigDecimal quantity = new BigDecimal(randomQuantity);
        SecOrderDTO secOrderDTO =  dataManager.create(SecOrderDTO.class);
        secOrderDTO.setUserID(123);
        secOrderDTO.setPrice(randPrice);
        secOrderDTO.setSize(quantity);

        SecOrderDTO response = restTemplate.postForObject(uri, secOrderDTO , SecOrderDTO.class);
    }

    @PutMapping("/tryorder")
    public ResponseEntity tryorder(@RequestBody SecOrderStore secOrderStore){

        dataManager.save(secOrderStore);
        return ResponseEntity.status(HttpStatus.OK).body(secOrderStore);
    }

}
