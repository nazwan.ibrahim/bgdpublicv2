package com.company.trade_engine.app;

import com.company.trade_engine.entity.*;

import com.company.trade_engine.services.KafkaConsumer;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.security.SystemAuthenticator;
import liquibase.pro.packaged.A;
import net.minidev.json.JSONObject;
import org.codehaus.groovy.control.CompilationUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.PublicKey;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@RestController
@RequestMapping("/primaryTrading")
public class PrimaryTradingApiController {
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private DataManager dataManager;

    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock writeLock = lock.writeLock();
    private Lock readLock = lock.readLock();
    Logger logger = LoggerFactory.getLogger(PrimaryTradingApiController.class);

    @Autowired
    private SystemAuthenticator systemAuthenticator;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private TimeSource timeSource;

    ExecutorService executorService = Executors.newSingleThreadExecutor();
    ExecutorService excBuyback = Executors.newFixedThreadPool(10);

    public KafkaConsumer kafkaConsumer;

    @Autowired
    PrimaryTradingApiController( KafkaConsumer kafkaConsumer){

        this.kafkaConsumer = kafkaConsumer;
    }

    public void consume(String message) throws IOException {
        logger.info(String.format("#### -> Consumed message -> %s", message));
    }

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {

        logger.info("hello world, I have just started up");

        logger.info("CUrrEnt TIME: "+timeSource.now());
        logger.info("current time:"+timeSource.currentTimestamp());

    }


    @GetMapping("/purchaseInit")
    public ResponseEntity purchaseInit(@RequestBody PrimOrderDTO primOrderDTO){

        try {
            String acccountName = "yan4";

            BigDecimal tradingFee = primOrderDTO.getFee();
            BigDecimal fee = null;
            BigDecimal price = primOrderDTO.getPrice();

            BigDecimal totalCash;
            BigDecimal quantity = null;
            BigDecimal netCash = null;


            if(primOrderDTO.getType().equals("S05")){ //marketplace buy

                if(primOrderDTO.getInputType().equals("S21")){ // buy by cash

                    fee= primOrderDTO.getTotalCash().multiply(tradingFee);

                    totalCash = primOrderDTO.getTotalCash();
                    quantity = (totalCash.subtract(fee)).divide(price,9,RoundingMode.HALF_UP);
                    netCash = totalCash.subtract(fee);

                }
                else{

                    fee = primOrderDTO.getQuantity().multiply(primOrderDTO.getPrice()).multiply(tradingFee);

                    quantity = primOrderDTO.getQuantity();
                    totalCash = (quantity.multiply(price)).add(fee);
                    netCash = totalCash.subtract(fee);

                }

            }
            else if(primOrderDTO.getType().equals("S06")){

                if(primOrderDTO.getInputType().equals("S21")){

                    fee= primOrderDTO.getTotalCash().multiply(tradingFee);

                    totalCash=primOrderDTO.getTotalCash();
                    quantity = (totalCash.add(fee)).divide(price,9,RoundingMode.HALF_UP);

                    netCash = totalCash.add(fee);


                }
                else{

                    fee = primOrderDTO.getQuantity().multiply(primOrderDTO.getPrice()).multiply(tradingFee);

                    quantity =  primOrderDTO.getQuantity();
                    totalCash = quantity.multiply(price).subtract(fee);
                    netCash = totalCash.add(fee);

                }

            }

            PrimOrderDTO returnOrder = dataManager.create(PrimOrderDTO.class);
            returnOrder.setTotalCash(netCash);
            returnOrder.setQuantity(quantity);
            returnOrder.setFee(fee);
            returnOrder.setPrice(primOrderDTO.getPrice());
            returnOrder.setInputType(primOrderDTO.getInputType());
            returnOrder.setType(primOrderDTO.getType());

            logger.info("TotalCash: "+returnOrder.getTotalCash());

            return ResponseEntity.status(HttpStatus.OK).body(returnOrder);
        }
        catch (Exception e){
            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setDate(timeSource.currentTimestamp());
            responseDTO.setStatus("G02");
            responseDTO.setDescription(e.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

//    @Transactional(transactionManager = "walletTransactionManager", rollbackFor = Exception.class)
    @PostMapping("/purchase")
    public ResponseEntity purchase(@RequestBody PrimOrderDTO primOrderDTO)  {

        try {

            for(TradingFeeDTO a : primOrderDTO.getFeesForms()){
                logger.info("Charge Fees: "+a.getChargeFees());
                logger.info("Uom code: "+a.getChargeFees());
                logger.info("Fee Type Code: "+a.getChargeFees());
            }
            logger.info("Net Price: "+primOrderDTO.getNetPrice());
            logger.info("Quantity: "+primOrderDTO.getQuantity());
            logger.info("Purchase Type: "+primOrderDTO.getType());

            BigDecimal quantity = primOrderDTO.getQuantity();
            BigDecimal netCash = primOrderDTO.getNetPrice() ;

            List<TransactionDTO> transactionDTOS= new ArrayList<>();

            if(primOrderDTO.getType().equals("S05")){ //marketplace buy

                transactionDTOS.add(newTransactionDTO(primOrderDTO.getReference(), primOrderDTO.getPriceRequestID(), "INVESTOR_CASH","BURSA_CASH",netCash,"F05","F06",primOrderDTO.getPrice()));
                transactionDTOS.add(newTransactionDTO(primOrderDTO.getReference(), primOrderDTO.getPriceRequestID(), "BURSA_ASSET","INVESTOR_ASSET",quantity,"F06","F05",primOrderDTO.getPrice()));

            }
            else if(primOrderDTO.getType().equals("S06")){

                transactionDTOS.add(newTransactionDTO(primOrderDTO.getReference(), primOrderDTO.getPriceRequestID(), "BURSA_CASH","INVESTOR_CASH",netCash,"F05","F06",primOrderDTO.getPrice()));
                transactionDTOS.add(newTransactionDTO(primOrderDTO.getReference(), primOrderDTO.getPriceRequestID(), "INVESTOR_ASSET","BURSA_ASSET",quantity,"F06","F05",primOrderDTO.getPrice()));

            }

            for(TradingFeeDTO tradingFee : primOrderDTO.getFeesForms()){

                if(tradingFee.getChargeUomCode().equals("J01")){
                    transactionDTOS.add(newTransactionDTO(primOrderDTO.getReference(), primOrderDTO.getPriceRequestID(), "INVESTOR_CASH","BURSA_CASH",tradingFee.getChargeFees(),tradingFee.getFeeTypeCode(),tradingFee.getFeeTypeCode(),primOrderDTO.getPrice()));
                }
                else if(tradingFee.getChargeUomCode().equals("J02")){
                    transactionDTOS.add(newTransactionDTO(primOrderDTO.getReference(), primOrderDTO.getPriceRequestID(), "INVESTOR_ASSET","BURSA_ASSET",tradingFee.getChargeFees(),tradingFee.getFeeTypeCode(),tradingFee.getFeeTypeCode(),primOrderDTO.getPrice()));
                }
            }
//        BigDecimal finalQuantity = quantity;
//
//        excBuyback.execute(new Runnable() {
//            @Override
//            public void run() {
//                systemAuthenticator.runWithSystem(() -> {
//                    reconcilitiation(finalQuantity, primOrderDTO.getType());
//                });
//            }
//        });

            return ResponseEntity.status(HttpStatus.OK).body(transactionDTOS);
        }
        catch (Exception e){

            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setReference(primOrderDTO.getReference());
            responseDTO.setDate(timeSource.currentTimestamp());
            responseDTO.setStatus("G02");
            responseDTO.setDescription(e.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }

    }

    public TransactionDTO newTransactionDTO(String reference, String priceRequestID, String creditWallet, String debitWallet, BigDecimal amount, String creditType , String debitType, BigDecimal bursaPrice){
        TransactionDTO newT = dataManager.create(TransactionDTO.class);
        newT.setReference(reference);
        newT.setPriceRequestID(priceRequestID);
        newT.setCreditWallet(creditWallet);
        newT.setDebitWallet(debitWallet);
        newT.setAmount(amount);
        newT.setCreditType(creditType);
        newT.setDebitType(debitType);
        newT.setBursaPrice(bursaPrice);

        return newT;
    }


}
