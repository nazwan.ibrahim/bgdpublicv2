package com.company.trade_engine.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@JmixEntity
public class PrimTxHistoryDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String transType;

    @Temporal(TemporalType.TIMESTAMP)
    private Date transDateTime;

    private BigDecimal goldTransValue;

    private BigDecimal cashTransValue;

    public BigDecimal getCashTransValue() {
        return cashTransValue;
    }

    public void setCashTransValue(BigDecimal cashTransValue) {
        this.cashTransValue = cashTransValue;
    }

    public BigDecimal getGoldTransValue() {
        return goldTransValue;
    }

    public void setGoldTransValue(BigDecimal goldTransValue) {
        this.goldTransValue = goldTransValue;
    }

    public Date getTransDateTime() {
        return transDateTime;
    }

    public void setTransDateTime(Date transDateTime) {
        this.transDateTime = transDateTime;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}