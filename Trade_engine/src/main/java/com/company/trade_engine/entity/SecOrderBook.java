package com.company.trade_engine.entity;


import liquibase.pro.packaged.A;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

public class SecOrderBook {

    private BigDecimal price;

    private BigDecimal size;

    private BigDecimal filled;

    private Date orderTime;

    private BigDecimal locked;

    private String Status;

    private UUID storeID;

    private String userName;


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

    public BigDecimal getLocked() {
        return locked;
    }

    public void setLocked(BigDecimal locked) {
        this.locked = locked;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public BigDecimal getFilled() {
        return filled;
    }

    public void setFilled(BigDecimal filled) {
        this.filled = filled;
    }

    public UUID getStoreID() {
        return storeID;
    }

    public void setStoreID(UUID storeID) {
        this.storeID = storeID;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
