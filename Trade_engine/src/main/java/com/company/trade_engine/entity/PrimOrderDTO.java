package com.company.trade_engine.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@JmixEntity
public class PrimOrderDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String reference;

    private String priceRequestID;

    private String inputType;

    private String type;

    private BigDecimal totalCash;

    private BigDecimal quantity;

    private BigDecimal price;

    private BigDecimal fee;

    private BigDecimal netPrice;

    private String status;

    private BigDecimal walletBalance;

    private List<TradingFeeDTO> feesForms;

    @InstanceName
    private String description;

    public String getPriceRequestID() {
        return priceRequestID;
    }

    public void setPriceRequestID(String priceRequestID) {
        this.priceRequestID = priceRequestID;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public BigDecimal getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(BigDecimal netPrice) {
        this.netPrice = netPrice;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public BigDecimal getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(BigDecimal totalCash) {
        this.totalCash = totalCash;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWalletBalance(BigDecimal walletBalance) {
        this.walletBalance = walletBalance;
    }

    public BigDecimal getWalletBalance() {
        return walletBalance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<TradingFeeDTO> getFeesForms() {
        return feesForms;
    }

    public void setFeesForms(List<TradingFeeDTO> tradingFeeList) {
        this.feesForms = tradingFeeList;
    }

}