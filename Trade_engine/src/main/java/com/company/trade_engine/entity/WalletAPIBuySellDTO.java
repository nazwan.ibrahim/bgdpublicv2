package com.company.trade_engine.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.util.UUID;

@JmixEntity
public class WalletAPIBuySellDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String prev_cash;

    private String current_cash;

    private String prev_asset;

    private String current_asset;

    public String getCurrent_asset() {
        return current_asset;
    }

    public void setCurrent_asset(String current_asset) {
        this.current_asset = current_asset;
    }

    public String getPrev_asset() {
        return prev_asset;
    }

    public void setPrev_asset(String prev_asset) {
        this.prev_asset = prev_asset;
    }

    public String getCurrent_cash() {
        return current_cash;
    }

    public void setCurrent_cash(String current_cash) {
        this.current_cash = current_cash;
    }

    public String getPrev_cash() {
        return prev_cash;
    }

    public void setPrev_cash(String prev_cash) {
        this.prev_cash = prev_cash;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}