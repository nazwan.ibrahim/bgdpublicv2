package com.company.trade_engine.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum PurchaseRequestType implements EnumClass<String> {

    BUY("BUY"),
    SELL("SELL");

    private String id;

    PurchaseRequestType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static PurchaseRequestType fromId(String id) {
        for (PurchaseRequestType at : PurchaseRequestType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}