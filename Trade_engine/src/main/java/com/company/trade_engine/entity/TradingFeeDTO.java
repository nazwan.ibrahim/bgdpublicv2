package com.company.trade_engine.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.math.BigDecimal;
import java.util.UUID;

@JmixEntity
public class TradingFeeDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String feeTypeCode;

    private BigDecimal chargeFees;

    private String chargeUomCode;

    public String getChargeUomCode() {
        return chargeUomCode;
    }

    public void setChargeUomCode(String chargeUomCode) {
        this.chargeUomCode = chargeUomCode;
    }

    public BigDecimal getChargeFees() {
        return chargeFees;
    }

    public void setChargeFees(BigDecimal chargeFees) {
        this.chargeFees = chargeFees;
    }

    public String getFeeTypeCode() {
        return feeTypeCode;
    }

    public void setFeeTypeCode(String feeTypeCode) {
        this.feeTypeCode = feeTypeCode;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}