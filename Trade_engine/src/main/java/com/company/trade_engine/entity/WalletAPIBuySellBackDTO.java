package com.company.trade_engine.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.util.UUID;

@JmixEntity
public class WalletAPIBuySellBackDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String prev_bursa_cash;

    private String current_bursa_cash;

    private String prev_bursa_asset;

    private String current_bursa_asset;

    private String prev_sup_cash;

    private String current_sup_cash;

    private String prev_sup_asset;

    private String current_sup_asset;

    public String getCurrent_sup_asset() {
        return current_sup_asset;
    }

    public void setCurrent_sup_asset(String current_sup_asset) {
        this.current_sup_asset = current_sup_asset;
    }

    public String getPrev_sup_asset() {
        return prev_sup_asset;
    }

    public void setPrev_sup_asset(String prev_sup_asset) {
        this.prev_sup_asset = prev_sup_asset;
    }

    public String getCurrent_sup_cash() {
        return current_sup_cash;
    }

    public void setCurrent_sup_cash(String current_sup_cash) {
        this.current_sup_cash = current_sup_cash;
    }

    public String getPrev_sup_cash() {
        return prev_sup_cash;
    }

    public void setPrev_sup_cash(String prev_sup_cash) {
        this.prev_sup_cash = prev_sup_cash;
    }

    public String getCurrent_bursa_asset() {
        return current_bursa_asset;
    }

    public void setCurrent_bursa_asset(String current_bursa_asset) {
        this.current_bursa_asset = current_bursa_asset;
    }

    public String getPrev_bursa_asset() {
        return prev_bursa_asset;
    }

    public void setPrev_bursa_asset(String prev_bursa_asset) {
        this.prev_bursa_asset = prev_bursa_asset;
    }

    public String getCurrent_bursa_cash() {
        return current_bursa_cash;
    }

    public void setCurrent_bursa_cash(String current_bursa_cash) {
        this.current_bursa_cash = current_bursa_cash;
    }

    public String getPrev_bursa_cash() {
        return prev_bursa_cash;
    }

    public void setPrev_bursa_cash(String prev_bursa_cash) {
        this.prev_bursa_cash = prev_bursa_cash;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}