package com.company.trade_engine.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.math.BigDecimal;
import java.util.UUID;

@JmixEntity
public class TransactionDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String reference;

    private String priceRequestID;

    private String creditWallet;

    private String debitWallet;

    private BigDecimal amount;

    private String creditType;

    private String debitType;


    private BigDecimal bursaPrice;

    public String getPriceRequestID() {
        return priceRequestID;
    }

    public void setPriceRequestID(String priceRequestID) {
        this.priceRequestID = priceRequestID;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setDebitType(String debitType) {
        this.debitType = debitType;
    }

    public String getDebitType() {
        return debitType;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDebitWallet() {
        return debitWallet;
    }

    public void setDebitWallet(String debitWallet) {
        this.debitWallet = debitWallet;
    }

    public String getCreditWallet() {
        return creditWallet;
    }

    public void setCreditWallet(String creditWallet) {
        this.creditWallet = creditWallet;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public BigDecimal getBursaPrice() {
        return bursaPrice;
    }

    public void setBursaPrice(BigDecimal bursaPrice) {
        this.bursaPrice = bursaPrice;
    }
}