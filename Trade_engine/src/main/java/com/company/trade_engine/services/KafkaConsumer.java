package com.company.trade_engine.services;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;

@Service
public class KafkaConsumer {

    private final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    private ConsumerFactory<String, Object> consumerFactory;

//    public void getLastMessage() {
//        logger.info("CONCUMER TEST: ");
//        Consumer<String, Object> consumer = consumerFactory.createConsumer();
//        consumer.subscribe(Collections.singletonList("jmix-kafka-json"));
//        consumer.seekToBeginning(consumer.assignment()); // set the consumer's offset to the latest available offset
//
//        ConsumerRecords<String, Object> records = consumer.poll(Duration.ofMillis(100));
//        for (ConsumerRecord<String, Object> record : records) {
//            // process the last message produced by the Kafka produce
//            logger.info("CONCUMER TEST: "+ record);
//
//        }
//    }

//    @KafkaListener(topics = "jmix-kafka-json", groupId = "tradeEngineService")
//    public void consumeJson(ConsumerRecord<String, Object> record) throws IOException {
//        try {
//            logger.info(String.format("#### -> Consumed json -> %s", record.value()));
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//        }
//    }
}
