package com.company.trade_engine.security;


import io.jmix.security.model.EntityAttributePolicyAction;
        import io.jmix.security.model.EntityPolicyAction;
        import io.jmix.security.role.annotation.EntityAttributePolicy;
        import io.jmix.security.role.annotation.EntityPolicy;
        import io.jmix.security.role.annotation.ResourceRole;

@ResourceRole(name = "RestApi", code = "rest-api", scope = "API")
public interface RestApiRole {

    @EntityPolicy(entityName = "*", actions = {EntityPolicyAction.ALL})
    @EntityAttributePolicy(entityName = "*", attributes = "*", action = EntityAttributePolicyAction.MODIFY)
//    @ScreenPolicy(screenIds = "*")
//    @MenuPolicy(menuIds = "*")
//    @SpecificPolicy(resources = "*")
    void entity();
}