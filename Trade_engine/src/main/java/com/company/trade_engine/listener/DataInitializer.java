package com.company.trade_engine.listener;

import com.company.trade_engine.app.PrimaryTradingApiController;
import com.company.trade_engine.entity.InputType;
import com.company.trade_engine.entity.TradeStatus;
import com.company.trade_engine.entity.TradeType;
import com.company.trade_engine.entity.TransStatus;
import io.jmix.core.DataManager;
import io.jmix.core.security.Authenticated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.stereotype.Component;
import org.springframework.context.event.EventListener;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataInitializer {

    @Autowired
    private DataManager dataManager;

    Logger logger = LoggerFactory.getLogger(DataInitializer.class);

    @EventListener
    @Authenticated
    public void onApplicationStarted(ApplicationStartedEvent event) {

        logger.info("CHECKING METADATA ...");
        inputTypeInit();
        tradeTypeInit();
        tradeStatusInit();
        initTransactionStatus();
        logger.info("METADATA INITIALIZED");
    }

    public void inputTypeInit(){

        String[] codes = {"S21", "S22"};
        List<InputType> inputTypeList = this.dataManager.load(InputType.class).all().list();

        if(inputTypeList.size()!=codes.length) {
            logger.info("ADDING MISSING InputType METADATA .. ");
            List<InputType> newInputTypeList = Arrays.stream(codes)
                    .filter(code -> inputTypeList.stream().noneMatch(a -> a.getCode().equals(code)))
                    .map(code -> {
                        InputType newType = this.dataManager.create(InputType.class);
                        newType.setCode(code);
                        if (code.equals("S21")) {
                            newType.setName("CASH");
                            newType.setDescription("User input amount to trade in cash");
                        } else if (code.equals("S22")) {
                            newType.setName("QUANTITY");
                            newType.setDescription("user input amount to trade in quantity gram");
                        }
                        return newType;
                    })
                    .collect(Collectors.toList());

            newInputTypeList.forEach(this.dataManager::save);

        }else{
            logger.info("NO MISSING InputType METADATA");
        }


    }

    public void tradeTypeInit(){

        String[] codes = {"S01", "S02","S03","S04","S05","S06"};
        List<TradeType> tradeTypeList = this.dataManager.load(TradeType.class).all().list();

        if(tradeTypeList.size()!=codes.length) {

            logger.info("ADDING MISSING TradeType METADATA .. ");
            List<TradeType> newTradeTypeList = Arrays.stream(codes)
                    .filter(code -> tradeTypeList.stream().noneMatch(a -> a.getCode().equals(code)))
                    .map(code -> {
                        TradeType newType = this.dataManager.create(TradeType.class);
                        newType.setCode(code);
                        if (code.equals("S01")) {
                            newType.setName("Limit Buy");
                            newType.setDescription("Exchange buy limit order");
                        } else if (code.equals("S02")) {
                            newType.setName("Limit Sell");
                            newType.setDescription("Exchange sell limit order");
                        } else if (code.equals("S03")) {
                            newType.setName("Market Buy");
                            newType.setDescription("Exchange buy market order");
                        } else if (code.equals("S04")) {
                            newType.setName("Market Sell");
                            newType.setDescription("exchange sell market order");
                        } else if (code.equals("S05")) {
                            newType.setName("Marketplace Buy");
                            newType.setDescription("Marketplace buy ");
                        } else if (code.equals("S06")) {
                            newType.setName("Marketplace Sell");
                            newType.setDescription("Marketplace sell");
                        }
                        return newType;
                    })
                    .collect(Collectors.toList());

            newTradeTypeList.forEach(this.dataManager::save);

        } else{
            logger.info("NO MISSING TradeType METADATA");
        }

    }

    public void tradeStatusInit(){

        String[] codes = {"S11", "S12","S13","S14","S15"};
        List<TradeStatus> tradeStatusList = this.dataManager.load(TradeStatus.class).all().list();

        if(tradeStatusList.size()!=codes.length) {

            logger.info("ADDING MISSING TradeStatus METADATA .. ");
            List<TradeStatus> newTradeStatusList = Arrays.stream(codes)
                    .filter(code -> tradeStatusList.stream().noneMatch(a -> a.getCode().equals(code)))
                    .map(code -> {
                        TradeStatus newType = this.dataManager.create(TradeStatus.class);
                        newType.setCode(code);
                        if (code.equals("S11")) {
                            newType.setName("Zero Filled");
                            newType.setDescription("Order not yet filled / matched");
                        } else if (code.equals("S12")) {
                            newType.setName("Partial Filled");
                            newType.setDescription("Order has partially matched");
                        } else if (code.equals("S13")) {
                            newType.setName("All Filled");
                            newType.setDescription("Order has fully matched");
                        } else if (code.equals("S14")) {
                            newType.setName("Cancelled");
                            newType.setDescription("Order has been cancelled");
                        } else if (code.equals("S15")) {
                            newType.setName("No Volume");
                            newType.setDescription("Order has been cancelled due to no order to be matched (market order) ");
                        }
                        return newType;
                    })
                    .collect(Collectors.toList());

            newTradeStatusList.forEach(this.dataManager::save);

        }else{
            logger.info("NO MISSING TradeStatus METADATA");
        }

    }

    private void initTransactionStatus() {

        String[] codes = {"G01", "G02","G05","G06","G07","G08"};
        List<TransStatus> transStatusList = this.dataManager.load(TransStatus.class).all().list();

        if(transStatusList.size()!=codes.length) {

            logger.info("ADDING MISSING TransStatus METADATA .. ");
            List<TransStatus> newTransStatusList = Arrays.stream(codes)
                    .filter(code -> transStatusList.stream().noneMatch(a -> a.getCode().equals(code)))
                    .map(code -> {
                        TransStatus newType = this.dataManager.create(TransStatus.class);
                        newType.setCode(code);
                        if (code.equals("G01")) {
                            newType.setName("Completed");
                            newType.setDescription("");
                        } else if (code.equals("G02")) {
                            newType.setName("Failed");
                            newType.setDescription("");
                        } else if (code.equals("G05")) {
                            newType.setName("Pending");
                            newType.setDescription("");
                        } else if (code.equals("G06")) {
                            newType.setName("Approved");
                            newType.setDescription("");
                        } else if (code.equals("G07")) {
                            newType.setName("Reject");
                            newType.setDescription("");
                        } else if (code.equals("G08")) {
                            newType.setName("Processing");
                            newType.setDescription("");
                        }
                        return newType;
                    })
                    .toList();

            newTransStatusList.forEach(this.dataManager::save);

        }else{
            logger.info("NO MISSING TrnasStatus METADATA");
        }
    }


}