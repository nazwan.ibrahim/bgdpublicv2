package com.company.trade_engine.listener;

import com.company.trade_engine.entity.AccountBgd;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class AccountBGDEventListener {

    @EventListener
    public void onAccountBGDLoading(EntityLoadingEvent<AccountBgd> event) {

    }

    @EventListener
    public void onAccountBGDSaving(EntitySavingEvent<AccountBgd> event) {

    }

    @EventListener
    public void onAccountBGDChangedBeforeCommit(EntityChangedEvent<AccountBgd> event) {

    }

    @TransactionalEventListener
    public void onAccountBGDChangedAfterCommit(EntityChangedEvent<AccountBgd> event) {

    }
}