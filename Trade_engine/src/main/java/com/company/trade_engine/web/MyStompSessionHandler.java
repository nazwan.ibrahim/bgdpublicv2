package com.company.trade_engine.web;

import com.company.trade_engine.entity.SecOrderBook;
import com.company.trade_engine.screen.test.TestScreen;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import elemental.json.Json;
import elemental.json.JsonArray;
import liquibase.pro.packaged.O;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is an implementation for <code>StompSessionHandlerAdapter</code>.
 * Once a connection is established, We subscribe to /topic/messages and
 * send a sample message to server.
 *
 * @author Kalyan
 *
 */
public class MyStompSessionHandler extends StompSessionHandlerAdapter {

    private TestScreen testScreen;
    private Logger logger = LogManager.getLogger(MyStompSessionHandler.class);

    public MyStompSessionHandler(TestScreen testScreen){
        this.testScreen= testScreen;
    }


    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        logger.info("New session established : " + session.getSessionId());
        session.subscribe("/topic/buyOrder", this);
        logger.info("Subscribed to /topic/message");
        session.send("/app/orderBook","client call");

        session.subscribe("/topic/buyGoldPrice",this);
        session.send("/app/marketplace","client call 2");
        logger.info("FINISHED SUBSCRIBING WEB SOCKET");

    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        logger.error("Got an exception", exception);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
            System.out.println("HA");
            return List.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {


        System.out.println("Received payload: " +  payload);
        System.out.println("HEADER"+headers);

        if(headers.getDestination().equals("/topic/buyOrder")) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<SecOrderBook> pojos = objectMapper.convertValue(
                    payload,
                    new TypeReference<List<SecOrderBook>>() {
                    });
            System.out.println(pojos.get(0).getPrice());
        }
//
//
//



    }

    /**
     * A sample message instance.
     * @return instance of <code>Message</code>
     */
    private Message getSampleMessage() {
        Message msg = new Message();
        msg.setFrom("Nicky");
        msg.setText("Howdy!!");
        return msg;
    }
}