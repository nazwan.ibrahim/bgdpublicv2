package com.company.trade_engine.screen.transferinfo;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransferInfo;

@UiController("TransferInfo.browse")
@UiDescriptor("transfer-info-browse.xml")
@LookupComponent("transferInfoesTable")
public class TransferInfoBrowse extends StandardLookup<TransferInfo> {
}