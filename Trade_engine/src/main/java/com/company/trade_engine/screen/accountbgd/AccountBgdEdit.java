package com.company.trade_engine.screen.accountbgd;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.AccountBgd;

@UiController("AccountBgd.edit")
@UiDescriptor("account-bgd-edit.xml")
@EditedEntityContainer("accountBgdDc")
public class AccountBgdEdit extends StandardEditor<AccountBgd> {
}