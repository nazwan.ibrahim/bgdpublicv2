package com.company.trade_engine.screen.transstatus;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransStatus;

@UiController("TransStatus.browse")
@UiDescriptor("trans-status-browse.xml")
@LookupComponent("transStatusesTable")
public class TransStatusBrowse extends StandardLookup<TransStatus> {
}