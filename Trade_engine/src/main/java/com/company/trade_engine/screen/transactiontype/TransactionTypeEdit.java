package com.company.trade_engine.screen.transactiontype;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransactionType;

@UiController("TransactionType.edit")
@UiDescriptor("transaction-type-edit.xml")
@EditedEntityContainer("transactionTypeDc")
public class TransactionTypeEdit extends StandardEditor<TransactionType> {
}