package com.company.trade_engine.screen.wallettype;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.WalletType;

@UiController("WalletType.edit")
@UiDescriptor("wallet-type-edit.xml")
@EditedEntityContainer("walletTypeDc")
public class WalletTypeEdit extends StandardEditor<WalletType> {
}