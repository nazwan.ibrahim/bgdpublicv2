package com.company.trade_engine.screen.transactionacct;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransactionAcct;

@UiController("TransactionAcct.edit")
@UiDescriptor("transaction-acct-edit.xml")
@EditedEntityContainer("transactionAcctDc")
public class TransactionAcctEdit extends StandardEditor<TransactionAcct> {
}