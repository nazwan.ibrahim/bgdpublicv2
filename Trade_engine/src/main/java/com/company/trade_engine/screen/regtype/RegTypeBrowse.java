package com.company.trade_engine.screen.regtype;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.RegType;

@UiController("RegType.browse")
@UiDescriptor("reg-type-browse.xml")
@LookupComponent("regTypesTable")
public class RegTypeBrowse extends StandardLookup<RegType> {
}