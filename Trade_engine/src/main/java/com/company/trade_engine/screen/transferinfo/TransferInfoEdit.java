package com.company.trade_engine.screen.transferinfo;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransferInfo;

@UiController("TransferInfo.edit")
@UiDescriptor("transfer-info-edit.xml")
@EditedEntityContainer("transferInfoDc")
public class TransferInfoEdit extends StandardEditor<TransferInfo> {
}