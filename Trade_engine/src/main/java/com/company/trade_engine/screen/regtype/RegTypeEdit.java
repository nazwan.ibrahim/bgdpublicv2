package com.company.trade_engine.screen.regtype;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.RegType;

@UiController("RegType.edit")
@UiDescriptor("reg-type-edit.xml")
@EditedEntityContainer("regTypeDc")
public class RegTypeEdit extends StandardEditor<RegType> {
}