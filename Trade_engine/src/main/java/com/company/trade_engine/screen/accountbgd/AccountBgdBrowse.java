package com.company.trade_engine.screen.accountbgd;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.AccountBgd;

@UiController("AccountBgd.browse")
@UiDescriptor("account-bgd-browse.xml")
@LookupComponent("accountBgdsTable")
public class AccountBgdBrowse extends StandardLookup<AccountBgd> {
}