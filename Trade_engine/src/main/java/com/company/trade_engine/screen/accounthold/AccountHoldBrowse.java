package com.company.trade_engine.screen.accounthold;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.AccountHold;

@UiController("AccountHold.browse")
@UiDescriptor("account-hold-browse.xml")
@LookupComponent("accountHoldsTable")
public class AccountHoldBrowse extends StandardLookup<AccountHold> {
}