package com.company.trade_engine.screen.transstatus;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransStatus;

@UiController("TransStatus.edit")
@UiDescriptor("trans-status-edit.xml")
@EditedEntityContainer("transStatusDc")
public class TransStatusEdit extends StandardEditor<TransStatus> {
}