package com.company.trade_engine.screen.test;

import com.company.trade_engine.app.PrimaryTradingApiController;
import com.company.trade_engine.entity.PurchaseRequestType;
import com.company.trade_engine.entity.SecOrderDTO;
import com.company.trade_engine.entity.SecOrderStore;
import com.company.trade_engine.web.MyStompSessionHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jmix.core.DataManager;
import io.jmix.ui.UiComponents;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@UiController("TestScreen")
@UiDescriptor("test-screen.xml")
public class TestScreen extends Screen {

    @Autowired
    private DataManager dataManager;
    @Autowired
    private Label label2;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private VBoxLayout vbox1;
    @Autowired
    private TextField priceField;
    @Autowired
    private TextField userField;
    @Autowired
    private TextField quantityField;
    @Autowired
    private Button refreshButton;
    @Autowired
    private HBoxLayout header1;
    @Autowired
    private HBoxLayout header2;
    @Autowired
    private Button sellButton;
    @Autowired
    private VBoxLayout vbox2;
    @Autowired
    private CollectionLoader<SecOrderStore> secOrderStoresDl;
    @Autowired
    private CollectionLoader<SecOrderStore> secOrderStoresDl2;

    Logger logger = LoggerFactory.getLogger(PrimaryTradingApiController.class);
    @Autowired
    private Button buyButton;
    @Autowired
    private RadioButtonGroup methodRadioButton;


    @Subscribe(id = "secOrderStoresDc", target = Target.DATA_CONTAINER)
    public void onSecOrderStoresDcItemPropertyChange(InstanceContainer.ItemPropertyChangeEvent<SecOrderStore> event) {
        System.out.println("HSHKKK");
    }



//    @Subscribe(id = "secOrderStoresDc2", target = Target.DATA_CONTAINER)
//    public void onSecOrderStoresDc2ItemPropertyChange(InstanceContainer.ItemPropertyChangeEvent<SecOrderStore> event) throws JsonProcessingException {
//        logger.info("Refresh Screen");
//        onRefreshButtonClick(new Button.ClickEvent(refreshButton));
//    }

//    @Subscribe(id = "secOrderStoresDc2", target = Target.DATA_CONTAINER)
//    public void onSecOrderStoresDc2CollectionChange(CollectionContainer.CollectionChangeEvent<SecOrderStore> event) throws JsonProcessingException {
//        logger.info("REFRESH Screen");
//        onRefreshButtonClick(new Button.ClickEvent(refreshButton));
//    }




    @Subscribe
    public void onInit(InitEvent event) {
        secOrderStoresDl.setParameter("type", PurchaseRequestType.BUY);
        secOrderStoresDl.load();

        secOrderStoresDl2.setParameter("type",PurchaseRequestType.SELL);
        secOrderStoresDl2.load();

        List<String> method= new ArrayList<>();
        method.add("LIMIT");
        method.add("MARKET");

        methodRadioButton.setOptionsList(method);
        methodRadioButton.setValue("LIMIT");

    }

    @Subscribe("methodRadioButton")
    public void onMethodRadioButtonValueChange(HasValue.ValueChangeEvent event) {
        if(event.getValue().equals("MARKET")){
            priceField.clear();
            priceField.setVisible(false);
        }
        else{
            priceField.setVisible(true);
        }
    }


    private static String URL = "ws://localhost:8080/trading-endpoint";

    @Subscribe
    public void onAfterShow(AfterShowEvent event) throws JsonProcessingException {

        onRefreshButtonClick(new Button.ClickEvent(refreshButton));

        WebSocketClient client = new StandardWebSocketClient();

        WebSocketStompClient stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        StompSessionHandler sessionHandler = new MyStompSessionHandler(this);
        stompClient.connect(URL, sessionHandler);




//        new Scanner(System.in).nextLine(); // Don't close immediately

    }

//    public void websocketRefresh() throws JsonProcessingException {
//        System.out.println("test print something");
//        onRefreshButtonClick(new Button.ClickEvent(refreshButton));
//    }


    @Subscribe("buyButton")
    public void onBuyButtonClick(Button.ClickEvent event) throws JsonProcessingException {
        String uri= environment.getProperty("secondaryTrading/buy");
        RestTemplate restTemplate = new RestTemplate();

        BigDecimal price= new BigDecimal(!priceField.getRawValue().equals("")? priceField.getRawValue():"0");
        BigDecimal quantity = new BigDecimal(quantityField.getRawValue());
        SecOrderDTO secOrderDTO =  dataManager.create(SecOrderDTO.class);
        secOrderDTO.setUserID(123);
        if (price.compareTo(BigDecimal.valueOf(0))!=0) {
            secOrderDTO.setPrice(price);
        }
        secOrderDTO.setSize(quantity);
        secOrderDTO.setMethod(methodRadioButton.getValue().toString());


        //SecOrderDTO response = restTemplate.postForObject(uri, secOrderDTO , SecOrderDTO.class);
        String response = restTemplate.postForObject(uri, secOrderDTO , String.class);
//        Assertions.assertEquals(response.getStatusCode(), HttpStatus.CREATED);

//        label1.setValue(response.getStatus());
        onRefreshButtonClick(new Button.ClickEvent(refreshButton));

    }

    @Subscribe("sellButton")
    public void onSellButtonClick(Button.ClickEvent event) throws JsonProcessingException {
        String uri= environment.getProperty("secondaryTrading/sell");
        RestTemplate restTemplate = new RestTemplate();

        BigDecimal price= new BigDecimal(!priceField.getRawValue().equals("")? priceField.getRawValue():"0");
        BigDecimal quantity = new BigDecimal(quantityField.getRawValue());
        SecOrderDTO secOrderDTO =  dataManager.create(SecOrderDTO.class);
        secOrderDTO.setUserID(123);
        if (price.compareTo(BigDecimal.valueOf(0))!=0) {
            secOrderDTO.setPrice(price);
        }
        secOrderDTO.setSize(quantity);
        secOrderDTO.setMethod(methodRadioButton.getValue().toString());

        //SecOrderDTO response = restTemplate.postForObject(uri, secOrderDTO , SecOrderDTO.class);
        String response = restTemplate.postForObject(uri, secOrderDTO , String.class);
//        Assertions.assertEquals(response.getStatusCode(), HttpStatus.CREATED);

//        label1.setValue(response.getStatus());
        onRefreshButtonClick(new Button.ClickEvent(refreshButton));
    }

    @Subscribe("refreshButton")
    public void onRefreshButtonClick(Button.ClickEvent event) throws JsonProcessingException {

        String uriBuy= environment.getProperty("secondaryTrading/orderBookBuy");
        String uriSell= environment.getProperty("secondaryTrading/orderBookSell");
        reloadOrderBook(uriBuy,vbox1,header1);
        reloadOrderBook(uriSell,vbox2,header2);


        secOrderStoresDl.setParameter("type", PurchaseRequestType.BUY);
        secOrderStoresDl.load();
        secOrderStoresDl2.setParameter("type",PurchaseRequestType.SELL);
        secOrderStoresDl2.load();

    }


    public void reloadOrderBook(String uri , VBoxLayout vbox, HBoxLayout header) throws JsonProcessingException {

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<String> response = restTemplate.getForEntity(uri,String.class);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());

        vbox.removeAll();
        vbox.add(header);


        for (final JsonNode objNode : root) {

            Label<String> priceLabel = uiComponents.create(Label.class);
            priceLabel.setValue(objNode.get("price").toString());
            priceLabel.setWidth("80px");
            priceLabel.setAlignment(Component.Alignment.MIDDLE_CENTER);

            Label<String> quantityLabel = uiComponents.create(Label.class);
            quantityLabel.setValue(objNode.get("size").toString());
            quantityLabel.setWidth("80px");
            quantityLabel.setAlignment(Component.Alignment.MIDDLE_CENTER);

            Label<String > filledLabel = uiComponents.create(Label.class);
            filledLabel.setValue(objNode.get("filled").toString());
            filledLabel.setWidth("80px");
            filledLabel.setAlignment(Component.Alignment.MIDDLE_CENTER);

            HBoxLayout hBoxLayout = uiComponents.create(HBoxLayout.class);
            hBoxLayout.setStyleName("{background:blue;}");
            hBoxLayout.add(priceLabel);
            hBoxLayout.add(quantityLabel);
            hBoxLayout.add(filledLabel);
            hBoxLayout.setAlignment(Component.Alignment.MIDDLE_CENTER);
            hBoxLayout.setWidth("100%");

            vbox.add(hBoxLayout);

        }
    }

    @Scheduled(fixedDelay = 5000)
    public void showOrderBookBuy() throws JsonProcessingException, InterruptedException {


        Random type= new Random();
//        DecimalFormat formatter = new DecimalFormat("#0.00");  // edited here.
        double randomValue = 0 + Math.random( ) * 1000;
        BigDecimal randPrice = new BigDecimal(randomValue).setScale(2, RoundingMode.HALF_UP);

        int randomQuantity = type.nextInt();
//        BigDecimal randQuantity = new BigDecimal(randomQuantity);

        logger.info("UPDATE PRICE AND QUANTITY");
        priceField.setValue(randPrice);
        quantityField.setValue(randomQuantity);
        Thread.sleep(2000);

        if(type.nextBoolean()){
            onBuyButtonClick(new Button.ClickEvent(buyButton));
            logger.info("AUTO BUY-> Price: "+randPrice+" | Quantity: "+randomQuantity);
        }
        else {
            onSellButtonClick(new Button.ClickEvent(sellButton));
            logger.info("AUTO Sell-> Price: "+randPrice+" | Quantity: "+randomQuantity);
        }
//        Thread.sleep(4000);
    }






}