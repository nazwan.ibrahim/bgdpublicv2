package com.company.trade_engine.screen.transmethod;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransMethod;

@UiController("TransMethod.edit")
@UiDescriptor("trans-method-edit.xml")
@EditedEntityContainer("transMethodDc")
public class TransMethodEdit extends StandardEditor<TransMethod> {
}