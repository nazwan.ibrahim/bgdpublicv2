package com.company.trade_engine.screen.accounthold;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.AccountHold;

@UiController("AccountHold.edit")
@UiDescriptor("account-hold-edit.xml")
@EditedEntityContainer("accountHoldDc")
public class AccountHoldEdit extends StandardEditor<AccountHold> {
}