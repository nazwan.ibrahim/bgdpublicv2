package com.company.trade_engine.screen.transmethod;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransMethod;

@UiController("TransMethod.browse")
@UiDescriptor("trans-method-browse.xml")
@LookupComponent("transMethodsTable")
public class TransMethodBrowse extends StandardLookup<TransMethod> {
}