package com.company.trade_engine.screen.transactionacct;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransactionAcct;

@UiController("TransactionAcct.browse")
@UiDescriptor("transaction-acct-browse.xml")
@LookupComponent("transactionAcctsTable")
public class TransactionAcctBrowse extends StandardLookup<TransactionAcct> {
}