package com.company.trade_engine.screen.wallettype;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.WalletType;

@UiController("WalletType.browse")
@UiDescriptor("wallet-type-browse.xml")
@LookupComponent("walletTypesTable")
public class WalletTypeBrowse extends StandardLookup<WalletType> {
}