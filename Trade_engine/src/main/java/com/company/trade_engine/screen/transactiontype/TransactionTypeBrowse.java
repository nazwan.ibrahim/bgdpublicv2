package com.company.trade_engine.screen.transactiontype;

import io.jmix.ui.screen.*;
import com.company.trade_engine.entity.TransactionType;

@UiController("TransactionType.browse")
@UiDescriptor("transaction-type-browse.xml")
@LookupComponent("transactionTypesTable")
public class TransactionTypeBrowse extends StandardLookup<TransactionType> {
}