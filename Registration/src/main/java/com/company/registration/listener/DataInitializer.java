package com.company.registration.listener;

import com.company.registration.entity.*;
import groovy.lang.Tuple2;
import groovy.lang.Tuple3;
import io.jmix.core.DataManager;
import io.jmix.core.SaveContext;
import io.jmix.core.security.Authenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DataInitializer {
    @Autowired
    private DataManager dataManager;

    @EventListener
    @Authenticated
    public void onApplicationStarted(ApplicationStartedEvent event){
        initRT_Type();
        initUserType();
        initCompanyType();
        initCompanyCategory();
        initActivationStatus();
        initAddressType();
        initIdentificationType();
        initSecurityQuestion();
        initBankList();
        initKycStatus();
        initKycType();
        initTierType();
        initNotificationCategory();
        initNotificationStatus();
        initNotificationType();
        initAgreementAcknowledgementType();
        initDeclarationType();
        initEmploymentStatus();
        initPositionList();
        initNatureOfBusiness();
        initVerificationType();
        initNsResult();
        initCrpResult();
        initVerificationResult();
        initAssessmentStatus();
        initTransactionPurposeType();
        initTotalMontlyTrans();
        initFreqMontlyTrans();
        initSourceOfFund();
        initSourceOfWealth();
    }

    public void initNotificationCategory(){
        List<NotificationCategory> notificationCategories = this.dataManager.load(NotificationCategory.class).all().list();

        if(notificationCategories.stream().noneMatch(a -> a.getCode().equals("NC01"))){
            NotificationCategory type = this.dataManager.create(NotificationCategory.class);
            type.setCode("NC01");
            type.setName("Activities");
            type.setDescription("User activities");
            this.dataManager.save(type);
        }
        if(notificationCategories.stream().noneMatch(a -> a.getCode().equals("NC02"))){
            NotificationCategory type = this.dataManager.create(NotificationCategory.class);
            type.setCode("NC02");
            type.setName("Announcements");
            type.setDescription("General Announcement");
            this.dataManager.save(type);
        }
    }
    public void initNotificationStatus(){
        List<NotificationStatus> notificationStatuses = this.dataManager.load(NotificationStatus.class).all().list();

        if(notificationStatuses.stream().noneMatch(a -> a.getCode().equals("G16"))){
            NotificationStatus type = this.dataManager.create(NotificationStatus.class);
            type.setCode("G16");
            type.setName("Unread");
            type.setDescription("Unread Notification");
            this.dataManager.save(type);
        }
        if(notificationStatuses.stream().noneMatch(a -> a.getCode().equals("G17"))){
            NotificationStatus type = this.dataManager.create(NotificationStatus.class);
            type.setCode("G17");
            type.setName("Read");
            type.setDescription("Read Notification");
            this.dataManager.save(type);
        }
        if(notificationStatuses.stream().noneMatch(a -> a.getCode().equals("G18"))){
            NotificationStatus type = this.dataManager.create(NotificationStatus.class);
            type.setCode("G18");
            type.setName("Deleted");
            type.setDescription("Deleted Notification");
            this.dataManager.save(type);
        }
    }

    public void initNotificationType(){
        List<NotificationType> notificationTypes = this.dataManager.load(NotificationType.class).all().list();

        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT011"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT011");
            type.setName("Topup Successful Notification");
            type.setDescription("Notification for topup succesful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT012"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT012");
            type.setName("Topup Unsuccessful Notification");
            type.setDescription("Notification for topup unsuccessful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT021"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT021");
            type.setName("Withdraw Successful Notification");
            type.setDescription("Notification for withdraw successful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT022"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT022");
            type.setName("Withdraw Unsuccessful Notification");
            type.setDescription("Notification for withdraw unsuccessful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT031"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT031");
            type.setName("Transfer Successful Notification");
            type.setDescription("Notification for transfer successful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT032"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT032");
            type.setName("Transfer Unsuccessful Notification");
            type.setDescription("Notification for transfer unsuccessful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT033"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT033");
            type.setName("Transfer Received Notification");
            type.setDescription("Notification for transfer received");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT041"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT041");
            type.setName("Redeem Successful Notification");
            type.setDescription("Notification for redeem successful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT042"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT042");
            type.setName("Redeem Unsuccessful Notification");
            type.setDescription("Notification for redeem unsuccessful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT051"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT051");
            type.setName("Buy Successful Notification");
            type.setDescription("Notification for buy successful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT052"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT052");
            type.setName("Buy UnsuccessfulNotification");
            type.setDescription("Notification for buy unsuccessful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT061"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT061");
            type.setName("Sell Successful Notification");
            type.setDescription("Notification for selling successful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT062"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT062");
            type.setName("Sell Unsuccessful Notification");
            type.setDescription("Notification for selling unsuccessful");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT071"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT071");
            type.setName("Parcel Tracking In-Transit Notification");
            type.setDescription("Notification for parcel tracking in-transit");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT072"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT072");
            type.setName("Parcel Tracking Request Placed Notification");
            type.setDescription("Notification for parcel tracking request placed");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT073"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT073");
            type.setName("Parcel Tracking Pickup Notification");
            type.setDescription("Notification for parcel tracking pickup");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT074"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT074");
            type.setName("Parcel Tracking Delivered Notification");
            type.setDescription("Notification for parcel tracking delivered");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT075"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT075");
            type.setName("Parcel Tracking Out For Delivery Notification");
            type.setDescription("Notification for parcel tracking out for delivery");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT076"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT076");
            type.setName("Parcel Tracking Undelivered Notification");
            type.setDescription("Notification for parcel tracking undelivered");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT081"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT081");
            type.setName("KYC Approved Notification");
            type.setDescription("Notification for KYC approval");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT082"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT082");
            type.setName("KYC Rejected Notification");
            type.setDescription("Notification for KYC rejection");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT121"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT121");
            type.setName("SignUp Reward Received Notification");
            type.setDescription("Notification for SignUp Reward Received");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT1221"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT1221");
            type.setName("Referrer Reward Received Notification");
            type.setDescription("Notification for Referrer Reward Received ");
            this.dataManager.save(type);
        }
        if(notificationTypes.stream().noneMatch(a -> a.getCode().equals("NT1222"))){
            NotificationType type = this.dataManager.create(NotificationType.class);
            type.setCode("NT1222");
            type.setName("Referee Reward Received Notification");
            type.setDescription("Notification for Referee Reward Received");
            this.dataManager.save(type);
        }
    }


    public void initRT_Type(){
        List<RtTypeGroup> groupList = this.dataManager.load(RtTypeGroup.class).all().list();
        List<RtType> rtTypeList = this.dataManager.load(RtType.class).all().list();

        //D = Registration Type
        if(groupList.stream().noneMatch(a -> a.getCode().equals("D"))) {
            RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
            group.setCode("D");
            group.setName("Registration Type");
            this.dataManager.save(group);
            groupList.add(group);
        }

        if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D01"))){
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("D01");
            rtType.setName("Platform Owner");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D02"))){
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("D02");
            rtType.setName("Individual Investor");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D03"))){
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("D03");
            rtType.setName("Company Investor");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D04"))){
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("D04");
            rtType.setName("Supplier");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        //L = Address Type
        if(groupList.stream().noneMatch(a -> a.getCode().equals("L"))) {
            RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
            group.setCode("L");
            group.setName("Address Type");
            this.dataManager.save(group);
            groupList.add(group);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("L01"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("L01");
            rtType.setName("Residential");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("L02"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("L02");
            rtType.setName("Company");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("L03"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("L03");
            rtType.setName("Mailing");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("L04"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("L04");
            rtType.setName("Delivery");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("L05"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("L05");
            rtType.setName("Warehouse");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("L06"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("L06");
            rtType.setName("Vault");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        //N = Company Type
        if(groupList.stream().noneMatch(a -> a.getCode().equals("N"))) {
            RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
            group.setCode("N");
            group.setName("Company Type");
            this.dataManager.save(group);
            groupList.add(group);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N01"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N01");
            rtType.setName("Sole Proprietry");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N02"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N02");
            rtType.setName("Partnership");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N03"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N03");
            rtType.setName("Private Limited Company (Sdn Bhd)");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N04"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N04");
            rtType.setName("Company Limited by Guarantee (GLC)");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N05"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N05");
            rtType.setName("Unlimited Company (Sdn)");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N06"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N06");
            rtType.setName("Public Limited Company (Bhd)");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N07"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N07");
            rtType.setName("Foreign Company");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }

        if (groupList.stream().noneMatch(a -> a.getCode().equals("N08"))) {
            RtType rtType = this.dataManager.create(RtType.class);
            rtType.setCode("N08");
            rtType.setName("Limited Liability Partnership (LLP)");
            rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).collect(Collectors.toList()).get(0));
            this.dataManager.save(rtType);
        }
    }

    private void initUserType(){
        List<UserType> userTypes = this.dataManager.load(UserType.class).all().list();

        if(userTypes.stream().noneMatch(a -> a.getCode().equals("K01"))){
            UserType type = this.dataManager.create(UserType.class);
            type.setCode("K01");
            type.setName("Individual");
            type.setDescription("Individual Investor");
            this.dataManager.save(type);
        }
        if(userTypes.stream().noneMatch(a -> a.getCode().equals("K02"))){
            UserType type = this.dataManager.create(UserType.class);
            type.setCode("K02");
            type.setName("Company");
            type.setDescription("Institutions/Corporates Investor");
            this.dataManager.save(type);
        }
    }

    private void initCompanyType(){
        List<CompanyType> companyTypes = this.dataManager.load(CompanyType.class).all().list();

        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N01"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N01");
            type.setName("Sole Proprietry");
            type.setDescription("Sole Proprietry");
            this.dataManager.save(type);
        }
        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N02"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N02");
            type.setName("Partnership");
            type.setDescription("Partnership");
            this.dataManager.save(type);
        }
        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N03"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N03");
            type.setName("Private Limited Company (Sdn Bhd)");
            type.setDescription("Private Limited Company (Sdn Bhd)");
            this.dataManager.save(type);
        }
        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N04"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N04");
            type.setName("Company Limited by Guarantee (GLC)");
            type.setDescription("Company Limited by Guarantee (GLC)");
            this.dataManager.save(type);
        }
        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N05"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N05");
            type.setName("Unlimited Company (Sdn)");
            type.setDescription("Unlimited Company (Sdn)");
            this.dataManager.save(type);
        }
        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N06"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N06");
            type.setName("Public Limited Company (Bhd)");
            type.setDescription("Public Limited Company (Bhd)");
            this.dataManager.save(type);
        }
        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N07"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N07");
            type.setName("Foreign Company");
            type.setDescription("Foreign Company");
            this.dataManager.save(type);
        }
        if(companyTypes.stream().noneMatch(a -> a.getCode().equals("N08"))){
            CompanyType type = this.dataManager.create(CompanyType.class);
            type.setCode("N08");
            type.setName("Limited Liability Partnership (LLP)");
            type.setDescription("Limited Liability Partnership (LLP)");
            this.dataManager.save(type);
        }

    }
    private void initCompanyCategory(){
        List<CompanyCategory> companyCategories = this.dataManager.load(CompanyCategory.class).all().list();

        if(companyCategories.stream().noneMatch(a -> a.getCode().equals("R01"))){
            CompanyCategory type = this.dataManager.create(CompanyCategory.class);
            type.setCode("R01");
            type.setName("SME");
            type.setDescription("Small and medium-sized enterprises (SMEs)");
            this.dataManager.save(type);
        }
        if(companyCategories.stream().noneMatch(a -> a.getCode().equals("R02"))){
            CompanyCategory type = this.dataManager.create(CompanyCategory.class);
            type.setCode("R02");
            type.setName("MNC");
            type.setDescription("Multinational Corporation (MNC)");
            this.dataManager.save(type);
        }

    }
    private void initActivationStatus(){
        List<ActivationStatus> activationStatus = this.dataManager.load(ActivationStatus.class).all().list();

        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G03"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G03");
            type.setName("Not Active");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G04"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G04");
            type.setName("Active");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G05"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G05");
            type.setName("Pending");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G06"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G06");
            type.setName("Approved");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G18"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G18");
            type.setName("Deleted");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G18/02"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G18/02");
            type.setName("Pending - Deleted");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G19"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G19");
            type.setName("Dormant");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G19/02"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G19/02");
            type.setName("Pending - Dormant");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G20"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G20");
            type.setName("Suspend (Disciplinary Action)");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G20/02"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G20/02");
            type.setName("Pending - Suspend (Disciplinary Action)");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G21"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G21");
            type.setName("Suspend (Under Investigation)");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G21/02"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G21/02");
            type.setName("Pending - Suspend (Under Investigation)");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G22"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G22");
            type.setName("Suspend (Redo e-KYC)");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G22/02"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G22/02");
            type.setName("Pending - Suspend (Redo e-KYC)");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G23"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G23");
            type.setName("Close");
            type.setDescription("");
            this.dataManager.save(type);
        }
        if(activationStatus.stream().noneMatch(a -> a.getCode().equals("G23/02"))){
            ActivationStatus type = this.dataManager.create(ActivationStatus.class);
            type.setCode("G23/02");
            type.setName("Pending - Close");
            type.setDescription("");
            this.dataManager.save(type);
        }
    }

    private void initAddressType(){
        List<AddressType> addressTypes = this.dataManager.load(AddressType.class).all().list();

        if(addressTypes.stream().noneMatch(a -> a.getCode().equals("L01"))){
            AddressType type = this.dataManager.create(AddressType.class);
            type.setCode("L01");
            type.setName("Residential");
            type.setDescription("Residential Address");
            this.dataManager.save(type);
        }
        if(addressTypes.stream().noneMatch(a -> a.getCode().equals("L02"))){
            AddressType type = this.dataManager.create(AddressType.class);
            type.setCode("L02");
            type.setName("Company");
            type.setDescription("Company Address");
            this.dataManager.save(type);
        }
        if(addressTypes.stream().noneMatch(a -> a.getCode().equals("L03"))){
            AddressType type = this.dataManager.create(AddressType.class);
            type.setCode("L03");
            type.setName("Mailing");
            type.setDescription("Mailing Address");
            this.dataManager.save(type);
        }
        if(addressTypes.stream().noneMatch(a -> a.getCode().equals("L04"))){
            AddressType type = this.dataManager.create(AddressType.class);
            type.setCode("L04");
            type.setName("Delivery");
            type.setDescription("Delivery Address");
            this.dataManager.save(type);
        }
    }

    private void initIdentificationType(){
        List<IdentificationType> identificationTypes = this.dataManager.load(IdentificationType.class).all().list();

        if(identificationTypes.stream().noneMatch(a -> a.getCode().equals("P01"))){
            IdentificationType type = this.dataManager.create(IdentificationType.class);
            type.setCode("P01");
            type.setName("MyKad");
            type.setDescription("MyKad ID");
            this.dataManager.save(type);
        }
        if(identificationTypes.stream().noneMatch(a -> a.getCode().equals("P03"))){
            IdentificationType type = this.dataManager.create(IdentificationType.class);
            type.setCode("P03");
            type.setName("Passport");
            type.setDescription("Passport");
            this.dataManager.save(type);
        }

//        if(identificationTypes.stream().noneMatch(a -> a.getCode().equals("P04"))){
//            IdentificationType type = this.dataManager.create(IdentificationType.class);
//            type.setCode("P04");
//            type.setName("MyTentera");
//            type.setDescription("Army ID");
//            this.dataManager.save(type);
//        }
//        if(identificationTypes.stream().noneMatch(a -> a.getCode().equals("P05"))){
//            IdentificationType type = this.dataManager.create(IdentificationType.class);
//            type.setCode("P05");
//            type.setName("Police");
//            type.setDescription("Police ID");
//            this.dataManager.save(type);
//        }
        if(identificationTypes.stream().noneMatch(a -> a.getCode().equals("P09"))){
            IdentificationType type = this.dataManager.create(IdentificationType.class);
            type.setCode("P09");
            type.setName("MyPR");
            type.setDescription("MyPR ID");
            this.dataManager.save(type);
        }
    }

    private void initSecurityQuestion(){
        List<SecurityQuestion> securityQuestions = this.dataManager.load(SecurityQuestion.class).all().list();

        if(securityQuestions.stream().noneMatch(a -> a.getCode().equals("Q01"))){
            SecurityQuestion type = this.dataManager.create(SecurityQuestion.class);
            type.setCode("Q01");
            type.setName("Cat Name");
            type.setDescription("What is your cat name?");
            this.dataManager.save(type);
        }
        if(securityQuestions.stream().noneMatch(a -> a.getCode().equals("Q02"))){
            SecurityQuestion type = this.dataManager.create(SecurityQuestion.class);
            type.setCode("Q02");
            type.setName("Birth Place");
            type.setDescription("In what city were you born?");
            this.dataManager.save(type);
        }
        if(securityQuestions.stream().noneMatch(a -> a.getCode().equals("Q03"))){
            SecurityQuestion type = this.dataManager.create(SecurityQuestion.class);
            type.setCode("Q03");
            type.setName("Mother's Maiden");
            type.setDescription("What is your mother's maiden name?");
            this.dataManager.save(type);
        }
        if(securityQuestions.stream().noneMatch(a -> a.getCode().equals("Q04"))){
            SecurityQuestion type = this.dataManager.create(SecurityQuestion.class);
            type.setCode("Q04");
            type.setName("Favourite Pet");
            type.setDescription("What is the name of your favourite pet?");
            this.dataManager.save(type);
        }
        if(securityQuestions.stream().noneMatch(a -> a.getCode().equals("Q05"))){
            SecurityQuestion type = this.dataManager.create(SecurityQuestion.class);
            type.setCode("Q05");
            type.setName("High School");
            type.setDescription("What high school did you attend?");
            this.dataManager.save(type);
        }
        if(securityQuestions.stream().noneMatch(a -> a.getCode().equals("Q06"))){
            SecurityQuestion type = this.dataManager.create(SecurityQuestion.class);
            type.setCode("Q06");
            type.setName("Spouse First Meet Place");
            type.setDescription("Where did you meet your spouse?");
            this.dataManager.save(type);
        }
        if(securityQuestions.stream().noneMatch(a -> a.getCode().equals("Q07"))){
            SecurityQuestion type = this.dataManager.create(SecurityQuestion.class);
            type.setCode("Q07");
            type.setName("Elementary School");
            type.setDescription("What was the name of your elementary school?");
            this.dataManager.save(type);
        }
    }

    private void initKycStatus(){
        List<KycStatus> bankList = this.dataManager.load(KycStatus.class).all().list();
        Map<String, String> map = new HashMap<String,String>();
        map.put("G05/01","verification.pending");
        map.put("G05/02","verification.pending-approval");
        map.put("G09","verification.accepted");
        map.put("G10","verification.declined");

        SaveContext saveContext = new SaveContext();
        for (String key: map.keySet()){
            KycStatus type = this.dataManager.create(KycStatus.class);
            if(bankList.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key));
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }

    private void initTierType(){
        List<TierType> bankList = this.dataManager.load(TierType.class).all().list();
        Map<String, String> map = new HashMap<String,String>();
        map.put("T00","Tier 0");
        map.put("T01","Tier 1");
        map.put("T02","Tier 2");
        map.put("T03","Tier 3");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            TierType type = this.dataManager.create(TierType.class);
            if(bankList.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key));
//                this.dataManager.save(type);
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }

    private void initKycType(){
        List<KycType> bankList = this.dataManager.load(KycType.class).all().list();
        Map<String, String> map = new HashMap<String,String>();
        map.put("T01","Tier 1");
        map.put("T02","Tier 2");
        map.put("T03","Tier 3");
        map.put("OCR","OCR");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            KycType type = this.dataManager.create(KycType.class);
            if(bankList.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key));
//                this.dataManager.save(type);
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }

    private void initBankList(){
        List<Bank> bankList = this.dataManager.load(Bank.class).all().list();

        Map<String, Tuple2<String, String>> map = new HashMap<>();
        map.put("ABB",new Tuple2<>("PHBMMYKL", "Affin Bank Berhad"));
        map.put("ABMB",new Tuple2<>("MFBBMYKL", "Alliance Bank Malaysia Berhad"));
        map.put("ARM",new Tuple2<>("RJHIMYKL", "Al-Rajhi"));
        map.put("AMFB",new Tuple2<>("ARBKMYKL", "Ambank Malaysia Berhad"));
        map.put("BIMB",new Tuple2<>("BIMBMYKL", "Bank Islam Malaysia Berhad"));
        map.put("BKRB",new Tuple2<>("BKRMMYKL", "Bank Kerjasama Rakyat Malaysia Berhad"));
        map.put("BMMB",new Tuple2<>("BMMBMYKL", "Bank Muamalat Malaysia Bhd"));
        map.put("BOA",new Tuple2<>("BOFAMY2X", "Bank of America (M) Berhad"));
        map.put("BOCM",new Tuple2<>("BKCHMYKL", "Bank of China (M) Berhad"));
        map.put("BPM",new Tuple2<>("AGOBMYKL", "Agrobank"));
        map.put("BSNB",new Tuple2<>("BSNAMYK1", "Bank Simpanan Nasional Berhad"));
        map.put("BOTM",new Tuple2<>("BOTKMYKX", "Bank of Tokyo-Mitsubishi UFJ (M) Berhad"));
        map.put("BNP",new Tuple2<>("BNPAMYKL", "BNP Paribas Malaysia Berhad"));
        map.put("CIMB",new Tuple2<>("CIBBMYKL", "CIMB Bank Berhad"));
        map.put("CITI",new Tuple2<>("CITIMYKL", "Citibank Berhad"));
        map.put("DB",new Tuple2<>("DEUTMYKL", "Deutsche Bank (Malaysia) Berhad"));
        map.put("HLB",new Tuple2<>("HLBBMYKL", "Hong Leong Bank Berhad"));
        map.put("HSBC",new Tuple2<>("HBMBMYKL", "HSBC Bank Malaysia Berhad"));
        map.put("ICB",new Tuple2<>("ICBKMYKL", "Industrial and Commercial Bank of China (M) Berhad"));
        map.put("JPMC",new Tuple2<>("CHASMYKX", "JP Morgan Chase Bank Berhad"));
        map.put("KFH",new Tuple2<>("KFHOMYKL", "Kuwait Finance House"));
        map.put("MBB",new Tuple2<>("MBBEMYKL", "Maybank Berhad"));
        map.put("MCBM",new Tuple2<>("MHCBMYKA", "Mizuho Bank (Malaysia) Berhad"));
        map.put("OCBC",new Tuple2<>("OCBCMYKL", "OCBC Bank Berhad"));
        map.put("PBB",new Tuple2<>("PBBEMYKL", "Public Bank Berhad"));
        map.put("RHB",new Tuple2<>("RHBBMYKL", "RHB Bank Berhad"));
        map.put("SCB",new Tuple2<>("SCBLMYKX", "Standard Chartered Bank Malaysia Berhad"));
        map.put("SMBC",new Tuple2<>("SMBCMYKL", "Sumitomo Mitsui Banking Corporation (M) Berhad"));
        map.put("UOB",new Tuple2<>("UOVBMYKL", "United Overseas Bank Berhad (UOB)"));
        map.put("MBSB",new Tuple2<>("AFBQMYKL", "MBSB Bank Berhad"));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            Bank type = this.dataManager.create(Bank.class);
            if(bankList.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setBicCode(map.get(key).getV1());
                type.setName(map.get(key).getV2());
//                this.dataManager.save(type);
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initAgreementAcknowledgementType(){
        List<AcknowledgementType> acknowledgementTypes = this.dataManager.load(AcknowledgementType.class).all().list();

        Map<String, Tuple2<String, String>> map = new HashMap<>();
        map.put("ACK01",new Tuple2<>("Terms and Conditions Agreement", "Agree to the General Terms and Conditions"));
        map.put("ACK02",new Tuple2<>("Receive Marketing Promotion Agreement", "Agree to receive marketing" +
                " communications and materials"));
//        map.put("ACK03",new Tuple2<>("Not U.S. Person Confirmation", "Confirm that neither a U.S. person" +
//                " nor a tax resident in any other jurisdiction under international tax regulations"));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            AcknowledgementType type = this.dataManager.create(AcknowledgementType.class);
            if(acknowledgementTypes.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key).getV1());
                type.setDescription(map.get(key).getV2());
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }

    private void initDeclarationType(){
        List<DeclarationType> declarationTypes = this.dataManager.load(DeclarationType.class).all().list();

        Map<String, Tuple2<String, String>> map = new HashMap<>();
        map.put("DCLRN01",new Tuple2<>("Not U.S. Citizenship", "i am not a U.S. citizen"));
        map.put("DCLRN02",new Tuple2<>("Not U.S. Green card Holder", "I am not a U.S. green card holder"));
        map.put("DCLRN03",new Tuple2<>("Not U.S. born", "I was not born in the U.S."));
        map.put("DCLRN04",new Tuple2<>("Malaysia Tax Residence", "I am not a tax residence of countries other than Malaysia"));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            DeclarationType type = this.dataManager.create(DeclarationType.class);
            if(declarationTypes.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key).getV1());
                type.setDescription(map.get(key).getV2());
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initEmploymentStatus(){
        List<EmploymentStatus> employmentStatuses = this.dataManager.load(EmploymentStatus.class).all().list();

        Map<String, Tuple2<String, Integer>> map = new HashMap<>();
        map.put("EMPL", new Tuple2<>("Employed",1));
        map.put("SLEMPL", new Tuple2<>("Self-employed",2));
        map.put("UNEMPL", new Tuple2<>("Unemployed",3));
        map.put("RETIRED", new Tuple2<>("Retired",4));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            EmploymentStatus status = this.dataManager.create(EmploymentStatus.class);
            if(employmentStatuses.stream().noneMatch(a -> a.getCode().equals(key))){
                status.setCode(key);
                status.setName(map.get(key).getV1());
                status.setSorting(map.get(key).getV2());
                saveContext.saving(status);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initPositionList(){
        List<JobPosition> positions = this.dataManager.load(JobPosition.class).all().list();
        // order by code
        Map<String, String> map = new HashMap<>();
        map.put("PSN01", "Director");
        map.put("PSN02", "Senior Management");
        map.put("PSN03", "Professional");
        map.put("PSN04", "Employee");
        map.put("PSN05", "Others");
        map.put("PSN06", "Not Applicable");
        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            JobPosition position = this.dataManager.create(JobPosition.class);
            if(positions.stream().noneMatch(a -> a.getCode().equals(key))){
                position.setCode(key);
                position.setName(map.get(key));
                saveContext.saving(position);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initNatureOfBusiness(){
        List<NatureOfBusiness> natureOfBusinesses = this.dataManager.load(NatureOfBusiness.class).all().list();

        Map<String, Tuple2<String, String>> map = new HashMap<>();

        map.put("A",new Tuple2<>("Agriculture, Forestry and Fishing", ""));
        map.put("B",new Tuple2<>("Mining and Quarrying", ""));
        map.put("C",new Tuple2<>("Manufacturing", ""));
        map.put("D",new Tuple2<>("Electricity, Gas, Steam and Air Conditioning Supply", ""));
        map.put("E",new Tuple2<>("Water Supply, Sewerage, Waste Management and Remediation Activities", ""));
        map.put("F",new Tuple2<>("Construction", ""));
        map.put("G",new Tuple2<>("Wholesale and Retail Trade", ""));
        map.put("H",new Tuple2<>("Transportation and Storage", ""));
        map.put("I",new Tuple2<>("Accommodation and Food Service Activities", ""));
        map.put("J",new Tuple2<>("Information and Communication", ""));
        map.put("K",new Tuple2<>("Financial and Insurance/Takaful Activities", ""));
        map.put("L",new Tuple2<>("Real Estate Activities", ""));
        map.put("M",new Tuple2<>("Professional, Scientific and Technical Activities", ""));
        map.put("N",new Tuple2<>("Administrative and Support Service Activities", ""));
        map.put("O",new Tuple2<>("Public Administration and Defence; Compulsory Social Security", ""));
        map.put("P",new Tuple2<>("Education", ""));
        map.put("Q",new Tuple2<>("Human Health and Social Work Activities", ""));
        map.put("R",new Tuple2<>("Arts, Entertainment and Recreation", ""));
        map.put("S",new Tuple2<>("Other Service Activities", ""));
        map.put("T",new Tuple2<>("Activities of Households as Employers", ""));
        map.put("U",new Tuple2<>("Activities of Extraterritorial Organizations and Bodies", ""));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            NatureOfBusiness type = this.dataManager.create(NatureOfBusiness.class);
            if(natureOfBusinesses.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key).getV1());
                type.setDescription(map.get(key).getV2());
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initVerificationType(){
        List<VerificationType> verificationType = this.dataManager.load(VerificationType.class).all().list();

        Map<String, String> map = new HashMap<>();
        map.put("VET01", "Tier1 Upgrade");
        map.put("VET02", "Edit Profile");
        map.put("VET11", "Periodic Review");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            VerificationType type = this.dataManager.create(VerificationType.class);
            if(verificationType.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key));
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initNsResult(){
        List<NameScreeningResult> nsResult = this.dataManager.load(NameScreeningResult.class).all().list();

        Map<String, String> map = new HashMap<>();
        map.put("G30", "Accept");
        map.put("G31", "Manual Review");
        map.put("G32", "Decline");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            NameScreeningResult status = this.dataManager.create(NameScreeningResult.class);
            if(nsResult.stream().noneMatch(a -> a.getCode().equals(key))){
                status.setCode(key);
                status.setName(map.get(key));
                saveContext.saving(status);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initCrpResult(){
        List<CrpResult> crpResults = this.dataManager.load(CrpResult.class).all().list();

        Map<String, String> map = new HashMap<>();
        map.put("LOW", "Low Risk");
        map.put("MEDIUM", "Medium Risk");
        map.put("HIGH", "High Risk");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            CrpResult status = this.dataManager.create(CrpResult.class);
            if(crpResults.stream().noneMatch(a -> a.getCode().equals(key))){
                status.setCode(key);
                status.setName(map.get(key));
                saveContext.saving(status);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initVerificationResult(){
        List<OveralVerificationResult> verificationResults = this.dataManager.load(OveralVerificationResult.class).all().list();

        Map<String, String> map = new HashMap<>();
//        map.put("G05", "Pending");
        map.put("G05/01", "Pending-Verification");
        map.put("G05/02", "Pending-Approval");
        map.put("G09", "Accepted");
        map.put("G10", "Declined");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            OveralVerificationResult status = this.dataManager.create(OveralVerificationResult.class);
            if(verificationResults.stream().noneMatch(a -> a.getCode().equals(key))){
                status.setCode(key);
                status.setName(map.get(key));
                saveContext.saving(status);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initAssessmentStatus(){
        List<AssessmentStatus> assessmentStatuses = this.dataManager.load(AssessmentStatus.class).all().list();

        Map<String, String> map = new HashMap<>();
        map.put("G24", "Ongoing");
        map.put("G01", "Completed");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            AssessmentStatus status = this.dataManager.create(AssessmentStatus.class);
            if(assessmentStatuses.stream().noneMatch(a -> a.getCode().equals(key))){
                status.setCode(key);
                status.setName(map.get(key));
                saveContext.saving(status);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initTransactionPurposeType(){
        List<TransactionPurposeType> purposes = this.dataManager.load(TransactionPurposeType.class).all().list();

        Map<String, String> map = new HashMap<>();
        map.put("TXP01", "Investment");
        map.put("TXP02", "Saving");
        map.put("TXP03", "Diversification");
        map.put("TXP04", "Wealth Preservation");
        map.put("TXP05", "Retirement");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            TransactionPurposeType type = this.dataManager.create(TransactionPurposeType.class);
            if(purposes.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key));
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initTotalMontlyTrans(){
        List<TotalMonthlyTransaction> transactions = this.dataManager.load(TotalMonthlyTransaction.class).all().list();

        Map<String, Tuple3<String, BigDecimal, BigDecimal>> map = new HashMap<>();
        map.put("TXT01",new Tuple3<>("Below RM3,000",BigDecimal.valueOf(0), BigDecimal.valueOf(2999.99)));
        map.put("TXT02",new Tuple3<>("RM3,000 - RM10,000",BigDecimal.valueOf(3000), BigDecimal.valueOf(9999.99)));
        map.put("TXT03",new Tuple3<>("More Than RM10,000",BigDecimal.valueOf(10000), null));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            TotalMonthlyTransaction type = this.dataManager.create(TotalMonthlyTransaction.class);
            if(transactions.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key).getV1());
                type.setMinTransRM(map.get(key).getV2());
                type.setMaxTransRM(map.get(key).getV3());
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initFreqMontlyTrans(){
        List<FreqMonthlyTransaction> transactions = this.dataManager.load(FreqMonthlyTransaction.class).all().list();

        Map<String, Tuple3<String, Integer, Integer>> map = new HashMap<>();
        map.put("TXF10BL",new Tuple3<>("Below 10 transactions", 0, 10));
        map.put("TXF30BL",new Tuple3<>("11 - 30 transactions",11, 30));
        map.put("TXF30AB",new Tuple3<>("More than 30 transactions",31, null));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            FreqMonthlyTransaction type = this.dataManager.create(FreqMonthlyTransaction.class);
            if(transactions.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key).getV1());
                type.setMinTransFreq(map.get(key).getV2());
                type.setMaxTransFreq(map.get(key).getV3());
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initSourceOfFund(){
        List<SourceOfFundType> sources = this.dataManager.load(SourceOfFundType.class).all().list();

        Map<String, String> map = new HashMap<>();
        map.put("FUND01", "Business revenue");
        map.put("FUND02", "Employment income");
        map.put("FUND03", "Family support");
        map.put("FUND04", "Inheritance or gifts");
        map.put("FUND05", "Investment, dividends or royalties income");
        map.put("FUND06", "Legal settlement");
        map.put("FUND07", "Loan");
        map.put("FUND08", "Pension funds");
        map.put("FUND09", "Rental income");
        map.put("FUND10", "Sale of property or assets");
        map.put("FUND11", "Saving");
        map.put("FUND99", "Others");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            SourceOfFundType type = this.dataManager.create(SourceOfFundType.class);
            if(sources.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key));
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
    private void initSourceOfWealth(){
        List<SourceOfWealthType> sources = this.dataManager.load(SourceOfWealthType.class).all().list();

        Map<String, String> map = new HashMap<>();
        map.put("WEALTH01", "Business ownership");
        map.put("WEALTH02", "Employment and salary");
        map.put("WEALTH03", "Inheritance and family wealth");
        map.put("WEALTH04", "Intellectual property");
        map.put("WEALTH05", "Investment");
        map.put("WEALTH06", "Legal settlement");
        map.put("WEALTH07", "Real estate or assets holdings");
        map.put("WEALTH08", "Rental income");
        map.put("WEALTH09", "Trust funds");
        map.put("WEALTH99", "Others");

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            SourceOfWealthType type = this.dataManager.create(SourceOfWealthType.class);
            if(sources.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key));
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }
}
