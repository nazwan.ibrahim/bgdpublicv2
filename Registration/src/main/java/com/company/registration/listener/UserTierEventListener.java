package com.company.registration.listener;

import com.company.registration.entity.KycStatus;
import com.company.registration.entity.UserTier;
import io.jmix.core.DataManager;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.List;

@Component
public class UserTierEventListener {
    @Autowired
    private DataManager dataManager;

    private UserTier userTierGlobal;

    @EventListener
    public void onUserTierLoading(EntityLoadingEvent<UserTier> event) {

    }

    @EventListener
    public void onUserTierSaving(EntitySavingEvent<UserTier> event) {

    }

    @EventListener
    public void onUserTierChangedBeforeCommit(EntityChangedEvent<UserTier> event) {

    }

    @TransactionalEventListener
    public void onUserTierChangedAfterCommit(EntityChangedEvent<UserTier> event) {

    }
}