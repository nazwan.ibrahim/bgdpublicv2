package com.company.registration.listener;

import com.company.registration.entity.User;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class UserEventListener {

    @EventListener
    public void onUserLoading(EntityLoadingEvent<User> event) {

    }

    @EventListener
    public void onUserSaving(EntitySavingEvent<User> event) {

        // code ecrypt
    }

    @EventListener
    public void onUserChangedBeforeCommit(EntityChangedEvent<User> event) {

    }

    @TransactionalEventListener
    public void onUserChangedAfterCommit(EntityChangedEvent<User> event) {

    }
}