package com.company.registration.responseForm;

import com.company.registration.form.TypeForm;
import com.company.registration.form.TypeFormWithID;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class UserSecurityQuestions implements Serializable {
    private String userID;
    private List<TypeFormWithID> userSecurityQuestion;

    public List<TypeFormWithID> getUserSecurityQuestion() {
        return userSecurityQuestion;
    }

    public void setUserSecurityQuestion(List<TypeFormWithID> userSecurityQuestion) {
        this.userSecurityQuestion = userSecurityQuestion;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
