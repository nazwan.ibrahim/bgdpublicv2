package com.company.registration.responseForm;

import com.company.registration.entity.CountryList;
import com.company.registration.entity.StateList;
import com.company.registration.form.CodeForm;
import com.company.registration.form.TypeForm;

import java.util.UUID;

public class AddressObj {
    private UUID id;
    private String recipientName;
    private String contactNumber;
    private String label;
    private String address1;
    private String address2;
    private String postcode;
    private String city;
    private CodeForm state;
    private CodeForm country;
    private TypeForm addressType;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public TypeForm getAddressType() {
        return addressType;
    }

    public void setAddressType(TypeForm addressType) {
        this.addressType = addressType;
    }

    public CodeForm getState() {
        return state;
    }

    public void setState(CodeForm state) {
        this.state = state;
    }

    public CodeForm getCountry() {
        return country;
    }

    public void setCountry(CodeForm country) {
        this.country = country;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
