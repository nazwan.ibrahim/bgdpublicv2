package com.company.registration.responseForm;

import com.company.registration.form.CodeForm;

public class PostcodeInfo {
    private String postcode;
    private String city;
    private CodeForm state;
    private CodeForm country;

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public CodeForm getState() {
        return state;
    }

    public void setState(CodeForm state) {
        this.state = state;
    }

    public CodeForm getCountry() {
        return country;
    }

    public void setCountry(CodeForm country) {
        this.country = country;
    }
}
