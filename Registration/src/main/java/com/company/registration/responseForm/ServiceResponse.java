package com.company.registration.responseForm;

import com.company.registration.entity.ErrorCode;
import com.company.registration.entity.SuccessCode;

public class ServiceResponse {
    private SuccessCode successCode;
    private ErrorCode errorCode;
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SuccessCode getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(SuccessCode successCode) {
        this.successCode = successCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
