package com.company.registration.responseForm;

import com.company.registration.form.CodeForm;

public class FinancialDetailsResponse {
    private String userName;
    private CodeForm transactionPurpose;
    private CodeForm totalMonthlyTransaction;
    private CodeForm freqMonthlyTransaction;
    private CodeForm sourceOfFund;
    private String otherSourceOfFund;
    private CodeForm sourceOfWealth;
    private String otherSourceOfWealth;

    public CodeForm getTransactionPurpose() {
        return transactionPurpose;
    }

    public void setTransactionPurpose(CodeForm transactionPurpose) {
        this.transactionPurpose = transactionPurpose;
    }

    public CodeForm getTotalMonthlyTransaction() {
        return totalMonthlyTransaction;
    }

    public void setTotalMonthlyTransaction(CodeForm totalMonthlyTransaction) {
        this.totalMonthlyTransaction = totalMonthlyTransaction;
    }

    public CodeForm getFreqMonthlyTransaction() {
        return freqMonthlyTransaction;
    }

    public void setFreqMonthlyTransaction(CodeForm freqMonthlyTransaction) {
        this.freqMonthlyTransaction = freqMonthlyTransaction;
    }

    public CodeForm getSourceOfFund() {
        return sourceOfFund;
    }

    public void setSourceOfFund(CodeForm sourceOfFund) {
        this.sourceOfFund = sourceOfFund;
    }

    public CodeForm getSourceOfWealth() {
        return sourceOfWealth;
    }

    public void setSourceOfWealth(CodeForm sourceOfWealth) {
        this.sourceOfWealth = sourceOfWealth;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOtherSourceOfFund() {
        return otherSourceOfFund;
    }

    public void setOtherSourceOfFund(String otherSourceOfFund) {
        this.otherSourceOfFund = otherSourceOfFund;
    }

    public String getOtherSourceOfWealth() {
        return otherSourceOfWealth;
    }

    public void setOtherSourceOfWealth(String otherSourceOfWealth) {
        this.otherSourceOfWealth = otherSourceOfWealth;
    }
}
