package com.company.registration.app;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("testing")
public class TestingController {

    @GetMapping("/validatePasswordFormat")
    public Boolean validatePassword(@RequestParam String password){

        if(password != null && !password.equals("")){
//          String passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#%^&*();,.?:{}|<>$_-=+[]])[A-Za-z\d!@#%^&*();,.?:{}|<>$_\-=+\[\]]{8,}$";
            String passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$!%^?&(){}|<>_*:;,.+=])(?=\\S+$).{8,}$";
            return password.matches(passwordRegex);
        }
        return false;
    }

}
