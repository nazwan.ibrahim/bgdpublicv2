package com.company.registration.app;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.company.registration.entity.*;
import com.company.registration.form.*;
import com.company.registration.responseForm.*;
import com.company.registration.services.CipherEncryption;
import com.company.registration.services.UserService;

import com.company.registration.services.ValidationService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jmix.core.DataManager;
import io.jmix.core.SaveContext;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;

//import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("user")
public class UserController {
    static Logger logger = LogManager.getLogger(UserController.class.getName());

    @Autowired
    private DataManager dataManager;
    @Autowired
    private Environment environment;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private CipherEncryption cipherEncryption;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private ValidationService validation;
    private AmazonS3 s3Client = null;
    private List<ActivationStatus> actStatusTypes;
    private List<DeclarationType> declarationTypes;
    private List<EmploymentStatus> employmentStatuses;
    private List<JobPosition> jobPositions;
    private List<NatureOfBusiness> natureOfBusinesses;
//    private List<UserType> userTypes;
    private List<IdentificationType> identificationTypes;
//    private List<TierType> tierTypes;
    private List<TransactionPurposeType> transactionPurposeTypes;
    private List<TotalMonthlyTransaction> totalMonthlyTransactions;
    private List<FreqMonthlyTransaction> freqMonthlyTransactions;
    private List<SourceOfFundType> sourceOfFundTypes;
    private List<SourceOfWealthType> sourceOfWealthTypes;
    private List<VerificationType> verificationTypes;

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationStartedEvent.class)
    public void doSomethingAfterStartup() {
        loadEntityTypes();
    }
    public void loadEntityTypes(){
        this.actStatusTypes = this.dataManager.load(ActivationStatus.class).all().list();
        this.declarationTypes = this.dataManager.load(DeclarationType.class).all().list();
        this.employmentStatuses = this.dataManager.load(EmploymentStatus.class).all().list();
        this.jobPositions = this.dataManager.load(JobPosition.class).all().list();
        this.natureOfBusinesses = this.dataManager.load(NatureOfBusiness.class).all().list();
//        this.userTypes = this.dataManager.load(UserType.class).all().list();
        this.identificationTypes = this.dataManager.load(IdentificationType.class).all().list();
//        this.tierTypes = this.dataManager.load(TierType.class).all().list();
        this.transactionPurposeTypes = this.dataManager.load(TransactionPurposeType.class).all().list();
        this.totalMonthlyTransactions = this.dataManager.load(TotalMonthlyTransaction.class).all().list();
        this.freqMonthlyTransactions = this.dataManager.load(FreqMonthlyTransaction.class).all().list();
        this.sourceOfFundTypes = this.dataManager.load(SourceOfFundType.class).all().list();
        this.sourceOfWealthTypes = this.dataManager.load(SourceOfWealthType.class).all().list();
        this.verificationTypes = this.dataManager.load(VerificationType.class).all().list();
    }

    //--    Internal used function      --//
    public void setS3Client(){
        String regionName = environment.getProperty("AWS_REGION");
        Regions clientRegion = Regions.fromName(regionName);
        String accessKey = environment.getProperty("AWS_ACCESS_KEY");
        String secretKey = environment.getProperty("AWS_SECRET_KEY");

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
        s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(clientRegion)
                .build();
    }
    public  URL getProfilePicture(UUID userID){
        try{
            if (s3Client == null){
                setS3Client();
            }
            String key = environment.getProperty("secret_key");
            Optional<UserProfilePicture> userProfilePicture = this.dataManager.load(UserProfilePicture.class)
                    .query("select e from UserProfilePicture e where e.user.id = :user")
                    .parameter("user", userID).optional();

            if (userProfilePicture.isEmpty() || userProfilePicture.get().getLocation() == null || Objects.equals(userProfilePicture.get().getLocation(), "")){
                return null;
            }

            String[] parts = userProfilePicture.get().getLocation().split("/",2);
            String bucketName = parts[0];
            String objectKey = parts[1];

            // Set the presigned URL to expire after 12 hours (s3 only allowed maximum 12 hours)
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = Instant.now().toEpochMilli();
            expTimeMillis += 1000 * 60 * 60 * 12;
            expiration.setTime(expTimeMillis);

            // Generate the presigned URL.
            GeneratePresignedUrlRequest generatePresignedUrlRequest =
                    new GeneratePresignedUrlRequest(bucketName, objectKey)
                            .withMethod(HttpMethod.GET)
                            .withExpiration(expiration);

            return s3Client.generatePresignedUrl(generatePresignedUrlRequest);
        }
        catch (AmazonServiceException ex){
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            logger.error("S3 processing error: {} ",ex.getMessage());
            return null;
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return null;
        }
    }
    private Boolean checkCountryCode(String phoneNumber){
        if(phoneNumber.startsWith("+")){
            String countryCode = phoneNumber.substring(1,Math.min(4, phoneNumber.length()));
            return countryCode.matches("\\d+");
        }
        return false;
    }
    private ErrorResponse setErrResponse(ErrorCode errorCode, String message){
        ErrorResponse response = new ErrorResponse();
        response.setErrorCode(errorCode.getId());
        response.setMessage(message);
        return response;
    }
    private String trimIdNumber(String idNumber){
        if(idNumber!=null && !idNumber.equals(""))
            return idNumber.replaceAll("\\s", "").replaceAll("-","");
        else
            return idNumber;
    }

    //--    API     --//
    @GetMapping("/test")
    public ResponseEntity test(){
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

    @GetMapping("/accountStatus")
    public  ResponseEntity getAccountStatus(String email){
        try{
            logger.info("GET: /user/accountStatus -> " + email);
//            String queryStr = "select e from User e where lower(e.username) = lower(:username)";
//            User user2 = this.dataManager.load(User.class)
//                    .query(queryStr)
//                    .parameter("username", email).one();

            List<User> user = this.dataManager.load(User.class)
                    .query("select e from User e where lower(e.email) = lower(:email)")
                    .parameter("email",email)
                    .list();
            // size==0 -> no user
            // size == 1 and not verified -> verified == false
            // size == 1 and verified -> verified == true
            // size > 1; check if there is verified email
            if(user.size() == 0){
                logger.info("User not exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid credentials\"}");
            }
            Optional<User> isVerified = user.stream().filter(f -> f.getEmail_verified().equals(true)).findAny();
            boolean emailVerified = isVerified.isPresent();
            Optional<UserSession> userSession = this.dataManager.load(UserSession.class)
                    .condition(PropertyCondition.equal("createdBy",user.get(0).getUsername())).optional();
            Boolean firstTimeLogin = true;
            if(userSession.isPresent())
                firstTimeLogin = false;
            String ssoUserID = "";
            if(user.get(0)!=null)
                ssoUserID = user.get(0).getSsoUserID().toString();
            // add status first time login true or false
            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("emailVerified",emailVerified);
            jsonResponse.put("firstTimeLogin",firstTimeLogin);
            jsonResponse.put("ssoUserID",ssoUserID);
            jsonResponse.put("accountStatus",user.get(0).getActStatus().getCode());
//            return ResponseEntity.status(HttpStatus.OK).body("{\"emailVerified\":\""+emailVerified+"\"}");
            String jsonStr = jsonResponse.toString();
            logger.info(jsonStr);
            return ResponseEntity.status(HttpStatus.OK).body(jsonStr);

        }catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid credentials\"}");
        }
    }

    @GetMapping("/name")
    public  ResponseEntity getName(String username){
        try{
            logger.info("GET: /name?username="+username);
            User user = this.dataManager.load(User.class)
                    .query("select e from User e where e.username = :username")
                    .parameter("username",username)
                    .one();


            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("username",username);
            jsonResponse.put("name",user.getFull_name());
            logger.info("Response -> GET: /name :" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body(jsonResponse.toString());

        }catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
        }
    }

    @GetMapping("/details")
    public  ResponseEntity getUserDetails(){
        try{
            logger.info("GET: user/details");
            //declare variables
            String icNo = null;
            String phoneNumber = null;

            String key = environment.getProperty("secret_key");
            UserDetails userDetails = currentAuthentication.getUser();
            VerificationType defaultVerType = verificationTypes.stream()
                    .filter(f -> f.getCode().equals("VET01")).collect(Collectors.toList()).get(0);

            //ArrayList arrayList = new ArrayList<>();
            List<RoleDTO> roleDTOS = new ArrayList<RoleDTO>();
            userDetails.getAuthorities().forEach(e -> {
                RoleDTO roleDTO = dataManager.create(RoleDTO.class);
                roleDTO.setRole(e.getAuthority());
                roleDTOS.add(roleDTO);
//                arrayList.add(e.getAuthority());
            });
            String userName = userDetails.getUsername();
            logger.info("user: -> "+ userName);
            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            // get user tier
            TierType tierStatus = null;

            List<UserTier> userTiers = this.dataManager.load(UserTier.class)
                    .query("select e from UserTier e where e.user.id = :user " +
                            "and e.currentTier = true")
                    .parameter("user", user.getId()).list();

            TypeDTO verificationStatus = dataManager.create(TypeDTO.class);
            TypeDTO cadAssessmentStatus = dataManager.create(TypeDTO.class);
            TypeDTO verificationType = dataManager.create(TypeDTO.class);
            //set default verification type to upgrade tier
            verificationType.setCode(defaultVerType.getCode());
            verificationType.setName(defaultVerType.getName());
            verificationType.setDescription(defaultVerType.getDescription());

            UserTier userTier = null;
            if (userTiers.size() == 1){
                userTier = userTiers.get(0);
            }
            else if (userTiers.size() > 1){
                userTier = userTiers.stream()
                        .sorted(Comparator.comparing(UserTier::getCreatedDate).reversed())
                        .collect(Collectors.toList()).get(0);
            }
            if(userTier != null){
                tierStatus = userTier.getTier();
                if( userTier.getVerification() != null){
                    logger.info("cad verification for current tier exist");
                    UserVerification userVerification = userTier.getVerification();
                    OveralVerificationResult verificationResult = userVerification.getOveralVerificationResult();
                    if(verificationResult!=null){
                        verificationStatus.setCode(verificationResult.getCode());
                        verificationStatus.setName(verificationResult.getName());
                        verificationStatus.setDescription(verificationResult.getDescription());
                    }
                    AssessmentStatus cadAssessment = userVerification.getAssessmentStatus();
                    if(cadAssessment!=null){
                        cadAssessmentStatus.setCode(cadAssessment.getCode());
                        cadAssessmentStatus.setName(cadAssessment.getName());
                        cadAssessmentStatus.setDescription(cadAssessment.getDescription());
                    }
                    VerificationType verType = userVerification.getVerificationType();
                    if(verType != null){
                        verificationType.setCode(verType.getCode());
                        verificationType.setName(verType.getName());
                        verificationType.setDescription(verType.getDescription());
                    }
                }
            }

            // get profile picture
            URL getProfileUrl = getProfilePicture(user.getId());
            String profileUrl = null;
            if (getProfileUrl != null)
                profileUrl = getProfileUrl.toString();

            //decrypt ic number and phone number before response
            if(user.getIdentificationNumber()!= null && !Objects.equals(user.getIdentificationNumber(), "")){
                try{
                    icNo = cipherEncryption.decrypt(user.getIdentificationNumber(), key);
                }
                catch (Exception ex){
                    logger.error(ex.getMessage());
                    logger.info("Failed to decrypt ic number");
                }
            }
            if(user.getPhoneNumber()!= null && !Objects.equals(user.getPhoneNumber(), "")){
                try{
                    phoneNumber = cipherEncryption.decrypt(user.getPhoneNumber(),key);
                }
                catch (Exception ex){
                    logger.error(ex.getMessage());
                    logger.info("Failed to decrypt phone number");
                }
            }
            String birthday = null;
            if(user.getBirthday() != null && !Objects.equals(user.getBirthday(),""))
                birthday = (cipherEncryption.decrypt(user.getBirthday(), key));

            Boolean allowPushNotification=null;
            Boolean allowEmailNotification=null;

            String queryPermission = "select e from UserPermission e where e.user.id = :userID";
            Optional<UserPermission> userPermissionOptional = this.dataManager.load(UserPermission.class)
                    .query(queryPermission).parameter("userID",user.getId()).optional();
            if(userPermissionOptional.isPresent()){
                UserPermission userPermission = userPermissionOptional.get();
                allowPushNotification = userPermission.getAllowPushNotification();
                allowEmailNotification = userPermission.getAllowEmailNotification();
            }

            UserDetailDTO res = dataManager.create(UserDetailDTO.class);
            res.setUserID(user.getId());
            res.setFullName(user.getFull_name());
            res.setUserName(user.getUsername());
            res.setEmail(user.getEmail());
            res.setPhoneNumber(phoneNumber);
            res.setUserType(user.getUserType());
            if(user.getNationality()!=null){                              // also will detect as a null if not in enums
                res.setNationality(user.getNationality().getId());
                res.setNationalityCode(user.getNationality().name());
                res.setCountry(user.getNationality().getId());
                res.setCountryCode(user.getNationality().name());
            }
            res.setReference(user.getReference());
            res.setReferral(user.getReferral());
            res.setEmailVerified(user.getEmail_verified());
            res.setUserTier(tierStatus);
            res.setTierStatus(verificationStatus);
            res.setPinEnable(user.getPin_enable());
            res.setActStatus(user.getActStatus());
            res.setIdentificationNumber(icNo);
            res.setIdentificationType(user.getIdentificationType());
//            res.setRole(roleDTOS);
            res.setDeviceID(user.getDeviceID());
            res.setProfilePicture(profileUrl);
            res.setDob(birthday);
            res.setCadAssessmentStatus(cadAssessmentStatus);
            res.setVerificationType(verificationType);
            res.setAllowPushNotification(allowPushNotification);
            res.setAllowEmailNotification(allowEmailNotification);
            if(user.getNextReviewDate()!=null)
                res.setNextReviewDate(user.getNextReviewDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            if(user.getIdentificationExpiryDate()!=null)
                res.setIdentificationExpiryDate(user.getIdentificationExpiryDate());
            return ResponseEntity.status(HttpStatus.OK).body(res);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PutMapping("/contactNumbers")
    public ResponseEntity updateContactNumbers(@RequestBody ContactNumbersForm form){

        try{
            logger.info("PUT: /user/contactNumbers");
            String secretKey = environment.getProperty("secret_key");

            // json response
            JSONObject invalidPhoneRes = new JSONObject();
            invalidPhoneRes.put("displayMessage","Please enter a valid phone number");
            invalidPhoneRes.put("message","Invalid phone number format");
            JSONObject invalidCountryRes = new JSONObject();
            invalidCountryRes.put("displayMessage","Please enter a valid country code");
            invalidCountryRes.put("message","Country code not allowed. Account suspended");

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if(form == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Request body cannot be null"));
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_ACCOUNT,"User not exist"));

            if(form.getPhoneNumber()!=null || !Objects.equals(form.getPhoneNumber(), "")){

                // compare new phone number with current
                if (!form.getPhoneNumber().equals(cipherEncryption.decrypt(user.getPhoneNumber(), secretKey))){
                    String queryStrOTP ="select e from Otp e where e.createdBy = :createdBy " +
                            "and e.phoneNumber = :phoneNumber and e.serviceType = :serviceType order by e.createdDate desc";

                    ActivationStatus approvedStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G06"))
                            .collect(Collectors.toList()).get(0);

                    Optional<Otp> otp = this.dataManager.load(Otp.class)
                            .query(queryStrOTP)
                            .parameter("createdBy",user.getUsername().toLowerCase())
                            .parameter("phoneNumber",form.getPhoneNumber())
                            .parameter("serviceType",OtpServiceType.CHANGE_PHONE_NUMBER)
                            .optional();

                    if(otp.isEmpty()){
                        logger.info("No generated OTP found!!!");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid phone number\"}");
                    }
                    else if(!otp.get().getStatus().equals(approvedStatus)){
                        logger.info("Phone number not verified!!!");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid phone number\"}");
                    }
                }

                // checking phone number format
                JSONObject validatePhoneNumber = validation.validatePhoneNumber(form.getPhoneNumber());
                if (validatePhoneNumber.get("validate").equals(false))
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidPhoneRes.toString());

                Boolean code = checkCountryCode(form.getPhoneNumber());
                if (!code)
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid phone number format\"}");
                user.setPhoneNumber(cipherEncryption.encrypt(form.getPhoneNumber(),secretKey));
                user.setHomeNumber(cipherEncryption.encrypt(form.getHomeNumber(),secretKey));
                user.setOfficeNumber(cipherEncryption.encrypt(form.getOfficeNumber(),secretKey));

                // suspend account if phone number other than malaysia number
                if (!validatePhoneNumber.get("countryCode").equals("60")){
                    logger.info(form.getPhoneNumber()," | Account suspend. Phone number country code other than Malaysia");
                    Optional<ActivationStatus> actStatus = this.dataManager.load(ActivationStatus.class)
                            .condition(PropertyCondition.equal("code","G20")).optional();
                    actStatus.ifPresent(user::setActStatus);
                    this.dataManager.save(user);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidCountryRes.toString());
                }
                logger.info("Phone number updated");
                this.dataManager.save(user);
            }
            return ResponseEntity.status(HttpStatus.OK).body(user);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
        }
    }

    @PostMapping("/pin")
    public ResponseEntity createPin(@RequestBody UserPinForm form){
        if(form.getNewPin()==null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Not Found\"}");
        try{
            String key = environment.getProperty("secret_key");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            if(user.getPin_enable() && user.getPin()!=null && !Objects.equals(user.getPin(), ""))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Pin already created\"}");
            user.setPin_enable(true);
            user.setPin(cipherEncryption.encrypt(form.getNewPin().toString(), key));
            this.dataManager.save(user);

            return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\":\"Pin successfully created\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }

    @PutMapping("/pin")
    public ResponseEntity changePin( @RequestBody UserPinForm form){
        try{
            logger.info("PUT: /user/pin");
            String key = environment.getProperty("secret_key");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            //verify pin
            String decryptPin="";
            try{
                decryptPin = cipherEncryption.decrypt(user.getPin(), key);
            }
            catch (Exception ex){
                logger.info("Failed to decrypt pin");
                logger.error(ex.getMessage());
            }
            if(!form.getCurrentPin().toString().equals(decryptPin) || !form.getNewPin().toString().equals(form.getConfirmPin().toString()))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Pin not match\"}");

            user.setPin(cipherEncryption.encrypt(form.getNewPin().toString(), key));
            this.dataManager.save(user);
            logger.info("New pin successfully updated. Response:"+HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"New pin successfully updated\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PutMapping("/resetPin")
    public ResponseEntity resetPin( @RequestBody UserPinForm form){
        logger.info("PUT: /user/resetPin");
        if(form.getNewPin()==null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Not Found\"}");
        try{
            String key = environment.getProperty("secret_key");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            //compare pin
            if(!form.getNewPin().toString().equals(form.getConfirmPin().toString()))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Pin not match\"}");

            user.setPin_enable(true);
            user.setPin(cipherEncryption.encrypt(form.getNewPin().toString(), key));
            this.dataManager.save(user);

            logger.info("Response -> PUT: /user/resetPin :" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"New pin successfully updated\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/verifyPin")
    public ResponseEntity verifyPin(@RequestBody UserPinForm form){
        try{
            logger.info("POST: /user/verifyPin");
            String key = environment.getProperty("secret_key");
//            UserDetails userDetails = currentAuthentication.getUser();
//            String userName = userDetails.getUsername();
//            User user = userService.getUserbyUsername(userName);

            User user = null;
            if(form.getUserName() != null && !Objects.equals(form.getUserName(), ""))
                user = userService.getUserbyUsername(form.getUserName());
            else if (form.getEmail() != null && !Objects.equals(form.getEmail(), ""))
                user = userService.getUserbyEmail(form.getEmail());
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            String decryptPin="";
            try{
                decryptPin = cipherEncryption.decrypt(user.getPin(), key);
            }
            catch (Exception ex){
                logger.info("Failed to decrypt pin");
                logger.error(ex.getMessage());
            }
            if(!form.getUserPin().toString().equals(decryptPin))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Pin not match\"}");
            if(!user.getPin_enable())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Pin not enabled\"}");

            logger.info("Response -> POST: /user/verifyPin :" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Verified\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PutMapping("/disablePin")
    public ResponseEntity disablePin(){
        try{
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            user.setPin_enable(false);
            this.dataManager.save(user);

            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Pin successfully disabled\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }

    @GetMapping("/bank")
    public ResponseEntity userBankDetails(@RequestParam(required = false) UUID userID){
        try{
            logger.info("GET:/user/bank");
            String encryptionKey = environment.getProperty("secret_key");

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user;
            if (userID != null){
                user = userService.getUserbyID(userID);
                logger.info("userName(form body) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }

            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");


            List<UserBank> userBanks = this.dataManager.load(UserBank.class)
//                    .condition(PropertyCondition.equal("user",user)).optional();
                    .query("select e from UserBank e where e.user.id = :user")
                    .parameter("user", user.getId()).list();

            List<BankForm> banks = new ArrayList<>();

            userBanks.forEach(e -> {
                //decrypt account
                String accountNumber = "";
                try{
                    accountNumber = cipherEncryption.decrypt(e.getAccountNumber(),encryptionKey);
                }
                catch (Exception ex){
                    logger.error(ex.getMessage());
                    logger.info("Failed to decrypt account number");
                }
                BankForm bank = new BankForm();
                bank.setId(e.getId());
                bank.setBankCode(e.getBank().getCode());
                bank.setBicCode(e.getBank().getBicCode());
                bank.setBankName(e.getBank().getName());
                bank.setAccountNumber(accountNumber);
                banks.add(bank);
            });


            return ResponseEntity.status(HttpStatus.OK).body(banks);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/bank")
    public  ResponseEntity insertBankDetails(@RequestBody BankForm form){
        try{
            logger.info("api POST:/user/bank");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);
            User user;
            String encryptionKey = environment.getProperty("secret_key");
            String accountNumber = cipherEncryption.encrypt(form.getAccountNumber(),encryptionKey);

            if (form.getUserID() != null){
                user = userService.getUserbyID(form.getUserID());
                logger.info("userName(form body) :->"+user.getUsername());
            }
            else {
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{\"message\":\"User not found\"}");

            String queryBank = "select e from Bank e where e.bicCode = :bicCode";
            String queryBank2 = "select e from Bank e where e.code = :code";
            Optional<Bank> bank;
            if (form.getBicCode() != null && !Objects.equals(form.getBicCode(), "")){
                bank = this.dataManager.load(Bank.class)
                        .query(queryBank)
                        .parameter("bicCode",form.getBicCode()).optional();
            }
            else{
                bank = this.dataManager.load(Bank.class)
                        .query(queryBank2)
                        .parameter("code",form.getBankCode()).optional();
            }
            if (bank.isEmpty())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid bank\"}");

            //Load all previous bank
            String query3="select e from UserBank e where e.user.id = :user";
            List<UserBank> userBanks = this.dataManager.load(UserBank.class)
//                    .condition(PropertyCondition.equal("user",user)).optional();
                    .query(query3)
                    .parameter("user", user.getId()).list();

            //DELETE ALL PREVIOUS BANK ACCOUNT for MVP2
            userBanks.forEach(userBank -> {
                dataManager.remove(userBank);
            });

            // COMMENT BECAUSE NOW FOR MVP2 WHEN USER CALL POST BANK TO UPDATE OTHER PREVIOUS BANK ACCOUNTS WILL BE DELETED
            //check for existing userBank
//            String queryUserbank = "select e from UserBank e where e.user.id = :user and e.bank.id = :bank";
//            Optional<UserBank> userBanks = this.dataManager.load(UserBank.class)
////                    .condition(PropertyCondition.equal("user",user)).optional();
//                    .query(queryUserbank)
//                    .parameter("user", user.getId())
//                    .parameter("bank", bank.get().getId()).optional();
//            if (userBanks.isPresent()){
//                logger.info("Account already exists");
//                return ResponseEntity.status(HttpStatus.CONFLICT).body("Account already exists");
//            }
            //Create new Userbank
            UserBank userBank = this.dataManager.create(UserBank.class);
            userBank.setUser(user);
            userBank.setBank(bank.get());
            userBank.setAccountNumber(accountNumber);
            this.dataManager.save(userBank);

            return ResponseEntity.status(HttpStatus.CREATED).body(userBank);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }

    @DeleteMapping("/bank")
    public  ResponseEntity deleteUserDetails(@RequestBody BankForm form){
        try{
            logger.info("DELETE:/user/bank");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            Optional<Bank> bank = this.dataManager.load(Bank.class)
                    .query("select e from Bank e where e.bicCode = :bicCode")
                    .parameter("bicCode",form.getBicCode()).optional();
            if (bank.isEmpty())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid bank");

            //check for existing userBank
            UserBank userBanks = this.dataManager.load(UserBank.class)
//                    .condition(PropertyCondition.equal("user",user)).optional();
                    .query("select e from UserBank e where e.user.id = :user and e.bank.id = :bank")
                    .parameter("user", user.getId())
                    .parameter("bank", bank.get().getId()).one();

            this.dataManager.remove(userBanks);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("User bank deleted");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }

    @PostMapping("/address")
    public  ResponseEntity insertUserAddress(@RequestBody List<AddressForm> forms){
        try{
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            SaveContext saveContext = new SaveContext();
            ServiceResponse insertAddresses = userService.updateUserAddress(forms,user);
            if(!insertAddresses.getSuccessCode().equals(SuccessCode.UPDATED)){
                logger.error(insertAddresses.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+insertAddresses.getMessage()+"\"}");
            }
            // response id
            return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\":\"Address successfully created\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PutMapping("/address")
    public  ResponseEntity updateUserAddress(@RequestBody List<AddressForm> forms){
        try{
            logger.info("PUT: /user/address");
            Boolean inputValidation = true;
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if (user == null){
                logger.info("user " +userName + " not exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            }

            SaveContext saveContext = new SaveContext();
            for (AddressForm form : forms) {
                //check for existing userAddress
                Optional<UserAddress> checkUserAddress = this.dataManager.load(UserAddress.class).id(form.getId()).optional();
                if (checkUserAddress.isEmpty()) {
                    logger.info("Address id " + form.getId().toString() + " not exist");
                    return ResponseEntity.status(HttpStatus.CONFLICT).body("{\"message\":\"Address not exists\"}");
                }
                UserAddress userAddress = checkUserAddress.get();
                if (form.getLabel() != null && !Objects.equals(form.getLabel(), ""))
                    userAddress.setLabel(form.getLabel());
                if (form.getAddress1() != null && !Objects.equals(form.getAddress1(), "")) {
                    userAddress.setRecipientName(form.getRecipientName());
                    userAddress.setContactNumber(form.getContactNumber());
                    userAddress.setAddress1(form.getAddress1());
                    userAddress.setAddress2(form.getAddress2());
                    userAddress.setPostcode(form.getPostcode());
                    userAddress.setTown(form.getCity());

                    // check state from enum list
                    if (form.getStateCode() != null && !Objects.equals(form.getStateCode(), "")) {
                        if (Arrays.stream(StateList.values()).noneMatch(e -> e.name().equals(form.getStateCode()))) {
                            logger.info(form.getStateCode() + " |Invalid state code");
                            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid state\"}");
                        }
                        userAddress.setState(StateList.valueOf(form.getStateCode()));
                    }
                    // check country from enum list
                    if (form.getCountryCode() != null && !Objects.equals(form.getCountryCode(), "")) {
                        if (Arrays.stream(CountryList.values()).noneMatch(e -> e.name().equals(form.getCountryCode()))) {
                            logger.info(form.getCountryCode() + " |Invalid country code");
                            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid country\"}");
                        }
                        userAddress.setCountry(CountryList.valueOf(form.getCountryCode()));
                    }
                }

                //check address type
                if (form.getAddressTypeCode() != null && !Objects.equals(form.getAddressTypeCode(), "")) {
                    // check is valid
                    Optional<AddressType> addressType = this.dataManager.load(AddressType.class)
                            .query("select e from AddressType e where e.code = :code")
                            .parameter("code", form.getAddressTypeCode()).optional();
                    if (addressType.isEmpty()) {
                        logger.info(form.getAddressTypeCode() + " |Invalid address type code");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid address type\"}");
                    }

                    //check if other address contain same address type
                    if (!Objects.equals(form.getAddressTypeCode(), userAddress.getAddType().getCode())) {
                        Optional<UserAddress> checkAddressType = this.dataManager.load(UserAddress.class)
                                .query("select e from UserAddress e where e.user.id = :user and e.addType.id = :addType")
                                .parameter("user", user.getId())
                                .parameter("addType", addressType.get().getId()).optional();
                        if (checkAddressType.isPresent()) {
                            logger.info("Same address type already exists");
                            return ResponseEntity.status(HttpStatus.CONFLICT).body("{\"message\":\"Same address type already exists\"}");
                        }

                    }
                    userAddress.setAddType(addressType.get());
                }
                saveContext.saving(userAddress);
//                this.dataManager.save(userAddress);
            }
            this.dataManager.save(saveContext);
//            return ResponseEntity.status(HttpStatus.OK).body(saveContext.getEntitiesToSave());
            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"User address successfully updated\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("/address")
    public  ResponseEntity getUserAddress(@RequestParam(required = false) String addressTypeCode){
        try{
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            List<UserAddress> userAddress;
            //check address type
            if(addressTypeCode!=null){

                Optional<AddressType> addressType = this.dataManager.load(AddressType.class)
                        .query("select e from AddressType e where e.code = :code")
                        .parameter("code",addressTypeCode).optional();
                if (addressType.isEmpty())
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid address type\"}");

                userAddress = this.dataManager.load(UserAddress.class)
                        .query("select e from UserAddress e where e.user.id = :user and e.addType.id = :addType")
                        .parameter("user", user.getId())
                        .parameter("addType", addressType.get().getId()).list();
            }
            else{
                userAddress = this.dataManager.load(UserAddress.class)
                        .query("select e from UserAddress e where e.user.id = :user")
                        .parameter("user", user.getId()).list();
            }

            List<AddressObj> addresses = new ArrayList<>();

            userAddress.forEach(e -> {

                AddressObj address = new AddressObj();
                address.setId(e.getId());
                address.setLabel(e.getLabel());
                address.setRecipientName(e.getRecipientName());
                address.setContactNumber(e.getContactNumber());
                address.setAddress1(e.getAddress1());
                address.setAddress2(e.getAddress2());
                address.setPostcode(e.getPostcode());
                address.setCity(e.getTown());

                if (e.getState() != null){
                    CodeForm state = new CodeForm();
                    state.setCode(e.getState().name());
                    state.setName(e.getState().getId());
                    address.setState(state);
                }
                if (e.getCountry() != null){
                    CodeForm country = new CodeForm();
                    country.setCode(e.getCountry().name());
                    country.setName(e.getCountry().getId());
                    address.setCountry(country);
                }
                if (e.getAddType() != null){
                    TypeForm addressType = new TypeForm();
                    addressType.setCode(e.getAddType().getCode());
                    addressType.setName(e.getAddType().getName());
                    addressType.setDescription(e.getAddType().getDescription());
                    address.setAddressType(addressType);
                }
                addresses.add(address);
            });

            return ResponseEntity.status(HttpStatus.OK).body(addresses);
        }
        catch (Exception ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/profilePicture")
    public  ResponseEntity profilePicture(@RequestParam @NotNull MultipartFile image){

//        Regions clientRegion = Regions.AP_SOUTHEAST_1;
//        Regions clientRegion = Regions.fromName(environment.getProperty("AWS_REGION"));
//        String accessKey = environment.getProperty("AWS_ACCESS_KEY");
//        String secretKey = environment.getProperty("AWS_SECRET_KEY");
        String bucketName = environment.getProperty("AWS_BUCKET_PROFILE");
//        String keyName = image.getOriginalFilename();

        try{
            logger.info("POST: /user/profilePicture");
            if (s3Client == null){
                setS3Client();
            }
            // check user
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("username: ->" +userName);

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{\"message\":\"User not found\"}");
//            String filename = FilenameUtils.getName(image.getOriginalFilename());
            String Name = user.getFull_name();
            if(Name.contains(" "))
                Name = Name.substring(0, Name.indexOf(" "));
//            logger.info("filename:->"+filename);
//            String keyName = user.getId() +"/"+ image.getOriginalFilename();
            String keyName = user.getId() +"/"+ Name+".jpeg";

//            var response = AmazonS3Service.putObjectToS3(bucketName, keyName, image);

            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("plain/text");
//            metadata.addUserMetadata("title", "someTitle");
            PutObjectRequest request = new PutObjectRequest(bucketName, keyName, image.getInputStream(),metadata);
            request.setMetadata(metadata);
            var response = s3Client.putObject(request);

            // Get the S3 bucket location URL
            String bucketLocation = s3Client.getBucketLocation(bucketName);
            String objectUrl = String.format("https://%s.s3.%s.amazonaws.com/%s", bucketName, bucketLocation, keyName);
            String s3Uri = String.format("s3://%s/%s", bucketName, keyName);
            String objectLocation = bucketName+"/"+keyName;

            // check existing profile picture
            List<UserProfilePicture> userProfilePicture = this.dataManager.load(UserProfilePicture.class)
                    .query("select e from UserProfilePicture e where e.user.id = :user")
                    .parameter("user", user.getId()).list();

            if(userProfilePicture.size() != 0){
                this.dataManager.remove(userProfilePicture);
            }
            UserProfilePicture profile = this.dataManager.create(UserProfilePicture.class);
            profile.setUser(user);
            profile.setLocation(objectLocation);
            this.dataManager.save(profile);

            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("bucketName",bucketName);
            jsonResponse.put("keyName",keyName);

            logger.info("bucket:-> " + bucketName);
            logger.info("keyName:-> "+ keyName);
            logger.info("Response -> POST: /user/profilePicture :" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body(jsonResponse.toString());
        }
        catch (AmazonServiceException ex){
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            logger.error(ex.getMessage());
            return ResponseEntity.status(ex.getStatusCode()).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
//        catch (SdkClientException ex){
//            // Amazon S3 couldn't be contacted for a response, or the client
//            // couldn't parse the response from Amazon S3.
//            logger.error(ex.getMessage());
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
//        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/employmentDetails")
    public  ResponseEntity insertEmploymentDetails(@RequestBody EmploymentDetailsForm form){
        try{
            logger.info("POST:/user/employmentDetails");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            //check type of employment status
            EmploymentStatus employmentStatus = employmentStatuses.stream().filter(
                    f -> f.getCode().equals(form.getEmploymentStatusCode())).collect(Collectors.toList()).get(0);
            if (employmentStatus == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid employmentStatusCode\"}");

            //check type of position
            JobPosition position = null;
            if(form.getPositionCode()!=null && !form.getPositionCode().equals("")){
                List<JobPosition> positions = jobPositions.stream().filter(
                        f -> f.getCode().equals(form.getPositionCode())).collect(Collectors.toList());
                if (positions.size()==0)
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid positionCode\"}");
                else
                    position = positions.get(0);
            }
            //check type nature of business
            NatureOfBusiness natureOfBusiness = null;
            if(form.getNatureOfBusinessCode()!=null && !form.getNatureOfBusinessCode().equals("")){
                List<NatureOfBusiness> natureOfBusinessList = natureOfBusinesses.stream().filter(
                        f -> f.getCode().equals(form.getNatureOfBusinessCode())).collect(Collectors.toList());
                if (natureOfBusinessList.size()==0)
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid natureOfBusinessCode\"}");
                else
                    natureOfBusiness = natureOfBusinessList.get(0);
            }

            //check type of country of tax residence
            if(Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(form.getCountryOfTaxResidence()))){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid countryOfTaxResidence\"}");
            }

            UserEmployment employment = this.dataManager.create(UserEmployment.class);
            employment.setUser(user);
            employment.setEmploymentStatus(employmentStatus);
            employment.setEmployerName(form.getEmployerName());
            employment.setBusinessName(form.getBusinessName());
            employment.setPosition(position);
            employment.setNatureOfBusiness(natureOfBusiness);
            employment.setPreviousEmployerName(form.getPreviousEmployerName());
            employment.setCountryOftaxResidence(CountryList.valueOf(form.getCountryOfTaxResidence()));

            this.dataManager.save(employment);

            // response id
            logger.info("Response -> POST:/user/employmentDetails :" + HttpStatus.CREATED);
            return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\":\"Successfully added\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @GetMapping("/employmentDetails")
    public ResponseEntity getEmploymentDetails(@RequestParam(required = false) UUID userID){
        try{
            logger.info("GET:/user/employmentDetails");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user;
            if (userID != null){
                user = userService.getUserbyID(userID);
                logger.info("userName(form parameter) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            String queryEmployment = "select e from UserEmployment e where e.user.id = :user " +
                    "order by COALESCE(e.createdDate, e.lastModifiedDate) desc";
            List<UserEmployment> userEmployments = this.dataManager.load(UserEmployment.class)
                    .query(queryEmployment)
                    .parameter("user", user.getId()).list();
            JSONObject employmentJson = new JSONObject();
            // later add sort by created date, then take latest in case termasuk more than one data
            if(userEmployments.size()!=0){
                UserEmployment userEmployment = userEmployments.get(0);

                employmentJson.put("userID", userEmployment.getUser().getId());

                JSONObject employmentStatus = new JSONObject();
                employmentStatus.put("code",userEmployment.getEmploymentStatus().getCode());
                employmentStatus.put("name",userEmployment.getEmploymentStatus().getName());
                employmentJson.put("employmentStatus",employmentStatus);
                employmentJson.put("employerName",userEmployment.getEmployerName());
                employmentJson.put("businessName",userEmployment.getBusinessName());
                employmentJson.put("previousEmployerName",userEmployment.getPreviousEmployerName());

                JSONObject position = new JSONObject();
                position.put("code",userEmployment.getPosition().getCode());
                position.put("name",userEmployment.getPosition().getName());
                employmentJson.put("position",position);

                JSONObject natureOfBusiness = new JSONObject();
                natureOfBusiness.put("code",userEmployment.getNatureOfBusiness().getCode());
                natureOfBusiness.put("name",userEmployment.getNatureOfBusiness().getName());
                employmentJson.put("natureOfBusiness",natureOfBusiness);

                JSONObject country = new JSONObject();
                country.put("code",userEmployment.getCountryOftaxResidence().name());
                country.put("name",userEmployment.getCountryOftaxResidence().getId());
                employmentJson.put("countryOfTaxResidence",country);
            }
            return ResponseEntity.status(HttpStatus.OK).body(employmentJson.toString());
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("/fatcaCrsDeclaration")
    public ResponseEntity insertFatcaCrsDeclaration(@RequestBody FactaCrsDeclarationForm form){
        try {
            logger.info("POST:/fatcaCrsDeclaration");

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            if(form == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"The request body cannot be null\"}");

            if (form.getNotUSCitizen() != null){
                logger.info("not U.S. Citizen: " + form.getNotUSCitizen());
                DeclarationType type_01 = declarationTypes.stream().filter(f -> f.getCode().equals("DCLRN01")).collect(Collectors.toList()).get(0);
                FatcaCrsDeclaration declaration_01 = this.dataManager.create(FatcaCrsDeclaration.class);
                declaration_01.setUser(user);
                declaration_01.setDeclaration(type_01);
                declaration_01.setAgreed(form.getNotUSCitizen());
                this.dataManager.save(declaration_01);
            }
            if (form.getNotUSGreenCardHolder() != null){
                logger.info("not U.S. green card holder: " + form.getNotUSGreenCardHolder());
                DeclarationType type_02 = declarationTypes.stream().filter(f -> f.getCode().equals("DCLRN02")).collect(Collectors.toList()).get(0);
                FatcaCrsDeclaration declaration_02 = this.dataManager.create(FatcaCrsDeclaration.class);
                declaration_02.setUser(user);
                declaration_02.setDeclaration(type_02);
                declaration_02.setAgreed(form.getNotUSGreenCardHolder());
                this.dataManager.save(declaration_02);
            }
            if (form.getNotUSBorn() != null){
                logger.info("not born in the U.S.: " + form.getNotUSBorn());
                DeclarationType type_03 = declarationTypes.stream().filter(f -> f.getCode().equals("DCLRN03")).collect(Collectors.toList()).get(0);
                FatcaCrsDeclaration declaration_03 = this.dataManager.create(FatcaCrsDeclaration.class);
                declaration_03.setUser(user);
                declaration_03.setDeclaration(type_03);
                declaration_03.setAgreed(form.getNotUSBorn());
                this.dataManager.save(declaration_03);
            }
            if (form.getMalaysiaTaxResidence() != null){
                logger.info("not a tax residence of countries other than Malaysia: " + form.getMalaysiaTaxResidence());
                DeclarationType type_04 = declarationTypes.stream().filter(f -> f.getCode().equals("DCLRN04")).collect(Collectors.toList()).get(0);
                FatcaCrsDeclaration declaration_04 = this.dataManager.create(FatcaCrsDeclaration.class);
                declaration_04.setUser(user);
                declaration_04.setDeclaration(type_04);
                declaration_04.setAgreed(form.getMalaysiaTaxResidence());
                this.dataManager.save(declaration_04);
            }
            logger.info("Response -> POST:/fatcaCrsDeclaration :" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Successfully added\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PutMapping("/personalDetails")
    public ResponseEntity updatePersonalDetails(@RequestBody PersonalDetailsForm form){
        try{
            logger.info("PUT: /user/personalDetails");
            String secretKey = environment.getProperty("secret_key");

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if(form == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Request body cannot be null"));
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_ACCOUNT,"User not exist"));

            // verifying details
            if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(form.getNationalityCode())))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid nationality\"}");
            CountryList nationality = CountryList.valueOf(form.getNationalityCode());
            CountryList issuanceCountry = null;
            if(nationality==CountryList.US || nationality==CountryList.KP || nationality==CountryList.IR ||
                    nationality==CountryList.MM || nationality==CountryList.IL){
                logger.info("Nationality "+nationality.getId()+" not allowed");
                ErrorResponse response = setErrResponse(ErrorCode.COUNTRY_NOT_ALLOWED, "Nationality not allowed");
                return ResponseEntity.badRequest().body(response);
            }
            if(form.getIssuanceCountry()!=null && !Objects.equals(form.getIssuanceCountry(), "")){
                if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(form.getIssuanceCountry()))){
                    ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid issuance country");
                    return ResponseEntity.badRequest().body(response);
                }
            }
            if(form.getIdentificationTypeCode() == null || Objects.equals(form.getIdentificationTypeCode(), "")
                    || form.getIdentificationNumber() == null || Objects.equals(form.getIdentificationNumber(), "")) {
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid identification number");
                return ResponseEntity.badRequest().body(response);
            }
            List<IdentificationType> idTypes = identificationTypes.stream().filter(f -> f.getCode().equals(form.getIdentificationTypeCode())).collect(Collectors.toList());
            if(idTypes.isEmpty())
                return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type"));
            IdentificationType idType = idTypes.get(0);
            //only allow Mykad and Mytentera for Malaysian
            if(nationality == CountryList.MY){
                switch (idType.getCode()){
                    case "P01":
                        break;
                    default:
                        return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type"));
                }
            } else if (idType.getCode().equals("P03")){
                if(form.getIssuanceCountry() == null || form.getIssuanceCountry().equals("") || form.getExpiryDate() == null){
                    logger.info("Passport required issuance country and expiry date");
                    ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid identification type");
                    return ResponseEntity.badRequest().body(response);
                }
                issuanceCountry = CountryList.valueOf(form.getIssuanceCountry());
            }
            //checking ic number
            Boolean updateIdNumber = Boolean.FALSE;
            String decryptIdNumber = cipherEncryption.decrypt(user.getIdentificationNumber(),secretKey);
            if(!trimIdNumber(decryptIdNumber).equalsIgnoreCase(trimIdNumber(form.getIdentificationNumber()))){
                //if difference id number
                updateIdNumber = Boolean.TRUE;
                ValidationStatus validateId = validation.validateIdentification(null,form.getIdentificationNumber(),idType);
                if (validateId == ValidationStatus.IC_EXIST){
                    logger.info("Identification number already exist");
                    ErrorResponse response = setErrResponse(ErrorCode.IDENTIFICATION_NUMBER_ALREADY_EXISTS,
                            "Identification number "+user.getIdentificationNumber()+" already exist");
                    return ResponseEntity.badRequest().body(response);
                }
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = dateFormat.format(form.getDob());
            //update details
            if(!user.getFull_name().equals(form.getFullName()))
                user.setFull_name(form.getFullName());
            if(user.getNationality()==null  || !user.getNationality().equals(nationality))
                user.setNationality(nationality);
            if(!user.getIdentificationType().equals(idType))
                user.setIdentificationType(idType);
            if(updateIdNumber.equals(Boolean.TRUE))
                user.setIdentificationNumber(cipherEncryption.encrypt(form.getIdentificationNumber(), secretKey));
            //for passport
            if(form.getIdentificationTypeCode().equals("P03")){
                if(user.getIdentificationIssuanceCountry()==null || !user.getIdentificationIssuanceCountry().equals(issuanceCountry))
                    user.setIdentificationIssuanceCountry(CountryList.valueOf(form.getIssuanceCountry()));
                if(user.getIdentificationExpiryDate()==null || !user.getIdentificationExpiryDate().equals(form.getExpiryDate()))
                    user.setIdentificationExpiryDate(form.getExpiryDate());
            }
            user.setBirthday(cipherEncryption.encrypt(strDate, secretKey));
            this.dataManager.save(user);
            /// smpai sini

            logger.info("Response -> PUT: /user/personalDetails :" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Details successfully updated\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @GetMapping("/personalDetails")
    public ResponseEntity getPersonalDetails(@RequestParam(required = false) UUID userID){
        try{
            logger.info("GET:/user/personalDetails");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);
            String key = environment.getProperty("secret_key");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String idNumber = null;
            String birthday = null;
            User user;
            if (userID != null){
                user = userService.getUserbyID(userID);
                logger.info("userName(form parameter) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            if(user.getIdentificationNumber()!= null && !Objects.equals(user.getIdentificationNumber(), ""))
                idNumber = cipherEncryption.decrypt(user.getIdentificationNumber(), key);
            if(user.getBirthday() != null && !Objects.equals(user.getBirthday(),""))
                birthday = (cipherEncryption.decrypt(user.getBirthday(), key));
            JSONObject details = new JSONObject();

            details.put("userID", user.getId());
            details.put("fullName",user.getFull_name());
            if(idNumber == null)
                details.put("identificationNumber",JSONObject.NULL);
            else
                details.put("identificationNumber",idNumber);
            if(birthday==null)
                details.put("dob",JSONObject.NULL);
            else
                details.put("dob",birthday);

            JSONObject identificationType = new JSONObject();
            identificationType.put("code",user.getIdentificationType().getCode());
            identificationType.put("name",user.getIdentificationType().getName());
            details.put("identificationType",identificationType);

            if(user.getNationality() != null){
                JSONObject nationality = new JSONObject();
                nationality.put("code",user.getNationality().name());
                nationality.put("name",user.getNationality().getId());
                details.put("nationality",nationality);
            }
            if(user.getIdentificationIssuanceCountry() != null){
                JSONObject issuanceCountry = new JSONObject();
                issuanceCountry.put("code",user.getIdentificationIssuanceCountry().name());
                issuanceCountry.put("name",user.getIdentificationIssuanceCountry().getId());
                details.put("issuanceCountry",issuanceCountry);
            }
            else
                details.put("issuanceCountry",JSONObject.NULL);
            if(user.getIdentificationExpiryDate() == null)
                details.put("expiryDate",JSONObject.NULL);
            else
                details.put("expiryDate",dateFormat.format(user.getIdentificationExpiryDate()));

            return ResponseEntity.status(HttpStatus.OK).body(details.toString());
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PutMapping("/contactDetails")
    public ResponseEntity updateContactDetails(@RequestBody ContactDetailsForm form){
        try{
            logger.info("PUT: /user/contactDetails");
            String secretKey = environment.getProperty("secret_key");

            // json response
            JSONObject invalidPhoneRes = new JSONObject();
            invalidPhoneRes.put("displayMessage","Please enter a valid phone number");
            invalidPhoneRes.put("message","Invalid phone number format");
            JSONObject invalidCountryRes = new JSONObject();
            invalidCountryRes.put("displayMessage","Please enter a valid country code");
            invalidCountryRes.put("message","Country code not allowed. Account suspended");

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if(form == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Request body cannot be null"));
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_ACCOUNT,"User not exist"));

            if(form.getPhoneNumber()!=null || !Objects.equals(form.getPhoneNumber(), "")){
                // compare new phone number with current
                if (!form.getPhoneNumber().equals(cipherEncryption.decrypt(user.getPhoneNumber(), secretKey))){
                    String queryStrOTP ="select e from Otp e where e.createdBy = :createdBy " +
                            "and e.phoneNumber = :phoneNumber and e.serviceType = :serviceType order by e.createdDate desc";

                    ActivationStatus approvedStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G06"))
                            .collect(Collectors.toList()).get(0);

                    Optional<Otp> otp = this.dataManager.load(Otp.class)
                            .query(queryStrOTP)
                            .parameter("createdBy",user.getUsername().toLowerCase())
                            .parameter("phoneNumber",form.getPhoneNumber())
                            .parameter("serviceType",OtpServiceType.CHANGE_PHONE_NUMBER)
                            .optional();

                    if(otp.isEmpty()){
                        logger.info("No generated OTP found!!!");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid phone number\"}");
                    }
                    else if(!otp.get().getStatus().equals(approvedStatus)){
                        logger.info("Phone number not verified!!!");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid phone number\"}");
                    }
                }
                // checking phone number format
                JSONObject validatePhoneNumber = validation.validatePhoneNumber(form.getPhoneNumber());
                if (validatePhoneNumber.get("validate").equals(false))
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidPhoneRes.toString());

                Boolean code = checkCountryCode(form.getPhoneNumber());
                if (!code)
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid phone number format\"}");
                user.setPhoneNumber(cipherEncryption.encrypt(form.getPhoneNumber(),secretKey));
                user.setHomeNumber(cipherEncryption.encrypt(form.getHomeNumber(),secretKey));
                user.setOfficeNumber(cipherEncryption.encrypt(form.getOfficeNumber(),secretKey));

                // suspend account if phone number other than malaysia number
                if (!validatePhoneNumber.get("countryCode").equals("60")){
                    logger.info(form.getPhoneNumber()," | Account suspend. Phone number country code other than Malaysia");
                    Optional<ActivationStatus> actStatus = this.dataManager.load(ActivationStatus.class)
                            .condition(PropertyCondition.equal("code","G20")).optional();
                    actStatus.ifPresent(user::setActStatus);
                    this.dataManager.save(user);
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidCountryRes.toString());
                }
                logger.info("Phone number updated");
                this.dataManager.save(user);
            }
            ServiceResponse insertAddresses = userService.updateUserAddress(form.getAddress(),user);
            if(!insertAddresses.getSuccessCode().equals(SuccessCode.UPDATED)){
                logger.error(insertAddresses.getMessage());
            }
            return ResponseEntity.status(HttpStatus.OK).body(user);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
        }
    }
    @GetMapping("/contactDetails")
    public ResponseEntity getContactDetails(@RequestParam(required = false) UUID userID){
        try{
            logger.info("GET:/user/contactDetails");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);
            String key = environment.getProperty("secret_key");

            String phoneNumber =null;
            String homeNumber =null;
            String officeNumber =null;

            User user;
            if (userID != null){
                user = userService.getUserbyID(userID);
                logger.info("userName(form parameter) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            if(user.getPhoneNumber()!= null && !Objects.equals(user.getPhoneNumber(), ""))
                phoneNumber = cipherEncryption.decrypt(user.getPhoneNumber(),key);
            if(user.getHomeNumber()!= null && !Objects.equals(user.getHomeNumber(), ""))
                homeNumber = cipherEncryption.decrypt(user.getHomeNumber(),key);
            if(user.getOfficeNumber()!= null && !Objects.equals(user.getOfficeNumber(), ""))
                officeNumber = cipherEncryption.decrypt(user.getPhoneNumber(),key);

            List<UserAddress> userAddress = this.dataManager.load(UserAddress.class)
                    .query("select e from UserAddress e where e.user.id = :user")
                    .parameter("user", user.getId()).list();

            List<AddressObj> addresses = new ArrayList<>();
            userAddress.forEach(e -> {

                AddressObj address = new AddressObj();
                address.setId(e.getId());
                address.setLabel(e.getLabel());
                address.setRecipientName(e.getRecipientName());
                address.setContactNumber(e.getContactNumber());
                address.setAddress1(e.getAddress1());
                address.setAddress2(e.getAddress2());
                address.setPostcode(e.getPostcode());
                address.setCity(e.getTown());

                if (e.getState() != null){
                    CodeForm state = new CodeForm();
                    state.setCode(e.getState().name());
                    state.setName(e.getState().getId());
                    address.setState(state);
                }
                if (e.getCountry() != null){
                    CodeForm country = new CodeForm();
                    country.setCode(e.getCountry().name());
                    country.setName(e.getCountry().getId());
                    address.setCountry(country);
                }
                if (e.getAddType() != null){
                    TypeForm addressType = new TypeForm();
                    addressType.setCode(e.getAddType().getCode());
                    addressType.setName(e.getAddType().getName());
                    addressType.setDescription(e.getAddType().getDescription());
                    address.setAddressType(addressType);
                }
                addresses.add(address);
            });
            JSONObject response = new JSONObject();
            response.put("email",user.getEmail());
            response.put("phoneNumber",phoneNumber);
            response.put("homeNumber",homeNumber);
            response.put("officeNumber",officeNumber);
            response.put("address",addresses);

            return ResponseEntity.status(HttpStatus.OK).body(response.toString());
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("/financialDetails")
    public  ResponseEntity insertFinancialDetails(@RequestBody FinancialDetailsForm form){
        try{
            logger.info("POST:/user/financialDetails");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            //check type Code
            TransactionPurposeType transactionPurposeType = transactionPurposeTypes.stream().filter(
                    f -> f.getCode().equals(form.getTransactionPurposeCode())).collect(Collectors.toList()).get(0);
            TotalMonthlyTransaction totalMonthlyTransaction = totalMonthlyTransactions.stream().filter(
                    f -> f.getCode().equals(form.getTotalMonthlyTransactionCode())).collect(Collectors.toList()).get(0);
            FreqMonthlyTransaction freqMonthlyTransaction = freqMonthlyTransactions.stream().filter(
                    f -> f.getCode().equals(form.getFreqMonthlyTransactionCode())).collect(Collectors.toList()).get(0);
            SourceOfFundType sourceOfFundType = sourceOfFundTypes.stream().filter(
                    f -> f.getCode().equals(form.getSourceOfFundCode())).collect(Collectors.toList()).get(0);
            SourceOfWealthType sourceOfWealthType = sourceOfWealthTypes.stream().filter(
                    f -> f.getCode().equals(form.getSourceOfWealthCode())).collect(Collectors.toList()).get(0);

            //************* Save details *************************//
            StatutoryDetails details = this.dataManager.create(StatutoryDetails.class);
            details.setUser(user);
            details.setTransactionPurpose(transactionPurposeType);
            details.setTotalMonthlyTransaction(totalMonthlyTransaction);
            details.setFreqMonthlyTransaction(freqMonthlyTransaction);
            this.dataManager.save(details);

            FinancialSources sources = this.dataManager.create(FinancialSources.class);
            sources.setUser(user);
            sources.setSourceOfFund(sourceOfFundType);
            sources.setOtherSourceOfFund(form.getOtherSourceOfFund());
            sources.setSourceOfWealth(sourceOfWealthType);
            sources.setOtherSourceOfWealth(form.getOtherSourceOfWealth());
            this.dataManager.save(sources);
            // response id
            logger.info("Response -> POST:/user/employmentDetails :" + HttpStatus.CREATED);
            return ResponseEntity.status(HttpStatus.CREATED).body("{\"message\":\"Successfully added\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @GetMapping("/financialDetails")
    public ResponseEntity getFinancialDetails(@RequestParam(required = false) UUID userID){
        try{
            logger.info("GET:/user/contactDetails");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user;
            if (userID != null){
                user = userService.getUserbyID(userID);
                logger.info("userName(form parameter) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            String queryStatutory = "select e from StatutoryDetails e where e.user.id = :userID " +
                    "order by COALESCE(e.createdDate, e.lastModifiedDate) desc";
            String queryFinSource = "select e from FinancialSources e where e.user.id = :userID " +
                    "order by COALESCE(e.createdDate, e.lastModifiedDate) desc";
            Optional<StatutoryDetails> statutoryDetailsOpt = this.dataManager.load(StatutoryDetails.class)
                    .query(queryStatutory).parameter("userID", user.getId()).optional();
            Optional<FinancialSources> financialSourcesOptional = this.dataManager.load(FinancialSources.class)
                    .query(queryFinSource).parameter("userID", user.getId()).optional();

            CodeForm transactionPurpose = null;
            CodeForm totalTrans = null;
            CodeForm freqTrans = null;
            CodeForm fund = null;
            CodeForm wealth = null;
            String otherSourceOfFund = null;
            String otherSourceOfWealth = null;

            if(statutoryDetailsOpt.isPresent()){
                StatutoryDetails details = statutoryDetailsOpt.get();

                transactionPurpose = new CodeForm();
                transactionPurpose.setCode(details.getTransactionPurpose().getCode());
                transactionPurpose.setName(details.getTransactionPurpose().getName());
                totalTrans = new CodeForm();
                totalTrans.setCode(details.getTotalMonthlyTransaction().getCode());
                totalTrans.setName(details.getTotalMonthlyTransaction().getName());
                freqTrans = new CodeForm();
                freqTrans.setCode(details.getFreqMonthlyTransaction().getCode());
                freqTrans.setName(details.getFreqMonthlyTransaction().getName());

            }
            if(financialSourcesOptional.isPresent()){
                FinancialSources source = financialSourcesOptional.get();
                fund = new CodeForm();
                fund.setCode(source.getSourceOfFund().getCode());
                fund.setName(source.getSourceOfFund().getName());
                otherSourceOfFund = source.getOtherSourceOfFund();
                wealth = new CodeForm();
                wealth.setCode(source.getSourceOfWealth().getCode());
                wealth.setName(source.getSourceOfWealth().getName());
                otherSourceOfWealth = source.getOtherSourceOfWealth();
            }
            FinancialDetailsResponse response = new FinancialDetailsResponse();
            response.setUserName(user.getUsername());
            response.setTransactionPurpose(transactionPurpose);
            response.setTotalMonthlyTransaction(totalTrans);
            response.setFreqMonthlyTransaction(freqTrans);
            response.setSourceOfFund(fund);
            response.setOtherSourceOfFund(otherSourceOfFund);
            response.setSourceOfWealth(wealth);
            response.setOtherSourceOfWealth(otherSourceOfWealth);

            return ResponseEntity.status(HttpStatus.OK).body(response);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PutMapping("/closeAccount")
    public  ResponseEntity deleteUserDetails(@RequestParam(required = false) UUID userID){
        try{
            logger.info("PUT:/user/closeAccount");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);
            LocalDateTime dateNow = LocalDateTime.now();
            ActivationStatus closeStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G23"))
                    .collect(Collectors.toList()).get(0);

            User user;
            if (userID != null){
                user = userService.getUserbyID(userID);
                logger.info("userName(form parameter) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            user.setActStatus(closeStatus);
            user.setCloseDate(Date.from(dateNow.atZone(ZoneId.systemDefault()).toInstant()));
            this.dataManager.save(user);

            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("User account deleted");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PutMapping("/profileDetails")
    public ResponseEntity updateProfileDetails(@RequestBody ProfileDetailsForm form){
        //for edit profile
        try{
            logger.info("PUT: /user/profileDetails");
            String secretKey = environment.getProperty("secret_key");
            Boolean isSuspend = false;
            Boolean redoEkyc = false;
            Boolean reVerified = false;
            if(form.getPeriodicReview()!= null && form.getPeriodicReview())
                reVerified = true;
            ActivationStatus suspendStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G21"))
                    .collect(Collectors.toList()).get(0);

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if(form == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Request body cannot be null"));
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(setErrResponse(ErrorCode.INVALID_ACCOUNT,"User not exist"));

            // verifying details
            if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(form.getPersonalDetails().getNationalityCode())))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid nationality\"}");
            CountryList nationality = CountryList.valueOf(form.getPersonalDetails().getNationalityCode());
            CountryList issuanceCountry = null;
            if(nationality==CountryList.US || nationality==CountryList.KP || nationality==CountryList.IR ||
                    nationality==CountryList.MM || nationality==CountryList.IL){
                logger.info("Nationality "+nationality.getId()+" not allowed");
                //suspend the account
                isSuspend = true;
//                ErrorResponse response = setErrResponse(ErrorCode.COUNTRY_NOT_ALLOWED, "Nationality not allowed");
            }
            if(form.getPersonalDetails().getIssuanceCountry()!=null && !Objects.equals(form.getPersonalDetails().getIssuanceCountry(), "")){
                if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(form.getPersonalDetails().getIssuanceCountry()))){
                    ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid issuance country");
                    return ResponseEntity.badRequest().body(response);
                }
            }
            if(form.getPersonalDetails().getIdentificationTypeCode() == null || Objects.equals(form.getPersonalDetails().getIdentificationTypeCode(), "")
                    || form.getPersonalDetails().getIdentificationNumber() == null || Objects.equals(form.getPersonalDetails().getIdentificationNumber(), "")) {
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid identification number");
                return ResponseEntity.badRequest().body(response);
            }
            List<IdentificationType> idTypes = identificationTypes.stream().filter(f -> f.getCode().equals(form.getPersonalDetails().getIdentificationTypeCode())).collect(Collectors.toList());
            if(idTypes.isEmpty())
                return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type"));
            IdentificationType idType = idTypes.get(0);

            //only allow Mykad for Malaysian
            if(nationality == CountryList.MY){
                switch (idType.getCode()){
                    case "P01":
                        break;
                    default:
                        return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type"));
                }
            } else if (idType.getCode().equals("P03")){
                if(form.getPersonalDetails().getIssuanceCountry() == null || form.getPersonalDetails().getIssuanceCountry().equals("") ||
                        form.getPersonalDetails().getExpiryDate() == null){
                    logger.info("Passport required issuance country and expiry date");
                    ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid identification type");
                    return ResponseEntity.badRequest().body(response);
                }
                issuanceCountry = CountryList.valueOf(form.getPersonalDetails().getIssuanceCountry());
            }
            //checking ic number
            Boolean updateIdNumber = Boolean.FALSE;
            String decryptIdNumber = cipherEncryption.decrypt(user.getIdentificationNumber(),secretKey);
            if(!trimIdNumber(decryptIdNumber).equalsIgnoreCase(trimIdNumber(form.getPersonalDetails().getIdentificationNumber()))){
                //if difference id number
                updateIdNumber = Boolean.TRUE;
                ValidationStatus validateId = validation.validateIdentification(null,form.getPersonalDetails().getIdentificationNumber(),idType);
                if (validateId == ValidationStatus.IC_EXIST){
                    logger.info("Identification number already exist");
                    ErrorResponse response = setErrResponse(ErrorCode.IDENTIFICATION_NUMBER_ALREADY_EXISTS,
                            "Identification number "+user.getIdentificationNumber()+" already exist");
                    return ResponseEntity.badRequest().body(response);
                }
            }
            //add checking OTP if difference phone number with registered

            //check type of employment status
            EmploymentStatus employmentStatus = employmentStatuses.stream().filter(
                    f -> f.getCode().equals(form.getEmploymentDetails().getEmploymentStatusCode())).collect(Collectors.toList()).get(0);
            if (employmentStatus == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid employmentStatusCode\"}");

            //check type of position
            JobPosition position = jobPositions.stream().filter(
                    f -> f.getCode().equals(form.getEmploymentDetails().getPositionCode())).collect(Collectors.toList()).get(0);
            if (position == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid positionCode\"}");

            //check type nature of business
            NatureOfBusiness natureOfBusiness = natureOfBusinesses.stream().filter(
                    f -> f.getCode().equals(form.getEmploymentDetails().getNatureOfBusinessCode())).collect(Collectors.toList()).get(0);
            if (natureOfBusiness == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid natureOfBusinessCode\"}");

            //check type Code
            TransactionPurposeType transactionPurposeType = transactionPurposeTypes.stream().filter(
                    f -> f.getCode().equals(form.getFinancialDetails().getTransactionPurposeCode())).collect(Collectors.toList()).get(0);
            TotalMonthlyTransaction totalMonthlyTransaction = totalMonthlyTransactions.stream().filter(
                    f -> f.getCode().equals(form.getFinancialDetails().getTotalMonthlyTransactionCode())).collect(Collectors.toList()).get(0);
            FreqMonthlyTransaction freqMonthlyTransaction = freqMonthlyTransactions.stream().filter(
                    f -> f.getCode().equals(form.getFinancialDetails().getFreqMonthlyTransactionCode())).collect(Collectors.toList()).get(0);
            SourceOfFundType sourceOfFundType = sourceOfFundTypes.stream().filter(
                    f -> f.getCode().equals(form.getFinancialDetails().getSourceOfFundCode())).collect(Collectors.toList()).get(0);
            SourceOfWealthType sourceOfWealthType = sourceOfWealthTypes.stream().filter(
                    f -> f.getCode().equals(form.getFinancialDetails().getSourceOfWealthCode())).collect(Collectors.toList()).get(0);
            logger.info("Data validation passsed");

            // check for account suspension
            CountryList countryOfTaxResidence = CountryList.valueOf(form.getEmploymentDetails().getCountryOfTaxResidence());
            if(countryOfTaxResidence != CountryList.MY){
                logger.info("Country of Tax Residence "+countryOfTaxResidence.getId()+" not allowed");
                isSuspend = true;
//                ErrorResponse response = setErrResponse(ErrorCode.COUNTRY_NOT_ALLOWED, "Country of Tax Residence not allowed");
            }
            // check for redo ekyc
            if(!user.getNationality().equals(nationality)
                    || !user.getIdentificationType().equals(idType) || updateIdNumber.equals(Boolean.TRUE)){
                logger.info("Edit profile : need to redo ekyc (changes in nationality or id details)");
                redoEkyc = true;
                reVerified = true;
            }

            //************* Save details *************************//
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = dateFormat.format(form.getPersonalDetails().getDob());
            //update personal details
            if(!user.getFull_name().equals(form.getPersonalDetails().getFullName()))
                user.setFull_name(form.getPersonalDetails().getFullName());
            if(user.getNationality()==null  || !user.getNationality().equals(nationality))
                user.setNationality(nationality);
            if(!user.getIdentificationType().equals(idType))
                user.setIdentificationType(idType);
            if(updateIdNumber.equals(Boolean.TRUE))
                user.setIdentificationNumber(cipherEncryption.encrypt(form.getPersonalDetails().getIdentificationNumber(), secretKey));
            //for passport
            if(form.getPersonalDetails().getIdentificationTypeCode().equals("P03")){
                if(user.getIdentificationIssuanceCountry()==null || !user.getIdentificationIssuanceCountry().equals(issuanceCountry))
                    user.setIdentificationIssuanceCountry(CountryList.valueOf(form.getPersonalDetails().getIssuanceCountry()));
                if(user.getIdentificationExpiryDate() == null || !user.getIdentificationExpiryDate().equals(form.getPersonalDetails().getExpiryDate()))
                    user.setIdentificationExpiryDate(form.getPersonalDetails().getExpiryDate());
            }
            if(isSuspend)
                user.setActStatus(suspendStatus);
            user.setBirthday(cipherEncryption.encrypt(strDate, secretKey));
            logger.info("Personal detail updated");
            //contact details
            // checking phone number format
            JSONObject validatePhoneNumber = validation.validatePhoneNumber(form.getContactDetails().getPhoneNumber());
            Boolean code = checkCountryCode(form.getContactDetails().getPhoneNumber());
            if (validatePhoneNumber.get("validate").equals(false) || !code){
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid phone number format");
                return ResponseEntity.badRequest().body(response);
            }
            user.setPhoneNumber(cipherEncryption.encrypt(form.getContactDetails().getPhoneNumber(),secretKey));
            user.setHomeNumber(cipherEncryption.encrypt(form.getContactDetails().getHomeNumber(),secretKey));
            user.setOfficeNumber(cipherEncryption.encrypt(form.getContactDetails().getOfficeNumber(),secretKey));
            logger.info("Phone number updated");
            this.dataManager.save(user);
            logger.info("Contact detail updated");

            //update address
            ServiceResponse insertAddresses = userService.updateUserAddress(form.getContactDetails().getAddress(),user);
            if(!insertAddresses.getSuccessCode().equals(SuccessCode.UPDATED)){
                logger.error(insertAddresses.getMessage());
            }
            //update employment details
            CountryList countryOftaxResidence = CountryList.valueOf(form.getEmploymentDetails().getCountryOfTaxResidence());
            List<UserEmployment> currentEmployment = this.dataManager.load(UserEmployment.class)
                    .query("select e from UserEmployment e where e.user.id = :userID " +
                            "order by e.createdDate desc")
                    .parameter("userID", user.getId()).list();
            if (currentEmployment.size()!=0){
                if(!currentEmployment.get(0).getNatureOfBusiness().equals(natureOfBusiness)){
                    logger.info("Changes in Nature of Business");
                    reVerified=true;
                }
                else if(!currentEmployment.get(0).getCountryOftaxResidence().equals(countryOftaxResidence)){
                    logger.info("Changes in Country of Tax Residence");
                    reVerified=true;
                }
                currentEmployment.forEach(e->{
                    this.dataManager.remove(e);
                });
            }
            UserEmployment employment = this.dataManager.create(UserEmployment.class);
            employment.setUser(user);
            employment.setEmploymentStatus(employmentStatus);
            employment.setEmployerName(form.getEmploymentDetails().getEmployerName());
            employment.setBusinessName(form.getEmploymentDetails().getBusinessName());
            employment.setPosition(position);
            employment.setNatureOfBusiness(natureOfBusiness);
            employment.setPreviousEmployerName(form.getEmploymentDetails().getPreviousEmployerName());
            employment.setCountryOftaxResidence(countryOftaxResidence);
            this.dataManager.save(employment);
            logger.info("Employment Details updated");

            //remove previous edit profile reference
            List<EditProfileRef> previousRef = this.dataManager.load(EditProfileRef.class)
                    .query("select e from EditProfileRef e where e.user.id = :userID")
                    .parameter("userID", user.getId()).list();
            if (previousRef.size()!=0){
                previousRef.forEach(e->{
                    this.dataManager.remove(e);
                });
            }
            //update edit profile refrence
            EditProfileRef editProfileRef = this.dataManager.create(EditProfileRef.class);
            editProfileRef.setUser(user);
            editProfileRef.setBypassEkyc(!redoEkyc);
            editProfileRef.setBypassVerification(!reVerified);
            this.dataManager.save(editProfileRef);

            List<StatutoryDetails> currentStatutory = this.dataManager.load(StatutoryDetails.class)
                    .query("select e from StatutoryDetails e where e.user.id = :userID")
                    .parameter("userID", user.getId()).list();
            if (currentStatutory.size()!=0){
                currentStatutory.forEach(e->{
                    this.dataManager.remove(e);
                });
            }
            StatutoryDetails details = this.dataManager.create(StatutoryDetails.class);
            details.setUser(user);
            details.setTransactionPurpose(transactionPurposeType);
            details.setTotalMonthlyTransaction(totalMonthlyTransaction);
            details.setFreqMonthlyTransaction(freqMonthlyTransaction);
            this.dataManager.save(details);
            logger.info("statutory details updated");

            List<FinancialSources> currentFinancials = this.dataManager.load(FinancialSources.class)
                    .query("select e from FinancialSources e where e.user.id = :userID")
                    .parameter("userID", user.getId()).list();
            if (currentFinancials.size()!=0){
                currentFinancials.forEach(e->{
                    this.dataManager.remove(e);
                });
            }
            FinancialSources sources = this.dataManager.create(FinancialSources.class);
            sources.setUser(user);
            sources.setSourceOfFund(sourceOfFundType);
            sources.setOtherSourceOfFund(form.getFinancialDetails().getOtherSourceOfFund());
            sources.setSourceOfWealth(sourceOfWealthType);
            sources.setOtherSourceOfWealth(form.getFinancialDetails().getOtherSourceOfWealth());
            this.dataManager.save(sources);
            logger.info("financial sources updated");

            //suspend account: -> if phone number other than malaysia number
            if (!validatePhoneNumber.get("countryCode").equals("60")){
                logger.info(form.getContactDetails().getPhoneNumber()," | Account suspend. Phone number country code other than Malaysia");
                Optional<ActivationStatus> actStatus = this.dataManager.load(ActivationStatus.class)
                        .condition(PropertyCondition.contains("code","G20")).optional();
                actStatus.ifPresent(user::setActStatus);
                this.dataManager.save(user);
                ErrorResponse response = setErrResponse(ErrorCode.COUNTRY_NOT_ALLOWED, "Country code not allowed. Account suspended");
                return ResponseEntity.badRequest().body(response);
            }
            if(isSuspend){
                ErrorResponse response = setErrResponse(ErrorCode.ACCOUNT_SUSPENDED, "Account Suspended");
                return ResponseEntity.status(HttpStatus.OK).body(response);
            }
            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("requiredReverified",reVerified);
            jsonResponse.put("message","Details successfully updated");
            logger.info("Success Response: "+HttpStatus.OK + jsonResponse);
            return ResponseEntity.status(HttpStatus.OK).body(jsonResponse.toString());
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            if(ex.getMessage()==null)
                logger.error(ex.toString());
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
}
