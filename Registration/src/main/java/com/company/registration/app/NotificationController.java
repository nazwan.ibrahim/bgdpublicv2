package com.company.registration.app;

import com.company.registration.entity.*;
import com.company.registration.form.GenerateOtpForm;
import com.company.registration.form.PushNotificationForm;
import com.company.registration.form.UpdateNotificationForm;
import com.company.registration.responseForm.GenericResponse;
import com.company.registration.services.UserService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.jmix.core.DataManager;
import io.jmix.core.FetchPlan;
import io.jmix.core.TimeSource;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.kafka.common.message.UpdateFeaturesResponseData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.*;

@RestController
@RequestMapping("notification")
public class NotificationController {
    static Logger logger = LogManager.getLogger(GeneralController.class.getName());

    @Autowired
    private DataManager dataManager;

    @Autowired
    private TimeSource timeSource;

    @Autowired
    private UserService userService;
    @Autowired
    private Environment env;
    @Autowired
    private CurrentAuthentication currentAuthentication;

    private NotificationCategory ANNOUNCEMENT_CATEGORY;

    private NotificationCategory ACTIVITIES_CATEGORY;

    private NotificationStatus READ_STATUS;

    private NotificationStatus UNREAD_STATUS;

    private NotificationStatus DELETED_STATUS;

    @ManagedOperation
    @Authenticated
    @EventListener
    public void onApplicationStarted(ApplicationStartedEvent event) {
        ACTIVITIES_CATEGORY= getNotificationCategory("NC01");
        ANNOUNCEMENT_CATEGORY= getNotificationCategory("NC02");


        READ_STATUS= getNotificationStatus("G17");
        UNREAD_STATUS= getNotificationStatus("G16");
        DELETED_STATUS= getNotificationStatus("G18");
    }



    public NotificationCategory getNotificationCategory(String code){
        String query1="select n from NotificationCategory n " +
                "where n.code = :code1";
        NotificationCategory notificationCategory = dataManager.load(NotificationCategory.class)
                .query(query1).parameter("code1", code).one();
        return notificationCategory;
    }

    public NotificationStatus getNotificationStatus(String code){
        String query1="select n from NotificationStatus n " +
                "where n.code = :code1";
        NotificationStatus notificationStatus = dataManager.load(NotificationStatus.class)
                .query(query1).parameter("code1", code).one();
        return notificationStatus;
    }

    public NotificationType getNotificationType(String code){
        String query1 = "select n from NotificationType n where n.code = :code1";
        NotificationType notificationType = dataManager.load(NotificationType.class)
                .query(query1).parameter("code1",code).one();
        return notificationType;
    }

    public GenericResponse errorResponseHandler(GenericResponse genericResponse,String errorMessage){
        //temp before while xda response error table
        switch (errorMessage){
            case "RCE000" :
                genericResponse.setResponseCode("RCE000");
                genericResponse.setHttpStatus(400);
                genericResponse.setDescription("TEST ERROR");
            case "RCE004" :
                genericResponse.setResponseCode("RCE004");
                genericResponse.setHttpStatus(400);
                genericResponse.setDescription("Data Not Found");
            case "RCE112" :
                genericResponse.setResponseCode("RCE112");
                genericResponse.setHttpStatus(400);
                genericResponse.setDescription("Permission is set to not allow");
            default:
                genericResponse.setResponseCode("RCE999");
                genericResponse.setHttpStatus(500);
                genericResponse.setDescription("Miscellaneous Error");
                genericResponse.setData(errorMessage);
        }
        return genericResponse;

    }

    //Function To create new notification
    public void createNotification(User user, PushNotificationForm body){

        //create new notification data
        Notification newNotification = dataManager.create(Notification.class);
        newNotification.setUserId(user);

        switch (body.getCategoryCode()) {
            case "NC01":
                newNotification.setCategoryId(ACTIVITIES_CATEGORY);
                break;
            case "NC02":
                newNotification.setCategoryId(ANNOUNCEMENT_CATEGORY);
                break;
            default:
                throw new RuntimeException("INVALID REQUEST");
        }

        newNotification.setStatusId(UNREAD_STATUS);
        newNotification.setSendDateTime(timeSource.currentTimestamp());

        //create new notificationdetail data
        NotificationDetails newNotificationDetails = dataManager.create(NotificationDetails.class);
        newNotificationDetails.setTitle(body.getTitle());
        newNotificationDetails.setBody(body.getBody());
        if (body.getDataReference() != null) {
            newNotificationDetails.setDataReference(body.getDataReference());
        } else {
            newNotificationDetails.setDataReference("");
        }
        if (body.getDataUrl() != null) {
            newNotificationDetails.setDataUrl(body.getDataUrl());
        } else {
            newNotificationDetails.setDataUrl("");
        }
        newNotificationDetails.setTypeId(getNotificationType(body.getTypeCode()));
        dataManager.save(newNotificationDetails);

        newNotification.setDetailId(newNotificationDetails);
        dataManager.save(newNotification);

    }

    @GetMapping("/getDeviceId")
    public ResponseEntity getDeviceId(){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500); // default

        try{
            ///get user
            User user = userService.getUserbyUsername(currentAuthentication.getUser().getUsername());

            UserPermission userPermission = userService.getUserperminssion(user.getUsername());
            if(userPermission!=null && userPermission.getAllowPushNotification()){
                if (user.getDeviceID()==null){
                    throw new RuntimeException("RCE004");
                }
            }
            else{
                throw new RuntimeException("RCE112"); // permission not allow
            }

            genericResponse.setData(user.getDeviceID());
            genericResponse.setResponseCode("RCS001");
            genericResponse.setDescription("OK");
            genericResponse.setHttpStatus(200);

        }
        catch(Exception e){
            genericResponse = errorResponseHandler(genericResponse,e.getMessage());
        }
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);
    }


    @PostMapping("/pushNotification") //authenticatedEndpoint
    public ResponseEntity pushNotification(@RequestBody PushNotificationForm body) {

        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500);
        try {
            //get userID
            User user = userService.getUserbyUsername(currentAuthentication.getUser().getUsername());

            createNotification(user,body);
            //update response
            genericResponse.setData("");
            genericResponse.setResponseCode("RCS001");
            genericResponse.setDescription("OK");
            genericResponse.setHttpStatus(200);

        } catch(Exception e){
            genericResponse = errorResponseHandler(genericResponse,e.getMessage());
        }finally {
        }
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);
    }

    @PostMapping("/pushNotificationInternal") //anonymous endpoint //private strictly for internal call only
    public ResponseEntity pushNotificationInternal(@RequestBody PushNotificationForm body) {

        logger.info("Push notification internal");
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500);
        try {
            //get userID
            User user = userService.getUserbyUsername(body.getUsername());


            Map<String,String> map =new HashMap<>();
            // Only sent deviceid if user set to allowed, else will not provide deviceid , hence no notification will be sent
            UserPermission userPermission = userService.getUserperminssion(body.getUsername());
            if(userPermission!=null && userPermission.getAllowPushNotification()){
                map.put("deviceId",user.getDeviceID());
            }
            else{
                map.put("deviceId","");
            }
            createNotification(user,body);

            //update response
            genericResponse.setData(map);
            genericResponse.setResponseCode("RCS001");
            genericResponse.setDescription("OK");
            genericResponse.setHttpStatus(200);

        } catch(Exception e){
            genericResponse = errorResponseHandler(genericResponse,e.getMessage());
        }finally {
        }
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);
    }

    @GetMapping("/notificationList")
    public ResponseEntity getNotificationList(){

        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500);
        try{
            String query1="select n from Notification n " +
                    "where n.userId.username = :username " +
                    "and n.deletedBy is null and n.statusId.code <> :statusIdCode1";
            List<Notification> notifications = dataManager.load(Notification.class)
                    .query(query1)
                    .parameter("username", currentAuthentication.getUser().getUsername())
                    .parameter("statusIdCode1","G18")
                    .list();

//            for(Notification notification : notifications){
//                Map<String, Object> dataMap = new HashMap<>();
//                dataMap.put("categoryCode",notification.getCategoryId().getCode());
//                dataMap.put("categoryName",notification.getCategoryId().getName());
//                dataMap.put("typeCode",notification.getDetailId().getTypeId().getCode());
//                dataMap.put("typeName",notification.getDetailId().getTypeId().getName();
//                dataMap.put("title",notification.getDetailId().getTitle());
//                dataMap.put("body",notification.getDetailId().getBody());
//                dataMap.put("dataReference",notification.getDetailId().getDataReference());
//                dataMap.put("dataUrl",notification.getDetailId().getDataUrl());
//                dataMap.put("statusCode",notification.getStatusId().getCode());
//                dataMap.put("statusName",notification.getStatusId().getName());
//            }

            for(Notification notification:notifications){
                notification.setUserId(null);
            }

            genericResponse.setData(notifications);
            genericResponse.setResponseCode("RCS001");
            genericResponse.setDescription("OK");
            genericResponse.setHttpStatus(200);

        }catch (Exception e){
            genericResponse = errorResponseHandler(genericResponse,e.getMessage());
        }
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);

    }

    @PutMapping("/updateNotificationStatus")
    public ResponseEntity updateNotificationStatus(@RequestBody List<UpdateNotificationForm> body){

        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500);
        try {

            body.forEach(e -> {
                String query1 = "select n from Notification n " +
                        "where n.id = :id";
                Optional<Notification> notification = dataManager.load(Notification.class)
                        .query(query1).parameter("id", e.getNotificationId())
                        .optional();
                if (notification.isEmpty()) {
                    throw new RuntimeException("INVALID REQUEST");
                }

                switch (e.getUpdateStatusCode()){
                    case "G17":
                        notification.get().setStatusId(READ_STATUS);
                        break;
                    case "G18":
                        notification.get().setStatusId(DELETED_STATUS);
                        break;
                    default: throw new RuntimeException("INVALID REQUEST");
                }
                dataManager.save(notification.get());
            });


            genericResponse.setData("Status Updated");
            genericResponse.setResponseCode("RCS001");
            genericResponse.setDescription("OK");
            genericResponse.setHttpStatus(200);

        }catch (Exception e){
            genericResponse = errorResponseHandler(genericResponse,e.getMessage());
        }
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);

    }


    @Async
    public void createAndSentPushNotification(String username,@NotNull String title,@NotNull String body,@NotNull String dataUrl,@NotNull String dataReference,@NotNull String categoryCode,@NotNull String typeCode){
        try{
            PushNotificationForm pushNotificationForm = new PushNotificationForm();
            pushNotificationForm.setUsername(username);
            pushNotificationForm.setTitle(title);
            pushNotificationForm.setBody(body);
            pushNotificationForm.setDataUrl(dataUrl);
            pushNotificationForm.setCategoryCode(categoryCode);
            pushNotificationForm.setTypeCode(typeCode);
            pushNotificationForm.setDataReference(dataReference);

            User user = userService.getUserbyUsername(username);

            // Only sent push notification if user set to allowed, else no notification will be sent, only created
            createNotification(user,pushNotificationForm);

            UserPermission userPermission = userService.getUserperminssion(username);
            if(userPermission!=null && userPermission.getAllowPushNotification()){
                sentPushNotificationFirebase(user.getDeviceID(),title,body,dataUrl,dataReference,categoryCode,typeCode);
            }
            else{
                logger.info("Permission notification not allow");
            }

        }catch (Exception e){
            logger.info("Error when creating notification: "+e.getMessage());
        }
    }

    @Async //will run in background
    public void sentPushNotificationFirebase(String deviceId,@NotNull String title,@NotNull String body,@NotNull String dataUrl,@NotNull String dataReference,@NotNull String categoryCode,@NotNull String typeCode){
        //this function will call proxy push firebase notification

        try{

            //check deviceId is valid
            if (deviceId == null || deviceId.equals("")) {
                throw new RuntimeException("Device Id null");
            }

            logger.info("deviceId: " + deviceId);

            // build body to call push api at proxy, this api will call firebase to sent notification
            Map<String, Object> mainBody = new HashMap<>();
            Map<String, String> notificationBody = new HashMap<>();
            notificationBody.put("title", title);
            notificationBody.put("body", body);

            Map<String, String> dataBody = new HashMap<>();
            dataBody.put("categoryCode", typeCode);
            dataBody.put("typeCode", typeCode);
            dataBody.put("dataUrl", dataUrl);
            dataBody.put("dataReference", dataReference);


            mainBody.put("notification", notificationBody);
            mainBody.put("data", dataBody);
            mainBody.put("token", deviceId);

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            String pushUrl = env.getProperty("push_proxy_url");
            HttpEntity<Map> pushRequest = new HttpEntity<>(mainBody, headers);
            ResponseEntity<String> pushResponseString = restTemplate.exchange(pushUrl, HttpMethod.POST, pushRequest, String.class);

            Gson pushResponseGson = new Gson();
            JsonObject pushResponseJson = pushResponseGson.fromJson(pushResponseString.getBody(), JsonObject.class);

            logger.info("GSON:" + pushResponseJson.toString());
        }catch (Exception e){

        }


    }



}