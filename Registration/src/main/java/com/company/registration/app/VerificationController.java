package com.company.registration.app;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.company.registration.entity.*;
import com.company.registration.form.*;
import com.company.registration.responseForm.ErrorResponse;
import com.company.registration.services.CipherEncryption;
import com.company.registration.services.EmailService;
import com.company.registration.services.PasswordService;
import com.company.registration.services.UserService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jmix.core.DataManager;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.xpath.operations.Bool;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.context.event.EventListener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("verification")
public class VerificationController {
    static Logger logger = LogManager.getLogger(VerificationController.class.getName());
    @Autowired
    private DataManager dataManager;
    @Autowired
    private UserService userService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private Environment env;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private NotificationController notificationController;
    @Autowired
    private CipherEncryption cipherEncryption;
    private List<KycStatus> kycStatuses;
    private List<NameScreeningResult> nameScreeningResults;
    private List<CrpResult> crpResults;
    private List<OveralVerificationResult> verificationResults;
    private List<ActivationStatus> actStatusTypes;

    private List<IdentificationType> identificationTypes;
    private List<AssessmentStatus> assessmentStatuses;
    private List<VerificationType> verificationTypes;
    private String ctosToken = "";
    private LocalDateTime ctosTokenExpiry = LocalDateTime.now();

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationStartedEvent.class)
    public void doSomethingAfterStartup() {
        loadEntityTypes();
    }

    //--    Internal used function      --//
    public void loadEntityTypes(){
        this.actStatusTypes = this.dataManager.load(ActivationStatus.class).all().list();
        this.identificationTypes = this.dataManager.load(IdentificationType.class).all().list();
        this.assessmentStatuses = this.dataManager.load(AssessmentStatus.class).all().list();
        this.kycStatuses = this.dataManager.load(KycStatus.class).all().list();
        this.nameScreeningResults = this.dataManager.load(NameScreeningResult.class).all().list();
        this.crpResults = this.dataManager.load(CrpResult.class).all().list();
        this.verificationResults = this.dataManager.load(OveralVerificationResult.class).all().list();
        this.verificationTypes = this.dataManager.load(VerificationType.class).all().list();
    }
    public void updateIntegrationResponse(String code, String serviceName, String url, String requestHeader, String requestBody,
                                          String responseHTTP, String responseHeader, String responseBody, Boolean successStatus) {

        logger.info("Update Integration response. URL:" + url);
        IntegrationService integration = this.dataManager.create(IntegrationService.class);
        try {
            integration.setCode(code);
            integration.setServiceName(serviceName);
            integration.setUrl(url);
            integration.setRequestHeader(requestHeader);
            integration.setRequestBody(requestBody);
            integration.setResponseHTTP(responseHTTP);
            integration.setResponseHeader(responseHeader);
            integration.setResponseBody(responseBody);
            integration.setSuccess(successStatus);
            this.dataManager.save(integration);
            logger.info("Integration Service saved");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
    public  LocalDateTime getTokenExpiry(String jwtStrToken){
        try{
            DecodedJWT decodedJWT = JWT.decode(jwtStrToken);
            LocalDateTime createdAt = decodedJWT.getIssuedAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime expiresAt = decodedJWT.getExpiresAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            logger.info("Ctos Token generated at " + createdAt.toString());
            logger.info("Ctos Token expires at " + expiresAt);
            return expiresAt;
        }catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }
    private String generateCtosToken(){
        logger.info("Generate CTOS token...");
        String serviceCode = "ITR04/01";
        String serviceName = "CTOS Authentication";
        String url = env.getProperty("ctosCadUrl") + "/User/authenticate";
        String username = env.getProperty("ctosCadUsername");
        String password = env.getProperty("ctosCadPassword");

        JSONObject jsonBody = new JSONObject();
        jsonBody.put("username",username);
        jsonBody.put("password",password);

        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType JSON = MediaType.parse("application/json");
        String jsonBodyStr = jsonBody.toString();
        okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBodyStr,JSON);
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(url)
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        try{
            okhttp3.Response response = client.newCall(request).execute();
            var responseBodyStr = response.body().string();
            if(response.code() == 200){
                JSONObject responseBodyJson = new JSONObject(responseBodyStr);
                ctosToken = responseBodyJson.getString("key");
                updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStr,
                        String.valueOf(response.code()), response.headers().toString(), responseBodyStr, true);

                //set token expiry
                LocalDateTime expiresAt = getTokenExpiry(ctosToken);
                if(expiresAt.isAfter(LocalDateTime.now().plusHours(1)))
                    ctosTokenExpiry = expiresAt.minusMinutes(15);
                else
                    ctosTokenExpiry = expiresAt;
                return ctosToken;
            }
            else {
                logger.info("Failed generate CTOS CAD token. " + responseBodyStr );
                updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStr,
                        String.valueOf(response.code()), response.headers().toString(), responseBodyStr, false);
                return null;
            }
        }
        catch (Exception ex){
            logger.error("CTOS CAD API get authentication error: " + ex.getMessage());
            return null;
        }
    }
    private ErrorResponse setErrResponse(ErrorCode errorCode, String message){
        ErrorResponse response = new ErrorResponse();
        response.setErrorCode(errorCode.getId());
        response.setMessage(message);
        return response;
    }
    //--    Webhook     --//
    @PostMapping("/webhookStatus")
    public ResponseEntity<String> ctosStatusNotification(@RequestBody String requestBody){
        try {
            //need to add set account to active if overal verificaiton accepted

            logger.info("POST: /verification/webhookStatus");
            String code = "ITR04/10";
            String serviceName = "CTOS Webhook Status Notification";
            logger.info(requestBody);
            LocalDateTime dateNow = LocalDateTime.now();
            LocalDate nextReviewDate = LocalDate.now().plusYears(1);
            ActivationStatus statusActive = actStatusTypes.stream().filter(f -> f.getCode().equals("G04")).collect(Collectors.toList()).get(0);

            WebhookStatusNotification statusNotification = this.dataManager.create(WebhookStatusNotification.class);
            statusNotification.setCode(code);
            statusNotification.setServiceName(serviceName);
            statusNotification.setRequestBody(requestBody);
            this.dataManager.save(statusNotification);

            //Mapped webhook body
            ObjectMapper objectMapper = new ObjectMapper();
//            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategies.UPPER_CAMEL_CASE);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            CadWebhookForm body = objectMapper.readValue(requestBody, CadWebhookForm.class);

            Integer applicationid = Integer.valueOf(body.getData().getApplicationid());
            String ekycresult = body.getData().getEkycresult();
            String gbgres = body.getData().getGbgres();
            String gbgprofile = body.getData().getGbgprofile();

            logger.info("applicationid= "+ applicationid);
            logger.info("ekycresult= " +ekycresult);
            logger.info("gbgres= " +gbgres);
            logger.info("gbgprofile= " +gbgprofile);
            String queryStr = "select e from UserVerification e where e.cadApplicationId = :applicationid " +
                    "order by e.createdDate desc";
            Optional<UserVerification> userVerification = this.dataManager.load(UserVerification.class)
                    .query(queryStr).parameter("applicationid",applicationid).optional();

            //check created verification
            if(userVerification.isPresent()) {
                UserVerification verificationData = userVerification.get();
                logger.info("verification present!");
                User user = userService.getUserbyID(verificationData.getUser().getId());
                GenerateEmailForm emailForm = new GenerateEmailForm();
                emailForm.setSentTo(user.getEmail());
                emailForm.setName(user.getFull_name());
                emailForm.setEmail(user.getEmail());

                KycStatus kycStatus = null;
                NameScreeningResult nsResult = null;
                CrpResult crpResult = null;
                OveralVerificationResult verificationResult = null;
                KYC kycRes = KYC.NULL;
                NsHit hit = NsHit.NULL;
                CRP crpCode = CRP.NULL;
                Boolean activateAccount = false;

                if(ekycresult != null){
                    logger.info("get e-kyc result");
                    if(ekycresult.equalsIgnoreCase("Pass")){
                        kycRes = KYC.PASS;
                        kycStatus = kycStatuses.stream().filter(f -> f.getCode().equals("G09")).collect(Collectors.toList()).get(0);    // G09=pass
//                        verificationResult = verificationResults.stream().filter(f -> f.getCode().equals("G09")).collect(Collectors.toList()).get(0); //G09=accepted
                    }
                    else if(ekycresult.equalsIgnoreCase("Fail")){
                        kycRes = KYC.FAIL;
                        kycStatus = kycStatuses.stream().filter(f -> f.getCode().equals("G10")).collect(Collectors.toList()).get(0);    // G10=fail
//                        verificationResult = verificationResults.stream().filter(f -> f.getCode().equals("G10")).collect(Collectors.toList()).get(0);
                    }
                    logger.info("kycPass:"+ kycRes.name());
                }
                if(gbgres != null){
                    logger.info("get NS result");
                    if(gbgres.equalsIgnoreCase("ACCEPT")){
                        hit = NsHit.NO_HIT;
                        nsResult = nameScreeningResults.stream().filter(f -> f.getCode().equals("G30")).collect(Collectors.toList()).get(0);
                    }
                    else if(gbgres.equalsIgnoreCase("MANUAL_REVIEW")){
                        hit = NsHit.HIT;
                        nsResult = nameScreeningResults.stream().filter(f -> f.getCode().equals("G31")).collect(Collectors.toList()).get(0);
                    }
                    else if(gbgres.equalsIgnoreCase("DECLINE")){
                        hit = NsHit.HIT;
                        nsResult = nameScreeningResults.stream().filter(f -> f.getCode().equals("G32")).collect(Collectors.toList()).get(0);
                    }
                }
                if(gbgprofile != null){
                    logger.info("get CRP result");
                    if(gbgprofile.equalsIgnoreCase("Low Risk")){
                        crpCode = CRP.LOW_RISK;
                        crpResult = crpResults.stream().filter(f -> f.getCode().equals("LOW")).collect(Collectors.toList()).get(0);
                    }
                    else if(gbgprofile.equalsIgnoreCase("Medium Risk")){
                        crpCode = CRP.MEDIUM_RISK;
                        crpResult = crpResults.stream().filter(f -> f.getCode().equals("MEDIUM")).collect(Collectors.toList()).get(0);
                    }
                    else if(gbgprofile.equalsIgnoreCase("High Risk")){
                        crpCode = CRP.HIGH_RISK;
                        crpResult = crpResults.stream().filter(f -> f.getCode().equals("HIGH")).collect(Collectors.toList()).get(0);
                    }
                }
                //set overall status
                if(kycRes == KYC.FAIL){
                    // declined
                    logger.info("ekyc fail");
                    verificationResult = verificationResults.stream().filter(f -> f.getCode().equals("G10")).collect(Collectors.toList()).get(0); //G10=declined
                }
                else if(hit==NsHit.NO_HIT && (crpCode == CRP.LOW_RISK || crpCode == CRP.MEDIUM_RISK)){
                    // auto approved
                    logger.info("Auto approval");
                    activateAccount = true;
                    verificationResult = verificationResults.stream().filter(f -> f.getCode().equals("G09")).collect(Collectors.toList()).get(0); //G09=accepted
                    if(crpCode == CRP.LOW_RISK)
                        nextReviewDate = LocalDate.now().plusYears(5);
                    else
                        nextReviewDate = LocalDate.now().plusYears(3);
                }
                else if(hit == NsHit.NO_HIT && crpCode == CRP.HIGH_RISK){
                    //manual approved
                    logger.info("Manual approval");
                    verificationResult = verificationResults.stream().filter(f -> f.getCode().equals("G05/02")).collect(Collectors.toList()).get(0); //G05=pending approval
                    nextReviewDate = LocalDate.now().plusYears(1);
                }
                else if(hit == NsHit.HIT){
                    //manual approved
                    logger.info("Manual approval");
                    verificationResult = verificationResults.stream().filter(f -> f.getCode().equals("G05/02")).collect(Collectors.toList()).get(0); //G05=pending approval
                }
                else if(kycRes==KYC.PASS && hit == NsHit.NULL && crpCode == CRP.NULL){
                    logger.info("ekyc pass");
                    if(verificationData.getCrpResult()==null && verificationData.getNsResult()==null
                            && !verificationData.getOveralVerificationResult().getCode().equals("G09")){
                        verificationResult = verificationResults.stream().filter(f -> f.getCode().equals("G05/01")).collect(Collectors.toList()).get(0); //G05=pending verification
                    }
                }
                logger.info("set assessment to complete");
                AssessmentStatus assessmentStatus = assessmentStatuses.stream().filter(f -> f.getCode().equals("G01")).collect(Collectors.toList()).get(0);

//                UserVerification verificationData = userVerification.get();
                if(kycStatus != null)
                    verificationData.setKycResult(kycStatus);
                if(nsResult != null)
                    verificationData.setNsResult(nsResult);
                if(crpResult != null)
                    verificationData.setCrpResult(crpResult);
                if(verificationResult!=null)
                    verificationData.setOveralVerificationResult(verificationResult);
                verificationData.setAssessmentStatus(assessmentStatus);
                this.dataManager.save(verificationData);

                if (crpCode != CRP.NULL){
                    //update next review date
                    user.setApproveDate(Date.from(dateNow.atZone(ZoneId.systemDefault()).toInstant()));
                    user.setNextReviewDate(Date.from(nextReviewDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
                    if(activateAccount)
                        user.setActStatus(statusActive);
                    this.dataManager.save(user);
                }
                logger.info("user verification for cad applicationid " + applicationid + " updated");

                //update user tier kyc status :: for verificationType = upgrade tier
                String queryTier = "select e from UserTier e where e.verification.id = :verificationID";
                Optional<UserTier> userTier = this.dataManager.load(UserTier.class)
                        .query(queryTier).parameter("verificationID",verificationData.getId()).optional();
                if(userTier.isPresent()){
                    userTier.get().setKycStatus(kycStatus);
                    this.dataManager.save(userTier.get());
                }

                String title="Account Successfully Upgraded";
                String notificationBody = "Congratulations, your Bursa Gold Dinar account has been successfully upgraded to Tradable Account.";
                //send push notification & email notification
                if(verificationResult != null && verificationResult.getCode().equals("G09")){
                    notificationController.createAndSentPushNotification(verificationData.getUser().getUsername(),title,notificationBody,"","","NC01","NT081");
                    emailService.successVerification(emailForm);
                }
                else if(verificationResult != null && verificationResult.getCode().equals("G10"))
                    emailService.failVerification(emailForm);
                else if(verificationResult != null && verificationResult.getCode().equals("G05/02"))
                    emailService.pendingVerification(emailForm);
            }
            else
                logger.error("cadApplicationId " + applicationid + " not exist.");

            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"OK\"}");
        } catch (Exception ex) {
            logger.error(ex.toString());
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }
    //--    API SIT     --//
    @PostMapping("/ctosAuthenticate")
    public ResponseEntity ctosAuthentication(){
        String cadToken = generateCtosToken();
        return ResponseEntity.status(HttpStatus.OK).body("{\"access_token\":\""+cadToken+"\"}");
    }
    @GetMapping("/ctosVerification")
    public ResponseEntity getCtosVerificationRecord(@RequestParam String id){
        try{
            logger.info("GET: /verification/ctosVerification_SIT");
            String serviceCode = "ITR04/03";
            String serviceName = "CTOS Retrieve Record(SIT)";
            String url = env.getProperty("ctosCadUrl") + "/Record/BGD/"+ id;

            //check token
            if(Objects.equals(ctosToken, "") || ctosToken == null || ctosTokenExpiry.isBefore(LocalDateTime.now())){
                String generateToken = generateCtosToken();
                if (generateToken == null){
                    ErrorResponse response = setErrResponse(ErrorCode.INTEGRATION_ERROR,"Failed to get CAD authentication");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
                }
            }
            OkHttpClient client = new OkHttpClient().newBuilder().build();
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .get()
                    .addHeader("Authorization", "Bearer "+ ctosToken)
                    .build();
            try{
                okhttp3.Response response = client.newCall(request).execute();
                var responseBodyStr = response.body().string();
                return ResponseEntity.status(HttpStatus.OK).body(responseBodyStr);
            }
            catch (Exception ex){
                logger.error("CTOS CAD API error: " + ex.getMessage());
                ErrorResponse errResponse = setErrResponse(ErrorCode.INTEGRATION_ERROR,ex.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            ErrorResponse errResponse = setErrResponse(ErrorCode.MISCELLANEOUS,ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
        }
    }
    @PostMapping("/ctosVerification_SIT")
    public  ResponseEntity ctosVerification_sit(@RequestBody CreateCadForm form){
        try{
            logger.info("POST: /verification/ctosVerification_SIT");
            String serviceCode = "ITR04/02";
            String serviceName = "CTOS Application Submission(SIT)";
            String url = env.getProperty("ctosCadUrl") + "/Record/BGD";
//            String token = env.getProperty("ctosToken");

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if (user == null){
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_ACCOUNT,"User not exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }
            // check user employment
            String countryTaxResidence = null;
            String queryStr = "select e from UserEmployment e where e.user.id = :userID";
            Optional<UserEmployment> employment = this.dataManager.load(UserEmployment.class)
                    .query(queryStr).parameter("userID", user.getId()).optional();
            if(employment.isPresent())
                countryTaxResidence = employment.get().getCountryOftaxResidence().name();
            //check nationality code
            if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(form.getNationalityCode()))){
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid nationality");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            IdentificationType idType = identificationTypes.stream().filter(f -> f.getCode().equals(form.getIdentificationTypeCode())).collect(Collectors.toList()).get(0);
            if(idType == null){
                logger.info("Invalid identification type");
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }
            String docType = "";
            if(form.getIdentificationTypeCode().equals("P01"))  //MyKad
                docType = "1";
//            else if(form.getIdentificationTypeCode().equals("P04")) //MyTentera
//                docType = "2";
            else if(form.getIdentificationTypeCode().equals("P03")) //Passport
                docType = "3";
            else{
                logger.info("Invalid identification type");
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            JSONObject data = new JSONObject();
            data.put("fullname",form.getFullName());
            data.put("idnumber",form.getIdentificationNumber());
            data.put("nationality",form.getNationalityCode());
            data.put("dob",dateFormat.format(form.getDob()));
            data.put("ekycdoctype",docType);

            data.put("memo1",form.getCustomerType());         //cutomer risk: customer type
//            data.put("memo2","No");                         //customer risk: foreign or domestic PEP
            data.put("memo3",form.getCountryOfTaxResidenceCode()); //customer risk: resident/non resident
            data.put("memo4","No");                         //customer risk: high net worth individual(need ctos clarification)
            data.put("memo5",form.getCustomerCategory());    //customer risk: type of customer(New Customer, Repeating Customer)
            data.put("memo7",form.getIndustryType());                 //customer risk: type of customer(need ctos clarification)
//            data.put("memo8","No");                         //customer risk: type of customer(need ctos clarification)
            data.put("memo9",form.getNationalityCode()); //country risk: nationality (country code)
            data.put("memo10",form.getCountryOfTaxResidenceCode());//country risk: country of residence (country code)

            data.put("memo16","No");                        //product risk:
            data.put("memo17","Yes");                        //product risk:
            data.put("memo18","No");                        //product risk:
            data.put("memo19","No");                        //product risk:
            data.put("memo20","No");                        //product risk:
            data.put("memo21","Yes");                        //product risk:

            data.put("memo22","");                        //delivery channel risk: mode of payment
            data.put("memo23","Non face-to-face");                        //delivery channel risk: delivery channel
            data.put("periodicreview","Yes");

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("type","BGD");
            jsonBody.put("data",data);

            OkHttpClient client = new OkHttpClient().newBuilder().build();
            MediaType JSON = MediaType.parse("application/json");

            //check token
            if(Objects.equals(ctosToken, "") || ctosToken == null || ctosTokenExpiry.isBefore(LocalDateTime.now())){
                String generateToken = generateCtosToken();
                if (generateToken == null){
                    ErrorResponse response = setErrResponse(ErrorCode.INTEGRATION_ERROR,"Failed to get CAD authentication");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
                }
            }
            String jsonBodyStrStr = jsonBody.toString();
            okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBodyStrStr,JSON);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("POST", body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer "+ ctosToken)
                    .build();
            JSONObject successResponseJson = new JSONObject();
            try{
                okhttp3.Response response = client.newCall(request).execute();
                var responseBodyStr = response.body().string();
                if(response.code() == 200){
                    JSONObject responseBodyJson = new JSONObject(responseBodyStr);

                    String id = responseBodyJson.getString("id");
                    JSONObject dataJson = (JSONObject) responseBodyJson.get("data");
                    String ekycurl = dataJson.getString("ekycurl");
                    successResponseJson.put("id",id);
                    successResponseJson.put("ekycurl",ekycurl);
                    updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStrStr,
                            String.valueOf(response.code()), response.headers().toString(), responseBodyStr, true);
                    //update data into user verification
                    createUserVerification(user,dataJson);
//                    return ResponseEntity.status(HttpStatus.OK).body(successResponseJson.toString());
                    return ResponseEntity.status(HttpStatus.OK).body(responseBodyStr);
                }
                else {
                    logger.info("Failed to create CAD record. " + responseBodyStr );
                    updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStrStr,
                            String.valueOf(response.code()), response.headers().toString(), responseBodyStr, false);
                    ErrorResponse errResponse = setErrResponse(ErrorCode.INTEGRATION_ERROR,responseBodyStr);
                    return ResponseEntity.status(HttpStatus.valueOf(response.code())).body(errResponse);
                }

            }
            catch (Exception ex){
                logger.error("CTOS CAD API error: " + ex.getMessage());
                ErrorResponse errResponse = setErrResponse(ErrorCode.INTEGRATION_ERROR,ex.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            ErrorResponse errResponse = setErrResponse(ErrorCode.MISCELLANEOUS,ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
        }
    }
    @PutMapping("/ctosVerification_SIT")
    public  ResponseEntity updateCtosVerification_sit(@RequestBody CreateCadForm form){
        try{
            logger.info("PUT: /verification/ctosVerification_SIT");
            String serviceCode = "ITR04/02";
            String serviceName = "CTOS Application Submission(SIT)";
            String url = env.getProperty("ctosCadUrl") + "/Record/BGD/" + form.getId();
//            String token = env.getProperty("ctosToken");

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user = userService.getUserbyUsername(userName);
            if (user == null){
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_ACCOUNT,"User not exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }
            // check user employment
            String countryTaxResidence = null;
            String queryStr = "select e from UserEmployment e where e.user.id = :userID";
            Optional<UserEmployment> employment = this.dataManager.load(UserEmployment.class)
                    .query(queryStr).parameter("userID", user.getId()).optional();
            if(employment.isPresent())
                countryTaxResidence = employment.get().getCountryOftaxResidence().name();
            //check nationality code
            if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(form.getNationalityCode()))){
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid nationality");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            IdentificationType idType = identificationTypes.stream().filter(f -> f.getCode().equals(form.getIdentificationTypeCode())).collect(Collectors.toList()).get(0);
            if(idType == null){
                logger.info("Invalid identification type");
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }
            String docType = "";
            if(form.getIdentificationTypeCode().equals("P01"))  //MyKad
                docType = "1";
//            else if(form.getIdentificationTypeCode().equals("P04")) //MyTentera
//                docType = "2";
            else if(form.getIdentificationTypeCode().equals("P03")) //Passport
                docType = "3";
            else{
                logger.info("Invalid identification type");
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            JSONObject data = new JSONObject();
            data.put("fullname",form.getFullName());
            data.put("idnumber",form.getIdentificationNumber());
            data.put("nationality",form.getNationalityCode());
            data.put("dob",dateFormat.format(form.getDob()));
            data.put("ekycdoctype",docType);

            data.put("memo1",form.getCustomerType());         //cutomer risk: customer type
//            data.put("memo2","No");                         //customer risk: foreign or domestic PEP
            data.put("memo3",form.getNationalityCode()); //customer risk: nationality(resident/non resident)
            data.put("memo4","No");                         //customer risk: high net worth individual(need ctos clarification)
            data.put("memo5",form.getCustomerCategory());    //customer risk: type of customer(New Customer, Repeating Customer)
            data.put("memo7",form.getIndustryType());                 //customer risk: type of customer(need ctos clarification)
//            data.put("memo8","No");                         //customer risk: type of customer(need ctos clarification)
            data.put("memo9",form.getNationalityCode()); //country risk: nationality (country code)
            data.put("memo10",countryTaxResidence);                        //country risk: country of residence (country code)

            data.put("memo16","No");                        //product risk:
            data.put("memo17","Yes");                        //product risk:
            data.put("memo18","No");                        //product risk:
            data.put("memo19","No");                        //product risk:
            data.put("memo20","No");                        //product risk:
            data.put("memo21","Yes");                        //product risk:

            data.put("memo22","");                        //delivery channel risk: mode of payment
            data.put("memo23","Non face-to-face");                        //delivery channel risk: delivery channel
            data.put("periodicreview","Yes");
            if(form.getBypassekyc().equalsIgnoreCase("Yes"))
                data.put("bypassekyc","Yes");

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("type","BGD");
            jsonBody.put("data",data);

            OkHttpClient client = new OkHttpClient().newBuilder().build();
            MediaType JSON = MediaType.parse("application/json");

            //check token
            if(Objects.equals(ctosToken, "") || ctosToken == null || ctosTokenExpiry.isBefore(LocalDateTime.now())){
                String generateToken = generateCtosToken();
                if (generateToken == null){
                    ErrorResponse response = setErrResponse(ErrorCode.INTEGRATION_ERROR,"Failed to get CAD authentication");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
                }
            }
            String jsonBodyStrStr = jsonBody.toString();
            okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBodyStrStr,JSON);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("PUT", body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer "+ ctosToken)
                    .build();
            JSONObject successResponseJson = new JSONObject();
            try{
                okhttp3.Response response = client.newCall(request).execute();
                var responseBodyStr = response.body().string();
                if(response.code() == 200){
                    JSONObject responseBodyJson = new JSONObject(responseBodyStr);

                    String id = responseBodyJson.getString("id");
                    JSONObject dataJson = (JSONObject) responseBodyJson.get("data");
                    String ekycurl = dataJson.getString("ekycurl");
                    successResponseJson.put("id",id);
                    updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStrStr,
                            String.valueOf(response.code()), response.headers().toString(), responseBodyStr, true);
                    //update data into user verification
                    createUserVerification(user,dataJson);
//                    return ResponseEntity.status(HttpStatus.OK).body(successResponseJson.toString());
                    return ResponseEntity.status(HttpStatus.OK).body(responseBodyStr);
                }
                else {
                    logger.info("Failed to create CAD record. " + responseBodyStr );
                    updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStrStr,
                            String.valueOf(response.code()), response.headers().toString(), responseBodyStr, false);
                    ErrorResponse errResponse = setErrResponse(ErrorCode.INTEGRATION_ERROR,responseBodyStr);
                    return ResponseEntity.status(HttpStatus.valueOf(response.code())).body(errResponse);
                }

            }
            catch (Exception ex){
                logger.error("CTOS CAD API error: " + ex.getMessage());
                ErrorResponse errResponse = setErrResponse(ErrorCode.INTEGRATION_ERROR,ex.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            ErrorResponse errResponse = setErrResponse(ErrorCode.MISCELLANEOUS,ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
        }
    }
    //--    API     --//
    @GetMapping("/ctosVerificationResult")
    public ResponseEntity getCtosVerificationResult(@RequestParam (required = false) UUID userID){
        try{
            logger.info("GET:/user/ctosVerificationResult");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            User user;
            if (userID != null){
                user = userService.getUserbyID(userID);
                logger.info("userName(form parameter) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            String queryStr = "select e from UserVerification e where e.user.id = :userID order by " +
                    "COALESCE(e.createdDate, e.lastModifiedDate) desc";
            List<UserVerification> userVerification = this.dataManager.load(UserVerification.class)
                    .query(queryStr).parameter("userID",user.getId()).list();

            JSONObject result = new JSONObject();
            Boolean allowRetry = false;
            logger.info("total verification link:"+userVerification.size());
            if(userVerification.size()!=0){
                UserVerification verificationStatus = userVerification.get(0);
                JSONObject verificationResult = new JSONObject();
                verificationResult.put("code",verificationStatus.getOveralVerificationResult().getCode());
                verificationResult.put("name",verificationStatus.getOveralVerificationResult().getName());
                result.put("verificationResult",verificationResult);

                //declined
                if(verificationStatus.getOveralVerificationResult().getCode().equals("G10") && userVerification.size()<2){
                    allowRetry = true;
                }
            }
            else{
                //no verification record
                allowRetry = true;
                result.put("verificationResult",JSONObject.NULL);
            }
            result.put("userID", user.getId());
            result.put("userName", user.getUsername());
            result.put("allowRetry",allowRetry);

            logger.info("result: " + result);
            logger.info("GET: /user/ctosVerificationResult -> response:" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body(result.toString());
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("/ctosVerification")
    public  ResponseEntity ctosVerification(@RequestBody CreateVerificationForm form){
        try{
            logger.info("POST: /verification/ctosVerification. verificationTypeCode" + form.getVerificationTypeCode());
            String serviceCode = "ITR04/02";
            String serviceName = "CTOS Application Submission";
            String url = env.getProperty("ctosCadUrl") + "/Record/BGD";
            String key = env.getProperty("secret_key");
//            Integer totalAttempt = Integer.valueOf(Objects.requireNonNull(env.getProperty("totalAttemptCtosEkyc")));
            User user;
            String idNumber = null;
            UUID reqCadId = null;
            Boolean retryEkyc = false;
            Boolean bypassEkyc = false;
            Boolean editProfileIsNew = false;

            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);

            if (form.getUserName() != null && !form.getUserName().equals("")){
                logger.info("userName(form body) :->"+form.getUserName());
                user = userService.getUserbyUsername(form.getUserName());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null){
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_ACCOUNT,"User not exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }
            //verificationType
            VerificationType verificationType;
            List<VerificationType> getVerificationTypes = verificationTypes.stream()
                    .filter(f -> f.getCode().equals(form.getVerificationTypeCode())).collect(Collectors.toList());
            if(getVerificationTypes.size()==0){
                logger.error("Invalid verification type code!!!");
                ErrorResponse errResponse = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid verification type code!!!");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
            }
            verificationType = getVerificationTypes.get(0);

            //edit profile
            EditProfileRef editProfileRef = null;
            if(form.getVerificationTypeCode().equals("VET02")){
                //check edit profile ref table
                String queryEditProfileRef = "select e from EditProfileRef e where e.user.id = :userID " +
                        "order by e.createdDate desc";
                Optional<EditProfileRef> checkEditProfileRef = this.dataManager.load(EditProfileRef.class)
                        .query(queryEditProfileRef)
                        .parameter("userID",user.getId()).optional();
                if(checkEditProfileRef.isPresent()){
                    logger.info("Edit profile ref exist");
                    editProfileRef = checkEditProfileRef.get();
                    bypassEkyc = editProfileRef.getBypassEkyc();
                    if(editProfileRef.getCadApplicationId() == null){
                        logger.info("create new verification for edit profile");
                        editProfileIsNew = true;
                    }
                    else
                        logger.info("verification for edit profile already exist");
                }
                else{
                    logger.info("Create new edit profile ref(new verification for edit profile)");
                    editProfileRef = this.dataManager.create(EditProfileRef.class);
                    editProfileRef.setUser(user);
                    editProfileRef.setBypassEkyc(false);
                    editProfileRef.setBypassVerification(false);
                    editProfileIsNew = true;
                }
            }
//            VerificationType verificationTypeUpgradeTier = verificationTypes.stream().filter(f -> f.getCode().equals("VET01")).collect(Collectors.toList()).get(0);
            // check for ongoing verification
             //assessment ongoing
            AssessmentStatus assessmentOngoing = null;
            AssessmentStatus assessmentCompleted = null;
            List<AssessmentStatus> getAssessmentStatusOngoing = assessmentStatuses.stream().filter(f -> f.getCode().equals("G24")).collect(Collectors.toList());
            List<AssessmentStatus> getAssessmentStatusCompleted = assessmentStatuses.stream().filter(f -> f.getCode().equals("G01")).collect(Collectors.toList());
            if(getAssessmentStatusOngoing.size()!=0)
                assessmentOngoing = getAssessmentStatusOngoing.get(0);
            if(getAssessmentStatusCompleted.size()!=0)
                assessmentCompleted = getAssessmentStatusCompleted.get(0);

            String queryVerification = "select e from UserVerification e where e.user.id = :userID " +
                    "and e.verificationType.id =:verificationType order by COALESCE(e.createdDate, e.lastModifiedDate) desc";
            List<UserVerification> checkUserVerification = this.dataManager.load(UserVerification.class)
                    .query(queryVerification)
                    .parameter("userID",user.getId())
                    .parameter("verificationType",verificationType.getId()).list();
            logger.info("total user verification: "+checkUserVerification.size());
            //periodic review : only check for create date after next review date
            if(form.getVerificationTypeCode().equals("VET11")){
                Date reviewDate = user.getNextReviewDate();
                checkUserVerification = checkUserVerification.stream().filter(f -> !f.getCreatedDate().before(reviewDate)).collect(Collectors.toList());
            }
            if(checkUserVerification.size()!=0 && !editProfileIsNew){
                int totalVerification = checkUserVerification.size();
                String assessmentCode = checkUserVerification.get(0).getAssessmentStatus().getCode();
                String resultCode = checkUserVerification.get(0).getOveralVerificationResult().getCode();

                //G24= assessment ongoing-use same ekyc url
                if(assessmentCode.equals("G24")){
                    logger.info("Previous ekycurl still ongoing");
                    Date createdDate = checkUserVerification.get(0).getCreatedDate();
                    LocalDateTime urlExpireDate = createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().plusHours(72);
                    if(urlExpireDate.isAfter(LocalDateTime.now().plusMinutes(-5))){
                        logger.info("Return existing URl. Application id =" + checkUserVerification.get(0).getCadApplicationId());
                        JSONObject responseJson = new JSONObject();
                        responseJson.put("id",checkUserVerification.get(0).getCadId());
                        responseJson.put("ekycurl",checkUserVerification.get(0).getEkycUrl());
                        if(checkUserVerification.get(0).getEkycUrl()==null || checkUserVerification.get(0).getEkycUrl().equals(""))
                            bypassEkyc = true;
                        responseJson.put("bypassEkyc",bypassEkyc);
                        return ResponseEntity.status(HttpStatus.OK).body(responseJson.toString());
                    }
                }
                //assessment complete
                else if(assessmentCode.equals("G01")){
                    // accepted (upgrade tier & periodic review)
                    if(resultCode.equals("G09") &&
                            (form.getVerificationTypeCode().equals("VET01") || form.getVerificationTypeCode().equals("VET11"))){
                        logger.info("Verification already accepted");
                        ErrorResponse errorResponse = new ErrorResponse();
                        errorResponse.setErrorCode(ErrorCode.INVALID_REQUEST.getId());
                        errorResponse.setMessage("Verification already accepted");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                    }
                    //declined and retry
                    else if(resultCode.equals("G10") && totalVerification < 2){
                        logger.info("Existing verification declined. Generate new ekyc URL with existing application ID. retryekyc = true");
                        reqCadId = checkUserVerification.get(0).getCadId();
                        retryEkyc = true;
                    }
                    //declined and exceeded
                    else if(resultCode.equals("G10")){
                        logger.info("Maximum verification attempts reached");
                        ErrorResponse errorResponse = new ErrorResponse();
                        errorResponse.setErrorCode(ErrorCode.TOO_MANY_ATTEMPS.getId());
                        errorResponse.setMessage("Maximum verification attempts reached");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                    }
                    // complete and pending result
                    else if(resultCode.equals("G05/01") || resultCode.equals("G05/02")){
                        logger.info("Verification still pending");
                        ErrorResponse errorResponse = new ErrorResponse();
                        errorResponse.setErrorCode(ErrorCode.INVALID_REQUEST.getId());
                        errorResponse.setMessage("Verification still pending");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
                    }
                    else{
                        logger.info("WARNING!! Unchecked condition for assessment status and result");
                    }
                }
                else {
                    logger.info("WARNING!! Unchecked condition for assessment status and result");
                }
            }

            // check user employment
            String countryTaxResidence = "";
            String industryTypeCode = "V";  //default if unemployed (Homemakers, Students, Unemployed)
            String queryStr = "select e from UserEmployment e where e.user.id = :userID order by " +
                    "COALESCE(e.createdDate, e.lastModifiedDate) desc";
            Optional<UserEmployment> employment = this.dataManager.load(UserEmployment.class)
                    .query(queryStr).parameter("userID", user.getId()).optional();
            if(employment.isPresent()){
                logger.info("Employment is present");
                countryTaxResidence = employment.get().getCountryOftaxResidence().name();
                if(employment.get().getNatureOfBusiness()!=null)
                    industryTypeCode = employment.get().getNatureOfBusiness().getCode();
                else if(employment.get().getEmploymentStatus().getCode().equals("UNEMPL"))
                    industryTypeCode = "V";
            }

            String docType = "";
            if(user.getIdentificationType().getCode().equals("P01"))  //MyKad
                docType = "1";
//            else if(user.getIdentificationType().getCode().equals("P04")) //MyTentera
//                docType = "2";
            else if(user.getIdentificationType().getCode().equals("P03")) //Passport
                docType = "3";
            else if(user.getIdentificationType().getCode().equals("P09"))   //myPR
                docType = "5";
            else{
                logger.info("Not supported identification type");
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Not supported identification type");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }
            //************************** Prepare Data ***********************************//
            //decrypt ic number and phone number before response
            if(user.getIdentificationNumber()!= null && !Objects.equals(user.getIdentificationNumber(), "")){
                try{
                    idNumber = cipherEncryption.decrypt(user.getIdentificationNumber(), key);
                }
                catch (Exception ex){
                    logger.error(ex.getMessage());
                    logger.info("Failed to decrypt ic number");
                }
            }
            String birthday = (cipherEncryption.decrypt(user.getBirthday(), key));
//            String dob = dateFormat.format(birthday);

            JSONObject data = new JSONObject();
            data.put("fullname",user.getFull_name());
            data.put("idnumber",idNumber);
            data.put("nationality",user.getNationality().name());
            data.put("dob",birthday);
            data.put("ekycdoctype",docType);
            data.put("memo1",user.getUserType().getName()); //cutomer risk: customer type
            data.put("memo3",countryTaxResidence);          //customer risk: resident/non resident
            data.put("memo4","No");                         //customer risk: high net worth individual(need ctos clarification)
            if(form.getVerificationTypeCode().equals("VET01"))
                data.put("memo5","New Customer");
            else
                data.put("memo5","Repeating Customer");
            data.put("memo7",industryTypeCode);                 //customer risk: type of industry(need ctos clarification)
//            data.put("memo8","No");                       //customer risk: adverse remark(need ctos clarification)
            data.put("memo9",user.getNationality().name()); //country risk: nationality (country code)
            data.put("memo10",countryTaxResidence);         //country risk: country of residence (country code)
            data.put("memo16","No");                        //product risk:
            data.put("memo17","Yes");                        //product risk:
            data.put("memo18","No");                        //product risk:
            data.put("memo19","No");                        //product risk:
            data.put("memo20","No");                        //product risk:
            data.put("memo21","No");                       //product risk:
            data.put("memo22","");                          //delivery channel risk: mode of payment
            data.put("memo23","Non face-to-face");          //delivery channel risk: delivery channel
            data.put("periodicreview","Yes");
            if(retryEkyc)
                data.put("retryekyc","Yes");
            if(!form.getVerificationTypeCode().equals("VET01") && bypassEkyc)
                data.put("bypassekyc","Yes");

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("type","BGD");
            jsonBody.put("data",data);

            OkHttpClient client = new OkHttpClient().newBuilder().build();
            MediaType JSON = MediaType.parse("application/json");
            //check token
            if(Objects.equals(ctosToken, "") || ctosToken == null || ctosTokenExpiry.isBefore(LocalDateTime.now())){
                String generateToken = generateCtosToken();
                if (generateToken == null){
                    logger.error("Fail to generate CAD authentication");
                    ErrorResponse response = setErrResponse(ErrorCode.INTEGRATION_ERROR,"Fail to generate CAD authentication");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
                }
            }
            String method = "POST";
            if(retryEkyc){
                method = "PUT";
                url = url + "/" + reqCadId;
            }
            String jsonBodyStrStr = jsonBody.toString();
            okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBodyStrStr,JSON);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method(method, body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer "+ ctosToken)
                    .build();
            JSONObject successResponseJson = new JSONObject();
            String cadId;
            int cadApplicationId;
            String gbgRef = "";
            String ekycurl = "";
            try{
                okhttp3.Response response = client.newCall(request).execute();
                var responseBodyStr = response.body().string();
                if(response.code() == 200){
                    logger.info("CTOS API response.code() :" + response.code());
                    JSONObject responseBodyJson = new JSONObject(responseBodyStr);

                    cadId = responseBodyJson.getString("id");
                    JSONObject dataJson = (JSONObject) responseBodyJson.get("data");
                    cadApplicationId = Integer.parseInt(dataJson.getString("applicationid"));
                    if(dataJson.has("ekycurl"))
                        ekycurl = dataJson.getString("ekycurl");
                    if(dataJson.has("gbgref"))
                        gbgRef = dataJson.getString("gbgref");

                    successResponseJson.put("id",cadId);
                    successResponseJson.put("ekycurl",ekycurl);
                    successResponseJson.put("bypassEkyc",bypassEkyc);
                    updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStrStr,
                            String.valueOf(response.code()), response.headers().toString(), responseBodyStr, true);

//                    //update data into user verification
//                    createUserVerification(user,dataJson);
                }
                else {
                    logger.info("Failed to create CAD record. " + responseBodyStr );
                    updateIntegrationResponse(serviceCode, serviceName, url, request.headers().toString(), jsonBodyStrStr,
                            String.valueOf(response.code()), response.headers().toString(), responseBodyStr, false);
                    ErrorResponse errResponse = setErrResponse(ErrorCode.INTEGRATION_ERROR,responseBodyStr);
                    return ResponseEntity.status(HttpStatus.valueOf(response.code())).body(errResponse);
                }
            }
            catch (Exception ex){
                logger.error("CTOS CAD API error:");
                logger.error(ex.getMessage());
                ErrorResponse errResponse = setErrResponse(ErrorCode.INTEGRATION_ERROR,ex.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
            }
            OveralVerificationResult verificationPending = verificationResults.stream().filter(f -> f.getCode().equals("G05/01")).collect(Collectors.toList()).get(0);

            //user verification table
            UserVerification userVerification = this.dataManager.create(UserVerification.class);
            userVerification.setUser(user);
            userVerification.setCadId(UUID.fromString(cadId));
            userVerification.setCadApplicationId(cadApplicationId);
            userVerification.setGbgRef(gbgRef);
            userVerification.setKycResult(null);
            userVerification.setNsResult(null);
            userVerification.setCrpResult(null);
            userVerification.setOveralVerificationResult(verificationPending);         //pending
            if(bypassEkyc)
                userVerification.setAssessmentStatus(assessmentCompleted);
            else
                userVerification.setAssessmentStatus(assessmentOngoing);
            userVerification.setEkycUrl(ekycurl);
            userVerification.setVerificationType(verificationType);
            this.dataManager.save(userVerification);

            //update edit profile ref
            if(form.getVerificationTypeCode().equals("VET02")) {
                assert editProfileRef != null;
                editProfileRef.setCadApplicationId(cadApplicationId);
                this.dataManager.save(editProfileRef);
            }
            //user tier table
            TierType tierType = this.dataManager.load(TierType.class)
                    .query("select e from TierType e where e.code = :code")
                    .parameter("code","T01").one();

            //G05/01 = pending-verification
            KycStatus kycStatus=null;
            List<KycStatus> kycStatusList = kycStatuses.stream().filter(f -> f.getCode().equals("G05/01")).collect(Collectors.toList());
            if(kycStatusList.size()!=0)
                kycStatus = kycStatusList.get(0);

//            if(form.getVerificationTypeCode().equals("VET01")){
            UserTier userTier = this.dataManager.create(UserTier.class);
            userTier.setUser(user);
//            userTier.setReference(form.getReference());
            userTier.setTier(tierType);
            userTier.setKycStatus(kycStatus);
            userTier.setCurrentTier(!kycStatus.getCode().equals("G10"));
            userTier.setVerification(userVerification);
            this.dataManager.save(userTier);
            if(userTier.getKycStatus() != null && !userTier.getKycStatus().getCode().equals("G10")) {   //G10=declined
                //set existing tier to false
                List<UserTier> userTiers = this.dataManager.load(UserTier.class)
                        .query("select e from UserTier e where e.user = :user_id " +
                                "and e.id <> :id and e.currentTier = true")
                        .parameter("user_id",userTier.getUser())
                        .parameter("id",userTier.getId())
                        .list();
                userTiers.forEach(e-> {
                    e.setCurrentTier(false);
                    dataManager.save(e);
//                    dataManager.remove(e);
                });
            }
//            }
            String responseBody = successResponseJson.toString();
            logger.info("Success Response: "+HttpStatus.OK + responseBody);
            return ResponseEntity.status(HttpStatus.OK).body(responseBody);
        }
        catch (Exception ex){
            logger.error("ERROR");
            logger.error(ex.getMessage());
            ErrorResponse errResponse = setErrResponse(ErrorCode.MISCELLANEOUS,ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errResponse);
        }
    }

    private JSONObject createUserVerification(User user, JSONObject cadResultJson){
        JSONObject response = new JSONObject();
        UserVerification userVerification = this.dataManager.create(UserVerification.class);
        userVerification.setUser(user);
//        userVerification.setCadId(cadResultJson.get("").toString());
        return response;
    }
    @GetMapping("/ctosTokenExpiry")
    public  LocalDateTime ctosTokenExpiry(@RequestBody String jwtStrToken){
        try{
            return getTokenExpiry(jwtStrToken);
        }catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }

}
