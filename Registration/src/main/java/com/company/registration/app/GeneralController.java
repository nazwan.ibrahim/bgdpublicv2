package com.company.registration.app;

import com.company.registration.entity.*;
import com.company.registration.form.*;
import com.company.registration.responseForm.AddressObj;
import com.company.registration.responseForm.PostcodeInfo;
import com.company.registration.responseForm.UserSecurityQuestions;
import com.company.registration.services.CipherEncryption;
import com.company.registration.services.EmailService;
import com.company.registration.services.UserService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twilio.rest.verify.v2.Service;
import com.twilio.Twilio;

import io.jmix.core.DataManager;
import io.jmix.core.querycondition.LogicalCondition;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.context.event.EventListener;

import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("general")
public class GeneralController {
    static Logger logger = LogManager.getLogger(GeneralController.class.getName());

    @Autowired
    private DataManager dataManager;
    @Autowired
    private Environment environment;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private UserService userService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private CipherEncryption cipherEncryption;
    @Autowired
    private PasswordController passwordController;

    private List<ActivationStatus> actStatusTypes;

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationStartedEvent.class)
    public void doSomethingAfterStartup() {
        loadEntityTypes();
    }
    public void loadEntityTypes(){
        this.actStatusTypes = this.dataManager.load(ActivationStatus.class).all().list();
    }

    public String createTwilioVerificationService(){
        String ACCOUNT_SID = environment.getProperty("TWILIO_ACCOUNT_SID");
        String AUTH_TOKEN = environment.getProperty("TWILIO_AUTH_TOKEN");
        //create service
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Service service = Service.creator("Bursa Gold Dinar Registration").create();
        return service.getSid();
    }

    @PostMapping("/generateOTP")
    public ResponseEntity generateOTP(@RequestBody GenerateOtpForm form){

        try{
            logger.info("POST: /generateOTP");
            logger.info("phone number :"+ form.getPhoneNumber());
            String key = environment.getProperty("secret_key");

            // set status pending
            ActivationStatus actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G05"))
                    .collect(Collectors.toList()).get(0);

            Date date = Calendar.getInstance().getTime();
            Date regenerateTime = DateUtils.addMinutes(date,-2);
            Random rnd = new Random();
            Integer otp = 100000 + rnd.nextInt(900000);

            String queryStr ="select e from Otp e where e.phoneNumber = :phoneNumber " +
                    "and e.createdDate > :regenerateTime";

            Optional<Otp> checkOtp = this.dataManager.load(Otp.class)
                    .query(queryStr)
                    .parameter("phoneNumber",form.getPhoneNumber())
                    .parameter("regenerateTime",regenerateTime)
                    .optional();
            if(checkOtp.isPresent()){
                logger.info("regenerate OTP within 2 minutes not allow");
                JSONObject jsonResponse = new JSONObject();
                jsonResponse.put("displayMessage","Please wait 2 minutes to generate new OTP");
                jsonResponse.put("message","Please wait 2 minutes to generate new OTP");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonResponse.toString());
            }

//            String body ="RM0 BGD: ";
            String body ="";
            String body2 ="";
            if(form.getServiceType() == OtpServiceType.REGISTRATION){
                body2 = " is your OTP for Bursa Gold Dinar registration.";
            }
            else if (form.getServiceType() == OtpServiceType.RESET_PASSWORD){
                body2 = " is your OTP verification code. This code expires in 2 minutes." +
                        " Please ignore this message if you didn't request for OTP.";
            }
            else if (form.getServiceType() == OtpServiceType.RESET_PIN){
                body2 = " is your OTP for reset BGD App PIN.";
            }
            else if (form.getServiceType() == OtpServiceType.UPDATE_BANK){
                body2 = " is your OTP for update BGD App bank.";
            }
            else if (form.getServiceType() == OtpServiceType.CHANGE_PHONE_NUMBER){
                body2 = " is your OTP for change BGD Phone Number.";
            }
            else{
                body2 = " is your OTP for Bursa Gold Dinar App.";
            }
            String smsBody = body + otp + body2;
            //exabytes
            smsBody = smsBody.replace(" ", "%20");
            String user = environment.getProperty("EXABYTES_USERNAME");
            String pass = environment.getProperty("EXABYTES_PASSWORD");
            String phoneNumber = form.getPhoneNumber();
            int type = 1;
            String sendid = "exabytes"; //Malaysia does not support sender id yet
            String url = environment.getProperty("EXABYTES_SEND_MESSAGE_URL");
            URL sendSmsUrl = new URL(url+"?un="+ user +"&pwd=" + pass + "&dstno=" + phoneNumber +
                            "&msg=" + smsBody + "&type=" + type + "&sendid=" + sendid + "&agreedterm=YES");
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(sendSmsUrl)
                    .get()
                    .build();

            String service_sid = "SME"+RandomHexStringGenerator(32);
            try{
                okhttp3.Response response = client.newCall(request).execute();
                var StrBody = response.body().string();
                if (response.code()==200){
                    logger.info(StrBody);
                    if(StrBody.startsWith("2000")){
                        logger.info("OTP successfully sent to " + phoneNumber);
                    }
                    else{
                        logger.error(StrBody);
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.message());
                    }
                }
                else{
                    logger.error("Failed to generate OTP");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(StrBody);
                }
                response.close();
            }
            catch (Exception ex){
                return null;
            }
            date = Calendar.getInstance().getTime();
            // validity 2 minutes with 15 seconds buffer
            Date expiry = DateUtils.addSeconds(date,135);

//            logger.info("OTP successfully sent to " + phoneNumber);
            Map<String,String> data = new HashMap<>();
//            data.put("service_sid",message.getSid());
            data.put("service_sid",service_sid);
            String res = objectMapper.writeValueAsString(data);

            Otp newOtp = this.dataManager.create(Otp.class);
            newOtp.setOtp(cipherEncryption.encrypt(String.valueOf(otp),key));
//            newOtp.setServiceSid(message.getSid());
            newOtp.setServiceSid(service_sid);
            newOtp.setPhoneNumber(phoneNumber);
            newOtp.setExpiryDateTime(expiry);
            newOtp.setServiceType(form.getServiceType());
            newOtp.setStatus(actStatus);
            dataManager.save(newOtp);

            return ResponseEntity.status(HttpStatus.OK).body(res);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public JSONObject generateOTPFunction(GenerateOtpForm form){
        logger.info("generateOTP for phone number :"+ form.getPhoneNumber());
        String key = environment.getProperty("secret_key");

        JSONObject badrequestResponse = new JSONObject();
        badrequestResponse.put("status",HttpStatus.BAD_REQUEST.value());

        // set status pending
        ActivationStatus actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G05"))
                .collect(Collectors.toList()).get(0);

        Date date = Calendar.getInstance().getTime();
        Date regenerateTime = DateUtils.addMinutes(date,-2);
        Random rnd = new Random();
        Integer otp = 100000 + rnd.nextInt(900000);

        String queryStr ="select e from Otp e where e.phoneNumber = :phoneNumber " +
                "and e.createdDate > :regenerateTime";

        Optional<Otp> checkOtp = this.dataManager.load(Otp.class)
                .query(queryStr)
                .parameter("phoneNumber",form.getPhoneNumber())
                .parameter("regenerateTime",regenerateTime)
                .optional();
        if(checkOtp.isPresent()){
            logger.info("regenerate OTP within 2 minutes not allow");
            badrequestResponse.put("message","Please wait 2 minutes to generate new OTP");
            return badrequestResponse;
        }
//            String body ="RM0 BGD: ";
        String body ="";
        String body2 ="";
        if(form.getServiceType() == OtpServiceType.REGISTRATION){
            body2 = " is your OTP for Bursa Gold Dinar registration.";
        }
        else if (form.getServiceType() == OtpServiceType.RESET_PASSWORD){
            body2 = " is your OTP verification code. This code expires in 2 minutes." +
                    " Please ignore this message if you didn't request for OTP.";
        }
        else if (form.getServiceType() == OtpServiceType.RESET_PIN){
            body2 = " is your OTP for reset BGD App PIN.";
        }
        else{
            body2 = " is your OTP for Bursa Gold Dinar App.";
        }
        String smsBody = body + otp + body2;

        //exabytes
        smsBody = smsBody.replace(" ", "%20");
        String user = environment.getProperty("EXABYTES_USERNAME");
        String pass = environment.getProperty("EXABYTES_PASSWORD");
        String phoneNumber = form.getPhoneNumber();
        int type = 1;
        String sendid = "exabytes"; //Malaysia does not support sender id yet
        String url = environment.getProperty("EXABYTES_SEND_MESSAGE_URL");

        String service_sid = "SME"+RandomHexStringGenerator(32);
        try{
            URL sendSmsUrl = new URL(url+"?un="+ user +"&pwd=" + pass + "&dstno=" + phoneNumber +
                    "&msg=" + smsBody + "&type=" + type + "&sendid=" + sendid + "&agreedterm=YES");
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(sendSmsUrl)
                    .get()
                    .build();

                okhttp3.Response response = client.newCall(request).execute();
                var StrBody = response.body().string();
                if (response.code()==200){
                    logger.info(StrBody);
                    if(StrBody.startsWith("2000")){
                        logger.info("OTP successfully sent to " + phoneNumber);
                    }
                    else{
                        logger.error(StrBody);
                        badrequestResponse.put("message",response.message());
                        return badrequestResponse;
//                        return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(response.message());
                    }
                }
                else{
                    logger.error("Failed to generate OTP");
                    badrequestResponse.put("message",StrBody);
                    return badrequestResponse;
//                    return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(StrBody);
                }
                response.close();
        }
        catch (Exception ex){
            return null;
        }
        date = Calendar.getInstance().getTime();
        // validity 2 minutes with 15 seconds buffer
        Date expiry = DateUtils.addSeconds(date,135);
        Otp newOtp = this.dataManager.create(Otp.class);
        newOtp.setOtp(cipherEncryption.encrypt(String.valueOf(otp),key));
        newOtp.setServiceSid(service_sid);
        newOtp.setPhoneNumber(phoneNumber);
        newOtp.setExpiryDateTime(expiry);
        newOtp.setServiceType(form.getServiceType());
        newOtp.setStatus(actStatus);
        dataManager.save(newOtp);

        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("status",HttpStatus.OK.value());
        jsonResponse.put("service_sid",service_sid);
        jsonResponse.put("otpEntity",newOtp);
        return jsonResponse;
    }

    private String RandomHexStringGenerator(int hexLength){
        try{
            StringBuilder sb = new StringBuilder();
            Random random = new Random();
            String hexChars = "0123456789abcdef";
            for (int i = 0; i < hexLength; i++) {
                int randomIndex = random.nextInt(hexChars.length());
                char randomChar = hexChars.charAt(randomIndex);
                sb.append(randomChar);
            }
            return sb.toString();
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return null;
        }
    }
    @PostMapping("/verifyOTP")
    public ResponseEntity verifyOTP(@RequestBody OtpForm form){
        try{
            logger.info("POST: /verifyOTP");
            logger.info("serciveSid :"+ form.getServiceSid());
            String key = environment.getProperty("secret_key");
            JSONObject verificationExpiredResponse = new JSONObject();
            verificationExpiredResponse.put("displayMessage","OTP expired");
            verificationExpiredResponse.put("message","Verification check expired");

            Date dateTimeNow = Calendar.getInstance().getTime();

            String queryStr ="select e from Otp e where e.serviceSid = :serviceSid " +
                    "order by e.createdDate desc";

            Optional<Otp> otp = this.dataManager.load(Otp.class)
                    .query(queryStr)
                    .parameter("serviceSid",form.getServiceSid())
//                    .parameter("phoneNumber",form.getPhoneNumber())
                    .optional();

            if (otp.isEmpty()){
                logger.info("No OTP found");
                JSONObject jsonResponse = new JSONObject();
                jsonResponse.put("displayMessage","Verification not found");
                jsonResponse.put("message","Verification not found");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonResponse.toString());
            }
            //validate phone number for registration
            else if(otp.get().getServiceType().equals(OtpServiceType.REGISTRATION)){
                if(!(trimPhoneNumber(form.getPhoneNumber())).equals(trimPhoneNumber(otp.get().getPhoneNumber()))){
                    logger.info("provided phone number not equal with stored number");
                    JSONObject jsonResponse = new JSONObject();
                    jsonResponse.put("message","Verification not found");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonResponse.toString());
                }
            }
            else if(otp.get().getExpiryDateTime().before(dateTimeNow)){
                logger.info("OTP verification check expired");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(verificationExpiredResponse.toString());
            }

            Date createdDate = otp.get().getCreatedDate();
            LocalDateTime createdLocalDateTime = createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            OtpForm responseForm = new OtpForm();
            if (!form.getOtp().equals(cipherEncryption.decrypt(otp.get().getOtp(), key))){
                logger.info("Wrong OTP");
                responseForm.setValid(false);
                responseForm.setDisplayMessage("Invalid OTP number");
                responseForm.setDateCreated(createdLocalDateTime);
                responseForm.setServiceSid(form.getServiceSid());
                responseForm.setPhoneNumber(otp.get().getPhoneNumber());
                responseForm.setStatus("pending");
            }
            else{
                ActivationStatus actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G06"))
                        .collect(Collectors.toList()).get(0);
                otp.get().setStatus(actStatus);
                dataManager.save(otp.get());

                if(otp.get().getServiceType() == OtpServiceType.RESET_PASSWORD){
                    String query ="select e from ResetPassword e where e.otpVerification = :otpVerification " +
                            "and e.active = :active and e.expiredDatetime > :dateTimeNow order by e.createdDate desc";
                    Optional<ResetPassword> resetPassword = this.dataManager.load(ResetPassword.class)
                            .query(query)
                            .parameter("otpVerification",otp.get())
                            .parameter("active",true)
                            .parameter("dateTimeNow",dateTimeNow)
                            .optional();
                    if(resetPassword.isEmpty()){
                        logger.info("No active reset password id");
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(verificationExpiredResponse.toString());
                    }
                    responseForm.setResetPasswordId(resetPassword.get().getId().toString());
                }
                String phoneNumber = otp.get().getPhoneNumber();
                int lenNumber = phoneNumber.length();
                String masked = StringUtils.repeat("*", lenNumber-7);
                String phoneNumberMask = phoneNumber.substring(0,5)+masked+phoneNumber.substring(lenNumber-2,lenNumber);

                responseForm.setValid(true);
                responseForm.setDisplayMessage("Verified");
                responseForm.setDateCreated(createdLocalDateTime);
                responseForm.setServiceSid(form.getServiceSid());
                responseForm.setPhoneNumber(phoneNumberMask);
                responseForm.setStatus("approved");
            }
            return ResponseEntity.status(HttpStatus.OK).body(responseForm);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/detailUpdate/verifyOTP")
    public ResponseEntity verifyOTP2(@RequestBody OtpForm form){
        try{
            logger.info("POST: /detailUpdate/verifyOTP");
            logger.info("phone number :"+ form.getPhoneNumber());
            String key = environment.getProperty("secret_key");

            Date dateTimeNow = Calendar.getInstance().getTime();

            String queryStr ="select e from Otp e where e.serviceSid = :serviceSid " +
                    "and e.phoneNumber = :phoneNumber order by e.createdDate desc";

            Optional<Otp> otp = this.dataManager.load(Otp.class)
                    .query(queryStr)
                    .parameter("serviceSid",form.getServiceSid())
                    .parameter("phoneNumber",form.getPhoneNumber())
                    .optional();

            if (otp.isEmpty()){
                logger.info("No OTP found");
                JSONObject jsonResponse = new JSONObject();
                jsonResponse.put("displayMessage","Verification not found");
                jsonResponse.put("message","Verification not found");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonResponse.toString());
            }
            else if(otp.get().getExpiryDateTime().before(dateTimeNow)){
                logger.info("OTP verification check expired");
                JSONObject jsonResponse = new JSONObject();
                jsonResponse.put("displayMessage","OTP expired");
                jsonResponse.put("message","Verification check expired");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsonResponse.toString());
            }

            Date createdDate = otp.get().getCreatedDate();
            LocalDateTime createdLocalDateTime = createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            OtpForm responseForm = new OtpForm();

            int lenNumber = form.getPhoneNumber().length();
            String masked = StringUtils.repeat("*", lenNumber-7);
            String phoneNumberMask = form.getPhoneNumber().substring(0,5)+masked+form.getPhoneNumber().substring(lenNumber-2,lenNumber);
            if (!form.getOtp().equals(cipherEncryption.decrypt(otp.get().getOtp(), key))){

                responseForm.setValid(false);
                responseForm.setDisplayMessage("Invalid OTP number");
                responseForm.setDateCreated(createdLocalDateTime);
                responseForm.setServiceSid(form.getServiceSid());
                responseForm.setPhoneNumber(phoneNumberMask);
                responseForm.setStatus("pending");
            }
            else{
                ActivationStatus actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G06"))
                        .collect(Collectors.toList()).get(0);
                otp.get().setStatus(actStatus);
                dataManager.save(otp.get());

                responseForm.setValid(true);
                responseForm.setDisplayMessage("Verified");
                responseForm.setDateCreated(createdLocalDateTime);
                responseForm.setServiceSid(form.getServiceSid());
                responseForm.setPhoneNumber(phoneNumberMask);
                responseForm.setStatus("approved");
            }
            return ResponseEntity.status(HttpStatus.OK).body(responseForm);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/securityQuestion")
    public ResponseEntity securityQuestion(@RequestBody SecurityQuestionForm form){

//        List<User> userList = dataManager.load(User.class).condition(PropertyCondition.equal("username",username)).list();
        try {
            logger.info("POST:/securityQuestion, Request body-> " + form.toString());
            String key = environment.getProperty("secret_key");
            User user = userService.getUserbyID(form.getUserID());
            if(user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user not exist or inactive");

            Optional<SecurityQuestion> securityQuestion = dataManager.load(SecurityQuestion.class)
                    .condition(PropertyCondition.equal("code",form.getQuestionCode())).optional();
            if (securityQuestion.isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("security question not valid");
            }

            List<SecurityAnswer> securityAnswers = this.dataManager.load(SecurityAnswer.class).condition(LogicalCondition.and(
                            PropertyCondition.equal("securityQuestion",securityQuestion.get()),
                            PropertyCondition.equal("user",user))
                            ).list();

            SecurityAnswer securityAnswer;
            if (!securityAnswers.isEmpty()){
                securityAnswer = securityAnswers.get(0);
            }

            else{
                securityAnswer = this.dataManager.create(SecurityAnswer.class);
                securityAnswer.setUser(user);
                securityAnswer.setSecurityQuestion(securityQuestion.get());
            }
            securityAnswer.setSecurityAnswer(cipherEncryption.encrypt(form.getAnswer(),key));
            this.dataManager.save(securityAnswer);

            return ResponseEntity.status(HttpStatus.OK).body(securityAnswer);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }

    @PutMapping("/securityQuestion")
    public ResponseEntity securityQuestionEdit(@RequestBody SecurityQuestionForm form){
        try {
            logger.info("PUT:/securityQuestion, Request body-> " + form.toString());
            String key = environment.getProperty("secret_key");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            if (userName == null || userName.equals("anonymous")){
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("");
            }

            User user = userService.getUserbyUsername(userName);
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user not exist");

            Optional<SecurityQuestion> securityQuestion = dataManager.load(SecurityQuestion.class)
                    .condition(PropertyCondition.equal("code",form.getQuestionCode())).optional();
            if(securityQuestion.isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("security question not valid");
            }

            Optional<SecurityAnswer> securityAnswers = this.dataManager.load(SecurityAnswer.class)
                    .query("select e from SecurityAnswer e where e.id = :id")
                    .parameter("id",form.getId()).optional();

            if (securityAnswers.isEmpty())
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("invalid id");

            else if(!Objects.equals(securityAnswers.get().getUser().getUsername(), userName))
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("");

            SecurityAnswer securityAnswer = securityAnswers.get();
            securityAnswer.setSecurityQuestion(securityQuestion.get());
//            securityAnswer.setSecurityAnswer(form.getAnswer());
            securityAnswer.setSecurityAnswer(cipherEncryption.encrypt(form.getAnswer(),key));
            this.dataManager.save(securityAnswer);

            return ResponseEntity.status(HttpStatus.OK).body(securityAnswers);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }

    @GetMapping("/recoverUsername")
    // body userID, email
    public ResponseEntity recoverUsername(@RequestBody RecoveryEmailForm form){

        User user;
        logger.info("GET:/recoverUsername, Request body-> " + form.toString());
        try {
            user = this.dataManager.load(User.class).id(form.getUserID()).one();
        }
        catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user not exist or inactive");
        }
        try{
            ResponseEntity response = emailService.sendRecoverUsername(form.getRecoveryEmail(),user.getUsername());
            return response;
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PutMapping("/verifySecurityQuestion")
    public ResponseEntity verifySecurityQuestion(@RequestBody SecurityQuestionForm form){
        try {
            logger.info("PUT:/general/verifySecurityQuestion, Request body-> " + form.toString());
            String key = environment.getProperty("secret_key");
            User user = userService.getUserbyID(form.getUserID());
            if(user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user not exist or inactive");

            SecurityQuestion securityQuestion = dataManager.load(SecurityQuestion.class)
                    .condition(PropertyCondition.equal("code",form.getQuestionCode())).one();

            SecurityAnswer securityAnswer = this.dataManager.load(SecurityAnswer.class).condition(LogicalCondition.and(
                    PropertyCondition.equal("securityQuestion",securityQuestion),
                    PropertyCondition.equal("user",user))
            ).one();

            String answer =cipherEncryption.decrypt(securityAnswer.getSecurityAnswer(),key);
            if(answer != null && answer.equalsIgnoreCase(form.getAnswer()))
                return ResponseEntity.status(HttpStatus.OK).body(true);
            else
                return ResponseEntity.status(HttpStatus.OK).body(false);
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
            //log.error(e.getMessage());
        }
    }

    //get security question for username retrieval and edit
    @GetMapping("/securityQuestion")
    public ResponseEntity getSecurityQuestion(@RequestParam(required = false) String phoneNumber, String email) {
        try{
            logger.info("GET:/verifySecurityQuestion?email=" + email);
            String key = environment.getProperty("secret_key");
            User user;
            String userID = null;
            // get user from email
            if (email != null){
                user = userService.getUserbyEmail(email);
                if(user!=null)
                    userID = user.getId().toString();
            }
            // get user from phone number
            else if(phoneNumber != null){
                // encode the value that might contain "+" symbol
                phoneNumber = URLEncoder.encode(phoneNumber, "UTF-8");
                user = userService.getUserbyPhone(cipherEncryption.encrypt(phoneNumber,key));
                if(user!=null)
                    userID = user.getId().toString();
            }
            else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("user not exist");
            }

            // get list of security question for given user
            ArrayList<TypeFormWithID> questions = new ArrayList<>();
            if(user != null){
                List<SecurityAnswer> securityAnswers = this.dataManager.load(SecurityAnswer.class)
                        .condition(PropertyCondition.equal("user",user)).list();

                for (SecurityAnswer securityAnswer : securityAnswers){
                    TypeFormWithID question = new TypeFormWithID();
                    question.setId(securityAnswer.getId());
                    question.setCode(securityAnswer.getSecurityQuestion().getCode());
                    question.setName(securityAnswer.getSecurityQuestion().getName());
                    question.setDescription(securityAnswer.getSecurityQuestion().getDescription());
                    questions.add(question);
                }
            }
            else {
                List<SecurityQuestion> securityQuestions = this.dataManager.load(SecurityQuestion.class).all().list();
                Random random = new Random();
                int randomIndex = random.nextInt(securityQuestions.size()-1);
                TypeFormWithID question = new TypeFormWithID();
                question.setId(securityQuestions.get(randomIndex).getId());
                question.setCode(securityQuestions.get(randomIndex).getCode());
                question.setName(securityQuestions.get(randomIndex).getName());
                question.setDescription(securityQuestions.get(randomIndex).getDescription());
                questions.add(question);
            }

            UserSecurityQuestions userSecurityQuestions = new UserSecurityQuestions();
            userSecurityQuestions.setUserID(userID);
            userSecurityQuestions.setUserSecurityQuestion(questions);

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .header(HttpHeaders.CACHE_CONTROL, "max-age=31536000")
                    .body(userSecurityQuestions);
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
            //log.error(e.getMessage());
        }

    }

//    @GetMapping("/nationality")
    public ResponseEntity<List<Country>> nationality() {

        List<Country> nationality = dataManager.load(Country.class).all().list();
        return ResponseEntity
                .status(HttpStatus.OK)
                .header(HttpHeaders.CACHE_CONTROL, "max-age=31536000")
                .body(nationality);
    }

    @GetMapping("/country")
    public ResponseEntity countryList() {

        try{
            logger.info("GET:/country");
            // values -> example MY,TH
            // id -> example Malaysia, Thailand
            //List<CountryList> countryLists = Arrays.asList(CountryList.values());
            //List<String> countryListName = countryLists.stream().map(CountryList::getId).collect(Collectors.toList());

            List <CodeForm> countries = new ArrayList<>();

            for (CountryList countryList : CountryList.values()){
                CodeForm country = new CodeForm();
                country.setCode(countryList.name());
                country.setName(countryList.getId());
                countries.add(country);
            }

            return ResponseEntity.status(HttpStatus.OK).body(countries);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("/state")
    public ResponseEntity stateList(@RequestParam(required = false) String countryCode) {
        //countryCode support malaysia only
        //if countryCode not provided, return default state in malaysia
//    public ResponseEntity stateList() {

        try{
            logger.info("GET:/state");
            // get state based on poscode
//            List<String> states = new ArrayList<>();

            List <CodeForm> states = new ArrayList<>();

            if(countryCode != null && !countryCode.equals("") && !Objects.equals(countryCode, CountryList.MY.name())){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("invalid countryCode");
            }
            else {
                for(StateList stateList : StateList.values()){
                    CodeForm state = new CodeForm();
                    state.setCode(stateList.name());
                    state.setName(stateList.getId());
                    states.add(state);
                }
                return ResponseEntity.status(HttpStatus.OK).body(states);
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("/postcodeInfo")
    public ResponseEntity postcodeInfo(@RequestParam(required = false) String postcode, String countryCode) {
        //countryCode support malaysia only
        //if countryCode not provided, return default state in malaysia

        try{

            List <CodeForm> states = new ArrayList<>();
            if(countryCode != null && !countryCode.equals("") && !Objects.equals(countryCode, CountryList.MY.name())){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("invalid countryCode");
            }
            String normalizedPostcode = normalizePostcode(postcode);
            Optional<Postcode> p = this.dataManager.load(Postcode.class)
                    .query("select e from Postcode e where e.postcode = :postcode")
                    .parameter("postcode",postcode).optional();
            if(p.isEmpty()){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("");
            }
            else {
                Postcode data = p.get();
                CodeForm state = new CodeForm();
                if(data.getState()!=null){
                    state.setCode(data.getState().name());
                    state.setName(data.getState().getId());
                }

                CodeForm country = new CodeForm();
                if(data.getCountry()!=null){
                    country.setCode(data.getCountry().name());
                    country.setName(data.getCountry().getId());
                }
                PostcodeInfo postcodeInfo = new PostcodeInfo();
                postcodeInfo.setPostcode(data.getPostcode());
                postcodeInfo.setCity(data.getCity());
                postcodeInfo.setState(state);
                postcodeInfo.setCountry(country);
                return ResponseEntity.status(HttpStatus.OK).body(postcodeInfo);
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/postcode")
    public ResponseEntity insertPostcode(@RequestBody PostcodeForm form) {
        //countryCode support malaysia only
        //if countryCode not provided, return default state in malaysia

        try{

            StateList state;
            if(form.getState().getCode() != null && !Objects.equals(form.getState().getCode(), ""))
                state = StateList.valueOf(form.getState().getCode());
            else
                state = StateList.fromId(form.getState().getName());

            CountryList country;
            if(form.getCountry().getCode() != null && !Objects.equals(form.getCountry().getCode(), ""))
                country = CountryList.valueOf(form.getCountry().getCode());
            else
                country = CountryList.valueOf(form.getCountry().getName());

            List<Postcode> postcodes = this.dataManager.load(Postcode.class)
                    .query("select e from Postcode e where e.postcode in :postcode")
                    .parameter("postcode",form.getPostcode()).list();
            if(postcodes.isEmpty()){
                for (String str : form.getPostcode()){
                    Postcode postcode = this.dataManager.create(Postcode.class);
                    postcode.setPostcode(str);
                    postcode.setCity(form.getCity());
                    postcode.setState(state);
                    postcode.setCountry(country);
                    this.dataManager.save(postcode);
                }
            }
            else {
                for (String str : form.getPostcode()){

                    Optional<Postcode> data = postcodes.stream().filter(f-> f.getPostcode().equals(str)).findFirst();
                    Postcode postcode;
                    if(data.isEmpty()){
                        postcode = this.dataManager.create(Postcode.class);
                    }
                    else {
                        postcode = data.get();
                    }
                    postcode.setPostcode(str);
                    postcode.setCity(form.getCity());
                    postcode.setState(state);
                    postcode.setCountry(country);
                    this.dataManager.save(postcode);
                }

            }
            return ResponseEntity.status(HttpStatus.CREATED).body("created");
        }
        catch (Exception ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    private String normalizePostcode(String postcode) {
        // Implement postcode normalization logic here, if necessary
        return postcode.toUpperCase();
    }
    private String trimPhoneNumber(String phoneNumber){
        return phoneNumber.replaceAll("\\s", "").replaceAll("-","");
    }
}
