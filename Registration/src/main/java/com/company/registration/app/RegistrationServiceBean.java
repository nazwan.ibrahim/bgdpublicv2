package com.company.registration.app;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service("registrationServiceBean")
public class RegistrationServiceBean {

    public String tryService(){
        return "ok";
    }
}