package com.company.registration.app;

import com.company.registration.entity.*;
import com.company.registration.form.*;
import com.company.registration.responseForm.ErrorResponse;
import com.company.registration.services.CipherEncryption;
import com.company.registration.services.KafkaProducer;
import com.company.registration.services.UserService;
import com.company.registration.services.ValidationService;
import io.jmix.core.querycondition.LogicalCondition;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.email.EmailException;
import io.jmix.email.EmailInfo;
import io.jmix.email.Emailer;
import io.jmix.core.DataManager;
import io.jmix.emailtemplates.EmailTemplates;
import io.jmix.emailtemplates.entity.EmailTemplate;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.env.Environment;
import org.springframework.context.event.EventListener;

import java.io.IOException;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.Random;

import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;

@RestController
@RequestMapping("register")
public class RegistrationController {
    static Logger logger = LogManager.getLogger(RegistrationController.class.getName());

    @Autowired
    private DataManager dataManager;
    @Autowired
    private Emailer emailer;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private Environment environment;
    @Inject
    EmailTemplates emailTemplates;
    @Autowired
    private KafkaProducer kafkaProducer;
    @Autowired
    private UserService userService;
    @Autowired
    private CipherEncryption cipherEncryption;
    @Autowired
    private ValidationService validation;
    @Autowired
    private CurrentAuthentication currentAuthentication;

    private List<ActivationStatus> actStatusTypes;
    private List<UserType> userTypes;
    private List<IdentificationType> identificationTypes;
    private List<TierType> tierTypes;
    private List<AcknowledgementType> acknowledgementTypes;

    private static SecretKeySpec secretKey;
    private static byte[] key;
    private static final String ALGORITHM = "AES";

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationStartedEvent.class)
    public void doSomethingAfterStartup() {
        loadEntityTypes();
    }

    public void loadEntityTypes(){
        this.actStatusTypes = this.dataManager.load(ActivationStatus.class).all().list();
        this.userTypes = this.dataManager.load(UserType.class).all().list();
        this.identificationTypes = this.dataManager.load(IdentificationType.class).all().list();
        this.tierTypes = this.dataManager.load(TierType.class).all().list();
        this.acknowledgementTypes = this.dataManager.load(AcknowledgementType.class).all().list();
    }
    private String getDOB(String idNumber){
        String Year = idNumber.substring(0, 2);
        String Month = idNumber.substring(2, 4);
        String Day = idNumber.substring(4, 6);
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        var cutoff = (calendar.get(Calendar.YEAR)) - 2000;
        String dob = (Integer.parseInt(Year) > cutoff ? "19" : "20") + Year + '-' + Month + '-' + Day;
        return dob;
    }
    private String generateReferral(String name, String idNumber){
        String referral = "";

        if (idNumber.isBlank()){
            Random rand = new Random();
            idNumber = String.valueOf(rand.nextInt(999999 - 111111 + 1) + 111111);
        }

        if (name.length() < 5){
            referral =  name.toUpperCase() + idNumber.substring(2,6);
        }
        else
            referral =  name.substring(0,5).toUpperCase() + idNumber.substring(3,8);

        //check with existing
        String queryStr = "select e from User e where e.referral LIKE concat('%', :referral, '%')";
        List<User> users = this.dataManager.load(User.class)
                .query(queryStr)
                .parameter("referral",referral).list();
        if(users.size()!=0)
            referral = referral + users.size();
        return referral;
    }

    //Individual registration
    @PostMapping("/individual")
    public ResponseEntity registerIndividual(@RequestBody IndividualForm user){

        logger.info("POST: /register/individual");
        String secretKey = environment.getProperty("secret_key");
        try{
            /// json response   ///
            JSONObject invalidEmailRes = new JSONObject();
            invalidEmailRes.put("displayMessage","Please enter a valid email address");
            invalidEmailRes.put("message","Invalid email format");

            JSONObject invalidPhoneRes = new JSONObject();
            invalidPhoneRes.put("displayMessage","Please enter a valid phone number");
            invalidPhoneRes.put("message","Invalid mobile number format");

//            var checkUser = this.dataManager.load(User.class)
//                    .condition(PropertyCondition.equal("username",user.getEmail())).list();
            User checkUser = userService.getUserbyUsername(user.getEmail());
            if(checkUser != null){
                logger.info("Username "+user.getEmail()+" already exist");
                ErrorResponse response = setErrResponse(ErrorCode.USERNAME_ALREADY_EXISTS,
                        "User with username "+user.getEmail()+" already exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            }

            // nationality
            if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(user.getNationalityCode())))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Invalid nationality\"}");
            CountryList nationality = CountryList.valueOf(user.getNationalityCode());
            if(nationality==CountryList.US){
                logger.info("US nationality not allowed");
                ErrorResponse response = setErrResponse(ErrorCode.COUNTRY_NOT_ALLOWED, "Nationality not allowed");
                return ResponseEntity.badRequest().body(response);
            }
            // passport issuance country
            if(user.getIssuanceCountry()!=null && !Objects.equals(user.getIssuanceCountry(), "")){
                if (Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(user.getIssuanceCountry()))){
                    ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid issuance country");
                    return ResponseEntity.badRequest().body(response);
                }
            }
            ActivationStatus actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G03")).collect(Collectors.toList()).get(0);
            UserType userType = userTypes.stream().filter(f -> f.getCode().equals("K01")).collect(Collectors.toList()).get(0);
            TierType tierType = tierTypes.stream().filter(f -> f.getCode().equals("T00")).collect(Collectors.toList()).get(0);
            IdentificationType idType;

            if(user.getIdentificationTypeCode() != null && !Objects.equals(user.getIdentificationTypeCode(), "")
                    && user.getIdentificationNumber() != null && !Objects.equals(user.getIdentificationNumber(), ""))
            {
                logger.info("checking identification number");
                List<IdentificationType> idTypes = identificationTypes.stream().filter(f -> f.getCode().equals(user.getIdentificationTypeCode())).collect(Collectors.toList());
                if(idTypes.isEmpty())
                    return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type"));
                else{
                    idType = idTypes.get(0);
                    //only allow Mykad and Mytentera for Malaysian
                    if(nationality == CountryList.MY){
                        switch (idType.getCode()){
                            case "P01":
                            case "P04":
                                break;
                            default:
                                return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid identification type"));
                        }
                    } else if (idType.getCode().equals("P03")){
                        if(user.getIssuanceCountry() == null || user.getIssuanceCountry().equals("") || user.getExpiryDate() == null || user.getExpiryDate().equals("")){
                            logger.info("Passport required issuance country and expiry date");
                            ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid identification type");
                            return ResponseEntity.badRequest().body(response);
                        }
                    }
                }
                //checking ic number
                ValidationStatus validateId = validation.validateIdentification(null,user.getIdentificationNumber(),idType);
                if (validateId == ValidationStatus.IC_EXIST){
                    logger.info("Identification number already exist: -> id number :" + user.getIdentificationNumber());
                    ErrorResponse response = setErrResponse(ErrorCode.IDENTIFICATION_NUMBER_ALREADY_EXISTS,
                            "Identification number "+user.getIdentificationNumber()+" already exist");
                    return ResponseEntity.badRequest().body(response);
                }
            }
            else{
                logger.error("Identification type/number null");
                ErrorResponse response = setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid identification number");
                return ResponseEntity.badRequest().body(response);
            }

            // checking phone number format, and email
            JSONObject validatePhoneNumber = validation.validatePhoneNumber(user.getPhoneNumber());
            if (validatePhoneNumber.get("validate").equals(false))
                return ResponseEntity.badRequest().body(invalidPhoneRes.toString());
            if (!validatePhoneNumber.get("countryCode").equals("60"))
                return ResponseEntity.badRequest().body(invalidPhoneRes.toString());
            if(!validation.validateEmail(user.getEmail()))
                return ResponseEntity.badRequest().body(invalidEmailRes.toString());

            //referral code
            if(user.getReferralCode() != null && !Objects.equals(user.getReferralCode(), "")){
                User referral = userService.getUserbyReferral(user.getReferralCode());
                if (referral == null)
                    return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES, "Invalid referral code"));
            }

            User newuser = this.dataManager.create(User.class);
            newuser.setFull_name(user.getFullName());
            newuser.setUsername(user.getEmail());
            newuser.setUserType(userType);
            newuser.setActStatus(actStatus);
            newuser.setActive(false);
            newuser.setEmail(user.getEmail());
            newuser.setEmail_verified(false);
            newuser.setPhoneNumber(cipherEncryption.encrypt(user.getPhoneNumber(),secretKey));
            newuser.setNationality(nationality);
            newuser.setReferral(generateReferral(user.getFullName(),user.getPhoneNumber()));
            newuser.setIdentificationType(idType);
            newuser.setIdentificationNumber(cipherEncryption.encrypt(user.getIdentificationNumber(), secretKey));
            //for passport
            if(user.getIdentificationTypeCode().equals("P03")){
                newuser.setIdentificationIssuanceCountry(CountryList.valueOf(user.getIssuanceCountry()));
                newuser.setIdentificationExpiryDate(user.getExpiryDate());
            }
            newuser.setReference(user.getReferralCode());
            logger.info("saving new user "+user.getEmail()+" ...");
            // guna unconstrained bila perlu sahaja
            this.dataManager.unconstrained().save(newuser);

            UserTier userTier = this.dataManager.create(UserTier.class);
            userTier.setUser(newuser);
            userTier.setTier(tierType);
            userTier.setCurrentTier(true);
            this.dataManager.save(userTier);

            logger.info("agree on terms and conditions: " + user.getTermsAndConditionsAgreement());
            AcknowledgementType type_01 = acknowledgementTypes.stream().filter(f -> f.getCode().equals("ACK01")).collect(Collectors.toList()).get(0);
            UserAcknowledgement acknowledgement_01 = this.dataManager.create(UserAcknowledgement.class);
            acknowledgement_01.setUser(newuser);
            acknowledgement_01.setAgreementAcknowledgement(type_01);
            acknowledgement_01.setAgreed(user.getTermsAndConditionsAgreement());
            this.dataManager.save(acknowledgement_01);

            logger.info("agree to received marketing promotion: " + user.getReceiveMarketing());
            AcknowledgementType type_02 = acknowledgementTypes.stream().filter(f -> f.getCode().equals("ACK02")).collect(Collectors.toList()).get(0);
            UserAcknowledgement acknowledgement_02 = this.dataManager.create(UserAcknowledgement.class);
            acknowledgement_02.setUser(newuser);
            acknowledgement_02.setAgreementAcknowledgement(type_02);
            acknowledgement_02.setAgreed(user.getReceiveMarketing());
            this.dataManager.save(acknowledgement_02);

            //create user notification permission
            UserPermission permission = this.dataManager.create(UserPermission.class);
            permission.setUser(newuser);
            this.dataManager.save(permission);

            logger.info("New user successfully created. Username="+newuser.getUsername());
            return ResponseEntity.status(HttpStatus.CREATED).body(newuser);
        }catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.internalServerError().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    private ErrorResponse setErrResponse(ErrorCode errorCode, String message){
        ErrorResponse response = new ErrorResponse();
        response.setErrorCode(errorCode.getId());
        response.setMessage(message);
        return response;
    }

    //call waktu registration
    @PostMapping("/emailVerification")
    public ResponseEntity generateEmailVerification(@RequestParam(required = false) UUID userID, String email){
        logger.info("POST: /register/emailVerification");
        try{
            String CREATED_TEMPLATE_CODE = "verification_created";
            String Url = environment.getProperty("emailVerificationUrl");
            Date date = Calendar.getInstance().getTime();
            Date validLink = DateUtils.addDays(date,5);

            //get user
            User user = null;
            if (userID != null){
                logger.info("userID="+userID);
                user = userService.getUserbyID(userID);
            }
            else if (email != null && !Objects.equals(email, "")){
                logger.info("email="+email);
                user = userService.getUserbyEmail(email);
            }

            if (user == null){
                logger.info("api -> POST: /emailVerification. user not exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            }

            if(user.getEmail_verified() != null && user.getEmail_verified()){
                logger.info("Email already verified. user:-> "+user.getUsername());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Email already verified\"}");
            }

            EmailVerification emailVerification = this.dataManager.create(EmailVerification.class);
            emailVerification.setExpiredDatetime(validLink);
            emailVerification.setUser(user);
            emailVerification.setActive(true);
            this.dataManager.save(emailVerification);

            String verificationID = emailVerification.getId().toString();
            String generatedLink =  Url +"?verificationID="+verificationID+"&userID=" +user.getId();
            String userEmail = user.getEmail();
            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(userEmail)
                    .setSubject("Bursa Gold Dinar Account Verification")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("url",generatedLink);
            params.put("email",userEmail);
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email verification sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email verification sent to "+emailInfo.getAddresses()+"\"}");
//                return ResponseEntity.status(HttpStatus.OK).body(emailInfo);
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping ("/emailVerification")
    // both UUID need to be declared with @RequestParam
    public ResponseEntity emailVerified(@RequestParam UUID verificationID, @RequestParam UUID userID){
        logger.info("GET: register/emailVerification. user:-> "+userID.toString());

        String redirectUrl = environment.getProperty("emailVerifiedUrl");
        String notFoundUrl = environment.getProperty("notFoundUrl");
        try{
            Date date = Calendar.getInstance().getTime();
//            User user = getUserbyID(UUID.fromString(userID));
            User user = userService.getUserbyID(userID);
            if(user.getEmail_verified()){
                return ResponseEntity.status(HttpStatus.OK).body("{\"redirectUrl\":\""+redirectUrl+"\"}");
//                return new ModelAndView("redirect:"+redirectedUrl);
//                return new RedirectView(redirectedUrl);
            }
            EmailVerification emailVerification;
            List<EmailVerification> emailVerifications = this.dataManager.load(EmailVerification.class)
                    .condition(LogicalCondition.and(
//                            PropertyCondition.equal("id",UUID.fromString(verificationID)),
                            PropertyCondition.equal("id",verificationID),
                            PropertyCondition.equal("user",user),
//                            PropertyCondition.equal("active",true),
                            PropertyCondition.greaterOrEqual("expiredDatetime",date))).list();
            if(emailVerifications.isEmpty()){
//                return new RedirectView("/registration/register/error");
//                return new ModelAndView("redirect:"+notFoundUrl);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"redirectUrl\":\""+notFoundUrl+"\"}");
            }

            else
                emailVerification=emailVerifications.get(0);
//            User user = this.dataManager.load(User.class).id(UUID.fromString(userID)).one();
            user.setEmail_verified(true);
            this.dataManager.save(user);
            emailVerification.setActive(false);
            this.dataManager.save(emailVerification);

//            model.addAttribute("message","Email has been verified");
//            return new ModelAndView("redirect:"+redirectedUrl,model);

//            redirectAttributes.addAttribute("message", "Your email has been verified");
//            return new RedirectView(redirectedUrl);
            redirectUrl = redirectUrl+"?message=Your email has been verified";
            logger.info("GET: /emailVerification -> response:" + HttpStatus.OK);
            return ResponseEntity.status(HttpStatus.OK).body("{\"redirectUrl\":\""+redirectUrl+"\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"redirectUrl\":\""+notFoundUrl+"\"}");
//            return new RedirectView("/registration/register/error");
//            return new ModelAndView("redirect:/register/error");
        }
    }

    @PutMapping("/deviceID")
    public ResponseEntity registerDeviceID(@RequestBody DeviceIDForm form){
        logger.info("PUT: /register/deviceID");
        try{
            if (form.getDeviceID() == null || form.getUserID() == null){
                String errorMessage = (form.getDeviceID() == null)? "deviceID parameter is null" : "userID parameter is null";
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
            }

            User user = userService.getUserbyID(form.getUserID());
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");

            user.setDeviceID(form.getDeviceID());
            this.dataManager.save(user);

            return ResponseEntity.status(HttpStatus.OK).body(user.getDeviceID());
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/company")
    public ResponseEntity registerCompany(@RequestBody CompanyForm form){

        try{
            // invalid email format
            JSONObject invalidEmailRes = new JSONObject();
            invalidEmailRes.put("displayMessage","Please enter a valid email address");
            invalidEmailRes.put("message","Invalid email format");

            // invalid phone format
            JSONObject invalidPhoneRes = new JSONObject();
            invalidPhoneRes.put("displayMessage","Please enter a valid phone number");
            invalidPhoneRes.put("message","Invalid mobile number format");

            String secretKey = environment.getProperty("secret_key");
            //check user exist
            var checkUser = this.dataManager.load(User.class)
                    .condition(PropertyCondition.equal("username",form.getEmail())).list();
            if(!checkUser.isEmpty()){
                logger.info("User with username "+form.getEmail()+" already exist");
                ErrorResponse response = setErrResponse(ErrorCode.USERNAME_ALREADY_EXISTS,"User with username "+form.getEmail()+" already exist");
                return ResponseEntity.badRequest().body(response);
            }
            // check country with enum list
            if(!Arrays.stream(CountryList.values()).anyMatch(e->e.name().equals(form.getCountryCode()))){
                return ResponseEntity.badRequest().body(setErrResponse(ErrorCode.INVALID_DATA_TYPES,"Invalid nationality"));
            }

            ActivationStatus actStatus = this.dataManager.load(ActivationStatus.class)
                    .condition(PropertyCondition.equal("code","G03")).one();
            CompanyType companyType = this.dataManager.load(CompanyType.class)
                    .condition(PropertyCondition.equal("code",form.getCompanyTypeCode())).one();
            CompanyCategory companyCategory = this.dataManager.load(CompanyCategory.class)
                    .condition(PropertyCondition.equal("code",form.getCompanyCategoryCode())).one();
            UserType userType = userTypes.stream().filter(f -> f.getCode().equals("K02")).collect(Collectors.toList()).get(0);

            // checking phone number format and email

            var validatePhoneNumber = validation.validatePhoneNumber(form.getPhoneNumber());
            if (validatePhoneNumber.equals(false))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidPhoneRes.toString());

            if(!validation.validateEmail(form.getEmail()))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidEmailRes.toString());

            //table user
            User newuser = this.dataManager.create(User.class);
//            newuser.setFull_name(form.getPIC());
//            newuser.setNationality(CountryList.fromId(form.getNationality()));
            newuser.setUsername(form.getEmail());
            newuser.setActStatus(actStatus);
            //newuser.setEmail(emailField.getRawValue());
            newuser.setEmail(form.getEmail());
            newuser.setUserType(userType);
//            newuser.setIdentificationNo(encrypt(form.getIdentificationNumber(), secretKey));
            newuser.setActStatus(actStatus);
            newuser.setActive(false);
            newuser.setEmail(form.getEmail());
            newuser.setEmail_verified(false);
            newuser.setPhoneNumber(cipherEncryption.encrypt(form.getPhoneNumber(),secretKey));
            newuser.setReferral(generateReferral(form.getCompanyName(),form.getRegistrationNumber()));
//            newuser.setReferral(generateReferral(form.getCompanyName(),form.getPhoneNumber()));
            this.dataManager.save(newuser);

            Company newcompany = this.dataManager.create(Company.class);
            newcompany.setCompanyName(form.getCompanyName());
            newcompany.setCompanyFullName(form.getCompanyFullName());
            newcompany.setRegistrationNo(form.getRegistrationNumber());
            newcompany.setCompanyType(companyType);
            newcompany.setCompanyCategory(companyCategory);
            newcompany.setCountryReg(CountryList.valueOf(form.getCountryCode()));
            newcompany.setBusinessNature(form.getBusinessNature());
            newcompany.setDirectorFullName(form.getDirectorFulName());
            newcompany.setDirectorSurname(form.getDirectorSurname());
            this.dataManager.save(newcompany);

            CompanyUser newCompanyUser = this.dataManager.create(CompanyUser.class);
            newCompanyUser.setUser(newuser);
//            newCompanyUser.setName(form.getName);
            newCompanyUser.setCompany(newcompany);
            newCompanyUser.setPosition(form.getPosition());
            this.dataManager.save(newCompanyUser);

            return ResponseEntity.status(HttpStatus.CREATED).body(newuser);
        }catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

//    @PostMapping("/fileUpload")
    public  ResponseEntity fileUpload(@RequestParam MultipartFile file) throws IOException {

        var file_contentType = file.getContentType();
        byte[] sourceBytes = file.getBytes();
        var file_base64 = Base64.getEncoder().encodeToString(sourceBytes);

        return ResponseEntity.status(HttpStatus.OK).body(file_base64);
    }
    @GetMapping("encrypt")
    public ResponseEntity encryptData(@RequestParam String strToEncrypt, String secret){
        var encrypt = cipherEncryption.encrypt(strToEncrypt,secret);
        return ResponseEntity.status(HttpStatus.OK).body(encrypt);
    }

    @GetMapping("decrypt")
    public ResponseEntity decryptData(@RequestParam String strToDecrypt, String secret){
        logger.info("strToDecrypt:" +strToDecrypt);
        var decrypt = cipherEncryption.decrypt(strToDecrypt,secret);
        return ResponseEntity.status(HttpStatus.OK).body(decrypt);
    }

    @GetMapping("/validateReferralCode")
    public ResponseEntity validateReferralCode(@RequestParam String referralCode){
        try{
            JSONObject notExist = new JSONObject();
            notExist.put("message","Referral code " + referralCode + " not valid");
            boolean status = validation.validateReferralCode(referralCode);
            if(status)
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Validated\"}");
            else
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(notExist.toString());
        }
        catch (Exception Ex){
            logger.error(Ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+Ex.getMessage()+"\"}");
        }
    }
    @PutMapping("/permission")
    public ResponseEntity updateNotificationPermission(@RequestBody NotificationPermissionForm form){
        try{
            logger.info("PUT:/registration/permission");
            UserDetails userDetails = currentAuthentication.getUser();
            String userName = userDetails.getUsername();
            logger.info("userName(auth) :->"+userName);
            if (form.getAllowPushNotification() == null && form.getAllowEmailNotification() == null){
                return ResponseEntity.unprocessableEntity().build();
            }

            User user;
            if (form.getUserID() != null){
                user = userService.getUserbyID(form.getUserID());
                logger.info("userName(form parameter) :->"+user.getUsername());
            }
            else{
                user = userService.getUserbyUsername(userName);
            }
            if (user == null)
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            String queryPermission = "select e from UserPermission e where e.user.id = :userID";
            Optional<UserPermission> permissionList = this.dataManager.load(UserPermission.class)
                            .query(queryPermission).parameter("userID",user.getId()).optional();
            UserPermission userPermission;
            userPermission = permissionList.orElseGet(() -> this.dataManager.create(UserPermission.class));

            userPermission.setUser(user);
            if(form.getAllowPushNotification()!=null)
                userPermission.setAllowPushNotification(form.getAllowPushNotification());
            if(form.getAllowEmailNotification()!=null)
                userPermission.setAllowEmailNotification(form.getAllowEmailNotification());
            this.dataManager.save(userPermission);

            return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Permission successfully updated\"}");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
}
