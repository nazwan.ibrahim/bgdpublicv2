package com.company.registration.app;

import com.company.registration.entity.*;
import com.company.registration.form.*;
import com.company.registration.responseForm.ErrorResponse;
import com.company.registration.responseForm.ServiceResponse;
import com.company.registration.services.*;

import io.jmix.core.DataManager;
import io.jmix.core.Sort;
import io.jmix.core.querycondition.LogicalCondition;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.email.EmailException;
import io.jmix.email.EmailInfo;
import io.jmix.email.Emailer;
import io.jmix.emailtemplates.EmailTemplates;
import io.jmix.emailtemplates.entity.EmailTemplate;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

@RestController
@RequestMapping("password")
public class PasswordController {
    static Logger logger = LogManager.getLogger(PasswordController.class.getName());


    @Autowired
    private DataManager dataManager;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private Environment environment;
    @Autowired
    private KafkaProducer kafkaProducer;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private GeneralController generalController;
    @Autowired
    private UserService userService;
    @Autowired
    private CipherEncryption cipherEncryption;
    @Autowired
    private ValidationService validation;
    @Autowired
    private PasswordService passwordService;
    @Autowired
    private EmailService emailService;
    @Autowired
    EmailTemplates emailTemplates;
    @Autowired
    private Emailer emailer;

    private List<ActivationStatus> actStatusTypes;

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationStartedEvent.class)
    public void doSomethingAfterStartup() {
        loadEntityTypes();
    }

    public void loadEntityTypes(){
        this.actStatusTypes = this.dataManager.load(ActivationStatus.class).all().list();
    }


    //for development testing
    @PostMapping("/verifyPassword")
    public ResponseEntity verifyPassword(@RequestParam UUID id, String password){

        User user = userService.getUserbyID(id);
        if (user == null)
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{\"message\":\"User not found\"}");

        boolean checkPassword = passwordEncoder.matches(password, user.getPassword());
        return ResponseEntity.status(HttpStatus.OK).body(checkPassword);
    }

    @PostMapping("/createPassword")
    public ResponseEntity createPassword(@RequestBody PasswordForm form){

        try{
            logger.info("POST: /password/createPassword");
            // weak password
            JSONObject weakPassword = new JSONObject();
            weakPassword.put("displayMessage","Your password is too weak. Use a stronger password");
            weakPassword.put("message","Password is too weak");

            //check password strength
            // comment dulu sbb fe xcater lg error ni
            Boolean passwordOK = validation.validatePassword(form.getPassword());
            if (!passwordOK){
                logger.info("Weak password");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(weakPassword.toString());
            }
            User user = userService.getUserbyID(form.getUserID());
            if (user == null){
                logger.info("User not exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            }
            String oldPassword = user.getPassword();
            //G04 = active
            ActivationStatus actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G04")).collect(Collectors.toList()).get(0);
            String passwordEncoded = passwordEncoder.encode(form.getPassword());

            //update user in keycloak -- need to change from create user to update user password
            String token = getKeyCloakToken();
            Map<String, Object> keycloakReg = createKeyCloakUser(user.getUsername(), user.getEmail(), form.getPassword(), token);
            if (!keycloakReg.get("code").equals(201)) {
                //rollback if fails
                actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G05")).collect(Collectors.toList()).get(0);
                user = this.dataManager.load(User.class).id(form.getUserID()).one();
                user.setPassword(oldPassword);
                user.setActStatus(actStatus);
                user.setActive(false);
                this.dataManager.save(user);

                return ResponseEntity.status(HttpStatus.valueOf((Integer) keycloakReg.get("code"))).body("{\"message\":\""+keycloakReg.get("message")+"\"}");
//                return ResponseEntity.status(HttpStatus.valueOf((Integer) keycloakReg.get("code"))).body(keycloakReg.get("message"));
            }
            user.setPassword(passwordEncoded);
            user.setActStatus(actStatus);
            user.setActive(true);
            user.setSsoUserID(UUID.fromString(keycloakReg.get("ssoUserID").toString()));
            this.dataManager.save(user);

            // save into password management
            PasswordManagement passwordManagement = this.dataManager.create(PasswordManagement.class);
            passwordManagement.setUser(user);
            passwordManagement.setPassword(passwordEncoded);
            this.dataManager.save(passwordManagement);

            return ResponseEntity.status(HttpStatus.CREATED).body(user);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            logger.error(ex.toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PutMapping("/changePassword")
    public ResponseEntity changePassword(@RequestBody ChangePasswordForm form){
        try {
            logger.info("PUT:/password/changePassword");
            // password not match
            JSONObject unmatchPassword = new JSONObject();
            unmatchPassword.put("displayMessage","Password not match");
            unmatchPassword.put("message","Password not match");

            JSONObject unallowedPassword = new JSONObject();
            unallowedPassword.put("displayMessage","New password must be different");
            unallowedPassword.put("message","Password not valid");

            //successfull update
            JSONObject successJson = new JSONObject();
            successJson.put("displayMessage","Password changed successfully");
            successJson.put("message","New password successfully updated");

            // weak password
            JSONObject weakPassword = new JSONObject();
            weakPassword.put("displayMessage","Your password is too weak. Use a stronger password");
            weakPassword.put("message","Password is too weak");

            UserDetails userDetails = currentAuthentication.getUser();
            String username = userDetails.getUsername();
            logger.info("username :"+username);
            User user= userService.getUserbyUsername(username);
            if (user == null)
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("{\"message\":\"User not found\"}");
            boolean checkPassword = passwordEncoder.matches(form.getOldPassword(), user.getPassword());
            if (!checkPassword){
                logger.info("Password not match");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unmatchPassword.toString());
            }
            //check password strength
            Boolean passwordOK = validation.validatePassword(form.getNewPassword());
            if (!passwordOK){
                logger.info("Weak password");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(weakPassword.toString());
            }
            // check with old password
            String queryStrPasswordHistories = "select e from PasswordManagement e where e.user = :user " +
                    "AND e.password is not NULL";
            List<PasswordManagement> passwordHistories = this.dataManager.load(PasswordManagement.class)
                    .query(queryStrPasswordHistories)
                    .parameter("user",user)
                    .sort(Sort.by(Sort.Direction.DESC,"createdDate"))
                    .maxResults(10)
                    .list();
            logger.info("total passwordHistories:"+passwordHistories.size());
            if (passwordHistories.size() > 0){
                boolean[] oldPassword = {false};
                for (PasswordManagement h : passwordHistories){
                    oldPassword[0] = passwordEncoder.matches(form.getNewPassword(), h.getPassword());
                    if (oldPassword[0])
                        break;
                }
                if(oldPassword[0]){
                    logger.info("New password same with old password histories");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unallowedPassword.toString());
                }
            }
            // if there is no password histories, check with current password
            else if (user.getPassword()!= null){
                PasswordManagement oldPassword = this.dataManager.create(PasswordManagement.class);
                oldPassword.setUser(user);
                oldPassword.setPassword(user.getPassword());
                this.dataManager.save(oldPassword);

                if(Objects.equals(form.getOldPassword(), form.getNewPassword())){
                    logger.info("New password same with old password histories");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unallowedPassword.toString());
                }
            }

//            if(!validation.validatePassword(form.getNewPassword()))
//                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"Password is weak\"}");

            String passwordEncoded = passwordEncoder.encode(form.getNewPassword());
            user.setPassword(passwordEncoded);
            this.dataManager.save(user);

            // save into password management
            PasswordManagement passwordManagement = this.dataManager.create(PasswordManagement.class);
            passwordManagement.setUser(user);
            passwordManagement.setPassword(passwordEncoded);
            this.dataManager.save(passwordManagement);

            // get keycloak token
            String token = getKeyCloakToken();
            if (token == null)
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\"User not found\"}");

            // get keycloak user id
            String keycloakId = getKeycloakUserId(user.getUsername(),token);
            // update password in keycloak
            Map<String, Object> changePassword = keycloakUpdatePassword(keycloakId,form.getNewPassword(),token);
            if (!changePassword.get("code").equals(204)) {
                //rollback if fails
                user = this.dataManager.load(User.class).id(user.getId()).one();
                user.setPassword(passwordEncoder.encode(form.getOldPassword()));
                this.dataManager.save(user);
                return ResponseEntity.status(HttpStatus.valueOf((Integer)changePassword.get("code"))).body("{\"message\":\""+changePassword.get("message")+"\"}");
            }
            return ResponseEntity.status(HttpStatus.OK).body(successJson.toString());

        }
        catch (Exception ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }
    @PostMapping("/generateResetPassword")
    public ResponseEntity generateResetPassword(@RequestBody ResetPasswordForm form){
        try{
            Integer totalAttempt = Integer.valueOf(Objects.requireNonNull(environment.getProperty("totalAttemptResetPassword")));
            logger.info("POST:/password/generateResetPassword->");
            logger.info("email(from request) :"+form.getEmail());
            String key = environment.getProperty("secret_key");
            User user = userService.getUserbyEmail(form.getEmail());
            //response for unauthorized user
            ErrorResponse unmatchResponse = new ErrorResponse();
            unmatchResponse.setErrorCode(ErrorCode.INVALID_ACCOUNT.getId());
            unmatchResponse.setMessage("Invalid email or identification number");

            if (user == null){
                // return "wrong answer" tp prevent user testing using different email
                logger.info("User Not Exist");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unmatchResponse);
//                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(unauthorizedResponse.toString());
            }
            logger.info("user :"+user.getUsername());
            //check total attempts
            int failedAttempts = getTotalAttempts(user);
            if ( failedAttempts >= totalAttempt){
                logger.info("Too many attempts");
                ErrorResponse errorResponse = new ErrorResponse();
                errorResponse.setErrorCode(ErrorCode.TOO_MANY_ATTEMPS.getId());
                errorResponse.setMessage("Too many attempts. Please try again in 15 minutes");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
            }
            String encryptedIdNumber = user.getIdentificationNumber();
            String decryptIdNumber="";
            String phoneNumber = "";
            try{
                decryptIdNumber = cipherEncryption.decrypt(user.getIdentificationNumber(),key);
                phoneNumber = cipherEncryption.decrypt(user.getPhoneNumber(),key);
            }
            catch (Exception ex){
                logger.info("Failed to decrypt IdNumber @ phone Number");
                logger.error(ex.getMessage());
            }
            String decryptIdNumberTrim = validation.trimIdNumber(decryptIdNumber);
            String inputIdNumberTrim = validation.trimIdNumber(form.getIdentificationNumber());
            if(!decryptIdNumberTrim.equalsIgnoreCase(inputIdNumberTrim)){
                logger.info("Invalid email or identification number");
                String resetPasswordId =  passwordService.generateResetPasswordId(user, null, ValidationStatus.NOT_MATCH,failedAttempts);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unmatchResponse);
            }
            GenerateOtpForm otpForm = new GenerateOtpForm();
            otpForm.setPhoneNumber(phoneNumber);
            otpForm.setServiceType(OtpServiceType.RESET_PASSWORD);

            JSONObject generateOTP = generalController.generateOTPFunction(otpForm);
            if((int)generateOTP.get("status") != 200){
                int statusCode = (int)generateOTP.get("status");
                return ResponseEntity.status(HttpStatus.valueOf(statusCode)).body("{\"message\":\""+generateOTP.get("message")+"\"}");
            }
            String generateId = passwordService.generateResetPasswordId(user, (Otp) generateOTP.get("otpEntity"), ValidationStatus.MATCH,failedAttempts);
            if (generateId == null){
                logger.error("Failed to generate reset password id for user " + form.getEmail());
            }
            int lenNumber = phoneNumber.length();
            String masked = StringUtils.repeat("*", lenNumber-7);
            String phoneNumberMask = phoneNumber.substring(0,5)+masked+phoneNumber.substring(lenNumber-2,lenNumber);

            logger.info("POST:/password/generateResetPassword -> response:" + HttpStatus.OK);
            JSONObject responseJson = new JSONObject();
            responseJson.put("service_sid",generateOTP.get("service_sid"));
            responseJson.put("phoneNumber",phoneNumberMask);
            return ResponseEntity.status(HttpStatus.OK).body(responseJson.toString());
        }

        catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }

    }
    public Integer getTotalAttempts(User user){
        //check total attempt within 15 minutes
//        LocalDateTime localeDate = LocalDateTime.now().plusMinutes(-15);
        Date date = Calendar.getInstance().getTime();
        Date expiry = DateUtils.addMinutes(date,-15);

        List<ResetPassword> currentResetPasswords = this.dataManager.load(ResetPassword.class)
                .condition(LogicalCondition.and(
                        PropertyCondition.equal("user",user),
                        PropertyCondition.equal("active",true),
                        PropertyCondition.equal("validationStatus",ValidationStatus.NOT_MATCH),
                        PropertyCondition.greaterOrEqual("lastModifiedDate",expiry))).list();
        if(currentResetPasswords.size()==0)
            return 0;

        Integer totalAttempts = currentResetPasswords.stream()
                .sorted(Comparator.comparing(ResetPassword::getCreatedDate).reversed())
                .filter(f -> f.getValidationStatus().equals(ValidationStatus.NOT_MATCH)).collect(Collectors.toList())
                .get(0).getTotalAttempts();
        return totalAttempts;
    }

    @PutMapping("/resetPassword")
    public ResponseEntity resetPassword(@RequestBody ChangePasswordForm form){
        try{
            logger.info("PUT:/password/resetPassword");
            logger.info("reset password id ->  " + form.getId());
            logger.info("TEMPORARY!!! newPassword:"+form.getNewPassword());

            JSONObject unallowedPassword = new JSONObject();
            unallowedPassword.put("displayMessage","New password must be different");
            unallowedPassword.put("message","Password not valid");

            //successfull update
            JSONObject successJson = new JSONObject();
            successJson.put("displayMessage","Password Changed Successfully");
            successJson.put("message","New password successfully updated");

            // weak password
            JSONObject weakPassword = new JSONObject();
            weakPassword.put("displayMessage","Your password is too weak. Use a stronger password");
            weakPassword.put("message","Password is too weak");

            Date date = Calendar.getInstance().getTime();
//            User user = userService.getUserbyID(form.getUserID());
            ResetPassword resetPassword;
            List<ResetPassword> ResetPasswords = this.dataManager.load(ResetPassword.class)
                    .condition(LogicalCondition.and(
                            PropertyCondition.equal("id",form.getId()),
                            PropertyCondition.equal("active",true),
                            PropertyCondition.greaterOrEqual("expiredDatetime",date))).list();
            if(ResetPasswords.isEmpty())
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("");
            else
                resetPassword=ResetPasswords.get(0);

            User user = resetPassword.getUser();
            if(user == null){
                logger.info("User not found");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"User not exist\"}");
            }
            logger.info("/resetPassword::userName(form body) :->"+user.getUsername());
            //check password strength
            Boolean passwordOK = validation.validatePassword(form.getNewPassword());
            if (!passwordOK){
                logger.info("Weak password");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(weakPassword.toString());
            }
            // check with old password
            String queryStrPasswordHistories = "select e from PasswordManagement e where e.user = :user " +
                    "AND e.password is not NULL";
            List<PasswordManagement> passwordHistories = this.dataManager.load(PasswordManagement.class)
                    .query(queryStrPasswordHistories)
                    .parameter("user",user)
                    .sort(Sort.by(Sort.Direction.DESC,"createdDate"))
                    .maxResults(5)
                    .list();
            logger.info("passwordHistories count:"+passwordHistories.size());
            if (passwordHistories.size() > 0){
                boolean[] oldPassword = {false};
                for (PasswordManagement h : passwordHistories){
                    oldPassword[0] = passwordEncoder.matches(form.getNewPassword(), h.getPassword());
                    if (oldPassword[0])
                        break;
                }
                if(oldPassword[0]){
                    logger.info("New password same with old password histories");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unallowedPassword.toString());
                }
            }
            // if there is no password histories, check with current password
            else if (user.getPassword()!= null){
                PasswordManagement oldPassword = this.dataManager.create(PasswordManagement.class);
                oldPassword.setUser(user);
                oldPassword.setPassword(user.getPassword());
                this.dataManager.save(oldPassword);

                if(passwordEncoder.matches(form.getNewPassword(), user.getPassword())){
                    logger.info("New password same with old password");
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(unallowedPassword.toString());
                }
            }
            // update keycloak
            String token = getKeyCloakToken();
            if (token == null)
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("keycloak user not found");
            // get keycloak user id
            String keycloakId = getKeycloakUserId(user.getUsername(),token);

            Map<String, Object> changePassword = keycloakUpdatePassword(keycloakId, form.getNewPassword(), token);
            if (!changePassword.get("code").equals(204)) {
                logger.info("Failed changed password. keycloak response code:"+changePassword.get("code"));
                logger.info("keycloak response message:"+changePassword.get("message"));
                return ResponseEntity.status(HttpStatus.valueOf((Integer)changePassword.get("code"))).body("{\"message\":\""+changePassword.get("message")+"\"}");
            }
            else {
                logger.info("keycloak response message:"+changePassword.get("message"));
                String passwordEncoded = passwordEncoder.encode(form.getNewPassword());
                user.setPassword(passwordEncoded);
                this.dataManager.save(user);
                resetPassword.setActive(false);
                this.dataManager.save(resetPassword);

                // save into password management
                PasswordManagement passwordManagement = this.dataManager.create(PasswordManagement.class);
                passwordManagement.setUser(user);
                passwordManagement.setPassword(passwordEncoded);
                this.dataManager.save(passwordManagement);
            }
            logger.info("Password Changed Successfully");
            GenerateEmailForm emailForm = new GenerateEmailForm();
            emailForm.setSentTo(user.getEmail());
            emailForm.setName(user.getFull_name());
            emailForm.setEmail(user.getEmail());
            emailService.successResetPassword(emailForm);
            return ResponseEntity.status(HttpStatus.OK).body(successJson.toString());
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    private String getKeyCloakToken()
    {
        logger.info("Get keycloak token (admin)");
        String username = environment.getProperty("keycloak_username");
        String password = environment.getProperty("keycloak_password");
        String client_id = environment.getProperty("keycloak_client_id");
        String authUrl = environment.getProperty("keycloak_generate_token_url");

        String formBody = "grant_type=password"
                +"&username="+username
                +"&password="+password
                +"&client_id="+client_id;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");

        okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType,formBody);
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(authUrl)
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        try{
            okhttp3.Response response = client.newCall(request).execute();
            String responseBodyStr = response.body().string();
            //logger.info("responseBodyStr: "+ responseBodyStr);
            //JSONObject bodyJSON = new JSONObject(response.body().string());
            JSONObject bodyJSON = new JSONObject(responseBodyStr);
            String token = bodyJSON.getString("access_token");
            if (response.code()==200)
                return String.valueOf(token);
            else{
                logger.info("failed to generate keycloak token");
                return null;
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return null;
//            return ex.getMessage();
        }
    }

    private String getKeycloakUserId(String username, String AUTH_TOKEN){

//        String realm = "sample";
        String updateuserUrl = environment.getProperty("keycloak_user_url") +"?username="+username;

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType JSON = MediaType.parse("application/json");
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(updateuserUrl)
                .get()
                .addHeader("Authorization", "Bearer "+ AUTH_TOKEN)
                .build();
        try{
            okhttp3.Response response = client.newCall(request).execute();
            var StrBody = response.body().string();
            if (StrBody.startsWith("[") && StrBody.endsWith("]")){
                StrBody = StrBody.substring(1,StrBody.length()-1);
            }
            JSONObject bodyJSON = new JSONObject(StrBody);
            if (response.isSuccessful()){
                return bodyJSON.getString("id");
            }
            else
                return null;
        }
        catch (Exception ex){
            return null;
        }
    }
    private Map<String,Object> keycloakUpdatePassword(String id, String newPassword, String AUTH_TOKEN){

//        String realm = "sample";
        String updateuserUrl = environment.getProperty("keycloak_user_url") +"/"+id+"/reset-password";
//        String unlockuserUrl = environment.getProperty("keycloak_realms_url") +"/attack-detection/brute-force/users/"+id;
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("type","password");
        jsonBody.put("value",newPassword);
        jsonBody.put("temporary",false);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType JSON = MediaType.parse("application/json");
        okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBody.toString(),JSON);
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(updateuserUrl)
                .method("PUT", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer "+ AUTH_TOKEN)
                .build();

//        okhttp3.Request requestUnlocked = new okhttp3.Request.Builder()
//                .url(unlockuserUrl).delete()
//                .addHeader("Authorization", "Bearer "+ AUTH_TOKEN).build();

        Map<String,Object> res = new HashMap<>();
        try{
            //unlocked user
//            okhttp3.Response responseUnlockedUser = client.newCall(requestUnlocked).execute();
//            logger.info("reset keycloak temporary locked response code: "+ responseUnlockedUser.code());
            //update password
            okhttp3.Response response = client.newCall(request).execute();
            res.put("code",response.code());
            res.put("message",response.message());
            logger.info("keycloak reset password response: ");
            logger.info(res);
            return res;
//            return String.valueOf(response.code());
        }
        catch (Exception ex){
            logger.error("Keycloak API error: " + ex.getMessage());
            res.put("code",400);
            res.put("message",ex.getMessage());
            return res;
//            return ex.getMessage();
        }
    }

    private Map<String,Object> createKeyCloakUser( String username, String email, String password, String AUTH_TOKEN)
    {
        logger.info("Create keycloak user with username " + username);
        String createuserUrl = environment.getProperty("keycloak_user_url");
        String getuserUrl = createuserUrl+"?username="+username.replace("@", "%40");

        JSONObject jsonBody = new JSONObject();
        jsonBody.put("username",username);
        jsonBody.put("email",email);

        JSONArray credentialsArray = new JSONArray();
        JSONObject credentials = new JSONObject();
        credentials.put("type","password");
        credentials.put("value",password);
        credentialsArray.put(credentials);
        jsonBody.put("credentials",credentialsArray);
        jsonBody.put("enabled","true");

        JSONObject access = new JSONObject();
        access.put("manageGroupMembership", true);
        access.put("view", true);
        access.put("mapRoles", true);
        access.put("impersonate", false);
        access.put("manage", true);
        jsonBody.put("access",access);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType JSON = MediaType.parse("application/json");

        okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBody.toString(),JSON);
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(createuserUrl)
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer "+ AUTH_TOKEN)
                .build();

        okhttp3.Request requestUser = new okhttp3.Request.Builder()
                .url(getuserUrl).get()
                .addHeader("Authorization", "Bearer "+ AUTH_TOKEN).build();

        Map<String,Object> res = new HashMap<>();
        try{
            okhttp3.Response response = client.newCall(request).execute();
            String ssoUserID = "";
            if(response.code()==201){
                okhttp3.Response responseGetUser = client.newCall(requestUser).execute();
                var StrBody = responseGetUser.body().string();
                if (StrBody.startsWith("[") && StrBody.endsWith("]")){
                    StrBody = StrBody.substring(1,StrBody.length()-1);
                }
                JSONObject bodyJSON = new JSONObject(StrBody);
                ssoUserID = bodyJSON.getString("id");
            }

            res.put("code",response.code());
            res.put("message",response.message());
            res.put("ssoUserID",ssoUserID);
            logger.info(res);
            logger.info(response.toString());
            return res;
        }
        catch (Exception ex){
            logger.error("Keycloak API error: " + ex.getMessage());
            res.put("code",400);
            res.put("message",ex.getMessage());
            res.put("ssoUserID","");
            return res;
        }
    }

    @GetMapping("/test")
    public ResponseEntity test(){
        return ResponseEntity.status(HttpStatus.OK).body("{\"message:\":\"OK\"}");
    }

    @PostMapping("/validatePassword")
    public ResponseEntity validatePassword(@RequestBody PasswordForm form){
        try{
            JSONObject weakPassword = new JSONObject();
            weakPassword.put("message","Password is too weak");
            boolean status = validation.validatePassword(form.getPassword());
            if(status)
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"validated\"}");
            else
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(weakPassword.toString());
        }
        catch (Exception Ex){
            logger.error(Ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+Ex.getMessage()+"\"}");
        }
    }
}
