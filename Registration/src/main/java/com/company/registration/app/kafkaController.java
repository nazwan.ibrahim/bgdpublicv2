package com.company.registration.app;
import com.company.registration.services.KafkaConsumer;
import com.company.registration.services.KafkaProducer;
import com.company.registration.entity.User;

import com.ibm.icu.text.RelativeDateTimeFormatter;
import io.jmix.core.DataManager;
import org.apache.catalina.LifecycleState;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("kafka")
public class kafkaController {

    @Autowired
    private DataManager dataManager;

    @Autowired
    private KafkaConsumer kafkaConsumer;

    private final KafkaProducer kafkaProducer;
    @Autowired
    kafkaController(KafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

    @PostMapping(value = "/publish")
    public ResponseEntity sendMessageToKafkaTopic(@RequestParam("message") String message) {
        // publish to topic "jmix-kafka-reference"
        this.kafkaProducer.sendMessage(message);

        return ResponseEntity.ok("Message Send to Kafka. message:" +message);
    }

    @PostMapping(value = "/publishKafkaJson")
    public ResponseEntity sendMessageJsonToKafkaTopic() {
        User users = this.dataManager.load(User.class).all().one();
        // publish to topic "jmix-kafka-json"
        this.kafkaProducer.sendMessageJson(users);

        return ResponseEntity.ok("OK");
    }

    @GetMapping(value = "/consume")
    public ResponseEntity getKafkaMessage(String message) throws IOException {
//        this.kafkaConsumer.message1
        logger.info("TRY KAFKA CONSUME: "+ this.kafkaConsumer.message1);

        return ResponseEntity.ok(this.kafkaConsumer.message1);
    }

}
