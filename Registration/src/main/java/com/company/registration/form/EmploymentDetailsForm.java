package com.company.registration.form;

import com.company.registration.entity.CountryList;
import com.company.registration.entity.EmploymentStatus;
import com.company.registration.entity.JobPosition;
import com.company.registration.entity.NatureOfBusiness;

public class EmploymentDetailsForm {
    private String employmentStatusCode;
    private String employerName;
    private String businessName;
    private String previousEmployerName;
    private String positionCode;
    private String natureOfBusinessCode;
    private String countryOfTaxResidence;

    public String getEmploymentStatusCode() {
        return employmentStatusCode;
    }

    public void setEmploymentStatusCode(String employmentStatusCode) {
        this.employmentStatusCode = employmentStatusCode;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getPositionCode() {
        return positionCode;
    }

    public void setPositionCode(String positionCode) {
        this.positionCode = positionCode;
    }

    public String getNatureOfBusinessCode() {
        return natureOfBusinessCode;
    }

    public void setNatureOfBusinessCode(String natureOfBusinessCode) {
        this.natureOfBusinessCode = natureOfBusinessCode;
    }

    public String getCountryOfTaxResidence() {
        return countryOfTaxResidence;
    }

    public void setCountryOfTaxResidence(String countryOfTaxResidence) {
        this.countryOfTaxResidence = countryOfTaxResidence;
    }

    public String getPreviousEmployerName() {
        return previousEmployerName;
    }

    public void setPreviousEmployerName(String previousEmployerName) {
        this.previousEmployerName = previousEmployerName;
    }
}
