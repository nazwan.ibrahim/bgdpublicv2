package com.company.registration.form;

import java.util.UUID;

public class DeviceIDForm {
    private UUID userID;
    private String deviceID;

    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }
}
