package com.company.registration.form;

import java.io.Serializable;
import java.util.List;

public class UserDetailsForm implements Serializable {
    private String userName;
    private List role;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List getRole() {
        return role;
    }

    public void setRole(List role) {
        this.role = role;
    }
}
