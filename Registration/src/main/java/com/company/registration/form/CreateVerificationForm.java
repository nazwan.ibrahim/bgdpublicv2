package com.company.registration.form;

import java.util.UUID;

public class CreateVerificationForm {
    private String userName;
//    private UUID userID;
    private String verificationTypeCode;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

//    public UUID getUserID() {
//        return userID;
//    }
//
//    public void setUserID(UUID userID) {
//        this.userID = userID;
//    }

    public String getVerificationTypeCode() {
        return verificationTypeCode;
    }

    public void setVerificationTypeCode(String verificationTypeCode) {
        this.verificationTypeCode = verificationTypeCode;
    }
}
