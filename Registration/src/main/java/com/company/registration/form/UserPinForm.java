package com.company.registration.form;

public class UserPinForm {
    private String userName;    // used for verify pin
    private String email;       // used for verify pin, preferred used username
    private Integer userPin;
    private Integer currentPin;
    private Integer newPin;
    private Integer confirmPin;

    public Integer getCurrentPin() {
        return currentPin;
    }

    public void setCurrentPin(Integer currentPin) {
        this.currentPin = currentPin;
    }

    public Integer getNewPin() {
        return newPin;
    }

    public void setNewPin(Integer newPin) {
        this.newPin = newPin;
    }

    public Integer getConfirmPin() {
        return confirmPin;
    }

    public void setConfirmPin(Integer confirmPin) {
        this.confirmPin = confirmPin;
    }

    public Integer getUserPin() {
        return userPin;
    }

    public void setUserPin(Integer userPin) {
        this.userPin = userPin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
