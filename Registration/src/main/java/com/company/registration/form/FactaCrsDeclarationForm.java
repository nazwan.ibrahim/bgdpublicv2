package com.company.registration.form;

public class FactaCrsDeclarationForm {
    private Boolean notUSCitizen;
    private Boolean notUSGreenCardHolder;
    private Boolean notUSBorn;
    private Boolean malaysiaTaxResidence;

    public Boolean getNotUSCitizen() {
        return notUSCitizen;
    }

    public void setNotUSCitizen(Boolean notUSCitizen) {
        this.notUSCitizen = notUSCitizen;
    }

    public Boolean getNotUSGreenCardHolder() {
        return notUSGreenCardHolder;
    }

    public void setNotUSGreenCardHolder(Boolean notUSGreenCardHolder) {
        this.notUSGreenCardHolder = notUSGreenCardHolder;
    }

    public Boolean getNotUSBorn() {
        return notUSBorn;
    }

    public void setNotUSBorn(Boolean notUSBorn) {
        this.notUSBorn = notUSBorn;
    }

    public Boolean getMalaysiaTaxResidence() {
        return malaysiaTaxResidence;
    }

    public void setMalaysiaTaxResidence(Boolean malaysiaTaxResidence) {
        this.malaysiaTaxResidence = malaysiaTaxResidence;
    }
}
