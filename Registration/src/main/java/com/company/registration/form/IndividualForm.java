package com.company.registration.form;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class IndividualForm {               //which page
    private String nationality;             //create profile
    private String nationalityCode;
    private String fullName;                //create profile
    private String email;                   //setup account
    private String identificationTypeCode;  //create profile
    private String identificationNumber;        //create profile
    private String issuanceCountry;
    private Date expiryDate;
    private String userName;                //setup account
    private String phoneNumber;             //create profile
    private Boolean termsAndConditionsAgreement;
    private Boolean receiveMarketing;
    private String referralCode;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.replaceAll("\\s", "").toLowerCase();
    }

    public String getNationality() {
        return nationality;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }


    public void setUserName(String userName) {
        this.userName = userName.replaceAll("\\s", "").toLowerCase();
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNationalityCode() {
        return nationalityCode;
    }

    public void setNationalityCode(String nationalityCode) {
        this.nationalityCode = nationalityCode;
    }

    public String getIdentificationTypeCode() {
        return identificationTypeCode;
    }

    public void setIdentificationTypeCode(String identificationTypeCode) {
        this.identificationTypeCode = identificationTypeCode;
    }

    public Boolean getTermsAndConditionsAgreement() {
        return termsAndConditionsAgreement;
    }

    public void setTermsAndConditionsAgreement(Boolean termsAndConditionsAgreement) {
        this.termsAndConditionsAgreement = termsAndConditionsAgreement;
    }

    public Boolean getReceiveMarketing() {
        return receiveMarketing;
    }

    public void setReceiveMarketing(Boolean receiveMarketing) {
        this.receiveMarketing = receiveMarketing;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getIssuanceCountry() {
        return issuanceCountry;
    }

    public void setIssuanceCountry(String issuanceCountry) {
        this.issuanceCountry = issuanceCountry;
    }
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}


