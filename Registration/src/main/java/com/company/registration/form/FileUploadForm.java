package com.company.registration.form;

import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;

public class FileUploadForm {
    private String userName;
    private MultipartFile faceImg;
    private MultipartFile documentImgFront;
    private MultipartFile documentImgBack;
    private String identificationNo;
//    private String addressImg;


    public String getUserName() {
        return userName;
    }

    public String getIdentificationNo() {
        return identificationNo;
    }

    public MultipartFile getFaceImg() {
        return faceImg;
    }

    public MultipartFile getDocumentImgFront() {
        return documentImgFront;
    }

    public MultipartFile getDocumentImgBack() {
        return documentImgBack;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setIdentificationNo(String identificationNo) {
        this.identificationNo = identificationNo;
    }

    public void setFaceImg(MultipartFile faceImg) {
        this.faceImg = faceImg;
    }

    public void setDocumentImgFront(MultipartFile documentImgFront) {
        this.documentImgFront = documentImgFront;
    }

    public void setDocumentImgBack(MultipartFile documentImgBack) {
        this.documentImgBack = documentImgBack;
    }
}
