package com.company.registration.form;

public class CadSubmissionResponse {
    private String id;
    private Data data;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    private class Data{
        private String applicationid;
        private String fullname;
        private String idnumber;
        private String gbgref;
        private String ekycurl;

        public String getApplicationid() {
            return applicationid;
        }

        public void setApplicationid(String applicationid) {
            this.applicationid = applicationid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getIdnumber() {
            return idnumber;
        }

        public void setIdnumber(String idnumber) {
            this.idnumber = idnumber;
        }

        public String getGbgref() {
            return gbgref;
        }

        public void setGbgref(String gbgref) {
            this.gbgref = gbgref;
        }

        public String getEkycurl() {
            return ekycurl;
        }

        public void setEkycurl(String ekycurl) {
            this.ekycurl = ekycurl;
        }
    }
}
