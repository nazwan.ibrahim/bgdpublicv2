package com.company.registration.form;

import com.company.registration.entity.OtpServiceType;

public class GenerateOtpForm {
    private String phoneNumber;
    private OtpServiceType serviceType;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public OtpServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(OtpServiceType serviceType) {
        this.serviceType = serviceType;
    }
}
