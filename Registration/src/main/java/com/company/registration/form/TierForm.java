package com.company.registration.form;


import java.util.UUID;

public class TierForm {
    private UUID id;
    private UUID userID;
    private String identificationTypeCode;
    private String identificationNumber;
    private String reference;
    private String tierCode;
    private String kycStatusCode;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTierCode() {
        return tierCode;
    }

    public void setTierCode(String tierCode) {
        this.tierCode = tierCode;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getKycStatusCode() {
        return kycStatusCode;
    }

    public void setKycStatusCode(String kycStatusCode) {
        this.kycStatusCode = kycStatusCode;
    }

    public String getIdentificationTypeCode() {
        return identificationTypeCode;
    }

    public void setIdentificationTypeCode(String identificationTypeCode) {
        this.identificationTypeCode = identificationTypeCode;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }
}
