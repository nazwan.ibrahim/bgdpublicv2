package com.company.registration.form;
import java.util.Date;
public class CreateCadForm {
    private String userName;
    private String fullName;
    private String identificationTypeCode;  //P01,P03,P04
    private String identificationNumber;
    private String nationalityCode;
    private String countryOfTaxResidenceCode;
    private String customerType;       // Individual, Entity, Legal Arragement
    private String customerCategory; //New Customer etc.
    private Date dob;
    private String industryType;    // Agriculture etc
    private String bypassekyc;
    private String id;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdentificationTypeCode() {
        return identificationTypeCode;
    }

    public void setIdentificationTypeCode(String identificationTypeCode) {
        this.identificationTypeCode = identificationTypeCode;
    }

    public String getNationalityCode() {
        return nationalityCode;
    }

    public void setNationalityCode(String nationalityCode) {
        this.nationalityCode = nationalityCode;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getCountryOfTaxResidenceCode() {
        return countryOfTaxResidenceCode;
    }

    public void setCountryOfTaxResidenceCode(String countryOfTaxResidenceCode) {
        this.countryOfTaxResidenceCode = countryOfTaxResidenceCode;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getBypassekyc() {
        return bypassekyc;
    }

    public void setBypassekyc(String bypassekyc) {
        this.bypassekyc = bypassekyc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
