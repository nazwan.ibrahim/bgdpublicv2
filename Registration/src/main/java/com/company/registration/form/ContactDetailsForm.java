package com.company.registration.form;

import java.util.List;

public class ContactDetailsForm {
    private String phoneNumber;
    private String homeNumber;
    private String officeNumber;
    private List<AddressForm> address;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public List<AddressForm> getAddress() {
        return address;
    }

    public void setAddress(List<AddressForm> address) {
        this.address = address;
    }
}
