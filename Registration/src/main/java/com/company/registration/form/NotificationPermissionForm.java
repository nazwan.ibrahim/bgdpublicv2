package com.company.registration.form;

import org.apache.xpath.operations.Bool;

import java.util.UUID;

public class NotificationPermissionForm {
    private UUID userID;
    private Boolean allowPushNotification;
    private Boolean allowEmailNotification;

    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    public Boolean getAllowPushNotification() {
        return allowPushNotification;
    }

    public void setAllowPushNotification(Boolean allowPushNotification) {
        this.allowPushNotification = allowPushNotification;
    }

    public Boolean getAllowEmailNotification() {
        return allowEmailNotification;
    }

    public void setAllowEmailNotification(Boolean allowEmailNotification) {
        this.allowEmailNotification = allowEmailNotification;
    }
}
