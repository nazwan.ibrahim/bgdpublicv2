package com.company.registration.form;

public class CadWebhookForm {
    private String id;
    private String type;
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    private Object labels;
    private String returnUrl;
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public Object getLabels() {
        return labels;
    }

    public void setLabels(Object labels) {
        this.labels = labels;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static class Data{
        private String applicationid;
        private String ekycresult;
        private String fullname;
        private String idnumber;
        private String gbgres;
        private String gbgprofile;
        private String updatedAt;

        public String getApplicationid() {
            return applicationid;
        }

        public void setApplicationid(String applicationid) {
            this.applicationid = applicationid;
        }

        public String getEkycresult() {
            return ekycresult;
        }

        public void setEkycresult(String ekycresult) {
            this.ekycresult = ekycresult;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getIdnumber() {
            return idnumber;
        }

        public void setIdnumber(String idnumber) {
            this.idnumber = idnumber;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getGbgres() {
            return gbgres;
        }

        public void setGbgres(String gbgres) {
            this.gbgres = gbgres;
        }

        public String getGbgprofile() {
            return gbgprofile;
        }

        public void setGbgprofile(String gbgprofile) {
            this.gbgprofile = gbgprofile;
        }
    }
}
