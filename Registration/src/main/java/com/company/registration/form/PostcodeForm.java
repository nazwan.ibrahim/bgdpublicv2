package com.company.registration.form;

import java.util.List;

public class PostcodeForm {
    private List<String> postcode;
    private String city;
    private CodeForm state;
    private CodeForm country;


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public CodeForm getState() {
        return state;
    }

    public void setState(CodeForm state) {
        this.state = state;
    }

    public CodeForm getCountry() {
        return country;
    }

    public void setCountry(CodeForm country) {
        this.country = country;
    }

    public List<String> getPostcode() {
        return postcode;
    }

    public void setPostcode(List<String> postcode) {
        this.postcode = postcode;
    }
}
