package com.company.registration.form;

import java.util.UUID;

public class PasswordForm {
//    private String userName;
    private UUID userID;
    private String password;

//    public String getUserName() {
//        return userName;
//    }


    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }


//    public void setUserName(String userName) {
//        this.userName = userName;
//    }


    public void setPassword(String password) {
        this.password = password;
    }

}
