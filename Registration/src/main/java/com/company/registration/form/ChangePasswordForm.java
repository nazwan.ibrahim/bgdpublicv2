package com.company.registration.form;

import java.util.UUID;

public class ChangePasswordForm {
//    private UUID userID;
    private UUID id;                // for reset password
//    private UUID userID;                // for reset password
    private String oldPassword;
    private String newPassword;
//    private String confirmPassword;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

//    public String getConfirmPassword() {
//        return confirmPassword;
//    }
//
//    public void setConfirmPassword(String confirmPassword) {
//        this.confirmPassword = confirmPassword;
//    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

//    public UUID getUserID() {
//        return userID;
//    }
//
//    public void setUserID(UUID userID) {
//        this.userID = userID;
//    }
}
