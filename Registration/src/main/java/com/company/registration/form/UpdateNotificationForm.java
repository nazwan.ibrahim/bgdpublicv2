package com.company.registration.form;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class UpdateNotificationForm {

    @NotNull
    private UUID notificationId;

    @NotNull
    private String updateStatusCode;

    public UUID getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(UUID notificationId) {
        this.notificationId = notificationId;
    }


    public String getUpdateStatusCode() {
        return updateStatusCode;
    }

    public void setUpdateStatusCode(String updateStatusCode) {
        this.updateStatusCode = updateStatusCode;
    }
}
