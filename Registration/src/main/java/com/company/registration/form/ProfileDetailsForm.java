package com.company.registration.form;

public class ProfileDetailsForm {
    private Boolean periodicReview;
    private PersonalDetailsForm personalDetails;
    private ContactDetailsForm contactDetails;
    private EmploymentDetailsForm employmentDetails;
    private FinancialDetailsForm financialDetails;

    public PersonalDetailsForm getPersonalDetails() {
        return personalDetails;
    }
    public void setPersonalDetails(PersonalDetailsForm personalDetails) {
        this.personalDetails = personalDetails;
    }

    public ContactDetailsForm getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetailsForm contactDetails) {
        this.contactDetails = contactDetails;
    }

    public EmploymentDetailsForm getEmploymentDetails() {
        return employmentDetails;
    }

    public void setEmploymentDetails(EmploymentDetailsForm employmentDetails) {
        this.employmentDetails = employmentDetails;
    }

    public FinancialDetailsForm getFinancialDetails() {
        return financialDetails;
    }

    public void setFinancialDetails(FinancialDetailsForm financialDetails) {
        this.financialDetails = financialDetails;
    }

    public Boolean getPeriodicReview() {
        return periodicReview;
    }

    public void setPeriodicReview(Boolean periodicReview) {
        this.periodicReview = periodicReview;
    }
}
