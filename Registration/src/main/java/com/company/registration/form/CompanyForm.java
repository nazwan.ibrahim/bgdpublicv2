package com.company.registration.form;

import com.company.registration.entity.Nationality;

public class CompanyForm {

    private String countryCode;
    private String email;
    private String phoneNumber;
    private String companyName;
    private String companyFullName;
    private String registrationNumber;
    private String companyTypeCode;
    private String companyCategoryCode;
    private String businessNature;
    private String directorFulName;
    private String directorSurname;
//    private String PIC;
    private String position;
    private String questionCode;
    private String answer;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBusinessNature() {
        return businessNature;
    }

    public void setBusinessNature(String businessNature) {
        this.businessNature = businessNature;
    }

    public String getCompanyFullName() {
        return companyFullName;
    }

    public void setCompanyFullName(String companyFullName) {
        this.companyFullName = companyFullName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyCategoryCode() {
        return companyCategoryCode;
    }

    public void setCompanyCategoryCode(String companyCategoryCode) {
        this.companyCategoryCode = companyCategoryCode;
    }

    public String getCompanyTypeCode() {
        return companyTypeCode;
    }

    public void setCompanyTypeCode(String companyTypeCode) {
        this.companyTypeCode = companyTypeCode;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getDirectorFulName() {
        return directorFulName;
    }

    public void setDirectorFulName(String directorFulName) {
        this.directorFulName = directorFulName;
    }

    public String getDirectorSurname() {
        return directorSurname;
    }

    public void setDirectorSurname(String directorSurname) {
        this.directorSurname = directorSurname;
    }

//    public String getPIC() {
//        return PIC;
//    }
//
//    public void setPIC(String PIC) {
//        this.PIC = PIC;
//    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.replaceAll("\\s", "").toLowerCase();
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
