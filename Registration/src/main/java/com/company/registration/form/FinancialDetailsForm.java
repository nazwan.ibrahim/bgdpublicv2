package com.company.registration.form;

public class FinancialDetailsForm {
    private String transactionPurposeCode;
    private String totalMonthlyTransactionCode;
    private String freqMonthlyTransactionCode;
    private String sourceOfFundCode;
    private String otherSourceOfFund;
    private String sourceOfWealthCode;
    private String otherSourceOfWealth;


    public String getTotalMonthlyTransactionCode() {
        return totalMonthlyTransactionCode;
    }

    public void setTotalMonthlyTransactionCode(String totalMonthlyTransactionCode) {
        this.totalMonthlyTransactionCode = totalMonthlyTransactionCode;
    }

    public String getFreqMonthlyTransactionCode() {
        return freqMonthlyTransactionCode;
    }

    public void setFreqMonthlyTransactionCode(String freqMonthlyTransactionCode) {
        this.freqMonthlyTransactionCode = freqMonthlyTransactionCode;
    }

    public String getSourceOfFundCode() {
        return sourceOfFundCode;
    }

    public void setSourceOfFundCode(String sourceOfFundCode) {
        this.sourceOfFundCode = sourceOfFundCode;
    }

    public String getSourceOfWealthCode() {
        return sourceOfWealthCode;
    }

    public void setSourceOfWealthCode(String sourceOfWealthCode) {
        this.sourceOfWealthCode = sourceOfWealthCode;
    }

    public String getTransactionPurposeCode() {
        return transactionPurposeCode;
    }

    public void setTransactionPurposeCode(String transactionPurposeCode) {
        this.transactionPurposeCode = transactionPurposeCode;
    }

    public String getOtherSourceOfFund() {
        return otherSourceOfFund;
    }

    public void setOtherSourceOfFund(String otherSourceOfFund) {
        this.otherSourceOfFund = otherSourceOfFund;
    }

    public String getOtherSourceOfWealth() {
        return otherSourceOfWealth;
    }

    public void setOtherSourceOfWealth(String otherSourceOfWealth) {
        this.otherSourceOfWealth = otherSourceOfWealth;
    }
}
