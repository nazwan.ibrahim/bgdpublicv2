package com.company.registration.form;

import java.time.LocalDateTime;

public class OtpForm {
    private String resetPasswordId;
    private boolean valid;
    private String displayMessage;
    private LocalDateTime dateCreated;
    private String serviceSid;
    private String phoneNumber;
    private String otp;
    private String status;

    public String getOtp() {
        return otp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getServiceSid() {
        return serviceSid;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setServiceSid(String serviceSid) {
        this.serviceSid = serviceSid;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getDisplayMessage() {
        return displayMessage;
    }

    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getResetPasswordId() {
        return resetPasswordId;
    }

    public void setResetPasswordId(String resetPasswordId) {
        this.resetPasswordId = resetPasswordId;
    }
}
