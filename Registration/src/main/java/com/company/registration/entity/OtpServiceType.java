package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum OtpServiceType implements EnumClass<String> {

    REGISTRATION("registration"),
    RESET_PASSWORD("reset password"),
    UPDATE_BANK("update bank"),
    RESET_PIN("reset pin"),
    CHANGE_PHONE_NUMBER("change phone number");

    private String id;

    OtpServiceType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static OtpServiceType fromId(String id) {
        for (OtpServiceType at : OtpServiceType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}