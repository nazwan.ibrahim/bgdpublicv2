package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum StateList implements EnumClass<String> {

    JHR("Johor"),
    KDH("Kedah"),
    KTN("Kelantan"),
    KUL("Kuala Lumpur"),
    LBN("Labuan"),
    MLK("Melaka"),
    NSN("Negeri Sembilan"),
    PHG("Pahang"),
    PNG("Pulau Pinang"),
    PRK("Perak"),
    PLS("Perlis"),
    PJY("Putrajaya"),
    SBH("Sabah"),
    SWK("Sarawak"),
    SGR("Selangor"),
    TRG("Terengganu");

    private String id;

    StateList(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static StateList fromId(String id) {
        for (StateList at : StateList.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}