package com.company.registration.entity;

import io.jmix.core.HasTimeZone;
import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.annotation.Secret;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.SystemLevel;
import io.jmix.core.metamodel.annotation.DependsOnProperties;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.security.authentication.JmixUserDetails;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Entity
@Table(name = "USER_", indexes = {
        @Index(name = "IDX_USER__ON_USERNAME", columnList = "USERNAME", unique = true),
        @Index(name = "IDX_USER__USER_TYPE", columnList = "USER_TYPE_ID"),
        @Index(name = "IDX_USER__IDENTIFICATION_TYPE", columnList = "IDENTIFICATION_TYPE_ID"),
        @Index(name = "IDX_USER__ACT_STATUS", columnList = "ACT_STATUS_ID")
})
public class User implements JmixUserDetails, HasTimeZone {

    @Id
    @Column(name = "ID", nullable = false)
    @JmixGeneratedValue
    private UUID id;

    @Column(name = "BGD_ID")
    private String bgdId;

    @Column(name = "FULL_NAME")
    private String full_name;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Integer version;

    @Column(name = "USERNAME", nullable = false)
    protected String username;

    @Length(min = 8, max = 20)
    @Secret
    @SystemLevel
    @Column(name = "PASSWORD")
    protected String password;

    @Column(name = "FIRST_NAME")
    protected String firstName;

    @Column(name = "LAST_NAME")
    protected String lastName;

    @Email(message = "{msg://com.company.registration.entity/User.email.validation.Email}")
    @Column(name = "EMAIL")
    protected String email;

    @JoinColumn(name = "ACT_STATUS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ActivationStatus actStatus;

    @Column(name = "ACTIVE")
    protected Boolean active = true;

    @Column(name = "TIME_ZONE_ID")
    protected String timeZoneId;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "HOME_NUMBER")
    private String homeNumber;

    @Column(name = "OFFICE_NUMBER")
    private String officeNumber;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "REFERRAL")
    private String referral;

    @Column(name = "NATIONALITY")
    private String nationality;

    @Column(name = "IDENTIFICATION_NO")
    private String identificationNumber;

    @JoinColumn(name = "IDENTIFICATION_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private IdentificationType identificationType;

    @Column(name = "IDENTIFICATION_ISSUANCE_COUNTRY")
    private String identificationIssuanceCountry;

    @Column(name = "IDENTIFICATION_EXPIRY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date identificationExpiryDate;

    @JoinColumn(name = "USER_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserType userType;

    @Column(name = "SECURITY_PHASE")
    private String security_phase;

    @Column(name = "EMAIL_VERIFIED")
    private Boolean email_verified;

    @Column(name = "PIN_ENABLE", columnDefinition = "boolean default false")
    private Boolean pin_enable = false;

    @Column(name = "PIN")
    private String pin;

    @Column(name = "DEVICE_ID")
    private String deviceID;

    @Column(name = "BIRTHDAY")
    private String birthday;

    @Column(name = "APPROVE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date approveDate;

    @Column(name = "CLOSE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closeDate;

    @Column(name = "NEXT_REVIEW_DATE")
    @Temporal(TemporalType.DATE)
    private Date nextReviewDate;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @Column(name = "SSO_USER_ID")
    private UUID ssoUserID;

    @Transient
    protected Collection<? extends GrantedAuthority> authorities;

    public UUID getSsoUserID() {
        return ssoUserID;
    }

    public void setSsoUserID(UUID ssoUserID) {
        this.ssoUserID = ssoUserID;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthday() {
        return birthday;
    }

    public Date getNextReviewDate() {
        return nextReviewDate;
    }

    public void setNextReviewDate(Date nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Date getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Date approveDate) {
        this.approveDate = approveDate;
    }

    public String getBgdId() {
        return bgdId;
    }

    public void setBgdId(String bgdId) {
        this.bgdId = bgdId;
    }

    public String getSecurity_phase() {
        return security_phase;
    }

    public void setSecurity_phase(String security_phase) {
        this.security_phase = security_phase;
    }

    public Date getIdentificationExpiryDate() {
        return identificationExpiryDate;
    }

    public void setIdentificationExpiryDate(Date identificationExpiryDate) {
        this.identificationExpiryDate = identificationExpiryDate;
    }

    public CountryList getIdentificationIssuanceCountry() {
        return identificationIssuanceCountry == null ? null : CountryList.fromId(identificationIssuanceCountry);
    }

    public void setIdentificationIssuanceCountry(CountryList identificationIssuanceCountry) {
        this.identificationIssuanceCountry = identificationIssuanceCountry == null ? null : identificationIssuanceCountry.getId();
    }

    public String getOfficeNumber() {
        return officeNumber;
    }

    public void setOfficeNumber(String officeNumber) {
        this.officeNumber = officeNumber;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public ActivationStatus getActStatus() {
        return actStatus;
    }

    public void setActStatus(ActivationStatus actStatus) {
        this.actStatus = actStatus;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public Boolean getPin_enable() {
        return pin_enable;
    }

    public void setPin_enable(Boolean pin_enable) {
        this.pin_enable = pin_enable;
    }

    public IdentificationType getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(IdentificationType identificationType) {
        this.identificationType = identificationType;
    }

    public Boolean getEmail_verified() {
        return email_verified;
    }

    public void setEmail_verified(Boolean email_verified) {
        this.email_verified = email_verified;
    }

    public void setNationality(CountryList nationality) {
        this.nationality = nationality == null ? null : nationality.getId();
    }

    public CountryList getNationality() {
        return nationality == null ? null : CountryList.fromId(nationality);
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities != null ? authorities : Collections.emptyList();
    }

    @Override
    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return Boolean.TRUE.equals(active);
    }

    @InstanceName
    @DependsOnProperties({"firstName", "lastName", "username"})
    public String getDisplayName() {
        return String.format("%s %s [%s]", (firstName != null ? firstName : ""),
                (lastName != null ? lastName : ""), username).trim();
    }

    @Override
    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }
}