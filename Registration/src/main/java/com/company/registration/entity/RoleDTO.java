package com.company.registration.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;

@JmixEntity
public class RoleDTO {
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}