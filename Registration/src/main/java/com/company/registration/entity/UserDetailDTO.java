package com.company.registration.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@JmixEntity
public class UserDetailDTO {

    private UUID userID;

    private Boolean pinEnable;

    private String fullName;

    private String userName;

    private String email;

    private String phoneNumber;

    private ActivationStatus actStatus;

    private String reference;

    private String referral;

    private String nationality;

    private String nationalityCode;

    private String country;

    private String countryCode;

    private String identificationNumber;

    @Temporal(TemporalType.TIMESTAMP)
    private Date identificationExpiryDate;

    private TierType userTier;

    private IdentificationType identificationType;

    private UserType userType;

    private Boolean emailVerified;

    private List<RoleDTO> role;

    private String deviceID;

    private String profilePicture;

    private TypeDTO tierStatus;

    private TypeDTO cadAssessmentStatus;

    private TypeDTO verificationType;

    private String dob;

    private Boolean allowPushNotification;

    private Boolean allowEmailNotification;

    private LocalDate nextReviewDate;

    public TypeDTO getVerificationType() {
        return verificationType;
    }

    public void setVerificationType(TypeDTO verificationType) {
        this.verificationType = verificationType;
    }

    public Date getIdentificationExpiryDate() {
        return identificationExpiryDate;
    }

    public void setIdentificationExpiryDate(Date identificationExpiryDate) {
        this.identificationExpiryDate = identificationExpiryDate;
    }

    public void setNextReviewDate(LocalDate nextReviewDate) {
        this.nextReviewDate = nextReviewDate;
    }

    public LocalDate getNextReviewDate() {
        return nextReviewDate;
    }

    public Boolean getAllowEmailNotification() {
        return allowEmailNotification;
    }

    public void setAllowEmailNotification(Boolean allowEmailNotification) {
        this.allowEmailNotification = allowEmailNotification;
    }

    public Boolean getAllowPushNotification() {
        return allowPushNotification;
    }

    public void setAllowPushNotification(Boolean allowPushNotification) {
        this.allowPushNotification = allowPushNotification;
    }

    public TypeDTO getCadAssessmentStatus() {
        return cadAssessmentStatus;
    }

    public void setCadAssessmentStatus(TypeDTO cadAssessmentStatus) {
        this.cadAssessmentStatus = cadAssessmentStatus;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNationalityCode() {
        return nationalityCode;
    }

    public void setNationalityCode(String nationalityCode) {
        this.nationalityCode = nationalityCode;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public TypeDTO getTierStatus() {
        return tierStatus;
    }

    public void setTierStatus(TypeDTO tierStatus) {
        this.tierStatus = tierStatus;
    }


    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public void setPinEnable(Boolean pinEnable) {
        this.pinEnable = pinEnable;
    }

    public Boolean getPinEnable() {
        return pinEnable;
    }

    public List<RoleDTO> getRole() {
        return role;
    }

    public void setRole(List<RoleDTO> role) {
        this.role = role;
    }

//    private List role;

    public void setIdentificationType(IdentificationType identificationType) {
        this.identificationType = identificationType;
    }

    public IdentificationType getIdentificationType() {
        return identificationType;
    }

    public void setActStatus(ActivationStatus actStatus) {
        this.actStatus = actStatus;
    }

    public ActivationStatus getActStatus() {
        return actStatus;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public UserType getUserType() {
        return userType;
    }

    public Boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(Boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public UUID getUserID() {
        return userID;
    }

    public void setUserID(UUID userID) {
        this.userID = userID;
    }

    public TierType getUserTier() {
        return userTier;
    }

    public void setUserTier(TierType userTier) {
        this.userTier = userTier;
    }


//    public List getRole() {
//        return role;
//    }
//
//    public void setRole(List role) {
//        this.role = role;
//    }
}