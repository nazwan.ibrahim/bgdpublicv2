package com.company.registration.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "STATUTORY_DETAILS", indexes = {
        @Index(name = "IDX_STATUTORY_DETAILS_USER", columnList = "USER_ID"),
        @Index(name = "IDX_STATUTORYDET_TRANSACTIONP", columnList = "TRANSACTION_PURPOSE_ID"),
        @Index(name = "IDX_STATUTORYDET_TOTALMONTHLY", columnList = "TOTAL_MONTHLY_TRANSACTION_ID"),
        @Index(name = "IDX_STATUTORYDET_FREQMONTHLYT", columnList = "FREQ_MONTHLY_TRANSACTION_ID")
})
@Entity
public class StatutoryDetails {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @NotNull
    @JoinColumn(name = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @JoinColumn(name = "TRANSACTION_PURPOSE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionPurposeType transactionPurpose;

    @JoinColumn(name = "TOTAL_MONTHLY_TRANSACTION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TotalMonthlyTransaction totalMonthlyTransaction;

    @JoinColumn(name = "FREQ_MONTHLY_TRANSACTION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private FreqMonthlyTransaction freqMonthlyTransaction;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public FreqMonthlyTransaction getFreqMonthlyTransaction() {
        return freqMonthlyTransaction;
    }

    public void setFreqMonthlyTransaction(FreqMonthlyTransaction freqMonthlyTransaction) {
        this.freqMonthlyTransaction = freqMonthlyTransaction;
    }

    public TotalMonthlyTransaction getTotalMonthlyTransaction() {
        return totalMonthlyTransaction;
    }

    public void setTotalMonthlyTransaction(TotalMonthlyTransaction totalMonthlyTransaction) {
        this.totalMonthlyTransaction = totalMonthlyTransaction;
    }

    public TransactionPurposeType getTransactionPurpose() {
        return transactionPurpose;
    }

    public void setTransactionPurpose(TransactionPurposeType transactionPurpose) {
        this.transactionPurpose = transactionPurpose;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}