package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum KYC implements EnumClass<String> {

    NULL("NULL"),
    PASS("PASS"),
    FAIL("FAIL");

    private String id;

    KYC(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static KYC fromId(String id) {
        for (KYC at : KYC.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}