package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum SuccessCode implements EnumClass<String> {

    OK("RCS001"),
    CREATED("RCS002"),
    UPDATED("RCS003");

    private String id;

    SuccessCode(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static SuccessCode fromId(String id) {
        for (SuccessCode at : SuccessCode.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}