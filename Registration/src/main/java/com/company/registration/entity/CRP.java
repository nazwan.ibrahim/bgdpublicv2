package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum CRP implements EnumClass<String> {

    NULL("NULL"),
    LOW_RISK("LOW"),
    MEDIUM_RISK("MEDIUM"),
    HIGH_RISK("HIGH");

    private String id;

    CRP(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static CRP fromId(String id) {
        for (CRP at : CRP.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}