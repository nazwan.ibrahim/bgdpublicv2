package com.company.registration.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "FINANCIAL_SOURCES", indexes = {
        @Index(name = "IDX_FINANCIAL_SOURCES_USER", columnList = "USER_ID"),
        @Index(name = "IDX_FINANCIALSOU_SOURCEOFWEAL", columnList = "SOURCE_OF_WEALTH_ID"),
        @Index(name = "IDX_FINANCIALSOUR_SOURCEOFFUND", columnList = "SOURCE_OF_FUND_ID")
})
@Entity
public class FinancialSources {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @NotNull
    @JoinColumn(name = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @JoinColumn(name = "SOURCE_OF_FUND_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SourceOfFundType sourceOfFund;

    @Column(name = "OTHER_SOURCE_OF_FUND")
    private String otherSourceOfFund;

    @JoinColumn(name = "SOURCE_OF_WEALTH_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SourceOfWealthType sourceOfWealth;

    @Column(name = "OTHER_SOURCE_OF_WEALTH")
    private String otherSourceOfWealth;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public String getOtherSourceOfWealth() {
        return otherSourceOfWealth;
    }

    public void setOtherSourceOfWealth(String otherSourceOfWealth) {
        this.otherSourceOfWealth = otherSourceOfWealth;
    }

    public String getOtherSourceOfFund() {
        return otherSourceOfFund;
    }

    public void setOtherSourceOfFund(String otherSourceOfFund) {
        this.otherSourceOfFund = otherSourceOfFund;
    }

    public SourceOfFundType getSourceOfFund() {
        return sourceOfFund;
    }

    public void setSourceOfFund(SourceOfFundType sourceOfFund) {
        this.sourceOfFund = sourceOfFund;
    }

    public SourceOfWealthType getSourceOfWealth() {
        return sourceOfWealth;
    }

    public void setSourceOfWealth(SourceOfWealthType sourceOfWealth) {
        this.sourceOfWealth = sourceOfWealth;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}