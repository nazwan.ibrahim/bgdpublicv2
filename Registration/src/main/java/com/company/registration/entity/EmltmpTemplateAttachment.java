package com.company.registration.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DdlGeneration;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Table(name = "emltmp_template_attachment")
@Entity
public class EmltmpTemplateAttachment {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "content_file", nullable = false)
    @Lob
    private String contentFile;

    @Column(name = "create_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTs;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "delete_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @JoinColumn(name = "email_template_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private EmltmpEmailTemplate emailTemplate;

    @InstanceName
    @Column(name = "name", nullable = false, length = 500)
    private String name;

    @Column(name = "update_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "version", nullable = false)
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdateTs() {
        return updateTs;
    }

    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmltmpEmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(EmltmpEmailTemplate emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeleteTs() {
        return deleteTs;
    }

    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public String getContentFile() {
        return contentFile;
    }

    public void setContentFile(String contentFile) {
        this.contentFile = contentFile;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}