package com.company.registration.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DdlGeneration;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Table(name = "emltmp_email_template")
@Entity
public class EmltmpEmailTemplate {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "bcc")
    @Lob
    private String bcc;

    @Column(name = "cc")
    @Lob
    private String cc;

    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Column(name = "create_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTs;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "delete_ts", unique = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @Column(name = "dtype", length = 100)
    private String dtype;

    @JoinColumn(name = "email_body_report_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private EmltmpTemplateReport emailBodyReport;

    @Column(name = "from_")
    private String from;

    @JoinColumn(name = "group_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private EmltmpTemplateGroup group;

    @Column(name = "html")
    @Lob
    private String html;

    @InstanceName
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "report_json")
    @Lob
    private String reportJson;

    @Column(name = "subject")
    private String subject;

    @Column(name = "to_")
    @Lob
    private String to;

    @Column(name = "type_", nullable = false, length = 50)
    private String type;

    @Column(name = "update_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "use_report_subject")
    private Boolean useReportSubject;

    @Column(name = "version", nullable = false)
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Boolean getUseReportSubject() {
        return useReportSubject;
    }

    public void setUseReportSubject(Boolean useReportSubject) {
        this.useReportSubject = useReportSubject;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdateTs() {
        return updateTs;
    }

    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReportJson() {
        return reportJson;
    }

    public void setReportJson(String reportJson) {
        this.reportJson = reportJson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public EmltmpTemplateGroup getGroup() {
        return group;
    }

    public void setGroup(EmltmpTemplateGroup group) {
        this.group = group;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public EmltmpTemplateReport getEmailBodyReport() {
        return emailBodyReport;
    }

    public void setEmailBodyReport(EmltmpTemplateReport emailBodyReport) {
        this.emailBodyReport = emailBodyReport;
    }

    public String getDtype() {
        return dtype;
    }

    public void setDtype(String dtype) {
        this.dtype = dtype;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeleteTs() {
        return deleteTs;
    }

    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}