package com.company.registration.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DdlGeneration;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Table(name = "emltmp_parameter_value")
@Entity
public class EmltmpParameterValue {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "alias", nullable = false)
    private String alias;

    @Column(name = "create_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createTs;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "default_value")
    private String defaultValue;

    @Column(name = "delete_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deleteTs;

    @Column(name = "deleted_by", length = 50)
    private String deletedBy;

    @Column(name = "parameter_type", nullable = false)
    private Integer parameterType;

    @JoinColumn(name = "template_report_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private EmltmpTemplateReport templateReport;

    @Column(name = "update_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTs;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    @Column(name = "version", nullable = false)
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdateTs() {
        return updateTs;
    }

    public void setUpdateTs(Date updateTs) {
        this.updateTs = updateTs;
    }

    public EmltmpTemplateReport getTemplateReport() {
        return templateReport;
    }

    public void setTemplateReport(EmltmpTemplateReport templateReport) {
        this.templateReport = templateReport;
    }

    public Integer getParameterType() {
        return parameterType;
    }

    public void setParameterType(Integer parameterType) {
        this.parameterType = parameterType;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeleteTs() {
        return deleteTs;
    }

    public void setDeleteTs(Date deleteTs) {
        this.deleteTs = deleteTs;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreateTs() {
        return createTs;
    }

    public void setCreateTs(Date createTs) {
        this.createTs = createTs;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}