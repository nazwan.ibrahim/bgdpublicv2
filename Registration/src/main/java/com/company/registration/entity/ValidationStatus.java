package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum ValidationStatus implements EnumClass<String> {

    IC_EXIST("Ic Number Exist"),
    OK("Validation OK"),
    NOT_MATCH("Not Match"),
    MATCH("Match");

    private String id;

    ValidationStatus(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ValidationStatus fromId(String id) {
        for (ValidationStatus at : ValidationStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}