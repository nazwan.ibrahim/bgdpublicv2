package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum NsHit implements EnumClass<String> {

    NULL("NULL"),
    HIT("HIT"),
    NO_HIT("NO_HIT");

    private String id;

    NsHit(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static NsHit fromId(String id) {
        for (NsHit at : NsHit.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}