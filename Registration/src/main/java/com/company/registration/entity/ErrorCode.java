package com.company.registration.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum ErrorCode implements EnumClass<String> {

    INVALID_ACCOUNT("RCE003"),
    DATA_NOT_FOUND("RCE004"),
    INVALID_DATA_TYPES("RCE005"),
    COUNTRY_NOT_ALLOWED("RCE006"),
    ACCOUNT_SUSPENDED("RCE007"),
    INTEGRATION_ERROR("RCE028"),
    TOO_MANY_ATTEMPS("RCE029"),
    INVALID_REQUEST("RCE030"),
    USERNAME_ALREADY_EXISTS("RCE110"),
    IDENTIFICATION_NUMBER_ALREADY_EXISTS("RCE111"),
    PERMISSION_NOT_ALLOW("RCE112"),
    MISCELLANEOUS("RCE999");

    private String id;

    ErrorCode(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ErrorCode fromId(String id) {
        for (ErrorCode at : ErrorCode.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}