package com.company.registration.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "USER_EMPLOYMENT", indexes = {
        @Index(name = "IDX_USER_EMPLOYMENT_USER", columnList = "USER_ID"),
        @Index(name = "IDX_USEREMPLOYME_EMPLOYMENTST", columnList = "EMPLOYMENT_STATUS_ID"),
        @Index(name = "IDX_USER_EMPLOYMENT_POSITION", columnList = "POSITION_ID"),
        @Index(name = "IDX_USEREMPLOYME_NATUREOFBUSI", columnList = "NATURE_OF_BUSINESS_ID")
})
@Entity
public class UserEmployment {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @JoinColumn(name = "EMPLOYMENT_STATUS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private EmploymentStatus employmentStatus;

    @Column(name = "EMPLOYER_NAME")
    private String employerName;

    @Column(name = "BUSINESS_NAME")
    private String businessName;

    @Column(name = "PREVIOUS_EMPLOYER_NAME")
    private String previousEmployerName;

    @JoinColumn(name = "POSITION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private JobPosition position;

    @JoinColumn(name = "NATURE_OF_BUSINESS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private NatureOfBusiness natureOfBusiness;

    @Column(name = "COUNTRY_OFTAX_RESIDENCE")
    private String countryOftaxResidence;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public String getPreviousEmployerName() {
        return previousEmployerName;
    }

    public void setPreviousEmployerName(String previousEmployerName) {
        this.previousEmployerName = previousEmployerName;
    }

    public CountryList getCountryOftaxResidence() {
        return countryOftaxResidence == null ? null : CountryList.fromId(countryOftaxResidence);
    }

    public void setCountryOftaxResidence(CountryList countryOftaxResidence) {
        this.countryOftaxResidence = countryOftaxResidence == null ? null : countryOftaxResidence.getId();
    }

    public NatureOfBusiness getNatureOfBusiness() {
        return natureOfBusiness;
    }

    public void setNatureOfBusiness(NatureOfBusiness natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    public JobPosition getPosition() {
        return position;
    }

    public void setPosition(JobPosition position) {
        this.position = position;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public EmploymentStatus getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(EmploymentStatus employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}