package com.company.registration.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "USER_VERIFICATION", indexes = {
        @Index(name = "IDX_USER_VERIFICATION_USER", columnList = "USER_ID"),
        @Index(name = "IDX_USERVERIFICA_ASSESSMENTST", columnList = "ASSESSMENT_STATUS_ID"),
        @Index(name = "IDX_USERVERIFICATION_NSRESULT", columnList = "NS_RESULT_ID"),
        @Index(name = "IDX_USERVERIFICATION_CRPRESULT", columnList = "CRP_RESULT_ID"),
        @Index(name = "IDX_USERVERIFICATION_KYCRESULT", columnList = "KYC_RESULT_ID"),
        @Index(name = "IDX_USERVERIFICA_OVERALVERIFI", columnList = "OVERAL_VERIFICATION_RESULT_ID"),
        @Index(name = "IDX_USERVERIFICA_VERIFICATION", columnList = "VERIFICATION_TYPE_ID")
})
@Entity
public class UserVerification {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "CAD_ID")
    private UUID cadId;

    @Column(name = "CAD_APPLICATION_ID")
    private Integer cadApplicationId;

    @Column(name = "GBG_REF")
    private String gbgRef;

    @JoinColumn(name = "KYC_RESULT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private KycStatus kycResult;

    @JoinColumn(name = "NS_RESULT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private NameScreeningResult nsResult;

    @JoinColumn(name = "CRP_RESULT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private CrpResult crpResult;

    @JoinColumn(name = "OVERAL_VERIFICATION_RESULT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private OveralVerificationResult overalVerificationResult;

    @Column(name = "EKYC_URL")
    @Lob
    private String ekycUrl;

    @JoinColumn(name = "ASSESSMENT_STATUS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private AssessmentStatus assessmentStatus;

    @JoinColumn(name = "VERIFICATION_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private VerificationType verificationType;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public VerificationType getVerificationType() {
        return verificationType;
    }

    public void setVerificationType(VerificationType verificationType) {
        this.verificationType = verificationType;
    }

    public OveralVerificationResult getOveralVerificationResult() {
        return overalVerificationResult;
    }

    public void setOveralVerificationResult(OveralVerificationResult overalVerificationResult) {
        this.overalVerificationResult = overalVerificationResult;
    }

    public KycStatus getKycResult() {
        return kycResult;
    }

    public void setKycResult(KycStatus kycResult) {
        this.kycResult = kycResult;
    }

    public String getGbgRef() {
        return gbgRef;
    }

    public void setGbgRef(String gbgRef) {
        this.gbgRef = gbgRef;
    }

    public Integer getCadApplicationId() {
        return cadApplicationId;
    }

    public void setCadApplicationId(Integer cadApplicationId) {
        this.cadApplicationId = cadApplicationId;
    }

    public CrpResult getCrpResult() {
        return crpResult;
    }

    public void setCrpResult(CrpResult crpResult) {
        this.crpResult = crpResult;
    }

    public NameScreeningResult getNsResult() {
        return nsResult;
    }

    public void setNsResult(NameScreeningResult nsResult) {
        this.nsResult = nsResult;
    }

    public AssessmentStatus getAssessmentStatus() {
        return assessmentStatus;
    }

    public void setAssessmentStatus(AssessmentStatus assessmentStatus) {
        this.assessmentStatus = assessmentStatus;
    }

    public String getEkycUrl() {
        return ekycUrl;
    }

    public void setEkycUrl(String ekycUrl) {
        this.ekycUrl = ekycUrl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UUID getCadId() {
        return cadId;
    }

    public void setCadId(UUID cadId) {
        this.cadId = cadId;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}