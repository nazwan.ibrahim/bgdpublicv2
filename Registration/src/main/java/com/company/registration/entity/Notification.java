package com.company.registration.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "NOTIFICATION", indexes = {
        @Index(name = "IDX_NOTIFICATION_USER_ID", columnList = "USER_ID_ID"),
        @Index(name = "IDX_NOTIFICATION_CATEGORY_ID", columnList = "CATEGORY_ID_ID"),
        @Index(name = "IDX_NOTIFICATION_STATUS_ID", columnList = "STATUS_ID_ID"),
        @Index(name = "IDX_NOTIFICATION_DETAIL_ID", columnList = "DETAIL_ID_ID")
})
@Entity
public class Notification {
    @InstanceName
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "USER_ID_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;

    @JoinColumn(name = "CATEGORY_ID_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private NotificationCategory categoryId;

    @JoinColumn(name = "STATUS_ID_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private NotificationStatus statusId;

    @JoinColumn(name = "DETAIL_ID_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private NotificationDetails detailId;

    @Column(name = "SEND_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendDateTime;

    @Column(name = "READ_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date readDateTime;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private LocalDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public Date getReadDateTime() {
        return readDateTime;
    }

    public void setReadDateTime(Date readDateTime) {
        this.readDateTime = readDateTime;
    }

    public Date getSendDateTime() {
        return sendDateTime;
    }

    public void setSendDateTime(Date sendDateTime) {
        this.sendDateTime = sendDateTime;
    }

    public NotificationDetails getDetailId() {
        return detailId;
    }

    public void setDetailId(NotificationDetails detailId) {
        this.detailId = detailId;
    }

    public NotificationStatus getStatusId() {
        return statusId;
    }

    public void setStatusId(NotificationStatus statusId) {
        this.statusId = statusId;
    }

    public NotificationCategory getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(NotificationCategory categoryId) {
        this.categoryId = categoryId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}