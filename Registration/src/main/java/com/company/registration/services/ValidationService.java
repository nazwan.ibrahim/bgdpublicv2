package com.company.registration.services;

import com.company.registration.entity.IdentificationType;
import com.company.registration.entity.PasswordDictionary;
import com.company.registration.entity.User;
import com.company.registration.entity.ValidationStatus;
import io.jmix.core.DataManager;
import io.jmix.core.entity.KeyValueEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service("validate")
public class ValidationService {
    static Logger logger = LogManager.getLogger(ValidationService.class.getName());

    @Autowired
    private DataManager dataManager;
    @Autowired
    private Environment environment;
    @Autowired
    private CipherEncryption cipherEncryption;

    public JSONObject validatePhoneNumber(String phoneNumber){
        JSONObject json = new JSONObject();
        if(phoneNumber.startsWith("+")){
            String countryCode = phoneNumber.substring(1,Math.min(3, phoneNumber.length()));
            Boolean validate = phoneNumber.matches("^\\+?[0-9]{10,}$");
            json.put("validate",validate);
            json.put("countryCode",countryCode);
        }
        else {
            json.put("validate", false);
            json.put("countryCode","");
        }
        return json;
    }
    public Boolean validateEmail(String email){
        if(email != null && !email.equals("")){
            String emailRegex = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$";
            return email.matches(emailRegex);
        }
        return false;
    }
    public Boolean validatePassword(String password){
        if(password != null && !password.equals("")){
            String passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@*#$!%^?&+=])(?=\\S+$).{8,}$";
            Boolean passwordOK = password.matches(passwordRegex);

            if(!passwordOK){
                logger.info("Password not contains all required characters");
                return false;
            }
            String query = "select e from PasswordDictionary e where :password like concat('%',lower(e.word), '%') and e.length > 3";
            Optional<PasswordDictionary> dictionary = this.dataManager.load(PasswordDictionary.class)
                    .query(query)
                    .parameter("password",password.toLowerCase())
                    .optional();
            if(dictionary.isPresent()){
                logger.info("Password contains word from dictionaries");
                return false;
            }
            return true;
        }
        return false;
    }
    public String trimIdNumber(String idNumber){
        return idNumber.replaceAll("\\s", "").replaceAll("-","");
    }
    public ValidationStatus validateIdentification(UUID userID, String identificationNumber, IdentificationType identificationType){
        String key = environment.getProperty("secret_key");
        long start = System.currentTimeMillis();
        String query = "select e.identificationNumber from User e where e.identificationNumber is not null and e.id <> :id";
        if(userID == null){
            query = "select e.identificationNumber from User e where e.identificationNumber is not null";
        }
        List<KeyValueEntity> kvEntities  = this.dataManager.loadValues(query)
                .properties("identificationNumber")
//                .parameter("identificationType",identificationType)
                .parameter("id",userID)
                .list();
        long end = System.currentTimeMillis();
        long time = end - start;
        logger.info("loadValues(miliseconds):"+time);

        if (kvEntities .size()>0){
            logger.info("Checking Identification Number: -> id number :" + identificationNumber);
            for (KeyValueEntity kvEntity : kvEntities ){
                if((identificationNumber.equals(cipherEncryption.decrypt(kvEntity.getValue("identificationNumber"), key)))){
                    logger.info("identification Number " + identificationNumber + " already exist");
                    return ValidationStatus.IC_EXIST;
                }
            }
        }
        logger.info("identificationNumber validation passed");
        return ValidationStatus.OK;
    }

    public Boolean validateReferralCode(String referralCode){

        if(referralCode != null && !referralCode.equals("")){
            String query = "select e from User e where lower(:referral) = :referral";
            Optional<User> dictionary = this.dataManager.load(User.class)
                    .query(query)
                    .parameter("referral",referralCode.toLowerCase())
                    .optional();
            if(dictionary.isEmpty()){
                logger.info("Referral code " + referralCode + " not valid");
                return false;
            }
            return true;
        }
        return false;
    }
}