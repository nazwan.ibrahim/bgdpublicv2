package com.company.registration.services;

import com.company.registration.app.*;
import io.jmix.core.EntitySerialization;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class KafkaConsumer {

    @Autowired
    private EntitySerialization entitySerialization;
    private final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    public String message1;

//    @KafkaListener(topics = "jmix-kafka-reference", groupId = "group_id")
//    public void consume(String message) throws IOException {
//        try {
//            logger.info(String.format("#### -> Consumed message -> %s", message));
//            message1 = message;
//        } catch (Exception ex) {
//            logger.error("TestConsume "+ex.getMessage());
//        }
//    }
//
//    @KafkaListener(topics = "jmix-kafka-json", groupId = "group_id")
//    public void consumeJson(ConsumerRecord<String, Object> record) throws IOException {
//        try {
//            logger.info(String.format("#### -> Consumed json -> %s", record.value()));
//        } catch (Exception ex) {
//            logger.error("TestConsume "+ex.getMessage());
//        }
//    }
//
//    @KafkaListener(topics = "userCreated", groupId = "group_id")
//    public void consumeNewUser(ConsumerRecord<String, Object> record) throws IOException {
//        try {
//
//
////            List<WalletForm> forms = null;
////            WalletForm formCash = new WalletForm();
////            formCash.setAcctOwner("fais");
////            formCash.setAmount(BigDecimal.ZERO);
////            formCash.setRegTypeCode("");
////            forms.add(formCash);
////
////
////            WalletForm formAsset = new WalletForm();
////
////            walletApiController.createWallet(forms);
//
//            logger.info(String.format("#### -> Consumed json -> %s", record.value()));
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//        }
//    }


}
