package com.company.registration.services;

import com.company.registration.entity.User;
import io.jmix.core.DataManager;
import io.jmix.core.EntitySerialization;
import io.jmix.core.EntitySerializationOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.List;

@Service
public class KafkaProducer {

    @Autowired
    private EntitySerialization entitySerialization;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);
    @Autowired
    private DataManager dataManager;

    public String sendMessage(String message) {
        try {
            logger.info(String.format("#### -> Producing message -> %s", message));
            this.kafkaTemplate.send("first_topic", message);
            return "SUCCESS: Producing message ->" +message;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return "ERROR: Producing message ->" +ex.getMessage();
        }
    }

    public void sendMessageJson(User user) {
        try {
            List<User> users = this.dataManager.load(User.class).all().list();
            logger.info(String.format("#### -> Producing message -> %s", entitySerialization.objectToJson(users)));
            this.kafkaTemplate.send("jmix-kafka-json", entitySerialization.objectToJson(users));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

//    public void sendMessage(String message) {
//        try {
////            this.kafkaTemplate.send("jmix-kafka-reference", message);
//            ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send("jmix-kafka-reference",message);
//            future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
//
//                @Override
//                public void onSuccess(SendResult<String, String> result) {
//
//                    logger.info("Message Sent  " + result.getRecordMetadata().timestamp());
//                    //your logic for the success scenario
//                }
//
//                @Override
//                public void onFailure(Throwable ex) {
//
//                    logger.info(" sending failed  ");
//                    // your logic if failed
//                    throw new RuntimeException("Kafka Failed");
//                }
//            });
//
//            logger.info(String.format("#### -> Producing message -> %s", message));
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//        }
//    }
}
