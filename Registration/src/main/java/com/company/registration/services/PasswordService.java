package com.company.registration.services;

import com.company.registration.entity.Otp;
import com.company.registration.entity.ResetPassword;
import com.company.registration.entity.User;
import com.company.registration.entity.ValidationStatus;
import com.company.registration.form.AddressForm;
import com.company.registration.responseForm.ServiceResponse;
import io.jmix.core.DataManager;
import io.jmix.core.Sort;
import io.jmix.core.querycondition.LogicalCondition;
import io.jmix.core.querycondition.PropertyCondition;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class PasswordService {
    static Logger logger = LogManager.getLogger(PasswordService.class.getName());
    @Autowired
    private DataManager dataManager;

    public String generateResetPasswordId(User user, Otp otp, ValidationStatus status, int failedAttempts){
        try{
            logger.info("generate reset password id for " + user.getUsername());
            logger.info("validation status: "+ status.getId());
            String resetPaswordId = null;

            if(status == ValidationStatus.NOT_MATCH){
                // increase failed attempts
                failedAttempts = failedAttempts+1;
                if(failedAttempts != 1){
                    List<ResetPassword> failedResetPasswords = this.dataManager.load(ResetPassword.class)
                            .condition(LogicalCondition.and(
                                    PropertyCondition.equal("user",user),
                                    PropertyCondition.equal("active",true),
                                    PropertyCondition.equal("validationStatus",status)))
                            .sort(Sort.by(Sort.Direction.DESC,"lastModifiedDate")).list();
                    ResetPassword failedResetPassword = failedResetPasswords.get(0);
                    failedResetPassword.setTotalAttempts(failedAttempts);
                    this.dataManager.save(failedResetPassword);
                }
                else {
                    // generate id for reset password
                    Date date = Calendar.getInstance().getTime();
                    Date validLink = DateUtils.addMinutes(date,20);
                    ResetPassword resetPassword = this.dataManager.create(ResetPassword.class);
                    resetPassword.setExpiredDatetime(validLink);
                    resetPassword.setUser(user);
                    resetPassword.setActive(true);
                    resetPassword.setOtpVerification(otp);
                    resetPassword.setValidationStatus(status);
                    resetPassword.setTotalAttempts(failedAttempts);
                    this.dataManager.save(resetPassword);
                    resetPaswordId = resetPassword.getId().toString();
                }
            }
            else {
                List<ResetPassword> currentResetPasswords = this.dataManager.load(ResetPassword.class)
                        .condition(LogicalCondition.and(
                                PropertyCondition.equal("user",user),
                                PropertyCondition.equal("active",true)))
                        .sort(Sort.by(Sort.Direction.DESC,"lastModifiedDate")).list();
                // deactivate previous id
                currentResetPasswords.forEach(e->{
                    e.setActive(false);
                    this.dataManager.save(e);
                });
                // generate id for reset password
                Date date = Calendar.getInstance().getTime();
                Date validLink = DateUtils.addMinutes(date,20);
                ResetPassword resetPassword = this.dataManager.create(ResetPassword.class);
                resetPassword.setExpiredDatetime(validLink);
                resetPassword.setUser(user);
                resetPassword.setActive(true);
                resetPassword.setOtpVerification(otp);
                resetPassword.setValidationStatus(status);
                resetPassword.setTotalAttempts(1);
                this.dataManager.save(resetPassword);
                resetPaswordId = resetPassword.getId().toString();
            }
            return resetPaswordId;
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return null;
        }
    }
}
