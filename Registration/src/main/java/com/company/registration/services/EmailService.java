package com.company.registration.services;
import com.company.registration.form.GenerateEmailForm;
import io.jmix.email.EmailException;
import io.jmix.email.Emailer;
import io.jmix.emailtemplates.EmailTemplates;
import io.jmix.emailtemplates.entity.EmailTemplate;
import io.jmix.emailtemplates.exception.ReportParameterTypeChangedException;
import io.jmix.emailtemplates.exception.TemplateNotFoundException;
import io.jmix.email.EmailInfo;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailService {
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(EmailService.class);
    @Autowired
    private Emailer emailer;
    @Inject
    EmailTemplates emailTemplates;

    public ResponseEntity sendRecoverUsername(String email, String userName){
        try{

            String CREATED_TEMPLATE_CODE = "recover_username";
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(email)
                    .setSubject("Bursa Gold Dinar Recover UserName")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("userName",userName);

            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);
            emailer.sendEmail(emailInfo);
            return ResponseEntity.status(HttpStatus.OK).body("email sent");

        }
        catch (TemplateNotFoundException | EmailException | ReportParameterTypeChangedException e) {
            logger.error("Error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
    }
    public String successVerification(GenerateEmailForm form){
        try{
            String TEMPLATE_CODE = "account_upgrade_success";
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(TEMPLATE_CODE)
                    .setTo(form.getSentTo())
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name",form.getName());
            params.put("email",form.getEmail());
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Successfull Account Upgrade email sent to "+emailInfo.getAddresses());
                return "OK";
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ex.getMessage();
            }
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return ex.getMessage();
        }
    }
    public String failVerification(GenerateEmailForm form){
        try{
            String TEMPLATE_CODE = "account_upgrade_failed";
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(TEMPLATE_CODE)
                    .setTo(form.getSentTo())
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name",form.getName());
            params.put("email",form.getEmail());
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Unsuccessfull Account Upgrade email sent to "+emailInfo.getAddresses());
                return "OK";
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ex.getMessage();
            }
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return ex.getMessage();
        }
    }
    public String pendingVerification(GenerateEmailForm form){
        try{
            String TEMPLATE_CODE = "account_upgrade_pending";
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(TEMPLATE_CODE)
                    .setTo(form.getSentTo())
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name",form.getName());
            params.put("email",form.getEmail());
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Pending Account Upgrade email sent to "+emailInfo.getAddresses());
                return "OK";
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ex.getMessage();
            }
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return ex.getMessage();
        }
    }
    public String successResetPassword(GenerateEmailForm form){
        try{
            String TEMPLATE_CODE = "reset_password_success";
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(TEMPLATE_CODE)
                    .setTo(form.getSentTo())
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name",form.getName());
            params.put("email",form.getEmail());
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Reset Password Successful email sent to "+emailInfo.getAddresses());
                return "OK";
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ex.getMessage();
            }
        }
        catch(Exception ex){
            logger.error(ex.getMessage());
            return ex.getMessage();
        }
    }
}
