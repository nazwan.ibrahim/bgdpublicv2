package com.company.registration.services;

import com.company.registration.app.PasswordController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.bouncycastle.jcajce.provider.digest.SHA3.DigestSHA3;
import org.bouncycastle.util.encoders.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/*
AES (Advanced Encryption Standard) used for encrypting and decrypting the data,
SHA-256 (Secure Hash Algorithm 256-bit) @ sha-3 is used for key derivation.
*/

@Service
public class CipherEncryption {
    Logger logger = LogManager.getLogger(CipherEncryption.class);
    private static SecretKeySpec secretKeySpec;
//    private static final String ALGORITHM = "AES";
    private static final String ALGORITHM = "AES/GCM/PKCS5Padding";
    private static final int TAG_LENGTH_BITS = 128;
    private static final int IV_LENGTH_BYTES = 12;

    public String encrypt(String strToEncrypt, String secret) {

        try {
            if(strToEncrypt.equals(""))
                return null;
            prepareSecretKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            byte[] iv = generateIV();
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(TAG_LENGTH_BITS, iv);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec,gcmParameterSpec);
            byte[] encryptedBytes = cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8));
            byte[] combined = new byte[iv.length + encryptedBytes.length];
            System.arraycopy(iv, 0, combined, 0, iv.length);
            System.arraycopy(encryptedBytes, 0, combined, iv.length, encryptedBytes.length);

            return Base64.getEncoder().encodeToString(combined);
        } catch (Exception e) {
            logger.error("Error while encrypting: " + e.getMessage());
        }
        return null;
    }
    public String decrypt(String strToDecrypt, String secret) {
        try {
            logger.info("strToDecrypt: "+ strToDecrypt);
            byte[] combined = Base64.getDecoder().decode(strToDecrypt);
            byte[] iv = new byte[IV_LENGTH_BYTES];
            byte[] encryptedBytes = new byte[combined.length - IV_LENGTH_BYTES];
            System.arraycopy(combined, 0, iv, 0, IV_LENGTH_BYTES);
            System.arraycopy(combined, IV_LENGTH_BYTES, encryptedBytes, 0, encryptedBytes.length);

            prepareSecretKey(secret);
//            SecretKeySpec secretKeySpec = prepareSecretKey(secret);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(TAG_LENGTH_BITS, iv);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, gcmParameterSpec);
            return new String(cipher.doFinal(encryptedBytes), StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error("Error while decrypting: " + e.getMessage());
        }
        return strToDecrypt;
    }

    public void prepareSecretKey(String myKey) {
        try {
            byte[] key = myKey.getBytes(StandardCharsets.UTF_8);

            DigestSHA3 sha3 = new DigestSHA3(256);
            sha3.update(key);
            key = sha3.digest();

            secretKeySpec = new SecretKeySpec(key, "AES");
        } catch (Exception ex){
            logger.error(ex.getMessage());
        }
    }

    private byte[] generateIV() {
        byte[] iv = new byte[IV_LENGTH_BYTES];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(iv);
        return iv;
    }
}
