package com.company.registration.services;

import com.company.registration.entity.*;
import com.company.registration.form.AddressForm;
import com.company.registration.form.PersonalDetailsForm;
import com.company.registration.responseForm.GenericResponse;
import com.company.registration.responseForm.ServiceResponse;
import io.jmix.core.DataManager;
import io.jmix.core.SaveContext;
import io.jmix.core.querycondition.PropertyCondition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    static Logger logger = LogManager.getLogger(UserService.class.getName());
    @Autowired
    private DataManager dataManager;

    public User getUserbyID(UUID userID){

        try {
            User user;
            user = this.dataManager.load(User.class).id(userID).one();
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

    public User getUserbyUsername(String userName){

        try {
            if(userName == null)
                return null;
            String queryStr = "select e from User e where lower(e.username) = lower(:username)";
            User user = this.dataManager.load(User.class)
                    .query(queryStr)
                    .parameter("username", userName).one();
//            user = this.dataManager.load(User.class)
//                    .condition(PropertyCondition.equal("username",userName)).one();
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

    public User getUserbyEmail(String email){
        try {
            if(email == null)
                return null;
            String queryStr = "select e from User e where lower(e.email) = lower(:email)";
            User user = this.dataManager.load(User.class)
                    .query(queryStr)
                    .parameter("email", email).one();
//            user = this.dataManager.load(User.class)
//                    .condition(PropertyCondition.equal("email",email)).optional();
            return user;
        } catch (Exception ex) {
            return null;
        }
    }
    public User getUserbyReferral(String referralCode){
        try {
            if(referralCode == null)
                return null;
            String queryStr = "select e from User e where lower(e.referral) = lower(:referral)";
            User user = this.dataManager.load(User.class)
                    .query(queryStr)
                    .parameter("referral", referralCode).one();
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

    public User getUserbyPhone(String phoneNumber){
        try {
            if(phoneNumber == null)
                return null;
            User user;
            user = this.dataManager.load(User.class)
                    .condition(PropertyCondition.contains("phone_number",phoneNumber)).one();
            return user;
        } catch (Exception ex) {
            return null;
        }
    }

//    public ServiceResponse updatePersonalDetails(PersonalDetailsForm personalDetails){
//        try{
//
//        }catch (Exception ex){
//            logger.error(ex.getMessage());
//            ServiceResponse response = new ServiceResponse();
//            response.setErrorCode(ErrorCode.MISCELLANEOUS);
//            response.setMessage(ex.getMessage());
//            return response;
//        }
//    }
    public ServiceResponse updateUserAddress(List<AddressForm> addresses, User user){
        try{
            logger.info("Update user address");
            ServiceResponse response = new ServiceResponse();
            SaveContext saveContext = new SaveContext();
            for (AddressForm address : addresses){
                //check address type
                Optional<AddressType> addressType = this.dataManager.load(AddressType.class)
                        .query("select e from AddressType e where e.code = :code")
                        .parameter("code",address.getAddressTypeCode()).optional();
                if (addressType.isEmpty()){
                    response.setErrorCode(ErrorCode.INVALID_DATA_TYPES);
                    response.setMessage("Invalid address type");
                    return response;
                }
                //check for existing userAddress with same address type
                List<UserAddress> checkUserAddress = this.dataManager.load(UserAddress.class)
//                    .condition(PropertyCondition.equal("user",user)).optional();
                        .query("select e from UserAddress e where e.user.id = :userID and e.addType.id = :addType")
                        .parameter("userID", user.getId())
                        .parameter("addType", addressType.get().getId()).list();
                if (checkUserAddress.size()!=0){
                    checkUserAddress.forEach(e->{
                        this.dataManager.remove(e);
                    });
                }

                UserAddress newAddress = this.dataManager.create(UserAddress.class);
                newAddress.setRecipientName(address.getRecipientName());
                newAddress.setContactNumber(address.getContactNumber());
                newAddress.setUser(user);
                newAddress.setLabel(address.getLabel());
                newAddress.setAddress1(address.getAddress1());
                newAddress.setAddress2(address.getAddress2());
                newAddress.setPostcode(address.getPostcode());
                newAddress.setTown(address.getCity());
                newAddress.setAddType(addressType.get());

                // check state from enum list
                if(address.getStateCode() != null && !Objects.equals(address.getStateCode(), "")){
                    if(Arrays.stream(StateList.values()).noneMatch(e->e.name().equals(address.getStateCode()))){
                        response.setErrorCode(ErrorCode.INVALID_DATA_TYPES);
                        response.setMessage("Invalid state");
                        return response;
                    }
                    newAddress.setState(StateList.valueOf(address.getStateCode()));
                }

                // check country from enum list
                if(address.getCountryCode() != null && !Objects.equals(address.getCountryCode(), "")){
                    if(Arrays.stream(CountryList.values()).noneMatch(e->e.name().equals(address.getCountryCode()))){
                        response.setErrorCode(ErrorCode.INVALID_DATA_TYPES);
                        response.setMessage("Invalid nationality");
                        return response;
                    }
                    newAddress.setCountry(CountryList.valueOf(address.getCountryCode()));
                }
                saveContext.saving(newAddress);
            }
            this.dataManager.save(saveContext);
            response.setSuccessCode(SuccessCode.UPDATED);
            response.setMessage("Address successfully updated");

            // response id
            return response;
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            ServiceResponse response = new ServiceResponse();
            response.setErrorCode(ErrorCode.MISCELLANEOUS);
            response.setMessage(ex.getMessage());
            return response;
        }
    }

    public UserPermission getUserperminssion(String username){

        try {
            return dataManager.load(UserPermission.class)
                    .query("select u from UserPermission u " +
                            "where u.user.username = :username " +
                            "and u.deletedDate is null").parameter("username", username).one();
        }
        catch (Exception e){
            logger.info("ERROR : UserPermission not found: Error message: " +e.getMessage());
            return null;
        }
    }

}
