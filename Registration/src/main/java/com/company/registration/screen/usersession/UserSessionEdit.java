package com.company.registration.screen.usersession;

import com.company.registration.entity.UserSession;
import io.jmix.ui.screen.*;

@UiController("UserSession.edit")
@UiDescriptor("user-session-edit.xml")
@EditedEntityContainer("userSessionDc")
public class UserSessionEdit extends StandardEditor<UserSession> {
}