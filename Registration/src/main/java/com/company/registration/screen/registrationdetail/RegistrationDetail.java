package com.company.registration.screen.registrationdetail;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("RegistrationDetail")
@UiDescriptor("registration-detail.xml")
public class RegistrationDetail extends Screen {
}