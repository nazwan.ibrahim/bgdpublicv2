package com.company.registration.screen.kyc;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("KycScreen")
@UiDescriptor("kyc-screen.xml")
public class KycScreen extends Screen {
}