package com.company.registration.screen.redirectlogin;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("RedirectLogin")
@UiDescriptor("redirect-login.xml")
public class RedirectLogin extends Screen {
}