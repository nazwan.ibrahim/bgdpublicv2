package com.company.registration.screen.oauthaccesstoken;

import io.jmix.ui.screen.*;
import com.company.registration.entity.OauthAccessToken;

@UiController("OauthAccessToken.browse")
@UiDescriptor("oauth-access-token-browse.xml")
@LookupComponent("oauthAccessTokensTable")
public class OauthAccessTokenBrowse extends StandardLookup<OauthAccessToken> {
}