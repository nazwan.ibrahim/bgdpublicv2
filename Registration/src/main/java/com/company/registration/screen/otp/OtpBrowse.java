package com.company.registration.screen.otp;

import com.company.registration.entity.Otp;
import io.jmix.ui.screen.*;

@UiController("Otp.browse")
@UiDescriptor("otp-browse.xml")
@LookupComponent("otpsTable")
public class OtpBrowse extends StandardLookup<Otp> {
}