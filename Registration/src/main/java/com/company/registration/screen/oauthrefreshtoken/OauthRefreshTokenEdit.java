package com.company.registration.screen.oauthrefreshtoken;

import io.jmix.ui.screen.*;
import com.company.registration.entity.OauthRefreshToken;

@UiController("OauthRefreshToken.edit")
@UiDescriptor("oauth-refresh-token-edit.xml")
@EditedEntityContainer("oauthRefreshTokenDc")
public class OauthRefreshTokenEdit extends StandardEditor<OauthRefreshToken> {
}