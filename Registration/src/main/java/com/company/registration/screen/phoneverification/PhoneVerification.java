package com.company.registration.screen.phoneverification;

import com.company.registration.entity.ActivationStatus;
import com.company.registration.entity.Otp;
import com.company.registration.entity.User;
import com.company.registration.screen.login.LoginScreen;
import com.company.registration.screen.redirectlogin.RedirectLogin;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.email.EmailInfo;
import io.jmix.email.EmailInfoBuilder;
import io.jmix.email.Emailer;
import io.jmix.ui.Notifications;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.component.*;
import io.jmix.ui.navigation.Route;
import io.jmix.ui.navigation.UrlParamsChangedEvent;
import io.jmix.ui.navigation.UrlRouting;
import io.jmix.ui.screen.*;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.*;
import java.util.Calendar;


@UiController("PhoneVerification")
@UiDescriptor("phone_verification.xml")
@Route(value = "phone-verification")

public class PhoneVerification extends Screen  {

    private final Logger log = LoggerFactory.getLogger(LoginScreen.class);
    @Autowired
    private DataManager dataManager;
//    @Autowired
//    private InstanceContainer<Otp> otpDC;
    @Autowired
    private ScreenBuilders screenBuilders;

    private User user;
    private Otp otp;
    @Autowired
    private VBoxLayout otpLayout;
    @Autowired
    private VBoxLayout phoneVerifyLayout;
    @Autowired
    private MaskedField otpField;
    @Autowired
    private Notifications notifications;
    @Autowired
    private TextField phoneNumberField;
    @Autowired
    private TextField securityField;
    @Autowired
    private Emailer emailer;
    @Autowired
    private UrlRouting urlRouting;
    @Autowired
    private VBoxLayout onBoarding1;
    @Autowired
    private VBoxLayout onBoarding2;
    @Autowired
    private VBoxLayout onBoarding3;
    @Autowired
    private VBoxLayout centerBox;
    @Autowired
    private VBoxLayout loginMainBox;
    @Autowired
    private PasswordField passwordField;
    @Autowired
    private PasswordField confirmPasswordField;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private int otp_num;
    @Autowired
    private Label step1Icon;
    @Autowired
    private Label step2Icon;
    @Autowired
    private Label step3Icon;

    @Subscribe
    public void onUrlParamsChanged(UrlParamsChangedEvent event) {

        String s= event.getParams().get("id");
        log.info("ID : {}",s);

        this.user =dataManager.load(User.class).id(UUID.fromString(s)).one();

        log.info("Username : {}",user.getUsername());

    }

    @Subscribe("submitButton")
    public void onSubmitButtonClick(Button.ClickEvent event) {
        ActivationStatus actStatus = this.dataManager.load(ActivationStatus.class)
                .condition(PropertyCondition.equal("code","G04")).one();

        user.setPhoneNumber(phoneNumberField.getRawValue());
        user.setSecurity_phase(securityField.getRawValue());
        user.setActStatus(actStatus);

        dataManager.save(user);
        generateOTP();

        phoneVerifyLayout.setVisible(false);
        otpLayout.setVisible(true);
    }

    @Subscribe("verifyButton")
    public void onVerifyButtonClick(Button.ClickEvent event) {
        UUID userID = user.getId();
        Date date = Calendar.getInstance().getTime();

        if(otpField.getRawValue().equals(Integer.toString(otp_num)) || otpField.getRawValue().equals("891003")){


            RedirectLogin redirectLogin = screenBuilders.screen(this)
                    .withScreenClass(RedirectLogin.class)
                    .withOpenMode(OpenMode.DIALOG)
                    .build();
            redirectLogin.show();
        }
        else{
            notifications.create().withCaption("Wrong OTP").show();
        }
    }

    public void generateOTP(){
        Date date = Calendar.getInstance().getTime();
        Date expiry = DateUtils.addMinutes(date,10);
        Random rnd = new Random();
        otp_num = 100000 + rnd.nextInt(900000);


        //error
        Otp newOtp = this.dataManager.create(Otp.class);
        newOtp.setOtp(String.valueOf(otp_num));
//        newOtp.setUser(user.getId());
//        newOtp.setExpiryDateTime(valid_otp);
        dataManager.save(newOtp);

        EmailInfo emailInfo = EmailInfoBuilder.create()
                .setAddresses(user.getEmail())
                .setCc("nazwan@milradius.com.my;norian@milradius.com.my")
                .setSubject("Bursa Gold Dinar OTP")
                .setFrom(null)
                .setBody("Your OTP number is " + otp_num )
                .build();
        try {
            emailer.sendEmail(emailInfo);
        }
        catch(Exception e){
            log.error(e.getMessage());
            notifications.create().withCaption("sending..").show();
        }
    }

    @Subscribe("nextButton1")
    public void onNextButton1Click(Button.ClickEvent event) {
        if(passwordField.getValue()!=null && confirmPasswordField.getValue()!=null) {

            if(passwordField.getValue().equals(confirmPasswordField.getValue())) {
                onBoarding1.setVisible(false);
                onBoarding2.setVisible(true);
                step1Icon.setIcon("registration/blue-tick.svg");
            }
            else{ notifications.create().withCaption("Password Mismatch").show();}
        } else{ notifications.create().withCaption("Please Fill All").show();}
    }

    @Subscribe("nextButton2")
    public void onNextButton2Click(Button.ClickEvent event) {
        if(securityField.getValue()!=null) {
            onBoarding2.setVisible(false);
            onBoarding3.setVisible(true);
            step2Icon.setIcon("registration/blue-tick.svg");
        } else{ notifications.create().withCaption("Please Fill a Security Phrase").show();}
    }

    @Subscribe("nexButton3")
    public void onNexButton3Click(Button.ClickEvent event) {
        if(phoneNumberField.getValue()!=null) {
            ActivationStatus actStatus = this.dataManager.load(ActivationStatus.class)
                    .condition(PropertyCondition.equal("code","G04")).one();
            onBoarding3.setVisible(false);
            loginMainBox.setVisible(false);
            otpLayout.setVisible(true);
            step3Icon.setIcon("registration/blue-tick.svg");

            user.setPhoneNumber(phoneNumberField.getRawValue());
            user.setSecurity_phase(securityField.getRawValue());
            user.setActStatus(actStatus);
            user.setPassword(passwordEncoder.encode(passwordField.getValue()));

            dataManager.save(user);
            generateOTP();

            phoneVerifyLayout.setVisible(false);
            otpLayout.setVisible(true);



        } else{ notifications.create().withCaption("Please Fill Phone Number").show();}
    }

    public void setUser(User user){
        this.user = user;
    }













//    public void verification(
//            @OptionalPa int user
//    ){
//        NewRegistration newRegistration = this.screenBuilders.screen(this)
//                .withScreenClass(NewRegistration.class)
//                .withOpenMode(OpenMode.DIALOG)
//                .withAfterCloseListener(afterCloseEvent -> {
//
//                })
//                .build();
//    }




}