package com.company.registration.screen.usersession;

import com.company.registration.entity.UserSession;
import io.jmix.ui.screen.*;

@UiController("UserSession.browse")
@UiDescriptor("user-session-browse.xml")
@LookupComponent("userSessionsTable")
public class UserSessionBrowse extends StandardLookup<UserSession> {
}