package com.company.registration.screen.amla;

import io.jmix.ui.screen.*;

@UiController("Amla.browse")
@UiDescriptor("amla-browse.xml")
@LookupComponent("amlasTable")
public class AmlaBrowse extends StandardLookup{
}