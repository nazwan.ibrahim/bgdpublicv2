package com.company.registration.screen.amla;

import io.jmix.ui.screen.*;

@UiController("Amla.edit")
@UiDescriptor("amla-edit.xml")
@EditedEntityContainer("amlaDc")
public class AmlaEdit extends StandardEditor{
}