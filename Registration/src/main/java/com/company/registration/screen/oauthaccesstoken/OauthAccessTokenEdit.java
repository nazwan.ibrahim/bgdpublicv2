package com.company.registration.screen.oauthaccesstoken;

import io.jmix.ui.screen.*;
import com.company.registration.entity.OauthAccessToken;

@UiController("OauthAccessToken.edit")
@UiDescriptor("oauth-access-token-edit.xml")
@EditedEntityContainer("oauthAccessTokenDc")
public class OauthAccessTokenEdit extends StandardEditor<OauthAccessToken> {
}