package com.company.registration.screen.otp;

import com.company.registration.entity.Otp;
import io.jmix.ui.screen.*;


@UiController("Otp.edit")
@UiDescriptor("otp-edit.xml")
@EditedEntityContainer("otpDc")
public class OtpEdit extends StandardEditor<Otp> {

}