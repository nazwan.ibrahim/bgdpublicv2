package com.company.registration.screen.oauthrefreshtoken;

import io.jmix.ui.screen.*;
import com.company.registration.entity.OauthRefreshToken;

@UiController("OauthRefreshToken.browse")
@UiDescriptor("oauth-refresh-token-browse.xml")
@LookupComponent("oauthRefreshTokensTable")
public class OauthRefreshTokenBrowse extends StandardLookup<OauthRefreshToken> {
}