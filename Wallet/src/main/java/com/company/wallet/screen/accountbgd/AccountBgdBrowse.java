package com.company.wallet.screen.accountbgd;

import io.jmix.ui.screen.*;
import com.company.wallet.entity.AccountBgd;

@UiController("AccountBgd.browse")
@UiDescriptor("account-bgd-browse.xml")
@LookupComponent("table")
public class AccountBgdBrowse extends MasterDetailScreen<AccountBgd> {
}