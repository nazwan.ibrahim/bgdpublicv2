package com.company.wallet.screen.topuppendingview;

import com.company.wallet.entity.TopupPendingView;
import io.jmix.ui.screen.*;

@UiController("TopupPendingView.browse")
@UiDescriptor("topup-pending-view-browse.xml")
@LookupComponent("topupPendingViewsTable")
public class TopupPendingViewBrowse extends StandardLookup<TopupPendingView> {
}