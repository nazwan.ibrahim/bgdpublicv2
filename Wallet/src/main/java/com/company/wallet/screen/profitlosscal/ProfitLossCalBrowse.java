package com.company.wallet.screen.profitlosscal;

import io.jmix.ui.screen.*;
import com.company.wallet.entity.ProfitLossCal;

@UiController("ProfitLossCal.browse")
@UiDescriptor("profit-loss-cal-browse.xml")
@LookupComponent("profitLossCalsTable")
public class ProfitLossCalBrowse extends StandardLookup<ProfitLossCal> {
}