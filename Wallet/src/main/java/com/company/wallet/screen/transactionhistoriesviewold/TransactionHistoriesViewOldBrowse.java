package com.company.wallet.screen.transactionhistoriesviewold;

import io.jmix.ui.screen.*;
import com.company.wallet.entity.TransactionHistoriesViewOld;

@UiController("TransactionHistoriesViewOld.browse")
@UiDescriptor("transaction-histories-view-old-browse.xml")
@LookupComponent("transactionHistoriesViewOldsTable")
public class TransactionHistoriesViewOldBrowse extends StandardLookup<TransactionHistoriesViewOld> {
}