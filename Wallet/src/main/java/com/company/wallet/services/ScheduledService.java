package com.company.wallet.services;

import com.company.wallet.app.PaymentGatewayAPIController;
import com.company.wallet.app.SchedulerController;
import com.company.wallet.app.SupplierGoldAPIController;
import com.company.wallet.app.WalletApiController;
import com.company.wallet.entity.*;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.jmix.core.DataManager;
import io.jmix.core.security.Authenticated;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class ScheduledService {
    @Autowired
    private Environment env;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private SchedulerController schedulerController;

    @Autowired
    private DataLoaderService dataLoaderService;

    @Autowired Metadata METADATA;

    @Autowired
    private SupplierGoldAPIController supplierGoldAPIController;

    @Autowired
    private NotificationService notificationService;

    private TransStatus TX_STATUS_CANCELLED;

    private TransStatus TX_STATUS_COMPLETED;

    private RedeemTrackerStatus TRACKER_STATUS_PICKUP;

    private RedeemTrackerStatus TRACKER_STATUS_IN_TRANSIT;

    private RedeemTrackerStatus TRACKER_STATUS_OUT_FOR_DELIVERY;

    private RedeemTrackerStatus TRACKER_STATUS_DELIVERED;

    private RedeemTrackerStatus TRACKER_STATUS_UNDELIVERED;

    static Logger logger = LogManager.getLogger(ScheduledService.class.getName());
    private final WalletApiController walletApiController;

    public ScheduledService(WalletApiController walletApiController) {
        this.walletApiController = walletApiController;
    }
    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
//        TX_STATUS_CANCELLED = dataLoaderService.getTransactionStatus("G13");
        TX_STATUS_COMPLETED = dataLoaderService.getTransactionStatus("G01");

        TRACKER_STATUS_PICKUP = dataLoaderService.getRedeemTrackerStatus("G42");
        TRACKER_STATUS_IN_TRANSIT= dataLoaderService.getRedeemTrackerStatus("G43");
        TRACKER_STATUS_OUT_FOR_DELIVERY = dataLoaderService.getRedeemTrackerStatus("G44");
        TRACKER_STATUS_DELIVERED = dataLoaderService.getRedeemTrackerStatus("G45");
        TRACKER_STATUS_UNDELIVERED = dataLoaderService.getRedeemTrackerStatus("G46");
    }

    @Scheduled(initialDelay = 120000, fixedRate = 120000) // run every 2 minutes -> in milliseconds
    public void topupCheckingScheduler() {
        try{
            String apiUrl = env.getProperty("topupStatusChecking_url");
            if(apiUrl !=null){
                logger.info("scheduler-> executing topupStatusChecking");
                RestTemplate restTemplate = new RestTemplate();
                HttpHeaders headers = new HttpHeaders();
                UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(apiUrl);
                String url = builder.toUriString();

                HttpEntity<String> request = new HttpEntity<>(headers);
                restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
            }
            else
                logger.info("topupStatusChecking_url is null");
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
        }
    }


    @Authenticated
//    @Scheduled(initialDelay = 10000, fixedRate = 3600000) // run every 1 hour -> in milliseconds
//    @Scheduled(initialDelay = 10000, fixedRate = 240000) // run every 4 minutes -> in milliseconds
    @Scheduled(initialDelay = 10000, fixedRate = 900000) // run every 15 minutes -> in milliseconds
    public void getLogisticStatus() {
        try{
            List<RedeemInfo> redeemInfoList= dataManager.load(RedeemInfo.class)
                    .query("select r from RedeemInfo r " +
                            "where r.status.code = :statusCode1 " +
                            "and r.deletedBy is null " +
                            "order by r.redemptionId asc")
                    .parameter("statusCode1", "G08")
                    .list();// the sort by redemtionId is important to make sure below code works
            Set<String> uniqueRedemptionIds = new HashSet<>(); ///use set for better data integrity and efficiency
            Set<String> redeemCompletedSet = new HashSet<>();
            for(RedeemInfo redeemInfo : redeemInfoList){

                String tracking_number = "";
                //filter so that it only call once per same redemption_id. Our database stores multiple row with same redemption_id when user request more than 1 unit of Gold dinar coin(different serial number)
                if(!uniqueRedemptionIds.contains(redeemInfo.getRedemptionId())){
                    tracking_number=""; //reset tracking number for each unique redemption_id
                    logger.info("REDEMPTION_ID : "+ redeemInfo.getRedemptionId() );
                    ResponseEntity<String> responseAce = supplierGoldAPIController.logisticStatus(redeemInfo.getRedemptionId());

                    if (responseAce.getStatusCode().is2xxSuccessful() && !responseAce.getBody().toString().contains("error")) {

                        String query1="select r from RedeemTracker r " +
                                "where r.transactionId = :transactionId1 " +
                                "and r.deletedBy is null";

                        List<RedeemTracker> redeemTrackers = dataManager.load(RedeemTracker.class)
                                .query(query1)
                                .parameter("transactionId1", redeemInfo.getTransaction())
                                .list();

                        // this is to check wether status already saved or not, based on timestamp
                        Set<String> timeStampRedeemTrackerList = new HashSet<>(); ///use set for better data integrity and efficiency
                        redeemTrackers.forEach(redeemTracker -> {
                            timeStampRedeemTrackerList.add(redeemTracker.getTimeStamp().toString());
                        });

                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.fromJson(responseAce.getBody().toString(),JsonObject.class);

                        //set trcking number // use to update redeeminfo
                        tracking_number = jsonObject.get("awbno").getAsString();
                        logger.info("tracking number: "+ tracking_number);


                        logger.info("Response from Ace.logistic_status: "+jsonObject.get("logistic_status").getAsJsonArray().toString());
                        JsonArray jsonArray = gson.fromJson(jsonObject.get("logistic_status").toString(),JsonArray.class);
                        logger.info("JSON array" + jsonArray.toString());

                        jsonArray.forEach(jsonElement -> {
                            String time= jsonElement.getAsJsonObject().get("time").getAsString();
                            logger.info("Time" + time);
                            String status=jsonElement.getAsJsonObject().get("api_status").getAsString();
                            logger.info("STATUS" +status);
                            String remark=jsonElement.getAsJsonObject().get("api_status_text").getAsString();
                            logger.info("Remarks" +remark);
                            logger.info("");

                            time = time.replace("UTC"," ");
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:SSS", Locale.ENGLISH);

//                            String dateInString = "7-Jun-2013";
                            Date date= null;
                            try {
                                date = formatter.parse(time);
                            } catch (ParseException e) {
                                throw new RuntimeException(e);
                            }

                            // this is to check wether status already saved or not, based on timestamp, if we have saved it before,ignored it
                            if(!timeStampRedeemTrackerList.contains(date.toString())){
                                RedeemTracker redeemTracker = dataManager.create(RedeemTracker.class);
                                redeemTracker.setTransactionId(redeemInfo.getTransaction());
                                redeemTracker.setTrackerStatus(getTrackerStatus(status));
                                redeemTracker.setRemarks(remark);
                                redeemTracker.setTimeStamp(date);

                                dataManager.save(redeemTracker);

//                                //if delivered change redeeminfo status to completed
//                                if(redeemTracker.getTrackerStatus().getCode().equals("G45")){
//                                    redeemInfo.setStatus(TX_STATUS_COMPLETED);
//                                }

                                redeemCompletedSet.add(redeemInfo.getRedemptionId());

                                //sent push Notification
                                String title= redeemTracker.getTrackerStatus().getName();
                                String body= redeemTracker.getRemarks();
                                notificationService.pushNotificationInternal(redeemInfo.getTransaction().getCreatedBy(),title,body,"",redeemInfo.getReference(),"NC01",getParcelTrackingNotificationType(redeemTracker.getTrackerStatus().getCode()));

                            }
                            else{
                                //ignore
                                logger.info("Old Status");
                            }

                        });

                    }
                        uniqueRedemptionIds.add(redeemInfo.getRedemptionId());
                }
                // for every redeem info we set the tracking number if we get the  number.
                // the redeem_info MUST be sorted by redemption_id during fetching above
                if(redeemInfo.getTrackingNumber()==null || !redeemInfo.getTrackingNumber().equals("")) {
                    redeemInfo.setTrackingNumber(tracking_number);
//                    dataManager.save(redeemInfo);
                }
            }

            //to update redeem info that need to update status completed
            redeemInfoList.forEach(redeemInfo -> {
                if(redeemCompletedSet.contains(redeemInfo.getRedemptionId())){
                    redeemInfo.setStatus(TX_STATUS_COMPLETED);
                }
                dataManager.save(redeemInfo);
            });

        }
        catch (Exception ex){
            logger.error(ex.getMessage());
        }
    }

    public RedeemTrackerStatus getTrackerStatus(String status) {
        switch (status) {
            case "Pickup":
                return TRACKER_STATUS_PICKUP;
            case "In Transit":
                return TRACKER_STATUS_IN_TRANSIT;
            case "Out For Delivery":
                return TRACKER_STATUS_OUT_FOR_DELIVERY;
            case "Delivered":
                return TRACKER_STATUS_DELIVERED;
            case "Undelivered":
                return TRACKER_STATUS_UNDELIVERED;
            default:
                return null;
        }
    }

    public String getParcelTrackingNotificationType(String status){
        switch (status){
            case "G42":
                return "NT073";
            case  "G43":
                return "NT071";
            case "G44":
                return "NT075";
            case "G45":
                return "NT074";
            case "G46":
                return "NT076";
            case "G41":
                return "NT072";
            default:
                return null;
        }
    }


    @Authenticated
    @Scheduled(initialDelay = 10000, fixedRate = 240000) // run every 4 minutes -> in milliseconds
    public void cancelTransaction() {
        try{

            //Auto cancel transactions that are made 5 minute before but still hasnt completed

            Date date2= Date.from(LocalDateTime.now().atZone(ZoneId.of("Asia/Kuala_Lumpur")).minusMinutes(5).toInstant());
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query("select t from TransactionAcct t " +
                            "where t.transStatus.code = :transStatusCode1 " +
                            "and t.createdDate < :createdDate1 " +
                            "and t.referenceId not like :referenceId1")
                    .parameter("createdDate1", date2)
                    .parameter("referenceId1", "F01%")
                    .parameter("transStatusCode1", "G05")
                    .list();
            logger.info("Size of transactions to be cancelled by scheduler -> "+transactionAcctList.size());
            int i=0;
            for(TransactionAcct transactionAcct:transactionAcctList){
                transactionAcct.setTransStatus(Metadata.TX_STATUS_CANCELLED);
                this.dataManager.save(transactionAcct);
                i+=1;
            }
            if(i!=0) {
                logger.info("Size of transactions that has been cancelled by scheduler-> " + transactionAcctList.size());
            }
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
        }
    }

    @Scheduled(cron = "0 35 13 * * ?") //execute at 12.30 am
    public void archiveTransaction() {
        try {
//            schedulerController.archiveTransaction();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

}
