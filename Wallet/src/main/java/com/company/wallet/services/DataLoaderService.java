package com.company.wallet.services;

import com.company.wallet.entity.RedeemTrackerStatus;
import com.company.wallet.entity.TransStatus;
import io.jmix.core.DataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//Service for loading data, purposely for metadata load
@Service
public class DataLoaderService {

    @Autowired
    private DataManager dataManager;
    public TransStatus getTransactionStatus(String code) {
        String query1="select e from TransStatus e where e.code = :code";
        return this.dataManager.load(TransStatus.class)
                .query(query1)
                .parameter("code", code).one();
    }

    public RedeemTrackerStatus getRedeemTrackerStatus(String code){
        String query1="select r from RedeemTrackerStatus r " +
                "where r.code = :code1";
        return dataManager.load(RedeemTrackerStatus.class)
                .query(query1)
                .parameter("code1", code)
                .one();
    }




}
