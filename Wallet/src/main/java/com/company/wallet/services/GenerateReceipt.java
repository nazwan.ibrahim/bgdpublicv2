package com.company.wallet.services;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.company.wallet.app.WalletApiController;
import com.company.wallet.form.FeesForm;
import com.company.wallet.form.ReceiptForm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.*;
import org.apache.pdfbox.util.Matrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class GenerateReceipt {
    static Logger logger = LogManager.getLogger(WalletApiController.class.getName());

    @Autowired
    private Environment env;

    private AmazonS3 s3Client = null;


    public void setS3Client(){
        String regionName = env.getProperty("AWS_REGION");
        Regions clientRegion = Regions.fromName(regionName);
        String accessKey = env.getProperty("AWS_ACCESS_KEY");
        String secretKey = env.getProperty("AWS_SECRET_KEY");

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
        s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(clientRegion)
                .build();
    }

    private void setTitle(PDPageContentStream contentStream,PDFont font, String text) throws IOException {
        contentStream.newLine();
        contentStream.setFont(font, 14);
        contentStream.setNonStrokingColor(123,123,123);
        contentStream.showText(text);
        contentStream.newLine();
    }
    private void setValue(PDPageContentStream contentStream,PDFont font, String text) throws IOException {
        contentStream.setFont(font, 14);
        contentStream.setNonStrokingColor(51,51,51);
        contentStream.showText(text);
        contentStream.newLine();
    }
    public byte[] generateTransactionReceipt(ReceiptForm receipt) {
        try{
            logger.info("generating receipt:"+receipt.getReference());
            if (s3Client == null){
                setS3Client();
            }
            String bucketName = env.getProperty("AWS_BUCKET_RECEIPT");
            String key = env.getProperty("RECEIPT_TEMPLATE_MEDIUM");

            if(Objects.equals(receipt.getTransactionTypeCode(), "F04")){
                key = env.getProperty("RECEIPT_TEMPLATE_LONG");
            }

            S3Object s3Object = s3Client.getObject(new GetObjectRequest(bucketName,key));

            logger.info("bucketName :"+bucketName);
            logger.info("key :"+key);

            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
            String dateStr = receipt.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().format(dateFormatter);
            String timeStr = receipt.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalTime().format(timeFormatter).toUpperCase();

            logger.info("dateStr :"+dateStr);
            logger.info("timeStr :"+timeStr);

            // Load the PDF document from the S3Object's content
            PDDocument document = PDDocument.load(s3Object.getObjectContent());
            PDPage page = document.getPage(0);

//            float startYaxis = 598; // 708.78- 110.78 = 598
            float startYaxis = (float) (page.getMediaBox().getHeight() - 110.78);

            //set font
            PDFont cabinRegular;
            PDFont cabinSemiBold;

            String fontPath = env.getProperty("font_path");
            File regularFontFile = new File(fontPath + "Cabin-Regular.ttf");
            File semiBoldFontFile = new File(fontPath + "Cabin-SemiBold.ttf");

            if(regularFontFile.exists() && semiBoldFontFile.exists()){
                logger.info("using fonts Cabin-Regular & Cabin-SemiBold");
                cabinRegular = PDType0Font.load(document, regularFontFile);
                cabinSemiBold = PDType0Font.load(document, semiBoldFontFile);
            }
            else{
                logger.info("Cabin-Regular.ttf & Cabin-SemiBold.ttf not found");
                cabinRegular = PDType1Font.HELVETICA;
                cabinSemiBold = PDType1Font.HELVETICA_BOLD;
            }

            //preparing the content stream
            PDPageContentStream contentStream = new PDPageContentStream(document, page,PDPageContentStream.AppendMode.APPEND, true, true);
            contentStream.beginText();
            contentStream.setFont(cabinSemiBold, 16);
            contentStream.setNonStrokingColor(51,51,51);
            contentStream.setLeading(17f);
            contentStream.newLineAtOffset(20, startYaxis);
            contentStream.showText(receipt.getTransactionDescription());
            contentStream.setFont(cabinSemiBold, 14);

//            // Calculate the starting position of the right-aligned text
            float width = 298;
            float textWidth = cabinSemiBold.getStringWidth(receipt.getStatus()) / 1000f * 14;
            float startXStatus = width - textWidth;
//            float startX = page.getMediaBox().getWidth() - 20 - textWidth; // Adjusted position

            // calculated date and time width
            float dateStrWidth = cabinRegular.getStringWidth(dateStr) / 1000f * 14;
            float timeStrWidth = cabinRegular.getStringWidth(timeStr) / 1000f * 14;
            float startXdateStr = width - dateStrWidth;
            float startXtimeStr = width - timeStrWidth;

            logger.info("receipt.getStatus() :"+receipt.getStatus());
            contentStream.newLineAtOffset(startXStatus, 0);
            contentStream.setFont(cabinSemiBold, 14);
            if(receipt.getStatusCode().matches("G07|G13"))
                contentStream.setNonStrokingColor(255, 0, 0);
            else
                contentStream.setNonStrokingColor(58, 175, 93);
            contentStream.showText(receipt.getStatus());
            contentStream.newLine();
            contentStream.newLine();
            contentStream.newLine();

            //reference number
            contentStream.newLineAtOffset(-startXStatus, 10);
            contentStream.setFont(cabinRegular, 14);
            contentStream.setNonStrokingColor(123,123,123);
            contentStream.showText("Reference Number");

            contentStream.newLineAtOffset(startXdateStr, 0);
            contentStream.showText(dateStr);
            contentStream.newLine();

            //reference content
            logger.info("receipt.getReference() :"+receipt.getReference());
            contentStream.newLineAtOffset(-startXdateStr, 0);
            contentStream.setFont(cabinSemiBold, 14);
            contentStream.setNonStrokingColor(51,51,51);
            contentStream.showText(receipt.getReference());

            //time
            contentStream.newLineAtOffset(startXtimeStr, 0);
            contentStream.setFont(cabinRegular, 14);
            contentStream.setNonStrokingColor(123,123,123);
            contentStream.showText(timeStr);

            contentStream.newLineAtOffset(-startXtimeStr, 0);
            contentStream.newLine();

            //receipt number
            if(receipt.getReceiptNumber() != null && !receipt.getTransactionTypeCode().matches("F03|F04")){
                contentStream.newLine();
                logger.info("receipt.getReceiptNumber() :"+receipt.getReceiptNumber());
                contentStream.setFont(cabinRegular, 14);
                contentStream.setNonStrokingColor(123,123,123);
                contentStream.showText("Receipt Number");
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);
                contentStream.showText(receipt.getReceiptNumber());
                contentStream.newLine();
            }

            // gold transfer
            if(Objects.equals(receipt.getTransactionTypeCode(), "F03")){
                String text = "Transferred to";
                String acctReceiver = receipt.getToAcct();
                String name = receipt.getReceiverName();
                if(receipt.getTransactionDescription().equals("Gold Receive")){
                    text = "Received from";
                    name = receipt.getSenderName();
                }

                contentStream.newLine();
                logger.info("receipt.getToAcct() :"+receipt.getToAcct());
                contentStream.setFont(cabinRegular, 14);
                contentStream.setNonStrokingColor(123,123,123);
                contentStream.showText(text);
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);
                contentStream.showText(name);
                contentStream.newLine();

                if(!receipt.getTransactionDescription().equals("Gold Receive")){
                    contentStream.setFont(cabinRegular, 14);
                    contentStream.setNonStrokingColor(123,123,123);
                    contentStream.showText(acctReceiver);
                    contentStream.newLine();
                }
                //Reason for Transfer
                contentStream.newLine();
                logger.info("receipt.getReason() :"+receipt.getReason());
                contentStream.setFont(cabinRegular, 14);
                contentStream.setNonStrokingColor(123,123,123);
                contentStream.showText("Reason for Transfer");
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);
                contentStream.showText(receipt.getReason());
                contentStream.newLine();
            }

            // Topup method
            if(Objects.equals(receipt.getTransactionTypeCode(), "F01")){
                logger.info("receipt.getTransactionMethod() :"+receipt.getTransactionMethod());
                setTitle(contentStream,cabinRegular,"Top-Up Method");
                setValue(contentStream,cabinSemiBold,receipt.getTransactionMethod());
            }

            // [buy gold, sell gold]
            if (receipt.getTransactionTypeCode().matches("F05|F06")){
                contentStream.newLine();
                logger.info("receipt.getPrice() :"+receipt.getPrice().toString());
                contentStream.setFont(cabinRegular, 14);
                contentStream.setNonStrokingColor(123,123,123);
                contentStream.showText("Price per Gram");
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);
                contentStream.showText(receipt.getPrice().toString()+"/g");
                contentStream.newLine();
            }

            // buy gold, sell gold, transfer gold
            if(receipt.getTransactionTypeCode().matches("F03|F05|F06")){
                String text = "Quantity";
                if(receipt.getTransactionDescription().equals("Gold Receive"))
                    text = "Quantity received";
                contentStream.newLine();
                logger.info("receipt.getQuantity() :"+receipt.getQuantity().toString());
                contentStream.setFont(cabinRegular, 14);
                contentStream.setNonStrokingColor(123,123,123);
                contentStream.showText(text);
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);
                contentStream.showText(receipt.getQuantity().setScale(6, RoundingMode.HALF_UP)+"g");
                contentStream.newLine();
            }

            // [topup, buy gold, sell gold]
            if (receipt.getTransactionTypeCode().matches("F01|F05|F06")){
                String text = "Total Amount";
                if (receipt.getTransactionTypeCode().equals("F01"))
                    text = "Top-Up Amount";
                logger.info("receipt.getAmount() :"+receipt.getAmount().toString());

                String textAmount = "RM"+ receipt.getAmount().setScale(2, RoundingMode.HALF_UP);
                setTitle(contentStream,cabinRegular,text);
                setValue(contentStream,cabinSemiBold,textAmount);
            }

            //[withdrawal]
            if (Objects.equals(receipt.getTransactionTypeCode(), "F02")){
                setTitle(contentStream,cabinRegular,"Bank Account");
                setValue(contentStream,cabinSemiBold,receipt.getBankAcct());

                setTitle(contentStream,cabinRegular,"Account Number");
                setValue(contentStream,cabinSemiBold,receipt.getBankAcctNumber());

                logger.info("receipt.getTotalWithdrawal() :"+receipt.getTotalWithdrawal().toString());

                String withdrawalAmount = "RM"+ receipt.getTotalWithdrawal().setScale(2, RoundingMode.HALF_UP);
                setTitle(contentStream,cabinRegular,"Withdrawal Amount");
                setValue(contentStream, cabinSemiBold,withdrawalAmount);
            }
            //[Redeem gold]
            if (Objects.equals(receipt.getTransactionTypeCode(), "F04")){
                logger.info("receipt.getDeliverTo()");

                setTitle(contentStream, cabinRegular, "Deliver to");
                setValue(contentStream,cabinSemiBold, receipt.getDeliverTo());

                logger.info("receipt.getPhoneNumber()");
                setTitle(contentStream, cabinRegular, "Phone Number");
                setValue(contentStream, cabinSemiBold, receipt.getPhoneNumber());

                logger.info("receipt.getAddress1()");
                setTitle(contentStream, cabinRegular, "Address Line 1");
                setValue(contentStream, cabinSemiBold, receipt.getAddress1());

                logger.info("receipt.getAddress2()");
                setTitle(contentStream, cabinRegular, "Address Line 2");
                setValue(contentStream, cabinSemiBold, receipt.getAddress2());

                logger.info("receipt.getPostcode()");
                setTitle(contentStream, cabinRegular, "Postcode");
                setValue(contentStream, cabinSemiBold, receipt.getPostcode());

                logger.info("receipt.getCity()");
                setTitle(contentStream, cabinRegular, "City");
                setValue(contentStream, cabinSemiBold, receipt.getCity());

                logger.info("receipt.getState()");
                setTitle(contentStream, cabinRegular, "State");
                setValue(contentStream, cabinSemiBold, receipt.getState());

                logger.info("receipt.getReason()");
                setTitle(contentStream, cabinRegular, "Reason for Redemption");
                setValue(contentStream, cabinSemiBold, receipt.getRemark());

                logger.info("receipt.getRedeemUnits()");
                setTitle(contentStream, cabinRegular, "Coins Redeemed");
                setValue(contentStream, cabinSemiBold, receipt.getRedeemUnits().toString());
            }

            // Transaction fee
            for (FeesForm f : receipt.getFees()) {
                contentStream.newLine();
                contentStream.setFont(cabinRegular, 14);
                contentStream.setNonStrokingColor(123,123,123);
                contentStream.showText(f.getFeeType() + " ");
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);
                contentStream.showText(f.getChargeUom());
                contentStream.showText(f.getChargeFees().setScale(2, RoundingMode.HALF_UP).toString());
                contentStream.newLine();
            }

            // SST
            if (receipt.getSST()!=null){
                contentStream.newLine();
                contentStream.setFont(cabinRegular, 14);
                contentStream.setNonStrokingColor(123,123,123);
                contentStream.showText("SST");
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);
                contentStream.showText("RM");
                contentStream.showText(receipt.getSST().setScale(2, RoundingMode.HALF_UP).toString());
                contentStream.newLine();
            }

            contentStream.newLineAtOffset(-20, 0);
            contentStream.setFont(cabinRegular, 14);
            contentStream.setNonStrokingColor(220,220,220);
            contentStream.showText("____________________________________________");
            contentStream.newLineAtOffset(20, 0);
            contentStream.newLine();

            if(!receipt.getTransactionDescription().equals("Gold Receive") && !receipt.getStatusCode().matches("G07|G13")){
                contentStream.newLine();
                contentStream.setNonStrokingColor(123,123,123);
                if (receipt.getTransactionTypeCode().matches("F03|F04|F05") && receipt.getAmountPaid() != null)
                    contentStream.showText("Total Amount Paid");
                else if (receipt.getTransactionTypeCode().matches("F01|F06") && receipt.getAmountReceived() != null)
                    contentStream.showText("Total Amount Received");
                else if(receipt.getAmount() != null)
                    contentStream.showText("Total Amount");
                contentStream.newLine();
                contentStream.setFont(cabinSemiBold, 14);
                contentStream.setNonStrokingColor(51,51,51);

                if (receipt.getTransactionTypeCode().matches("F03|F04|F05") && receipt.getAmountPaid() != null)
                    contentStream.showText("RM"+ receipt.getAmountPaid().setScale(2, RoundingMode.HALF_UP));
                else if (receipt.getTransactionTypeCode().matches("F01|F06") && receipt.getAmountReceived() != null)
                    contentStream.showText("RM"+ receipt.getAmountReceived().setScale(2, RoundingMode.HALF_UP));
                else if(Objects.equals(receipt.getTransactionTypeCode(), "F02") && receipt.getAmount() != null)
                    contentStream.showText("RM"+ receipt.getAmount().setScale(2, RoundingMode.HALF_UP));
                else if(receipt.getAmount() != null)
                    contentStream.showText("RM"+ receipt.getAmount().setScale(2, RoundingMode.HALF_UP));
            }

            contentStream.setLeading(14f);
            contentStream.newLine();
            contentStream.newLine();
            contentStream.setFont(cabinRegular, 12);
            contentStream.setNonStrokingColor(123,123,123);
            contentStream.showText("No signature is required for this computer-generated");
            contentStream.newLine();
            contentStream.showText("receipt.");
            contentStream.newLine();
            contentStream.newLine();
            contentStream.showText("Bursa Malaysia Digital Sdn Bhd");
            contentStream.newLine();
            contentStream.showText("(Formerly known as BM Digital Sdn Bhd)");
            contentStream.newLine();
            contentStream.showText("202201039636 (1485333-H)");
            contentStream.newLine();
            contentStream.newLine();
            contentStream.showText("Bursa2U Center, Lower Ground Floor, Exchange Square,");
            contentStream.newLine();
            contentStream.showText("50200, Kuala Lumpur, Malaysia");
            contentStream.newLine();
            contentStream.newLine();
            contentStream.showText("Tel: 03-2732 0067");
            contentStream.newLine();
            contentStream.showText("Fax: 03-2732 5258");
            contentStream.newLine();
            contentStream.showText("Email: bgdsupport@bursamalaysia.com");
            contentStream.newLine();
            contentStream.showText("Website: www.bursamalaysia.com");

            contentStream.endText();
            contentStream.close();

            // Convert the modified document to a byte array
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            document.save(baos);
            return baos.toByteArray();
        } catch (IOException e) {
            // Handle any potential IO exceptions
            logger.error("Error generateTransactionReceipt :"+e.getMessage());
        }

        return new byte[0];
    }
}
