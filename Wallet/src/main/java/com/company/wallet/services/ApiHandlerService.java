package com.company.wallet.services;

import com.company.wallet.form.GenericResponse;
import org.springframework.stereotype.Service;

@Service
public class ApiHandlerService {

    public GenericResponse errorResponseHandler(GenericResponse genericResponse, String errorMessage){
        //temp before while xda response error table
        switch (errorMessage){
            case "RCE000" :
                genericResponse.setResponseCode("RCE000");
                genericResponse.setHttpStatus(400);
                genericResponse.setDescription("TEST ERROR");
            case "RCE004" :
                genericResponse.setResponseCode("RCE004");
                genericResponse.setHttpStatus(400);
                genericResponse.setDescription("Data Not Found");
            default:
                genericResponse.setResponseCode("RCE999");
                genericResponse.setHttpStatus(500);
                genericResponse.setDescription("Miscellaneous Error");
                genericResponse.setData(errorMessage);
        }
        return genericResponse;

    }

}
