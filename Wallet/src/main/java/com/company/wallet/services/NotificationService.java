package com.company.wallet.services;

import com.company.wallet.app.PaymentGatewayAPIController;
import com.company.wallet.form.PushNotificationForm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.jmix.core.DataManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class NotificationService {


    @Autowired
    private Environment env;

    private final Logger logger = LogManager.getLogger(NotificationService.class.getName());

    @Autowired
    private DataManager dataManager;

    @Async  //so that this will run in background
    public void pushNotificationInternal(@NotNull String username,@NotNull String title,@NotNull String body,@NotNull String dataUrl,@NotNull String dataReference,@NotNull String categoryCode,@NotNull String typeCode) {
        //function to push notification direct to registration
        try {

            //create notification body to call notification api of registration , this api will save notification in db
            // this will not go through proxy first
            PushNotificationForm pushNotificationForm = new PushNotificationForm();
            pushNotificationForm.setUsername(username);
            pushNotificationForm.setTitle(title);
            pushNotificationForm.setBody(body);
            pushNotificationForm.setDataUrl(dataUrl);
            pushNotificationForm.setCategoryCode(categoryCode);
            pushNotificationForm.setTypeCode(typeCode);
            pushNotificationForm.setDataReference(dataReference);

            // create api request
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            String url = env.getProperty("push_notification_internal_url");
            HttpEntity<PushNotificationForm> request = new HttpEntity<>(pushNotificationForm, headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

            //check wether we get response or not
            if (response.getBody() == null || !response.getStatusCode().is2xxSuccessful()) {
                throw new RuntimeException("Failed to get response during push internal call");
            }
            //convert to json
            Gson gson = new Gson();
            JsonObject jsonObject = gson.fromJson(response.getBody(), JsonObject.class);
            logger.info("JSOn from push_internal api :" + jsonObject.toString());

            JsonObject data = gson.fromJson(jsonObject.getAsJsonObject("data"), JsonObject.class);



            //check deviceId is valid
            if (data.get("deviceId") == null || data.get("deviceId").getAsString().equals("")) {
                throw new RuntimeException("Device Id null");
            }

            logger.info("deviceId: " + data.get("deviceId").getAsString());

            // build body to call push api at proxy, this api will call firebase to sent notification
            Map<String, Object> mainBody = new HashMap<>();
            Map<String, String> notificationBody = new HashMap<>();
            notificationBody.put("title", title);
            notificationBody.put("body", body);

            Map<String, String> dataBody = new HashMap<>();
            dataBody.put("categoryCode", typeCode);
            dataBody.put("typeCode", typeCode);
            dataBody.put("dataUrl", dataUrl);
            dataBody.put("dataReference", dataReference);


            mainBody.put("notification", notificationBody);
            mainBody.put("data", dataBody);
            mainBody.put("token", data.get("deviceId").getAsString());


            String pushUrl = env.getProperty("push_proxy_url");
            HttpEntity<Map> pushRequest = new HttpEntity<>(mainBody, headers);
            ResponseEntity<String> pushResponseString = restTemplate.exchange(pushUrl, HttpMethod.POST, pushRequest, String.class);

            Gson pushResponseGson = new Gson();
            JsonObject pushResponseJson = pushResponseGson.fromJson(pushResponseString.getBody(), JsonObject.class);

            logger.info("GSON:" + pushResponseJson.toString());

        }catch(Exception e){

            logger.info("Error on Push notification: "+e.getMessage());

        }

    }


}
