package com.company.wallet.services;

import com.company.wallet.entity.BankDetailDTO;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.jmix.core.DataManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class ProxyApiService {


    @Autowired
    private Environment env;

    private final Logger logger = LogManager.getLogger(NotificationService.class.getName());

    @Autowired
    private DataManager dataManager;


    public BankDetailDTO getBankDetail(String token){
        BankDetailDTO bankDetailDTO = dataManager.create(BankDetailDTO.class);
        try{
            String getBankDetailUrl = env.getProperty("Get_Bank_Detail_Url");
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getBankDetailUrl);
            String url = builder.toUriString();
//            logger.info("url -> " + url);
            HttpEntity<String> request = new HttpEntity<>(headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

            if (response.getBody() != null){

                Gson gson = new Gson();
                JsonArray jsonObject = gson.fromJson(response.getBody().toString(),JsonArray.class);

                JsonObject bankDetailObject = jsonObject.get(0).getAsJsonObject();

                bankDetailDTO.setBankCode(bankDetailObject.get("bankCode").getAsString());
                bankDetailDTO.setBankName(bankDetailObject.get("bankName").getAsString());
                bankDetailDTO.setBicCode(bankDetailObject.get("bicCode").getAsString());
                bankDetailDTO.setAccountNumber(bankDetailObject.get("accountNumber").getAsString());

                return bankDetailDTO;
            }
            else
                return null;
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return null;
        }
    }



}
