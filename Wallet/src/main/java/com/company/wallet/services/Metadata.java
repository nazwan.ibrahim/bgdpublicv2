package com.company.wallet.services;

import com.company.wallet.entity.RedeemTrackerStatus;
import com.company.wallet.entity.TransStatus;
import io.jmix.core.security.Authenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Service;

// this service user to load all metadata ,
// this metadata is load at every service start to increase speed, reduce blocking io db operation
//
@Service
public class Metadata {
    //Strictly DO NOT and CAN NOT MODIFIED any of this class variable outside of this class
    @Autowired
    private DataLoaderService dataLoaderService;
    public static TransStatus TX_STATUS_CANCELLED;

//    public static RedeemTrackerStatus TRACKER_STATUS_REQUEST_PLACED;


    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        TX_STATUS_CANCELLED = dataLoaderService.getTransactionStatus("G13");

//        TRACKER_STATUS_REQUEST_PLACED = dataLoaderService.getRedeemTrackerStatus("G41");
    }

}
