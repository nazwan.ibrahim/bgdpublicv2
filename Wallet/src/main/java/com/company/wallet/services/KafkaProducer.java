package com.company.wallet.services;

import com.company.wallet.entity.User;
import io.jmix.core.DataManager;
import io.jmix.core.EntitySerialization;
import io.jmix.core.EntitySerializationOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KafkaProducer {

    @Autowired
    private EntitySerialization entitySerialization;

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);
    @Autowired
    private DataManager dataManager;

    public void sendMessage(String message) {
        try {
            logger.info(String.format("#### -> Producing message -> %s", message));
            this.kafkaTemplate.send("jmix-kafka-reference", message);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void sendMessageJson() {
        try {
            List<User> users = this.dataManager.load(User.class).all().list();
            logger.info(String.format("#### -> Producing message -> %s", entitySerialization.objectToJson(users)));
            this.kafkaTemplate.send("jmix-kafka-json", entitySerialization.objectToJson(users));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
}