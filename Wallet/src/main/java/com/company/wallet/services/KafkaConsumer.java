package com.company.wallet.services;

import com.company.wallet.app.*;
import com.company.wallet.entity.AccountBgd;
import com.company.wallet.form.WalletForm;
import io.jmix.core.EntitySerialization;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class KafkaConsumer {

    @Autowired
    private EntitySerialization entitySerialization;
    private final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    private WalletApiController walletApiController;

//    @KafkaListener(topics = "jmix-kafka-reference", groupId = "walletService")
//    public void consume(String message) throws IOException {
//        try {
//            logger.info(String.format("#### -> Consumed message -> %s", message));
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//        }
//    }

//    @KafkaListener(topics = "jmix-kafka-json", groupId = "walletService")
//    public void consumeJson(ConsumerRecord<String, Object> record) throws IOException {
//        try {
//            logger.info(String.format("#### -> Consumed json -> %s", record.value()));
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//        }
//    }

//    @KafkaListener(topics = "userCreated", groupId = "walletService")
//    public void consumeNewUser(ConsumerRecord<String, Object> record) throws IOException {
//        try {
//
//
//            List<WalletForm> forms = null;
//            WalletForm formCash = new WalletForm();
//            formCash.setAcctOwner("fais");
//            formCash.setAmount(BigDecimal.ZERO);
//            formCash.setRegTypeCode("");
//            forms.add(formCash);
//
//
//            WalletForm formAsset = new WalletForm();
//
//            walletApiController.createWallet(forms);
//
//            logger.info(String.format("#### -> Consumed json -> %s", record.value()));
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//        }
//    }


}
