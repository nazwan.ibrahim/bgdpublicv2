package com.company.wallet.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "WITHDRAWAL_INFO", indexes = {
        @Index(name = "IDX_WITHDRAWALIN_TRANSACTIONI", columnList = "TRANSACTION_ID_ID")
})
@Entity
public class WithdrawalInfo {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "TRANSACTION_ID_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionAcct transactionId;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "RECIPIENT_BANK_NO")
    private String recipientBankNo;

    @Column(name = "RECIPIENT_BANK_BIC")
    private String recipientBankBic;

    @Column(name = "RECIPIENT_BANK_NAME")
    private String recipientBankName;

    @Column(name = "SENDER_BANK_NO")
    private String senderBankNo;

    @Column(name = "SENDER_BANK_BIC")
    private String senderBankBic;

    @Column(name = "SENDER_BANK_NAME")
    private String senderBankName;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public String getSenderBankName() {
        return senderBankName;
    }

    public void setSenderBankName(String senderBankName) {
        this.senderBankName = senderBankName;
    }

    public String getSenderBankBic() {
        return senderBankBic;
    }

    public void setSenderBankBic(String senderBankBic) {
        this.senderBankBic = senderBankBic;
    }

    public String getSenderBankNo() {
        return senderBankNo;
    }

    public void setSenderBankNo(String senderBankNo) {
        this.senderBankNo = senderBankNo;
    }

    public String getRecipientBankName() {
        return recipientBankName;
    }

    public void setRecipientBankName(String recipientBankName) {
        this.recipientBankName = recipientBankName;
    }

    public String getRecipientBankBic() {
        return recipientBankBic;
    }

    public void setRecipientBankBic(String recipientBankBic) {
        this.recipientBankBic = recipientBankBic;
    }

    public String getRecipientBankNo() {
        return recipientBankNo;
    }

    public void setRecipientBankNo(String recipientBankNo) {
        this.recipientBankNo = recipientBankNo;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public TransactionAcct getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(TransactionAcct transactionId) {
        this.transactionId = transactionId;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}