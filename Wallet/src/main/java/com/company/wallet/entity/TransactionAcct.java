package com.company.wallet.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@JmixEntity
@Table(name = "transaction_acct", indexes = {
        @Index(name = "IDX_TRANSACTION_ACCT", columnList = "ACCOUNT_ID"),
        @Index(name = "IDX_TRANSACTION_ACCT_1", columnList = "TRANS_METHOD_ID"),
        @Index(name = "IDX_TRANSACTION_ACCT_2", columnList = "TRANS_STATUS_ID"),
        @Index(name = "IDX_TRANSACTION_ACCT_3", columnList = "TRANS_TYPE_ID")
})
@Entity
public class TransactionAcct {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "PERCENT_", precision = 19, scale = 2)
    private BigDecimal percent;

    @Column(name = "REFERENCE_ID")
    private String referenceId;

    @Column(name = "PRICE", precision = 19, scale = 2)
    private BigDecimal price;

    @JoinColumn(name = "account_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private AccountBgd account;

    @Column(name = "amount", precision = 19, scale = 8)
    private BigDecimal amount;

    @JoinColumn(name = "trans_method_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TransMethod transMethod;

    @JoinColumn(name = "trans_status_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TransStatus transStatus;

    @JoinColumn(name = "trans_type_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionType transType;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @OneToMany(mappedBy = "transAcct")
    private List<TransferInfo> transactionInfo;

    @Column(name = "RECEIPT_NUMBER")
    private String receiptNumber;

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public List<TransferInfo> getTransactionInfo() {
        return transactionInfo;
    }

    public void setTransactionInfo(List<TransferInfo> transactionInfo) {
        this.transactionInfo = transactionInfo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public TransactionType getTransType() {
        return transType;
    }

    public void setTransType(TransactionType transType) {
        this.transType = transType;
    }

    public TransStatus getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(TransStatus transStatus) {
        this.transStatus = transStatus;
    }

    public TransMethod getTransMethod() {
        return transMethod;
    }

    public void setTransMethod(TransMethod transMethod) {
        this.transMethod = transMethod;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public AccountBgd getAccount() {
        return account;
    }

    public void setAccount(AccountBgd account) {
        this.account = account;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}