package com.company.wallet.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DbView;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@DbView
@JmixEntity
@Table(name = "profit_loss_v2")
@Entity
public class ProfitLossV2 {
    @Column(name = "acc_owner")
    @Id
    private String accOwner;

    @Column(name = "average_price")
    private BigDecimal averagePrice;

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "cumulative_gold")
    private BigDecimal cumulativeGold;

    @Column(name = "cumulative_total")
    private BigDecimal cumulativeTotal;

    public BigDecimal getCumulativeTotal() {
        return cumulativeTotal;
    }

    public void setCumulativeTotal(BigDecimal cumulativeTotal) {
        this.cumulativeTotal = cumulativeTotal;
    }

    public BigDecimal getCumulativeGold() {
        return cumulativeGold;
    }

    public void setCumulativeGold(BigDecimal cumulativeGold) {
        this.cumulativeGold = cumulativeGold;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(BigDecimal averagePrice) {
        this.averagePrice = averagePrice;
    }

    public String getAccOwner() {
        return accOwner;
    }

    public void setAccOwner(String accOwner) {
        this.accOwner = accOwner;
    }
}