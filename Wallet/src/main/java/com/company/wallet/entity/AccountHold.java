package com.company.wallet.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "ACCOUNT_HOLD", indexes = {
        @Index(name = "IDX_ACCOUNT_HOLD_ACCOUNT_BGD", columnList = "ACCOUNT_BGD_ID"),
        @Index(name = "IDX_ACCOUNTHOLD_TRANSACTIONACC", columnList = "TRANSACTION_ACCT_ID")
})
@Entity
public class AccountHold {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "ACCOUNT_BGD_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private AccountBgd accountBgd;

    @JoinColumn(name = "TRANSACTION_ACCT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionAcct transactionAcct;

    @Column(name = "AMOUNT", precision = 19, scale = 4)
    private BigDecimal amount;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public TransactionAcct getTransactionAcct() {
        return transactionAcct;
    }

    public void setTransactionAcct(TransactionAcct transactionAcct) {
        this.transactionAcct = transactionAcct;
    }

    public AccountBgd getAccountBgd() {
        return accountBgd;
    }

    public void setAccountBgd(AccountBgd accountBgd) {
        this.accountBgd = accountBgd;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}