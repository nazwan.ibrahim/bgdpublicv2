package com.company.wallet.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.math.BigDecimal;
import java.util.UUID;

@JmixEntity
public class CreateTransactionDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private AccountBgd creditAcct;

    private AccountBgd debitAcct;

    private BigDecimal amount;

    private BigDecimal percent;

    private TransactionType creditType;

    private TransactionType debitType;

    private String reference;

    private TransStatus status;

    private BigDecimal bursaPrice;

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

    public TransStatus getStatus() {
        return status;
    }

    public void setStatus(TransStatus status) {
        this.status = status;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public TransactionType getDebitType() {
        return debitType;
    }

    public void setDebitType(TransactionType debitType) {
        this.debitType = debitType;
    }

    public TransactionType getCreditType() {
        return creditType;
    }

    public void setCreditType(TransactionType creditType) {
        this.creditType = creditType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public AccountBgd getDebitAcct() {
        return debitAcct;
    }

    public void setDebitAcct(AccountBgd debitAcct) {
        this.debitAcct = debitAcct;
    }

    public AccountBgd getCreditAcct() {
        return creditAcct;
    }

    public void setCreditAcct(AccountBgd creditAcct) {
        this.creditAcct = creditAcct;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getBursaPrice() {
        return bursaPrice;
    }

    public void setBursaPrice(BigDecimal bursaPrice) {
        this.bursaPrice = bursaPrice;
    }
}