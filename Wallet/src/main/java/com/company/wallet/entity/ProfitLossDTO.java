package com.company.wallet.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.math.BigDecimal;
import java.util.UUID;

@JmixEntity
public class ProfitLossDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private BigDecimal amount;

    private BigDecimal percentChanges;

    private BigDecimal averageCostPrice;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public BigDecimal getPercentChanges() {
        return percentChanges;
    }

    public void setPercentChanges(BigDecimal percentChanges) {
        this.percentChanges = percentChanges;
    }


    public BigDecimal getAverageCostPrice() {
        return averageCostPrice;
    }

    public void setAverageCostPrice(BigDecimal averageCostPrice) {
        this.averageCostPrice = averageCostPrice;
    }
}