package com.company.wallet.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DbView;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.UUID;

@DbView
@JmixEntity
@Table(name = "account_wallet_view")
@Entity
public class AccountWalletView {
    @JmixGeneratedValue
    @Column(name = "account_id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "account_owner")
    private String accountOwner;

    @Column(name = "acct_number")
    private String acctNumber;

    @Column(name = "available_amount", precision = 131089, scale = 4)
    private BigDecimal availableAmount;

    @Column(name = "code")
    private String code;

    @Column(name = "current_ammount", precision = 131089, scale = 4)
    private BigDecimal currentAmmount;

    @Column(name = "type")
    private String type;

    @Column(name = "uom")
    private String uom;

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getCurrentAmmount() {
        return currentAmmount;
    }

    public void setCurrentAmmount(BigDecimal currentAmmount) {
        this.currentAmmount = currentAmmount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getAvailableAmount() {
        return availableAmount;
    }

    public void setAvailableAmount(BigDecimal availableAmount) {
        this.availableAmount = availableAmount;
    }

    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }

    public String getAccountOwner() {
        return accountOwner;
    }

    public void setAccountOwner(String accountOwner) {
        this.accountOwner = accountOwner;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}