package com.company.wallet.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;

@JmixEntity
public class TransactionHistoriesDTO extends AccountBgd {
    private TransactionAcct transaction;

    private TransactionType type;

    private TransMethod method;

    private TransStatus status;

    public TransStatus getStatus() {
        return status;
    }

    public void setStatus(TransStatus status) {
        this.status = status;
    }

    public TransMethod getMethod() {
        return method;
    }

    public void setMethod(TransMethod method) {
        this.method = method;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public TransactionAcct getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionAcct transaction) {
        this.transaction = transaction;
    }
}