package com.company.wallet.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.util.UUID;

@JmixEntity
public class AmbankCTStatusDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String cTsrcRefNo;

    public String getCTsrcRefNo() {
        return cTsrcRefNo;
    }

    public void setCTsrcRefNo(String cTsrcRefNo) {
        this.cTsrcRefNo = cTsrcRefNo;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}