package com.company.wallet.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.DependsOnProperties;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@JmixEntity
@Table(name = "account_bgd", indexes = {
        @Index(name = "IDX_ACCOUNT_BGD_WALLET_TYPE", columnList = "WALLET_TYPE_ID")
})
@Entity
public class AccountBgd {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "ACC_OWNER")
    private String accOwner;

    @Column(name = "acct_number")
    private String acctNumber;

    @Column(name = "current_amount", precision = 19, scale = 4)
    private BigDecimal currentAmount;

    @Column(name = "reg_id")
    private Integer reg;

    @JoinColumn(name = "reg_type_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private RegType regType;

    @JoinColumn(name = "unit_measure_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UnitOfMeasure unitMeasure;

    @JoinColumn(name = "WALLET_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private WalletType walletType;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @OneToMany(mappedBy = "account")
    private List<TransactionAcct> transactionAcct;

    @OneToMany(mappedBy = "accountBgd")
    private List<AccountHold> accountHold;

    @OneToMany(mappedBy = "toAcct")
    private List<TransferInfo> transactionInfo;

    public String getAccOwner() {
        return accOwner;
    }

    public void setAccOwner(String accOwner) {
        this.accOwner = accOwner;
    }

    public List<TransferInfo> getTransactionInfo() {
        return transactionInfo;
    }

    public void setTransactionInfo(List<TransferInfo> transactionInfo) {
        this.transactionInfo = transactionInfo;
    }

    public List<AccountHold> getAccountHold() {
        return accountHold;
    }

    public void setAccountHold(List<AccountHold> accountHold) {
        this.accountHold = accountHold;
    }

    public List<TransactionAcct> getTransactionAcct() {
        return transactionAcct;
    }

    public void setTransactionAcct(List<TransactionAcct> transactionAcct) {
        this.transactionAcct = transactionAcct;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public WalletType getWalletType() {
        return walletType;
    }

    public void setWalletType(WalletType walletType) {
        this.walletType = walletType;
    }

    public UnitOfMeasure getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(UnitOfMeasure unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public RegType getRegType() {
        return regType;
    }

    public void setRegType(RegType regType) {
        this.regType = regType;
    }

    public Integer getReg() {
        return reg;
    }

    public void setReg(Integer reg) {
        this.reg = reg;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }

    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @InstanceName
    @DependsOnProperties({"acctNumber", "accOwner"})
    public String getInstanceName() {
        return String.format("%s %s", acctNumber, accOwner);
    }
}