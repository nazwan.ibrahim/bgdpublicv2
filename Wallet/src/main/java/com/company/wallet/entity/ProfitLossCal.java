package com.company.wallet.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DbView;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@DbView
@JmixEntity
@Table(name = "profit_loss_cal")
@Entity
public class ProfitLossCal {
    @Column(name = "acc_owner", nullable = false)
    @Id
    private String accOwner;

    @Column(name = "available_asset", precision = 131089, scale = 0)
    private BigDecimal availableAsset;

    @Column(name = "avg_cost", precision = 131089, scale = 0)
    private BigDecimal avgCost;

    public BigDecimal getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(BigDecimal avgCost) {
        this.avgCost = avgCost;
    }

    public BigDecimal getAvailableAsset() {
        return availableAsset;
    }

    public void setAvailableAsset(BigDecimal availableAsset) {
        this.availableAsset = availableAsset;
    }

    public String getAccOwner() {
        return accOwner;
    }

    public void setAccOwner(String accOwner) {
        this.accOwner = accOwner;
    }
}