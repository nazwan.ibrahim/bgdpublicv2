package com.company.wallet.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum Substate implements EnumClass<String> {

    CLEARED("CLEARED"),
    RETRIEVED("RETRIEVED"),
    RECEIVED("RECEIVED"),
    ACCEPTED("PENDING AUTHORIZATION"),
    CANCELLED("CANCELLED"),
    REJECTED("REJECTED");

    private String id;

    Substate(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static Substate fromId(String id) {
        for (Substate at : Substate.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}