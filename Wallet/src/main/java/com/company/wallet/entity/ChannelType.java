package com.company.wallet.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum ChannelType implements EnumClass<String> {

    MOBILE_APPLICATION("BA"),
    WEB_BROWSER("BW");

    private final String id;

    ChannelType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ChannelType fromId(String id) {
        for (ChannelType at : ChannelType.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}