package com.company.wallet.listener;

import com.company.wallet.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.math.BigDecimal;

@Component
public class AccountBgdEventListener {

    static Logger logger = LogManager.getLogger(AccountBgdEventListener.class.getName());

    @Autowired
    private DataManager dataManager;

    @EventListener
    public void onAccountBgdLoading(EntityLoadingEvent<AccountBgd> event) {

    }

    @EventListener
    public void onAccountBgdSaving(EntitySavingEvent<AccountBgd> event) {
        try {
            AccountBgd accountBgd = event.getEntity();
            accountBgd.setAcctNumber(StringUtils.right(event.getEntity().getId().toString(), 12));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    @EventListener
    public void onAccountBgdChangedBeforeCommit(EntityChangedEvent<AccountBgd> event) {
        if(event.getType().equals(EntityChangedEvent.Type.CREATED)) {
            AccountBgd accountBgd = this.dataManager.load(event.getEntityId()).one();

            registerTransactionAcct(accountBgd);
            registerAccountHold(accountBgd);
        }
    }

    public void registerTransactionAcct(AccountBgd accountBgd) {
        try {
            TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
            transactionAcct.setAccount(accountBgd);
            transactionAcct.setAmount(BigDecimal.ZERO);
            String query1="select e from TransStatus e where e.code = :code";
            transactionAcct.setTransStatus(this.dataManager.load(TransStatus.class)
                    .query(query1)
                    .parameter("code", "G01").one());
            String query2="select e from TransactionType e where e.code = :code";
            transactionAcct.setTransType(this.dataManager.load(TransactionType.class)
                    .query(query2)
                    .parameter("code", "F10").one());
            this.dataManager.save(transactionAcct);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void registerAccountHold(AccountBgd accountBgd) {
        try {
            AccountHold accountHold = this.dataManager.create(AccountHold.class);
            accountHold.setAccountBgd(accountBgd);
            accountHold.setAmount(BigDecimal.ZERO);
            this.dataManager.save(accountHold);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    @TransactionalEventListener
    public void onAccountBgdChangedAfterCommit(EntityChangedEvent<AccountBgd> event) {
        try {
//            TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
//            transactionAcct.setAccount(this.dataManager.load(event.getEntityId()).joinTransaction(true).one());
//            transactionAcct.setAmount(BigDecimal.ZERO);
//            this.dataManager.save(transactionAcct);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
}