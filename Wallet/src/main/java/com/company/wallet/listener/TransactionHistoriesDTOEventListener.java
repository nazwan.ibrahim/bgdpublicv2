package com.company.wallet.listener;

import com.company.wallet.entity.TransactionHistoriesDTO;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class TransactionHistoriesDTOEventListener {

    @EventListener
    public void onTransactionHistoriesDTOLoading(EntityLoadingEvent<TransactionHistoriesDTO> event) {

    }

    @EventListener
    public void onTransactionHistoriesDTOSaving(EntitySavingEvent<TransactionHistoriesDTO> event) {

    }

    @EventListener
    public void onTransactionHistoriesDTOChangedBeforeCommit(EntityChangedEvent<TransactionHistoriesDTO> event) {

    }

    @TransactionalEventListener
    public void onTransactionHistoriesDTOChangedAfterCommit(EntityChangedEvent<TransactionHistoriesDTO> event) {

    }
}