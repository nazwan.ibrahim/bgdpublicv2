package com.company.wallet.listener;

import com.company.wallet.entity.*;
import groovy.lang.Tuple2;
import io.jmix.core.DataManager;
import io.jmix.core.SaveContext;
import io.jmix.core.security.Authenticated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DataInitializer {
    @Autowired
    private DataManager dataManager;

    @EventListener
    @Authenticated
    public void onApplicationStarted(ApplicationStartedEvent event) {
//        initBankList();
        initAccountType();
        initWalletType();
        initUOM();
        initTransactionType();
        initTransactionStatus();
        initTransactionMethod();
        initAccountBGD();
        initRedeemTrackerStatus();
    }

    private void initBankList(){
        List<PaynetBankList> bankList = this.dataManager.load(PaynetBankList.class).all().list();

        Map<String, Tuple2<String, String>> map = new HashMap<>();
        map.put("ABB0233",new Tuple2<>("Affin Bank", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("ABMB0212",new Tuple2<>("Alliance Bank Malaysia Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("AGRO01",new Tuple2<>("Agro Bank", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("AMBB0209",new Tuple2<>("Ambank Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("BCBB0235",new Tuple2<>("CIMB Bank", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("BIMB0340",new Tuple2<>("Bank Islam", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("BKRM0602",new Tuple2<>("Bank Kerjasama Rakyat Malaysia Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("BMMB0341",new Tuple2<>("Bank Muamalat", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("BSN0601",new Tuple2<>("Bank Simpanan Nasional Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("HLB0224",new Tuple2<>("Hong Leong Bank Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("HSBC0223",new Tuple2<>("HSBC Bank", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("KFH0346",new Tuple2<>("Kuwait Finance House", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("MB2U0227",new Tuple2<>("MayBank Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("OCBC0229",new Tuple2<>("OCBC Bank (Malaysia) Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("PBBEMYKL",new Tuple2<>("Public Bank Berhad", "https://www2.pbebank.com/eaijct/Public_Bank/JunctionManager?RDO=ReqToPay/RPP/MY/Redirect/RTP"));
        map.put("RHB0218",new Tuple2<>("RHB Banking Group", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("SCB0216",new Tuple2<>("Standard Chartered Bank Malaysia Berhad", "https://www.mepsfpx.com.my/FPXMain/r2pReceiver.jsp"));
        map.put("UOVBMYKL",new Tuple2<>("United Overseas Bank Berhad (UOB)", "https://mydbsoi.uob.com.my"));

        SaveContext saveContext = new SaveContext();

        for (String key: map.keySet()){
            PaynetBankList type = this.dataManager.create(PaynetBankList.class);
            if(bankList.stream().noneMatch(a -> a.getCode().equals(key))){
                type.setCode(key);
                type.setName(map.get(key).getV1());
                type.setRedirectUrl(map.get(key).getV2());
                type.setRedirectUrlType("RET");
//                this.dataManager.save(type);
                saveContext.saving(type);
            }
        }
        this.dataManager.save(saveContext);
    }


    private void initAccountType() {
        List<RegType> regTypes = this.dataManager.load(RegType.class).all().list();

            if (regTypes.stream().noneMatch(a -> a.getCode().equals("D01"))) {
                RegType regType = this.dataManager.create(RegType.class);
                regType.setCode("D01");
                regType.setName("Platform Owner");
                this.dataManager.save(regType);
            }
            if (regTypes.stream().noneMatch(a -> a.getCode().equals("D02"))) {
                RegType regType = this.dataManager.create(RegType.class);
                regType.setCode("D02");
                regType.setName("Individual Investor");
                this.dataManager.save(regType);
            }
            if (regTypes.stream().noneMatch(a -> a.getCode().equals("D03"))) {
                RegType regType = this.dataManager.create(RegType.class);
                regType.setCode("D03");
                regType.setName("Company Investor");
                this.dataManager.save(regType);
            }
            if (regTypes.stream().noneMatch(a -> a.getCode().equals("D04"))) {
                RegType regType = this.dataManager.create(RegType.class);
                regType.setCode("D04");
                regType.setName("Supplier");
                this.dataManager.save(regType);
            }

    }

    private void initWalletType() {
        List<WalletType> walletTypes = this.dataManager.load(WalletType.class).all().list();

        if(walletTypes.stream().noneMatch(a -> a.getCode().equals("E01"))) {
            WalletType walletType = this.dataManager.create(WalletType.class);
            walletType.setCode("E01");
            walletType.setName("Cash");
            this.dataManager.save(walletType);
        }

        if(walletTypes.stream().noneMatch(a -> a.getCode().equals("E02"))) {
            WalletType walletType = this.dataManager.create(WalletType.class);
            walletType.setCode("E02");
            walletType.setName("Gold");
            this.dataManager.save(walletType);
        }
    }

    private void initUOM() {
        List<UnitOfMeasure> unitOfMeasures = this.dataManager.load(UnitOfMeasure.class).all().list();

        if(unitOfMeasures.stream().noneMatch(a -> a.getCode().equals("J01"))) {
            UnitOfMeasure unitOfMeasure = this.dataManager.create(UnitOfMeasure.class);
            unitOfMeasure.setCode("J01");
            unitOfMeasure.setName("RM");
            this.dataManager.save(unitOfMeasure);
        }
        if(unitOfMeasures.stream().noneMatch(a -> a.getCode().equals("J02"))) {
            UnitOfMeasure unitOfMeasure = this.dataManager.create(UnitOfMeasure.class);
            unitOfMeasure.setCode("J02");
            unitOfMeasure.setName("Gram");
            this.dataManager.save(unitOfMeasure);
        }
    }

    private void initTransactionType() {
        List<TransactionType> transactionTypes = this.dataManager.load(TransactionType.class).all().list();

        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F01"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F01");
            transactionType.setName("Top-Up");
            transactionType.setDescription("Cash Top-Up");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F02"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F02");
            transactionType.setName("Withdraw");
            transactionType.setDescription("Cash Withdrawal");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F03"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F03");
            transactionType.setName("Gold Transfer");
            transactionType.setDescription("Gold Transfer");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F03/E02-02"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F03/E02-02");
            transactionType.setName("Gold Receive");
            transactionType.setDescription("Gold Receive");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F03/E01-01"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F03/E01-01");
            transactionType.setName("Cash Transfer");
            transactionType.setDescription("Cash Transfer");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F03/E01-02"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F03/E01-02");
            transactionType.setName("Cash Receive");
            transactionType.setDescription("Cash Receive");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F04"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F04");
            transactionType.setName("Redeem");
            transactionType.setDescription("Gold Redemption");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F05"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F05");
            transactionType.setName("Marketplace Buy");
            transactionType.setDescription("Gold Purchase");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F06"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F06");
            transactionType.setName("Bursa Store Sell");
            transactionType.setDescription("Gold Sell");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F07"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F07");
            transactionType.setName("Exchange Buy");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F08"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F08");
            transactionType.setName("Exchange Sell");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09");
            transactionType.setName("Transaction Fee");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/F01"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/F01");
            transactionType.setName("Top-Up Fee");
            this.dataManager.save(transactionType);
        }
		if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/F02"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/F02");
            transactionType.setName("Withdrawal Fee");
            this.dataManager.save(transactionType);
        }
		if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/F03"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/F03");
            transactionType.setName("Transfer Fee");
            this.dataManager.save(transactionType);
        }
		if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/F05"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/F05");
            transactionType.setName("Purchase Fee");
            this.dataManager.save(transactionType);
        }
		if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/F06"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/F06");
            transactionType.setName("Sale Fee");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F10"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F10");
            transactionType.setName("Opening Balance");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/01"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/01");
            transactionType.setName("Administrative Fee");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/02"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/02");
            transactionType.setName("SST");
            this.dataManager.save(transactionType);
        }
//        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/03"))) {
//            TransactionType transactionType = this.dataManager.create(TransactionType.class);
//            transactionType.setCode("F09/03");
//            transactionType.setName("GST");
//            this.dataManager.save(transactionType);
//        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/B03"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/B03");
            transactionType.setName("Courier Fee");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/B04"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/B04");
            transactionType.setName("Insurance/Takaful Fee");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F09/B05"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F09/B05");
            transactionType.setName("Minting Fee");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F12/01"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F12/01");
            transactionType.setName("SignUp Reward");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F12/02-01"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F12/02-01");
            transactionType.setName("Referrer Reward");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F12/02-02"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F12/02-02");
            transactionType.setName("Referee Reward");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F13/E01"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F13/E01");
            transactionType.setName("Cash Adjustment");
            this.dataManager.save(transactionType);
        }
        if(transactionTypes.stream().noneMatch(a -> a.getCode().equals("F13/E02"))) {
            TransactionType transactionType = this.dataManager.create(TransactionType.class);
            transactionType.setCode("F13/E02");
            transactionType.setName("Gold Adjustment");
            this.dataManager.save(transactionType);
        }

    }

    private void initTransactionStatus() {
        List<TransStatus> transStatuses = this.dataManager.load(TransStatus.class).all().list();

        if(transStatuses.stream().noneMatch(a -> a.getCode().equals("G01"))) {
            TransStatus transStatus = this.dataManager.create(TransStatus.class);
            transStatus.setCode("G01");
            transStatus.setName("Completed");
            this.dataManager.save(transStatus);
        }
        if(transStatuses.stream().noneMatch(a -> a.getCode().equals("G02"))) {
            TransStatus transStatus = this.dataManager.create(TransStatus.class);
            transStatus.setCode("G02");
            transStatus.setName("Failed");
            this.dataManager.save(transStatus);
        }
        if(transStatuses.stream().noneMatch(a -> a.getCode().equals("G05"))) {
            TransStatus transStatus = this.dataManager.create(TransStatus.class);
            transStatus.setCode("G05");
            transStatus.setName("Pending");
            this.dataManager.save(transStatus);
        }
        if(transStatuses.stream().noneMatch(a -> a.getCode().equals("G06"))) {
            TransStatus transStatus = this.dataManager.create(TransStatus.class);
            transStatus.setCode("G06");
            transStatus.setName("Approved");
            this.dataManager.save(transStatus);
        }
        if(transStatuses.stream().noneMatch(a -> a.getCode().equals("G07"))) {
            TransStatus transStatus = this.dataManager.create(TransStatus.class);
            transStatus.setCode("G07");
            transStatus.setName("Reject");
            this.dataManager.save(transStatus);
        }
        if(transStatuses.stream().noneMatch(a -> a.getCode().equals("G08"))) {
            TransStatus transStatus = this.dataManager.create(TransStatus.class);
            transStatus.setCode("G08");
            transStatus.setName("Processing");
            this.dataManager.save(transStatus);
        }
        if(transStatuses.stream().noneMatch(a -> a.getCode().equals("G13"))) {
            TransStatus transStatus = this.dataManager.create(TransStatus.class);
            transStatus.setCode("G13");
            transStatus.setName("Cancelled");
            this.dataManager.save(transStatus);
        }
    }

    private void initRedeemTrackerStatus() {
        List<RedeemTrackerStatus> redeemTrackerStatuses = this.dataManager.load(RedeemTrackerStatus.class).all().list();

        if(redeemTrackerStatuses.stream().noneMatch(a -> a.getCode().equals("G41"))) {
            RedeemTrackerStatus redeemTrackerStatus = this.dataManager.create(RedeemTrackerStatus.class);
            redeemTrackerStatus.setCode("G41");
            redeemTrackerStatus.setName("Request Placed");
            this.dataManager.save(redeemTrackerStatus);
        }
        if(redeemTrackerStatuses.stream().noneMatch(a -> a.getCode().equals("G42"))) {
            RedeemTrackerStatus redeemTrackerStatus = this.dataManager.create(RedeemTrackerStatus.class);
            redeemTrackerStatus.setCode("G42");
            redeemTrackerStatus.setName("Pickup");
            this.dataManager.save(redeemTrackerStatus);
        }
        if(redeemTrackerStatuses.stream().noneMatch(a -> a.getCode().equals("G43"))) {
            RedeemTrackerStatus redeemTrackerStatus = this.dataManager.create(RedeemTrackerStatus.class);
            redeemTrackerStatus.setCode("G43");
            redeemTrackerStatus.setName("In Transit");
            this.dataManager.save(redeemTrackerStatus);
        }
        if(redeemTrackerStatuses.stream().noneMatch(a -> a.getCode().equals("G44"))) {
            RedeemTrackerStatus redeemTrackerStatus = this.dataManager.create(RedeemTrackerStatus.class);
            redeemTrackerStatus.setCode("G44");
            redeemTrackerStatus.setName("Out For Delivery");
            this.dataManager.save(redeemTrackerStatus);
        }
        if(redeemTrackerStatuses.stream().noneMatch(a -> a.getCode().equals("G45"))) {
            RedeemTrackerStatus redeemTrackerStatus = this.dataManager.create(RedeemTrackerStatus.class);
            redeemTrackerStatus.setCode("G45");
            redeemTrackerStatus.setName("Delivered");
            this.dataManager.save(redeemTrackerStatus);
        }
        if(redeemTrackerStatuses.stream().noneMatch(a -> a.getCode().equals("G46"))) {
            RedeemTrackerStatus redeemTrackerStatus = this.dataManager.create(RedeemTrackerStatus.class);
            redeemTrackerStatus.setCode("G46");
            redeemTrackerStatus.setName("Undelivered");
            this.dataManager.save(redeemTrackerStatus);
        }
    }


    private void initTransactionMethod() {
        List<TransMethod> transMethods = this.dataManager.load(TransMethod.class).all().list();

        if(transMethods.stream().noneMatch(a -> a.getCode().equals("H01"))) {
            TransMethod transMethod = this.dataManager.create(TransMethod.class);
            transMethod.setCode("H01");
            transMethod.setName("DuitNow Online Backing (DNOB)");
            this.dataManager.save(transMethod);
        }
        if(transMethods.stream().noneMatch(a -> a.getCode().equals("H02"))) {
            TransMethod transMethod = this.dataManager.create(TransMethod.class);
            transMethod.setCode("H02");
            transMethod.setName("Credit Card / Debit Card");
            this.dataManager.save(transMethod);
        }
        if(transMethods.stream().noneMatch(a -> a.getCode().equals("H03"))) {
            TransMethod transMethod = this.dataManager.create(TransMethod.class);
            transMethod.setCode("H03");
            transMethod.setName("DuitNow Auto Debit (DNAD)");
            this.dataManager.save(transMethod);
        }
    }

    private void initAccountBGD() {
        String query1="select e from AccountBgd e " +
                "where e.regType.code = :regTypeCode1";
        List<AccountBgd> accountBgdsPlatformOwner = this.dataManager.load(AccountBgd.class)
                .query(query1)
                .parameter("regTypeCode1", "D01")
                .list();

        if((long) accountBgdsPlatformOwner.size() < 2) {
            //create asset wallet for admin
            AccountBgd assetWallet = this.dataManager.create(AccountBgd.class);
            assetWallet.setCurrentAmount(BigDecimal.ZERO);
            String query2="select e from RegType e where e.code = :code";
            assetWallet.setRegType(this.dataManager.load(RegType.class)
                    .query(query2)
                    .parameter("code", "D01").one());
            String query3="select e from UnitOfMeasure e where e.code = :code";
            assetWallet.setUnitMeasure(this.dataManager.load(UnitOfMeasure.class)
                    .query(query3)
                    .parameter("code", "J02").one());
            String query4="select e from WalletType e where e.code = :code";
            assetWallet.setWalletType(this.dataManager.load(WalletType.class)
                    .query(query4)
                    .parameter("code", "E02").one());
//            assetWallet.setCreatedBy("BgdAdmin");
            assetWallet.setAccOwner("BgdAdmin");
            this.dataManager.save(assetWallet);

            //create cash wallet for admin
            AccountBgd cashWallet = this.dataManager.create(AccountBgd.class);
            cashWallet.setCurrentAmount(BigDecimal.ZERO);
            String query5="select e from RegType e where e.code = :code";
            cashWallet.setRegType(this.dataManager.load(RegType.class)
                    .query(query5)
                    .parameter("code", "D01")
                    .one());
            String query6="select e from UnitOfMeasure e where e.code = :code";
            cashWallet.setUnitMeasure(this.dataManager.load(UnitOfMeasure.class)
                    .query(query6)
                    .parameter("code", "J01").one());
            String query7="select e from WalletType e where e.code = :code";
            cashWallet.setWalletType(this.dataManager.load(WalletType.class)
                    .query(query7)
                    .parameter("code", "E01").one());
//            cashWallet.setCreatedBy("BgdAdmin");
            cashWallet.setAccOwner("BgdAdmin");
            this.dataManager.save(cashWallet);
        }

        String query8="select e from AccountBgd e " +
                "where e.regType.code = :regTypeCode1";
        List<AccountBgd> accountBgdsSupplier = this.dataManager.load(AccountBgd.class)
                .query(query8)
                .parameter("regTypeCode1", "D04")
                .list();

        if((long) accountBgdsSupplier.size() < 2) {
            //create asset wallet for admin
            AccountBgd assetWallet = this.dataManager.create(AccountBgd.class);
            assetWallet.setCurrentAmount(BigDecimal.ZERO);
            String query9="select e from RegType e where e.code = :code";
            assetWallet.setRegType(this.dataManager.load(RegType.class)
                    .query(query9)
                    .parameter("code", "D04").one());
            String query10="select e from UnitOfMeasure e where e.code = :code";
            assetWallet.setUnitMeasure(this.dataManager.load(UnitOfMeasure.class)
                    .query(query10)
                    .parameter("code", "J02").one());
            String query11="select e from WalletType e where e.code = :code";
            assetWallet.setWalletType(this.dataManager.load(WalletType.class)
                    .query(query11)
                    .parameter("code", "E02").one());
//            assetWallet.setCreatedBy("AceAdmin");
            assetWallet.setAccOwner("AceAdmin");
            this.dataManager.save(assetWallet);

            //create cash wallet for admin
            AccountBgd cashWallet = this.dataManager.create(AccountBgd.class);
            cashWallet.setCurrentAmount(BigDecimal.ZERO);
            String query12="select e from RegType e where e.code = :code";
            cashWallet.setRegType(this.dataManager.load(RegType.class)
                    .query(query12)
                    .parameter("code", "D04")
                    .one());
            String query13="select e from UnitOfMeasure e where e.code = :code";
            cashWallet.setUnitMeasure(this.dataManager.load(UnitOfMeasure.class)
                    .query(query13)
                    .parameter("code", "J01").one());
            String query14="select e from WalletType e where e.code = :code";
            cashWallet.setWalletType(this.dataManager.load(WalletType.class)
                    .query(query14)
                    .parameter("code", "E01").one());
//            cashWallet.setCreatedBy("AceAdmin");
            cashWallet.setAccOwner("AceAdmin");
            this.dataManager.save(cashWallet);
        }

    }
}
