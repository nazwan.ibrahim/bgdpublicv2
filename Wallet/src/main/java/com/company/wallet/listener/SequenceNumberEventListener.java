package com.company.wallet.listener;

import com.company.wallet.entity.SequenceNumber;
import io.jmix.core.DataManager;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Objects;

@Component
public class SequenceNumberEventListener {

    @Autowired
    private DataManager dataManager;

    @EventListener
    public void onSequenceNumberLoading(EntityLoadingEvent<SequenceNumber> event) {

    }

    @EventListener
    public void onSequenceNumberSaving(EntitySavingEvent<SequenceNumber> event) {

    }

    @EventListener
    public void onSequenceNumberChangedBeforeCommit(EntityChangedEvent<SequenceNumber> event) {
//        SequenceNumber sequenceNumber = this.dataManager.load(SequenceNumber.class).id(event.getEntityId()).one();

//        if(!Objects.equals(event.getChanges().getOldValue("seqNo"), sequenceNumber.getSeqNo())) {
//
//        }

    }

    @TransactionalEventListener
    public void onSequenceNumberChangedAfterCommit(EntityChangedEvent<SequenceNumber> event) {

    }
}