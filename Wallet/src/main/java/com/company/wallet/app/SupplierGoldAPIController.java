package com.company.wallet.app;

import com.company.wallet.entity.IntegrationService;
import com.company.wallet.entity.PriceQuery;
import com.company.wallet.form.IntegrationServiceForm;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.jmix.core.DataManager;
import net.minidev.json.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/supplierGoldAPI")
public class SupplierGoldAPIController {
    private final Logger logger = LogManager.getLogger(SupplierGoldAPIController.class.getName());
    @Autowired
    private DataManager dataManager;
    @Autowired
    private Environment env;
    @Autowired
    private WalletApiController walletApiController;

    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
            HttpClientBuilder.create().build());

    public Date getDateToday() {

        return Date.from(LocalDate.now().atStartOfDay(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
    }

    public Date getDateTimeNow() {
        return Date.from(LocalDateTime.now().atZone(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
    }

    public String generateTimestamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(getDateTimeNow());
    }

    public String generateDigest(String params) throws NoSuchAlgorithmException {
        String signature = params+ "&key=" +env.getProperty("ace_secretKey");

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedHash = digest.digest(signature.getBytes(StandardCharsets.UTF_8));

        String hashedString = bytesToHex(encodedHash);

        return hashedString;
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (byte b : hash) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public IntegrationService updateIntegrationResponse(String code, String serviceName, String url, String requestHeader, String requestBody,
                                          String responseHTTP, String responseHeader, String responseBody, Boolean successStatus) {

        IntegrationService integration = this.dataManager.create(IntegrationService.class);
        try {

            integration.setCode(code);
            integration.setServiceName(serviceName);
            integration.setUrl(url);
            integration.setRequestHeader(requestHeader);
            integration.setRequestBody(requestBody);
            integration.setResponseHTTP(responseHTTP);
            integration.setResponseHeader(responseHeader);
            integration.setResponseBody(responseBody);
            integration.setSuccess(successStatus);

            this.dataManager.save(integration);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        } finally {
            return integration;
        }
    }

    public ResponseEntity<String> priceAdjustment(String transactionCode,BigDecimal margin, String uomCode, ResponseEntity<String> supplierResponsePrice, String reference) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String priceRequestId = objectMapper.readValue(supplierResponsePrice.getBody(), new TypeReference<Map<String, Object>>() {}).get("price_request_id").toString();
            String price = objectMapper.readValue(supplierResponsePrice.getBody(), new TypeReference<Map<String, Object>>() {}).get("total_price").toString();
            BigDecimal supplierPrice = new BigDecimal(price);
            BigDecimal bursaPrice = supplierPrice;
            if(transactionCode.equals("F05")) { //INVESTOR buy
                if (uomCode.equals("J01")) { // RM
                    bursaPrice = supplierPrice.add(margin);
                } else if (uomCode.equals("J04")) { //%
                    bursaPrice = supplierPrice.add(supplierPrice.multiply(margin.divide(BigDecimal.valueOf(100))));
                }
            } else if(transactionCode.equals("F06")) { //INVESTOR sell
                if (uomCode.equals("J01")) { // RM
                    bursaPrice = supplierPrice.subtract(margin);
                } else if (uomCode.equals("J04")) { //%
                    bursaPrice = supplierPrice.subtract(supplierPrice.multiply(margin.divide(BigDecimal.valueOf(100))));
                }
            }

            JSONObject supplier = new JSONObject();
            supplier.put("code", "A01");
            supplier.put("name", "Supplier Price");
            supplier.put("price", supplierPrice);

            JSONObject bursa = new JSONObject();
            bursa.put("code", "A02");
            bursa.put("name", "Bursa Price");
            bursa.put("price", bursaPrice);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("priceRequestID", priceRequestId);
            jsonObject.put("reference", reference);
            jsonObject.put("A01", supplier);
            jsonObject.put("A02", bursa);

            PriceQuery priceQuery = dataManager.create(PriceQuery.class);
            priceQuery.setSupplierPrice(supplierPrice);
            priceQuery.setBursaPrice(bursaPrice);
            priceQuery.setPriceRequestID(priceRequestId);
            priceQuery.setReference(reference);
            priceQuery.setMarginUomCode(uomCode);
            priceQuery.setMargin(margin);
            priceQuery.setTransactionCode(transactionCode);
            dataManager.save(priceQuery);

            return ResponseEntity.ok(jsonObject.toJSONString());
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("queryBuyPrice") //INVESTOR SELL, BURSA BUY
    public ResponseEntity<String> queryBuyPrice(BigDecimal margin, String uomCode) {
        String reference = "F06" + new SimpleDateFormat("yyyyMMdd").format(getDateToday()) + walletApiController.generateSeqNo("F05", "6");
        ResponseEntity<String> response = queryPrice("ITR03/01",env.getProperty("ace_actionPriceBuy"), "F05", reference);

        return priceAdjustment("F06", margin, uomCode, response, reference);
    }

    @GetMapping("querySellPrice") //INVESTOR BUY, BURSA SELL
    public ResponseEntity<String> querySellPrice(BigDecimal margin, String uomCode) {
        String reference = "F05" + new SimpleDateFormat("yyyyMMdd").format(getDateToday()) + walletApiController.generateSeqNo("F06", "6");
        ResponseEntity<String> response = queryPrice("ITR03/02",env.getProperty("ace_actionPriceSell"), "F06", reference);

        return priceAdjustment("F05", margin, uomCode, response, reference);
    }
    public ResponseEntity<String> queryPrice(String serviceCode, String action, String transactionCode, String reference) {
        try {
            String timestamp = generateTimestamp();
            String params = "version=" +env.getProperty("ace_version")+
                    "&merchant_id=" +env.getProperty("ace_merchantID")+
                    "&action=" +action+
                    "&product=" +env.getProperty("ace_product")+
                    "&currency=" +env.getProperty("ace_currency")+
                    "&reference=" +reference+
                    "&timestamp=" +timestamp;
            String digest = generateDigest(params);
            String url = env.getProperty("ace_urlAPI") + "?" + params + "&digest=" +digest;

            //generate request Body
            String jsonBody = "{\"name1\":\"value1\",\"name2\":\"value2\",\"name3\":\"value3\"}";

            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>("", headers);

            try { //invoke api
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);



                //update call response on table IntegrationService as a record
                updateIntegrationResponse(serviceCode, action, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true); //success = true

                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
                updateIntegrationResponse(serviceCode, action, url, request.getHeaders().toString(), request.getBody(),
                        HttpStatus.BAD_REQUEST.toString(), "", exHttp.getMessage(), false); //success = true

                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            //update call response on table IntegrationService as a record
                updateIntegrationResponse(serviceCode, action, env.getProperty("ace_urlAPI"), "", "",
                        "", "", ex.getMessage(), false); //success = false
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("sportOrderBuy")
    public ResponseEntity<String> sportOrderBuy(String priceRequestId, BigDecimal price, BigDecimal weight, String reference) {
        return spotOrder("ITR03/03",env.getProperty("ace_actionSpotBuy"), priceRequestId, price, weight, reference);
    }

    @GetMapping("sportOrderSell")
    public ResponseEntity<String> sportOrderSell(String priceRequestId, BigDecimal price, BigDecimal weight, String reference) {
        return spotOrder("ITR03/04",env.getProperty("ace_actionSpotSell"), priceRequestId, price, weight, reference);
    }

//    @GetMapping("spotOrder")
    public ResponseEntity<String> spotOrder(String serviceCode, String action, String priceRequestId, BigDecimal price, BigDecimal weight, String reference) {
        logger.info("Start SpotOrder: "+serviceCode+action+priceRequestId+price.toString()+price.toString()+weight.toString()+reference);
        try{
//            logger.info("Start SpotOrder: "+serviceCode+action+priceRequestId+price.toString()+price.toString()+weight.toString()+reference);
            String timestamp = generateTimestamp();
            BigDecimal amount = price.multiply(weight);
//            String referenceId = new BigInteger(12 * 5, new SecureRandom()).toString(32).substring(0, 12);
//            String referenceId = reference.substring(reference.length() - 12);
//            String referenceId = reference.charAt(2) + reference.substring(5, 11) + reference.substring(12);
            String referenceId = reference.charAt(2) + reference.substring(5, 11) +env.getProperty("ace_env_srcCode")+ reference.substring(13);
//
//            String referenceId = reference;
            String params = "version=" +env.getProperty("ace_version")+
                    "&merchant_id=" +env.getProperty("ace_merchantID")+
                    "&action=" +action+
                    "&ref_id=" +referenceId+
                    "&price_request_id=" +priceRequestId+
                    "&future_ref_id=" +
                    "&total_price=" +price+
                    "&product=" +env.getProperty("ace_product")+
                    "&order_type=" +"weight"+
                    "&weight=" +weight+
                    "&amount=" +amount+
                    "&reference=" +reference+
                    "&timestamp=" +timestamp;
            logger.info("spotOrder_params: "+params);

            String digest = generateDigest(params);
            logger.info("spotOrder_digest: "+digest);
            
            String url = env.getProperty("ace_urlAPI") + "?" + params + "&digest=" +digest;
            logger.info("sportOrder_url: "+url);

            //generate request Body
            String jsonBody = "{\"name1\":\"value1\",\"name2\":\"value2\",\"name3\":\"value3\"}";

            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>("", headers);

            try { //invoke api
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

                //update call response on table IntegrationService as a record
                updateIntegrationResponse(serviceCode, action, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true); //success = true
                logger.info("Response -> spotOrder :" + HttpStatus.OK);
                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
                updateIntegrationResponse(serviceCode, action, url, request.getHeaders().toString(), request.getBody(),
                        HttpStatus.BAD_REQUEST.toString(), "", exHttp.getMessage(), true); //success = true

                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            //update call response on table IntegrationService as a record
            updateIntegrationResponse(serviceCode, action, env.getProperty("ace_urlAPI"), "", "",
                    "", "", ex.getMessage(), false); //success = false

            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("redemption")
    public ResponseEntity<IntegrationServiceForm> redemption(BigDecimal redeemGram, String reference, String itemSerialNo, BigDecimal itemDenomination, BigDecimal itemQuantity ,
                                                             String deliveryName1, String deliveryName2, String deliveryContact1, String deliveryContact2, String deliveryAddress1, String deliveryAddress2, String deliveryAddress3, String deliveryAddress4, String deliveryState, String deliveryPostcode) {
        logger.info("ace_redeption: "+redeemGram.toString()+reference+itemSerialNo+itemDenomination.toString()+itemQuantity.toString()
        +deliveryName1+deliveryName2+deliveryContact1+deliveryContact2+deliveryAddress1+deliveryAddress2+deliveryAddress3+deliveryAddress4+deliveryState+deliveryPostcode);

        deliveryContact1=deliveryContact1.replace("+","");
        deliveryContact1=deliveryContact1.replace(" ","");
        deliveryContact2=deliveryContact2.replace("+","");
        deliveryContact2=deliveryContact2.replace(" ","");


        String serviceCode = "ITR03/05";
        String action = "redemption";
        IntegrationServiceForm serviceForm = new IntegrationServiceForm();
        try {
            String timestamp = generateTimestamp();
//            String referenceId = new BigInteger(12 * 5, new SecureRandom()).toString(32).substring(0, 12);
//            String referenceId = reference.substring(reference.length() - 12);
//            String referenceId = reference.charAt(2) + reference.substring(5, 11) + reference.substring(12);
            String referenceId = reference.charAt(2) + reference.substring(5, 11) +env.getProperty("ace_env_srcCode")+ reference.substring(13);
//            String referenceId = reference;
            String type = "delivery";
            String item = "{\"item\":[{" +
                    "\"serialno\":\"" +itemSerialNo+ "\"," +
                    "\"denomination\":\"" +itemDenomination+ "\"," +
                    "\"quantity\":\"" +itemQuantity+ "\"" +
                    "}]}";


            String itemDigest = "[{" +
                    "\"serialno\":\"" +itemSerialNo+ "\"," +
                    "\"denomination\":\"" +itemDenomination+ "\"," +
                    "\"quantity\":\"" +itemQuantity+ "\"" +
                    "}]";

            String delivery_info = "{\"delivery_info\":{" +
                    "\"contactname1\":\"" +deliveryName1+ "\"," +
                    "\"contactname2\":\"" +deliveryName2+ "\"," +
                    "\"contact_mobile1\":\"" +deliveryContact1+ "\"," +
                    "\"contact_mobile2\":\"" +deliveryContact2+ "\"," +
                    "\"address1\":\"" +deliveryAddress1+ "\"," +
                    "\"address2\":\"" +deliveryAddress2+ "\"," +
                    "\"address3\":\"" +deliveryAddress3+ "\"," +
                    "\"address4\":\"" +deliveryAddress4+ "\"," +
                    "\"state\":\"" +deliveryState+ "\"," +
                    "\"postcode\":\"" +deliveryPostcode+ "\"" +
                    "}}";

            String delivery_infoDigest = "{" +
                    "\"contactname1\":\"" +deliveryName1+ "\"," +
                    "\"contactname2\":\"" +deliveryName2+ "\"," +
                    "\"contact_mobile1\":\"" +deliveryContact1+ "\"," +
                    "\"contact_mobile2\":\"" +deliveryContact2+ "\"," +
                    "\"address1\":\"" +deliveryAddress1+ "\"," +
                    "\"address2\":\"" +deliveryAddress2+ "\"," +
                    "\"address3\":\"" +deliveryAddress3+ "\"," +
                    "\"address4\":\"" +deliveryAddress4+ "\"," +
                    "\"state\":\"" +deliveryState+ "\"," +
                    "\"postcode\":\"" +deliveryPostcode+ "\"" +
                    "}";

            String params = "version=" +env.getProperty("ace_version")+
                    "&merchant_id=" +env.getProperty("ace_merchantID")+
                    "&action=" +action+
                    "&ref_id=" +referenceId+
                    "&type=" +type+
                    "&redeem_gram=" +redeemGram+
                    "&branch_id=\"\"" +
//                    "&item=" +URLEncoder.encode(itemDigest, "UTF-8")+
//                    "&delivery_info=" +URLEncoder.encode(delivery_infoDigest, "UTF-8")+
                    "&item=" +item+
                    "&delivery_info=" +delivery_info+
                    "&schedule_info=\"\"" +
                    "&reference=" +reference+
                    "&timestamp=" +timestamp;

            String paramsDigest = "version=" +env.getProperty("ace_version")+
                    "&merchant_id=" +env.getProperty("ace_merchantID")+
                    "&action=" +action+
                    "&ref_id=" +referenceId+
                    "&type=" +type+
                    "&redeem_gram=" +redeemGram+
                    "&branch_id=" +1+
                    "&item=" + itemDigest+
                    "&delivery_info=" +delivery_infoDigest+
                    "&schedule_info=\"\"" +
                    "&reference=" +reference+
                    "&timestamp=" +timestamp;

            logger.info("ace_redemption_params: "+paramsDigest);
            String digest = generateDigest(paramsDigest);
            logger.info("ace_redemption_digest: "+digest);
            String url = env.getProperty("ace_urlAPI") + "?" + paramsDigest + "&digest=" +digest;
            logger.info("ace_redemption_url: "+url);

            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();

//            //generate request using HttpEntity(org.springframework.http.*)
//            HttpEntity<String> request = new HttpEntity<>(null,null);

            // ** Using okhttp3 ** //
            OkHttpClient client = new OkHttpClient().newBuilder().build();
            okhttp3.Request request = new Request.Builder().url(url).get().build();

            try { //invoke api
//                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
//                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
//
//                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(serviceCode, action, url, request.getHeaders().toString(), request.getBody(),
//                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true); //success = true

                // ** Using okhttp3 ** //
                okhttp3.Response response= client.newCall(request).execute();
                String responseBodyString = "";
                okhttp3.ResponseBody responseBody  = response.body();
                if (responseBody  != null){
                    responseBodyString = responseBody.string();
                }
                IntegrationService service = updateIntegrationResponse(serviceCode, action, url, request.headers().toString(), String.valueOf(request.body()),
                        String.valueOf(response.code()), response.headers().toString(), responseBodyString, true); //success = true


//                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());
                serviceForm.setId(service.getId().toString());
                serviceForm.setResponse(responseBodyString);
//                return ResponseEntity.status(HttpStatus.OK).body(responseBodyString);
                return ResponseEntity.status(HttpStatus.OK).body(serviceForm);
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
                IntegrationService service = updateIntegrationResponse(serviceCode, action, url, request.headers().toString(), String.valueOf(request.body()),
                        HttpStatus.BAD_REQUEST.toString(), "", exHttp.getMessage(), true); //success = true

//                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
                serviceForm.setId(service.getId().toString());
                serviceForm.setResponse(exHttp.getMessage());
                return ResponseEntity.status(exHttp.getStatusCode()).body(serviceForm);
            } catch (HttpServerErrorException ex) {
                logger.error(ex.getMessage());
                //update call response on table IntegrationService as a record
                IntegrationService service = updateIntegrationResponse(serviceCode, action, url, request.headers().toString(), String.valueOf(request.body()),
                        HttpStatus.BAD_REQUEST.toString(), "", ex.getMessage(), true); //success = true

                serviceForm.setId(service.getId().toString());
                serviceForm.setResponse(ex.getMessage());
                return ResponseEntity.status(ex.getStatusCode()).body(serviceForm);
            } catch (Exception ex) {
                logger.error(ex.getMessage());
                //update call response on table IntegrationService as a record

                IntegrationService service = updateIntegrationResponse(serviceCode, action, url, request.headers().toString(), String.valueOf(request.body()),
                        HttpStatus.BAD_REQUEST.toString(), "", ex.getMessage(), true); //success = true

                serviceForm.setId(service.getId().toString());
                serviceForm.setResponse(ex.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(serviceForm);
            }


        } catch (Exception ex) {
            logger.error(ex.getMessage());
            //update call response on table IntegrationService as a record
            IntegrationService service = updateIntegrationResponse(serviceCode, action, env.getProperty("ace_urlAPI"), "", "",
                    "", "", ex.getMessage(), false); //success = false

            serviceForm.setId(service.getId().toString());
            serviceForm.setResponse(ex.getMessage());
            return ResponseEntity.badRequest().body(serviceForm);
        }
    }

    @GetMapping("logisticStatus")
    public ResponseEntity<String> logisticStatus(String redemption_id) {
        logger.info("get logistic status was called for redemption_id: "+redemption_id);
        String serviceCode = "ITR03/06";
        String action = "getlogisticstatus";
        try {

//            String timestamp = generateTimestamp();
            String params = "version=" +env.getProperty("ace_version")+
                    "&merchant_id=" +env.getProperty("ace_merchantID")+
                    "&action=" +action+
                    "&ref_id=" +redemption_id;

            String digest = generateDigest(params);
            String url = env.getProperty("ace_urlAPI") + "?" + params + "&digest=" +digest;

            //generate request Body
//            String jsonBody = "{\"name1\":\"value1\",\"name2\":\"value2\",\"name3\":\"value3\"}";

            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>("", headers);

            try { //invoke api
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
                logger.info("get logistic status response: "+response.getBody().toString());
                //update call response on table IntegrationService as a record
                updateIntegrationResponse(serviceCode, action, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true); //success = true

                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
                updateIntegrationResponse(serviceCode, action, url, request.getHeaders().toString(), request.getBody(),
                        HttpStatus.BAD_REQUEST.toString(), "", exHttp.getMessage(), false); //success = true

                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            //update call response on table IntegrationService as a record
            updateIntegrationResponse(serviceCode, action, env.getProperty("ace_urlAPI"), "", "",
                    "", "", ex.getMessage(), false); //success = false
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
}
