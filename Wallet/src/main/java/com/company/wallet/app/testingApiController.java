package com.company.wallet.app;

import com.company.wallet.form.GeneratePaynetSignatureForm;
import com.company.wallet.services.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Objects;
import java.util.stream.Collectors;

//@Component
@RestController
@RequestMapping("Testing")
public class testingApiController {

    @Autowired
    private KafkaProducer kafkaProducer;

    @PostMapping(value = "/publishKafka")
    public ResponseEntity sendMessageToKafkaTopic(@RequestParam("message") String message) {
        this.kafkaProducer.sendMessage(message);

        return ResponseEntity.ok("Message Send to Kafka. message:" +message);
    }

    @PostMapping(value = "/publishKafkaJson")
    public ResponseEntity sendMessageJsonToKafkaTopic(@RequestParam("message") String message) {
        this.kafkaProducer.sendMessageJson();

        return ResponseEntity.ok("Message Send to Kafka. message:" +message);
    }

    private static final String KEY_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

    @PostMapping("paynetSignature")
    public ResponseEntity<String> generatPaynetSignature (@RequestBody GeneratePaynetSignatureForm form)throws Exception{
        // path to private key and public certificate
//        String privateKeyPath = "C:\\Users\\izaidi\\paynet\\bgdbursadev.key";
        String privateKeyPath = "C:\\Users\\izaidi\\paynet\\prod\\prod-bgd.key";
//        String publicKeyPath = "C:\\Users\\izaidi\\paynet\\bgdselfpubkey.cer";

        // message to sign
//        String clientId = "M0010152";
//        String messageId = "20230411M0010152650OBA00000003";
//        String transactionId ="20230411M001015265000000003";
        String message ="";
        if(Objects.equals(form.getEndpointName(), "retrieve bank list"))
            message = form.getClientId()+"RPPEMYKL"+form.getMessageId()+form.getTransactionId()+form.getMessageId()+form.getClientId();

        else if(Objects.equals(form.getEndpointName(), "initiate payment"))
            message = form.getClientId()+"RPPEMYKL"+form.getMessageId()+form.getTransactionId()+"RPPEMYKL"+form.getEndToEndId()+form.getAmount()+form.getClientId();
        else if(Objects.equals(form.getEndpointName(), "status inquiry"))
            message = form.getClientId()+"RPPEMYKL"+form.getMessageId()+form.getTransactionId()+form.getEndToEndId()+form.getEndToEndId();
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("");
        // signature to verify
        String responseSignature = "signature_to_verify";

        // Request Signing (Use to construct Request Message X-Signature)
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(createPrivateKeyInstance(privateKeyPath));
        signature.update(message.getBytes(StandardCharsets.UTF_8));

//        System.out.println(Base64.getEncoder().encodeToString(signature.sign()));
        return ResponseEntity.status(HttpStatus.OK).body(Base64.getEncoder().encodeToString(signature.sign()));
    }

    public static PrivateKey createPrivateKeyInstance(String pathToKey)
            throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        try (
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(new FileInputStream(pathToKey))
                )
        ) {
            String content = reader
                    .lines()
                    .filter(line -> !line.startsWith("-----"))
                    .collect(Collectors.joining());
            KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
                    Base64.getDecoder().decode(content)
            );
            return factory.generatePrivate(keySpec);
        }
    }

    public static PublicKey createPublicKeyInstance(String pathToKey)
            throws IOException, NoSuchAlgorithmException, CertificateException {
        try (FileInputStream reader = new FileInputStream(pathToKey)) {
            CertificateFactory f = CertificateFactory.getInstance("X.509");
            X509Certificate certificate = (X509Certificate) f.generateCertificate(
                    reader
            );
            return certificate.getPublicKey();
        }
    }

}
