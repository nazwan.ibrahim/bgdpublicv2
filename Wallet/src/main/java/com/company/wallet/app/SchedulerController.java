package com.company.wallet.app;

import com.company.wallet.entity.ArchiveTransactionHistories;
import com.company.wallet.entity.TransactionHistoriesView;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jmix.core.DataManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class SchedulerController {
    static Logger logger = LogManager.getLogger(SchedulerController.class.getName());
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private DataManager dataManager;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

//    public void archiveTransactionx() {
//        try {
//            logger.info("************* Schedule transactionHistoriesViewArchive Start ********************");
////            EntityManagerFactory emf = Persistence.createEntityManagerFactory("main");
////            this.entityManager = emf.createEntityManager();
//
//            LocalDate currentDate = LocalDate.now();
//            LocalDate yesterdayDate = currentDate.minusDays(1);
//
//            String sql = "create table Archive_TransactionHistory_"+ yesterdayDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))+ " " +
//                    "as select * from transaction_histories_view thv " +
//                    "where thv.created_date < '" +currentDate.toString()+ "' and thv.created_date > '" +yesterdayDate.toString()+ "' ";
//            Query query = entityManager.createNativeQuery(sql);
////            query.executeUpdate();
//            List<Object[]> results = query.getResultList();
//
//            String sql2 = "create table Archive_TransactionAcct_"+ yesterdayDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))+ " " +
//                    "as select * from transaction_acct ta " +
//                    "where ta.created_date < '" +currentDate.toString()+ "' and ta.created_date > '" +yesterdayDate.toString()+ "' ";
//            Query query2 = entityManager.createNativeQuery(sql2);
////            query2.executeUpdate();
//            List<Object[]> results2 = query2.getResultList();
//
//
//            // Process the result if applicable
//            logger.info("************* Schedule transactionHistoriesViewArchive End ********************");
//        } catch (Exception ex) {
//            logger.error(ex.getMessage());
//        }
//    }

    @Transactional
    public void archiveTransaction(String date) {
        try{
            logger.info("************* Schedule transactionHistoriesViewArchive Start ********************");

            LocalDate currentDate = LocalDate.parse(date, formatter);
            LocalDate yesterday = currentDate.minusDays(1);

            String sql = "insert into archive_transaction_histories (id, acct_number , acc_owner, wallet_code, wallet, reference_id, transaction_code, transaction, " +
                    "amount, uom_code, uom, status_code, status, created_date, percent_, receipt_number, transaction_method) " +
                    "select id, acct_number, acc_owner, wallet_code, wallet, reference_id, transaction_code, transaction, amount, uom_code, uom, " +
                    "status_code, status, created_date, percent_, receipt_number, transaction_method " +
                    "from transaction_histories_view where created_date < '" +currentDate.toString()+ "' and created_date > '" +yesterday.toString()+ "' ";

            entityManager.createNativeQuery(sql)
                    .executeUpdate();
//            query.getResultList();

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
}