package com.company.wallet.app;

//import com.google.common.net.MediaType;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.company.wallet.entity.*;
import com.company.wallet.form.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jose.shaded.json.parser.JSONParser;
import elemental.json.Json;
import io.jmix.core.DataManager;
import io.jmix.core.SaveContext;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/paymentGatewayAPI")
public class PaymentGatewayAPIController {
    private final Logger logger = LogManager.getLogger(PaymentGatewayAPIController.class.getName());

    @Autowired
    private DataManager dataManager;

    @Autowired
    private Environment env;
    @Autowired
    private WalletApiController walletApiController;

    private AmazonS3 s3Client = null;

    private final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
            HttpClientBuilder.create().build());

//    @Autowired
//    private RestTemplate restTemplate;

//    @Autowired
//    private ObjectMapper objectMapper;

    public String base64Decoder (String text) {
        return Base64.getDecoder().decode(text).toString();
    }

    public String base64Encoder (String text) {
        return Base64.getEncoder().encodeToString(text.getBytes(StandardCharsets.UTF_8));
    }

    public void setS3Client(){
        String regionName = env.getProperty("AWS_REGION");
        Regions clientRegion = Regions.fromName(regionName);
        String accessKey = env.getProperty("AWS_ACCESS_KEY");
        String secretKey = env.getProperty("AWS_SECRET_KEY");
        if(accessKey != null && secretKey!=null){
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
            s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                    .withRegion(clientRegion)
                    .build();
        }

    }

    private String access_token = "";
    private LocalDateTime tokenExpired = LocalDateTime.now();
    private String paynet_access_token = "";
    private LocalDateTime paynetTokenExpired = LocalDateTime.now();

    private static final String PAYNET_KEY_ALGORITHM = "RSA";
    private static final String PAYNET_SIGNATURE_ALGORITHM = "SHA256withRSA";

    public Date getDateToday() {

        return Date.from(LocalDate.now().atStartOfDay(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
    }

    public Date getDateTimeNow() {
        return Date.from(LocalDateTime.now().atZone(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
    }

    public void updateIntegrationResponse(String code, String serviceName, String url, String requestHeader, String requestBody,
                                          String responseHTTP, String responseHeader, String responseBody, Boolean successStatus) {


        try {
            IntegrationService integration = this.dataManager.create(IntegrationService.class);
            integration.setCode(code);
            integration.setServiceName(serviceName);
            integration.setUrl(url);
            integration.setRequestHeader(requestHeader);
            integration.setRequestBody(requestBody);
            integration.setResponseHTTP(responseHTTP);
            integration.setResponseHeader(responseHeader);
            integration.setResponseBody(responseBody);
            integration.setSuccess(successStatus);

            this.dataManager.save(integration);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    ///////////////////////////////Ambank section (Token, Account Enquiry, Credit Transfer, etc)/////////////////////////////////

    public String generateSeqNo() {
        Date dateToday = getDateToday();

        Integer seqNo = 0;
        String query1="select e from AmbankSequenceNo e " +
                "where e.seqDate = :dateToday";
        List<AmbankSequenceNo> currentSeqNo = this.dataManager.load(AmbankSequenceNo.class)
                .query(query1)
                .parameter("dateToday", dateToday)
                .list();

        if(currentSeqNo.size() > 0) {
            seqNo = currentSeqNo.get(0).getSeqNo() + 1;

            currentSeqNo.get(0).setSeqNo(seqNo);
            this.dataManager.save(currentSeqNo.get(0));

        } else {
            seqNo = 1;

            AmbankSequenceNo sequenceNo = this.dataManager.create(AmbankSequenceNo.class);
            sequenceNo.setSeqDate(dateToday);
            sequenceNo.setSeqNo(seqNo);
            this.dataManager.save(sequenceNo);
        }

        return String.format("%06d", seqNo);

    }
    public String generateSrcRefNo() {
//        return env.getProperty("ambank_PrefixId") + new SimpleDateFormat("ddMMyyyy").format(getDateToday()) + generateSeqNo();

        return env.getProperty("ambank_PrefixId") + new SimpleDateFormat("ddMMyyyy").format(getDateToday()) + env.getProperty("ambank_env_srcCode") + walletApiController.generateSeqNo(env.getProperty("ambank_PrefixId"), "5");
    }

    @PostMapping("token")
    public ResponseEntity<String> token () {
        String code = "ITR01/01";
        String serviceName = "Ambank Token";
        String url = env.getProperty("ambank_TokenAPI");
        try {
//            Unirest.setTimeouts(0, 0);
//            HttpResponse<String> response = Unirest.post("https://amgatewayuat.ambg.com.my/api/oauth/v2.0/token")
//                    .header("ClientID", "0f26f692-18cc-479f-9c21-7706f98bf71f")
//                    .header("Authorization", "Basic MGYyNmY2OTItMThjYy00NzlmLTljMjEtNzcwNmY5OGJmNzFmOjBkYjdlOTgyLWNhMzAtNDlkNS04ZjgyLTlhYTQwN2I3OWQxNQ==")
//                    .header("Content-Type", "application/x-www-form-urlencoded")
//                    .field("grant_type", "client_credentials")
//                    .field("scope", "resource.READ,resource.WRITE")
//                    .asString();

//            OkHttpClient client = new OkHttpClient().newBuilder()
//                    .build();
//            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
//            okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType, "grant_type=client_credentials&scope=resource.READ,resource.WRITE");
//            okhttp3.Request request = new okhttp3.Request.Builder()
//                    .url(env.getProperty("ambank_TokenAPI"))
//                    .method("POST", body)
//                    .addHeader("ClientID", env.getProperty("ambank_Channel_ClientId"))
////                    .addHeader("Authorization", "Basic "+base64Decoder(env.getProperty("ambank_Channel_ClientId")+":"+env.getProperty("ambank_ClientID_Secret")))
//                    .addHeader("Authorization", "Basic MGYyNmY2OTItMThjYy00NzlmLTljMjEtNzcwNmY5OGJmNzFmOjBkYjdlOTgyLWNhMzAtNDlkNS04ZjgyLTlhYTQwN2I3OWQxNQ==")
//                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
//                    .build();
//            okhttp3.Response response = client.newCall(request).execute();




            HttpHeaders headers = new HttpHeaders();
            headers.setBasicAuth(env.getProperty("ambank_Channel_ClientId"), env.getProperty("ambank_ClientID_Secret"));
//            headers.add("Authorization", "Basic MGYyNmY2OTItMThjYy00NzlmLTljMjEtNzcwNmY5OGJmNzFmOjBkYjdlOTgyLWNhMzAtNDlkNS04ZjgyLTlhYTQwN2I3OWQxNQ==");
//            headers.add("Content-Type", "application/x-www-form-urlencoded");
            headers.add("ClientID", env.getProperty("ambank_Channel_ClientId"));
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            body.add("grant_type", env.getProperty("ambank_grant_type"));
            body.add("scope", env.getProperty("ambank_scope"));


            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);
            logger.info("token request: "+request);
            RestTemplate restTemplate = new RestTemplate();
            try {
                ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

                ObjectMapper objectMapper = new ObjectMapper();
                access_token = objectMapper.readValue(response.getBody(), new TypeReference<Map<String, Object>>() {
                }).get("access_token").toString();
                String expires_in = objectMapper.readValue(response.getBody(), new TypeReference<Map<String, Object>>() {
                }).get("expires_in").toString();
                tokenExpired = LocalDateTime.now().plusSeconds(Long.parseLong(expires_in));

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), Objects.requireNonNull(request.getBody()).toString(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                return ResponseEntity.ok(response.getBody());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody().toString(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "","","", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public String generateAmbankSignature(String uri, String timeStamp, String requestBody) throws NoSuchAlgorithmException, InvalidKeyException {
        //////generate HeaderToSign
        String header = "{\"alg\":\"HS256\",\"typ\":\"JWT\",\"uri\":\""+uri+"\",\"iat\":\""+timeStamp+"\"}";
        String headerBase64 = base64Encoder(header);
        logger.info("base64 HeaderToSign -> " +headerBase64);

        //////generate BodyToSign
        String body = requestBody;
        String bodyBase64 = base64Encoder(body);
        logger.info("base64 BodyToSign -> " +bodyBase64);

        //////generate StringToHash
        String stringToHash = headerBase64.replace("=", "")+"."+bodyBase64.replace("=", "");
        logger.info("StringToHash -> " +stringToHash);

        //////generate Signature

        // Create an HMAC-SHA256 key from the secret key
        SecretKeySpec secretKeySpec = new SecretKeySpec(Objects.requireNonNull(env.getProperty("ambank_APIKey_Secret")).getBytes(), "HmacSHA256");

        // Create an HMAC-SHA256 hash object and initialize it with the secret key
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secretKeySpec);


        // Compute the HMAC-SHA256 hash of the message
        byte[] hashBytes = mac.doFinal(stringToHash.getBytes());

        // Encode the hash as a Base64 string
//        String hashString = Base64.getEncoder().encodeToString(hashBytes);
//        String hashString = base64Encoder(hashBytes.toString());
        String hashString = Base64.getEncoder().encodeToString(hashBytes);

        logger.info("FINAL HASH 64:"+hashString);
        logger.info("FINAL HASH FROM TEST:");
        test(stringToHash);

        //base64 kan hmacSha256
        String signature = hashString.replace("=", "");

        return signature;
    }
    @PostMapping("testsign")
    public ResponseEntity<String> test(@RequestParam String combinedsign) {
        try {

//            String combinedsign="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsInVyaSI6Ii9hcGkvUGF5bWVudEFQSS92My4wL0dldEFjY291bnRFbnF1aXJ5L0JNMTA3MDQyMDIzMDAwMDAxIiwiaWF0IjoiMDcwNDIwMjMxMDE4MDkifQ.eyJjcmVkaXRvckFjY291bnRUeXBlIjoiREZMVCIsInJlY2VpdmVyQklDIjoiTUZCQk1ZS0wiLCJjcmVkaXRvckFjY291bnRObyI6IjY0MDg3MDAxMDAxMzgwNiIsInNlY29uZFZhbGlkYXRpb25JbmRpY2F0b3IiOiJOIiwiaWRUeXBlIjoiMDEiLCJpZE5vIjoiOTgxMTA4NTQ1NjQzIn0";

            SecretKeySpec secretKeySpec = new SecretKeySpec(Objects.requireNonNull(env.getProperty("ambank_APIKey_Secret")).getBytes(), "HmacSHA256");

            // Create an HMAC-SHA256 hash object and initialize it with the secret key
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKeySpec);

            // Compute the HMAC-SHA256 hash of the message
            byte[] hashBytes = mac.doFinal(combinedsign.getBytes());
            logger.info("HASH Byte:"+hashBytes);
            // Encode the hash as a Base64 string
//            String hashString = base64Encoder(hashBytes);
//            String hashString = Base64.getEncoder().encodeToString(hashBytes.toString().getBytes(StandardCharsets.UTF_8));
            String hashString = Base64.getEncoder().encodeToString(hashBytes);
            logger.info("FINAL HASH 64:"+hashString);

//            MessageDigest md = MessageDigest.getInstance("SHA-256");
//            md.update(combinedsign.getBytes());
//            byte[] digestSHA = md.digest();
//            StringBuffer sb = new StringBuffer();
//            for (byte b : digestSHA) {
//                sb.append(String.format("%02x", b & 0xff));
//            }
//            String digest = String.valueOf(sb);


            return ResponseEntity.ok("Hello");

        } catch (HttpClientErrorException exHttp) {
            return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("accountEnquiryx")
    public ResponseEntity<String> accountEnquiryx(@RequestBody AmbankAccountEnquiryForm form) {
        String code = "ITR01/02";
        String serviceName = "Account Enquiry";
        //call api getAccountEnquiry
//        String srcRefNo = env.getProperty("ambank_PrefixId")
////                + LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy"))
//                + new SimpleDateFormat("ddMMyyyy").format(getDateToday())
//                + generateSeqNo();
////                + env.getProperty("ambank_seqNo");

        String srcRefNo = generateSrcRefNo();
        String url = env.getProperty("ambank_AcctEnquiryAPI") + "/" + srcRefNo;
//        String uri = "/api/PaymentAPI/v3.0/GetAccountEnquiry/" + srcRefNo;
        String uri = URI.create(url).getPath();
//        String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyyHHmmss"));
        String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(getDateTimeNow());

        try {

            ObjectMapper objectMapper = new ObjectMapper();


            if(Objects.equals(access_token, "") || Objects.equals(access_token, null) || LocalDateTime.now().isAfter(tokenExpired)) { // for token expired, get new token
                token();
            }
            logger.info("token -> " + access_token);
            logger.info("token expired -> " +tokenExpired);
//                if (tokenResponse.getStatusCode().equals(HttpStatus.OK)) {
//           if (!Objects.equals(access_token, "") && !Objects.equals(access_token, null) && LocalDateTime.now().isBefore(tokenExpired)) {



//                String idType_Bank = switch (idType) {
//                    case "MyKad" -> "01";
//                    case "Army", "Police" -> "02";
//                    case "Passport" -> "03";
//                    case "RegistrationNumber" -> "04";
//                    default -> "00";
//                };

                JsonObject jsonBody = new JsonObject();
                jsonBody.addProperty("creditorAccountType", form.getCreditorAccountType());
                jsonBody.addProperty("receiverBIC", form.getReceiverBIC());
                jsonBody.addProperty("creditorAccountNo", form.getCreditorAccountNo());
                jsonBody.addProperty("secondValidationIndicator", form.getSecondValidationIndicator());
                jsonBody.addProperty("idType", form.getIdType());
                jsonBody.addProperty("idNo", form.getIdNo());
                String stringBody = "{\"creditorAccountType\": \"" + form.getCreditorAccountType() + "\"," +
                        "\"receiverBIC\": \"" + form.getReceiverBIC() + "\"," +
                        "\"creditorAccountNo\": \"" + form.getCreditorAccountNo() + "\"," +
                        "\"secondValidationIndicator\": \"" + form.getSecondValidationIndicator() + "\"," +
                        "\"idType\": \"" + form.getIdType() + "\"," +
                        "\"idNo\": \"" + form.getIdNo() + "\"}";

                HttpHeaders headers = new HttpHeaders();
                headers.setBearerAuth(access_token);
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
                headers.add("srcRefNo", srcRefNo);
                headers.add("Channel-Token", env.getProperty("ambank_Channel-token"));
                headers.add("Channel-APIKey", env.getProperty("ambank_Channel_APIKey"));
                headers.add("AmBank-Signature", generateAmbankSignature(uri, timestamp, jsonBody.toString()));
                headers.add("AmBank-Timestamp", timestamp);

                HttpEntity<String> request = new HttpEntity<>(stringBody, headers);
                logger.info("request -> " +request);

                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<String> response;
                try {
                    response = restTemplate.postForEntity(url, request, String.class);
                    objectMapper = new ObjectMapper();
                    Map<String, Object> responseBody = objectMapper.readValue(response.getBody(), new TypeReference<Map<String, Object>>() {});

                    logger.info("RESPONSE BODY: ");
                    logger.info(responseBody.toString());
                    updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                            response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                    return ResponseEntity.ok(response.getBody());

                } catch (HttpClientErrorException exHttp) {
                    logger.error(exHttp);

                    updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                            exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);

                    return ResponseEntity.status(exHttp.getStatusCode()).body(exHttp.getResponseBodyAsString());
                }
//                catch (Exception ex) {
//                    logger.error(ex);
//                    updateIntegrationResponse(code, serviceName, url, "",form.toString(), "", "", ex.getMessage(), false);
//
//                    return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
//                }

//            }
//
//            return ResponseEntity.noContent().build();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "",form.toString(), "", "", ex.getMessage(), false);

            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("accountEnquiry")
    public ResponseEntity<String> accountEnquiry(@RequestBody AmbankAccountEnquiryForm form) {
        String code = "ITR01/02";
        String serviceName = "Account Enquiry";
        // generate/set srcRefNo, url, uri, timestamp. this used on header & generate signature for api call
        String srcRefNo = generateSrcRefNo();
//        srcRefNo="BM106042023000018";
        String url = env.getProperty("ambank_AcctEnquiryAPI") + "/" + srcRefNo;
        String uri = URI.create(url).getPath();
        String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(getDateTimeNow());

        logger.info("url: "+ url);
        logger.info("uri: "+uri);

        try {

            //check token validity. Ambank used Bearer token with 3600 second expires.
            ObjectMapper objectMapper = new ObjectMapper();

            if(Objects.equals(access_token, "") || Objects.equals(access_token, null) || LocalDateTime.now().isAfter(tokenExpired)) { // for token expired, get new token
                token();
            }
            logger.info("token -> " + access_token);
            logger.info("token expired -> " +tokenExpired);

            //generate request Body
            String stringBody = "{\"creditorAccountType\":\"" + form.getCreditorAccountType()
                    + "\",\"receiverBIC\":\"" + form.getReceiverBIC()
                    + "\",\"creditorAccountNo\":\"" + form.getCreditorAccountNo()
                    + "\",\"secondValidationIndicator\":\"" + form.getSecondValidationIndicator()
                    + "\",\"idType\":\"" + form.getIdType()
                    + "\",\"idNo\":\"" + form.getIdNo() + "\"}";


            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(access_token);
            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.add("srcRefNo", srcRefNo);
            headers.add("Channel-Token", env.getProperty("ambank_Channel-token"));
            headers.add("Channel-APIKey", env.getProperty("ambank_Channel_APIKey"));
            headers.add("AmBank-Signature", generateAmbankSignature(uri, timestamp, stringBody));
            headers.add("AmBank-Timestamp", timestamp);
            headers.add("Accept","application/json");
//            headers.add("content-type", "application/json");
//            headers.add("Authorization", "Bearer "+access_token);

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>(stringBody, headers);
            logger.info("request -> " +request);

            try { // invoke api
                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

//                ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

                logger.info("RESPONSE BODY");
                logger.info(response.getBody());
                //update call response on table IntegrationService as a record (success = true)
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(response.getBody());

                if(json.get("creditorAccountName")!=null){
                    logger.info("ACTIVE ACCOUNT");
                    return ResponseEntity.status(HttpStatus.OK).body(response.getBody());
                }
                else{
                    logger.info("INVALID/INACTIVE ACCOUNT");
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(response.getBody());
                }




            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp);
                logger.error(exHttp.getResponseBodyAsString());
                logger.error(exHttp.toString());

                //update call response on table IntegrationService as a record (success = true)
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), false);

                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getResponseBodyAsString()+"\"}");
            }
            catch (HttpServerErrorException exServer){

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exServer.getStatusCode().toString(), exServer.getResponseHeaders().toString(), exServer.getResponseBodyAsString(), false);

                return ResponseEntity.status(HttpStatus.CONFLICT).body("{\"message\":\""+exServer.getResponseBodyAsString()+"\"}");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            //update call response on table IntegrationService as a record (success = false)
            updateIntegrationResponse(code, serviceName, url, "",form.toString(), "", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("accountEnquiryV4")
    public ResponseEntity<String> accountEnquiryV4(@RequestBody AmbankAccountEnquiryForm form) {
        String code = "ITR01/02";
        String serviceName = "Account Enquiry";
        // generate/set srcRefNo, url, uri, timestamp. this used on header & generate signature for api call
        String srcRefNo = generateSrcRefNo();
//        srcRefNo="BM106042023000018";
        String url = env.getProperty("ambank_AcctEnquiryAPI") + "/" + srcRefNo;
        String uri = URI.create(url).getPath();
        String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(getDateTimeNow());

        logger.info("url: "+ url);
        logger.info("uri: "+uri);

        try {

            //check token validity. Ambank used Bearer token with 3600 second expires.
            ObjectMapper objectMapper = new ObjectMapper();

            if(Objects.equals(access_token, "") || Objects.equals(access_token, null) || LocalDateTime.now().isAfter(tokenExpired)) { // for token expired, get new token
                token();
            }
            logger.info("token -> " + access_token);
            logger.info("token expired -> " +tokenExpired);

            //generate request Body
            String stringBody = "{\"debitorAccountNo\":\"" + env.getProperty("ambank_debitor_acc_no")
                    + "\",\"amount\":\"" + "1.00"
                    + "\",\"creditorAccountType\":\"" + form.getCreditorAccountType()
                    + "\",\"receiverBIC\":\"" + form.getReceiverBIC()
                    + "\",\"creditorAccountNo\":\"" + form.getCreditorAccountNo()
                    + "\",\"secondValidationIndicator\":\"" + form.getSecondValidationIndicator()
                    + "\",\"idType\":\"" + form.getIdType()
                    + "\",\"idNo\":\"" + form.getIdNo() + "\"}";


            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(access_token);
            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.add("srcRefNo", srcRefNo);
            headers.add("Channel-Token", env.getProperty("ambank_Channel-token"));
            headers.add("Channel-APIKey", env.getProperty("ambank_Channel_APIKey"));
            headers.add("AmBank-Signature", generateAmbankSignature(uri, timestamp, stringBody));
            headers.add("AmBank-Timestamp", timestamp);
            headers.add("Accept","application/json");
//            headers.add("content-type", "application/json");
//            headers.add("Authorization", "Bearer "+access_token);

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>(stringBody, headers);
            logger.info("request -> " +request);

            try { // invoke api
                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

//                ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

                logger.info("RESPONSE BODY");
                logger.info(response.getBody());
                //update call response on table IntegrationService as a record (success = true)
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(response.getBody());

                if(json.get("creditorAccountName")!=null){
                    logger.info("ACTIVE ACCOUNT");
                    return ResponseEntity.status(HttpStatus.OK).body(response.getBody());
                }
                else{
                    logger.info("INVALID/INACTIVE ACCOUNT");
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(response.getBody());
                }




            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp);
                logger.error(exHttp.getResponseBodyAsString());
                logger.error(exHttp.toString());

                //update call response on table IntegrationService as a record (success = true)
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), false);

                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getResponseBodyAsString()+"\"}");
            }
            catch (HttpServerErrorException exServer){

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exServer.getStatusCode().toString(), exServer.getResponseHeaders().toString(), exServer.getResponseBodyAsString(), false);

                return ResponseEntity.status(HttpStatus.CONFLICT).body("{\"message\":\""+exServer.getResponseBodyAsString()+"\"}");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            //update call response on table IntegrationService as a record (success = false)
            updateIntegrationResponse(code, serviceName, url, "",form.toString(), "", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("getCTStatus")
    public ResponseEntity<String> getCTStatus(@RequestParam String cTsrcRefNo) {
        String code = "ITR01/04";
        String serviceName = "Credit Transfer Status";

        // generate/set srcRefNo, url, uri, timestamp. this used on header & generate signature for api call
        String srcRefNo = generateSrcRefNo();
        String url = env.getProperty("ambank_GetCTStatusAPI") + "/" + srcRefNo;
        String uri = URI.create(url).getPath();
        String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(getDateTimeNow());
        logger.info("CTREF: "+cTsrcRefNo);
        try {
            //check token validity. Ambank used Bearer token with 3600 second expires.
            logger.info("token -> " + access_token);
            logger.info("token expired -> " +tokenExpired);

            if(Objects.equals(access_token, "") || Objects.equals(access_token, null) || LocalDateTime.now().isAfter(tokenExpired)) { // for token expired, get new token
                token();
            }

            //generate request Body
//            String jsonBody = CTsrcRefNo;
            String jsonBody = "{\"CTsrcRefNo\":\""+cTsrcRefNo+"\"}";
//            String jsonBody = "{\"CTsrcRefNo\":\"DFLT\"}";;

            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(access_token);
            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.add("srcRefNo", srcRefNo);
            headers.add("Channel-Token", env.getProperty("ambank_Channel-token"));
            headers.add("Channel-APIKey", env.getProperty("ambank_Channel_APIKey"));
            headers.add("AmBank-Signature", generateAmbankSignature(uri, timestamp, jsonBody));
            headers.add("AmBank-Timestamp", timestamp);
            headers.add("Accept","application/json");
//            headers.add("content-type", "application/json");
//            headers.add("Authorization", "Bearer "+access_token);


            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>(jsonBody, headers);
            logger.info("request -> " +request);

            try { //invoke api
                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory); //import org.springframework.web.client.RestTemplate
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);


                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());

            } catch (HttpClientErrorException exHttp) { //while got error from api call
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), false);


                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getResponseBodyAsString()+"\"}");
            }
        }
        catch (Exception ex) {
            logger.error(ex.getMessage());

            //update call response on table IntegrationService as a record
            updateIntegrationResponse(code, serviceName, url, "", cTsrcRefNo, "", "", ex.getMessage(), false);


            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }


    @PostMapping("creditTransfer")
    public ResponseEntity<String> creditTransfer(@RequestBody AmbankCreditTransferForm form) {
        String code = "ITR01/03";
        String serviceName = "Credit Transfer";
        // generate/set srcRefNo, url, uri, timestamp. this used on header & generate signature for api call
        String srcRefNo = generateSrcRefNo();
        String url = env.getProperty("ambank_CreditTransferAPI") + "/" + srcRefNo;
        String uri = URI.create(url).getPath();
        String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(getDateTimeNow());

        // set paymentReference, paymentDescription, lookUpReference

//        String paymentReference = timestamp+"_BeneNRIC";
//        String paymentDescription = form.getCreditorAccountNo()+"_"+form.getCreditorAccountName();

        String lookUpReference = "";

        try {
            //check token validity. Ambank used Bearer token with 3600 second expires.

            if(Objects.equals(access_token, "") || Objects.equals(access_token, null) || LocalDateTime.now().isAfter(tokenExpired)) { // for token expired, get new token
                token();
            }
            logger.info("token -> " + access_token);
            logger.info("token expired -> " +tokenExpired);


            //generate request Body
//            String jsonBody2 = "{\"creditorAccountType\":\"DFLT\",\"receiverBIC\":\"HLBBMYKL\",\"creditorAccountNo\":\"00150849898\",\"creditorAccountName\":\"AMALINA\",\"amount\":\"200.30\",\"paymentReference\":\""+timestamp+"_BeneNRIC\",\"paymentDescription\":\"00150849898_AMALINA\",\"lookUpReference\":\"20230407ARBKMYKL510B15143051\"}";
//            String jsonBody = "{\"creditorAccountType\":\""+form.getCreditorAccountType()+"\",\"receiverBIC\":\""+form.getReceiverBIC()+"\",\"creditorAccountNo\":\""+form.getCreditorAccountNo()+"\",\"creditorAccountName\":\""+form.getCreditorAccountName()+"\",\"amount\":\""+form.getAmount()+"\",\"paymentReference\":\""+paymentReference+"\",\"paymentDescription\":\""+paymentDescription+"\",\"lookUpReference\":\""+lookUpReference+"\"}";
//            String jsonBodyV3 = "{\"creditorAccountType\":\""+form.getCreditorAccountType()+"\",\"receiverBIC\":\""+form.getReceiverBIC()+"\",\"creditorAccountNo\":\""+form.getCreditorAccountNo()+"\",\"creditorAccountName\":\""+form.getCreditorAccountName()+"\",\"amount\":\""+form.getAmount()+"\",\"paymentReference\":\""+form.getPaymentReference()+"\",\"paymentDescription\":\""+form.getPaymentDescription()+"\",\"lookUpReference\":\""+form.getLookUpReference()+"\"}";
            String jsonBody = "{\"creditorAccountType\":\""+form.getCreditorAccountType()+"\",\"receiverBIC\":\""+form.getReceiverBIC()+"\",\"creditorAccountNo\":\""+form.getCreditorAccountNo()+"\",\"creditorAccountName\":\""+form.getCreditorAccountName()+"\",\"amount\":\""+form.getAmount()+"\",\"paymentReference\":\""+form.getPaymentReference()+"\",\"paymentDescription\":\""+form.getPaymentDescription()+"\",\"lookUpReference\":\""+form.getLookUpReference()+"\"}";

//            logger.info("STRING FROM SYSTEM : "+jsonBody2);
            logger.info("STRING FROM API : "+jsonBody);


            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(access_token);
            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.add("srcRefNo", srcRefNo);
            headers.add("Channel-Token", env.getProperty("ambank_Channel-token"));
            headers.add("Channel-APIKey", env.getProperty("ambank_Channel_APIKey"));
            headers.add("AmBank-Signature", generateAmbankSignature(uri, timestamp, jsonBody));
            headers.add("AmBank-Timestamp", timestamp);
            headers.add("Accept","application/json");
//            headers.add("content-type", "application/json");
//            headers.add("Authorization", "Bearer "+access_token);
//            headers.add("Accept-Encoding","gzip, deflate, br");

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>(jsonBody, headers);
            logger.info("request -> " +request);

            try { //invoke api

                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory); //import org.springframework.web.client.RestTemplate
//                restTemplate.setMessageConverters(getJsonMessageConverters());
                logger.info("MESSAGE_CONVERTER : " + restTemplate.getMessageConverters());
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);


                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());

            }
            catch (HttpClientErrorException exHttp) { //while got error from api call
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), false);


                logger.info("ERRROR ! STATUS: "+ exHttp.getStatusCode());
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getResponseBodyAsString()+"\"}");
            }
            catch (HttpServerErrorException exServer){

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exServer.getStatusCode().toString(), exServer.getResponseHeaders().toString(), exServer.getResponseBodyAsString(), false);

                return ResponseEntity.status(HttpStatus.CONFLICT).body("{\"message\":\""+exServer.getResponseBodyAsString()+"\"}");
            }
        }
        catch (Exception ex) {
            logger.error(ex.getMessage());

            //update call response on table IntegrationService as a record
            updateIntegrationResponse(code, serviceName, url, "", form.toString(), "", "", ex.getMessage(), false);

            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("creditTransferV4")
    public ResponseEntity<String> creditTransferV4(@RequestBody AmbankCreditTransferForm form) {
        String code = "ITR01/03";
        String serviceName = "Credit Transfer";
        // generate/set srcRefNo, url, uri, timestamp. this used on header & generate signature for api call
        String srcRefNo = generateSrcRefNo();
        String url = env.getProperty("ambank_CreditTransferAPI") + "/" + srcRefNo;
        String uri = URI.create(url).getPath();
        String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(getDateTimeNow());

        // set paymentReference, paymentDescription, lookUpReference

//        String paymentReference = timestamp+"_BeneNRIC";
//        String paymentDescription = form.getCreditorAccountNo()+"_"+form.getCreditorAccountName();

        String lookUpReference = "";

        try {
            //check token validity. Ambank used Bearer token with 3600 second expires.

            if(Objects.equals(access_token, "") || Objects.equals(access_token, null) || LocalDateTime.now().isAfter(tokenExpired)) { // for token expired, get new token
                token();
            }
            logger.info("token -> " + access_token);
            logger.info("token expired -> " +tokenExpired);


            //generate request Body
//            String jsonBody2 = "{\"creditorAccountType\":\"DFLT\",\"receiverBIC\":\"HLBBMYKL\",\"creditorAccountNo\":\"00150849898\",\"creditorAccountName\":\"AMALINA\",\"amount\":\"200.30\",\"paymentReference\":\""+timestamp+"_BeneNRIC\",\"paymentDescription\":\"00150849898_AMALINA\",\"lookUpReference\":\"20230407ARBKMYKL510B15143051\"}";
//            String jsonBody = "{\"creditorAccountType\":\""+form.getCreditorAccountType()+"\",\"receiverBIC\":\""+form.getReceiverBIC()+"\",\"creditorAccountNo\":\""+form.getCreditorAccountNo()+"\",\"creditorAccountName\":\""+form.getCreditorAccountName()+"\",\"amount\":\""+form.getAmount()+"\",\"paymentReference\":\""+paymentReference+"\",\"paymentDescription\":\""+paymentDescription+"\",\"lookUpReference\":\""+lookUpReference+"\"}";
//            String jsonBodyV3 = "{\"creditorAccountType\":\""+form.getCreditorAccountType()+"\",\"receiverBIC\":\""+form.getReceiverBIC()+"\",\"creditorAccountNo\":\""+form.getCreditorAccountNo()+"\",\"creditorAccountName\":\""+form.getCreditorAccountName()+"\",\"amount\":\""+form.getAmount()+"\",\"paymentReference\":\""+form.getPaymentReference()+"\",\"paymentDescription\":\""+form.getPaymentDescription()+"\",\"lookUpReference\":\""+form.getLookUpReference()+"\"}";
            String jsonBody = "{\"creditorAccountType\":\""+form.getCreditorAccountType()
                    +"\",\"debitorAccountNo\":\""+env.getProperty("ambank_debitor_acc_no")
                    +"\",\"receiverBIC\":\""+form.getReceiverBIC()
                    +"\",\"creditorAccountNo\":\""+form.getCreditorAccountNo()
                    +"\",\"creditorAccountName\":\""+form.getCreditorAccountName()
                    +"\",\"amount\":\""+form.getAmount()
                    +"\",\"paymentReference\":\""+form.getPaymentReference()
                    +"\",\"paymentDescription\":\""+form.getPaymentDescription()
                    +"\",\"lookUpReference\":\""+form.getLookUpReference()+"\"}";

//            logger.info("STRING FROM SYSTEM : "+jsonBody2);
            logger.info("STRING FROM API : "+jsonBody);


            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(access_token);
            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.add("srcRefNo", srcRefNo);
            headers.add("Channel-Token", env.getProperty("ambank_Channel-token"));
            headers.add("Channel-APIKey", env.getProperty("ambank_Channel_APIKey"));
            headers.add("AmBank-Signature", generateAmbankSignature(uri, timestamp, jsonBody));
            headers.add("AmBank-Timestamp", timestamp);
            headers.add("Accept","application/json");
//            headers.add("content-type", "application/json");
//            headers.add("Authorization", "Bearer "+access_token);
//            headers.add("Accept-Encoding","gzip, deflate, br");

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>(jsonBody, headers);
            logger.info("request -> " +request);

            try { //invoke api

                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory); //import org.springframework.web.client.RestTemplate
//                restTemplate.setMessageConverters(getJsonMessageConverters());
                logger.info("MESSAGE_CONVERTER : " + restTemplate.getMessageConverters());
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);


                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());

            }
            catch (HttpClientErrorException exHttp) { //while got error from api call
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), false);


                logger.info("ERRROR ! STATUS: "+ exHttp.getStatusCode());
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getResponseBodyAsString()+"\"}");
            }
            catch (HttpServerErrorException exServer){

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exServer.getStatusCode().toString(), exServer.getResponseHeaders().toString(), exServer.getResponseBodyAsString(), false);

                return ResponseEntity.status(HttpStatus.CONFLICT).body("{\"message\":\""+exServer.getResponseBodyAsString()+"\"}");
            }
        }
        catch (Exception ex) {
            logger.error(ex.getMessage());

            //update call response on table IntegrationService as a record
            updateIntegrationResponse(code, serviceName, url, "", form.toString(), "", "", ex.getMessage(), false);

            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    private List<HttpMessageConverter<?>> getJsonMessageConverters() {
        List<HttpMessageConverter<?>> converters = new ArrayList<>();
        converters.add(new MappingJackson2HttpMessageConverter());
        return converters;
    }


    @PostMapping("reference")
    public ResponseEntity<String> callAmbankAPIGuadline(@RequestBody AmbankAccountEnquiryForm form) {

        // generate/set srcRefNo, url, uri, timestamp. this used on header & generate signature for api call
        String srcRefNo = generateSrcRefNo();
        String url = env.getProperty("xxx") + "/" + srcRefNo;
        String uri = URI.create(url).getPath();
        String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(getDateTimeNow());

        try {
            //check token validity. Ambank used Bearer token with 3600 second expires.
            logger.info("token -> " + access_token);
            logger.info("token expired -> " +tokenExpired);

            if(Objects.equals(access_token, "") || Objects.equals(access_token, null) || LocalDateTime.now().isAfter(tokenExpired)) { // for token expired, get new token
                token();
            }

            //generate request Body
            String jsonBody = "{\"name1\":\"value1\",\"name2\":\"value2\",\"name3\":\"value3\"}";

            //generate request Header. used HttpHeaders(org.springframework.http.*) for easier
            HttpHeaders headers = new HttpHeaders();

            //generate request using HttpEntity(org.springframework.http.*)
            HttpEntity<String> request = new HttpEntity<>(jsonBody, headers);
            logger.info("request -> " +request);

            try { //invoke api
                RestTemplate restTemplate = new RestTemplate(); //import org.springframework.web.client.RestTemplate
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true

                return ResponseEntity.status(HttpStatus.OK).body(response.getBody());

            } catch (HttpClientErrorException exHttp) { //while got error from api call
                logger.error(exHttp.getMessage());
                //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = true

                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            //update call response on table IntegrationService as a record
//                updateIntegrationResponse(); //success = false

            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    ///////////////////////////////Paynet section (List Bank, Initiate Payment, Cancel Payment, Status Inquiry)/////////////////////////////////

    @GetMapping("paynetToken")
    public ResponseEntity<String> generatePaynetToken (){

        String code = "ITR02/01";
        String serviceName = "Paynet Token";
        String url = env.getProperty("paynet_TokenAPI");
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();

            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            body.add("grant_type", env.getProperty("paynet_grant_type"));
            body.add("client_id", env.getProperty("paynet_client_id"));
            body.add("client_secret", env.getProperty("paynet_client_secret"));

            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);

            try {
                ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);

                ObjectMapper objectMapper = new ObjectMapper();
                paynet_access_token = objectMapper.readValue(response.getBody(), new TypeReference<Map<String, Object>>() {
                }).get("access_token").toString();
                String expires_in = objectMapper.readValue(response.getBody(), new TypeReference<Map<String, Object>>() {
                }).get("expires_in").toString();
                paynetTokenExpired = LocalDateTime.now().plusSeconds(Long.parseLong(expires_in));

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), Objects.requireNonNull(request.getBody()).toString(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                return ResponseEntity.ok(response.getBody());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody().toString(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "","","", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("integration/listBank")
    public ResponseEntity<String> getIntegrationBankList(@RequestParam String channelCode, @RequestParam(required = false) String pageKey ){
        String code = "ITR02/02";
        String serviceName = "Paynet List Bank";
        String url = env.getProperty("paynet_BankListAPI");
        String clientId = env.getProperty("paynet_BIC_code"); // clientId is BIC of the participant
        String gpsCoordinates = env.getProperty("Gps_Coordinates");
        String ipAddress = env.getProperty("Ip_Address");

        if (Objects.equals(pageKey, ""))
            pageKey = null;
        JsonArray banksJsonArray = new JsonArray();
//        String gpsCoordinates = env.getProperty("Gps-Coordinates");
//        String ipAddress = env.getProperty("Ip-Address");

        try {
            //check paynet token
            if(Objects.equals(paynet_access_token, "") || paynetTokenExpired.isBefore(LocalDateTime.now())){
                generatePaynetToken();
            }
            // check channel code
            if(Arrays.stream(ChannelType.values()).noneMatch(e->e.getId().equals(channelCode)))
                return ResponseEntity.badRequest().body("{\"message\":\"Invalid channelCode\"}");

            String currDate = new SimpleDateFormat("yyyyMMdd").format(getDateToday());
            String transactionType = "650";
            String sequenceNumber = walletApiController.generateSeqNo(env.getProperty("paynet_PrefixId"), "8");
            String messageId = currDate + clientId + transactionType + "O" + channelCode + sequenceNumber;
            String transactionId = currDate + clientId + transactionType + sequenceNumber;

            String messageToSign = clientId + "RPPEMYKL" + messageId + transactionId + messageId + clientId;
            String messageSignature = generatePaynetSignature(messageToSign);

            RestTemplate restTemplate = new RestTemplate();

            //generate request Header
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(paynet_access_token);

            headers.add("X-Gps-Coordinates", gpsCoordinates);
            headers.add("X-Ip-Address", ipAddress);
            headers.add("X-Signature", messageSignature);
            headers.add("X-Signature-Key", env.getProperty("certificate_serial_number"));

            // Add parameters to the url
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("clientId", clientId)
                    .queryParam("messageId", messageId)
                    .queryParam("transactionId", transactionId);

            if (pageKey != null){
                builder = builder.queryParam("pageKey",pageKey);
            }
            url = builder.toUriString();

            HttpEntity<String> request = new HttpEntity<>(headers);

            try {

                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                JsonObject jsonResponse = JsonParser.parseString(response.getBody()).getAsJsonObject();

//                JsonObject responseBanks = new JsonObject(jsonResponse.get("banks").toString());
//                for (int i = 0; i< jsonResponse.size(); i++ ){
//
//                }
//                return ResponseEntity.ok(response.getBody());
                return ResponseEntity.ok(response.getBody());

            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }
        } catch (Exception ex){
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "","","", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("listBank")
    public ResponseEntity<String> getBankList(@RequestParam String channelCode ){
        logger.info("GET: /listBank");
        String code = "ITR02/02";
        String serviceName = "Paynet List Bank";
        String url = env.getProperty("paynet_BankListAPI");
        String clientId = env.getProperty("paynet_BIC_code"); // clientId is BIC of the participant
        String gpsCoordinates = env.getProperty("Gps_Coordinates");
        String ipAddress = env.getProperty("Ip_Address");
        String pageKey = null;
        JsonArray banksJsonArray = new JsonArray();
//        String gpsCoordinates = env.getProperty("Gps-Coordinates");
//        String ipAddress = env.getProperty("Ip-Address");

        try {
            //check paynet token
            if(Objects.equals(paynet_access_token, "") || paynetTokenExpired.isBefore(LocalDateTime.now())){
                generatePaynetToken();
            }

            // check channel code
            if(Arrays.stream(ChannelType.values()).noneMatch(e->e.getId().equals(channelCode)))
                return ResponseEntity.badRequest().body("{\"message\":\"Invalid channelCode\"}");

            String currDate = new SimpleDateFormat("yyyyMMdd").format(getDateToday());
            String transactionType = "650";
            String sequenceNumber = walletApiController.generateSeqNo(env.getProperty("paynet_PrefixId"), "8");
            String messageId = currDate + clientId + transactionType + "O" + channelCode + sequenceNumber;
            String transactionId = currDate + clientId + transactionType + sequenceNumber;

            String messageToSign = clientId + "RPPEMYKL" + messageId + transactionId + messageId + clientId;
            String messageSignature = generatePaynetSignature(messageToSign);

            RestTemplate restTemplate = new RestTemplate();

            //generate request Header
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(paynet_access_token);

            headers.add("X-Gps-Coordinates", gpsCoordinates);
            headers.add("X-Ip-Address", ipAddress);
            headers.add("X-Signature", messageSignature);
            headers.add("X-Signature-Key", env.getProperty("certificate_serial_number"));

            // Add parameters to the url

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("clientId", clientId)
                    .queryParam("messageId", messageId)
                    .queryParam("transactionId", transactionId);
//            url = builder.toUriString();

            HttpEntity<String> request = new HttpEntity<>(headers);

            try {
                List<String> bankNameList = new ArrayList<>();

                //check stored bank list from db
                String query1="Select e from PaynetBankList e ";
                List<PaynetBankList> bankList = this.dataManager.load(PaynetBankList.class)
                        .query(query1)
                        .list();
                logger.info("Total PaynetBankList:"+ bankList.size());
                List<String>bicCodeList = new ArrayList<>();
                for (PaynetBankList bank : bankList){
                    bicCodeList.add(bank.getCode());
                }

                //set maximum 50 pageKeys
                for (int i =0; i< 50; i++){
                    if (pageKey != null){
                        builder = builder.replaceQueryParam("pageKey",pageKey);
                    }

                    url = builder.toUriString();
                    ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
                    updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                            response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                    JsonObject jsonResponse = JsonParser.parseString(response.getBody()).getAsJsonObject();
                    JsonArray banks = jsonResponse.get("banks").getAsJsonArray();

                    for (JsonElement bank : banks){

                        JsonObject bankObject = bank.getAsJsonObject();
                        JsonElement redirectUrls = bankObject.get("redirectUrls");
                        JsonElement activeStatus = bankObject.get("active");
                        String bankName = bankObject.get("name").getAsString();
                        String bankCode = bankObject.get("code").getAsString();

                        if (redirectUrls!= null && !redirectUrls.isJsonNull() && Objects.equals(activeStatus.getAsString(), "true")){
                            JsonArray redirectUrlsArray = redirectUrls.getAsJsonArray();
                            boolean hasRETType = false;
                            String urlRedirectUrl = "";

                            for (int k = 0; k < redirectUrlsArray.size(); k++) {
                                JsonElement redirectUrl = redirectUrlsArray.get(k);
                                JsonObject redirectUrlObject = redirectUrl.getAsJsonObject();
                                JsonElement redirectUrlType = redirectUrlObject.get("type");

                                if (redirectUrlType != null && redirectUrlType.getAsString().equals("RET")) {
                                    hasRETType = true;
                                    urlRedirectUrl = redirectUrlObject.get("url").getAsString();
                                    break;
                                } else {
                                    redirectUrlsArray.remove(k);
                                    k--; // Adjust the loop counter to account for the removed element
                                }
                            }
                            if(hasRETType){
                                String redirectUrlTypeStr = "RET";
                                // check if already contain with same name
                                if(bankNameList.contains(bankName) && !urlRedirectUrl.contains("mepsfpx.com.my/FPXMain")){
                                    //check and remove if existing is fpx and new is duitnow
                                    for (int k = 0; k < banksJsonArray.size(); k++) {
                                        JsonElement bankJson = banksJsonArray.get(k);
                                        JsonObject bankJsonObject = bankJson.getAsJsonObject();
                                        String name = bankJsonObject.get("name").getAsString();

                                        if(name.equals(bankName)){
                                            JsonArray redirectUrlsArray2 = bankJsonObject.getAsJsonArray("redirectUrls");
                                            for (JsonElement redirectUrl2 : redirectUrlsArray2) {
                                                JsonObject redirectUrlObject2 = redirectUrl2.getAsJsonObject();
                                                String url2 = redirectUrlObject2.get("url").getAsString();

                                                if (url2.contains("mepsfpx.com.my/FPXMain")) {
                                                    banksJsonArray.remove(k);
                                                    k--; // Adjust the loop counter to account for the removed element
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    banksJsonArray.add(bank);
                                    if(!bicCodeList.contains(bankCode)){
                                        updatePaynetBankList(bankCode,bankName,urlRedirectUrl,redirectUrlTypeStr);
                                    }
                                }
                                else if(bankNameList.contains(bankName) && urlRedirectUrl.contains("mepsfpx.com.my/FPXMain")){
                                    logger.info("duplicate "+bankName);
                                }
                                else{
                                    banksJsonArray.add(bank);
                                    bankNameList.add(bankName);
                                    if(!bicCodeList.contains(bankCode))
                                        updatePaynetBankList(bankCode,bankName,urlRedirectUrl,redirectUrlTypeStr);
                                }
                            }

                        }

                    }
                    //banksJsonArray.addAll(banks);
                    if(!jsonResponse.get("pageKey").isJsonNull())
                        pageKey = jsonResponse.get("pageKey").getAsString();
                    else
                        break;
                }

                // Convert the JSONArray to an ArrayList of JSONObjects for sorting
                ArrayList<JsonObject> banksList = new ArrayList<>();
                for (int j = 0; j < banksJsonArray.size(); j++) {
                    banksList.add(banksJsonArray.get(j).getAsJsonObject());
                }

                // Create a custom comparator to sort by the "name" property
                Comparator<JsonObject> nameComparator = (bank1, bank2) -> {
                    String name1 = bank1.get("name").getAsString();
                    String name2 = bank2.get("name").getAsString();
                    return name1.compareTo(name2);
                };

                // Sort the ArrayList using the custom comparator
                Collections.sort(banksList, nameComparator);
                JsonArray sortedBanksJsonArray = new JsonArray();
                for (JsonObject bank : banksList) {
                    sortedBanksJsonArray.add(bank);
                }

                JsonObject response = new JsonObject();
                response.add("banks",sortedBanksJsonArray);
                return ResponseEntity.ok(response.toString());

            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }
        } catch (Exception ex){
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "","","", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public void updatePaynetBankList(String code, String name, String redirect_url, String redirect_url_type){
        try{
            PaynetBankList newBank = this.dataManager.create(PaynetBankList.class);
            newBank.setCode(code);
            newBank.setName(name);
            newBank.setRedirectUrl(redirect_url);
            newBank.setRedirectUrlType(redirect_url_type);
            this.dataManager.save(newBank);
            logger.info("add new bank in PaynetBankList. bicCode -> "+ code);

        }catch (Exception ex){
            logger.error(ex.getMessage());
        }
    }

    @PostMapping("initiatePayment")
    public ResponseEntity<String> initiatePayment(@RequestBody PaynetInitiatePaymentForm form){
        logger.info("POST: /initiatePayment");
        String code = "ITR02/03";
        String serviceName = "Paynet Initiate Payment";
        String url = env.getProperty("paynet_InitiatePaymentAPI");
        String clientId = env.getProperty("paynet_BIC_code"); // clientId is BIC of the participant
        String productId = env.getProperty("paynet_productId");
        String gpsCoordinates = form.getGpsCoordinates();
        String ipAddress = form.getIpAddress();
        String currency = form.getCurrency();

        if(gpsCoordinates == null || gpsCoordinates.equals(""))
            gpsCoordinates = env.getProperty("Gps_Coordinates");
        if(ipAddress == null || ipAddress.equals(""))
            ipAddress = env.getProperty("Ip_Address");
        if(currency == null || currency.equals(""))
            currency = env.getProperty("paynet_InitiatePaymentCurrency");

        try {
            //check paynet token
            if(Objects.equals(paynet_access_token, "") || paynetTokenExpired.isBefore(LocalDateTime.now())){
                generatePaynetToken();
            }

            // check channel code
            if(Arrays.stream(ChannelType.values()).noneMatch(e->e.getId().equals(form.getChannelCode())))
                return ResponseEntity.badRequest().body("{\"message\":\"Invalid channelCode\"}");

            String currDate = new SimpleDateFormat("yyyyMMdd").format(getDateToday());
            String transactionType = "861";
            String sequenceNumber = walletApiController.generateSeqNo(env.getProperty("paynet_PrefixId"), "8");
            String messageId = currDate + clientId + transactionType + "O" + form.getChannelCode() + sequenceNumber;
            String transactionId = currDate + clientId + transactionType + sequenceNumber;
            String endToEndId = messageId;

            BigDecimal amount = form.getAmount().setScale(2, RoundingMode.HALF_UP);
            String amountStr = String.format("%.2f", amount);

            String messageToSign = clientId + "RPPEMYKL" + messageId + transactionId + "RPPEMYKL" + endToEndId + amountStr + clientId;
            String messageSignature = generatePaynetSignature(messageToSign);

            // get url
            //for mvp 2, need to provide redirectUrlType since same bic code may contain both RET and COR
            String queryStr = "select e from PaynetBankList e where e.code = :code and e.redirectUrlType = :type";
//            String queryStr = "select e from PaynetBankList e where e.code = :code";
            Optional<PaynetBankList> paynetBank = this.dataManager.load(PaynetBankList.class)
                    .query(queryStr)
                    .parameter("code",form.getBicCode())
                    .parameter("type","RET")
                    .optional();

            if(paynetBank.isEmpty())
            {
                logger.info("invalid bic code :->"+form.getBicCode());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\""+"invalid bic code"+"\"}");
            }
            String redirectUrl = paynetBank.get().getRedirectUrl();
            String redirectUrlType = paynetBank.get().getRedirectUrlType();     //RET or COR
            logger.info("redirectUrl :->"+redirectUrl);

            //generate request Header
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(paynet_access_token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("X-Gps-Coordinates", gpsCoordinates);
            headers.add("X-Ip-Address", ipAddress);
            headers.add("X-Signature", messageSignature);
            headers.add("X-Signature-Key", env.getProperty("certificate_serial_number"));

            //generate request Body

            JsonObject customer = new JsonObject();
            customer.addProperty("name", form.getName());
            customer.addProperty("bankType", redirectUrlType);

            if(form.getIdentificationNumber() != null){
                logger.info("initiate payment. 1st party check = true");
                String identificationType = "01";
                if(form.getIdentificationTypeCode().equals("P01"))
                    identificationType = "01";
                if(form.getIdentificationTypeCode().equals("P04"))
                    identificationType = "02";
                if(form.getIdentificationTypeCode().equals("P03"))
                    identificationType = "03";
                customer.addProperty("identificationType", identificationType);
                customer.addProperty("identification", form.getIdentificationNumber());
                customer.addProperty("identityValidation", "02");
            }
            else {
                logger.info("initiate payment. 1st party check = false");
            }
            JsonObject merchant = new JsonObject();
            merchant.addProperty("name", env.getProperty("paynet_Merchant_Name"));
            merchant.addProperty("accountType", env.getProperty("paynet_Merchant_AccountType"));

            JsonArray sourceOfFunds = new JsonArray();
            sourceOfFunds.add("01");    // current & saving account
//            sourceOfFunds.add("03");    // eWallet

            JsonObject jsonBody = new JsonObject();
            jsonBody.addProperty("clientId", clientId);
            jsonBody.addProperty("messageId", messageId);
            jsonBody.addProperty("transactionId", transactionId);
            jsonBody.addProperty("endToEndId", endToEndId);
            jsonBody.addProperty("currency", currency);
            jsonBody.addProperty("amount", amount);
            jsonBody.addProperty("productId", productId);
            jsonBody.add("customer", customer);
            jsonBody.add("merchant", merchant);
            jsonBody.add("sourceOfFunds", sourceOfFunds);
            jsonBody.addProperty("recipientReference", form.getRecipientReference());
            if(form.getExpiryDate() != null)
                jsonBody.addProperty("expiryDate", form.getExpiryDate());

//            HttpEntity<String> request = new HttpEntity<>(headers);
            HttpEntity<String> request = new HttpEntity<>(jsonBody.toString(), headers);

            try {
                RestTemplate restTemplate = new RestTemplate();
//                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                JsonObject jsonResponse = JsonParser.parseString(response.getBody()).getAsJsonObject();
//                String endToEndId = jsonResponse.get("endToEndId").getAsString();
                String endToEndIdSignature = jsonResponse.get("endToEndIdSignature").getAsString();
                String encodedEndToEndIdSignature = URLEncoder.encode(endToEndIdSignature,StandardCharsets.UTF_8.toString());

                //for FPX bridge
                if(redirectUrl.contains("mepsfpx.com.my/FPXMain")){
                    redirectUrl = redirectUrl
                            +"?EndtoEndId="
                            +endToEndId+"&EndtoEndIdSignature="+encodedEndToEndIdSignature
                            +"&DbtrAgt="+form.getBicCode();
                }
                else{
                    // for RPP duitnow
                    if(!redirectUrl.contains("/RPP/MY/Redirect/RTP"))
                        redirectUrl = redirectUrl +"/RPP/MY/Redirect/RTP";
                    redirectUrl = redirectUrl
                            +"?EndtoEndId="
                            +endToEndId+"&EndtoEndIdSignature="+encodedEndToEndIdSignature
                            +"&DbtrAgt="+form.getBicCode();
                }

                jsonResponse.addProperty("redirectUrl",redirectUrl);

//                return ResponseEntity.ok(response.getBody());
                return ResponseEntity.ok(jsonResponse.toString());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());
                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }
        } catch (Exception ex){
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "","","", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PutMapping("cancelPayment")
    public ResponseEntity<String> cancelPayment(@RequestBody PaynetCancelPayment form){
        String code = "ITR02/04";
        String serviceName = "Paynet Cancel Payment";
        String url = env.getProperty("paynet_CancelPaymentAPI");
        String clientId = env.getProperty("paynet_BIC_code"); // clientId is BIC of the participant
        String productId = env.getProperty("paynet_productId");
        String gpsCoordinates = form.getGpsCoordinates();
        String ipAddress = form.getIpAddress();

        if(gpsCoordinates == null || gpsCoordinates.equals(""))
            gpsCoordinates = env.getProperty("Gps_Coordinates");
        if(ipAddress == null || ipAddress.equals(""))
            ipAddress = env.getProperty("Ip_Address");

        try {
            //check paynet token
            if(Objects.equals(paynet_access_token, "") || paynetTokenExpired.isBefore(LocalDateTime.now())){
                generatePaynetToken();
            }
            // check channel code
            if(Arrays.stream(ChannelType.values()).noneMatch(e->e.getId().equals(form.getChannelCode())))
                return ResponseEntity.badRequest().body("{\"message\":\"Invalid channelCode\"}");

            String currDate = new SimpleDateFormat("yyyyMMdd").format(getDateToday());
            String transactionType = "865";
            String sequenceNumber = walletApiController.generateSeqNo(env.getProperty("paynet_PrefixId"), "8");
            String messageId = currDate + clientId + transactionType + "O" + form.getChannelCode() + sequenceNumber;
            String transactionId = currDate + clientId + transactionType + sequenceNumber;
            String endToEndId = form.getEndToEndId();
            String status = "CANC";

            String messageToSign = clientId + "RPPEMYKL" + messageId + transactionId + endToEndId + status + clientId;
            String messageSignature = generatePaynetSignature(messageToSign);

            //generate request Header
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(paynet_access_token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("X-Gps-Coordinates", gpsCoordinates);
            headers.add("X-Ip-Address", ipAddress);
            headers.add("X-Signature", messageSignature);
            headers.add("X-Signature-Key", env.getProperty("certificate_serial_number"));

            //generate request Body

            JsonObject jsonBody = new JsonObject();
            jsonBody.addProperty("clientId", clientId);
            jsonBody.addProperty("messageId", messageId);
            jsonBody.addProperty("transactionId", transactionId);
            jsonBody.addProperty("endToEndId", endToEndId);
            jsonBody.addProperty("productId", productId);
            jsonBody.addProperty("status", status);

//            HttpEntity<String> request = new HttpEntity<>(headers);
            HttpEntity<String> request = new HttpEntity<>(jsonBody.toString(), headers);

            try {
                RestTemplate restTemplate = new RestTemplate();
//                RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                JsonObject jsonResponse = JsonParser.parseString(response.getBody()).getAsJsonObject();

//                return ResponseEntity.ok(response.getBody());
                return ResponseEntity.ok(jsonResponse.toString());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);
//                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
                return ResponseEntity.status(exHttp.getStatusCode()).body(exHttp.getMessage());
            }
        } catch (Exception ex){
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "","","", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @GetMapping("statusInquiry")
    public ResponseEntity<String> statusInquiry(@RequestParam String endToEndId, String channelCode){
        String code = "ITR02/05";
        String serviceName = "Paynet Status Inquiry";
        String url = env.getProperty("paynet_StatInquiryAPI");
        String clientId = env.getProperty("paynet_BIC_code"); // clientId is BIC of the participant
        String gpsCoordinates = env.getProperty("Gps_Coordinates");
        String ipAddress = env.getProperty("Ip_Address");

        try {
            logger.info("calling api -> /statusInquiry");
            //check paynet token
            if(Objects.equals(paynet_access_token, "") || paynetTokenExpired.isBefore(LocalDateTime.now())){
                generatePaynetToken();
            }

            // check channel code
            if(Arrays.stream(ChannelType.values()).noneMatch(e->e.getId().equals(channelCode)))
                return ResponseEntity.badRequest().body("{\"message\":\"Invalid channelCode\"}");

            String currDate = new SimpleDateFormat("yyyyMMdd").format(getDateToday());
            String transactionType = "864";
            String sequenceNumber = walletApiController.generateSeqNo(env.getProperty("paynet_PrefixId"), "8");
            String messageId = currDate + clientId + transactionType + "O" + channelCode + sequenceNumber;
            String transactionId = currDate + clientId + transactionType + sequenceNumber;

            String messageToSign = clientId + "RPPEMYKL" + messageId + transactionId + endToEndId + endToEndId;
            String messageSignature = generatePaynetSignature(messageToSign);

            RestTemplate restTemplate = new RestTemplate();

            //generate request Header
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(paynet_access_token);

            headers.add("X-Gps-Coordinates", gpsCoordinates);
            headers.add("X-Ip-Address", ipAddress);
            headers.add("X-Signature", messageSignature);
            headers.add("X-Signature-Key", env.getProperty("certificate_serial_number"));

            // Add parameters to the url
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                    .queryParam("clientId", clientId)
                    .queryParam("endToEndId", endToEndId)
                    .queryParam("messageId", messageId)
                    .queryParam("transactionId", transactionId);
            url = builder.toUriString();


            HttpEntity<String> request = new HttpEntity<>(headers);

            try {
                ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        response.getStatusCode().toString(), response.getHeaders().toString(), response.getBody(), true);

                return ResponseEntity.ok(response.getBody());
            } catch (HttpClientErrorException exHttp) {
                logger.error(exHttp.getMessage());

                updateIntegrationResponse(code, serviceName, url, request.getHeaders().toString(), request.getBody(),
                        exHttp.getStatusCode().toString(), exHttp.getResponseHeaders().toString(), exHttp.getResponseBodyAsString(), true);
                return ResponseEntity.status(exHttp.getStatusCode()).body("{\"message\":\""+exHttp.getMessage()+"\"}");
            }
        } catch (Exception ex){
            logger.error(ex.getMessage());
            updateIntegrationResponse(code, serviceName, url, "","","", "", ex.getMessage(), false);
            return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("RPP/MY/Notification/PaymentStatus")
    public ResponseEntity<String> PaymentStatusNotification(@RequestBody String requestBody){

        logger.info("calling api -> " + "RPP/MY/Notification/PaymentStatus");
        logger.info("request body -> " + requestBody);
        String code = "ITR02/06";
        String serviceName = "Paynet Payment Status Notification";
//        String url = env.getProperty("paynet_BankListAPI");
//        String clientId = env.getProperty("paynet_BIC_code"); // clientId is BIC of the participant

        try {
            try{
                PaynetStatusNotification paynetStatusNotification = this.dataManager.create(PaynetStatusNotification.class);
                paynetStatusNotification.setCode(code);
                paynetStatusNotification.setServiceName(serviceName);
//            paynetStatusNotification.setRequestBody("requestString");
                paynetStatusNotification.setRequestBody(requestBody);
                this.dataManager.save(paynetStatusNotification);
            }catch (Exception ex) {
                logger.info("failed store to paynetStatusNotification table");
                logger.error(ex.getMessage());
            }


            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategies.UPPER_CAMEL_CASE);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            PaynetStatusNotificationForm form = objectMapper.readValue(requestBody, PaynetStatusNotificationForm.class);
            String requestString = objectMapper.writeValueAsString(form);
//            String requestString = objectMapper.writeValueAsString(requestBody);

            String query1="select a from PaynetRef a " +
                    "where a.endToEndId = :endToEndId";
            PaynetRef paynetRef = this.dataManager.load(PaynetRef.class)
                    .query(query1)
                    .parameter("endToEndId", form.getNotification().getEventInfo().getEndToEndID())
                    .one();

            String query2="select e from TransactionAcct e " +
                    "where e.referenceId = :referenceId";
            List<TransactionAcct> transactionAcct = this.dataManager.load(TransactionAcct.class)
                    .query(query2)
                    .parameter("referenceId", paynetRef.getReferenceId())
                    .list();

            String paymentStatus = form.getNotification().getEventInfo().getPaymentStatus().getSubstate();
            String txStatusCode = "G05";
            if (Objects.equals(paymentStatus, Substate.CLEARED.getId()))
                txStatusCode = "G01";
            else if (Objects.equals(paymentStatus, Substate.CANCELLED.getId()))
                txStatusCode = "G13";
            else if (Objects.equals(paymentStatus, Substate.REJECTED.getId()))
                txStatusCode = "G07";

            SaveContext saveContext = new SaveContext();
            TransStatus txCompleted = walletApiController.getTransactionStatus(txStatusCode);
            transactionAcct.forEach(e->{
                e.setTransStatus(txCompleted);
                saveContext.saving(e);
            });
            this.dataManager.save(saveContext);

            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }



    public String generatePaynetSignature(String message)throws Exception{

        if (s3Client == null){
            setS3Client();
        }
        // path to private key and public certificate
        String bucketName = env.getProperty("AWS_BUCKET_ASSET");
        String keyName = env.getProperty("paynet_private_key");
        S3Object s3Object = s3Client.getObject(bucketName,keyName);
        try{
            // Request Signing (Use to construct Request Message X-Signature)
            Signature signature = Signature.getInstance(PAYNET_SIGNATURE_ALGORITHM);
            signature.initSign(createPrivateKeyInstance(s3Object));
            signature.update(message.getBytes(StandardCharsets.UTF_8));

            return Base64.getEncoder().encodeToString(signature.sign());
        }
        finally {
            if (s3Object != null) {
                s3Object.close();
            }
        }
    }

    public static PrivateKey createPrivateKeyInstance(S3Object s3PathToKey)
            throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(s3PathToKey.getObjectContent())
        )) {
            String content = reader.lines()
                    .filter(line -> !line.startsWith("-----"))
                    .collect(Collectors.joining());
            KeyFactory factory = KeyFactory.getInstance(PAYNET_KEY_ALGORITHM);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
                    Base64.getDecoder().decode(content)
            );
            return factory.generatePrivate(keySpec);
        }finally {
            if (s3PathToKey != null) {
                s3PathToKey.close();
            }
        }
    }



    /////////////////////////////Test//////////////////////////////////

}
