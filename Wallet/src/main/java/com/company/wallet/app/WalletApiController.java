package com.company.wallet.app;

//import io.jmix.core.DataManager;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.company.wallet.entity.*;
import com.company.wallet.form.*;
import com.company.wallet.listener.AccountBgdEventListener;
import com.company.wallet.services.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.jmix.core.*;
import io.jmix.core.querycondition.LogicalCondition;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.core.security.Authenticated;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import io.jmix.core.security.CurrentAuthentication;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

//@Component
@RestController
@RequestMapping("wallet")
public class WalletApiController {

    static Logger logger = LogManager.getLogger(WalletApiController.class.getName());

    @Autowired
    private TimeSource timeSource;
    @Autowired
    private DataManager dataManager;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private CurrentAuthentication currentAuthentication;

    @Autowired
    private AccountBgdEventListener accountBgdEventListener;

    @Autowired
    private EntitySerialization entitySerialization;

    @Autowired
    private SupplierGoldAPIController supplierGoldAPIController;

    @Autowired
    private PaymentGatewayAPIController paymentGatewayAPIController;

    @Autowired
    private GenerateReceipt generateReceipt;

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private Environment env;

    @Autowired
    private ApiHandlerService apiHandlerService;

    @Autowired
    private DataLoaderService dataLoaderService;

    @Autowired
    private ProxyApiService proxyApiService;

    private AmazonS3 s3Client = null;

    public void setS3Client(){
        String regionName = env.getProperty("AWS_REGION");
        Regions clientRegion = Regions.fromName(regionName);
        String accessKey = env.getProperty("AWS_ACCESS_KEY");
        String secretKey = env.getProperty("AWS_SECRET_KEY");

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);
        s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(clientRegion)
                .build();
    }

    AccountBgd BURSA_ASSET_WALLET ;
    AccountBgd BURSA_CASH_WALLET ;
    AccountBgd SUPPLIER_CASH_WALLET;
    AccountBgd SUPPLIER_ASSET_WALLET;

    TransStatus TX_STATUS_COMPLETED;
    TransStatus TX_STATUS_FAILED;
    TransStatus TX_STATUS_PENDING;
    TransStatus TX_STATUS_REJECT;
    TransStatus TX_STATUS_PROCESSING;
    TransStatus TX_STATUS_CANCELLED;

    List<TransactionType> TX_TYPE_LIST;
    TransactionType TX_TYPE_TOPUP;

    TransactionType TX_TYPE_WITHDRAW;
    TransactionType TX_TYPE_BUY;
    TransactionType TX_TYPE_SELL;
    TransactionType TX_TYPE_FEE;
    TransactionType TX_TYPE_TRANSFER;
    TransactionType TX_TYPE_REDEEM;
    TransactionType TX_TYPE_MINTING_FEE;
    TransactionType TX_TYPE_COURIER_FEE;
    TransactionType TX_TYPE_TAKAFUL_FEE;
    TransactionType TX_TYPE_SST;
    TransactionType TX_TYPE_GST;

    TransactionType TX_TYPE_TOPUP_FEE;

    TransactionType TX_TYPE_WITHDRAW_FEE;

    TransactionType TX_TYPE_TRANSFER_FEE;

    TransactionType TX_TYPE_BUY_FEE;

    TransactionType TX_TYPE_SELL_FEE;

    TransMethod TX_METHOD_ONLINE_BANKING;

    RedeemTrackerStatus TRACKER_STATUS_REQUEST_PLACED;


    private List<PaynetBankList> paynetBankLists;
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

    @PostMapping("setAccountNo")
    public String setAccountNo(String lastuuid) {
        String accountNo = lastuuid;
        try{
            String timestamp = String.valueOf(Instant.now().getEpochSecond());
            String seqNo = generateSeqNo("AcctNo", "4");
            accountNo = timestamp.toString()+seqNo;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return accountNo;
    }

    @GetMapping("/testApi")
    public String testApi() throws JsonProcessingException {
        logger.info("Start");
        notificationService.pushNotificationInternal("norian@milradius.com.my","Try push internal","Saje","","FC009","NC01","NT02");
        logger.info("IM FIRSR");
        return "hei";
    }


    @GetMapping("generateSeqNumber")
    public String generateSeqNo(String code, String length) {
        Integer seqNo = 0;
        boolean done = false;
        Integer retry =0;
        while(!done && retry<5) {
            seqNo=0;
            try {

                Date dateToday = Date.from(LocalDate.now().atStartOfDay(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());


                String query1="select e from SequenceNumber e " +
                        "where e.code = :code " +
                        "and e.seqDate = :dateToday " +
                        "order by e.seqNo desc";
                List<SequenceNumber> currentSeqNo = this.dataManager.load(SequenceNumber.class)
                        .query(query1)
                        .parameter("code", code)
                        .parameter("dateToday", dateToday)
                        .list();

                if (currentSeqNo.size() > 0) {
                    seqNo = currentSeqNo.get(0).getSeqNo() + 1;

                    currentSeqNo.get(0).setSeqNo(seqNo);
                    this.dataManager.save(currentSeqNo.get(0));

                } else {
                    seqNo = 1;

                    SequenceNumber sequenceNo = this.dataManager.create(SequenceNumber.class);
                    sequenceNo.setCode(code);
                    sequenceNo.setSeqDate(dateToday);
                    sequenceNo.setSeqNo(seqNo);
                    this.dataManager.save(sequenceNo);
                }

                logger.info("SEC NUMBER GENERATED: ");
                logger.info(String.format("%0" + length + "d", seqNo));

                done=true;

            } catch (Exception e) {
                logger.error("ERROR WHEN GENERATING SEQ NUMBER BECAUSE LOCKING");
                retry += 1;
//            return generateSeqNo(code,length);
//                generateSeqNo(code, length);
//                WalletApiController walletApiController = new WalletApiController();
//                walletApiController.generateSeqNo(code, length);
            }
        }
        logger.info("Seq Number "+seqNo);
        return String.format("%0" + length + "d", seqNo);
    }

    public static String generateRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(CHARACTERS.length());
            sb.append(CHARACTERS.charAt(index));
        }
        return sb.toString();
    }

    public WalletType getWalletType(String walletCode) {
        String query1="select e from WalletType e where e.code = :code";
        return this.dataManager.load(WalletType.class)
                .query(query1)
                .parameter("code", walletCode).one();
    }

    public RegType getRegType(String regCode) {
        String query1="select e from RegType e where e.code = :code";
        return this.dataManager.load(RegType.class)
                .query(query1)
                .parameter("code", regCode).one();
    }
    public UnitOfMeasure getUnitMeasure(String uomCode) {
        String query1="select e from UnitOfMeasure e where e.code = :code";
        return this.dataManager.load(UnitOfMeasure.class)
                .query(query1)
                .parameter("code", uomCode).one();
    }

    public TransMethod getTransactionMethod(String code) {
        String query1="select e from TransMethod e where e.code = :code";
        return this.dataManager.load(TransMethod.class)
                .query(query1)
                .parameter("code", code).one();
    }

    public TransStatus getTransactionStatus(String code) {
        String query1="select e from TransStatus e where e.code = :code";
        return this.dataManager.load(TransStatus.class)
                .query(query1)
                .parameter("code", code).one();
    }

    public TransactionType getTransactionType(String code) {
        String query1="select e from TransactionType e where e.code = :code";
        return this.dataManager.load(TransactionType.class)
                .query(query1)
                .parameter("code", code).one();
    }

    public List<TransactionType> getTransactionTypeList() {
        return this.dataManager.load(TransactionType.class).all().list();
    }

    public Date getDateToday() {
        return Date.from(LocalDate.now().atStartOfDay(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
    }
    public Date getDateTimeNow() {
        return Date.from(LocalDateTime.now().atZone(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
    }

    public String getTopUpMethod(String referenceId){
        Optional<PaynetRef> paynetRef = this.dataManager.load(PaynetRef.class)
                .query("select e from PaynetRef e where e.referenceId =:referenceId")
                .parameter("referenceId", referenceId).optional();
        if(paynetRef.isPresent()){
            List<PaynetBankList> bank = paynetBankLists.stream().filter(
                    f->f.getCode().equals(paynetRef.get().getBicCode())).toList();
            if(bank.size()!=0){
                return(bank.get(0).getName());
            }
            else
                return "-";
        }
        else return "-";
    }

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {

        logger.info("hello world, I have just started up");

        //GET ACCOUNT OF BURSA
        BURSA_ASSET_WALLET = getAdminsWallet("D01", "E02");
        BURSA_CASH_WALLET = getAdminsWallet("D01", "E01");

        SUPPLIER_ASSET_WALLET = getAdminsWallet("D04","E02");
        SUPPLIER_CASH_WALLET = getAdminsWallet("D04","E01");

        TX_STATUS_COMPLETED = getTransactionStatus("G01");
        TX_STATUS_FAILED = getTransactionStatus("G02");
        TX_STATUS_PENDING = getTransactionStatus("G05");
        TX_STATUS_REJECT = getTransactionStatus("G07");
        TX_STATUS_PROCESSING = getTransactionStatus("G08");
        TX_STATUS_CANCELLED = getTransactionStatus("G13");

        TX_TYPE_LIST = getTransactionTypeList();
        TX_TYPE_WITHDRAW = getTransactionType("F02");
        TX_TYPE_TOPUP = getTransactionType("F01");
        TX_TYPE_BUY = getTransactionType("F05");
        TX_TYPE_SELL = getTransactionType("F06");
        TX_TYPE_FEE = getTransactionType("F09");
        TX_TYPE_TRANSFER = getTransactionType("F03");
        TX_TYPE_REDEEM = getTransactionType("F04");
        TX_TYPE_SST = getTransactionType("F09/02");
        TX_TYPE_GST = getTransactionType("F09/03");
        TX_TYPE_COURIER_FEE=getTransactionType("F09/B03");
        TX_TYPE_TAKAFUL_FEE=getTransactionType("F09/B04");
        TX_TYPE_MINTING_FEE=getTransactionType("F09/B05");

        TX_TYPE_TOPUP_FEE =getTransactionType("F09/F01");
        TX_TYPE_WITHDRAW_FEE= getTransactionType("F09/F02");
        TX_TYPE_TRANSFER_FEE = getTransactionType("F09/F03");
        TX_TYPE_BUY_FEE=getTransactionType("F09/F05");
        TX_TYPE_SELL_FEE=getTransactionType("F09/F06");

        TX_METHOD_ONLINE_BANKING = getTransactionMethod("H01");

        TRACKER_STATUS_REQUEST_PLACED = dataLoaderService.getRedeemTrackerStatus("G41");
        loadPaynetbankLists();
    }
    public void loadPaynetbankLists(){
        this.paynetBankLists = this.dataManager.load(PaynetBankList.class).all().list();
    }

    public Boolean accountValidator(AccountBgd accountBgd){
        //to check whether account received from payload is same as the authenticated
        String currentUser = currentAuthentication.getUser().getUsername();
        return accountBgd.getAccOwner().toString().equals(currentUser);
    }

    public AccountBgd getAdminsWallet(String regCode,String walletTypeCode){
        String query1="select e from AccountBgd e " +
                "where e.regType.code = :regTypeCode1 " +
                "and e.walletType.code = :walletTypeCode1";
        return this.dataManager.load(AccountBgd.class)
                .query(query1)
                .parameter("regTypeCode1", regCode)
                .parameter("walletTypeCode1", walletTypeCode)
                .one();
    }


    @PostMapping("/createWallet")
    public ResponseEntity createWallet(@RequestBody List<WalletForm> form) {

        //// Checking for Current User ////
        UserDetails userDetails = currentAuthentication.getUser();
        logger.info("Current User Is : " + userDetails.getUsername());
        String currentUsername = userDetails.getUsername();

        //// Checking for Acccount Exits or Not ////
        String query1="select e from AccountBgd e where e.accOwner = :accOwner";
        List<AccountBgd> acctBgd = this.dataManager.load(AccountBgd.class)
                .query(query1)
                .parameter("accOwner", currentUsername)
                .list();
        logger.info(acctBgd);

        if (acctBgd == null || acctBgd.isEmpty()) {
            logger.info("Create Account");
            try {
            form.forEach(e -> {
                AccountBgd entity = this.dataManager.create(AccountBgd.class);
                entity.setUnitMeasure(getUnitMeasure(e.getUomCode()));
                entity.setWalletType(getWalletType(e.getWalletTypeCode()));
                entity.setRegType(getRegType(e.getRegTypeCode()));
                entity.setAccOwner(currentUsername);
                this.dataManager.save(entity);
            });
                return ResponseEntity.status(HttpStatus.CREATED).body("{\"Msg\":\"Success Create Account\"}");
            } catch (Exception ex) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
            }
        }
        else
        {
            logger.info("data is already exists");
            return ResponseEntity.status(HttpStatus.CONFLICT).body("{\"Msg\":\"Account Already Exists\"}");
        }
    }

    @GetMapping("/walletByUser")
    public ResponseEntity walletByUser() {
        try {
            UserDetails userDetails = currentAuthentication.getUser();
            logger.info("Current User Logsin = " + userDetails.getUsername());
            String query1="select e from AccountWalletView e " +
                    "where e.accountOwner = :accountOwner " +
                    "order by e.code asc";
            List<AccountWalletView> walletDetails = this.dataManager.load(AccountWalletView.class)
                    .query(query1)
                    .parameter("accountOwner", userDetails.getUsername())
                    .list();

            if(walletDetails.size() == 0) {
//              return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Wallet Not Found");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"Msg\":\"Wallet Not Found\"}");
            }

            return ResponseEntity.status(HttpStatus.OK).body(walletDetails);

        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

//    @PutMapping("walletTopup_old")
    public ResponseEntity WalletTopup_old(@RequestBody List<WalletForm> form) {
        String reference_id = "F01" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F01", "6");
        try {
            UserDetails userDetails = currentAuthentication.getUser();
            Boolean thirdPartyRespond = false;

            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            form.forEach(e -> {

                String query1="select a from AccountBgd a " +
                        "where a.acctNumber = :acctNumber1";
                List<AccountBgd> accountBgd1 = this.dataManager.load(AccountBgd.class)
                        .query(query1)
                        .parameter("acctNumber1", e.getAcctNumber())
                        .list();

                String query2="select a from AccountBgd a " +
                        "where a.acctNumber = :acctNumber1";
                AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
                        .query(query2)
                        .parameter("acctNumber1", e.getAcctNumber())
                        .one();

                TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
                transactionAcct.setReferenceId(transactionAcct.getId().toString());
                transactionAcct.setAccount(accountBgd);
                transactionAcct.setTransMethod(getTransactionMethod(e.getTransactionMethodCode()));
                transactionAcct.setTransType(getTransactionType(e.getTransactionTypeCode()));
                transactionAcct.setTransStatus(getTransactionStatus(e.getStatusCode()));
                transactionAcct.setAmount(e.getAmount());

                this.dataManager.save(transactionAcct);

                //create transaction for fee charge
                List<CreateTransactionDTO> dtoList = new ArrayList<>();
                e.getFeesForms().forEach(f -> {
                    AccountBgd creditAccount = getWallet(accountBgd.getAccOwner(), "E01");
                    if(f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) > 0) {
                        CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                        dto.setCreditAcct(creditAccount);
                        dto.setDebitAcct(BURSA_CASH_WALLET);
                        dto.setCreditType(TX_TYPE_FEE);
                        dto.setDebitType(TX_TYPE_FEE);
                        dto.setAmount(f.getChargeFees());
                        dto.setReference(transactionAcct.getReferenceId());
                        dto.setStatus(TX_STATUS_COMPLETED);
                        dtoList.add(dto);
                    }
                });
                createTransaction(dtoList);

                responseDTO.setAmount(e.getAmount());

                responseDTO.setDate(timeSource.now().toLocalDateTime());
                responseDTO.setReference(reference_id);
                responseDTO.setStatus("G01");
                responseDTO.setDescription("SUCCESFUL");
                responseDTO.setTransactionType("F01");
            });
//            return ResponseEntity.ok("Wallet Successful topUp");
//            return ResponseEntity.status(HttpStatus.CREATED).body("{\"Msg\":\"Wallet Successful Topup\"}");
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

        } catch (Exception ex) {
            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F01");
            responseDTO.setSuccess(false);
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }


    public TransMethod getTransactionMethodMetadata(String code){

        switch (code) {
            case "H01":
                return TX_METHOD_ONLINE_BANKING;
            default:
                return null;

        }
    }


    @PostMapping("preTopup")
    public ResponseEntity preTopup(@RequestBody List<TopupWalletForm> form) {

        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        responseDTO.setDate(timeSource.now().toLocalDateTime());
        logger.info("pre TOPUP: requested by "+currentAuthentication.getAuthentication().getName());
        String reference_id = "F01" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F01", "6");
        try {

            form.forEach(e -> {

                String query1="select a from AccountBgd a " +
                        "where a.acctNumber = :acctNumber1";
                AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
                        .query(query1)
                        .parameter("acctNumber1", e.getAcctNumber())
                        .one();

                if(!accountValidator(accountBgd)){
                    logger.error("TOPUP: ACCOUNT INVALID");
                    throw new RuntimeException("ACCOUNT_INVALID");
                }

                TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
                transactionAcct.setReferenceId(reference_id);
                transactionAcct.setReceiptNumber("RC"+transactionAcct.getReferenceId());
                transactionAcct.setAccount(accountBgd);
                transactionAcct.setTransType(TX_TYPE_TOPUP);
                transactionAcct.setTransStatus(TX_STATUS_PENDING); //pending status
                transactionAcct.setAmount(e.getAmount());
                transactionAcct.setTransMethod(getTransactionMethodMetadata(e.getTransactionMethodCode()));


                //create transaction for fee charge -> change hardcode status
                List<CreateTransactionDTO> dtoList = new ArrayList<>();
                e.getFeesForms().forEach(f -> {
                    if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) >= 0) {
                        CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                        dto.setCreditAcct(accountBgd);
                        dto.setDebitAcct(BURSA_CASH_WALLET);
                        dto.setCreditType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setDebitType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setAmount(f.getChargeFees());
                        if(f.getSetupUom().equals("%")) {
                            dto.setPercent(f.getSetupFees());
                        }
                        dto.setReference(reference_id);
                        dto.setStatus(TX_STATUS_PENDING); //pending status
                        dtoList.add(dto);
                    }
                });

                Date transactionDate=getDateTimeNow();
                transactionAcct.setCreatedDate(transactionDate);
                this.dataManager.save(transactionAcct);
                createTransaction(dtoList);


                responseDTO.setReference(reference_id);
                responseDTO.setAmount(e.getAmount());
                responseDTO.setStatus("G01");
                responseDTO.setDescription("SUCCESFUL");
                responseDTO.setTransactionType("F01");
            });

            //if success
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

        } catch (Exception ex) {
            responseDTO.setReference(reference_id);
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F02");
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @PutMapping("walletTopup")
    public ResponseEntity WalletTopup(@RequestBody UpdateTransactionTopupForm form) {

        if(form.getReferenceId().equals("") || form.getReferenceId()==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid reference_id");
        }

        logger.info("TOPUP: requested by "+currentAuthentication.getAuthentication().getName());
        DepositResponseDTO responseDTO = dataManager.create(DepositResponseDTO.class);
        responseDTO.setDate(timeSource.now().toLocalDateTime());
        try {
            String integrationServiceCode = "ITR02/05";
            UserDetails userDetails = currentAuthentication.getUser();
            Boolean thirdPartyRespond = false;



            //load all transaction with given referenceid
            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();


            logger.info("List of transaction: "+transactionAcctList.size());
            if(transactionAcctList.size() == 0){
                logger.error("No transaction was found for referenceId: "+form.getReferenceId());
                throw new RuntimeException("No transaction was found");
            }


//            AccountBgd investorCashAccount = null;
            BigDecimal topupAmount=null;
//            AtomicReference<BigDecimal> totalCash= new AtomicReference<>(BigDecimal.ZERO);
            for(TransactionAcct transactionAcct : transactionAcctList){

                if(!transactionAcct.getTransStatus().equals(TX_STATUS_PENDING)){
                    throw new RuntimeException("Invalid ReferenceId , Transaction  is not currently PENDING");
                }
                if(!transactionAcct.getAccount().equals(BURSA_CASH_WALLET)) {
//                    totalCash.getAndSet(totalCash.get().add(transactionAcct.getAmount().negate()));
                    if( transactionAcct.getTransType().equals(TX_TYPE_TOPUP) ) {
//                        investorCashAccount = transactionAcct.getAccount();
                        topupAmount = transactionAcct.getAmount().abs();

                    }
                }
            }

                // initiate payment -paynet
                logger.info("idNumber:"+form.getIdentificationNumber());
                PaynetInitiatePaymentForm initiatePaymentForm = new PaynetInitiatePaymentForm();
                initiatePaymentForm.setChannelCode(form.getChannelCode());
                initiatePaymentForm.setBicCode(form.getBicCode());
                initiatePaymentForm.setAmount(topupAmount);
                initiatePaymentForm.setName(form.getCustomerName());
                initiatePaymentForm.setIdentificationTypeCode(form.getIdentificationTypeCode());
                initiatePaymentForm.setIdentificationNumber(form.getIdentificationNumber());
                initiatePaymentForm.setRecipientReference(form.getReferenceId());
                ResponseEntity<String> initiatePayment = paymentGatewayAPIController.initiatePayment(initiatePaymentForm);

                if(initiatePayment.getStatusCode().value() == 200){
                    String endToEndId ="";
                    try {
                        ObjectMapper objectMapper = new ObjectMapper();
                        JsonNode jsonNode = objectMapper.readTree(initiatePayment.getBody());
                        String redirectUrl = jsonNode.get("redirectUrl").asText();
                        responseDTO.setRedirectUrl(redirectUrl);

                        endToEndId = jsonNode.get("endToEndId").asText();
                    } catch (JsonProcessingException ex) {
                        logger.error(ex.getMessage());
                        throw new RuntimeException("Error when parsing json response initiate payment");
                    }

                    responseDTO.setReference(form.getReferenceId());
                    responseDTO.setStatus("G05");
                    responseDTO.setDescription("PENDING");
                    responseDTO.setTransactionType("F01");

                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MINUTE, 5);
                    Date timerEndDate = calendar.getTime();

                    PaynetRef paynetRef = this.dataManager.create(PaynetRef.class);
                    paynetRef.setEndToEndId(endToEndId);
                    paynetRef.setReferenceId(form.getReferenceId());
                    paynetRef.setBicCode(form.getBicCode());
                    paynetRef.setChannelCode(form.getChannelCode());
                    paynetRef.setServiceType(integrationServiceCode);
                    paynetRef.setTimerEnd(timerEndDate);
                    this.dataManager.save(paynetRef);

                }
                else {
                    throw new RuntimeException("Failed during initiate payment api: "+initiatePayment.getBody());
                }

            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

        } catch (Exception ex) {


            responseDTO.setReference(form.getReferenceId());
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F01");
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }


    @PutMapping("walletTopupOld")
    public ResponseEntity WalletTopupOld(@RequestBody List<TopupWalletForm> form) {
        logger.info("TOPUP: requested by "+currentAuthentication.getAuthentication().getName());
        String reference_id = "F01" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F01", "6");

        try {
            String integrationServiceCode = "ITR02/05";
            UserDetails userDetails = currentAuthentication.getUser();
            Boolean thirdPartyRespond = false;

            DepositResponseDTO responseDTO = dataManager.create(DepositResponseDTO.class);

            form.forEach(e -> {

                String query1="select a from AccountBgd a " +
                        "where a.acctNumber = :acctNumber1";
                AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
                        .query(query1)
                        .parameter("acctNumber1", e.getAcctNumber())
                        .one();

                if(!accountValidator(accountBgd)){
                    logger.error("TOPUP: ACCOUNT INVALID");
                   throw new RuntimeException("ACCOUNT_INVALID");
                }

                TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
                transactionAcct.setReferenceId(reference_id);
                transactionAcct.setReceiptNumber("RC"+transactionAcct.getReferenceId());
                transactionAcct.setAccount(accountBgd);
                transactionAcct.setTransMethod(getTransactionMethod(e.getTransactionMethodCode()));
                transactionAcct.setTransType(getTransactionType(e.getTransactionTypeCode()));
                transactionAcct.setTransStatus(getTransactionStatus("G05")); //pending status
                transactionAcct.setAmount(e.getAmount());

                this.dataManager.save(transactionAcct);

                    //create transaction for fee charge -> change hardcode status
                    List<CreateTransactionDTO> dtoList = new ArrayList<>();
                    e.getFeesForms().forEach(f -> {
                        AccountBgd creditAccount = getWallet(accountBgd.getAccOwner(), "E01");
                        if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) > 0) {
                            TransactionType feeType = getTxTypeTrade(f.getFeeTypeCode());
                            CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                            dto.setCreditAcct(creditAccount);
                            dto.setDebitAcct(BURSA_CASH_WALLET);
                            dto.setCreditType(feeType);
                            dto.setDebitType(feeType);
                            dto.setAmount(f.getChargeFees());
                            if(f.getSetupUom().equals("%")) {
                                dto.setPercent(f.getSetupFees());
                            }
                            dto.setReference(reference_id);
                            dto.setStatus(getTransactionStatus("G05")); //pending status
                            dtoList.add(dto);
                        }
                    });
                    createTransaction(dtoList);

                // initiate payment -paynet
                logger.info("idNumber:"+e.getIdentificationNumber());
                PaynetInitiatePaymentForm initiatePaymentForm = new PaynetInitiatePaymentForm();
//                initiatePaymentForm.setGpsCoordinates(e.getGpsCoordinates());
//                initiatePaymentForm.setIpAddress(e.getIpAddress());
                initiatePaymentForm.setChannelCode(e.getChannelCode());
                initiatePaymentForm.setBicCode(e.getBicCode());
                initiatePaymentForm.setAmount(e.getAmount());
                initiatePaymentForm.setCurrency(e.getCurrency());
                initiatePaymentForm.setName(e.getCustomerName());
                initiatePaymentForm.setIdentificationTypeCode(e.getIdentificationTypeCode());
                initiatePaymentForm.setIdentificationNumber(e.getIdentificationNumber());
                initiatePaymentForm.setRecipientReference(transactionAcct.getReferenceId());
                ResponseEntity<String> initiatePayment = paymentGatewayAPIController.initiatePayment(initiatePaymentForm);

                if(initiatePayment.getStatusCode().value() == 200){
                    String endToEndId ="";
                    try {
                        ObjectMapper objectMapper = new ObjectMapper();
                        JsonNode jsonNode = objectMapper.readTree(initiatePayment.getBody());
                        String redirectUrl = jsonNode.get("redirectUrl").asText();
                        responseDTO.setRedirectUrl(redirectUrl);

                        endToEndId = jsonNode.get("endToEndId").asText();
                    } catch (JsonProcessingException ex) {
                        logger.error(ex.getMessage());
                    }

                    responseDTO.setDate(timeSource.now().toLocalDateTime());
                    responseDTO.setReference(reference_id);
                    responseDTO.setStatus("G05");
                    responseDTO.setDescription("PENDING");
                    responseDTO.setTransactionType("F01");

                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.MINUTE, 5);
                    Date timerEndDate = calendar.getTime();

                    PaynetRef paynetRef = this.dataManager.create(PaynetRef.class);
                    paynetRef.setEndToEndId(endToEndId);
                    paynetRef.setReferenceId(transactionAcct.getReferenceId());
                    paynetRef.setBicCode(e.getBicCode());
                    paynetRef.setChannelCode(e.getChannelCode());
                    paynetRef.setServiceType(integrationServiceCode);
                    paynetRef.setTimerEnd(timerEndDate);
                    this.dataManager.save(paynetRef);
                }
                else {
                    logger.info("PayNet initiate payment failed.");
                    responseDTO.setDate(timeSource.now().toLocalDateTime());
                    responseDTO.setReference(reference_id);
                    responseDTO.setStatus("G02");
                    responseDTO.setDescription(initiatePayment.getBody());
                    responseDTO.setTransactionType("F01");

//                    PaynetRef paynetRef = this.dataManager.create(PaynetRef.class);
////                    paynetRef.setEndToEndId(endToEndId);
//                    paynetRef.setReferenceId(transactionAcct.getReferenceId());
//                    paynetRef.setServiceType(integrationServiceCode);
//                    this.dataManager.save(paynetRef);
                }
            });

            if(responseDTO.getStatus().equals(TX_STATUS_FAILED.getCode())){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseDTO.getDescription());
            }
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

        } catch (Exception ex) {
            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F01");
            responseDTO.setSuccess(false);
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @GetMapping("topupStatus")
    public ResponseEntity getTopupStatus(@RequestParam String endToEndId, String endToEndIdSignature){
        try{
            logger.info("calling api -> /topupStatus?endToEndId="+endToEndId);
            String query1="select a from PaynetRef a " +
                    "where a.endToEndId = :endToEndId";
            PaynetRef paynetRef = this.dataManager.load(PaynetRef.class)
                    .query(query1)
                    .parameter("endToEndId", endToEndId)
                    .one();

            TransactionAcct transactionAcct = this.dataManager.load(TransactionAcct.class)
                    .condition(LogicalCondition.and(
                            PropertyCondition.equal("referenceId",paynetRef.getReferenceId()),
                            PropertyCondition.equal("transType",TX_TYPE_TOPUP)
                    )).one();

            if(transactionAcct.getTransStatus().equals(TX_STATUS_PENDING)){
                logger.info("Transaction pending...");
                ResponseEntity<String> statusInquiry = paymentGatewayAPIController.statusInquiry(endToEndId, "BA");

                if(statusInquiry.getStatusCode().value() == 200){
                    JsonObject jsonResponse = JsonParser.parseString(statusInquiry.getBody()).getAsJsonObject();
                    String paymentStatus = jsonResponse.get("transactionStatus").getAsString();
                    TransStatus transStatus;
//                    if (Objects.equals(paymentStatus,Substate.RECEIVED)){
//
//                    }
                    if (Objects.equals(paymentStatus, Substate.CLEARED.getId()))
                        transStatus = TX_STATUS_COMPLETED;
                    else if (Objects.equals(paymentStatus, Substate.CANCELLED.getId()))
                        transStatus = TX_STATUS_CANCELLED;
                    else if (Objects.equals(paymentStatus, Substate.REJECTED.getId()))
                        transStatus = TX_STATUS_REJECT;
                    else
                        transStatus = TX_STATUS_PENDING;

                    //update topupstatus
                    String query2="select e from TransactionAcct e " +
                            "where e.referenceId = :referenceId";
                    List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                            .query(query2)
                            .parameter("referenceId", paynetRef.getReferenceId())
                            .list();
                    SaveContext saveContext = new SaveContext();
                    transactionAcctList.forEach(e->{
                        e.setTransStatus(transStatus);
                        saveContext.saving(e);
                    });
                    this.dataManager.save(saveContext);
                    transactionAcct.setTransStatus(transStatus);
                }
            }

            List<TransactionAcct> transactionAcctFees = this.dataManager.load(TransactionAcct.class)
                    .condition(LogicalCondition.and(
                            PropertyCondition.equal("referenceId",paynetRef.getReferenceId()),
                            PropertyCondition.equal("account",transactionAcct.getAccount()),
                            PropertyCondition.notEqual("transType",TX_TYPE_TOPUP)
                    )).list();

            List<FeesForm> transactionFees = new ArrayList<>();
            transactionAcctFees.forEach(e -> {
                FeesForm fee = new FeesForm();
                fee.setFeeType(e.getTransType().getName());
                fee.setFeeTypeCode(e.getTransType().getCode());
                fee.setSetupFees(e.getPercent());
                fee.setSetupUom("%");
                fee.setSetupUomCode("J04");
                fee.setChargeFees(e.getAmount().abs());
                fee.setChargeUom(e.getAccount().getUnitMeasure().getName());
                fee.setChargeUomCode(e.getAccount().getUnitMeasure().getCode());
                transactionFees.add(fee);
            });

            Date createdDate = transactionAcct.getCreatedDate();
            LocalDateTime localDateTime = createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

            TopupDetailsForm responseForm = new TopupDetailsForm();
            responseForm.setAmount(transactionAcct.getAmount());
            responseForm.setDate(localDateTime);
            responseForm.setReference(transactionAcct.getReferenceId());
            responseForm.setStatus(transactionAcct.getTransStatus().getName());
            responseForm.setStatusCode(transactionAcct.getTransStatus().getCode());
            responseForm.setDescription("SUCCESFUL");
            responseForm.setTransactionType("F01");
            responseForm.setTransactionMethodCode(transactionAcct.getTransMethod().getCode());
            responseForm.setFees(transactionFees);


            //sent notification if success
            if(transactionAcct.getTransStatus().getCode().equals("G01")){
                //if completed sent notification
                String title = "Top-Up Successful";
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");

                String body ="You have topup RM " +decimalFormat.format(transactionAcct.getAmount().setScale(2, RoundingMode.HALF_UP))+".";

                logger.info("PAYMENT HAS BEEN DONE : Ref: "+transactionAcct.getReferenceId()+"Amount: "+transactionAcct.getAmount());
                //Run as async in background
                notificationService.pushNotificationInternal(transactionAcct.getAccount().getAccOwner(),title,body,"",transactionAcct.getReferenceId(),"NC01","NT02");

            }

            return ResponseEntity.status(HttpStatus.OK).body(responseForm);

        }catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }

    @PutMapping("topupStatusChecking")
    public ResponseEntity getTopupStatusPending(){
        logger.info("PUT: -> /wallet/topupStatusPending");
        try{
            //checking pending status
            String queryStrTopupPending = "select e from TopupPendingView e where e.statusCode = :statusCode and e.transactionCode = :transactionCode";
            List<TopupPendingView> topupPending = this.dataManager.load(TopupPendingView.class)
                    .query(queryStrTopupPending)
                    .parameter("statusCode", TX_STATUS_PENDING.getCode())
                    .parameter("transactionCode", TX_TYPE_TOPUP.getCode())
                    .sort(Sort.by(Sort.Direction.ASC,"createdDate"))
                    .maxResults(50)
                    .list();
            logger.info("Total pending = "+topupPending.size());

            topupPending.forEach(f->{
                if(f.getEndToEndId()!=null && !f.getEndToEndId().equals("")) {
                    String endToEndId = f.getEndToEndId();
                    String channelCode = f.getChannelCode();
                    String bicCode = f.getBicCode();

                    logger.info("checking topup status, endToEndId -> :" + endToEndId);
                    ResponseEntity<String> statusInquiry = paymentGatewayAPIController.statusInquiry(endToEndId, channelCode);

                    if (statusInquiry.getStatusCode().value() == 200) {
                        JsonObject jsonResponse = JsonParser.parseString(statusInquiry.getBody()).getAsJsonObject();
                        logger.info("statusInquiry response ->");
                        logger.info(jsonResponse);
                        String paymentStatus = jsonResponse.get("transactionStatus").getAsString();
                        TransStatus transStatus = TX_STATUS_PENDING;

                        if (Objects.equals(paymentStatus, Substate.RECEIVED.getId())) {
                            //call paynet cancel payment
                            PaynetCancelPayment cancelForm = new PaynetCancelPayment();
                            cancelForm.setChannelCode(channelCode);
                            cancelForm.setBicCode(bicCode);
                            cancelForm.setEndToEndId(endToEndId);
                            ResponseEntity<String> cancelPayment = paymentGatewayAPIController.cancelPayment(cancelForm);
                            if (cancelPayment.getStatusCode().value() == 200) {
                                JsonObject cancelResponse = JsonParser.parseString(Objects.requireNonNull(cancelPayment.getBody())).getAsJsonObject();
                                logger.info("cancelPayment response ->");
                                logger.info(cancelResponse.toString());
                                String cancelPaymentStatusCode = cancelResponse.getAsJsonObject("header")
                                        .getAsJsonObject("status")
                                        .get("code").getAsString();
                                if (cancelPaymentStatusCode.equals("ACSP")) {
                                    transStatus = TX_STATUS_CANCELLED;
                                } else
                                    transStatus = TX_STATUS_PENDING;
                            } else if (cancelPayment.getStatusCode().value() == 400) {
                                String responseBody = Objects.requireNonNull(cancelPayment.getBody());
                                if (responseBody.contains("<EOL>")) {
                                    responseBody = responseBody.replaceAll("<EOL>", "");
                                    int firstBraceIndex = responseBody.indexOf("{");
                                    int lastBraceIndex = responseBody.lastIndexOf("}");
                                    if (firstBraceIndex != -1 && lastBraceIndex != -1) {
                                        responseBody = responseBody.substring(firstBraceIndex, lastBraceIndex + 1);
                                    }
                                }
                                try {

                                    JsonObject cancelResponse = JsonParser.parseString(responseBody).getAsJsonObject();
                                    logger.info("cancelPayment response ->");
                                    logger.info(cancelResponse.toString());
                                    String code = cancelResponse.getAsJsonObject("header")
                                            .getAsJsonObject("status")
                                            .get("code").getAsString();
                                    String reason = cancelResponse.getAsJsonObject("header")
                                            .getAsJsonObject("status")
                                            .get("reason").getAsString();
                                    if (code.equals("RJCT") && reason.equals("U310")) {
                                        transStatus = TX_STATUS_REJECT;
                                    }
                                } catch (Exception ex) {
                                    logger.error(ex.getMessage());
                                    if (responseBody.contains("U310")) {
                                        transStatus = TX_STATUS_REJECT;
                                    }
                                }
                            } else
                                transStatus = TX_STATUS_PENDING;
                        } else if (Objects.equals(paymentStatus, Substate.RETRIEVED.getId())) {
                            //need to call paynet cancel
                            transStatus = TX_STATUS_CANCELLED;
                        } else if (Objects.equals(paymentStatus, Substate.CLEARED.getId()))
                            transStatus = TX_STATUS_COMPLETED;
                        else if (Objects.equals(paymentStatus, Substate.CANCELLED.getId()))
                            transStatus = TX_STATUS_CANCELLED;
                        else if (Objects.equals(paymentStatus, Substate.REJECTED.getId()))
                            transStatus = TX_STATUS_REJECT;
                        else
                            transStatus = TX_STATUS_PENDING;

                        //update topupstatus
                        String queryStrTrans = "select e from TransactionAcct e  where e.referenceId = :referenceId ";
                        List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                                .query(queryStrTrans)
                                .parameter("referenceId", f.getReferenceId())
                                .list();
                        SaveContext saveContext = new SaveContext();
                        TransStatus finalTransStatus = transStatus;
                        transactionAcctList.forEach(e -> {
                            e.setTransStatus(finalTransStatus);
                            saveContext.saving(e);
                        });
                        this.dataManager.save(saveContext);
                    }
                } else{
                    // if there is no endtoendid found, meaning user not submit to initiate payment and tx should be set to cancel
                    List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                            .query("select e from TransactionAcct e  where e.referenceId = :referenceId ")
                            .parameter("referenceId", f.getReferenceId())
                            .list();

                    transactionAcctList.forEach(e -> {
                        e.setTransStatus(TX_STATUS_CANCELLED);
                        dataManager.save(e);
                    });
                }
            });

        }catch (Exception ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
        logger.info("Response -> PUT: /wallet/topupStatusPending :" + HttpStatus.OK);
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

    @GetMapping("/WalletDetails")
    public ResponseEntity getWalletDetails() {
        try {
            UserDetails userDetails = currentAuthentication.getUser();
            logger.info("Current User Is : " + userDetails.getUsername());
            return ResponseEntity.status(HttpStatus.OK).body("test");

        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
    }


    @PostMapping("preWithdraw")
    public ResponseEntity preWithdraw(@RequestBody List<WalletWithdrawForm> form) {

        logger.info(" Pre Withdrawal called :");
        String reference_id = "F02" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F02", "6");
        try {

            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            for(WalletWithdrawForm e: form) {

                String query1="select e from AccountBgd e where e.acctNumber = :acctNumber";
                AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
                        .query(query1)
                        .parameter("acctNumber", e.getAcctNumber()).one();

                if(!accountValidator(accountBgd)){
                    logger.error("WITHDRAW: ACCOUNT INVALID");
                    throw new RuntimeException("ACCOUNT_INVALID");
                }

                //create Transaction for withdrawal with status PENDING
                TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
                transactionAcct.setReferenceId(reference_id);
                transactionAcct.setAccount(accountBgd);
                transactionAcct.setReceiptNumber("RC"+transactionAcct.getReferenceId());
                transactionAcct.setTransMethod(getTransactionMethod(e.getTransactionMethodCode()));
                transactionAcct.setTransType(TX_TYPE_WITHDRAW);
                transactionAcct.setTransStatus(TX_STATUS_PENDING); //set status as pending
                transactionAcct.setAmount(BigDecimal.valueOf(Math.abs(e.getTotalAmount().doubleValue())).negate());

                Date transactionDate=getDateTimeNow();

                //create transaction for fee charge with status pending
                AtomicReference<BigDecimal> totalFee = new AtomicReference<>(BigDecimal.valueOf(0));
                List<CreateTransactionDTO> dtoList = new ArrayList<>();
                e.getFeesForms().forEach(f -> {
//                    AccountBgd creditAccount = getWallet(accountBgd.getAccOwner(), "E01");
                    if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) >= 0) {
                        CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                        dto.setCreditAcct(accountBgd);
                        dto.setDebitAcct(BURSA_CASH_WALLET);
                        dto.setCreditType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setDebitType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setAmount(f.getChargeFees());
                        if(f.getSetupUom().equals("%")){
                            dto.setPercent(f.getSetupFees());
                        }
                        dto.setReference(transactionAcct.getReferenceId());
                        dto.setStatus(TX_STATUS_PENDING);
                        dtoList.add(dto);
                        totalFee.getAndSet(totalFee.get().add(f.getChargeFees()));
                        logger.info("CHARGE FEES, "+ dto.getCreditType().getName()+ " Amount: "+dto.getAmount().toString());
                    }
                });

                logger.info("TOTAL FEE WITHDRAWAL :"+totalFee.get().toString());

                logger.info("TOTAL WITHDRAWAL :"+totalFee.get().add(transactionAcct.getAmount().negate()).toString());
//                logger.info("TOTAL WITHDRAWAL :"+totalFee.get().add(transactionAcct.getAmount().negate()).toString()+". WALLET BALANCE: "+walletView.getAvailableAmount().toString());

                WithdrawalInfo withdrawalInfo = dataManager.create(WithdrawalInfo.class);
                withdrawalInfo.setRecipientBankBic(e.getInvestorBankBic());
                withdrawalInfo.setRecipientBankNo(e.getInvestorBankAccountNo());
                withdrawalInfo.setRecipientBankName(e.getInvestorBankName());

                withdrawalInfo.setSenderBankBic(env.getProperty("ambank_debitor_bank_bic"));
                withdrawalInfo.setSenderBankNo(env.getProperty("ambank_debitor_acc_no"));
                withdrawalInfo.setReference(reference_id);
                withdrawalInfo.setTransactionId(transactionAcct);

                transactionAcct.setCreatedDate(transactionDate);
                this.dataManager.save(transactionAcct);
                createTransaction(dtoList);
                this.dataManager.save(withdrawalInfo);

                responseDTO.setDate(timeSource.now().toLocalDateTime());
                responseDTO.setReference(reference_id);
                responseDTO.setAmount(e.getAmount());
                responseDTO.setStatus("G01");
                responseDTO.setDescription("SUCCESFUL");
                responseDTO.setTransactionType("F02");
            }
            //if success
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

        } catch (Exception ex) {
            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F02");
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @PostMapping("preTransfer")
    public ResponseEntity preTransfer(@RequestBody List<WalletTransferForm> form) {
        logger.info(" Pre Transfer called :");
        String reference_id = "F03" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F02", "6");
        try {

        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        for(WalletTransferForm e: form) {

            //get gold wallet sender
            String query1="select e from AccountBgd e where e.acctNumber = :acctNumber";
            AccountBgd accountBgdSender = this.dataManager.load(AccountBgd.class)
                    .query(query1)
                    .parameter("acctNumber", e.getAcctNumberSender())
                    .one();

            //check account bgd given is same as authenticated user's
            if(!accountValidator(accountBgdSender)){
                logger.error("TRANSFER: ACCOUNT INVALID");
                throw new RuntimeException("ACCOUNT_INVALID");
            }

            //make sure not sending to own account
            if(e.getAcctNumberSender().equals(e.getAcctNumberReceiver())){
                logger.error("TRANSFER: ACCOUNT RECEIVER SAME AS SENDER");
                throw new RuntimeException("ACCOUNT_RECEIVER_SAME_AS_SENDER");
            }

            // get gold wallet receiver
            String query2="select e from AccountBgd e where e.acctNumber = :acctNumber";
            AccountBgd accountBgdReceiver = this.dataManager.load(AccountBgd.class)
                    .query(query2)
                    .parameter("acctNumber", e.getAcctNumberReceiver())
                    .one();

            //get sender gold and cash balance
            String query3="select e from AccountWalletView e " +
                    "where e.accountOwner = :accountOwner1";
            List<AccountWalletView> walletSenderViews = this.dataManager.load(AccountWalletView.class)
                    .query(query3)
                    .parameter("accountOwner1", accountBgdSender.getAccOwner())
                    .list();
            AccountWalletView goldWalletView = walletSenderViews.stream().filter(f -> f.getCode().equals("E02")).toList().get(0);
            AccountWalletView cashWalletView = walletSenderViews.stream().filter(f -> f.getCode().equals("E01")).toList().get(0);

            //log detail
            logger.info("TRANSFER:  from :" + accountBgdSender.getAcctNumber()+ " OWNER: "+accountBgdSender.getAccOwner());
            logger.info("TRANSFER:  to :" + accountBgdReceiver.getAcctNumber()+ " OWNER: "+accountBgdReceiver.getAccOwner());
            logger.info("TRANSFER: amount(g) to be transfered: "+ e.getAmount());

            // calculate fees
            BigDecimal totalCashFees = new BigDecimal(0);
            List<CreateTransactionDTO> feeTransactionList = new ArrayList<>();
            for(FeesForm f : e.getFeesForms()) {
                if (f.getChargeUomCode().equals("J01")) { //if fees in cash
                    //get sender cash wallet
                    AccountBgd creditAccount = getWallet(accountBgdSender.getAccOwner(), "E01");
                    if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) >= 0) {
                        CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                        dto.setCreditAcct(creditAccount);
                        dto.setDebitAcct(BURSA_CASH_WALLET);
                        dto.setCreditType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setDebitType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setAmount(f.getChargeFees());
                        if(f.getSetupUom().equals("%")){
                            dto.setPercent(f.getSetupFees());
                        }
                        dto.setReference(reference_id);
                        dto.setStatus(TX_STATUS_PENDING);
                        dto.setBursaPrice(e.getBursaSellPrice()!=null?e.getBursaSellPrice():null);
                        feeTransactionList.add(dto);
                        logger.info("CHARGE FEES, "+ dto.getCreditType().getName()+ " Amount: "+dto.getAmount().toString());
                    }
                    //sum all cash fees
                    totalCashFees = totalCashFees.add(f.getChargeFees());
                }
            }

            logger.info("TRANSFER: Total cash fees: "+totalCashFees);
            logger.info("TRANSFER: Gold amount to deduct : "+e.getAmount()+ " Wallet Balance before: "+goldWalletView.getAvailableAmount());
            logger.info("TRANSFER: Cash amount to deduct: "+totalCashFees+ " Wallet Balance before: "+cashWalletView.getAvailableAmount());

            //creating transactions to db
            createTransaction(accountBgdSender, accountBgdReceiver, e.getAmount(), TX_TYPE_TRANSFER, TX_TYPE_TRANSFER, reference_id, TX_STATUS_PENDING, e.getReason(),e.getBursaSellPrice());
            createTransaction(feeTransactionList);
            e.setReference(reference_id);

            //if no error, success
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setAmount(e.getAmount());// gold being transfered
            responseDTO.setStatus("G01");
            responseDTO.setDescription("SUCCESFUL");
            responseDTO.setTransactionType("F03");
        }
        //if success
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

    } catch (Exception ex) {
        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        responseDTO.setDate(timeSource.now().toLocalDateTime());
        responseDTO.setReference(reference_id);
        responseDTO.setStatus("G02");
        responseDTO.setDescription(ex.getMessage());
        responseDTO.setTransactionType("F03");
        return ResponseEntity.badRequest().body(responseDTO);
    }
}

    @PutMapping("walletTransfer")
    public ResponseEntity walletTransfer(@RequestBody UpdateTransactionForm form,@RequestHeader(value = "Authorization")String authToken) {

        if(form.getReferenceId().equals("") || form.getReferenceId()==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid reference_id");
        }
        logger.info(" Transfer called :");
        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);

        try {
            //load all transaction with given referenceid
            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();

            logger.info("List of transaction: "+transactionAcctList.size());
            if(transactionAcctList.size() == 0){
                logger.error("No transaction was found for referenceId: "+form.getReferenceId());
                throw new RuntimeException("No transaction was found");
            }

            AccountBgd investorAssetWallet = null;
            AccountBgd investorCashWallet = null;

            String receiverUsername= null;
            String senderUsername=currentAuthentication.getUser().getUsername();

            BigDecimal transferAmount=null;
            AtomicReference<BigDecimal> totalCash= new AtomicReference<>(BigDecimal.ZERO);

            for(TransactionAcct transactionAcct : transactionAcctList) {

                //check the status is still pending
                if (!transactionAcct.getTransStatus().equals(TX_STATUS_PENDING)) {
                    throw new RuntimeException("Invalid ReferenceId , Transaction  is not currently PENDING");
                }
                //find sender cash wallet (if wallet type cash and not bursa's)
                if (transactionAcct.getAccount().getWalletType().getCode().equals("E01") && !transactionAcct.getAccount().equals(BURSA_CASH_WALLET)) {
                    //get total cash that must be paid (basicly this is the fees)
                    totalCash.getAndSet(totalCash.get().add(transactionAcct.getAmount().negate()));
                    investorCashWallet = transactionAcct.getAccount();

                } else if (transactionAcct.getAccount().getWalletType().getCode().equals("E02")) {
                        //find sender gold waller
                        if (transactionAcct.getAccount().getAccOwner().equals(senderUsername)) {
                            investorAssetWallet = transactionAcct.getAccount();
                            transferAmount = transactionAcct.getAmount().abs();
                        } else {
                            //find receiver username
                            receiverUsername = transactionAcct.getAccount().getAccOwner();
                        }
                    }
                }


//            String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(transactionDate);


            logger.info("TOTAL CASH Fees:"+totalCash.get().toString());


            String query3="select a from AccountWalletView a " +
                    "where a.acctNumber = :acctNumber1";
            AccountWalletView senderCashView = this.dataManager.load(AccountWalletView.class)
                    .query(query3)
                    .parameter("acctNumber1", investorCashWallet.getAcctNumber())
                    .one();
            String query4="select a from AccountWalletView a " +
                    "where a.acctNumber = :acctNumber1";
            AccountWalletView senderAssetView = this.dataManager.load(AccountWalletView.class)
                    .query(query4)
                    .parameter("acctNumber1", investorAssetWallet.getAcctNumber())
                    .one();

            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(form.getReferenceId());
            responseDTO.setAmount(transferAmount);
            responseDTO.setTransactionType("F03");
            ////// Check balance
            if(totalCash.get().compareTo(senderCashView.getAvailableAmount())>0) {

                logger.info("NOT ENOUGH CASH BALANCE , TOTAL FEES :"+totalCash.get()+". WALLET BALANCE: "+senderCashView.getAvailableAmount().toString());
                responseDTO.setStatus("G02");
                responseDTO.setDescription("NOT_ENOUGH_CASH_BALANCE");
                throw new RuntimeException("NOT_ENOUGH_CASH_BALANCE");
//                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
            }
            else if(transferAmount.compareTo(senderAssetView.getAvailableAmount())>0) {
                logger.info("NOT ENOUGH GOLD BALANCE , TOTAL TRANSFER :"+transferAmount+". WALLET BALANCE: "+senderAssetView.getAvailableAmount().toString());
                responseDTO.setStatus("G02");
                responseDTO.setDescription("NOT_ENOUGH_GOLD_BALANCE");
                throw new RuntimeException("NOT_ENOUGH_GOLD_BALANCE");
//                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
            }
            else{ //success
                    transactionAcctList.forEach(transactionAcct -> {
                        transactionAcct.setTransStatus(TX_STATUS_COMPLETED);
                        this.dataManager.save(transactionAcct);
                    });
                    String token = authToken.replaceAll("(?i)bearer\\s*", "");
                    String receiverName="";
                    String senderName="";
                    try {
                        receiverName = getName(receiverUsername, token);
                        senderName= getName(senderUsername,token);
                    }catch(Exception ex){
                        logger.error("name not found");
                    }
                    responseDTO.setStatus("G01");
                    responseDTO.setDescription("SUCCESFUL");
                    responseDTO.setMessage(receiverName);
                    DecimalFormat decimalFormat = new DecimalFormat("#,##0.######");
                    String title = "Gold Received";
                    String body ="You have received "+decimalFormat.format(transferAmount.setScale(6,RoundingMode.HALF_UP))+"g from "+ senderName;

                    //Run as async in background
                    notificationService.pushNotificationInternal(receiverUsername,title,body,"",form.getReferenceId(),"NC01","NT033");

                return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
            }

        } catch (Exception ex) {

            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();

            if(transactionAcctList.size() > 0){
                transactionAcctList.forEach(transactionAcct -> {
                    transactionAcct.setTransStatus(TX_STATUS_FAILED);
                    this.dataManager.save(transactionAcct);
                });
                logger.info(transactionAcctList.size()+" transaction with reference: "+ form.getReferenceId() + ", has been set to failed.");
            }

            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(form.getReferenceId());
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F03");
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }


    @PutMapping("walletWithdraw")
    public ResponseEntity walletWithdraw(@RequestBody UpdateTransactionWithdrawForm form) {

        if(form.getReferenceId().equals("") || form.getReferenceId()==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid reference_id");
        }

        logger.info(" Withdrawal called :");
        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        try {
            //load all transaction with given referenceid
            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();


            logger.info("List of transaction: "+transactionAcctList.size());
            if(transactionAcctList.size() == 0){
                logger.error("No transaction was found for referenceId: "+form.getReferenceId());
                throw new RuntimeException("No transaction was found");
            }

            ///// find Investor AccountBgd Id

            AccountBgd investorCashAccount = null;
            BigDecimal withdrawAmount=null;
            AtomicReference<BigDecimal> totalCash= new AtomicReference<>(BigDecimal.ZERO);
            TransactionAcct mainTransaction = null;
            for(TransactionAcct transactionAcct : transactionAcctList){

                if(!transactionAcct.getTransStatus().equals(TX_STATUS_PENDING)){
                    throw new RuntimeException("Invalid ReferenceId , Transaction  is not currently PENDING");
                }
                if(!transactionAcct.getAccount().equals(BURSA_CASH_WALLET)) {
                        totalCash.getAndSet(totalCash.get().add(transactionAcct.getAmount().negate()));
                        if( transactionAcct.getTransType().equals(TX_TYPE_WITHDRAW) ) {
                            investorCashAccount = transactionAcct.getAccount();
                            withdrawAmount = transactionAcct.getAmount().abs();
                            mainTransaction= transactionAcct;
                        }
                }
            }

            logger.info("WALLET ACCOUNT: "+investorCashAccount.getAccOwner());
            logger.info("WithdrawAmount : "+withdrawAmount.toString());


            ////// Get Bank Account No
//            String query2="select i from IntegrationService i " +
//                    "where i.code = :code1 " +
//                    "and i.createdBy = :createdBy1 " +
//                    "order by i.createdDate desc";
//            List<IntegrationService> service = this.dataManager.load(IntegrationService.class)
//                    .query(query2)
//                    .parameter("code1", "ITR01/02")
//                    .parameter("createdBy1", currentAuthentication.getUser().getUsername()).list();

//            ObjectMapper objectMapper = new ObjectMapper();
//            JsonNode jsonNodeResponse = objectMapper.readTree(service.get(0).getResponseBody());
//            JsonNode jsonNodeRequest = objectMapper.readTree(service.get(0).getRequestBody());

            ////// Setup api CT request

//            AmbankCreditTransferForm CTForm = new AmbankCreditTransferForm();
//            CTForm.setCreditorAccountType("DFLT");
//            CTForm.setReceiverBIC(jsonNodeRequest.get("receiverBIC").toString().replace("\"", ""));
//            CTForm.setCreditorAccountNo(jsonNodeRequest.get("creditorAccountNo").toString().replace("\"", "").replace(" ","_"));
//            CTForm.setCreditorAccountName(jsonNodeResponse.get("creditorAccountName").toString().replace("\"", "").replace(" ","_"));
//            CTForm.setAmount(withdrawAmount.setScale(2).toString());
//
//            Date transactionDate=getDateTimeNow();
//            String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(transactionDate);
//            CTForm.setPaymentReference(timestamp+"_"+investorCashAccount.getAcctNumber());
//            CTForm.setPaymentDescription(CTForm.getCreditorAccountNo()+"_"+CTForm.getCreditorAccountName());
//            CTForm.setLookUpReference("");

            WithdrawalInfo withdrawalInfo = dataManager.load(WithdrawalInfo.class)
                    .query("select w from WithdrawalInfo w " +
                            "where w.reference = :reference")
                    .parameter("reference", form.getReferenceId())
                    .one();

            if(withdrawalInfo== null){
                throw new RuntimeException("Withdrawal Info Not Found");
            }

            AmbankCreditTransferForm CTForm = new AmbankCreditTransferForm();
            CTForm.setCreditorAccountType("DFLT");
            CTForm.setReceiverBIC(withdrawalInfo.getRecipientBankBic());
            CTForm.setCreditorAccountNo(withdrawalInfo.getRecipientBankNo().replace(" ",""));
            CTForm.setCreditorAccountName(form.getInvestorFullName().replace(" ","_"));
            CTForm.setAmount(withdrawAmount.setScale(2).toString());

            Date transactionDate=getDateTimeNow();
            String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(transactionDate);
            CTForm.setPaymentReference(timestamp+"_"+investorCashAccount.getAcctNumber());
            CTForm.setPaymentDescription(CTForm.getCreditorAccountNo()+"_"+CTForm.getCreditorAccountName());
            CTForm.setLookUpReference("");

            logger.info("CT API Req Body : ",CTForm.toString());





            logger.info("TOTAL CASH :"+totalCash.get().toString());
            String query3="select a from AccountWalletView a " +
                    "where a.acctNumber = :acctNumber1";
            AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
                    .query(query3)
                    .parameter("acctNumber1", investorCashAccount.getAcctNumber())
                    .one();

            ////// Check balance
            if(totalCash.get().compareTo(walletView.getAvailableAmount())<=0) {

                //Call ambank API
                ResponseEntity<String> responseCT = paymentGatewayAPIController.creditTransferV4(CTForm);

                //check api is success
                if (responseCT.getStatusCode().is2xxSuccessful() && !responseCT.getBody().contains("error") && !responseCT.getBody().contains("reject")) {

                    transactionAcctList.forEach(transactionAcct -> {
                        transactionAcct.setTransStatus(TX_STATUS_COMPLETED);
                        this.dataManager.save(transactionAcct);
                    });



                    responseDTO.setDate(timeSource.now().toLocalDateTime());
                    responseDTO.setReference(form.getReferenceId());
                    responseDTO.setAmount(withdrawAmount);
                    responseDTO.setStatus("G01");
                    responseDTO.setDescription("SUCCESFUL");
                    responseDTO.setTransactionType("F02");

                    return ResponseEntity.status(HttpStatus.OK).body(responseDTO);

                } else {
                    logger.error(responseCT.getBody());
//                    return ResponseEntity.badRequest().body(responseCT.getBody());
                    throw new RuntimeException("AMBANK API RETURN FAILED: "+responseCT.getBody());
                }
            }
            else{
                logger.info("NOT ENOUGH BALANCE , TOTAL WITHDRAWAL :"+totalCash+". WALLET BALANCE: "+walletView.getAvailableAmount().toString());
//                responseDTO.setDate(timeSource.now().toLocalDateTime());
//                responseDTO.setReference(form.getReferenceId());
                responseDTO.setAmount(withdrawAmount);
//                responseDTO.setStatus("G02");
//                responseDTO.setDescription("NOT_ENOUGH_BALANCE");
//                responseDTO.setTransactionType("F02");

                throw new RuntimeException("NOT_ENOUGH_BALANCE");
//                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
            }

        } catch (Exception ex) {

            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();

            if(transactionAcctList.size() > 0){
                transactionAcctList.forEach(transactionAcct -> {
                    transactionAcct.setTransStatus(TX_STATUS_FAILED);
                    this.dataManager.save(transactionAcct);
                });
                logger.info(transactionAcctList.size()+" transaction with reference: "+ form.getReferenceId() + ", has been set to failed.");
            }
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(form.getReferenceId());
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F02");
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }


    @PutMapping("walletWithdrawOld")
    public ResponseEntity walletWithdrawOld(@RequestBody List<WalletForm> form) {

        logger.info(" Withdrawal called :");
//        String reference_id = UUID.randomUUID().toString();
        String reference_id = "F02" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F02", "6");
        try {
            UserDetails userDetails = currentAuthentication.getUser();
            Boolean thirdPartyRespond = false;

            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            for(WalletForm e: form) {

                String query1="select e from AccountBgd e where e.acctNumber = :acctNumber";
                AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
                        .query(query1)
                        .parameter("acctNumber", e.getAcctNumber()).one();

                if(!accountValidator(accountBgd)){
                    logger.error("WITHDRAW: ACCOUNT INVALID");
                    throw new RuntimeException("ACCOUNT_INVALID");
                }

                TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
                transactionAcct.setReferenceId(reference_id);
                transactionAcct.setAccount(accountBgd);
                transactionAcct.setReceiptNumber("RC"+transactionAcct.getReferenceId());
                transactionAcct.setTransMethod(getTransactionMethod(e.getTransactionMethodCode()));
                transactionAcct.setTransType(getTransactionType(e.getTransactionTypeCode()));
                transactionAcct.setTransStatus(getTransactionStatus(e.getStatusCode()));
                transactionAcct.setAmount(BigDecimal.valueOf(Math.abs(e.getTotalAmount().doubleValue())).negate());

                Date transactionDate=getDateTimeNow();

                //call api ambank CT

                try {
                    String query2="select i from IntegrationService i " +
                            "where i.code = :code1 " +
                            "and i.createdBy = :createdBy1 " +
                            "order by i.createdDate desc";
                    List<IntegrationService> service = this.dataManager.load(IntegrationService.class)
                                    .query(query2)
                            .parameter("code1", "ITR01/02")
                            .parameter("createdBy1", currentAuthentication.getUser().getUsername()).list();

                    ObjectMapper objectMapper = new ObjectMapper();
                    JsonNode jsonNodeResponse = objectMapper.readTree(service.get(0).getResponseBody());
                    JsonNode jsonNodeRequest = objectMapper.readTree(service.get(0).getRequestBody());

                    AmbankCreditTransferForm CTForm = new AmbankCreditTransferForm();
                    CTForm.setCreditorAccountType("DFLT");
                    CTForm.setReceiverBIC(jsonNodeRequest.get("receiverBIC").toString().replace("\"", ""));
                    CTForm.setCreditorAccountNo(jsonNodeRequest.get("creditorAccountNo").toString().replace("\"", "").replace(" ","_"));
                    CTForm.setCreditorAccountName(jsonNodeResponse.get("creditorAccountName").toString().replace("\"", "").replace(" ","_"));
                    CTForm.setAmount(e.getTotalAmount().setScale(2).toString()); //negate because from withdraw api req we put - value while CT need + value


                    String timestamp = new SimpleDateFormat("ddMMyyyyHHmmss").format(transactionDate);
                    CTForm.setPaymentReference(timestamp+"_"+e.getAcctNumber());
                    CTForm.setPaymentDescription(CTForm.getCreditorAccountNo()+"_"+CTForm.getCreditorAccountName());
                    CTForm.setLookUpReference("");

                    logger.info("CT API Req Body : ",CTForm.toString());


                    ResponseEntity<String> responseCT = paymentGatewayAPIController.creditTransferV4(CTForm);

                    if (responseCT.getStatusCode().is2xxSuccessful() && !responseCT.getBody().contains("error") && !responseCT.getBody().contains("reject")) {
//                        ObjectMapper objectMapper = new ObjectMapper();
//                        JsonNode root = objectMapper.readTree((JsonParser) responseCT.getBody());

                    } else {
                        logger.error(responseCT.getBody());
                        return ResponseEntity.badRequest().body(responseCT.getBody());
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                    return ResponseEntity.badRequest().body(ex.getMessage());
                }

                AtomicReference<BigDecimal> totalFee = new AtomicReference<>(BigDecimal.valueOf(0));
                //create transaction for fee charge
                List<CreateTransactionDTO> dtoList = new ArrayList<>();
                e.getFeesForms().forEach(f -> {
                    AccountBgd creditAccount = getWallet(accountBgd.getAccOwner(), "E01");
                    if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) > 0) {
                        CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                        dto.setCreditAcct(creditAccount);
                        dto.setDebitAcct(BURSA_CASH_WALLET);
                        dto.setCreditType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setDebitType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setAmount(f.getChargeFees());
                        if(f.getSetupUom().equals("%")){
                            dto.setPercent(f.getSetupFees());
                        }
                        dto.setReference(transactionAcct.getReferenceId());
                        dto.setStatus(TX_STATUS_COMPLETED);
                        dtoList.add(dto);
                        totalFee.getAndSet(totalFee.get().add(f.getChargeFees()));
                        logger.info("CHARGE FEES, "+ dto.getCreditType().getName()+ " Amount: "+dto.getAmount().toString());
                    }
                });

                logger.info("TOTAL FEE WITHDRAWAL :"+totalFee.get().toString());

                String query3="select a from AccountWalletView a " +
                        "where a.acctNumber = :acctNumber1";
                AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
                        .query(query3)
                        .parameter("acctNumber1", e.getAcctNumber())
                        .one();

                //LAST CHECKING TO CHECK BALANCE IS ENOUGH
                if(totalFee.get().add(transactionAcct.getAmount().negate()).compareTo(walletView.getAvailableAmount())<=0) {
                    logger.info("TOTAL WITHDRAWAL :"+totalFee.get().add(transactionAcct.getAmount().negate()).toString()+". WALLET BALANCE: "+walletView.getAvailableAmount().toString());

                    transactionAcct.setCreatedDate(transactionDate);
                    this.dataManager.save(transactionAcct);
                    createTransaction(dtoList);
                }
                else{
                    logger.info("NOT ENOUGH BALANCE , TOTAL WITHDRAWAL :"+totalFee.get().add(transactionAcct.getAmount().negate()).toString()+". WALLET BALANCE: "+walletView.getAvailableAmount().toString());
                    responseDTO.setDate(timeSource.now().toLocalDateTime());
                    responseDTO.setReference(reference_id);
                    responseDTO.setAmount(e.getAmount());
                    responseDTO.setStatus("G02");
                    responseDTO.setDescription("NOT_ENOUGH_BALANCE");
                    responseDTO.setTransactionType("F02");

                    return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
                }

                responseDTO.setDate(timeSource.now().toLocalDateTime());
                responseDTO.setReference(reference_id);
                responseDTO.setAmount(e.getAmount());
                responseDTO.setStatus("G01");
                responseDTO.setDescription("SUCCESFUL");
                responseDTO.setTransactionType("F02");
//            });
            }
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

        } catch (Exception ex) {
            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F02");
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @GetMapping("validateWallet")
    public ResponseEntity validateWallet(String acctNumber) {
        try {
            String query1="select a from AccountBgd a " +
                    "where a.acctNumber = :acctNumber1";
            List<AccountBgd> accountBgds = this.dataManager.load(AccountBgd.class)
                    .query(query1).parameter("acctNumber1", acctNumber).list();

            if(accountBgds.size() > 0) {
                return ResponseEntity.ok("Valid Account Number");
            } else {
                return ResponseEntity.badRequest().body("Account Number Not Valid");
            }

        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }


    @PutMapping("walletTransferOld")
    public ResponseEntity walletTransferOld(@RequestBody List<WalletTransferForm> forms, @RequestHeader(value = "Authorization")String authToken) {
        logger.info("TRANSFER: requested by: "+currentAuthentication.getAuthentication().getName());
        String reference_id = "F03" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F03", "6");
        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        try{
            forms.forEach(e -> {

                String query1="select e from AccountBgd e where e.acctNumber = :acctNumber";
                AccountBgd accountBgdSender = this.dataManager.load(AccountBgd.class)
                        .query(query1)
                        .parameter("acctNumber", e.getAcctNumberSender())
                        .one();

                if(!accountValidator(accountBgdSender)){
                    logger.error("TRANSFER: ACCOUNT INVALID");
                    throw new RuntimeException("ACCOUNT_INVALID");
                }

                if(e.getAcctNumberSender().equals(e.getAcctNumberReceiver())){
                    logger.error("TRANSFER: ACCOUNT RECEIVER SAME AS SENDER");
                    throw new RuntimeException("ACCOUNT_RECEIVER_SAME_AS_SENDER");
                }

                String query2="select e from AccountBgd e where e.acctNumber = :acctNumber";
                AccountBgd accountBgdReceiver = this.dataManager.load(AccountBgd.class)
                        .query(query2)
                        .parameter("acctNumber", e.getAcctNumberReceiver())
                        .one();

                String query3="select e from AccountWalletView e " +
                        "where e.accountOwner = :accountOwner1";
                List<AccountWalletView> walletSenderViews = this.dataManager.load(AccountWalletView.class)
                        .query(query3)
                        .parameter("accountOwner1", accountBgdSender.getAccOwner())
                        .list();
                String receiverUsername=accountBgdReceiver.getAccOwner();
                String senderUsername= accountBgdSender.getAccOwner();
                AccountWalletView goldWalletView = walletSenderViews.stream().filter(f -> f.getCode().equals("E02")).toList().get(0);
                AccountWalletView cashWalletView = walletSenderViews.stream().filter(f -> f.getCode().equals("E01")).toList().get(0);
//                AccountWalletView goldWalletView = walletSenderViews.stream().filter(f -> f.getCode().equals("E02")).collect(Collectors.toList()).get(0);
//                AccountWalletView cashWalletView = walletSenderViews.stream().filter(f -> f.getCode().equals("E01")).collect(Collectors.toList()).get(0);
                logger.info("TRANSFER:  from :" + accountBgdSender.getAcctNumber()+ " OWNER: "+accountBgdSender.getAccOwner());
                logger.info("TRANSFER:  to :" + accountBgdReceiver.getAcctNumber()+ " OWNER: "+accountBgdReceiver.getAccOwner());
                logger.info("TRANSFER: amount to be transfered: "+ e.getAmount());
                BigDecimal totalCashFees = new BigDecimal(0);

                List<CreateTransactionDTO> dtoList = new ArrayList<>();

                 for(FeesForm f : e.getFeesForms()) {
                     if (f.getChargeUomCode().equals("J01")) {
                         AccountBgd creditAccount = getWallet(accountBgdSender.getAccOwner(), "E01");
                         if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) > 0) {
                             CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                             dto.setCreditAcct(creditAccount);
                             dto.setDebitAcct(BURSA_CASH_WALLET);
                             dto.setCreditType(getTxTypeTrade(f.getFeeTypeCode()));
                             dto.setDebitType(getTxTypeTrade(f.getFeeTypeCode()));
                             dto.setAmount(f.getChargeFees());
                             if(f.getSetupUom().equals("%")){
                                 dto.setPercent(f.getSetupFees());
                             }
                             dto.setReference(reference_id);
                             dto.setStatus(TX_STATUS_COMPLETED);
                             dto.setBursaPrice(e.getBursaSellPrice()!=null?e.getBursaSellPrice():null);
                             dtoList.add(dto);
                             logger.info("CHARGE FEES, "+ dto.getCreditType().getName()+ " Amount: "+dto.getAmount().toString());
                         }
                         totalCashFees = totalCashFees.add(f.getChargeFees());
                     }
                 }

                 logger.info("TRANSFER: Total fees: "+totalCashFees);

                 logger.info("TRANSFER: Gold amount to deduct: "+e.getAmount()+ " Wallet Balance: "+goldWalletView.getAvailableAmount());
                 logger.info("TRANSFER: Cash amount to deduct: "+totalCashFees+ " Wallet Balance: "+cashWalletView.getAvailableAmount());

                if(goldWalletView.getAvailableAmount().compareTo(e.getAmount()) < 0 || cashWalletView.getAvailableAmount().compareTo(totalCashFees) < 0 ) {
                    logger.error("TRANSFER: INSUFFICIENT AMOUNT");
                    try {
                        throw new Exception("INSUFFICIENT AMOUNT");
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                }else {
                    try {
                        createTransaction(accountBgdSender, accountBgdReceiver, e.getAmount(), TX_TYPE_TRANSFER, TX_TYPE_TRANSFER, reference_id, getTransactionStatus("G01"), e.getReason(),e.getBursaSellPrice());
                        createTransaction(dtoList);
                        e.setReference(reference_id);

                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                    String token = authToken.replaceAll("(?i)bearer\\s*", "");
                    String receiverName="";
                    String senderName="";
                    try {
                        receiverName = getName(receiverUsername, token);
                        senderName= getName(senderUsername,token);
                    }catch(Exception ex){
                        logger.error("name not found");
                    }
                    responseDTO.setDate(timeSource.now().toLocalDateTime());
                    responseDTO.setReference(reference_id);
                    responseDTO.setAmount(e.getAmount());
                    responseDTO.setStatus("G01");
                    responseDTO.setDescription("SUCCESFUL");
                    responseDTO.setTransactionType("F03");
                    responseDTO.setMessage(receiverName);
                    String title = "Gold Received";
                    String body ="You have received "+e.getAmount().toString()+"g from "+ senderName;

                    //Run as async in background
                    notificationService.pushNotificationInternal(receiverUsername,title,body,"",reference_id,"NC01","NT033");

                    }
            });

            return ResponseEntity.ok(responseDTO);
        }
        catch (Exception ex) {

            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setAmount(null);
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F03");
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @GetMapping("initWalletTransfer")
    public ResponseEntity initWalletTransfer(@RequestBody List<WalletTransferForm> forms) {
        try {
            return ResponseEntity.ok("");
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("walletTransfer")
    public ResponseEntity getWalletTransfer(@RequestBody WalletTransferForm form) {
        try {
            String query1="select e from TransferInfoView e where e.transCode = :transCode";
            List<TransferInfoView> transferInfoList = this.dataManager.load(TransferInfoView.class)
                    .query(query1)
                    .parameter("transCode", "F03")
                    .list();

            List<TransferInfoView> list = null;

            //completed
            switch (form.getStatusCode()) {
                case "-1": //all
                    list = transferInfoList;
                    break;

                case "G01": //completed
                    list = transferInfoList.stream()
                            .filter(f -> f.getStatusCode().equals("G01"))
                            .collect(Collectors.toList());
                    break;

                case "G02": //fail
                    list = transferInfoList.stream()
                            .filter(f -> f.getStatusCode().equals("G02"))
                            .collect(Collectors.toList());
                    break;

                case "G05": //pending
                    list = transferInfoList.stream()
                            .filter(f -> f.getStatusCode().equals("G05"))
                            .collect(Collectors.toList());
                    break;
            }


            return ResponseEntity.ok(list);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }


    @Transactional
    @PutMapping("approvedWalletTransfer")
    public ResponseEntity approvedWalletTransfer(@RequestBody List<TransferInfoView> forms) {
        try {
            forms.forEach(form -> {
                TransferInfo transferInfo = this.dataManager.load(TransferInfo.class).id(form.getId()).one();
                transferInfo.setApprovedBy(currentAuthentication.getUser().getUsername());
//            transferInfo.setApprovedDate(Date.from(Instant.from(LocalDateTime.now())));

                String query1="select e from AccountHold e where e.transactionAcct.referenceId = :referenceId";
                List<AccountHold> accountHold = this.dataManager.load(AccountHold.class)
                        .query(query1)
                        .parameter("referenceId", form.getReference())
                        .list();

                accountHold.forEach(e -> {
                    e.setAmount(BigDecimal.ZERO);

                    TransactionAcct transactionAcct = e.getTransactionAcct();
                    transactionAcct.setTransStatus(getTransactionStatus("G06"));

                    this.dataManager.save(e);
                    this.dataManager.save(transactionAcct);
                });


                this.dataManager.save(transferInfo);
            });

            return ResponseEntity.ok("Approved Successes");
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }


    public AccountBgd getWallet(String username,String typeCode){

        String query1="select a from AccountBgd a " +
                "where a.accOwner = :accOwner " +
                "and a.walletType.code = :walletTypeCode";
        return dataManager.load(AccountBgd.class)
                .query(query1)
                .parameter("accOwner", username)
                .parameter("walletTypeCode", typeCode)
                .one();

    }

    public AccountBgd getAccountBgd(String acctnumber) {
        String query1="select a from AccountBgd a " +
                "where a.acctNumber = :acctNumber1";
        return this.dataManager.load(AccountBgd.class)
                .query(query1)
                .parameter("acctNumber1", acctnumber).one();
    }

    public AccountBgd getWalletTrade(String type){
        UserDetails userDetails = currentAuthentication.getUser();


        AccountBgd result = null;
        switch (type) {
            case "INVESTOR_CASH":
                result = getWallet(userDetails.getUsername(),"E01");
                break;
            case "INVESTOR_ASSET":
                result = getWallet(userDetails.getUsername(),"E02");
                break;
            case "BURSA_CASH":
                result = BURSA_CASH_WALLET;
                break;
            case "BURSA_ASSET":
                result = BURSA_ASSET_WALLET;
                break;
            case "SUPPLIER_CASH":
                result = SUPPLIER_CASH_WALLET;
                break;
            case "SUPPLIER_ASSET":
                result = SUPPLIER_ASSET_WALLET;
                break;
            default:
                break;
        }
        return result;
    }

    public TransactionType getTxTypeTrade(String type){

        TransactionType result = null;
        switch (type) {
            case "F05":
                result = TX_TYPE_BUY;
                break;
            case "F06":
                result = TX_TYPE_SELL;
                break;
            case "F09",  "C04" :
                result = TX_TYPE_FEE;
                break;
            case  "B03":
                result = TX_TYPE_COURIER_FEE;
                break;
            case  "B04":
                result = TX_TYPE_TAKAFUL_FEE;
                break;
            case "B05":
                result = TX_TYPE_MINTING_FEE;
                break;
            case "TAX01":
                result = TX_TYPE_SST;
                break;
            case "TAX02":
                result = TX_TYPE_GST;
                break;
            case "F09/F01":
                result = TX_TYPE_TOPUP_FEE;
                break;
            case "F09/F02":
                result = TX_TYPE_WITHDRAW_FEE;
                break;
            case "F09/F03":
                result = TX_TYPE_TRANSFER_FEE;
                break;
            case "F09/F05": ;
                result = TX_TYPE_BUY_FEE;
                break;
            case "F09/F06":
                result = TX_TYPE_SELL_FEE;
                break;
            default:
                break;
        }
        return result;
    }


    @Transactional
    @PostMapping("/createTransaction")
    public ResponseEntity createTradeTransaction(@RequestBody  List<TransactionDTO> transactionDTOList) {
        String reference_id = UUID.randomUUID().toString();
        AtomicReference<ResponseDTO> responseDTO = new AtomicReference<>(this.dataManager.create(ResponseDTO.class));


        String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
        List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                .query(query1)
                .parameter("referenceId",transactionDTOList.get(0).getReference())
                .list();

        if(transactionAcctList.size()>0){
            //remove any reference id that are pending,
            //this is to solve issue where user click back button after calling this api and front end use the same reference id of query price to call this function again
            //prevent duplicated transaction
            if(transactionAcctList.get(0).getTransStatus().equals(TX_STATUS_PENDING)){
                transactionAcctList.forEach(transactionAcct -> {
                    dataManager.remove(transactionAcct);
                });
            } else{
                // if other than pending, same reference id should not be submit again.
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ReferenceId has been used");
            }
        }



        //To check balance first
//        if(!purchaseBalanceChecker(transactionDTOList)){
//            responseDTO.get().setDate(timeSource.now().toLocalDateTime());
//            responseDTO.get().setReference(reference_id);
//            responseDTO.get().setStatus("G02");
//            responseDTO.get().setDescription("INSUFFICIENT BALANCE");
//            return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO.get());
//        }

        List<CreateTransactionDTO> dtoList = new ArrayList<>();
        for(int i = 0; i < transactionDTOList.size(); i++) {
            CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);

            dto.setCreditAcct(getWalletTrade(transactionDTOList.get(i).getCreditWallet()));
            dto.setDebitAcct(getWalletTrade(transactionDTOList.get(i).getDebitWallet()));
            dto.setAmount(transactionDTOList.get(i).getAmount());
            dto.setCreditType(getTxTypeTrade(transactionDTOList.get(i).getCreditType()));
            dto.setDebitType(getTxTypeTrade(transactionDTOList.get(i).getDebitType()));
            dto.setReference(transactionDTOList.get(i).getReference());
            dto.setStatus(TX_STATUS_PENDING);
            dto.setBursaPrice(transactionDTOList.get(i).getBursaPrice());
            dtoList.add(dto);
            logger.info("Creating transaction with reference id :"+dto.getReference().toString());
            logger.info("Wallet Type: "+dto.getCreditAcct().getWalletType().getName());
            logger.info("Crediting wallet: "+dto.getCreditAcct().getAccOwner().toString());
            logger.info("Debiting wallet: "+dto.getDebitAcct().getAccOwner().toString());
            logger.info("Amount: "+dto.getAmount().toString());
            logger.info("Crediting type: "+dto.getCreditType().getName().toString());
            logger.info("Debiting type: "+dto.getDebitType().getName().toString());
            logger.info(" ");
        }

        responseDTO.set(createTransaction(dtoList));

        if(responseDTO.get().getSuccess()) {
//            setTradeBackTransaction(transactionDTOList.get(1));
            return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
        }
//            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
//            responseDTO.setDate(timeSource.currentTimestamp());
//            responseDTO.setReference(reference_id);
//            responseDTO.setStatus("G01");
//            responseDTO.setDescription("SUCCESFUL");
//            responseDTO.setSuccess(true);
//
//            return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
//        }catch(Exception e){
//            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
//            responseDTO.setDate(timeSource.currentTimestamp());
//            responseDTO.setReference(reference_id);
//            responseDTO.setStatus("G02");
//            responseDTO.setDescription("ERROR : " + e.getMessage());
//            responseDTO.setSuccess(false);
//            return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
//        }
    }

    @PostMapping("/confirmPurchase")
    public ResponseEntity confirmPurchase(@RequestBody UpdateTransactionForm form) {

        if(form.getReferenceId().equals("") || form.getReferenceId()==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid reference_id");
        }

        logger.info(form.toString());
        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        try {

            //load all transaction with given referenceid
            if(form.getReferenceId()==null){
                logger.error("Invalid ReferenceId");
                throw new RuntimeException("Invalid ReferenceId");
            }
            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();

            logger.info("REFERENCCE_ID: "+form.getReferenceId());
            for(TransactionAcct transactionAcct:transactionAcctList) {
               logger.info("ACCOUNT:"+transactionAcct.getAccount().getAccOwner());
               logger.info("STATUS: "+transactionAcct.getTransStatus().getCode());
               logger.info("Amount: "+transactionAcct.getAmount());
               logger.info("referenceID"+transactionAcct.getReferenceId());
            }

            String purchaseActionType =""; // either INVESTOR_BUYING or INVESTOR_SELLING

            BigDecimal totalGram = null;

            AtomicReference<BigDecimal> totalCashInvestorSpent= new AtomicReference<>(BigDecimal.ZERO);
            AtomicReference<BigDecimal> totalAssetInvestorSpent= new AtomicReference<>(BigDecimal.ZERO);

            AtomicReference<BigDecimal> totalCashBursaSpent= new AtomicReference<>(BigDecimal.ZERO);
            AtomicReference<BigDecimal> totalAssetBursaSpent= new AtomicReference<>(BigDecimal.ZERO);

            AccountBgd investorAssetWallet = getWalletTrade("INVESTOR_ASSET");
            AccountBgd investorCashWallet = getWalletTrade("INVESTOR_CASH");


            //Check size is not zero
            if(transactionAcctList.size() == 0){
                logger.error("No transaction was found for referenceId: "+form.getReferenceId());
                throw new RuntimeException("No transaction was found");
            }

            for(TransactionAcct transactionAcct:transactionAcctList){

                logger.info("DEBUG : TRANS STATUS"+transactionAcct.getTransStatus().getName());
                //check status is still pending
                if(!transactionAcct.getTransStatus().equals(TX_STATUS_PENDING)){
                    logger.error("Invalid ReferenceId , Transaction  is not currently PENDING");
                    throw new RuntimeException("Invalid ReferenceId , Transaction  is not currently PENDING");
                }

                //calculate all netspent
                if(transactionAcct.getAccount().equals(BURSA_ASSET_WALLET)){
                    totalAssetBursaSpent.getAndSet(totalAssetBursaSpent.get().add(transactionAcct.getAmount()));
                }
                else if(transactionAcct.getAccount().equals(BURSA_CASH_WALLET)){
                    totalCashBursaSpent.getAndSet(totalCashBursaSpent.get().add(transactionAcct.getAmount()));
                }
                else if(transactionAcct.getAccount().equals(investorAssetWallet)){
                    totalAssetInvestorSpent.getAndSet(totalAssetInvestorSpent.get().add(transactionAcct.getAmount()));
                }
                else if(transactionAcct.getAccount().equals(investorCashWallet)){
                    totalCashInvestorSpent.getAndSet(totalCashInvestorSpent.get().add(transactionAcct.getAmount()));
                }

                if(transactionAcct.getAccount().equals(BURSA_ASSET_WALLET) && transactionAcct.getTransType().equals(TX_TYPE_BUY)){
                    //means bursa buying from investor // INVESTOR SELLING
                    purchaseActionType="INVESTOR_SELLING";
//                    createTransaction(getWalletTrade("BURSA_CASH"),getWalletTrade("SUPPLIER_CASH"),totalPrice,getTxTypeTrade("F05"),getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_PENDING, "");
//                    createTransaction(getWalletTrade("SUPPLIER_ASSET"),getWalletTrade("BURSA_ASSET"),totalPrice,getTxTypeTrade("F06"),getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_PENDING, "");
                    totalGram=transactionAcct.getAmount().abs();
                }
                else if(transactionAcct.getAccount().equals(BURSA_ASSET_WALLET) && transactionAcct.getTransType().equals(TX_TYPE_SELL)){
                    //means bursa selling to investor // INVESTOR BUYING
                    purchaseActionType="INVESTOR_BUYING";
//                    createTransaction(getWalletTrade("SUPPLIER_CASH"),getWalletTrade("BURSA_CASH"),totalPrice,getTxTypeTrade("F05"),getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_PENDING, "");
//                    createTransaction(getWalletTrade("BURSA_ASSET"),getWalletTrade("SUPPLIER_ASSET"),totalPrice,getTxTypeTrade("F06"),getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_PENDING, "");
                    totalGram=transactionAcct.getAmount().abs();
                }

            }

            //CHECK ACCOUNT BALANCE SUFFICIENT
            //totalspent was negate because in db we stored -ve for deduction, should compare wallet balance and amount to be deducted
            if(walletBalanceCheck(BURSA_ASSET_WALLET,totalAssetBursaSpent.get().negate()) &&
                    walletBalanceCheck(BURSA_CASH_WALLET,totalCashBursaSpent.get().negate()) &&
                    walletBalanceCheck(investorAssetWallet,totalAssetInvestorSpent.get().negate()) &&
                    walletBalanceCheck(investorCashWallet,totalCashInvestorSpent.get().negate())){
                //PASS
                transactionAcctList.forEach(transactionAcct -> {
                    transactionAcct.setTransStatus(TX_STATUS_COMPLETED);
                    dataManager.save(transactionAcct);
                });

            }else{
                logger.error("BALANCE_INSUFFICENT");
                transactionAcctList.forEach(transactionAcct -> {
                    transactionAcct.setTransStatus(TX_STATUS_FAILED);
                    dataManager.save(transactionAcct);
                });
                throw new RuntimeException("BALANCE_INSUFFICENT");
            }

            //////////////////////////////// TRADE BACK ////////////////////////////////////

            logger.info("Creating TradeBack Transaction for reference id : "+form.getReferenceId());

//            String query2="select i from IntegrationService i " +
//                    "where (i.code = :code1 or i.code = :code2) " +
//                    "and i.responseBody like CONCAT('%', :responseBody1, '%')";
//            IntegrationService integrationService = this.dataManager.load(IntegrationService.class)
//                    .query(query2)
//                    .parameter("code1", "ITR03/01")
//                    .parameter("code2", "ITR03/02")
//                    .parameter("responseBody1", form.getPriceRequestID())
//                    .one();
//            if(integrationService== null) {
//                logger.error("No result when fetching the response data of price data for price id in integration service table");
//                logger.error("Price ID: " +form.getPriceRequestID());
//            }
//
            ObjectMapper objectMapper = new ObjectMapper();
//            JsonNode jsonNode = objectMapper.readTree(integrationService.getResponseBody());
//
//            BigDecimal acePrice = new BigDecimal(jsonNode.get("total_price").toString().replace("\"", ""));

            PriceQuery priceQuery = dataManager.load(PriceQuery.class)
                    .query("select p from PriceQuery p " +
                            "where p.reference = :reference")
                    .parameter("reference", form.getReferenceId() )
                    .one();

            BigDecimal acePrice = null;
            String priceRequestId = null;
            if(priceQuery.getSupplierPrice()==null || priceQuery.getPriceRequestID()==null){
                createTransaction(getWalletTrade("SUPPLIER_CASH"),getWalletTrade("BURSA_CASH"),acePrice.multiply(totalGram),getTxTypeTrade("F05"),getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_FAILED, "SUPPLIER PRICE NOT FOUND",acePrice);
                createTransaction(getWalletTrade("BURSA_ASSET"),getWalletTrade("SUPPLIER_ASSET"),totalGram,getTxTypeTrade("F06"),getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_FAILED, "SUPPLIER PRICE NOT FOUND",acePrice);

            }
            else {
                acePrice = priceQuery.getSupplierPrice();
                priceRequestId = priceQuery.getPriceRequestID();

                //call
                ResponseEntity<String> response = null;
                if (purchaseActionType.equals("INVESTOR_BUYING")) { //investor buy from bursa

                    //call api spotOrderSell to buy back from ace  ( ACE API: to buy from them, call spotsell meaning ace selling)
                    logger.info("Calling spotsell order api to buy back from Supplier.. ");
                    response = supplierGoldAPIController.sportOrderSell(priceRequestId, acePrice, totalGram, form.getReferenceId());

                    logger.info("Response Spot Sell Order From Supplier : " + response.getBody().toString());

                    if (response.getStatusCode().is2xxSuccessful() && !response.getBody().contains("error")) {

                        JsonNode sellResponse = objectMapper.readTree(response.getBody().toString());

                        String totalWeightResponse = sellResponse.get("weight").asText();
                        String totalPriceResponse = sellResponse.get("amount").asText();

                        BigDecimal totalWeight = new BigDecimal(totalWeightResponse);
                        BigDecimal totalPrice = new BigDecimal(totalPriceResponse);

                        logger.info("Total weight response from acespot sell order : " + totalWeight);
                        logger.info("Total price response from acespot sell order : " + totalPrice);

                        logger.info("TOTAL MISMATCH BETWEEN ORIGINAL TRANSACTION AND BUYBACK TRANSACTION : " + totalGram.subtract(totalWeight));

                        //check both gold and cash wallet of supplier is sufficient to proceed to buyBack
                        if (walletBalanceCheck("BURSA_CASH", totalPrice) && walletBalanceCheck("SUPPLIER_ASSET", totalWeight)) {

                            createTransaction(getWalletTrade("BURSA_CASH"), getWalletTrade("SUPPLIER_CASH"), totalPrice, getTxTypeTrade("F05"), getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_COMPLETED, "", acePrice);
                            createTransaction(getWalletTrade("SUPPLIER_ASSET"), getWalletTrade("BURSA_ASSET"), totalWeight, getTxTypeTrade("F06"), getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_COMPLETED, "", acePrice);

                        } else {
                            logger.error("CANT PROCEED TO BUY BACK. CAUSE: SUPPLER ASSET NOT SUFFICIENT OR BURSA CASH NOT SUFFICNET");
                            createTransaction(getWalletTrade("BURSA_CASH"), getWalletTrade("SUPPLIER_CASH"), totalPrice, getTxTypeTrade("F05"), getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_FAILED, "INSUFFICIENT SUPPLIER/BURSA CASH/ASSET BALANCE", acePrice);
                            createTransaction(getWalletTrade("SUPPLIER_ASSET"), getWalletTrade("BURSA_ASSET"), totalWeight, getTxTypeTrade("F06"), getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_FAILED, "INSUFFICIENT SUPPLIER/BURSA CASH/ASSET BALANCE", acePrice);
                        }

                    } else {
                        logger.error("spotOrderBuy: " + response.getBody());
                        createTransaction(getWalletTrade("BURSA_CASH"), getWalletTrade("SUPPLIER_CASH"), acePrice.multiply(totalGram), getTxTypeTrade("F05"), getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_FAILED, response.getBody(), acePrice);
                        createTransaction(getWalletTrade("SUPPLIER_ASSET"), getWalletTrade("BURSA_ASSET"), totalGram, getTxTypeTrade("F06"), getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_FAILED, response.getBody(), acePrice);
                    }
                } else if (purchaseActionType.equals("INVESTOR_SELLING")) { //investor sell to bursa

                    //call api spotOrderBuy to sell back to ace ( ACE API: to sell to them, call spotbuy meaning ace buying)
                    logger.info("Calling spotbuy order api to buy back from supplier.. ");
                    response = supplierGoldAPIController.sportOrderBuy(priceRequestId, acePrice, totalGram, form.getReferenceId());

                    logger.info("Response Spot Buy Order From Supplier : " + response.getBody().toString());
                    if (response.getStatusCode().is2xxSuccessful() && !response.getBody().contains("error")) {

                        JsonNode buyResponse = objectMapper.readTree(response.getBody().toString());

                        String totalWeightResponse = buyResponse.get("weight").asText();
                        String totalPriceResponse = buyResponse.get("amount").asText();

                        BigDecimal totalWeight = new BigDecimal(totalWeightResponse);
                        BigDecimal totalPrice = new BigDecimal(totalPriceResponse);

                        logger.info("Total weight response from acespot buy order : " + totalWeight);
                        logger.info("Total price response from acespot buy order : " + totalPrice);

                        logger.info("TOTAL MISMATCH BETWEEN ORIGINAL TRANSACTION AND BUYBACK TRANSACTION : " + totalGram.subtract(totalWeight));
                        //check both gold and cash wallet of supplier is sufficient to proceed to sell Back
                        if (walletBalanceCheck("SUPPLIER_CASH", totalPrice) && walletBalanceCheck("BURSA_ASSET", totalWeight)) {

                            createTransaction(getWalletTrade("SUPPLIER_CASH"), getWalletTrade("BURSA_CASH"), totalPrice, getTxTypeTrade("F05"), getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_COMPLETED, "", acePrice);
                            createTransaction(getWalletTrade("BURSA_ASSET"), getWalletTrade("SUPPLIER_ASSET"), totalWeight, getTxTypeTrade("F06"), getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_COMPLETED, "", acePrice);

                        } else {
                            logger.error("CANT PROCEED TO SELL BACK. CAUSE: SUPPLER CASH BALANCE NOT SUFFICIENT OR BURSA ASSET NOT SUFFICIENT");

                            createTransaction(getWalletTrade("SUPPLIER_CASH"), getWalletTrade("BURSA_CASH"), totalPrice, getTxTypeTrade("F05"), getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_FAILED, "INSUFFICIENT SUPPLIER/BURSA CASH/ASSET BALANCE", acePrice);
                            createTransaction(getWalletTrade("BURSA_ASSET"), getWalletTrade("SUPPLIER_ASSET"), totalWeight, getTxTypeTrade("F06"), getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_FAILED, "INSUFFICIENT SUPPLIER/BURSA CASH/ASSET BALANCE", acePrice);

                        }
                    } else {
                        logger.error("spotOrderSell: " + response.getBody());

                        createTransaction(getWalletTrade("SUPPLIER_CASH"), getWalletTrade("BURSA_CASH"), acePrice.multiply(totalGram), getTxTypeTrade("F05"), getTxTypeTrade("F06"), form.getReferenceId(), TX_STATUS_FAILED, response.getBody(), acePrice);
                        createTransaction(getWalletTrade("BURSA_ASSET"), getWalletTrade("SUPPLIER_ASSET"), totalGram, getTxTypeTrade("F06"), getTxTypeTrade("F05"), form.getReferenceId(), TX_STATUS_FAILED, response.getBody(), acePrice);

                    }
                }
            }

            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(form.getReferenceId());
            responseDTO.setAmount(totalGram);
            responseDTO.setStatus("G01");
            responseDTO.setDescription("SUCCESS");
            responseDTO.setTransactionType(null);
            DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
            responseDTO.setMessage(decimalFormat.format(totalCashInvestorSpent.get().abs().setScale(2,RoundingMode.HALF_UP)));
            return ResponseEntity.ok().body(responseDTO);

        } catch (Exception ex) {
            logger.error("Error updateTransaction: " +ex.getMessage());
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(form.getReferenceId());
            responseDTO.setAmount(null);
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType(null);
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @Transactional
    @PostMapping("/createTransactionOld")
    public ResponseEntity createTradeTransactionOld(@RequestBody  List<TransactionDTO> transactionDTOList) {

            AtomicReference<ResponseDTO> responseDTO = new AtomicReference<>(this.dataManager.create(ResponseDTO.class));

        //To check balance first
        if(!purchaseBalanceChecker(transactionDTOList)){
            responseDTO.get().setDate(timeSource.now().toLocalDateTime());
            responseDTO.get().setReference(transactionDTOList.get(0).getReference()!=null? transactionDTOList.get(0).getReference():"no_reference");
            responseDTO.get().setStatus("G02");
            responseDTO.get().setDescription("INSUFFICIENT BALANCE");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO.get());
        }

        List<CreateTransactionDTO> dtoList = new ArrayList<>();
            for(int i = 0; i < transactionDTOList.size(); i++) {
                CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                dto.setCreditAcct(getWalletTrade(transactionDTOList.get(i).getCreditWallet()));
                dto.setDebitAcct(getWalletTrade(transactionDTOList.get(i).getDebitWallet()));
                dto.setAmount(transactionDTOList.get(i).getAmount());
                dto.setCreditType(getTxTypeTrade(transactionDTOList.get(i).getCreditType()));
                dto.setDebitType(getTxTypeTrade(transactionDTOList.get(i).getDebitType()));
                dto.setReference(transactionDTOList.get(i).getReference());
                dto.setStatus(TX_STATUS_COMPLETED);
                dtoList.add(dto);
                logger.info("Creating transaction with reference id :"+dto.getReference().toString());
                logger.info("Wallet Type: "+dto.getCreditAcct().getWalletType().getName());
                logger.info("Crediting wallet: "+dto.getCreditAcct().getAccOwner().toString());
                logger.info("Debiting wallet: "+dto.getDebitAcct().getAccOwner().toString());
                logger.info("Amount: "+dto.getAmount().toString());
                logger.info("Crediting type: "+dto.getCreditType().getName().toString());
                logger.info("Debiting type: "+dto.getDebitType().getName().toString());
                logger.info(" ");
            }

            responseDTO.set(createTransaction(dtoList));

            if(responseDTO.get().getSuccess()) {
                setTradeBackTransaction(transactionDTOList.get(1));
                return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
            } else {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
            }
//            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
//            responseDTO.setDate(timeSource.currentTimestamp());
//            responseDTO.setReference(reference_id);
//            responseDTO.setStatus("G01");
//            responseDTO.setDescription("SUCCESFUL");
//            responseDTO.setSuccess(true);
//
//            return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
//        }catch(Exception e){
//            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
//            responseDTO.setDate(timeSource.currentTimestamp());
//            responseDTO.setReference(reference_id);
//            responseDTO.setStatus("G02");
//            responseDTO.setDescription("ERROR : " + e.getMessage());
//            responseDTO.setSuccess(false);
//            return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
//        }
    }


    public Boolean purchaseBalanceChecker(List<TransactionDTO> transactionDTOList){
        BigDecimal invCashCreditTotal = new BigDecimal(0);
        BigDecimal invAssetCreditTotal = new BigDecimal(0);
        BigDecimal bursCashCreditTotal = new BigDecimal(0);
        BigDecimal bursAssetCreditTotal = new BigDecimal(0);

        Boolean sellingIndicator = false;

        for (TransactionDTO i:transactionDTOList) {
            if(i.getCreditWallet().equals("INVESTOR_ASSET")){
                invAssetCreditTotal=invAssetCreditTotal.add(i.getAmount());
            }
            else if(i.getCreditWallet().equals("INVESTOR_CASH")){
                invCashCreditTotal=invCashCreditTotal.add(i.getAmount());
            }
            else if(i.getCreditWallet().equals("BURSA_ASSET")){
                bursAssetCreditTotal=bursAssetCreditTotal.add(i.getAmount());
            }
            else if (i.getCreditWallet().equals("BURSA_CASH")){
                bursCashCreditTotal=bursCashCreditTotal.add(i.getAmount());
            }
            //if inv sell we want to exclude the system to check for cash balance because the fee will be deducted from the total rm that investor get after selling
            if(i.getCreditWallet().equals("INVESTOR_ASSET") && i.getDebitWallet().equals("BURSA_ASSET") && i.getCreditType().equals("F06")){ //means if selling
                sellingIndicator=true;
            }
        }
        if(sellingIndicator){
            invCashCreditTotal=BigDecimal.valueOf(0); // set inv cash fees need to pay to 0 because the amount will be deducted from what the inv get after purcahse later
        }

        if(walletBalanceCheck("INVESTOR_ASSET",invAssetCreditTotal) &&
                walletBalanceCheck("INVESTOR_CASH",invCashCreditTotal) &&
                walletBalanceCheck("BURSA_ASSET",bursAssetCreditTotal) &&
                walletBalanceCheck("BURSA_CASH",bursCashCreditTotal)){
            logger.info("ALL WALLET BALANCE SUFFICIENT");
            return true;
        }
        else {
            logger.error("WALLET BALANCE INSUFFICIENT");
            return false;
        }

    }

    public TransactionDTO newTransactionDTO(String reference, String priceRequestID, String creditWallet, String debitWallet, BigDecimal amount, String creditType , String debitType){
        TransactionDTO newT = dataManager.create(TransactionDTO.class);
        newT.setReference(reference);
        newT.setPriceRequestID(priceRequestID);
        newT.setCreditWallet(creditWallet);
        newT.setDebitWallet(debitWallet);
        newT.setAmount(amount);
        newT.setCreditType(creditType);
        newT.setDebitType(debitType);

        return newT;
    }

    //not used
    public void setTradeBackTransaction(TransactionDTO transactionDTO) {
        try {

            logger.info("Creating TradeBack Transaction for reference id : "+transactionDTO.getReference().toString());

            String query1="select i from IntegrationService i " +
                    "where (i.code = :code1 or i.code = :code2) " +
                    "and i.responseBody like CONCAT('%', :responseBody1, '%')";
            IntegrationService integrationService = this.dataManager.load(IntegrationService.class)
                    .query(query1)
                    .parameter("code1", "ITR03/01")
                    .parameter("code2", "ITR03/02")
                    .parameter("responseBody1", transactionDTO.getPriceRequestID())
                    .one();
            if(integrationService== null) {
                logger.error("No result when fetching the response data of price data for price id in integration service table");
                logger.error("Price ID: " +transactionDTO.getPriceRequestID());
            }

            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(integrationService.getResponseBody());

            BigDecimal acePrice = new BigDecimal(jsonNode.get("total_price").toString().replace("\"", ""));
            //call
            ResponseEntity<String> response = null;
            if(transactionDTO.getCreditWallet().equals("BURSA_ASSET")) { //investor buy from bursa

                //call api spotOrderSell to buy back from ace  ( ACE API: to buy from them, call spotsell meaning ace selling)
                logger.info("Calling spotsell order api to buy back from Supplier.. ");
                response = supplierGoldAPIController.sportOrderSell(transactionDTO.getPriceRequestID(), acePrice, transactionDTO.getAmount(), transactionDTO.getReference());

                logger.info("Response Spot Sell Order From Supplier : "+ response.getBody().toString());

                if(response.getStatusCode().is2xxSuccessful() && !response.getBody().contains("error")) {

                    JsonNode sellResponse = objectMapper.readTree(response.getBody().toString());

                    String totalWeightResponse = sellResponse.get("weight").asText();
                    String totalPriceResponse = sellResponse.get("amount").asText();

                    BigDecimal totalWeight = new BigDecimal(totalWeightResponse);
                    BigDecimal totalPrice = new BigDecimal(totalPriceResponse);

                    logger.info("Total weight response from acespot sell order : " + totalWeight);
                    logger.info("Total price response from acespot sell order : "+totalPrice);

                    logger.info("TOTAL MISMATCH BETWEEN ORIGINAL TRANSACTION AND BUYBACK TRANSACTION : "+transactionDTO.getAmount().subtract(totalWeight));

                    //check both gold and cash wallet of supplier is sufficient to proceed to tradeBack
                    if(walletBalanceCheck("SUPPLIER_CASH",totalPrice) && walletBalanceCheck("SUPPLIER_ASSET",totalWeight)) {
                        //createTradeBackTransaction
                        List<TransactionDTO> transactionDTOS= new ArrayList<>();
                        transactionDTOS.add(newTransactionDTO(transactionDTO.getReference(), transactionDTO.getPriceRequestID(), "BURSA_CASH", "SUPPLIER_CASH", totalPrice, "F05", "F06"));
                        transactionDTOS.add(newTransactionDTO(transactionDTO.getReference(), transactionDTO.getPriceRequestID(), "SUPPLIER_ASSET", "BURSA_ASSET", totalWeight, "F06", "F05"));
                        createTradeBackTransaction(transactionDTOS);
                    }
                    else{
                        logger.error("CANT PROCEED TO BUY BACK. CAUSE: SUPPLER BALANCE NOT SUFFICIENT ");
                    }

                } else {
                    logger.error("spotOrderBuy: " +response.getBody());
                }
            } else if (transactionDTO.getDebitWallet().equals("BURSA_ASSET")) { //investor sell to bursa

                //call api spotOrderBuy to sell back to ace ( ACE API: to sell to them, call spotbuy meaning ace buying)
                logger.info("Calling spotbuy order api to buy back from supplier.. ");
                response = supplierGoldAPIController.sportOrderBuy(transactionDTO.getPriceRequestID(), acePrice, transactionDTO.getAmount(), transactionDTO.getReference());

                logger.info("Response Spot Buy Order From Supplier : "+ response.getBody().toString());
                if(response.getStatusCode().is2xxSuccessful() && !response.getBody().contains("error")) {

                    JsonNode buyResponse = objectMapper.readTree(response.getBody().toString());

                    String totalWeightResponse = buyResponse.get("weight").asText();
                    String totalPriceResponse = buyResponse.get("amount").asText();

                    BigDecimal totalWeight = new BigDecimal(totalWeightResponse);
                    BigDecimal totalPrice = new BigDecimal(totalPriceResponse);

                    logger.info("Total weight response from acespot buy order : " + totalWeight);
                    logger.info("Total price response from acespot buy order : "+totalPrice);

                    logger.info("TOTAL MISMATCH BETWEEN ORIGINAL TRANSACTION AND BUYBACK TRANSACTION : "+transactionDTO.getAmount().subtract(totalWeight));
                    //check both gold and cash wallet of supplier is sufficient to proceed to tradeBack
                    if(walletBalanceCheck("SUPPLIER_CASH",totalPrice) && walletBalanceCheck("SUPPLIER_ASSET",totalWeight)) {
                        //createTradeBackTransaction
                        List<TransactionDTO> transactionDTOS = new ArrayList<>();

                        transactionDTOS.add(newTransactionDTO(transactionDTO.getReference(), transactionDTO.getPriceRequestID(), "SUPPLIER_CASH", "BURSA_CASH", totalPrice, "F05", "F06"));
                        transactionDTOS.add(newTransactionDTO(transactionDTO.getReference(), transactionDTO.getPriceRequestID(), "BURSA_ASSET", "SUPPLIER_ASSET", totalWeight, "F06", "F05"));

                        createTradeBackTransaction(transactionDTOS);
                    }
                    else{
                        logger.error("CANT PROCEED TO SELL BACK. CAUSE: SUPPLER BALANCE NOT SUFFICIENT ");
                    }
                }else {
                    logger.error("spotOrderSell: " +response.getBody());
                }
            }

        } catch (Exception ex) {
            logger.error("Error setTradeBackTransaction: " +ex.getMessage());
        }
    }

    public Boolean walletBalanceCheck(String wallet , BigDecimal amount){
        String query1="select a from AccountWalletView a " +
                "where a.acctNumber = :acctNumber1";
        AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
                .query(query1)
                .parameter("acctNumber1", getWalletTrade(wallet).getAcctNumber()).one();
        logger.info("WALLET BALANCE CHECKING");
        logger.info("Wallet: Owner "+walletView.getAccountOwner()+" .Type: "+walletView.getType()+" .Number: "+walletView.getAcctNumber()+" .Balance: "+walletView.getAvailableAmount().toString() +" Amount to compare: "+amount.toString());
        logger.info(" ");
        return walletView.getAvailableAmount().compareTo(amount) >=0;
    }
    public Boolean walletBalanceCheck(AccountBgd account , BigDecimal amount){
        String query1="select a from AccountWalletView a " +
                "where a.acctNumber = :acctNumber1";
        AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
                .query(query1)
                .parameter("acctNumber1", account.getAcctNumber()).one();
        logger.info("WALLET BALANCE CHECKING");
        logger.info("Wallet: Owner "+walletView.getAccountOwner()+" .Type: "+walletView.getType()+" .Number: "+walletView.getAcctNumber()+" .Balance: "+walletView.getAvailableAmount().toString() +" Amount to compare: "+amount.toString());
        logger.info(" ");
        return walletView.getAvailableAmount().compareTo(amount) >=0;
    }

    @PostMapping("/createTradeBackTransaction")
    public ResponseEntity createTradeBackTransaction(@RequestBody  List<TransactionDTO> transactionDTOList){
        String reference_id = UUID.randomUUID().toString();
        if(transactionDTOList.get(0).getReference() != null) {
            reference_id = transactionDTOList.get(0).getReference();
        }

        try {
            String finalReference_id = reference_id;
            transactionDTOList.stream().forEach(transactionDTO -> {
                try {
                    createTransaction(getWalletTrade(transactionDTO.getCreditWallet()),getWalletTrade(transactionDTO.getDebitWallet()),transactionDTO.getAmount(),getTxTypeTrade(transactionDTO.getCreditType()),getTxTypeTrade(transactionDTO.getDebitType()), finalReference_id, TX_STATUS_COMPLETED, "",null);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });

            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setStatus("G01");
            responseDTO.setDescription("SUCCESFUL");

            return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
        }catch(Exception e){
            ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(reference_id);
            responseDTO.setStatus("G02");
            responseDTO.setDescription("ERROR : " + e.getMessage());
            return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
        }
    }


    @Transactional
    public ResponseDTO createTransaction(List<CreateTransactionDTO> list) {
        ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);

        List<TransactionAcct> newCreditTxs = new ArrayList<>();
        List<TransactionAcct> newDebitTxs = new ArrayList<>();
        List<TransferInfo> newTransfers = new ArrayList<>();
        List<AccountHold> holdCreditTxs = new ArrayList<>();
        List<AccountHold> holdDebitTxs = new ArrayList<>();

        Boolean txSetupComplete= true;

//        try { // because transactional should not have try catch to work
            for (int i = 0; i < list.size(); i++) {

                // not used
//                String query1="select a from AccountWalletView a " +
//                        "where a.acctNumber = :acctNumber1";
//                AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
//                        .query(query1)
//                        .parameter("acctNumber1", list.get(i).getCreditAcct().getAcctNumber()).one();


                TransactionAcct newCreditTx = dataManager.create(TransactionAcct.class);
                newCreditTx.setAccount(list.get(i).getCreditAcct());
                newCreditTx.setAmount(list.get(i).getAmount().negate());
                newCreditTx.setPercent(list.get(i).getPercent());
                newCreditTx.setTransType(list.get(i).getCreditType());
                newCreditTx.setTransStatus(list.get(i).getStatus());
                newCreditTx.setReferenceId(list.get(i).getReference());
                newCreditTx.setReceiptNumber("RC"+newCreditTx.getReferenceId());
                newCreditTx.setPrice(list.get(i).getBursaPrice()!=null?list.get(i).getBursaPrice():null);
                newCreditTxs.add(newCreditTx);


                TransactionAcct newDebitTx = dataManager.create(TransactionAcct.class);
                newDebitTx.setAccount(list.get(i).getDebitAcct());
                newDebitTx.setAmount(list.get(i).getAmount());
                newDebitTx.setPercent(list.get(i).getPercent());
                newDebitTx.setTransType(list.get(i).getDebitType());
                newDebitTx.setTransStatus(list.get(i).getStatus());
                newDebitTx.setReferenceId(list.get(i).getReference());
                newDebitTx.setReceiptNumber("RC"+newDebitTx.getReferenceId());
                newDebitTx.setPrice(list.get(i).getBursaPrice()!=null?list.get(i).getBursaPrice():null);
                newDebitTxs.add(newDebitTx);

                TransferInfo newTransfer = dataManager.create(TransferInfo.class);
                newTransfer.setTransAcct(newCreditTx);
                newTransfer.setToAcct(list.get(i).getDebitAcct());
                newTransfers.add(newTransfer);


//                if (list.get(i).getStatus().equals(TX_STATUS_PENDING)) { // if status pending, set as hold amount
//                    AccountHold holdCreditTx = this.dataManager.create(AccountHold.class);
//                    AccountHold holdDebitTx = this.dataManager.create(AccountHold.class);
//
//                    holdCreditTx.setAccountBgd(list.get(i).getCreditAcct());
//                    holdCreditTx.setTransactionAcct(newCreditTx);
//                    holdCreditTx.setAmount(list.get(i).getAmount());
//                    holdCreditTxs.add(holdCreditTx);
//
//                    holdDebitTx.setAccountBgd(list.get(i).getDebitAcct());
//                    holdDebitTx.setTransactionAcct(newDebitTx);
//                    holdDebitTx.setAmount(list.get(i).getAmount());
//                    holdDebitTxs.add(holdDebitTx);
//
//
//                }
                responseDTO.setDate(timeSource.now().toLocalDateTime());
                responseDTO.setReference(list.get(i).getReference());
                responseDTO.setStatus("G01");
                responseDTO.setSuccess(true);
                responseDTO.setDescription("SUCCESS");
            }

            if(txSetupComplete) {

                newCreditTxs.forEach(e -> {
                    dataManager.save(e);
                });
                newDebitTxs.forEach(e -> {
                    dataManager.save(e);
                });
                newTransfers.forEach(e -> {
                    dataManager.save(e);
                });
                holdCreditTxs.forEach(e -> {
                    dataManager.save(e);
                });
                holdDebitTxs.forEach(e -> {
                    dataManager.save(e);
                });
            }

//        } catch (Exception ex) {
//            responseDTO.setDate(timeSource.currentTimestamp());
//            responseDTO.setReference(list.get(0).getReference());
//            responseDTO.setStatus("G02");
//            responseDTO.setSuccess(false);
//            responseDTO.setDescription("ERROR: Amount Exceed Limit");
//        }


        return responseDTO;
    }

    public ResponseDTO createTransaction(AccountBgd creditAcct, AccountBgd debitAcct, BigDecimal amount, TransactionType creditType , TransactionType debitType ,String referenceID, TransStatus status, String reason,BigDecimal price) {
        //check available amount on creditAcct
//        try {
        ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);

        logger.info("Creating transaction with reference id :"+referenceID);
        logger.info("Crediting wallet: "+creditAcct.getAccOwner().toString()+ creditAcct.getWalletType().getName());
        logger.info("Debiting wallet: "+debitAcct.getAccOwner().toString()+creditAcct.getWalletType().getName());
        logger.info("Amount : "+amount.toString());
        logger.info("Crediting type: "+creditType.getName().toString());
        logger.info("Debiting type: "+debitType.getName().toString());
        logger.info(" ");

        //Comment Out because wallet Checking has been handled before calling this function considering that all row of transaction that need to be created need to be check together before proceeding to this function
        //perlu check semua row of transaction sekali gus
        //this function only create one row of transaction, hence not suitable to check balance here.
        //hence, the checking has been handled before calling this function
        //eliminating issues where gold transaction succeed , tx created, but cash transaction failed, tx not created and vice versa
//        if (walletView.getAvailableAmount().compareTo(amount) < 0) {
//            logger.info("CREATE TRANSACTION FAILED BECAUSE OF NOT ENOUGH AVAILABLE AMOUNT");
//            logger.info("AVAILABLE WALLET BALANCE: "+walletView.getAvailableAmount().toString());
//            logger.info("AMOUNT: "+ amount.toString());
//            responseDTO.setDate(timeSource.now().toLocalDateTime());
//            responseDTO.setReference(referenceID);
//            responseDTO.setStatus("G02");
//            responseDTO.setSuccess(false);
//            responseDTO.setDescription("2ERROR: Amount Exceed Limit");
//            return responseDTO;
//        }

        TransactionAcct newCreditTx = dataManager.create(TransactionAcct.class);
        newCreditTx.setAccount(creditAcct);
        newCreditTx.setAmount(amount.negate());
        newCreditTx.setTransType(creditType);
        newCreditTx.setTransStatus(status);
        newCreditTx.setReferenceId(referenceID);
        newCreditTx.setReceiptNumber("RC"+newCreditTx.getReferenceId());
        newCreditTx.setPrice(price!=null?price:null);


        TransactionAcct newDebitTx = dataManager.create(TransactionAcct.class);
        newDebitTx.setAccount(debitAcct);
        newDebitTx.setAmount(amount);
        newDebitTx.setTransType(debitType);
        newDebitTx.setTransStatus(status);
        newDebitTx.setReferenceId(referenceID);
        newDebitTx.setReceiptNumber("RC"+newDebitTx.getReferenceId());
        newDebitTx.setPrice(price!=null?price:null);

        TransferInfo newTransfer = dataManager.create(TransferInfo.class);
        newTransfer.setTransAcct(newCreditTx);
        newTransfer.setToAcct(debitAcct);
        newTransfer.setReason(reason);

        dataManager.save(newCreditTx);
        dataManager.save(newDebitTx);
        dataManager.save(newTransfer);

        // we are not implementing hold wallet amount here
//        if (status.equals(TX_STATUS_PENDING)) { // if status pending, set as hold amount
//            AccountHold holdCreditTx = this.dataManager.create(AccountHold.class);
//            AccountHold holdDebitTx = this.dataManager.create(AccountHold.class);
//
//            holdCreditTx.setAccountBgd(creditAcct);
//            holdCreditTx.setTransactionAcct(newCreditTx);
//            holdCreditTx.setAmount(amount);
//
//            holdDebitTx.setAccountBgd(debitAcct);
//            holdDebitTx.setTransactionAcct(newDebitTx);
//            holdDebitTx.setAmount(amount);
//
//            dataManager.save(holdCreditTx);
//            dataManager.save(holdDebitTx);
//        }
        responseDTO.setDate(timeSource.now().toLocalDateTime());
        responseDTO.setReference(referenceID);
        responseDTO.setStatus("G01");
        responseDTO.setSuccess(true);
        responseDTO.setDescription("SUCCESS");
        return responseDTO;
    }

    @GetMapping("cashWalletHistories")
    public ResponseEntity getCashWalletHistories(Integer days) {
        try {
            UserDetails userDetails = currentAuthentication.getUser();

            Date todayDate = Calendar.getInstance().getTime();
            Date filterDate = DateUtils.addDays(todayDate, -days);

            List<TransactionHistoriesView> historiesViewList = this.dataManager.load(TransactionHistoriesView.class)
                    .query("select e from TransactionHistoriesView e " +
                            "where e.accOwner = :accOwner " +
                            "and e.walletCode = :walletCode " +
                            "and e.createdDate >= :filterDate " +
                            "and e.transactionCode <> :transactionCode " +
                            "and e.amount <> :amount " +
                            "and e.statusCode <> :statusCode " +
                            "order by e.createdDate desc")
                    .parameter("accOwner", userDetails.getUsername())
                    .parameter("amount", BigDecimal.valueOf(0))
                    .parameter("filterDate", filterDate)
                    .parameter("transactionCode", "F10")
                    .parameter("walletCode", "E01")
                    .parameter("statusCode", "G05/01")
                    .list();

            return ResponseEntity.ok(historiesViewList);

        } catch(Exception ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("transactionHistories")
    public ResponseEntity getTransactionHistories(String days, String transactionCode) {

        UserDetails userDetails = currentAuthentication.getUser();

//        String trsCode = "";
//        if (transactionCode.equals("All"))
//            trsCode = "All";
//        else if (transactionCode.equals("Buy"))
//            trsCode = "F05";
//        else if (transactionCode.equals("Sell"))
//            trsCode = "F06";
//        else if (transactionCode.equals("Withdrawal"))
//            trsCode = "F02";
//        else if (transactionCode.equals("Topup"))
//            trsCode = "F01";
//        else if (transactionCode.equals("Redeem"))
//            trsCode = "F04";

        try {
//            String sql = "select json_agg(row_to_json(cj)) as json  from transaction_histories_view st \n" +
//                            "cross join lateral (select amount, transaction, uom, created_date) cj \n" +
//                            "where st.acc_owner = '" + userDetails.getUsername() + "' and transaction_code != 'F10'  group by st.reference_id";
            String sql = "";
            if(days.equals("All") && transactionCode.equals("All"))
                sql = "select json_agg(row_to_json(cj)) as json  from transaction_histories_view st \n" +
                        "cross join lateral (select st.transaction_code, created_date,reference_id, status,status_code, acct_number, amount, transaction, uom) cj  \n" +
//                        "where st.acc_owner = '" + userDetails.getUsername() + "' and st.transaction_code not in ('F09','F10','F09/02','F09/03','F09/B03','F09/B04','F09/B05') \n" +
                        "where st.acc_owner = '" + userDetails.getUsername() + "' and st.transaction_code in ('F01','F02','F03','F04','F05','F06','F03/E01-01','F03/E01-02','F03/E02-02','F12/01','F12/02-01','F12/02-02','F13/E01','F13/E02') and st.status_code != 'G05/02' \n" +
                        "group by st.reference_id order by min(st.created_date) desc";

            else if(days.equals("All") && transactionCode != "All")
                sql = "select json_agg(row_to_json(cj)) as json  from transaction_histories_view st \n" +
                        "cross join lateral (select st.transaction_code, created_date,reference_id, status, status_code, acct_number, amount, transaction, uom) cj  \n" +
                        "where st.transaction_code = '" + transactionCode + "' and st.acc_owner = '" + userDetails.getUsername() + "' and st.transaction_code != 'F10' \n" +
                        "group by st.reference_id order by min(st.created_date) desc";

            else if(days != "All" && transactionCode.equals("All"))
                sql = "select json_agg(row_to_json(cj)) as json  from transaction_histories_view st \n" +
                        "cross join lateral (select st.transaction_code, created_date,reference_id, status,status_code, acct_number, amount, transaction, uom) cj  \n" +
//                        "where st.created_date >= NOW() - INTERVAL '" + days + " days'  and st.acc_owner = '" + userDetails.getUsername() + "' and st.transaction_code not in ('F09','F10','F09/02','F09/03','F09/B03','F09/B04','F09/B05') \n" +
                        "where st.created_date >= NOW() - INTERVAL '" + days + " days'  and st.acc_owner = '" + userDetails.getUsername() + "' and st.transaction_code in ('F01','F02','F03','F04','F05','F06','F03/E01-01','F03/E01-02','F03/E02-02','F12/01','F12/02-01','F12/02-02','F13/E01','F13/E02') and st.status_code != 'G05/02' \n" +
                        "group by st.reference_id order by min(st.created_date) desc";

            else
                sql = "select json_agg(row_to_json(cj)) as json  from transaction_histories_view st \n" +
                        "cross join lateral (select st.transaction_code, created_date,reference_id, status,status_code, acct_number, amount, transaction, uom) cj  \n" +
                        "where st.created_date >= NOW() - INTERVAL '" + days + " days' and st.transaction_code = '" + transactionCode + "' and st.acc_owner = '" + userDetails.getUsername() + "' and st.transaction_code != 'F10' and st.status_code != 'G05/02' \n" +
                        "group by st.reference_id order by min(st.created_date) desc";

            Query query = entityManager.createNativeQuery(sql);
            List resultList = query.getResultList();
            ObjectMapper objectMapper = new ObjectMapper();
            String jsonString = objectMapper.writeValueAsString(resultList);

            // Deserialize JSON string into List of Map objects
            List<Map<String, Object>> jsonList = objectMapper.readValue(jsonString, new TypeReference<List<Map<String, Object>>>(){});

            // Retrieve "value" field from each JSON object in the list
            List<String> valuesList = new ArrayList<>();
            for (Map<String, Object> jsonMap : jsonList) {
                String value = (String) jsonMap.get("value");
                valuesList.add(value);
            }
            return ResponseEntity.ok(valuesList);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("transactionDetails")
    public ResponseEntity getTransactionDetails(@RequestParam String referenceId,@RequestParam String transactionTypeCode,@RequestHeader(value = "Authorization")String authToken ){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500);
        try{

            UserDetails userDetails = currentAuthentication.getUser();
            String query1="select e from AccountBgd e where e.accOwner = :accOwner";
            List<AccountBgd> accounts = this.dataManager.load(AccountBgd.class)
                    .query(query1)
                    .parameter("accOwner",userDetails.getUsername()).list();

            List<String> acc_numbers = new ArrayList<>();
            accounts.forEach(e -> {
                String acc_number = e.getAcctNumber();
                acc_numbers.add(acc_number);
            });

            List<TransactionHistoriesView> transactionAccts = this.dataManager.load(TransactionHistoriesView.class)
                    .condition(LogicalCondition.and(
                            PropertyCondition.inList("acctNumber",acc_numbers),
                            PropertyCondition.equal("reference",referenceId)
                    )).list();

            if(transactionAccts.size()==0){
                throw new RuntimeException("RCE004"); // data not found
            }

            Map<String,Object> responseDetail = new HashMap<>();

            responseDetail.put("referenceID",referenceId);
            responseDetail.put("transactionTypeCode",transactionTypeCode);
            responseDetail.put("statusCode",transactionAccts.get(0).getStatusCode());
            responseDetail.put("dateTime",LocalDateTime.ofInstant(transactionAccts.get(0).getCreatedDate().toInstant(),ZoneId.of("Asia/Kuala_Lumpur")));

            AtomicReference<BigDecimal> totalSSTFee = new AtomicReference<>(BigDecimal.ZERO);
            AtomicReference<BigDecimal> totalGSTFee = new AtomicReference<>(BigDecimal.ZERO);
            AtomicReference<BigDecimal> totalAdminFee = new AtomicReference<>(BigDecimal.ZERO);

            AtomicReference<BigDecimal> netAmount = new AtomicReference<>(BigDecimal.ZERO);
            for (TransactionHistoriesView tx : transactionAccts){

                String txCode = tx.getTransactionCode();
                ///////////////////  topup  ///////////////////
                if(txCode.equals("F01")){
                    responseDetail.put("topupAmount",tx.getAmount().abs());
//                    responseDetail.put("topupMethod",tx.getTransactionMethod());
                    responseDetail.put("topupMethod",getTopUpMethod(referenceId));
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }
                if(txCode.equals("F09/F01")){
                    responseDetail.put("topupFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }

                ///////////////////////  withdraw  ///////////////////
                if(txCode.equals("F02")){
                    responseDetail.put("totalNetAmount",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));

//                    String token = authToken.replaceAll("(?i)bearer\\s*", "");
//                    BankDetailDTO bankDetailDTO = proxyApiService.getBankDetail(token);

                    WithdrawalInfo withdrawalInfo = dataManager.load(WithdrawalInfo.class)
                            .query("select w from WithdrawalInfo w " +
                                    "where w.reference = :reference")
                            .parameter("reference", referenceId)
                            .one();

                    if(withdrawalInfo!=null){
                        responseDetail.put("bankName",withdrawalInfo.getRecipientBankName());
                        responseDetail.put("accountNumber",withdrawalInfo.getRecipientBankNo());
                    }
                }
                if(txCode.equals("F09/F02")){
                    responseDetail.put("withdrawalFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }
                ///////////////////////   transfer  ///////////////////
                if(txCode.equals("F03")){
//                    if(tx.getAmount().signum()<=0) { //sender
                        responseDetail.put("transferedAmount", tx.getAmount().abs());
                        responseDetail.put("price",tx.getPrice());

                     //load transfer info
                    Optional<TransferInfoView> transferInfoView = this.dataManager.load(TransferInfoView.class)
                            .condition(PropertyCondition.equal("reference",referenceId)).optional();
                    if(transferInfoView.isPresent()){
                        responseDetail.put("senderAccNumber",transferInfoView.get().getSenderAccNo());
                        responseDetail.put("receiverAccNumber",transferInfoView.get().getReceiverAccNo());
                        if(transferInfoView.get().getReason() != null)
                            responseDetail.put("reason",transferInfoView.get().getReason());
                        else
                            responseDetail.put("reason","-");

                        String token = authToken.replaceAll("(?i)bearer\\s*", "");

                        String senderName = getName(transferInfoView.get().getSender(),token);
                        responseDetail.put("senderName",senderName);

                        String receiverName = getName(transferInfoView.get().getReceiver(),token);
                        responseDetail.put("receiverName",receiverName);
                    }
                }
                if(txCode.equals("F09/F03")){
                    responseDetail.put("transferFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }

                ////////////////////////Redeem ///////////////////
                if(txCode.equals("F04")){
                    responseDetail.put("redeemGramAmount",tx.getAmount().abs());
                    List<RedeemInfo> redeemInfo = this.dataManager.load(RedeemInfo.class)
                            .query("select e from RedeemInfo e where e.reference = :referenceId")
                            .parameter("referenceId", referenceId).list();

                    if(redeemInfo.size()>0){
                        String[] addressLines = redeemInfo.get(0).getAddress().split(", \n");
                        //remove space at first and end of sentences
                        for (int i = 0; i < addressLines.length; i++) {
                            addressLines[i] = addressLines[i].trim();
                        }
                        responseDetail.put("redeemCoinUnit",redeemInfo.size());
                        responseDetail.put("remarks",redeemInfo.get(0).getRemarks());
                        responseDetail.put("reason",redeemInfo.get(0).getReason());
                        responseDetail.put("deliverTo",redeemInfo.get(0).getName());
                        responseDetail.put("phoneNumber",redeemInfo.get(0).getPhone().trim());
                        responseDetail.put("address1",addressLines[0]);
                        responseDetail.put("address2",addressLines[1]);
                        responseDetail.put("postCode",addressLines[2]);
                        responseDetail.put("city",addressLines[3]);
                        responseDetail.put("state",addressLines[4]);
                    }
                }

                if(txCode.equals("F09/F04")){
                    responseDetail.put("redeemFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }
                if(txCode.equals("F09/B03")){
                    responseDetail.put("courierFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }
                if(txCode.equals("F09/B04")){
                    responseDetail.put("takafulFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }
                if(txCode.equals("F09/B05")){
                    responseDetail.put("mintingFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }

                ///////////////////////   Buy    ///////////////////
                if(txCode.equals("F05")){
                    if(tx.getWalletCode().equals("E02")){//gold
                        responseDetail.put("buyQuantity",tx.getAmount().abs());
                        responseDetail.put("price",tx.getPrice());
                    }
                    else if(tx.getWalletCode().equals("E01")){
                        responseDetail.put("buyAmount",tx.getAmount().abs());
                        netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                    }
                }
                if(txCode.equals("F09/F05")){
                    responseDetail.put("buyFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }

                /////////////////////////   Sell  ///////////////////
                if(txCode.equals("F06")){
                    if(tx.getWalletCode().equals("E02")){//gold
                        responseDetail.put("sellQuantity",tx.getAmount().abs());
                        responseDetail.put("price",tx.getPrice());
                    }
                    else if(tx.getWalletCode().equals("E01")){
                        responseDetail.put("sellAmount",tx.getAmount().abs());
                        netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                    }
                }
                if(txCode.equals("F09/F06")){
                    responseDetail.put("sellFee",tx.getAmount().abs());
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }


                /////////////////////SignUp Reward///////////////
                if(txCode.equals("F12/01")) {
                    try {
                        responseDetail.put("rewardType", tx.getTransaction());
                        responseDetail.put("amount", tx.getAmount());
                        responseDetail.put("uom", tx.getUom());

                        RewardInfo rewardInfo = this.dataManager.load(RewardInfo.class)
                                .query("select e from RewardInfo e " +
                                        "where e.transaction.referenceId = :referenceId")
                                .parameter("referenceId", referenceId)
                                .one();

                        responseDetail.put("promoCode", rewardInfo.getCampaignCode());

                    } catch (Exception ex) {
                        responseDetail.put("promoCode", "-");
                        logger.error(ex.getMessage());
                    }
                }


                //CALCULATE GENERIC FEEs FOR ALL TX TYPE
                if(txCode.equals("F09/01")){ // admin fee
                    totalSSTFee.getAndSet(totalAdminFee.get().add(tx.getAmount()));
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }
                if(txCode.equals("F09/02")){ // sst fee
                    totalSSTFee.getAndSet(totalSSTFee.get().add(tx.getAmount()));
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }
                if(txCode.equals("F09/03")){ //gst fee
                    totalSSTFee.getAndSet(totalGSTFee.get().add(tx.getAmount()));
                    netAmount.getAndSet(netAmount.get().add(tx.getAmount()));
                }

            }

            if(transactionTypeCode.equals("F02")){
                responseDetail.put("withdrawalAmount",netAmount.get().abs());
            }
            else{
                responseDetail.put("totalNetAmount",netAmount.get().abs());
            }

            responseDetail.put("totalAdminFee",totalAdminFee.get().abs());
            responseDetail.put("totalSSTFee",totalSSTFee.get().abs());
            responseDetail.put("totalGSTFee",totalGSTFee.get().abs());

            genericResponse.setData(responseDetail);
            genericResponse.setResponseCode("RCS001");
            genericResponse.setDescription("OK");
            genericResponse.setHttpStatus(200);

        }catch (Exception e){
            logger.info("ERROR: "+e.getMessage());
            genericResponse = apiHandlerService.errorResponseHandler(genericResponse,e.getMessage());
        }

        logger.info(genericResponse.getData().toString());
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);
    }

    @GetMapping("downloadReceipt")
    public ResponseEntity<byte[]> downloadReceipt(@RequestParam String referenceId,@RequestParam String transactionTypeCode, @RequestHeader(value = "Authorization")String authToken ){
        try{
            logger.info("GET: /downloadReceipt?");
            logger.info("referenceId="+referenceId);
            logger.info("&transactionTypeCode="+transactionTypeCode);
            final BigDecimal[] totalFees = {BigDecimal.ZERO};
            final BigDecimal[] totalSST = {BigDecimal.ZERO};

            logger.info("line:"+getLineNumber()+":referenceId :"+referenceId);
            logger.info("transactionTypeCode :"+transactionTypeCode);
            ReceiptForm receipt = new ReceiptForm();
            receipt.setReference(referenceId);
            receipt.setTransactionTypeCode(transactionTypeCode);

            TransactionType transactionType = getTransactionType(transactionTypeCode);
            receipt.setTransactionDescription(transactionType.getDescription());

            UserDetails userDetails = currentAuthentication.getUser();
            String query1="select e from AccountBgd e where e.accOwner = :accOwner";
            List<AccountBgd> accounts = this.dataManager.load(AccountBgd.class)
                    .query(query1)
                    .parameter("accOwner",userDetails.getUsername()).list();

            List<String> acc_numbers = new ArrayList<>();
            List<String> acc_username = new ArrayList<>();
            accounts.forEach(e -> {
                String acc_number = e.getAcctNumber();
                String username = e.getAccOwner();
                acc_numbers.add(acc_number);
                acc_username.add(username);
                logger.info("acc_number :"+acc_number);
            });
            if(acc_numbers.size()==0){
                logger.info("Wallet for "+userDetails.getUsername()+" Not Found");
                return new ResponseEntity<>(new byte[0], HttpStatus.BAD_REQUEST);
            }

            List<TransactionHistoriesView> transactionHistories = this.dataManager.load(TransactionHistoriesView.class)
                    .condition(LogicalCondition.and(
                            PropertyCondition.inList("acctNumber",acc_numbers),
                            PropertyCondition.equal("reference",referenceId)
                    )).list();

            if(transactionHistories.size() == 0){
                logger.info("No transaction found referenceId: " + referenceId + "for user " + userDetails.getUsername());
                return new ResponseEntity<>(new byte[0], HttpStatus.BAD_REQUEST);
            }
            //withdrawal
            if(transactionTypeCode.equals("F02")){
                Optional<WithdrawalInfo>withdrawalInfo=this.dataManager.load(WithdrawalInfo.class)
                        .query("select e from WithdrawalInfo e where e.reference = :referenceId")
                        .parameter("referenceId",referenceId).optional();
                if(withdrawalInfo.isPresent()){
                    receipt.setBankAcct(withdrawalInfo.get().getRecipientBankName());
                    receipt.setBankAcctNumber(withdrawalInfo.get().getRecipientBankNo());
                }
            }
            // gold transfer, redeem gold
            if(transactionTypeCode.equals("F03") || transactionTypeCode.equals("F04")){
//                UUID transactionId = transactionHistories.stream()
//                        .filter(f -> f.getWalletCode().equals("E02") && f.getTransactionCode().equals(transactionTypeCode))
//                        .toList().get(0).getId();

                // [gold transfer,gold received]
                if(transactionTypeCode.equals("F03")){
                    Optional<TransferInfoView> transferInfoView = this.dataManager.load(TransferInfoView.class)
                            .condition(PropertyCondition.equal("reference",referenceId)).optional();
                    if(transferInfoView.isPresent()){
                        receipt.setSenderAcct(transferInfoView.get().getSender());
                        receipt.setToAcct(transferInfoView.get().getReceiverAccNo());
                        if(transferInfoView.get().getReason() != null)
                            receipt.setReason(transferInfoView.get().getReason());
                        else
                            receipt.setReason("-");

                        String token = authToken.replaceAll("(?i)bearer\\s*", "");
                        String receiverName = getName(transferInfoView.get().getReceiver(),token);
                        receipt.setReceiverName(receiverName);

                        if(!acc_numbers.contains(transferInfoView.get().getSenderAccNo())){
                            receipt.setTransactionDescription("Gold Receive");

                            String senderName = getName(transferInfoView.get().getSender(),token);
                            receipt.setSenderName(senderName);
                        }
                    }
                }
//                 redeem gold (F04)
                else {
                    List<RedeemInfo> redeemInfo = this.dataManager.load(RedeemInfo.class)
                            .query("select e from RedeemInfo e where e.reference = :referenceId")
                            .parameter("referenceId", referenceId).list();
                    Integer[] units = {0};
                    redeemInfo.forEach(e-> {
                        units[0] = units[0] + e.getUnit().intValue();
                    });
                    if(redeemInfo.size()>0){
                        String address = redeemInfo.get(0).getAddress();
                        logger.info("delivery address -> " +address);
                        String[] parts = address.split(", \n");
                        for (int i = 0; i < parts.length; i++) {
                            parts[i] = parts[i].trim();
                            logger.info(parts[i]);
                        }
                        receipt.setRedeemUnits(units[0]);
                        receipt.setReason(redeemInfo.get(0).getReason());
                        receipt.setRemark(redeemInfo.get(0).getRemarks());
                        receipt.setDeliverTo(redeemInfo.get(0).getName());
                        receipt.setPhoneNumber("+"+redeemInfo.get(0).getPhone().trim());
                        receipt.setAddress1(parts[0]);
                        receipt.setAddress2(parts[1]);
                        receipt.setPostcode(parts[2]);
                        receipt.setCity(parts[3]);
                        receipt.setState(parts[4]);
                    }
                }
            }

            List<FeesForm> transactionFees = new ArrayList<>();

            transactionHistories.forEach(e-> {
                if(Objects.equals(e.getTransactionCode(), transactionTypeCode)){
                    receipt.setDate(e.getCreatedDate());
                    //amount
                    if(Objects.equals(e.getUomCode(), "J01"))
                        receipt.setAmount(e.getAmount().abs());
                    //quantity
                    if(Objects.equals(e.getUomCode(), "J02"))
                        receipt.setQuantity(e.getAmount().abs());
                    //receipt number
                    receipt.setReceiptNumber(e.getReceiptNumber());
                    logger.info("ReceiptNumber :"+e.getReceiptNumber());
                    //status
                    receipt.setStatusCode(e.getStatusCode());
                    if (Objects.equals(e.getStatusCode(), TX_STATUS_COMPLETED.getCode())){
                        logger.info("statusCode :"+TX_STATUS_COMPLETED.getCode());
                        receipt.setStatus("Successful");
                    }
                    else if (Objects.equals(e.getStatusCode(), TX_STATUS_REJECT.getCode())){
                        logger.info("statusCode :"+TX_STATUS_REJECT.getCode());
                        receipt.setStatus("Unsuccessful");
                    }
                    else
                        receipt.setStatus(e.getStatus());
                    if(Objects.equals(transactionTypeCode, TX_TYPE_TOPUP.getCode())){
//                        receipt.setTransactionMethod(e.getTransactionMethod());
                        String tpMethod=getTopUpMethod(referenceId);
                        receipt.setTransactionMethod(tpMethod);
                    }
                    // total amount
                    if(transactionTypeCode.matches("F05|F06|F03")){
                        // check for price per gram from transaction acc
                        String query="select e from TransactionAcct e where e.referenceId =:referenceId" +
                                " and e.transType =:transactionType and e.price is not null and e.account IN :accounts";
                        List<TransactionAcct> transactionAcct = this.dataManager.load(TransactionAcct.class)
                                .query(query)
                                .parameter("referenceId",referenceId)
                                .parameter("transactionType",transactionType)
                                .parameter("accounts",accounts)
                                .list();
                        if (transactionAcct.size() > 0){
                            receipt.setPrice(transactionAcct.get(0).getPrice());
                        }
                        else if(receipt.getAmount() != null && receipt.getQuantity()!= null && receipt.getQuantity().compareTo(BigDecimal.ZERO) != 0){
                            BigDecimal price_per_gram = receipt.getAmount().divide(receipt.getQuantity(),2, BigDecimal.ROUND_HALF_UP);
                            receipt.setPrice(price_per_gram);
                        }
                    }
                }
                //sst
                else if(Objects.equals(e.getTransactionCode(), "F09/02")){
                    totalSST[0] = totalSST[0].add(e.getAmount().abs());
                }
                else {
                    totalFees[0] = totalFees[0].add(e.getAmount().abs());
                    FeesForm fee = new FeesForm();
                    fee.setFeeType(e.getTransaction());
//                    if(e.getPercent()!=null){
//                        fee.setSetupFees(e.getPercent());
//                        fee.setSetupUom("%");
//                        fee.setSetupUomCode("J04");
//                    }
                    fee.setChargeFees(e.getAmount().abs());
                    fee.setChargeUom(e.getUom());
                    fee.setChargeUomCode(e.getUomCode());
                    transactionFees.add(fee);
                    logger.info("transactionFees :"+transactionFees);
                }
            });
            receipt.setFees(transactionFees);
            if(!Objects.equals(totalSST[0], BigDecimal.ZERO)){
                logger.info("totalSST[0] :-> " + totalSST[0]);
                logger.info("totalFees[0] :-> " + totalFees[0]);
                receipt.setSST(totalSST[0].abs());
                totalFees[0] = totalFees[0].add(totalSST[0].abs());
            }
            if(receipt.getTransactionTypeCode().matches("F03|F04")){
                receipt.setAmountPaid(totalFees[0].abs());
            }
            else if(Objects.equals(receipt.getTransactionTypeCode(), "F05")){
                receipt.setAmountPaid(receipt.getAmount().add(totalFees[0].abs()));
            }
            //topup, sell gold
            else if (receipt.getTransactionTypeCode().matches("F01|F06")){
                receipt.setAmountReceived(receipt.getAmount().subtract(totalFees[0].abs()));
            }
            // cash withdrawal
            else if (Objects.equals(receipt.getTransactionTypeCode(), "F02")){
                receipt.setTotalWithdrawal(receipt.getAmount().add(totalFees[0].abs()));
            }

            // Convert the receipt to a byte array (e.g., PDF or any other format)
            byte[] receiptBytes = generateReceipt.generateTransactionReceipt(receipt);
//            byte[] receiptBytes = generateTransactionReceipt(receipt);

            // Set the appropriate response headers
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_PDF);
            headers.setContentDispositionFormData("attachment", "receipt.pdf");

            // Return the byte array as a ResponseEntity
            logger.info("response -> downloadReceipt:" + HttpStatus.OK);
            return new ResponseEntity<>(receiptBytes, headers, HttpStatus.OK);
        }catch (Exception ex) {
            logger.error("Error downloadReceipt :"+ex.getMessage());
            logger.info("response -> downloadReceipt:" + HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(new byte[0], HttpStatus.BAD_REQUEST);
        }
    }


    private String getName(String username,String token){
        logger.info("getname from username");

        UserDetails userDetails = currentAuthentication.getUser();
//        String getNameUrl = env.getProperty("Get_Name_Url") +"?username="+username;
        String getNameUrl = env.getProperty("Get_Name_Url");
//        logger.info("Get_Name_Url ->"+getNameUrl);

        try{
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(token);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getNameUrl)
                    .queryParam("username", username);
            String url = builder.toUriString();
            logger.info("url -> " + url);

            HttpEntity<String> request = new HttpEntity<>(headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
            if (response.getBody() != null){
                JsonObject responseName = JsonParser.parseString(response.getBody()).getAsJsonObject();
                String name = responseName.get("name").getAsString();
                logger.info("name:"+name);
                return name;
            }
            else
                return "-";
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return "-";
        }
    }


    @GetMapping("transactionHistories2")
    public ResponseEntity getTransactionHistories2() {
        try {

            String jpql = "select ab.acct_number, ab.acc_owner, wt.code as wallet_code, wt.name as wallet, ta.reference_id, tt.code as transaction_code, tt.name as transaction, \n" +
                    "abs(ta.amount) as amount, uom.code as uom_code, uom.name as uom, ts.code as status_code, ts.name as status, ta.created_date \n" +
                    "from account_bgd as ab \n" +
                    "join wallet_type as wt on ab.wallet_type_id = wt.id \n" +
                    "join transaction_acct as ta on ta.account_id = ab.id \n" +
                    "join transaction_type as tt on ta.trans_type_id = tt.id\n" +
                    "join unit_of_measures as uom on ab.unit_measure_id = uom.id \n" +
                    "join trans_status as ts on ta.trans_status_id = ts.id \n" +
                    "where ab.acc_owner = :acc_owner";
            TypedQuery query = (TypedQuery) this.entityManager.createQuery(jpql);
            query.setParameter("acc_owner", currentAuthentication.getUser().getUsername());


            return ResponseEntity.ok(entitySerialization.objectToJson(query.getResultList()));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @PostMapping("preRedeem")
    public ResponseEntity preRedeem(@RequestBody List<WalletRedeemForm> forms) {

        logger.info(" Pre Withdrawal called :");
        String referenceNo = "F04" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F04", "6");


        logger.info("WALLET REDEEM CALLED");
        ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);
        responseDTO.setReference(referenceNo);
        try {
            for (WalletRedeemForm e: forms) {

                AccountBgd accountBgdAsset = getAccountBgd(e.getAcctNumber()); //goldwallet

                if(!accountValidator(accountBgdAsset)){
                    logger.error("REDEEM: ACCOUNT INVALID");
                    throw new RuntimeException("ACCOUNT_INVALID");
                }

                TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
                transactionAcct.setReferenceId(referenceNo);
                transactionAcct.setReceiptNumber("RC"+transactionAcct.getReferenceId());
                transactionAcct.setAccount(accountBgdAsset);
                transactionAcct.setTransType(getTransactionType("F04")); // type redeem
                transactionAcct.setTransStatus(TX_STATUS_PENDING); // status pending
                transactionAcct.setAmount(e.getWeight().negate());  // minus gold wallet
                transactionAcct.setPrice(e.getBursaSellPrice());

                logger.info("REDEEM : Weight to be deducted: "+ transactionAcct.getAmount().toString());
                logger.info("REDEEM : From Account: "+ accountBgdAsset.getAccOwner().toString());
                logger.info("REDEEM : Reference id: "+referenceNo);

                List<RedeemInfo> redeemInfos = new ArrayList<>();

                //create new redeem info , one row per unit, each unit has unique serial no
                for (int i=0; i < e.getUnit().toBigInteger().intValue(); i++) {
                    RedeemInfo redeemInfo = this.dataManager.create(RedeemInfo.class);
                    redeemInfo.setTransaction(transactionAcct);
                    redeemInfo.setReference(transactionAcct.getReferenceId());
                    redeemInfo.setUnit(BigDecimal.valueOf(1));
                    redeemInfo.setName(e.getName());
                    redeemInfo.setPhone(e.getPhone());
                    redeemInfo.setReason(e.getRemarks());
                    redeemInfo.setAddress(e.getAddress() + ", \n" +
                            "" + e.getAddress2() + ", \n" +
                            "" + e.getPostcode() + ", \n" +
                            "" + e.getCity() + ", \n" +
                            "" + e.getState());
                    redeemInfo.setRemarks(e.getRemarks());
                    redeemInfos.add(redeemInfo);
                }

                AtomicReference<BigDecimal> totalFee = new AtomicReference<>(BigDecimal.valueOf(0));
                //create transaction for fee charge
                List<CreateTransactionDTO> dtoList = new ArrayList<>();

                AccountBgd creditAccount = getWallet(accountBgdAsset.getAccOwner(), "E01"); //cashwallet
                e.getFeesForms().forEach(f -> {
                    if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) >= 0) {
                        CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                        dto.setCreditAcct(creditAccount);
                        dto.setDebitAcct(BURSA_CASH_WALLET);
                        dto.setCreditType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setDebitType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setAmount(f.getChargeFees());
                        if(f.getSetupUom().equals("%")){
                            dto.setPercent(f.getSetupFees());
                        }
                        dto.setReference(transactionAcct.getReferenceId());
                        dto.setStatus(TX_STATUS_PENDING);
                        dtoList.add(dto);
                        totalFee.getAndSet(totalFee.get().add(f.getChargeFees()));
                        logger.info("REDEEM: CHARGE FEES SETUP, "+dto.getCreditType().getName() + " SETUP: " + f.getSetupFees()+" ("+f.getSetupUom()+") ");
                        logger.info("REDEEM: CHARGE FEES, "+ dto.getCreditType().getName()+ " Amount: "+dto.getAmount().toString());
                    }
                });

                Date transactionDate=getDateTimeNow();

                transactionAcct.setCreatedDate(transactionDate);
                dataManager.save(transactionAcct);
                createTransaction(dtoList);
                redeemInfos.forEach(redeemInfo -> {
                    dataManager.save(redeemInfo);
                });


                responseDTO.setDate(timeSource.now().toLocalDateTime());
                responseDTO.setTransactionType("F04");
                responseDTO.setStatus("G01");
                responseDTO.setDescription("SUCCESFUL");
                responseDTO.setMessage("Your redemption in progress");
                responseDTO.setSuccess(true);
                responseDTO.setAmount(transactionAcct.getAmount().negate());
            }
            //if success
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(responseDTO);

        } catch (Exception ex) {
            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setStatus("G02");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType("F04");
            responseDTO.setDescription("UNSUCCESSFUL");
            responseDTO.setSuccess(false);
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    private String generateSMS(String phoneNumber, String smsType){
        try{
            logger.info("generate "+smsType+" SMS to " + phoneNumber);
            String smsBody ="Your BGD redemption is successful. Please call 03-27320067 if you didn't perform this activity.";

            smsBody = smsBody.replace(" ", "%20");
            String user = env.getProperty("EXABYTES_USERNAME");
            String pass = env.getProperty("EXABYTES_PASSWORD");
            String url = env.getProperty("EXABYTES_SEND_MESSAGE_URL");
            int type = 1;
            String sendid = "exabytes"; //Malaysia does not support sender id yet
            try{
                URL sendSmsUrl = new URL(url+"?un="+ user +"&pwd=" + pass + "&dstno=" + phoneNumber +
                        "&msg=" + smsBody + "&type=" + type + "&sendid=" + sendid + "&agreedterm=YES");
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                okhttp3.Request request = new okhttp3.Request.Builder()
                        .url(sendSmsUrl)
                        .get()
                        .build();

                okhttp3.Response response = client.newCall(request).execute();
                var StrBody = response.body().string();
                if (response.code()==200){
                    logger.info(StrBody);
                    if(StrBody.startsWith("2000")){
                        logger.info("Successfully sent SMS to " + phoneNumber);
                    }
                    else{
                        logger.error(StrBody);
                        logger.info("message: "+ response.message());
                        return null;
                    }
                }
                else{
                    logger.error("Failed to generate OTP");
                    logger.info(StrBody);
                    return null;
                }
                response.close();
            }
            catch (Exception ex){
                return null;
            }
        }

        catch(Exception ex){
            logger.error(ex.getMessage());
        }
        return "200";
    }

    @PutMapping("walletRedeem")
    public ResponseEntity walletRedeem(@RequestBody UpdateTransactionRedeemForm form) {

        if(form.getReferenceId().equals("") || form.getReferenceId()==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid reference_id");
        }
        logger.info("WALLET REDEEM CALLED");
        ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);
        responseDTO.setDate(timeSource.now().toLocalDateTime());
        try {

            //load all transaction with given referenceid
            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();

            //load also the redeem info
            String query2="select r from RedeemInfo r " +
                    "where r.transaction.referenceId = :transactionReferenceId1 " +
                    "and r.deletedBy is null";
            List<RedeemInfo> redeemInfos = dataManager.load(RedeemInfo.class)
                    .query(query2)
                    .parameter("transactionReferenceId1", form.getReferenceId())
                    .list();

            //validation all exist
            if(transactionAcctList.size() == 0 || redeemInfos.size()==0){
                logger.error("Either transaction or redeem info was not found for referenceId: "+form.getReferenceId());
                throw new RuntimeException("Either transaction and redeem info was not found");
            }

            ///// find Investor AccountBgd Id
            AccountBgd investorAssetWallet = null;
            AccountBgd investorCashWallet = null;

            BigDecimal redeemAmount=null;
            TransactionAcct mainTransaction=null; // used for setting redeemTracker transactionId
            AtomicReference<BigDecimal> totalCash= new AtomicReference<>(BigDecimal.ZERO);
            AtomicReference<BigDecimal> totalGold= new AtomicReference<>(BigDecimal.ZERO);

            // extract required information from transactionAct
            for(TransactionAcct transactionAcct : transactionAcctList) {

                //check the status is still pending
                if (!transactionAcct.getTransStatus().equals(TX_STATUS_PENDING)) {
                    throw new RuntimeException("Invalid ReferenceId , Transaction  is not currently PENDING");
                }
                //find investor cash wallet (if wallet type cash and not bursa's)
                if (transactionAcct.getAccount().getWalletType().getCode().equals("E01") && !transactionAcct.getAccount().equals(BURSA_CASH_WALLET)) {
                    //get total cash that must be paid (basicly this is the fees)
                    totalCash.getAndSet(totalCash.get().add(transactionAcct.getAmount().negate()));
                    investorCashWallet = transactionAcct.getAccount();

                    // finde investor gold wallet (if wallet type gold and not bursa's)
                } else if (transactionAcct.getAccount().getWalletType().getCode().equals("E02")&&!transactionAcct.getAccount().equals(BURSA_ASSET_WALLET)) {
                    //get gold that need to be deducted by investor
                    totalGold.getAndSet(totalGold.get().add(transactionAcct.getAmount().negate()));
                    investorAssetWallet = transactionAcct.getAccount();
                    redeemAmount = transactionAcct.getAmount().negate();
                    mainTransaction =transactionAcct;
                }
            }

            // find cash and gold balance for checking
            String query3="select a from AccountWalletView a " +
                    "where a.acctNumber = :acctNumber1";
            AccountWalletView investorCashView = this.dataManager.load(AccountWalletView.class)
                    .query(query3)
                    .parameter("acctNumber1", investorCashWallet.getAcctNumber())
                    .one();
            String query4="select a from AccountWalletView a " +
                    "where a.acctNumber = :acctNumber1";
            AccountWalletView investorAssetView = this.dataManager.load(AccountWalletView.class)
                    .query(query4)
                    .parameter("acctNumber1", investorAssetWallet.getAcctNumber())
                    .one();

            responseDTO.setReference(form.getReferenceId());
            responseDTO.setAmount(redeemAmount);
            responseDTO.setTransactionType("F04");

            ////// Check balance
            if(totalCash.get().compareTo(investorCashView.getAvailableAmount())>0) {

                logger.info("NOT ENOUGH CASH BALANCE , TOTAL FEES :"+totalCash.get()+". WALLET BALANCE: "+investorCashView.getAvailableAmount().toString());
                responseDTO.setStatus("G02");
                responseDTO.setDescription("NOT_ENOUGH_CASH_BALANCE");
                throw new RuntimeException("NOT_ENOUGH_CASH_BALANCE");
//                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
            }
            else if(totalGold.get().compareTo(investorAssetView.getAvailableAmount())>0) {
                logger.info("NOT ENOUGH GOLD BALANCE , TOTAL REDEEM GRAM :"+totalGold.get()+". WALLET BALANCE: "+investorAssetView.getAvailableAmount().toString());
                responseDTO.setStatus("G02");
                responseDTO.setDescription("NOT_ENOUGH_GOLD_BALANCE");
                throw new RuntimeException("NOT_ENOUGH_GOLD_BALANCE");
//                return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
            }
            else{ //success

                String[] addressLines = redeemInfos.get(0).getAddress().split(", \n");
                if(addressLines.length!=5) {
                    throw new RuntimeException("PROBLEM WHEN PARSING ADDRESS: address total lines is not 5");
                }
                //addressLines[0] > address line 1
                //addressLines[1] > address line 2
                //addressLines[2] > postcode
                //addressLines[3] > city
                //addressLines[4] > state


                ResponseEntity<IntegrationServiceForm> responseAce = supplierGoldAPIController.redemption(redeemAmount,form.getReferenceId(),
                        "", BigDecimal.valueOf(4.25), BigDecimal.valueOf(redeemInfos.size()),
                        redeemInfos.get(0).getName(), form.getInvestorFullName(), redeemInfos.get(0).getPhone(), form.getInvestorPhoneNumber(),
                        addressLines[0], addressLines[1], addressLines[2] + " " + addressLines[3], addressLines[4], addressLines[4], addressLines[2]);

//                logger.info("RES:"+responseAce.getBody().getResponse().toString());
                if (responseAce.getStatusCode().is2xxSuccessful() && !responseAce.getBody().getResponse().toString().contains("error")) {

                    Gson gson = new Gson();
                    JsonObject jsonObject = gson.fromJson(responseAce.getBody().getResponse().toString(),JsonObject.class);


                    logger.info("Response from Ace :"+jsonObject.toString());

                    String redemption_id = jsonObject.get("redemption_id").getAsString();
                    logger.info("Response from Ace.redemption_id: "+ redemption_id);

                    logger.info("Response from Ace.redemption_item: "+jsonObject.get("redemption_item").getAsJsonArray().toString());
                    JsonArray jsonArray = gson.fromJson(jsonObject.get("redemption_item").toString(),JsonArray.class);
                    logger.info("JSON array" + jsonArray.toString());

                    int j=0;
                    logger.info("REDEEM INFO LIST SIZE: "+redeemInfos.size());
                    logger.info("REDEEM jsonArray SIZE "+jsonArray.size());
                    if(redeemInfos.size()==jsonArray.size()) {
                        for (RedeemInfo r : redeemInfos) {
                            logger.info(j);
                            logger.info("Item Detail " + j + ": " + jsonArray.get(j).toString());
                            logger.info("Serial no " + j + ": " + jsonArray.get(j).getAsJsonObject().get("serialno"));
                            r.setSerialNumber(jsonArray.get(j).getAsJsonObject().get("serialno").getAsString().toString());
                            r.setServiceId(responseAce.getBody().getId());
                            r.setServiceResponse(responseAce.getBody().getResponse());
                            r.setRedemptionId(redemption_id);
                            r.setStatus(TX_STATUS_PROCESSING);
                            r.setTrackingNumber("");
                            Date estDeliverydate= Date.from(LocalDateTime.now().atZone(ZoneId.of("Asia/Kuala_Lumpur")).plusDays(5).toInstant());
                            r.setEstDeliveryDateTime(estDeliverydate);
                            j = j + 1;
                        }
                        RedeemTracker redeemTracker = dataManager.create(RedeemTracker.class);
                        redeemTracker.setTransactionId(mainTransaction);
                        redeemTracker.setTrackerStatus(TRACKER_STATUS_REQUEST_PLACED);
                        redeemTracker.setRemarks("Redemption request placed");
                        redeemTracker.setTimeStamp(timeSource.currentTimestamp());

                        //All success
                        this.dataManager.save(redeemTracker);
                        transactionAcctList.forEach(transactionAcct -> {
                            transactionAcct.setTransStatus(TX_STATUS_COMPLETED);
                            this.dataManager.save(transactionAcct);
                        });
                        redeemInfos.forEach(redeemInfo -> {
                            dataManager.save(redeemInfo);
                        });
                    }
                    else{
                        throw new RuntimeException("REDEEM: No of serial number returned not match with total unit redeem");
                    }
                    //send sms successfull redemption
                    generateSMS(form.getInvestorPhoneNumber(),"Redemption");

                } else {
                    logger.error(responseAce.getBody());
                    //set transaction as failed

                    throw new RuntimeException("REDEEM: Ace api return failed: "+responseAce.getBody().getResponse().toString());
                }
                responseDTO.setStatus("G01");
                responseDTO.setDescription("SUCCESFUL");
                responseDTO.setSuccess(true);
                responseDTO.setMessage("Your redemption in progress");
            }
            return ResponseEntity.ok(responseDTO);
        } catch (Exception ex) {

            String query1="select e from TransactionAcct e where e.referenceId =:referenceId";
            List<TransactionAcct> transactionAcctList = this.dataManager.load(TransactionAcct.class)
                    .query(query1)
                    .parameter("referenceId",form.getReferenceId())
                    .list();

            if(transactionAcctList.size() > 0){
                transactionAcctList.forEach(transactionAcct -> {
                    transactionAcct.setTransStatus(TX_STATUS_FAILED);
                    this.dataManager.save(transactionAcct);
                });
                logger.info(transactionAcctList.size()+" transaction with reference: "+ form.getReferenceId() + ", has been set to failed.");
            }

            responseDTO.setStatus("G02");
            responseDTO.setSuccess(false);
            responseDTO.setMessage(ex.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @PutMapping("walletRedeemOld")
    public ResponseEntity walletRedeemOld(@RequestBody List<WalletRedeemForm> forms) {

        logger.info("WALLET REDEEM CALLED");
        ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);
        try {
            for (WalletRedeemForm e: forms) {
//            forms.forEach(e -> {
                AccountBgd accountBgd = getAccountBgd(e.getAcctNumber()); //goldwallet

                if(!accountValidator(accountBgd)){
                    logger.error("REDEEM: ACCOUNT INVALID");
                    throw new RuntimeException("ACCOUNT_INVALID");
                }

                TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
                String referenceNo = "F04" + new SimpleDateFormat("yyyyMMdd").format(getDateToday())+generateSeqNo("F04", "6");
                transactionAcct.setReferenceId(referenceNo);
                transactionAcct.setReceiptNumber("RC"+transactionAcct.getReferenceId());
                transactionAcct.setAccount(accountBgd);
                transactionAcct.setTransType(getTransactionType("F04")); // type redeem
                transactionAcct.setTransStatus(TX_STATUS_COMPLETED); // status processing
                transactionAcct.setAmount(e.getWeight().negate());

                logger.info("REDEEM : Weight to be deducted: "+ transactionAcct.getAmount().toString());
                logger.info("REDEEM : From Account: "+ accountBgd.getAccOwner().toString());
                logger.info("REDEEM : Reference id: "+e.getReference());

                List<RedeemInfo> redeemInfos = new ArrayList<>();

                for (int i=0; i < e.getUnit().toBigInteger().intValue(); i++) {
                    RedeemInfo redeemInfo = this.dataManager.create(RedeemInfo.class);
                    redeemInfo.setTransaction(transactionAcct);
                    redeemInfo.setReference(transactionAcct.getReferenceId());
                    redeemInfo.setUnit(BigDecimal.valueOf(1));
//                    redeemInfo.setSerialNumber(e.getSerialNumber());
                    redeemInfo.setName(e.getName());
                    redeemInfo.setPhone(e.getPhone());
                    redeemInfo.setReason(e.getReason());
                    redeemInfo.setAddress(e.getAddress() + "," +
                            "" + e.getAddress2() + "," +
                            "" + e.getPostcode() + "," +
                            "" + e.getCity() + "," +
                            "" + e.getState());
                    redeemInfo.setRemarks(e.getRemarks());
                    redeemInfos.add(redeemInfo);
                }

                AtomicReference<BigDecimal> totalFee = new AtomicReference<>(BigDecimal.valueOf(0));
                //create transaction for fee charge
                List<CreateTransactionDTO> dtoList = new ArrayList<>();

                AccountBgd creditAccount = getWallet(accountBgd.getAccOwner(), "E01"); //cashwallet
                e.getFeesForms().forEach(f -> {
                    if (f.getChargeFees() != null && f.getChargeFees().compareTo(BigDecimal.ZERO) > 0) {
                        CreateTransactionDTO dto = this.dataManager.create(CreateTransactionDTO.class);
                        dto.setCreditAcct(creditAccount);
                        dto.setDebitAcct(BURSA_CASH_WALLET);
                        dto.setCreditType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setDebitType(getTxTypeTrade(f.getFeeTypeCode()));
                        dto.setAmount(f.getChargeFees());
                        if(f.getSetupUom().equals("%")){
                            dto.setPercent(f.getSetupFees());
                        }
                        dto.setReference(transactionAcct.getReferenceId());
                        dto.setStatus(TX_STATUS_COMPLETED);
                        dtoList.add(dto);
                        totalFee.getAndSet(totalFee.get().add(f.getChargeFees()));
                        logger.info("REDEEM: CHARGE FEES SETUP, "+dto.getCreditType().getName() + " SETUP: " + f.getSetupFees()+" ("+f.getSetupUom()+") ");
                        logger.info("REDEEM: CHARGE FEES, "+ dto.getCreditType().getName()+ " Amount: "+dto.getAmount().toString());
                    }
                });

                String query1="select a from AccountWalletView a " +
                        "where a.acctNumber = :acctNumber1";
                AccountWalletView walletViewAsset= this.dataManager.load(AccountWalletView.class)
                        .query(query1)
                        .parameter("acctNumber1", e.getAcctNumber())
                        .one();
                String query2="select a from AccountWalletView a " +
                        "where a.acctNumber = :acctNumber1";
                AccountWalletView walletViewCash= this.dataManager.load(AccountWalletView.class)
                        .query(query2)
                        .parameter("acctNumber1", creditAccount.getAcctNumber().toString())
                        .one();


                logger.info("REDEEM: TOTAL FEES: "+totalFee.get().toString()+".   CASH WALLET BALANCE: "+walletViewCash.getAvailableAmount().toString());
                logger.info("REDEEM: TOTAL AMOUNT ASSET: "+transactionAcct.getAmount().negate().toString()+".    ASSET WALLET BALANCE: "+walletViewAsset.getAvailableAmount().toString());
                //LAST CHECKING TO CHECK BALANCE IS ENOUGH
                if(totalFee.get().compareTo(walletViewCash.getAvailableAmount())<=0  && transactionAcct.getAmount().negate().compareTo(walletViewAsset.getAvailableAmount())<=0) {
                    //call api ace for redeem to get detail (tracking no)

                    try {
                        ResponseEntity<IntegrationServiceForm> responseAce = supplierGoldAPIController.redemption(e.getWeight(), referenceNo,
                                e.getSerialNumber(), BigDecimal.valueOf(4.25), e.getUnit(),
                                e.getName(), e.getName(), e.getPhone(), e.getPhone(),
                                e.getAddress(), e.getAddress2(), e.getPostcode() + " " + e.getCity(), e.getState(), e.getState(), e.getPostcode());

                        if (responseAce.getStatusCode().is2xxSuccessful() && !responseAce.getBody().getResponse().toString().contains("error")) {

                            Gson gson = new Gson();
                            JsonObject jsonObject = gson.fromJson(responseAce.getBody().getResponse().toString(),JsonObject.class);


                            logger.info("Response from Ace :"+jsonObject.toString());

                            String redemption_id = jsonObject.get("redemption_id").getAsString();
                            logger.info("Response from Ace.redemtion_id: "+ redemption_id);

                            logger.info("Response from Ace.redemption_item: "+jsonObject.get("redemption_item").getAsJsonArray().toString());
                            JsonArray jsonArray = gson.fromJson(jsonObject.get("redemption_item").toString(),JsonArray.class);
                            logger.info("JSON array" + jsonArray.toString());

                            int j=0;
                            logger.info("REDEEM INFO LIST SIZE: "+redeemInfos.size());
                            logger.info("REDEEM jsonArray SIZE "+jsonArray.size());
                            if(redeemInfos.size()==jsonArray.size()) {
                                for (RedeemInfo r : redeemInfos) {
                                    logger.info(j);
                                    logger.info("Item Detail " + j + ": " + jsonArray.get(j).toString());
                                    logger.info("Serial no " + j + ": " + jsonArray.get(j).getAsJsonObject().get("serialno"));
                                    r.setSerialNumber(jsonArray.get(j).getAsJsonObject().get("serialno").getAsString().toString());
                                    r.setServiceId(responseAce.getBody().getId());
                                    r.setServiceResponse(responseAce.getBody().getResponse());
                                    r.setRedemptionId(redemption_id);
                                    r.setStatus(TX_STATUS_PROCESSING);
                                    r.setTrackingNumber("");
                                    Date estDeliverydate= Date.from(LocalDateTime.now().atZone(ZoneId.of("Asia/Kuala_Lumpur")).plusDays(5).toInstant());
                                    r.setEstDeliveryDateTime(estDeliverydate);
                                    j = j + 1;
                                }
                                RedeemTracker redeemTracker = dataManager.create(RedeemTracker.class);
                                redeemTracker.setTransactionId(transactionAcct);
                                redeemTracker.setTrackerStatus(TRACKER_STATUS_REQUEST_PLACED);
                                redeemTracker.setRemarks("Redemption request placed");
                                redeemTracker.setTimeStamp(timeSource.currentTimestamp());




                                this.dataManager.save(transactionAcct);
                                this.dataManager.save(redeemTracker);
                                redeemInfos.forEach(redeemInfo -> {
                                    dataManager.save(redeemInfo);
                                });
                                createTransaction(dtoList);
                            }
                            else{
                                throw new RuntimeException("REDEEM: No of serial number return not match with total unit redeem");
                            }

                        } else {
                            logger.error(responseAce.getBody().getResponse());
                            return ResponseEntity.badRequest().body(responseAce.getBody().getResponse());
                        }

                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                        return ResponseEntity.badRequest().body(ex.getMessage());
                    }
//                String trackingNumber = generateRandomString(10); //dummy tracking number
//                redeemInfo.setTrackingNumber(trackingNumber);

                }
                else{
                    logger.info("REDEEM: NOT ENOUGH BALANCE ");
                    responseDTO.setDate(timeSource.now().toLocalDateTime());
                    responseDTO.setReference(e.getReference());
                    responseDTO.setAmount(totalFee.get().add(transactionAcct.getAmount().negate()));
                    responseDTO.setStatus("G02");
                    responseDTO.setDescription("NOT_ENOUGH_BALANCE");
                    responseDTO.setTransactionType("F04");

                    return ResponseEntity.status(HttpStatus.CONFLICT).body(responseDTO);
                }

                responseDTO.setReference(transactionAcct.getReferenceId());
                responseDTO.setTransactionType(TX_TYPE_REDEEM.getName());
                responseDTO.setStatus(TX_STATUS_PROCESSING.getName());
                responseDTO.setDescription(e.toString());
                responseDTO.setAmount(transactionAcct.getAmount().negate());
//            });
            }

            responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setSuccess(true);
            responseDTO.setMessage("Your redemption in progress");
            return ResponseEntity.ok(responseDTO);
        } catch (Exception ex) {
            responseDTO.setSuccess(false);
            responseDTO.setMessage(ex.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @GetMapping("redemptionTracking")
    public ResponseEntity redemptionTracking(@RequestParam String referenceId){

        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500);
        try{
            String query1="select r from RedeemInfo r " +
                    "where r.transaction.referenceId = :transactionReferenceId1 " +
                    "and r.deletedBy is null";
            List<RedeemInfo> redeemInfos = dataManager.load(RedeemInfo.class)
                    .query(query1)
                    .parameter("transactionReferenceId1", referenceId)
                    .list();


            List<RedeemTracker> redeemTrackers = dataManager.load(RedeemTracker.class)
                    .query("select r from RedeemTracker r " +
                            "where r.transactionId.referenceId = :transactionIdReferenceId1 " +
                            "and r.deletedBy is null " +
                            "order by r.timeStamp desc")
                    .parameter("transactionIdReferenceId1", referenceId)
                    .list();
//            logger.info("ss: "+redeemTrackers.size());
            // build body to call push api at proxy, this api will call firebase to sent notification
            Map<String, Object> mainBody = new HashMap<>();


            if(redeemInfos.size()>0 && redeemTrackers.size()>0) {

                List<Map<String,Object>> trackingStatusList = new ArrayList<>();

                for(RedeemTracker redeemTracker : redeemTrackers){
                    Map<String,Object> stats = new HashMap<>();
                    stats.put("timeStamp",redeemTracker.getTimeStamp().toInstant().atZone(ZoneId.of("Asia/Kuala_Lumpur")).toLocalDateTime().toString());
                    stats.put("trackerStatus",redeemTracker.getTrackerStatus().getName());
                    stats.put("trackerStatusCode",redeemTracker.getTrackerStatus().getCode());
                    stats.put("remark",redeemTracker.getRemarks());
                    trackingStatusList.add(stats);
                }

                mainBody.put("estimatedDelivery",redeemInfos.get(0).getEstDeliveryDateTime().toInstant().atZone(ZoneId.of("Asia/Kuala_Lumpur")).toLocalDateTime().toString());
                mainBody.put("referenceId", referenceId);
                mainBody.put("trackingNo", redeemInfos.get(0).getTrackingNumber());
                mainBody.put("redeemTrackers", trackingStatusList);

                genericResponse.setData(mainBody);
                genericResponse.setResponseCode("RCS001");
                genericResponse.setDescription("OK");
                genericResponse.setHttpStatus(200);

            }else{
                throw new RuntimeException("RCE004"); // data not found
            }

        }catch (Exception e){
            logger.info("ERROR: "+e.getMessage());
            genericResponse = apiHandlerService.errorResponseHandler(genericResponse,e.getMessage());
        }

        logger.info(genericResponse.getData().toString());
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);
    }

    @PostMapping("returnUrl")
    public ResponseEntity returnUrl(@RequestBody ResponseDTO responseDTO) {
        try {
            return ResponseEntity.ok(responseDTO);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("returnUrl")
    public ResponseEntity paynetReturnUrl() {
        try {
            PaynetReturnUrl paynetReturnUrl = this.dataManager.create(PaynetReturnUrl.class);
            paynetReturnUrl.setStatus("Successful/Transaction Accepted");
            this.dataManager.save(paynetReturnUrl);
            return ResponseEntity.ok("Successfully topup wallet");
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }


    @GetMapping("profit_loss")
    public ResponseEntity profitLoss(@RequestParam BigDecimal current_price){
        GenericResponse genericResponse = new GenericResponse();
        genericResponse.setHttpStatus(500);
        try{

//            String query1="select p from ProfitLossCal p " +
//                    "where p.accOwner = :accOwner";
//            ProfitLossCal profitLossCal = dataManager.load(ProfitLossCal.class)
//                    .query(query1)
//                    .parameter("accOwner", currentAuthentication.getUser().getUsername())
//                    .one();
            String query1="select p from ProfitLossV2 p " +
                    "where p.accOwner = :accOwner";
            ProfitLossV2  profitLossV2 = dataManager.load(ProfitLossV2.class)
                    .query(query1)
                    .parameter("accOwner",currentAuthentication.getUser().getUsername())
                    .one();

            BigDecimal averageCostPrice = profitLossV2.getAveragePrice()!=null? profitLossV2.getAveragePrice() : BigDecimal.valueOf(0);
            BigDecimal priceDiff = current_price.subtract(averageCostPrice);
            BigDecimal profitLossAmount = null;
            BigDecimal percentChange = null;

                if (profitLossV2.getCumulativeGold() != null && profitLossV2.getCumulativeGold().compareTo(BigDecimal.ZERO) >= 0 ) {
                    profitLossAmount=priceDiff.multiply(profitLossV2.getCumulativeGold()).setScale(2,RoundingMode.HALF_UP);

                } else{
                    profitLossAmount=BigDecimal.ZERO;
                }
                if (profitLossV2.getCumulativeGold() != null && profitLossV2.getCumulativeGold().compareTo(BigDecimal.ZERO) >= 0 ) {
                    percentChange=priceDiff.divide(averageCostPrice.abs(),2,RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
                } else{
                    profitLossAmount=BigDecimal.ZERO;
                }


            ProfitLossDTO profitLossDTO= dataManager.create(ProfitLossDTO.class);
            profitLossDTO.setAmount(profitLossAmount);
            profitLossDTO.setPercentChanges(percentChange);
            profitLossDTO.setAverageCostPrice(averageCostPrice);


            genericResponse.setData(profitLossDTO);
            genericResponse.setResponseCode("RCS001");
            genericResponse.setDescription("OK");
            genericResponse.setHttpStatus(200);

        }catch (Exception e){
            logger.info("ERROR: "+e.getMessage());
            genericResponse = apiHandlerService.errorResponseHandler(genericResponse,e.getMessage());
        }

        logger.info(genericResponse.getData().toString());
        genericResponse.setDateTime(timeSource.now().toLocalDateTime());
        return ResponseEntity.status(genericResponse.getHttpStatus()).body(genericResponse);
    }

    @Autowired
    SchedulerController schedulerController;
    @GetMapping("archiveTransaction")
    public ResponseEntity<String> archiveTransaction(String date) {
        try {
            UserDetails userDetails = currentAuthentication.getUser();
//            ScheduleController scheduleController = new ScheduleController();
//            scheduleController.archiveTransaction();
            schedulerController.archiveTransaction(date);

            return ResponseEntity.ok("OK");
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }
}
