package com.company.wallet.app;

import com.company.wallet.entity.TransactionHistoriesView;
import io.jmix.core.DataManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class ScheduleController {
    static Logger logger = LogManager.getLogger(WalletApiController.class.getName());
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private DataManager dataManager;
    public void archiveTransaction() {
        try {
            logger.info("************* Schedule transactionHistoriesViewArchive Start ********************");
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("main");
            this.entityManager = emf.createEntityManager();

            LocalDate currentDate = LocalDate.now();
            LocalDate yesterdayDate = currentDate.minusDays(1);

            String sql = "create table ArchiveTransactionHistory_"+ yesterdayDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))+ " " +
                    "as select * from transaction_histories_view thv " +
                    "where thv.created_date < '" +currentDate.toString()+ "' and thv.created_date > '" +yesterdayDate.toString()+ "' ";
            Query query = entityManager.createNativeQuery(sql);
            query.executeUpdate();

            String sql2 = "create table ArchiveTransactionAcct_"+ yesterdayDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"))+ " " +
                    "as select * from transaction_acct ta " +
                    "where ta.created_date < '" +currentDate.toString()+ "' and ta.created_date > '" +yesterdayDate.toString()+ "' ";
            Query query2 = entityManager.createNativeQuery(sql2);
            query2.executeUpdate();


            // Process the result if applicable
            logger.info("************* Schedule transactionHistoriesViewArchive End ********************");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
}
