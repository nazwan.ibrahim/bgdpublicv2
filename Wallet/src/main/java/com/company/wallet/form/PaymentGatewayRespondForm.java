package com.company.wallet.form;

import java.io.Serializable;

public class PaymentGatewayRespondForm implements Serializable {
    private Boolean success;
    private String respondCode;
}
