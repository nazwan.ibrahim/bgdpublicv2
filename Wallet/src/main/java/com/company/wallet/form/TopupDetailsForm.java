package com.company.wallet.form;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class TopupDetailsForm {
    private BigDecimal amount;
    private String reference;
    private String transactionType;
    private String transactionMethodCode;
    private LocalDateTime date;
    private String status;
    private String statusCode;
    private String description;
    private List<FeesForm> fees;

    public String getTransactionMethodCode() {
        return transactionMethodCode;
    }

    public void setTransactionMethodCode(String transactionMethodCode) {
        this.transactionMethodCode = transactionMethodCode;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public static class Transaction{
        private String walletTypeCode;
        private String walletType;
        private BigDecimal amount;
        private String oum;

        public String getWalletTypeCode() {
            return walletTypeCode;
        }

        public void setWalletTypeCode(String walletTypeCode) {
            this.walletTypeCode = walletTypeCode;
        }

        public String getWalletType() {
            return walletType;
        }

        public void setWalletType(String walletType) {
            this.walletType = walletType;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public String getOum() {
            return oum;
        }

        public void setOum(String oum) {
            this.oum = oum;
        }
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FeesForm> getFees() {
        return fees;
    }

    public void setFees(List<FeesForm> fees) {
        this.fees = fees;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
