package com.company.wallet.form;

public class UpdateTransactionWithdrawForm extends UpdateTransactionForm{

    private String investorFullName;


    public String getInvestorFullName() {
        return investorFullName;
    }

    public void setInvestorFullName(String investorFullName) {
        this.investorFullName = investorFullName;
    }


}
