package com.company.wallet.form;

import java.io.Serializable;

public class IntegrationServiceForm implements Serializable {
    private String id;
    private String response;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
