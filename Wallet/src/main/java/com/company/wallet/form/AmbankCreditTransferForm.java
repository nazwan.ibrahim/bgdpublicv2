package com.company.wallet.form;

import java.io.Serializable;

public class AmbankCreditTransferForm implements Serializable {

    private String debitorAccountNo;
    private String creditorAccountType;
    private String receiverBIC;
    private String creditorAccountNo;
    private String creditorAccountName;
    private String amount;
    private String paymentReference;
    private String paymentDescription;
    private String lookUpReference;

//    public AmbankCreditTransferForm(String creditorAccountType, String receiverBIC, String creditorAccountNo, String creditorAccountName, String amount, String paymentReference, String paymentDescription, String lookUpReference) {
//        this.creditorAccountType = creditorAccountType;
//        this.receiverBIC = receiverBIC;
//        this.creditorAccountNo = creditorAccountNo;
//        this.creditorAccountName = creditorAccountName;
//        this.amount = amount;
//        this.paymentReference = paymentReference;
//        this.paymentDescription = paymentDescription;
//        this.lookUpReference = lookUpReference;
//    }

    public String getCreditorAccountType() {
        return creditorAccountType;
    }

    public void setCreditorAccountType(String creditorAccountType) {
        this.creditorAccountType = creditorAccountType;
    }

    public String getReceiverBIC() {
        return receiverBIC;
    }

    public void setReceiverBIC(String receiverBIC) {
        this.receiverBIC = receiverBIC;
    }

    public String getCreditorAccountNo() {
        return creditorAccountNo;
    }

    public void setCreditorAccountNo(String creditorAccountNo) {
        this.creditorAccountNo = creditorAccountNo;
    }

    public String getCreditorAccountName() {
        return creditorAccountName;
    }

    public void setCreditorAccountName(String creditorAccountName) {
        this.creditorAccountName = creditorAccountName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public String getLookUpReference() {
        return lookUpReference;
    }

    public void setLookUpReference(String lookUpReference) {
        this.lookUpReference = lookUpReference;
    }

    public String getDebitorAccountNo() {
        return debitorAccountNo;
    }

    public void setDebitorAccountNo(String debitorAccountNo) {
        this.debitorAccountNo = debitorAccountNo;
    }
}
