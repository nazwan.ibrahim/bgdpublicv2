package com.company.wallet.form;

public class WalletWithdrawForm extends WalletForm{

    private String investorFullName;

    private String investorBankName;

    private String investorBankAccountNo;

    private String investorBankBic;

    public String getInvestorFullName() {
        return investorFullName;
    }

    public void setInvestorFullName(String investorFullName) {
        this.investorFullName = investorFullName;
    }

    public String getInvestorBankName() {
        return investorBankName;
    }

    public void setInvestorBankName(String investorBankName) {
        this.investorBankName = investorBankName;
    }

    public String getInvestorBankAccountNo() {
        return investorBankAccountNo;
    }

    public void setInvestorBankAccountNo(String investorBankAccountNo) {
        this.investorBankAccountNo = investorBankAccountNo;
    }

    public String getInvestorBankBic() {
        return investorBankBic;
    }

    public void setInvestorBankBic(String investorBankBic) {
        this.investorBankBic = investorBankBic;
    }


}
