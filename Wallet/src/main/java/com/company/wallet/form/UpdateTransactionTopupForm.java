package com.company.wallet.form;

public class UpdateTransactionTopupForm extends  UpdateTransactionForm{

    private String channelCode;
    private String bicCode;
    private String identificationTypeCode;
    private String identificationNumber;

    private String transactionMethodCode;

    public String getIdentificationTypeCode() {
        return identificationTypeCode;
    }

    public void setIdentificationTypeCode(String identificationTypeCode) {
        this.identificationTypeCode = identificationTypeCode;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getBicCode() {
        return bicCode;
    }

    public void setBicCode(String bicCode) {
        this.bicCode = bicCode;
    }

    public String getTransactionMethodCode() {
        return transactionMethodCode;
    }

    public void setTransactionMethodCode(String transactionMethodCode) {
        this.transactionMethodCode = transactionMethodCode;
    }
}
