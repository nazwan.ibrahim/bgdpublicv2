package com.company.wallet.form;

import java.io.Serializable;

public class AmbankAccountEnquiryForm implements Serializable {

    private String debitorAccountNo;
    private String amount;
    private String creditorAccountType;
    private String receiverBIC;
    private String creditorAccountNo;
    private String secondValidationIndicator;
    private String idType;
    private String idNo;

    public AmbankAccountEnquiryForm(String creditorAccountType, String receiverBIC, String creditorAccountNo, String secondValidationIndicator, String idType, String idNo) {
        this.creditorAccountType = creditorAccountType;
        this.receiverBIC = receiverBIC;
        this.creditorAccountNo = creditorAccountNo;
        this.secondValidationIndicator = secondValidationIndicator;
        this.idType = idType;
        this.idNo = idNo;
    }

    public String getCreditorAccountType() {
        return creditorAccountType;
    }

    public void setCreditorAccountType(String creditorAccountType) {
        this.creditorAccountType = creditorAccountType;
    }

    public String getReceiverBIC() {
        return receiverBIC;
    }

    public void setReceiverBIC(String receiverBIC) {
        this.receiverBIC = receiverBIC;
    }

    public String getCreditorAccountNo() {
        return creditorAccountNo;
    }

    public void setCreditorAccountNo(String creditorAccountNo) {
        this.creditorAccountNo = creditorAccountNo;
    }

    public String getSecondValidationIndicator() {
        return secondValidationIndicator;
    }

    public void setSecondValidationIndicator(String secondValidationIndicator) {
        this.secondValidationIndicator = secondValidationIndicator;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getDebitorAccountNo() {
        return debitorAccountNo;
    }

    public void setDebitorAccountNo(String debitorAccountNo) {
        this.debitorAccountNo = debitorAccountNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
