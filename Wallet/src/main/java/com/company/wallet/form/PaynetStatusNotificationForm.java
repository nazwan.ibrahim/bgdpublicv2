package com.company.wallet.form;

public class PaynetStatusNotificationForm {

    private Notifications Notification;

    public Notifications getNotification() {
        return Notification;
    }

    public void setNotification(Notifications notification) {
        Notification = notification;
    }

    public static class Notifications {
        private EventHeaders EventHeader;
        private EventInfos EventInfo;

        public EventHeaders getEventHeader() {
            return EventHeader;
        }

        public void setEventHeader(EventHeaders eventHeader) {
            EventHeader = eventHeader;
        }

        public EventInfos getEventInfo() {
            return EventInfo;
        }

        public void setEventInfo(EventInfos eventInfo) {
            EventInfo = eventInfo;
        }

        public static class EventHeaders {
            private String MsgId;
            private String EventCd;

            public String getMsgId() {
                return MsgId;
            }

            public void setMsgId(String msgId) {
                MsgId = msgId;
            }

            public String getEventCd() {
                return EventCd;
            }

            public void setEventCd(String eventCd) {
                EventCd = eventCd;
            }
        }

        public static class EventInfos {
            private String EndToEndID;
            private String Signature;
            private PaymentStatuss PaymentStatus;

            public String getEndToEndID() {
                return EndToEndID;
            }

            public void setEndToEndID(String endToEndID) {
                EndToEndID = endToEndID;
            }

            public String getSignature() {
                return Signature;
            }

            public void setSignature(String signature) {
                Signature = signature;
            }

            public PaymentStatuss getPaymentStatus() {
                return PaymentStatus;
            }

            public void setPaymentStatus(PaymentStatuss paymentStatus) {
                PaymentStatus = paymentStatus;
            }
        }

        public static class PaymentStatuss {
            private String Code;
            private String Substate;
            private String Reason;

            public String getCode() {
                return Code;
            }

            public void setCode(String code) {
                Code = code;
            }

            public String getSubstate() {
                return Substate;
            }

            public void setSubstate(String substate) {
                Substate = substate;
            }

            public String getReason() {
                return Reason;
            }

            public void setReason(String reason) {
                Reason = reason;
            }
        }
    }
}