package com.company.wallet.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class WalletTransferForm implements Serializable {
    private String acctNumberSender;
    private String acctNumberReceiver;
    private BigDecimal amount;
    private BigDecimal bursaSellPrice;
    private String statusCode;
    private String reason;
    private String reference;
    private List<FeesForm> feesForms;


    public String getAcctNumberSender() {
        return acctNumberSender;
    }

    public void setAcctNumberSender(String acctNumberSender) {
        this.acctNumberSender = acctNumberSender;
    }

    public String getAcctNumberReceiver() {
        return acctNumberReceiver;
    }

    public void setAcctNumberReceiver(String acctNumberReceiver) {
        this.acctNumberReceiver = acctNumberReceiver;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<FeesForm> getFeesForms() {
        return feesForms;
    }

    public void setFeesForms(List<FeesForm> feesForms) {
        this.feesForms = feesForms;
    }

    public BigDecimal getBursaSellPrice() {
        return bursaSellPrice;
    }

    public void setBursaSellPrice(BigDecimal bursaSellPrice) {
        this.bursaSellPrice = bursaSellPrice;
    }
}
