package com.company.wallet.form;

import java.io.Serializable;

public class AmbankCTStatusForm implements Serializable{
    private String cTsrcRefNo;


    public String getCTsrcRefNo() {
        return cTsrcRefNo;
    }

    public void setCTsrcRefNo(String cTsrcRefNo) {
        this.cTsrcRefNo = cTsrcRefNo;
    }
}
