package com.company.wallet.form;

public class UpdateTransactionRedeemForm extends UpdateTransactionForm{

    private String investorPhoneNumber;

    private String investorFullName;


    public String getInvestorPhoneNumber() {
        return investorPhoneNumber;
    }

    public void setInvestorPhoneNumber(String investorPhoneNumber) {
        this.investorPhoneNumber = investorPhoneNumber;
    }

    public String getInvestorFullName() {
        return investorFullName;
    }

    public void setInvestorFullName(String investorFullName) {
        this.investorFullName = investorFullName;
    }
}
