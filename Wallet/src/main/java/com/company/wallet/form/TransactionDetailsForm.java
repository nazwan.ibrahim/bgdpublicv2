package com.company.wallet.form;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;

public class TransactionDetailsForm {
    private BigDecimal amount;
    private BigDecimal price;
    private String reference;
    private String transactionType;
    private Date date;
    private String status;
    private String statusCode;
    private String description;
    private List<Transaction> transactions;
    private List<FeesForm> fees;

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public static class Transaction{
        private String walletTypeCode;
        private String walletType;
        private BigDecimal amount;
        private String oum;

        public String getWalletTypeCode() {
            return walletTypeCode;
        }

        public void setWalletTypeCode(String walletTypeCode) {
            this.walletTypeCode = walletTypeCode;
        }

        public String getWalletType() {
            return walletType;
        }

        public void setWalletType(String walletType) {
            this.walletType = walletType;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public String getOum() {
            return oum;
        }

        public void setOum(String oum) {
            this.oum = oum;
        }
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<FeesForm> getFees() {
        return fees;
    }

    public void setFees(List<FeesForm> fees) {
        this.fees = fees;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
